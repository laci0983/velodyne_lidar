function matrOfObstacles = Create2DMatrixOfObstacles(matrOfObstacles3D,sensorOrientation,heightFromGround,maxHeight)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Deleting ground objects and too high objects     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
matrOfObstacles3D(isnan(matrOfObstacles3D(:,3)),:) = [];
matrOfObstacles3D(matrOfObstacles3D(:,3)>maxHeight-heightFromGround,:) = [];
matrOfObstacles3D(matrOfObstacles3D(:,3)<-heightFromGround,:) = [];
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Processing 2D matrix storing obstacle points    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Calculating rotation needed from Lidar location%
e1=sensorOrientation(1,:);
e2=sensorOrientation(2,:);
e3=sensorOrientation(3,:);
RotationMatrix=[e1/norm(e1);e2/norm(e2);e3/norm(e3)];
rotatedMatrix=(RotationMatrix*matrOfObstacles3D')'; 

matrOfObstacles=rotatedMatrix(:,1:2);
%%%%%%Deleting all NaNs from matrix%%%%%%%%%%
matrOfObstacles(isnan(matrOfObstacles(:,1)),:) = [];
matrOfObstacles(isnan(matrOfObstacles(:,2)),:) = [];
end

