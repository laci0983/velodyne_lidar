function possibleParkingPlaces = FindHerringBoneParking(obstaclesInRegionOfInterest,minimalDistanceFromCar,maximalDistanceFromCar,minXsizeOfParkingPlace,minYsizeOfParkingPlace,isItRightSide)



possibleParkingPlaces=[];
drawAllignment=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Creating BW image from obstaclematrix         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%An image can only have positive coordinates with integer coordinates, as
%it will be used with canny edge detecting algorithm only a rough image is
%needed to find significant edges
pictureMatrix=round((obstaclesInRegionOfInterest+abs(min(min(obstaclesInRegionOfInterest)))+1));%*100);
pictureMatrix = unique(pictureMatrix,'rows');
x=round(pictureMatrix(:,1))';
y=round(pictureMatrix(:,2))';
BW=full(sparse(x,y,true));



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Canny edge detection               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BW = edge(BW,'Sobel');
%  figure
% imshow(BW);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Hough Transformation               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Transferring BW image to Hough space%
[H,theta,rho] = hough(BW,'Theta',-35:-0.5:-60);
%Getting the lines with the highest amount of points%
P  = houghpeaks(H,30,'threshold',ceil(0.3*max(H(:))));
lines = houghlines(BW,theta,rho,P,'FillGap',5,'MinLength',7);

if(size(P,1)<15)
    return;
end


% Rho=x*cos(theta)+y*sin(theta) is the equation of line, so to determine
% rotation around axes, theta needes to be found
orientationsOfLines=zeros(length(lines),1);
for k = 1:length(lines)
    orientationsOfLines(k)=lines(k).theta;
end
%Finding the most frequent rotation value%
rotAngle=mean(orientationsOfLines);
alpha=-rotAngle;

Salpha=sind(alpha);
Calpha=cosd(alpha);

%rotation in the opposite direction
rotationMatrix=[Calpha,Salpha;...
    -Salpha Calpha];



obstaclesInRegionOfInterest=(rotationMatrix*obstaclesInRegionOfInterest')';

%     figure
%     plot(obstaclesInRegionOfInterest(:,1),obstaclesInRegionOfInterest(:,2),'rx');

possibleParkingPlaces=...
    findParkingPlace(obstaclesInRegionOfInterest,...
    minimalDistanceFromCar,maximalDistanceFromCar,...
    minXsizeOfParkingPlace,minYsizeOfParkingPlace,alpha,isItRightSide);

%%%inv rotation matrix, for efficient run time%%%
invRotationMatrix=[Calpha,-Salpha;...
    Salpha Calpha];



for i=1:size(possibleParkingPlaces,1)
    
    coordsToTransform=reshape(possibleParkingPlaces(i,:,1:2),[],2);
    TransformedCoords=(invRotationMatrix*coordsToTransform')';
    possibleParkingPlaces(i,:,1:2)=reshape(TransformedCoords,1,[],2);
    closestPoint=max(TransformedCoords(:,2));
    if(closestPoint>-minimalDistanceFromCar) %if it is too close to the car
        possibleParkingPlaces(i,:,:)=0;
    end
end


if(drawAllignment)
    max_len = 0;
    for k = 1:length(lines)
        hold on
        xy = [lines(k).point1; lines(k).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','red');
        
        % Plot beginnings and ends of lines
        plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','green');
        plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','yellow');
        
        % Determine the endpoints of the longest line segment
        len = norm(lines(k).point1 - lines(k).point2);
        if ( len > max_len)
            max_len = len;
            xy_long = xy;
        end
    end
    
    plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','cyan');
end

end

