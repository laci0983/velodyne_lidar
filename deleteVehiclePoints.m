function objectMatrix = deleteVehiclePoints(objectMatrix,carRadius)

objectMatrix(sqrt((objectMatrix(:,1)).^2+(objectMatrix(:,2)).^2)<carRadius,:) = [];
end

