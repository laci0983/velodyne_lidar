%%Testing herringbone parking%%%%

close all
clear all

Im=imread('angle2.jpg');

Im2=imrotate(Im,-90);
BW=im2bw(Im2,0.5);
BW2 = edge(BW,'Sobel');
[rows,cols] = find(BW2==1);
coordinates=[rows cols];
% figure
% plot(coordinates(:,1),coordinates(:,2),'rx');

coordinates(:,2)=coordinates(:,2)-max(coordinates(:,2))-100;

figure
plot(coordinates(:,1),coordinates(:,2),'rx');
axis([0 600 -600 0]);


herringBoneParkings = FindHerringBoneParking(...
    coordinates,1,...
    1000,50,150);

numOfHerringboneParkings=size(herringBoneParkings,1)


if  numOfHerringboneParkings>0
    allignPointsHerringbone=(herringBoneParkings(:,:,3));
    allignPointsHerringbone=allignPointsHerringbone(:,1);
    distancePointsHerringbone=(herringBoneParkings(:,:,4));
    distancePointsHerringbone=distancePointsHerringbone(:,1);
    herringbonePoints=...
        allignPointsHerringbone.*distancePointsHerringbone;
    [herringbonePoint,indexOfBestHerringbone]=max(herringbonePoints);
    
    %%%%inverse transformation%%%
    coordsToTransform=reshape(herringBoneParkings(:,:,1:2),[],2);
    transformedCoords=coordsToTransform;%(invRotationMatrix*coordsToTransform')';
    herringBoneParkings(:,:,1:2)=permute(...
        reshape(transformedCoords,size(herringBoneParkings,1),...
        4, 2), [1 2 3]);
    
%     if(herringbonePoint==0)
%         numOfHerringboneParkings=0;
%         herringBoneParkings=[];
%     end
end


figure
plot(coordinates(:,1),coordinates(:,2),'rx');
axis([0 600 -600 0]);
hold on
if(numOfHerringboneParkings~=0)
    title( {['Quality of herringbone parking is '...
        num2str( herringbonePoint )]...
        ['Distance points ' ...
        num2str( max(distancePointsHerringbone) )]...
        ['Allignment points ' ...
        num2str( max(allignPointsHerringbone) )]...
        });
else
    title('Possible herringbone Parkings');
end
xlabel('Distance [m]');
ylabel('Distance [m]');
for i=1:size(herringBoneParkings,1)
    if(i==indexOfBestHerringbone)
        width=3;
        color=[0 1 0];
    else
        width=0.5;
        color=[0 0 0];
    end
    plot([herringBoneParkings(i,1:4,1),...
        herringBoneParkings(i,1,1)],...
        [herringBoneParkings(i,1:4,2)...
        herringBoneParkings(i,1,2)],'LineWidth',...
        width,'Color',color);
end
