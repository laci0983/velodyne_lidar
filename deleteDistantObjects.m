function closeObjects = deleteDistantObjects(originalObjectMatrix,maxDistance)

%deleting all points, that are too far
closeObjects=originalObjectMatrix;
closeObjects(abs(closeObjects(:,1))>maxDistance,:) = [];
closeObjects(abs(closeObjects(:,2))>maxDistance,:) = [];

%deleting left side
closeObjects(closeObjects(:,2)>0,:) = [];
end

