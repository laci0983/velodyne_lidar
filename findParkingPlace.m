function possibleParkingPlaces = findParkingPlace(obstaclesInRegionOfInterest,minimalDistanceFromCar,maximalDistanceFromCar,minXsizeOfParkingPlace,minYsizeOfParkingPlace,alpha,isItRightSide)

%possibleParkingPlaces=[];
%%%maximum 40 places are to be found%%%%
possibleParkingPlaces=NaN(40,4,4);

ParkingPlace=obstaclesInRegionOfInterest;

%%%%%%%%%%%Finding Parking Place on the right side of the car %%%%%%%%%%%%

%Selecting the right side
ParkingPlace(ParkingPlace(:,2)>0,:) = [];


%%%%Looking for parking places only in Quadrant IV%%%
boxCoordinates=...
    [0, -maximalDistanceFromCar; ...   %bottom left corner
    0,maximalDistanceFromCar+minYsizeOfParkingPlace; ...   %top left corner
    0+minXsizeOfParkingPlace,maximalDistanceFromCar+minYsizeOfParkingPlace; ... %top right corner
    0+minXsizeOfParkingPlace,-maximalDistanceFromCar];  %bottom right corner

%%%%%%Bigger box is used to detect neighbouring cars/objects%%%%
maximalBoxCoordinates=...
    [0, -maximalDistanceFromCar; ...   %bottom left corner
    0,0; ...   %top left corner minimalDistanceFromCar
    0+minXsizeOfParkingPlace,0; ... %top right corner minimalDistanceFromCar
    0+minXsizeOfParkingPlace,-maximalDistanceFromCar];  %bottom right corner

%%%%%Herringbone parking offset%%%%%%%%%
%For herringbone parking an offset is used in order to avoid overhanging
if(0~=alpha)
    parkingPlaceOffset=minXsizeOfParkingPlace/tand(alpha);
else
    parkingPlaceOffset=0;
end


indexOfNewParkingPlace=1;
while maximalBoxCoordinates(4,1)<=maximalDistanceFromCar && indexOfNewParkingPlace<size(possibleParkingPlaces,1)
    coordinatesToAdd=boxCoordinates;
    
    %%%%Find all obstacles within the area%%%%%
    in=inpolygon(ParkingPlace(:,1),ParkingPlace(:,2),maximalBoxCoordinates(:,1),maximalBoxCoordinates(:,2));
    
    if(nnz(in)==0) %%%If there are no obstacles, the car is alligned to the nearby objects/cars
        
        areaToCheckAroundParkingPlace=...   %The box which includes cars around the possible parking place
            [maximalBoxCoordinates(1,1)-minXsizeOfParkingPlace,maximalBoxCoordinates(1,2);...  %bottom left corner
            maximalBoxCoordinates(2,1)-minXsizeOfParkingPlace,maximalBoxCoordinates(2,2);...   %top left corner
            maximalBoxCoordinates(3,1)+minXsizeOfParkingPlace,maximalBoxCoordinates(3,2);      %top right corner
            maximalBoxCoordinates(4,1)+minXsizeOfParkingPlace,maximalBoxCoordinates(4,2)];     %bottom right corner
        
        [coordinatesToAdd,pointForAllignment] =...
            AllignCar(ParkingPlace,areaToCheckAroundParkingPlace,...
            coordinatesToAdd,minYsizeOfParkingPlace,...
            -maximalDistanceFromCar,parkingPlaceOffset);
        
        pointForParkingDistance=...
            GetParkingDistancePoints(coordinatesToAdd,...
            minimalDistanceFromCar,maximalDistanceFromCar,isItRightSide);
        
        %%%%%Possible parking place is added if, it fullfills the
        %%%%%requriement regarding the minimal distance from the car/lidar
        if(max(coordinatesToAdd(:,2))<-minimalDistanceFromCar)
            possibleParkingPlaces(indexOfNewParkingPlace,:,1:2)=coordinatesToAdd;
            possibleParkingPlaces(indexOfNewParkingPlace,1,3:4)=[pointForAllignment,pointForParkingDistance];
            indexOfNewParkingPlace=indexOfNewParkingPlace+1;
        end
        
    else
        %%%If obstacles found in the possible intervall of parkingplace it
        %%%should be checked whether there is still enough place to park
        
        obstacleCoordinates=ParkingPlace(in,:);
        closestObstY=max(obstacleCoordinates(:,2));
        
        
        if(closestObstY+minYsizeOfParkingPlace<=-minimalDistanceFromCar) %check if there is enough space left
            
            coordinatesToAdd=...
                [boxCoordinates(1,1),closestObstY;...
                boxCoordinates(2,1),closestObstY+minYsizeOfParkingPlace;...
                boxCoordinates(3,1),closestObstY+minYsizeOfParkingPlace;
                boxCoordinates(4,1),closestObstY];
            
            
            areaToCheckAroundParkingPlace=... %The box which includes cars around the possible parking place
                [maximalBoxCoordinates(1,1)-minXsizeOfParkingPlace,maximalBoxCoordinates(1,2);...  %bottom left corner
                maximalBoxCoordinates(2,1)-minXsizeOfParkingPlace,maximalBoxCoordinates(2,2);...   %top left corner
                maximalBoxCoordinates(3,1)+minXsizeOfParkingPlace,maximalBoxCoordinates(3,2);      %top right corner
                maximalBoxCoordinates(4,1)+minXsizeOfParkingPlace,maximalBoxCoordinates(4,2)];     %bottom right corner
            
            [coordinatesToAdd,pointForAllignment] =...
                AllignCar(ParkingPlace,areaToCheckAroundParkingPlace,...
                coordinatesToAdd,minYsizeOfParkingPlace,...
                closestObstY,parkingPlaceOffset);
            
            
            pointForParkingDistance=...
                GetParkingDistancePoints(coordinatesToAdd,...
                minimalDistanceFromCar,maximalDistanceFromCar,isItRightSide);
            
            %%%%%Possible parking place is added if, it fullfills the
            %%%%%requriement regarding the minimal distance from the car/lidar
            if(max(coordinatesToAdd(:,2))<-minimalDistanceFromCar)
                possibleParkingPlaces(indexOfNewParkingPlace,:,1:2)=coordinatesToAdd;
                possibleParkingPlaces(indexOfNewParkingPlace,1,3:4)=[pointForAllignment,pointForParkingDistance];
                indexOfNewParkingPlace=indexOfNewParkingPlace+1;
            end
            
        end
        
    end
    
    %%%Checking possible parking places with 0.1 mm resolution
    boxCoordinates(:,1)=boxCoordinates(:,1)+0.1;
    maximalBoxCoordinates(:,1)=maximalBoxCoordinates(:,1)+0.1;
end

%%Deleting unused places from matrix%%
possibleParkingPlaces(isnan(possibleParkingPlaces(:,1)),:,:)=[];
if(isempty(possibleParkingPlaces))
    possibleParkingPlaces=[];
end
end

