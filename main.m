%This program is the second subsystem of the autonomous parking system
%where a LiDAR attached to the top of the vehicle performs the search for
%adequate parking places.

clear all;
close all;
clc

%%Replace Canny with something else optimal: ~10Hz
global draw;
draw=1;
makeVideo=0;

numOfFrame=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Initialization parameters                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sensorLocation=[0,0,0];
sensorOrientation=[...   %Local coordinate system vectors
    0,-1,0;...
    1,0,0;...
    0,0,1];


heightOfLiDARFromGround=1.1;%m
maxHeightFromGround=5; %m

maxDistance=20; %m  , furthes area to be inspected
carRadius=2; %m
minimalDistance=3; %m
parallelParkingMinWidth=4.5;
parallelParkingMinHeight=2;
perpendicularParkingMinWidth=2.5;
perpendicularParkingMinHeight=4;

filepath='road.pcap';

for time=13:30  %iterating through the sample data

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %              Preprocessing pointCloud                    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
  matrOfObstacles3D = Get3DdataFromLIDAR(filepath,time);
    
    originalMatrOfObstacles = Create2DMatrixOfObstacles(matrOfObstacles3D,...
        sensorOrientation,heightOfLiDARFromGround,maxHeightFromGround);
    
    [rotationMatrix,invRotationMatrix] =...
        AllignCoordinateSystem(originalMatrOfObstacles);
    matrOfObstacles=(rotationMatrix*...
        [originalMatrOfObstacles(:,1)';originalMatrOfObstacles(:,2)'])';
    
    %right side
    rightSideObjects = deleteDistantObjects(matrOfObstacles,maxDistance);
    
    rightSideObjects = deleteVehiclePoints(rightSideObjects,carRadius);
    
    
    
    %left side
    mirroringMatrix=[1 zeros(size(matrOfObstacles,2)-1);...
                                zeros(size(matrOfObstacles,2)-1) -1];
                            
    mirroredMatrOfObstacles=matrOfObstacles*mirroringMatrix;    
    leftSideObjects = deleteDistantObjects(mirroredMatrOfObstacles,...
                                                            maxDistance);     
    leftSideObjects = deleteVehiclePoints(leftSideObjects,carRadius);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                 Finding Parking Places                   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %right side parkings
[parallelRight,perpendicularRight,herringboneRight]...
        = findAllParkingPlaces(rightSideObjects,...
                                            minimalDistance,maxDistance,...
                       parallelParkingMinWidth,parallelParkingMinHeight,...
               perpendicularParkingMinWidth,perpendicularParkingMinHeight,1);
    

     
     %left side parkings  
[parallelLeft,perpendicularLeft,herringboneLeft]...
 = findAllParkingPlaces(leftSideObjects,minimalDistance,maxDistance,...
                       parallelParkingMinWidth,parallelParkingMinHeight,...
               perpendicularParkingMinWidth,perpendicularParkingMinHeight,0);

possibleParallelParkings=[parallelRight;parallelLeft];
possiblePerpendicularParkings=[perpendicularRight;perpendicularLeft];
herringBoneParkings=[herringboneRight;herringboneLeft];

%%%count number of parkings
%right side
numOfRightParallel=size(parallelRight,1);
numOfRightPerpendicular=size(perpendicularRight,1);
numOfRightHerringbone=size(herringboneRight,1);
%left side
numOfLeftParallel=size(parallelLeft,1);
numOfLeftPerpendicular=size(perpendicularLeft,1);
numOfLeftHerringbone=size(herringboneLeft,1);
%both
numOfPossibleParalellParkings=size(possibleParallelParkings,1)
numOfPossiblePerpendicularParkings=size(possiblePerpendicularParkings,1)
numOfHerringboneParkings=size(herringBoneParkings,1)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               Secting best parking places                %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    


    %%%%%%%%% Selecting the best parallel parking place %%%%%%%%%%%%%%
    if  numOfPossibleParalellParkings>0
        alligPointsParalell=(possibleParallelParkings(:,:,3));
        alligPointsParalell=alligPointsParalell(:,1);
        distancePointsParalell=(possibleParallelParkings(:,:,4));
        distancePointsParalell=distancePointsParalell(:,1);
        paralellPoints=alligPointsParalell.*distancePointsParalell;
        [paralellPoint,indexOfBestParalell]=max(paralellPoints);
        %%%%inverse transformation%%%
        %right side
        if(numOfRightParallel>0)
            coordsToTransform=reshape(...
                possibleParallelParkings(1:numOfRightParallel,:,1:2),[],2);
            transformedCoords=(invRotationMatrix*coordsToTransform')';
            possibleParallelParkings(1:numOfRightParallel,:,1:2)=...
                permute(reshape(transformedCoords,numOfRightParallel,...
                                                           4, 2), [1 2 3]);
        end
        %left side
        if(numOfLeftParallel>0)
            coordsToTransform=reshape(possibleParallelParkings(...
                                    numOfRightParallel+1:end,:,1:2),[],2);
            transformedCoords=...
                (invRotationMatrix*mirroringMatrix*coordsToTransform')';
            possibleParallelParkings(numOfRightParallel+1:end,:,1:2)=...
                    permute(reshape(transformedCoords,numOfLeftParallel,...
                                                           4, 2), [1 2 3]);
        end
        
    else
        paralellPoint=0;
    end
    
    

    
    %%%%%%%%% Selecting the best perpendicular parking place %%%%%%%%%%%%%
    if numOfPossiblePerpendicularParkings>0
        alligPointsPerPendicular=(possiblePerpendicularParkings(:,:,3));
        alligPointsPerPendicular=alligPointsPerPendicular(:,1);
        distancePointsPendicular=(possiblePerpendicularParkings(:,:,4));
        distancePointsPendicular=distancePointsPendicular(:,1);
        perpendicularPoints=...
            alligPointsPerPendicular.*distancePointsPendicular;
        [perpendicularPoint,indexOfBestPerpendicular]=...
            max(perpendicularPoints);
        
        %%%%inverse transformation%%%
        %right side
        if(numOfRightPerpendicular>0)
            coordsToTransform=reshape(...
                possiblePerpendicularParkings(...
                                    1:numOfRightPerpendicular,:,1:2),[],2);
            transformedCoords=(invRotationMatrix*coordsToTransform')';
            possiblePerpendicularParkings(...
                1:numOfRightPerpendicular,:,1:2)=permute(...
                      reshape(transformedCoords,numOfRightPerpendicular,...
                                                           4, 2), [1 2 3]);
        end
        %left side
        if(numOfLeftPerpendicular>0)
            coordsToTransform=reshape(possiblePerpendicularParkings(...
                                numOfRightPerpendicular+1:end,:,1:2),[],2);
            transformedCoords=...
                (invRotationMatrix*mirroringMatrix*coordsToTransform')';
            possiblePerpendicularParkings(...
                           numOfRightPerpendicular+1:end,:,1:2)=permute(...
                      reshape(transformedCoords,numOfLeftPerpendicular,...
                                                           4, 2), [1 2 3]);
        end
    else
        perpendicularPoint=0;
    end
    
    

    
    %%%%%%%%% Selecting the best Herringbone parking place %%%%%%%%%%%%%
    if  numOfHerringboneParkings>0
        allignPointsHerringbone=(herringBoneParkings(:,:,3));
        allignPointsHerringbone=allignPointsHerringbone(:,1);
        distancePointsHerringbone=(herringBoneParkings(:,:,4));
        distancePointsHerringbone=distancePointsHerringbone(:,1);
        herringbonePoints=...
            allignPointsHerringbone.*distancePointsHerringbone;
        [herringbonePoint,indexOfBestHerringbone]=max(herringbonePoints);
        
        %%%%inverse transformation%%%
        %right side
        if(numOfRightHerringbone>0)
            coordsToTransform=reshape(herringBoneParkings(...
                                      1:numOfRightHerringbone,:,1:2),[],2);
            transformedCoords=(invRotationMatrix*coordsToTransform')';
            herringBoneParkings(...
                                 1:numOfRightHerringbone,:,1:2)=permute(...
                        reshape(transformedCoords,numOfRightHerringbone,...
                                                           4, 2), [1 2 3]);
        end
        %left side
        if(numOfLeftHerringbone>0)
            coordsToTransform=reshape(herringBoneParkings(...
                                   numOfRightHerringbone+1:end,:,1:2),[],2);
            transformedCoords=...
                (invRotationMatrix*mirroringMatrix*coordsToTransform')';
            herringBoneParkings(...
                              numOfRightHerringbone+1:end,:,1:2)=permute(...
                         reshape(transformedCoords,numOfLeftHerringbone,...
                                                           4, 2), [1 2 3]);
        end
        
        if(herringbonePoint==0)
            numOfHerringboneParkings=0;
            herringBoneParkings=[];
        end
    end
    
    
    
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                        Drawings                          %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%Drawing alligned map%%%%%%%%%
    if(draw)
        figure
        plot(matrOfObstacles(:,1),matrOfObstacles(:,2),'rx');
        title('Alligned map');
        xlabel('Distance [m]');
        ylabel('Distance [m]');
    end
    
    %%%%%%%%%Drawing all parallel%%%%%%%%%
    if(draw)
        colors=[1 0 0;0 1 0;0 0 1;1 1 0;1 0 1;0 1 1;0 0 0]; %for drawing
        paralellFigure= figure;
        plot(originalMatrOfObstacles(:,1),originalMatrOfObstacles(:,2),'rx');
        axis([-maxDistance maxDistance -maxDistance maxDistance]);
        title('Possible parallel Parkings');
        xlabel('Distance [m]');
        ylabel('Distance [m]');
    end
    if(draw)

            hold on
            for i=1:numOfPossibleParalellParkings
                if(i==indexOfBestParalell)
                    width=3;
                    color=[0 1 0];
                else
                    width=0.5;
                    color=[0 0 0];
                end
                plot([possibleParallelParkings(i,1:4,1),...
                    possibleParallelParkings(i,1,1)],...
                    [possibleParallelParkings(i,1:4,2)...
                    possibleParallelParkings(i,1,2)],'LineWidth',...
                    width,'Color',color);
            end
    end
    
    %%%%%%%%%Drawing all perpendicular%%%%%%%%%
    if(draw)
        perpendicularFigure = figure;
        plot(originalMatrOfObstacles(:,1),originalMatrOfObstacles(:,2),'rx');
        axis([-maxDistance maxDistance -maxDistance maxDistance]);
        title('Possible perpendicular Parkings');
        xlabel('Distance [m]');
        ylabel('Distance [m]');
    end
    if(draw)
%         for i=1:numOfPossiblePerpendicularParkings
            hold on
            for i=1:numOfPossiblePerpendicularParkings
                if(i==indexOfBestPerpendicular)
                    width=3;
                   color=[0 1 0];
                else
                    width=0.5;
                     color=[0 0 0];
                end
                plot([possiblePerpendicularParkings(i,1:4,1),...
                    possiblePerpendicularParkings(i,1,1)],...
                    [possiblePerpendicularParkings(i,1:4,2)...
                    possiblePerpendicularParkings(i,1,2)],'LineWidth',...
                    width,'Color',color);
            end
    end
    
    %%%Marking the best parallel or perpendicular%%%
    if(draw)
        if(perpendicularPoint>paralellPoint)
            figure (perpendicularFigure)
            hold on
            title( {['Quality of perpendicular parking is '...
                num2str( perpendicularPoint )]...
                ['Distance points '...
                num2str( max(distancePointsPendicular) )]...
                ['Allignment points ' ...
                num2str( max(alligPointsPerPendicular) )]...
                });
            if(makeVideo)
                F(numOfFrame) = getframe(gcf) ;
                numOfFrame=numOfFrame+1;
                drawnow
            end
        elseif(paralellPoint>0)
            figure (paralellFigure)
            hold on
            title( {['Quality of parallel parking is ' ...
                num2str( paralellPoint )]...
                ['Distance points '...
                num2str( max(distancePointsParalell) )]...
                ['Allignment points '...
                num2str( max(alligPointsParalell) )]...
                });
            
            if(makeVideo)
                F(numOfFrame) = getframe(gcf) ;
                numOfFrame=numOfFrame+1;
                drawnow
            end
        end
    end
    
    %%%Plotting herringbone parkings, and marking the best one%%
    if(draw)
        figure
        plot(originalMatrOfObstacles(:,1),originalMatrOfObstacles(:,2),'rx');
        axis([-maxDistance maxDistance -maxDistance maxDistance]);
        hold on
        if(numOfHerringboneParkings~=0)
            title( {['Quality of herringbone parking is '...
                num2str( herringbonePoint )]...
                ['Distance points ' ...
                num2str( max(distancePointsHerringbone) )]...
                ['Allignment points ' ...
                num2str( max(allignPointsHerringbone) )]...
                });
        else
            title('Possible herringbone Parkings');
        end
        xlabel('Distance [m]');
        ylabel('Distance [m]');
        for i=1:size(herringBoneParkings,1)
            if(i==indexOfBestHerringbone)
                width=3;
                color=[0 1 0];
            else
                width=0.5;
                color=[0 0 0];
            end
            plot([herringBoneParkings(i,1:4,1),...
                herringBoneParkings(i,1,1)],...
                [herringBoneParkings(i,1:4,2)...
                herringBoneParkings(i,1,2)],'LineWidth',...
                    width,'Color',color);
        end
    end
    
    
    
end

if(makeVideo)
    % create the video writer with 1 fps
    writerObj = VideoWriter('myVideo.avi');
    writerObj.FrameRate = 1;
    % set the seconds per image
    % open the video writer
    open(writerObj);
    % write the frames to the video
    for i=1:length(F)
        % convert the image to a frame
        frame = F(i) ;
        writeVideo(writerObj, frame);
    end
    % close the writer object
    close(writerObj);
end
