function [coordinatesToAdd,pointForAllignment] = AllignCar(ParkingPlace,areaToCheckAroundParkingPlace,coordinatesToAdd,minYsizeOfParkingPlace,closestObstY,parkingPlaceOffset)
%Alligns car in in the parkingplace if it is possible without any
%collision. The fucntions also calculates a scalar representing the
%quality of allignment, which is inversly proportional with the distance
%from the perfect allignment
%
%
%%%%%%%The allignment should be done only if it won't cause any
%%%%%%%collision
%%%%%%%%%%(0,0)------------------------------------------------------> x
%%%%%%%%%% |
%%%%%%%%%% |
%%%%%%%%%% |                                
%%%%%%%%%% |              ----------------      -----(closestObstYExtended)      
%%%%%%%%%% |   ----       |       P      |  m    -----------------------
%%%%%%%%%% |  ---------   |       A      |  i      --------------
%%%%%%%%%% |              |       R      |  n          ---
%%%%%%%%%% | ----------   |       K      |  Y 
%%%%%%%%%% |              |       I      |  s   ----------------
%%%%%%%%%% |              |       N      |  i
%%%%%%%%%% |              |       G      |  z
%%%%%%%%%% |              |              |  e
%%%%%%%%%% |              |              |  Of
%%%%%%%%%% |              |              |  Parkingplace
%%%%%%%%%% |             -----------------
%%%%%%%%%% |
%%%%%%%%%% |    ------------------------------------- (closestObstY)          
%%%%%%%%%% |
%%%%%%%%%  \/  -y
%
% inputs:     
%             ParkingPlace - The whole area where parking places are to be
%                             searched - array of coordinates representing
%                             the obstacle points 
%                           - nx2 martix
%                             
%              areaToCheckAroundParkingPlace - a bigger square around the
%                                              the car and its surroundings
%                                              This should be at least 3
%                                              times the width of the
%                                              minimal parking place for
%                                              the car, so that the car can
%                                              be alligned to the nearby
%                                              cars
%                                            - 4x2 matrix:
%                                       [x1,y1;x2,y2;x3,y3;x4,y4] - bottom
%                                       left, top left, top right, bottom
%                                       right coordinates
%
%             coordinatestoAdd - Possible parking place without allignment           
%                              -         [x1,y1;x2,y2;x3,y3;x4,y4] - bottom
%                                       left, top left, top right, bottom
%                                       right coordinates
%
%          minYsizeOfParkingPlace - minimal size of parking place measured
%                                   on the y axis (height of parking place)
%
%          closestObstY - coordinate of obstacle point that is the closest
%                         point to the origin (0,0)
%
%         parkingPlaceOffset - for herringbone parking the parking place
%                              should be aligned with the corners of the
%                              vehicle. This parameter can be calculated
%                              with the law of cosines, as the angle of the
%                              parking is known from Hough transform
%                              (~45�), and the width of the car is known
%
% outputs: 
%          coordinatesToAdd - Parking place with allignment, if it can be
%                              done, else it is unchanged (its an input too)
%                            -4x2 matrix (see inputs)
%
%           pointForAllignment - allignment points to indicate the quality
%                                of allignment to the nearby cars


%%%Finding the nearby cars, to which the car should be alligned
in=inpolygon(ParkingPlace(:,1),ParkingPlace(:,2),areaToCheckAroundParkingPlace(:,1),areaToCheckAroundParkingPlace(:,2));
closestObstYExtended=-inf;
pointsToCheck=ParkingPlace(in,:)';


%%%%If car is found around, the maximum y coordinate of the minimal
%%%%bounding box determines the allignment
if(nnz(in)~=0)
    closestObstYExtended=max(pointsToCheck(2,:))-parkingPlaceOffset;
end




if(nnz(in)~=0 && size(pointsToCheck,2)>=3 && (closestObstYExtended-minYsizeOfParkingPlace)>closestObstY && closestObstYExtended<0)
    
    coordinatesToAdd=...
        [coordinatesToAdd(1,1),closestObstYExtended-minYsizeOfParkingPlace;...  %%%bottom left
        coordinatesToAdd(2,1),closestObstYExtended;...                          %%top left
        coordinatesToAdd(3,1),closestObstYExtended;                             %%top right
        coordinatesToAdd(4,1),closestObstYExtended-minYsizeOfParkingPlace];     %%bottom right
    
    
    distanceFromAllignment=abs(abs(closestObstYExtended)-abs(coordinatesToAdd(2,2)));
    pointForAllignment=1-abs(distanceFromAllignment/minYsizeOfParkingPlace);
elseif(nnz(in)~=0)
    distanceFromAllignment=abs(abs(closestObstYExtended)-abs(coordinatesToAdd(2,2)));
    pointForAllignment=1-abs(distanceFromAllignment/minYsizeOfParkingPlace);
else
    pointForAllignment=0;
end

if(pointForAllignment<0)
    pointForAllignment=0;
end
end

