function [possibleParallelParkings,possiblePerpendicularParkings,herringBoneParkings] = findAllParkingPlaces(closeObjects,minimalDistance,maxDistance,...
parallelParkingMinWidth,parallelParkingMinHeight,...
perpendicularParkingMinWidth,perpendicularParkingMinHeight,isItRightSide)


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                 Finding Parking Places                   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%  Finding Parallel parking places %%%%%%%%%%%%%%%%%%
    %finding left side parkings
    possibleParallelParkings=findParkingPlace(closeObjects,...
        minimalDistance, maxDistance,...
        parallelParkingMinWidth,parallelParkingMinHeight,0,isItRightSide);
    %finding right side parkings




    %%%%%%%%%%%%%  Finding perpendicular parking places %%%%%%%%%%%%%%%%%%
    possiblePerpendicularParkings=findParkingPlace(closeObjects,...
        minimalDistance,maxDistance,...
        perpendicularParkingMinWidth,perpendicularParkingMinHeight,0,isItRightSide);
    
    %%%%%%%%%%%%%  Finding Herringbone parking places %%%%%%%%%%%%%%%%%%
    
    herringBoneParkings=FindHerringBoneParking(closeObjects,...
        minimalDistance,maxDistance,...
        perpendicularParkingMinWidth,perpendicularParkingMinHeight,isItRightSide);




end