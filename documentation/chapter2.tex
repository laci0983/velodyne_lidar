%----------------------------------------------------------------------------
\chapter{LiDAR basics}\label{LiDARbasics}
%----------------------------------------------------------------------------
This project uses LiDAR measurements to detect a possible parking place. Velodyne VLP-16 LiDAR data  is used to analyze the surroundings and to find possible parking places for the autonomous vehicle.

%----------------------------------------------------------------------------
\section{LiDAR description}
%----------------------------------------------------------------------------
The VLP-16 sensor uses an array of 16 infra-red (IR) lasers paired with IR detectors to measure distances to objects. The device is mounted securely within a compact, weather-resistant housing. The array of laser/detector pairs spins rapidly within its fixed housing to scan the surrounding environment, firing each laser approximately 18,000 times per second, providing, in real-time, a rich set of 3D point data.

Advanced digital signal processing and waveform analysis provide highly accurate long-range sensing, as well as calibrated reflectivity data, enabling easy detection of retro-reflectors like street-signs, license plates, and lane markings.
Combining 16 laser/detector pairs into one VLP-16 sensor and pulsing each at 18.08 kHz enables measurements of up to 300,000 data points per second -- or double that in dual return mode.\cite{VLP16_Manual}

\section{Processing LiDAR data in MATLAB}

Data processing is performed in MATLAB, where a preprocession of raw data is performed in order to increase the efficiency of the calculations.

\newpage 

\subsection{Data directly from LiDAR}

Data from LiDAR is stored in pointCloud object, which is a 3D-object storing [$x,y,z$] coordinates of obstacles points. As a consequence the pointCloud determines the surroundings of the LiDAR in Cartesian coordinate system, where the LiDAR is located at the origin (0,0,0).



\begin{figure}[ht]
\centering
\includegraphics[width=70mm, keepaspectratio]{figures/pointCloud1.pdf} \vspace{2mm} \includegraphics[width=70mm, keepaspectratio]{figures/pointCloud2.pdf}\\ \hspace{3mm}
\includegraphics[width=70mm, keepaspectratio]{figures/pointCloud3.pdf} \vspace{2mm} \includegraphics[width=70mm, keepaspectratio]{figures/pointCloud4.pdf}\\
\caption{Example pointCloud object read from LiDAR} 
\label{fig:pointCloud} 
\end{figure}

The \figref{pointCloud}~image shows an example of the data read from LiDAR. As it can be seen it is a 3 dimensional representation of the surroundings, where the LiDAR is located at the origin.

%----------------------------------------------------------------------------
\subsection{Transformation of pointCloud to 3D matrix}\label{TransOfPCLtoMatr}
%----------------------------------------------------------------------------
The pointCloud object has location object, which contain $xyz$ coordinates. By transforming it to a matrix (\emph{matrOfObstacles3D}), it can be used in MATLAB to take the initial orientation of the LiDAR into consideration.

%----------------------------------------------------------------------------
\subsection{Taking the orientation of LiDAR into consideration}
%----------------------------------------------------------------------------
The orientation of the LiDAR might be different from the reference coordinate system. The origin of the reference coordinate system represents the center of the vehicle. In order to simplify further calculations let the orientation matrix ($\mathbf{A_{o}}$) of the reference be the identity matrix, and let the position ($\mathbf{p_{o}}$) of the vehicle be at the \emph{origin}.
Let $\mathbf{K_{o}}$ denote the reference coordinate system.

\begin{align} \label{eq:BasisMatrix}
	\mathbf{K_{o}}=
	\begin{bmatrix}
		\mathbf{A_{o}} & | & \mathbf{p_{o}}\\
		- & | & -\\
		\mathbf{0^{T}} & | & 1\\
	\end{bmatrix}	=	
	\begin{bmatrix}
		1 & 0 & 0 & | & 0\\
		0 & 1 & 0 & | & 0\\
		0 & 0 & 1 & | & 0\\
		- & - & - & | & -\\
		0 & 0 & 0 & | & 1\\
	\end{bmatrix}
\end{align}

The reference system of the LiDAR can be described by the following figure:

\begin{figure}[ht]
\centering
\includegraphics[width=150mm, keepaspectratio]{figures/orientationOfLiDAR.png}
\caption{Reference system of the LiDAR \cite{VLP16_Manual}} 
\label{fig:LiDAR_Orientation} 
\end{figure}

The vehicle is moving in $x$ direction (see \figref{LiDAR_Orientation}~figure). 


Let $K_l$ denote the frame fixed to the middle point of the LiDAR\footnote{Or any other sensor that is used for detection}. As in most cases the $K_l$ coordinate system is not identical with $K_o$, a transformation is required in order to determine distances relative to the frame fixed in the middle point of the vehicle. 

The transformation can be expressed as a transformation matrix called \emph{homogenous transformation matrix}.

\begin{align}
\mathbf{K_{l}}=\mathbf{T_{o,l}}\mathbf{K_{o}} \Longrightarrow \mathbf{T_{o,l}} = \mathbf{K_{l}}\mathbf{K_{o}^{-1}}
\label{eq:HomTransMatrixComp}
\end{align}

As $\mathbf{K_{o}}$ is an identity matrix (see \eqref{BasisMatrix}~equation) there is no need for inverse calculation (\eqref{HomTransMatrixComp}~equation). Which means that for the further calculations the \emph{homogenous transformation matrix} is the frame fixed to the center of the sensor.

\begin{align}
\mathbf{T_{o,l}}=\mathbf{K_{l}}=
	\begin{bmatrix}
		\mathbf{A_{o,l}} & | & \mathbf{p_{0,l}}\\
		- & | & -\\
		\mathbf{0^{T}} & | & 1\\
	\end{bmatrix}
\label{eq:HomTransMatrix}
\end{align}

%----------------------------------------------------------------------------
\subsection{Ground segmentation}\label{segmentationChapter}
%----------------------------------------------------------------------------
The \emph{matrOfObstacles3D} contains all the obstacles points detected by the LiDAR. The LiDAR is situated at the top of the vehicle at the height of $h_{0}$ 
(measured from the ground). The eliminations of the obstacle points that do not influence the occupancy of a given parking place, can be performed by the following equation: 
\begin{align}
matrOfObstacles3D(x_{i},y_{i},z_{i})=\left\{
                \begin{array}{ll}
                 \big[ \qquad \big], \quad\text{if} \quad z_{i}<-h_{0} \quad\lor\quad z_{i}>h_{0} \\
                 \big[ x_{i},y_{i},z_{i} \big], \quad\text{otherwise}
                \end{array}
              \right.
		\qquad,\forall i					
\end{align}

This method of ground segmentation was introduced, to make faster execution possible. In the previous version the ground segmentation was based on the normal vector of the surface, which in case of a 3 dimensional space would be $n_{3}=[0 0 1]$ vector. In the future phase of the development, the segmentation based on the normal vector will be used.
%----------------------------------------------------------------------------
\subsection{Transformation of 3D matrix to 2D matrix}
%----------------------------------------------------------------------------

 As parking maneuvers are to be planned in the $x$-$y$ plane, the 3rd ($z$) coordinate is not needed for the calculations. VPL-16 has 16 layers (in $z$ direction) in order to preserve this resolution of sensor about the surroundings, all the 16 layers of VLP-16 are projected to the $x$-$y$ plane. 

For further analysis a $n\times 2$ matrix is created storing all obstacle-point coordinates in $x$-$y$ plane.



