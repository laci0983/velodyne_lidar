%---------------------------------------------------------------------------
\chapter{Finding parking places}\label{findParkingPlaces}
%---------------------------------------------------------------------------
The previous chapters described the preprocessing of the \emph{pointCloud} arriving from the LiDAR (Chapter~\ref{LiDARbasics}),
and the alignment of the coordinate system (Chapter~\ref{CoordSystemAlignment}) projected to the $x-y$ plain.

As the car is moving forward and circling around the possible location of parking places, only the positive $x$ coordinates are taken into consideration since the LiDAR is situated at the origin, and the car is moving along the $x$ axis (see: \figref{LiDAR_Orientation}~figure).

%---------------------------------------------------------------------------
\section{Algorithm of finding possible parking places}
%---------------------------------------------------------------------------

For both parallel and perpendicular parking places the algorithms are the same. The algorithm has the following steps:
\begin{enumerate}
	\item Creating a box (\emph{minimalBox}) representing the minimal parking size needed for the specific parking type
	\item Creating a box (\emph{maximalBox}) around the parking place to check possible collisions, when reaching parking place
	\item Scanning the map (in Quadrant IV - if parking places are to be searched on the right side) to find places within \emph{maximalBox}
	\item Aligning the car to the nearby vehicles/objects
	\item Grading parking place with alignment, and distance from the car points
	\item Choose the best parking place
\end{enumerate}

\newpage
%---------------------------------------------------------------------------
\section{Determining the parameters of minimal parking place needed}
%---------------------------------------------------------------------------
The parking places can be parallel or perpendicular to the roadside. 
The vehicle needs a fixed size of area for parking, which includes the size of the vehicle and safety distances on all sides.


\begin{figure}[ht]
\centering
\includegraphics[width=60mm, keepaspectratio]{figures/minimalPerpendicularParking.png}\\\vspace{5mm}
\includegraphics[width=120mm, keepaspectratio]{figures/minimalParallelParking.png}
\caption{Minimal sizes needed for the specific parkings} 
\label{fig:minmalParkingPlaces}
\end{figure}

For parallel parking the height might not be the same as the width by perpendicular, 
and the width by parallel might differ from the height by perpendicular parking.


\newpage
%---------------------------------------------------------------------------
\section{Determine the parameters of maximal box}
%---------------------------------------------------------------------------

In order to increase the chance of finding a reachable parking place, the algorithm determines a bigger box (\emph{maximalBox}), 
with the same width as the \emph{minimalBox}, but the $y$ coordinates are between 2 values: \emph{minimalDistanceFromCar}, 
and \emph{maximalDistanceFromCar}. This method gives a possibility to determine the minimal and the maximal distance from 
the vehicle, where parking places are to be searched for.
As a consequence in the first step the parking places are to be searched in the following areas:


\begin{figure}[ht]
\centering
\includegraphics[width=150mm, keepaspectratio]{figures/maxBox.pdf}
\caption{Minimal sizes needed for the specific parkings (left: parallel, right: perpendicular)} 
\label{fig:maxBoxOfParkings}
\end{figure}

%---------------------------------------------------------------------------
\section{Scanning the map for parking places}
%---------------------------------------------------------------------------
First of all, it should be determined, whether there is enough place left 
between the car and the closest obstacle point within the \emph{maximalBox}. 
If there is not enough space, than there are no possibilities in this region.

If there is enough space than there are 2 possibilities:

\begin{itemize}
	\item The vehicle can park to \emph{maximalDistanceFromCar} (furthest points of the vehicle will have this $y$ coordinate)
	\item There was an obstacle found in the \emph{maximalBox} with the distance of \emph{closestObstY} , so the furthest point will have the $y$ coordinate of \emph{closestObstY}
\end{itemize}

%---------------------------------------------------------------------------
\section{Align the car to the nearby vehicles or objects}
%---------------------------------------------------------------------------
The vehicle can be aligned to the nearby cars by expanding the \emph{maximalBox} in both $x$ and $-x$ directions. 
The value of extension should be at least a car width ($x$ direction of the minimal parking width), so that the neighboring cars could be detected.
By recalculating the value of \emph{closestObstY}, the closest point of the neighboring car might be detected. This means that by setting the closest point of the vehicle to \emph{closestObstY} value, the vehicle will be aligned with the neighboring cars.\footnote{It should be checked whether between the new and the older value of \emph{closestObstY} there is enough space for the vehicle. Otherwise it can not be aligned}

%---------------------------------------------------------------------------
\section{Grading the parking place}
%---------------------------------------------------------------------------
As it can be seen from the previous paragraphs, the parking places might not be accurate, based on only LiDAR data. As a consequence quality factor needs to be added to these measurements, which determine the quality of the parking place based on several factors. The quality factors are in an interval of $\interval{0}{1}$.


%---------------------------------------------------------------------------
\subsection{Quality of alignment}\label{alignmentQualityChapter}
%---------------------------------------------------------------------------

This quality factor gives grades for the quality of alignment, based on the distance from the perfect alignment. The function of the grading is a triangular function.

\begin{figure}[ht]
\centering
\includegraphics[width=120mm, keepaspectratio]{figures/qualityOfAlignment.pdf}
\caption{The function grading quality based on distance measured from perfect alignment} 
\label{fig:qualityOfAlignment}
\end{figure}

The function shown in \figref{qualityOfAlignment}~figure has the following properties:
\begin{itemize}
	\item The center of this triangular function is always 0, so the input is the distance from the reference point, which would be the perfect alignment.
	\item The distances from which the quality is 0 (no alignment could be done) can be chosen arbitrarily
\end{itemize}

%---------------------------------------------------------------------------
\subsection{Quality factor based on distance from the car}
%---------------------------------------------------------------------------

This quality factor gives grades based on the distance from the car. As it is not obvious to determine an optimal distance from the car, it should/can not be described by a triangular function. The distance from a hypothetical optimum is described by a Gaussian function, because small distances should not be downgraded as much as a too far parking places.

\begin{figure}[ht]
\centering
\includegraphics[width=120mm, keepaspectratio]{figures/qualityOfDistance.pdf}
\caption{The function grading quality based on distance measured from the car} 
\label{fig:qualityOfDistance}
\end{figure}

The function shown in \figref{qualityOfDistance}~figure has the following properties:
\begin{itemize}
	\item The center of this function ($\mu$) is calculated from maximal distance from the vehicle ($d_{max}$) and minimal distance from the vehicle ($d_{min}$) inputs. The optimum is somewhere between these values, usually a bit closer to the vehicle.
		\begin{align}
		 \mu ={\frac{|{d_{max}-d_{min}}|}{2} -2*d_{min}}
		\label{eq:centreOfGaussian}
		\end{align}
			
	\item The $\sigma$ parameter of the Gaussian curve depends on $d_{min}$ and $d_{max}$. It is proportional with the size of the interval, where parking places are to be looked for.
	  \begin{align}
		  \sigma=k*\frac{|d_{max}-d_{min}|}{d_{max}}
		\label{eq:sigmaOfGaussian}
		\end{align}
		
		The parameter $k$ can be chosen arbitrarily\footnote{For the application I have chosen $k=3$} in the interval of $\left[1 ,5\right]$ .

\end{itemize}

%---------------------------------------------------------------------------
\subsection{Getting the final grade}
%---------------------------------------------------------------------------

As several factors determine the final quality of a parking places, a final score should be created. 
\begin{figure}[ht]
\centering
\includegraphics[width=150mm, keepaspectratio]{figures/finalGrade.pdf}
\caption{The input of the algorithm is represented by the green line, and the outputs are $q_{1}, q_{2}$} 
\label{fig:finalGrade}
\end{figure}

All of these grades are scaled between $\left[0, 1\right]$, and if any of these scores are 0, the final score should be 0. As a consequence a simple arithmetical product\footnote{Minimum can be used too} of the factors can give the final score.

For $n$ quality factors the final grade can be calculated with the following formula:
	  \begin{align}
		  q=\prod_{i=1}^{n}q_{i}
		\label{eq:sigmaOfGaussian}
		\end{align}








