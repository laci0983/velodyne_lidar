function [rotationMatrix,invRotationMatrix] = AllignCoordinateSystem(matrOfObstacles)
%This function determines the 2 dimensional rotation matrix, that is needed
%to allign the matrix of obstacles, with the parking cars, and the 
%roadside, to/with which the parking should be perpendicular/parallel.
%The function uses Canny edge detection, Hough transformation to determine
%the roatation matrix that is needed.
%
%input:     
%           matrOfObstacles  -Matrix storing all obstacles coordinates in
%                             in the possible parking zone
%                            - nx2 matrix [x,y]
%                        
%output:    
%           rotationMatrix   -Matrix of rotation around origin
%                            -2x2 matrix


drawAllignmentFigures=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Creating BW image from obstaclematrix         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%An image can only have positive coordinates with integer coordinates, as
%it will be used with canny edge detecting algorithm only a rough image is
%needed to find significant edges
obstacleMatrix=round((matrOfObstacles+abs(min(min(matrOfObstacles)))+1).*10);
obstacleMatrix = unique(obstacleMatrix,'rows');
x=round(obstacleMatrix(:,1))';
y=round(obstacleMatrix(:,2))';
BW=full(sparse(x,y,true));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Edge detection                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BW = edge(BW,'Sobel');

if(drawAllignmentFigures)
    figure
    imshow(BW);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Hough Transformation               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Transferring BW image to Hough space%
[H,theta,rho] = hough(BW,'Theta',-45:0.5:45);
%Getting the lines with the highest amount of points%
P  = houghpeaks(H,1,'threshold',ceil(0.3*max(H(:))));
lines = houghlines(BW,theta,rho,P,'FillGap',5,'MinLength',7);

%If no lines found, Identity matrix is returned%
if(length(lines)<1)
    rotationMatrix=eye(2);
    invRotationMatrix=eye(2);
    return;
end

% Rho=x*cos(theta)+y*sin(theta) is the equation of line, so to determine
% rotation around axes, theta needes to be found
orientationsOfLines=zeros(length(lines),1);
for k = 1:length(lines)
    orientationsOfLines(k)=lines(k).theta;
end
%Finding the most frequent rotation value%
rotAngle=mode(orientationsOfLines);
alpha=-rotAngle;

Salpha=sind(alpha);
Calpha=cosd(alpha);

%rotation in the opposite direction
rotationMatrix=[Calpha,Salpha;...
    -Salpha Calpha];

%%%inv rotation matrix, for efficient run time%%%
invRotationMatrix=[Calpha,-Salpha;...
    Salpha Calpha];

%For debugging purposes the chosen line can be drawn
if(drawAllignmentFigures)
    
    max_len = 0;
    for k = 1:length(lines)
        hold on
        xy = [lines(k).point1; lines(k).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','red');
        
        % Plot beginnings and ends of lines
        plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','green');
        plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','yellow');
        
        % Determine the endpoints of the longest line segment
        len = norm(lines(k).point1 - lines(k).point2);
        if ( len > max_len)
            max_len = len;
            xy_long = xy;
        end
    end
    
    plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','cyan');
end

end

