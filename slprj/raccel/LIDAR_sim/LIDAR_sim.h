#include "__cf_LIDAR_sim.h"
#ifndef RTW_HEADER_LIDAR_sim_h_
#define RTW_HEADER_LIDAR_sim_h_
#include <stddef.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "rtw_modelmap.h"
#ifndef LIDAR_sim_COMMON_INCLUDES_
#define LIDAR_sim_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "simtarget/slSimTgtSigstreamRTW.h"
#include "simtarget/slSimTgtSlioCoreRTW.h"
#include "simtarget/slSimTgtSlioClientsRTW.h"
#include "simtarget/slSimTgtSlioSdiRTW.h"
#include "sigstream_rtw.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "raccel.h"
#include "slsv_diagnostic_codegen_c_api.h"
#include "rt_logging.h"
#include "dt_info.h"
#include "ext_work.h"
#endif
#include "LIDAR_sim_types.h"
#include "multiword_types.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#include "rtGetNaN.h"
#include "mwmathutil.h"
#include "rt_defines.h"
#define MODEL_NAME LIDAR_sim
#define NSAMPLE_TIMES (2) 
#define NINPUTS (0)       
#define NOUTPUTS (0)     
#define NBLOCKIO (15) 
#define NUM_ZC_EVENTS (0) 
#ifndef NCSTATES
#define NCSTATES (0)   
#elif NCSTATES != 0
#error Invalid specification of NCSTATES defined in compiler command
#endif
#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm) (*rt_dataMapInfoPtr)
#endif
#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val) (rt_dataMapInfoPtr = &val)
#endif
#ifndef IN_RACCEL_MAIN
#endif
typedef struct { boolean_T kawqnhxb2f [ 50000 ] ; boolean_T govgf3u3xk [
50000 ] ; real_T goq3onclur [ 640 ] ; } j5iaypgo4w ; typedef struct { real_T
jchx0zuxkl [ 100000 ] ; real_T pfdcc5z305 [ 100000 ] ; real_T fkte2rffmt [
100000 ] ; real_T cg1rhxssex [ 100000 ] ; real_T iaodizuncw [ 50000 ] ;
real_T h02wktfbca [ 50000 ] ; real_T bfl2j5cwsl [ 50000 ] ; boolean_T
bppyky21i0 [ 50000 ] ; boolean_T i0nqmhqe3w [ 50000 ] ; boolean_T pcks52bgwm
[ 28928 ] ; real_T ezzhteg1xo [ 640 ] ; real_T mexy5kiv0b [ 640 ] ; real_T
p2yyp3srr2 [ 640 ] ; real_T kha5mzzgqc ; real_T hdaavyjubq ; real_T
lugzgbxfhr ; real_T bm2ecttvmh ; real_T olntrt1e2z ; real_T lgqeexcw0n ;
real_T fk5xnfg3yk [ 100000 ] ; real_T hcam2mccgn [ 640 ] ; real_T i0g1d0zord
[ 100000 ] ; real_T iacoajksff [ 4 ] ; real_T mv4usdwq1o [ 4 ] ; j5iaypgo4w
pfn4jkw0lz ; j5iaypgo4w fc5iplyfcw ; } B ; typedef struct { struct { void *
LoggedData ; } j4r0p4r45o ; struct { void * LoggedData ; } i0jmyypjsr ;
struct { void * LoggedData ; } bzoqinxqop ; struct { void * LoggedData ; }
elniu2v02l ; struct { void * LoggedData ; } mkxnotw5mg ; struct { void *
LoggedData ; } h4ilxvk1wn ; struct { void * LoggedData ; } gspnaslyzx ;
struct { void * LoggedData ; } eikhvowyc2 ; struct { void * LoggedData ; }
cbxrulyzt1 ; struct { void * LoggedData ; } axkpr3abd1 ; struct { void *
LoggedData ; } kx1yhpclz5 ; struct { void * LoggedData ; } oebh0ofekg ;
struct { void * LoggedData ; } ptxdqhbsen ; struct { void * LoggedData ; }
era0vhkehy ; struct { void * LoggedData ; } fcvgduedbv ; } DW ; typedef
struct { rtwCAPI_ModelMappingInfo mmi ; } DataMapInfo ; struct P_ { real_T
carRadius ; real_T heightOfLiDARFromGround ; real_T matrOfObstacles3D [ 86784
] ; real_T maxDistance ; real_T maxHeightFromGround ; real_T minimalDistance
; real_T parallelParkingMinHeight ; real_T parallelParkingMinWidth ; real_T
perpendicularParkingMinHeight ; real_T perpendicularParkingMinWidth ; real_T
sensorOrientation [ 9 ] ; real_T Constant_Value ; real_T Constant1_Value ; }
; extern const char * RT_MEMORY_ALLOCATION_ERROR ; extern B rtB ; extern DW
rtDW ; extern P rtP ; extern const rtwCAPI_ModelMappingStaticInfo *
LIDAR_sim_GetCAPIStaticMap ( void ) ; extern SimStruct * const rtS ; extern
const int_T gblNumToFiles ; extern const int_T gblNumFrFiles ; extern const
int_T gblNumFrWksBlocks ; extern rtInportTUtable * gblInportTUtables ; extern
const char * gblInportFileName ; extern const int_T gblNumRootInportBlks ;
extern const int_T gblNumModelInputs ; extern const int_T
gblInportDataTypeIdx [ ] ; extern const int_T gblInportDims [ ] ; extern
const int_T gblInportComplex [ ] ; extern const int_T gblInportInterpoFlag [
] ; extern const int_T gblInportContinuous [ ] ; extern const int_T
gblParameterTuningTid ; extern DataMapInfo * rt_dataMapInfoPtr ; extern
rtwCAPI_ModelMappingInfo * rt_modelMapInfoPtr ; void MdlOutputs ( int_T tid )
; void MdlOutputsParameterSampleTime ( int_T tid ) ; void MdlUpdate ( int_T
tid ) ; void MdlTerminate ( void ) ; void MdlInitializeSizes ( void ) ; void
MdlInitializeSampleTimes ( void ) ; SimStruct * raccel_register_model ( void
) ;
#endif
