#include "__cf_LIDAR_sim.h"
#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "LIDAR_sim_capi_host.h"
#define sizeof(s) ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el) ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s) (s)    
#else
#include "builtin_typeid_types.h"
#include "LIDAR_sim.h"
#include "LIDAR_sim_capi.h"
#include "LIDAR_sim_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST                  
#define TARGET_STRING(s)               (NULL)                    
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif
static const rtwCAPI_Signals rtBlockSignals [ ] = { { 0 , 3 , TARGET_STRING (
"LIDAR_sim/FindParallelParkingPlaces" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 ,
0 , 0 } , { 1 , 4 , TARGET_STRING (
"LIDAR_sim/FindPerpendicularParkingPlaces" ) , TARGET_STRING ( "" ) , 0 , 0 ,
0 , 0 , 0 } , { 2 , 9 , TARGET_STRING ( "LIDAR_sim/MATLAB Function" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 1 , 0 , 0 } , { 3 , 10 , TARGET_STRING (
"LIDAR_sim/MATLAB Function1" ) , TARGET_STRING ( "" ) , 0 , 0 , 2 , 0 , 0 } ,
{ 4 , 10 , TARGET_STRING ( "LIDAR_sim/MATLAB Function1" ) , TARGET_STRING (
"" ) , 1 , 0 , 2 , 0 , 0 } , { 5 , 10 , TARGET_STRING (
"LIDAR_sim/MATLAB Function1" ) , TARGET_STRING ( "" ) , 2 , 0 , 2 , 0 , 0 } ,
{ 6 , 10 , TARGET_STRING ( "LIDAR_sim/MATLAB Function1" ) , TARGET_STRING (
"" ) , 3 , 0 , 2 , 0 , 0 } , { 7 , 10 , TARGET_STRING (
"LIDAR_sim/MATLAB Function1" ) , TARGET_STRING ( "" ) , 4 , 0 , 2 , 0 , 0 } ,
{ 8 , 10 , TARGET_STRING ( "LIDAR_sim/MATLAB Function1" ) , TARGET_STRING (
"" ) , 5 , 0 , 2 , 0 , 0 } , { 9 , 1 , TARGET_STRING (
"LIDAR_sim/AllignCoordinateSystem/MATLAB Function1" ) , TARGET_STRING ( "" )
, 0 , 0 , 3 , 0 , 0 } , { 10 , 1 , TARGET_STRING (
"LIDAR_sim/AllignCoordinateSystem/MATLAB Function1" ) , TARGET_STRING ( "" )
, 1 , 0 , 3 , 0 , 0 } , { 11 , 2 , TARGET_STRING (
"LIDAR_sim/AllignCoordinateSystem/MATLAB Function2" ) , TARGET_STRING ( "" )
, 0 , 0 , 1 , 0 , 0 } , { 12 , 7 , TARGET_STRING (
"LIDAR_sim/FindPossibleHerringBoneParkings/MATLAB Function3" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 13 , 11 , TARGET_STRING (
"LIDAR_sim/SelectTheRegionOfInterest/MATLAB Function3" ) , TARGET_STRING ( ""
) , 0 , 0 , 1 , 0 , 0 } , { 14 , 12 , TARGET_STRING (
"LIDAR_sim/SelectTheRegionOfInterest/MATLAB Function4" ) , TARGET_STRING ( ""
) , 0 , 0 , 1 , 0 , 0 } , { 0 , 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 , 0 , 0 }
} ; static const rtwCAPI_BlockParameters rtBlockParameters [ ] = { { 15 ,
TARGET_STRING ( "LIDAR_sim/FindPossibleHerringBoneParkings/Constant" ) ,
TARGET_STRING ( "Value" ) , 0 , 2 , 0 } , { 16 , TARGET_STRING (
"LIDAR_sim/FindPossibleHerringBoneParkings/Constant1" ) , TARGET_STRING (
"Value" ) , 0 , 2 , 0 } , { 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 } } ; static
const rtwCAPI_ModelParameters rtModelParameters [ ] = { { 17 , TARGET_STRING
( "carRadius" ) , 0 , 2 , 0 } , { 18 , TARGET_STRING (
"heightOfLiDARFromGround" ) , 0 , 2 , 0 } , { 19 , TARGET_STRING (
"matrOfObstacles3D" ) , 0 , 4 , 0 } , { 20 , TARGET_STRING ( "maxDistance" )
, 0 , 2 , 0 } , { 21 , TARGET_STRING ( "maxHeightFromGround" ) , 0 , 2 , 0 }
, { 22 , TARGET_STRING ( "minimalDistance" ) , 0 , 2 , 0 } , { 23 ,
TARGET_STRING ( "parallelParkingMinHeight" ) , 0 , 2 , 0 } , { 24 ,
TARGET_STRING ( "parallelParkingMinWidth" ) , 0 , 2 , 0 } , { 25 ,
TARGET_STRING ( "perpendicularParkingMinHeight" ) , 0 , 2 , 0 } , { 26 ,
TARGET_STRING ( "perpendicularParkingMinWidth" ) , 0 , 2 , 0 } , { 27 ,
TARGET_STRING ( "sensorOrientation" ) , 0 , 5 , 0 } , { 0 , ( NULL ) , 0 , 0
, 0 } } ;
#ifndef HOST_CAPI_BUILD
static void * rtDataAddrMap [ ] = { & rtB . fc5iplyfcw . goq3onclur [ 0 ] , &
rtB . pfn4jkw0lz . goq3onclur [ 0 ] , & rtB . fk5xnfg3yk [ 0 ] , & rtB .
kha5mzzgqc , & rtB . hdaavyjubq , & rtB . lugzgbxfhr , & rtB . bm2ecttvmh , &
rtB . olntrt1e2z , & rtB . lgqeexcw0n , & rtB . iacoajksff [ 0 ] , & rtB .
mv4usdwq1o [ 0 ] , & rtB . i0g1d0zord [ 0 ] , & rtB . hcam2mccgn [ 0 ] , &
rtB . pfdcc5z305 [ 0 ] , & rtB . jchx0zuxkl [ 0 ] , & rtP . Constant_Value ,
& rtP . Constant1_Value , & rtP . carRadius , & rtP . heightOfLiDARFromGround
, & rtP . matrOfObstacles3D [ 0 ] , & rtP . maxDistance , & rtP .
maxHeightFromGround , & rtP . minimalDistance , & rtP .
parallelParkingMinHeight , & rtP . parallelParkingMinWidth , & rtP .
perpendicularParkingMinHeight , & rtP . perpendicularParkingMinWidth , & rtP
. sensorOrientation [ 0 ] , } ; static int32_T * rtVarDimsAddrMap [ ] = { (
NULL ) } ;
#endif
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap [ ] = { { "double" ,
"real_T" , 0 , 0 , sizeof ( real_T ) , SS_DOUBLE , 0 , 0 } } ;
#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif
static TARGET_CONST rtwCAPI_ElementMap rtElementMap [ ] = { { ( NULL ) , 0 ,
0 , 0 , 0 } , } ; static const rtwCAPI_DimensionMap rtDimensionMap [ ] = { {
rtwCAPI_MATRIX_COL_MAJOR_ND , 0 , 3 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 3 ,
2 , 0 } , { rtwCAPI_SCALAR , 5 , 2 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 7 , 2
, 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 9 , 2 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR
, 11 , 2 , 0 } } ; static const uint_T rtDimensionArray [ ] = { 40 , 4 , 4 ,
50000 , 2 , 1 , 1 , 2 , 2 , 28928 , 3 , 3 , 3 } ; static const real_T
rtcapiStoredFloats [ ] = { 1.0 , 0.0 } ; static const rtwCAPI_FixPtMap
rtFixPtMap [ ] = { { ( NULL ) , ( NULL ) , rtwCAPI_FIX_RESERVED , 0 , 0 , 0 }
, } ; static const rtwCAPI_SampleTimeMap rtSampleTimeMap [ ] = { { ( const
void * ) & rtcapiStoredFloats [ 0 ] , ( const void * ) & rtcapiStoredFloats [
1 ] , 0 , 0 } } ; static rtwCAPI_ModelMappingStaticInfo mmiStatic = { {
rtBlockSignals , 15 , ( NULL ) , 0 , ( NULL ) , 0 } , { rtBlockParameters , 2
, rtModelParameters , 11 } , { ( NULL ) , 0 } , { rtDataTypeMap ,
rtDimensionMap , rtFixPtMap , rtElementMap , rtSampleTimeMap ,
rtDimensionArray } , "float" , { 2516477063U , 2308891101U , 3822211718U ,
1507670244U } , ( NULL ) , 0 , 0 } ; const rtwCAPI_ModelMappingStaticInfo *
LIDAR_sim_GetCAPIStaticMap ( void ) { return & mmiStatic ; }
#ifndef HOST_CAPI_BUILD
void LIDAR_sim_InitializeDataMapInfo ( void ) { rtwCAPI_SetVersion ( ( *
rt_dataMapInfoPtr ) . mmi , 1 ) ; rtwCAPI_SetStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , & mmiStatic ) ; rtwCAPI_SetLoggingStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ; rtwCAPI_SetDataAddressMap ( ( *
rt_dataMapInfoPtr ) . mmi , rtDataAddrMap ) ; rtwCAPI_SetVarDimsAddressMap (
( * rt_dataMapInfoPtr ) . mmi , rtVarDimsAddrMap ) ;
rtwCAPI_SetInstanceLoggingInfo ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArray ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( ( * rt_dataMapInfoPtr ) . mmi , 0 ) ; }
#else
#ifdef __cplusplus
extern "C" {
#endif
void LIDAR_sim_host_InitializeDataMapInfo ( LIDAR_sim_host_DataMapInfo_T *
dataMap , const char * path ) { rtwCAPI_SetVersion ( dataMap -> mmi , 1 ) ;
rtwCAPI_SetStaticMap ( dataMap -> mmi , & mmiStatic ) ;
rtwCAPI_SetDataAddressMap ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetVarDimsAddressMap ( dataMap -> mmi , NULL ) ; rtwCAPI_SetPath (
dataMap -> mmi , path ) ; rtwCAPI_SetFullPath ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetChildMMIArray ( dataMap -> mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( dataMap -> mmi , 0 ) ; }
#ifdef __cplusplus
}
#endif
#endif
