#include "__cf_LIDAR_sim.h"
#ifndef RTW_HEADER_LIDAR_sim_private_h_
#define RTW_HEADER_LIDAR_sim_private_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "LIDAR_sim.h"
#if !defined(rt_VALIDATE_MEMORY)
#define rt_VALIDATE_MEMORY(S, ptr)   if(!(ptr)) {\
  ssSetErrorStatus(rtS, RT_MEMORY_ALLOCATION_ERROR);\
  }
#endif
#if !defined(rt_FREE)
#if !defined(_WIN32)
#define rt_FREE(ptr)   if((ptr) != (NULL)) {\
  free((ptr));\
  (ptr) = (NULL);\
  }
#else
#define rt_FREE(ptr)   if((ptr) != (NULL)) {\
  free((void *)(ptr));\
  (ptr) = (NULL);\
  }
#endif
#endif
extern int32_T div_s32 ( int32_T numerator , int32_T denominator ) ; extern
void l25gwhqalr ( const real_T fefdjmqfi5 [ 100000 ] , real_T ff2y1prgor ,
real_T oh2rcykxs2 , real_T itbacjpg3o , real_T a1f3itsmvp , j5iaypgo4w *
localB ) ;
#if defined(MULTITASKING)
#error Model (LIDAR_sim) was built in \SingleTasking solver mode, however the MULTITASKING define is \present. If you have multitasking (e.g. -DMT or -DMULTITASKING) \defined on the Code Generation page of Simulation parameter dialog, please \remove it and on the Solver page, select solver mode \MultiTasking. If the Simulation parameter dialog is configured \correctly, please verify that your template makefile is \configured correctly.
#endif
#endif
