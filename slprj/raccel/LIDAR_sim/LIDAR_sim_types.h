#include "__cf_LIDAR_sim.h"
#ifndef RTW_HEADER_LIDAR_sim_types_h_
#define RTW_HEADER_LIDAR_sim_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T
struct emxArray_real_T { real_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_evh4huxdqo
#define typedef_evh4huxdqo
typedef struct emxArray_real_T evh4huxdqo ;
#endif
#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T
struct emxArray_int32_T { int32_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_k3i5coxmly
#define typedef_k3i5coxmly
typedef struct emxArray_int32_T k3i5coxmly ;
#endif
#ifndef struct_emxArray_uint16_T
#define struct_emxArray_uint16_T
struct emxArray_uint16_T { uint16_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_iipmsdfjdk
#define typedef_iipmsdfjdk
typedef struct emxArray_uint16_T iipmsdfjdk ;
#endif
#ifndef struct_tag_skA4KFEZ4HPkJJBOYCrevdH
#define struct_tag_skA4KFEZ4HPkJJBOYCrevdH
struct tag_skA4KFEZ4HPkJJBOYCrevdH { uint32_T SafeEq ; uint32_T Absolute ;
uint32_T NaNBias ; uint32_T NaNWithFinite ; uint32_T FiniteWithNaN ; uint32_T
NaNWithNaN ; } ;
#endif
#ifndef typedef_fewpaslsoy
#define typedef_fewpaslsoy
typedef struct tag_skA4KFEZ4HPkJJBOYCrevdH fewpaslsoy ;
#endif
#ifndef struct_tag_spGKsvEVm7uA89hv31XX4LH
#define struct_tag_spGKsvEVm7uA89hv31XX4LH
struct tag_spGKsvEVm7uA89hv31XX4LH { uint32_T MissingPlacement ; uint32_T
ComparisonMethod ; } ;
#endif
#ifndef typedef_ll1p15n0hy
#define typedef_ll1p15n0hy
typedef struct tag_spGKsvEVm7uA89hv31XX4LH ll1p15n0hy ;
#endif
#ifndef struct_tag_sBaHy6MF1FZJsDHxMqvBaiH
#define struct_tag_sBaHy6MF1FZJsDHxMqvBaiH
struct tag_sBaHy6MF1FZJsDHxMqvBaiH { int32_T xstart ; int32_T xend ; int32_T
depth ; } ;
#endif
#ifndef typedef_mmmalklddr
#define typedef_mmmalklddr
typedef struct tag_sBaHy6MF1FZJsDHxMqvBaiH mmmalklddr ;
#endif
#ifndef struct_tag_sPmiUZY2qQ0dLsc3OKaugUE
#define struct_tag_sPmiUZY2qQ0dLsc3OKaugUE
struct tag_sPmiUZY2qQ0dLsc3OKaugUE { real_T DEFAULT ; real_T TOPLEFT ; real_T
BOTTOMRIGHT ; } ;
#endif
#ifndef typedef_hf1d3zm10e
#define typedef_hf1d3zm10e
typedef struct tag_sPmiUZY2qQ0dLsc3OKaugUE hf1d3zm10e ;
#endif
#ifndef struct_tag_swEUy0VManMfNlh9fC038aH
#define struct_tag_swEUy0VManMfNlh9fC038aH
struct tag_swEUy0VManMfNlh9fC038aH { real_T NONE ; real_T CONSTANT ; real_T
REPLICATE ; real_T SYMMETRIC ; } ;
#endif
#ifndef typedef_g3vajyf0cu
#define typedef_g3vajyf0cu
typedef struct tag_swEUy0VManMfNlh9fC038aH g3vajyf0cu ;
#endif
#ifndef struct_tag_sho5krkycRf4zNoOFwnsnyC
#define struct_tag_sho5krkycRf4zNoOFwnsnyC
struct tag_sho5krkycRf4zNoOFwnsnyC { boolean_T YES ; boolean_T NO ; } ;
#endif
#ifndef typedef_lpz4gow5pc
#define typedef_lpz4gow5pc
typedef struct tag_sho5krkycRf4zNoOFwnsnyC lpz4gow5pc ;
#endif
#ifndef struct_tag_szuUIsjSIN6GWJW5YzVNVmG
#define struct_tag_szuUIsjSIN6GWJW5YzVNVmG
struct tag_szuUIsjSIN6GWJW5YzVNVmG { uint32_T Theta ; uint32_T RhoResolution
; } ;
#endif
#ifndef typedef_hb3hsyquci
#define typedef_hb3hsyquci
typedef struct tag_szuUIsjSIN6GWJW5YzVNVmG hb3hsyquci ;
#endif
#ifndef struct_tag_s9s8BC13iTohZXRbLMSIDHE
#define struct_tag_s9s8BC13iTohZXRbLMSIDHE
struct tag_s9s8BC13iTohZXRbLMSIDHE { boolean_T CaseSensitivity ; boolean_T
StructExpand ; boolean_T PartialMatching ; } ;
#endif
#ifndef typedef_az3zctx22d
#define typedef_az3zctx22d
typedef struct tag_s9s8BC13iTohZXRbLMSIDHE az3zctx22d ;
#endif
#ifndef struct_tag_skZBV47kFp42DPFCMXy0uBE
#define struct_tag_skZBV47kFp42DPFCMXy0uBE
struct tag_skZBV47kFp42DPFCMXy0uBE { uint32_T Threshold ; uint32_T NHoodSize
; uint32_T Theta ; } ;
#endif
#ifndef typedef_f3krslylkc
#define typedef_f3krslylkc
typedef struct tag_skZBV47kFp42DPFCMXy0uBE f3krslylkc ;
#endif
#ifndef struct_tag_sQBCJjKKgS8ZYkH5STYFEHG
#define struct_tag_sQBCJjKKgS8ZYkH5STYFEHG
struct tag_sQBCJjKKgS8ZYkH5STYFEHG { uint32_T FillGap ; uint32_T MinLength ;
} ;
#endif
#ifndef typedef_jo3qxh3mun
#define typedef_jo3qxh3mun
typedef struct tag_sQBCJjKKgS8ZYkH5STYFEHG jo3qxh3mun ;
#endif
#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T
struct emxArray_boolean_T { boolean_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_csq05abd22
#define typedef_csq05abd22
typedef struct emxArray_boolean_T csq05abd22 ;
#endif
#ifndef struct_emxArray_real32_T
#define struct_emxArray_real32_T
struct emxArray_real32_T { real32_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_fs5fzuj10b
#define typedef_fs5fzuj10b
typedef struct emxArray_real32_T fs5fzuj10b ;
#endif
#ifndef struct_emxArray_uint32_T
#define struct_emxArray_uint32_T
struct emxArray_uint32_T { uint32_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_dsnlzixlef
#define typedef_dsnlzixlef
typedef struct emxArray_uint32_T dsnlzixlef ;
#endif
#ifndef struct_tag_sJCxfmxS8gBOONUZjbjUd9E
#define struct_tag_sJCxfmxS8gBOONUZjbjUd9E
struct tag_sJCxfmxS8gBOONUZjbjUd9E { boolean_T CaseSensitivity ; boolean_T
StructExpand ; char_T PartialMatching [ 6 ] ; boolean_T IgnoreNulls ; } ;
#endif
#ifndef typedef_atyyfhbmmf
#define typedef_atyyfhbmmf
typedef struct tag_sJCxfmxS8gBOONUZjbjUd9E atyyfhbmmf ;
#endif
#ifndef struct_tag_skoeQIuVNKJRHNtBIlOCZhD
#define struct_tag_skoeQIuVNKJRHNtBIlOCZhD
struct tag_skoeQIuVNKJRHNtBIlOCZhD { real_T point1 [ 2 ] ; real_T point2 [ 2
] ; real_T theta ; real_T rho ; } ;
#endif
#ifndef typedef_esxxud0bdd
#define typedef_esxxud0bdd
typedef struct tag_skoeQIuVNKJRHNtBIlOCZhD esxxud0bdd ;
#endif
#ifndef struct_emxArray_tag_skoeQIuVNKJRHNtBIl
#define struct_emxArray_tag_skoeQIuVNKJRHNtBIl
struct emxArray_tag_skoeQIuVNKJRHNtBIl { esxxud0bdd * data ; int32_T * size ;
int32_T allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_glcpjx2zog
#define typedef_glcpjx2zog
typedef struct emxArray_tag_skoeQIuVNKJRHNtBIl glcpjx2zog ;
#endif
#ifndef typedef_kmkfgbarzp
#define typedef_kmkfgbarzp
typedef struct { csq05abd22 * d ; k3i5coxmly * colidx ; k3i5coxmly * rowidx ;
int32_T m ; int32_T n ; } kmkfgbarzp ;
#endif
#ifndef struct_s1NkunOY9kgapStCcmh6fID_tag
#define struct_s1NkunOY9kgapStCcmh6fID_tag
struct s1NkunOY9kgapStCcmh6fID_tag { k3i5coxmly * f1 ; } ;
#endif
#ifndef typedef_koooartffq
#define typedef_koooartffq
typedef struct s1NkunOY9kgapStCcmh6fID_tag koooartffq ;
#endif
typedef struct P_ P ;
#endif
