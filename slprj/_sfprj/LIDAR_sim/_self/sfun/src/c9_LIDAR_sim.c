/* Include files */

#include "LIDAR_sim_sfun.h"
#include "c9_LIDAR_sim.h"
#include <math.h>
#include <string.h>
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LIDAR_sim_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c9_debug_family_names[20] = { "rotAngle",
  "obstaclesInRegionOfInterestTemp", "pictureMatrix", "x", "y", "BW", "H",
  "theta", "rho", "P", "lines", "orientationsOfLines", "k", "nargin", "nargout",
  "obstaclesInRegionOfInterest", "minRotation", "maxRotation", "rotationMatrix",
  "invRotationMatrix" };

static emlrtRTEInfo c9_emlrtRTEI = { 7,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_b_emlrtRTEI = { 9,/* lineNo */
  39,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_c_emlrtRTEI = { 14,/* lineNo */
  9,                                   /* colNo */
  "isnan",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\isnan.m"/* pName */
};

static emlrtRTEInfo c9_d_emlrtRTEI = { 9,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_e_emlrtRTEI = { 10,/* lineNo */
  39,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_f_emlrtRTEI = { 10,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_g_emlrtRTEI = { 19,/* lineNo */
  66,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_h_emlrtRTEI = { 13,/* lineNo */
  5,                                   /* colNo */
  "min",                               /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\min.m"/* pName */
};

static emlrtRTEInfo c9_i_emlrtRTEI = { 19,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_j_emlrtRTEI = { 20,/* lineNo */
  24,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_k_emlrtRTEI = { 1,/* lineNo */
  29,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_l_emlrtRTEI = { 20,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_m_emlrtRTEI = { 24,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_n_emlrtRTEI = { 21,/* lineNo */
  3,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_o_emlrtRTEI = { 27,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_p_emlrtRTEI = { 52,/* lineNo */
  20,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c9_q_emlrtRTEI = { 52,/* lineNo */
  9,                                   /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c9_r_emlrtRTEI = { 21,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_s_emlrtRTEI = { 86,/* lineNo */
  15,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_t_emlrtRTEI = { 22,/* lineNo */
  3,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_u_emlrtRTEI = { 1,/* lineNo */
  34,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c9_v_emlrtRTEI = { 58,/* lineNo */
  32,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c9_w_emlrtRTEI = { 22,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_x_emlrtRTEI = { 23,/* lineNo */
  16,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_y_emlrtRTEI = { 286,/* lineNo */
  35,                                  /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo c9_ab_emlrtRTEI = { 306,/* lineNo */
  5,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo c9_bb_emlrtRTEI = { 23,/* lineNo */
  18,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_cb_emlrtRTEI = { 85,/* lineNo */
  40,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c9_db_emlrtRTEI = { 23,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_eb_emlrtRTEI = { 28,/* lineNo */
  11,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_fb_emlrtRTEI = { 28,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_gb_emlrtRTEI = { 80,/* lineNo */
  5,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo c9_hb_emlrtRTEI = { 36,/* lineNo */
  23,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_ib_emlrtRTEI = { 84,/* lineNo */
  5,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo c9_jb_emlrtRTEI = { 46,/* lineNo */
  1,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_kb_emlrtRTEI = { 46,/* lineNo */
  5,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_lb_emlrtRTEI = { 98,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo c9_mb_emlrtRTEI = { 36,/* lineNo */
  2,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_nb_emlrtRTEI = { 36,/* lineNo */
  4,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_ob_emlrtRTEI = { 36,/* lineNo */
  10,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_pb_emlrtRTEI = { 38,/* lineNo */
  47,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_qb_emlrtRTEI = { 844,/* lineNo */
  31,                                  /* colNo */
  "unaryMinOrMax",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pName */
};

static emlrtRTEInfo c9_rb_emlrtRTEI = { 38,/* lineNo */
  17,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_sb_emlrtRTEI = { 38,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_tb_emlrtRTEI = { 39,/* lineNo */
  20,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_ub_emlrtRTEI = { 39,/* lineNo */
  23,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_vb_emlrtRTEI = { 39,/* lineNo */
  29,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_wb_emlrtRTEI = { 39,/* lineNo */
  33,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_xb_emlrtRTEI = { 39,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_yb_emlrtRTEI = { 43,/* lineNo */
  1,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_ac_emlrtRTEI = { 48,/* lineNo */
  15,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_bc_emlrtRTEI = { 27,/* lineNo */
  30,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_cc_emlrtRTEI = { 52,/* lineNo */
  1,                                   /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c9_dc_emlrtRTEI = { 36,/* lineNo */
  34,                                  /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_ec_emlrtRTEI = { 44,/* lineNo */
  6,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_fc_emlrtRTEI = { 44,/* lineNo */
  13,                                  /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_gc_emlrtRTEI = { 165,/* lineNo */
  1,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_hc_emlrtRTEI = { 166,/* lineNo */
  1,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_ic_emlrtRTEI = { 23,/* lineNo */
  9,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_jc_emlrtRTEI = { 39,/* lineNo */
  9,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_kc_emlrtRTEI = { 38,/* lineNo */
  6,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_lc_emlrtRTEI = { 28,/* lineNo */
  6,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_mc_emlrtRTEI = { 23,/* lineNo */
  4,                                   /* colNo */
  "FindPossibleHerringBoneParkings/MATLAB Function2",/* fName */
  "#LIDAR_sim:68"                      /* pName */
};

static emlrtRTEInfo c9_nc_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c9_oc_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "round",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elfun\\round.m"/* pName */
};

static emlrtRTEInfo c9_pc_emlrtRTEI = { 33,/* lineNo */
  14,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_qc_emlrtRTEI = { 120,/* lineNo */
  43,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_rc_emlrtRTEI = { 121,/* lineNo */
  43,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_sc_emlrtRTEI = { 13,/* lineNo */
  5,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\sparfun\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_tc_emlrtRTEI = { 13,/* lineNo */
  1,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\sparfun\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_uc_emlrtRTEI = { 120,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_vc_emlrtRTEI = { 121,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_wc_emlrtRTEI = { 126,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_xc_emlrtRTEI = { 1647,/* lineNo */
  17,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_yc_emlrtRTEI = { 1679,/* lineNo */
  22,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_ad_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "insertionsort",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\insertionsort.m"/* pName */
};

static emlrtRTEInfo c9_bd_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c9_cd_emlrtRTEI = { 48,/* lineNo */
  21,                                  /* colNo */
  "stack",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\stack.m"/* pName */
};

static emlrtRTEInfo c9_dd_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c9_ed_emlrtRTEI = { 40,/* lineNo */
  14,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c9_fd_emlrtRTEI = { 1,/* lineNo */
  18,                                  /* colNo */
  "sortpartition",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\private\\sortpartition.m"/* pName */
};

static emlrtRTEInfo c9_gd_emlrtRTEI = { 1689,/* lineNo */
  14,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_hd_emlrtRTEI = { 1,/* lineNo */
  17,                                  /* colNo */
  "fillIn",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\fillIn.m"/* pName */
};

static emlrtRTEInfo c9_id_emlrtRTEI = { 273,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_jd_emlrtRTEI = { 69,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_kd_emlrtRTEI = { 75,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_ld_emlrtRTEI = { 673,/* lineNo */
  1,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_md_emlrtRTEI = { 15,/* lineNo */
  9,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_nd_emlrtRTEI = { 917,/* lineNo */
  11,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_od_emlrtRTEI = { 110,/* lineNo */
  17,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_pd_emlrtRTEI = { 820,/* lineNo */
  21,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_qd_emlrtRTEI = { 102,/* lineNo */
  6,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_rd_emlrtRTEI = { 102,/* lineNo */
  10,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_sd_emlrtRTEI = { 108,/* lineNo */
  18,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_td_emlrtRTEI = { 110,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_ud_emlrtRTEI = { 153,/* lineNo */
  13,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_vd_emlrtRTEI = { 42,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_wd_emlrtRTEI = { 690,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_xd_emlrtRTEI = { 696,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_yd_emlrtRTEI = { 118,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_ae_emlrtRTEI = { 697,/* lineNo */
  37,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_be_emlrtRTEI = { 50,/* lineNo */
  5,                                   /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo c9_ce_emlrtRTEI = { 697,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_de_emlrtRTEI = { 120,/* lineNo */
  30,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_ee_emlrtRTEI = { 723,/* lineNo */
  32,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_fe_emlrtRTEI = { 38,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_ge_emlrtRTEI = { 147,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_he_emlrtRTEI = { 39,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_ie_emlrtRTEI = { 120,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_je_emlrtRTEI = { 250,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_ke_emlrtRTEI = { 250,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_le_emlrtRTEI = { 251,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_me_emlrtRTEI = { 251,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_ne_emlrtRTEI = { 252,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_oe_emlrtRTEI = { 59,/* lineNo */
  9,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_pe_emlrtRTEI = { 1,/* lineNo */
  22,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c9_qe_emlrtRTEI = { 88,/* lineNo */
  17,                                  /* colNo */
  "CannyThresholdingTbbBuildable",     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\+images\\+internal\\+coder\\+buildable\\CannyThresholdingTbbBuildable.m"/* pName */
};

static emlrtRTEInfo c9_re_emlrtRTEI = { 80,/* lineNo */
  5,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_se_emlrtRTEI = { 836,/* lineNo */
  5,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_te_emlrtRTEI = { 30,/* lineNo */
  1,                                   /* colNo */
  "repmat",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\repmat.m"/* pName */
};

static emlrtRTEInfo c9_ue_emlrtRTEI = { 25,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo c9_ve_emlrtRTEI = { 769,/* lineNo */
  5,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_we_emlrtRTEI = { 845,/* lineNo */
  9,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_xe_emlrtRTEI = { 846,/* lineNo */
  14,                                  /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_ye_emlrtRTEI = { 846,/* lineNo */
  36,                                  /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_af_emlrtRTEI = { 769,/* lineNo */
  9,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_bf_emlrtRTEI = { 839,/* lineNo */
  9,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_cf_emlrtRTEI = { 762,/* lineNo */
  14,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c9_df_emlrtRTEI = { 845,/* lineNo */
  30,                                  /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c9_ef_emlrtRTEI = { 19,/* lineNo */
  24,                                  /* colNo */
  "scalexpAllocNoCheck",               /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAllocNoCheck.m"/* pName */
};

static emlrtRTEInfo c9_ff_emlrtRTEI = { 16,/* lineNo */
  9,                                   /* colNo */
  "scalexpAlloc",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAlloc.m"/* pName */
};

static emlrtRTEInfo c9_gf_emlrtRTEI = { 45,/* lineNo */
  6,                                   /* colNo */
  "applyBinaryScalarFunction",         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\applyBinaryScalarFunction.m"/* pName */
};

static emlrtRTEInfo c9_hf_emlrtRTEI = { 18,/* lineNo */
  32,                                  /* colNo */
  "scalexpAlloc",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAlloc.m"/* pName */
};

static emlrtRTEInfo c9_if_emlrtRTEI = { 18,/* lineNo */
  34,                                  /* colNo */
  "scalexpAlloc",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAlloc.m"/* pName */
};

static emlrtRTEInfo c9_jf_emlrtRTEI = { 12,/* lineNo */
  1,                                   /* colNo */
  "hypot",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elfun\\hypot.m"/* pName */
};

static emlrtRTEInfo c9_kf_emlrtRTEI = { 35,/* lineNo */
  9,                                   /* colNo */
  "scalexpAllocNoCheck",               /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAllocNoCheck.m"/* pName */
};

static emlrtRTEInfo c9_lf_emlrtRTEI = { 27,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_mf_emlrtRTEI = { 28,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_nf_emlrtRTEI = { 57,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_of_emlrtRTEI = { 50,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_pf_emlrtRTEI = { 52,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_qf_emlrtRTEI = { 60,/* lineNo */
  16,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_rf_emlrtRTEI = { 53,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_sf_emlrtRTEI = { 60,/* lineNo */
  26,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_tf_emlrtRTEI = { 54,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_uf_emlrtRTEI = { 60,/* lineNo */
  15,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_vf_emlrtRTEI = { 61,/* lineNo */
  16,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_wf_emlrtRTEI = { 61,/* lineNo */
  26,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_xf_emlrtRTEI = { 60,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_yf_emlrtRTEI = { 397,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_ag_emlrtRTEI = { 65,/* lineNo */
  7,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_bg_emlrtRTEI = { 66,/* lineNo */
  7,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_cg_emlrtRTEI = { 41,/* lineNo */
  30,                                  /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c9_dg_emlrtRTEI = { 41,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_eg_emlrtRTEI = { 46,/* lineNo */
  1,                                   /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c9_fg_emlrtRTEI = { 48,/* lineNo */
  23,                                  /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c9_gg_emlrtRTEI = { 48,/* lineNo */
  17,                                  /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c9_hg_emlrtRTEI = { 48,/* lineNo */
  5,                                   /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c9_ig_emlrtRTEI = { 70,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_jg_emlrtRTEI = { 71,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_kg_emlrtRTEI = { 72,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_lg_emlrtRTEI = { 85,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_mg_emlrtRTEI = { 82,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_ng_emlrtRTEI = { 11,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_og_emlrtRTEI = { 16,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_pg_emlrtRTEI = { 17,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_qg_emlrtRTEI = { 1,/* lineNo */
  24,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c9_rg_emlrtRTEI = { 33,/* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c9_sg_emlrtRTEI = { 1,/* lineNo */
  27,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_tg_emlrtRTEI = { 50,/* lineNo */
  29,                                  /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_ug_emlrtRTEI = { 210,/* lineNo */
  5,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c9_vg_emlrtRTEI = { 74,/* lineNo */
  1,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c9_wg_emlrtRTEI = { 1,/* lineNo */
  18,                                  /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c9_xg_emlrtRTEI = { 173,/* lineNo */
  5,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c9_yg_emlrtRTEI = { 82,/* lineNo */
  5,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c9_ah_emlrtRTEI = { 9,/* lineNo */
  59,                                  /* colNo */
  "validateinteger",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\+valattr\\validateinteger.m"/* pName */
};

static emlrtRTEInfo c9_bh_emlrtRTEI = { 10,/* lineNo */
  43,                                  /* colNo */
  "validatefinite",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\+valattr\\validatefinite.m"/* pName */
};

static emlrtRTEInfo c9_ch_emlrtRTEI = { 74,/* lineNo */
  15,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_dh_emlrtRTEI = { 159,/* lineNo */
  60,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_eh_emlrtRTEI = { 76,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_fh_emlrtRTEI = { 213,/* lineNo */
  52,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_gh_emlrtRTEI = { 108,/* lineNo */
  5,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_hh_emlrtRTEI = { 137,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_ih_emlrtRTEI = { 240,/* lineNo */
  51,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_jh_emlrtRTEI = { 115,/* lineNo */
  15,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_kh_emlrtRTEI = { 120,/* lineNo */
  9,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_lh_emlrtRTEI = { 286,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_mh_emlrtRTEI = { 121,/* lineNo */
  9,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_nh_emlrtRTEI = { 128,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_oh_emlrtRTEI = { 128,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_ph_emlrtRTEI = { 129,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_qh_emlrtRTEI = { 129,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_rh_emlrtRTEI = { 130,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_sh_emlrtRTEI = { 130,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_th_emlrtRTEI = { 131,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_uh_emlrtRTEI = { 131,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_vh_emlrtRTEI = { 84,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_wh_emlrtRTEI = { 87,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_xh_emlrtRTEI = { 90,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_yh_emlrtRTEI = { 93,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_ai_emlrtRTEI = { 115,/* lineNo */
  5,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_bi_emlrtRTEI = { 1,/* lineNo */
  18,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_ci_emlrtRTEI = { 108,/* lineNo */
  18,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_di_emlrtRTEI = { 213,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_ei_emlrtRTEI = { 203,/* lineNo */
  23,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_fi_emlrtRTEI = { 302,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c9_gi_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_hi_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo c9_ii_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_ji_emlrtRTEI = { 482,/* lineNo */
  32,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_ki_emlrtRTEI = { 520,/* lineNo */
  32,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_li_emlrtRTEI = { 297,/* lineNo */
  5,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c9_mi_emlrtRTEI = { 25,/* lineNo */
  13,                                  /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c9_ni_emlrtRTEI = { 38,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_oi_emlrtRTEI = { 19,/* lineNo */
  14,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c9_pi_emlrtRTEI = { 14,/* lineNo */
  25,                                  /* colNo */
  "anonymous_function",                /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\anonymous_function.m"/* pName */
};

static emlrtRTEInfo c9_qi_emlrtRTEI = { 1683,/* lineNo */
  23,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_ri_emlrtRTEI = { 1684,/* lineNo */
  23,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_si_emlrtRTEI = { 1682,/* lineNo */
  36,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_ti_emlrtRTEI = { 18,/* lineNo */
  13,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c9_ui_emlrtRTEI = { 36,/* lineNo */
  14,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c9_vi_emlrtRTEI = { 37,/* lineNo */
  50,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c9_wi_emlrtRTEI = { 34,/* lineNo */
  13,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c9_xi_emlrtRTEI = { 34,/* lineNo */
  52,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c9_yi_emlrtRTEI = { 47,/* lineNo */
  56,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c9_aj_emlrtRTEI = { 51,/* lineNo */
  45,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c9_bj_emlrtRTEI = { 49,/* lineNo */
  51,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c9_cj_emlrtRTEI = { 35,/* lineNo */
  35,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c9_dj_emlrtRTEI = { 26,/* lineNo */
  58,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c9_ej_emlrtRTEI = { 1691,/* lineNo */
  5,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c9_fj_emlrtRTEI = { 166,/* lineNo */
  9,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c9_gj_emlrtRTEI = { 16,/* lineNo */
  9,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c9_hj_emlrtRTEI = { 164,/* lineNo */
  9,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c9_ij_emlrtRTEI = { 243,/* lineNo */
  18,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_jj_emlrtRTEI = { 238,/* lineNo */
  20,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_kj_emlrtRTEI = { 243,/* lineNo */
  36,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_lj_emlrtRTEI = { 94,/* lineNo */
  17,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_mj_emlrtRTEI = { 243,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_nj_emlrtRTEI = { 199,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_oj_emlrtRTEI = { 201,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_pj_emlrtRTEI = { 202,/* lineNo */
  12,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_qj_emlrtRTEI = { 202,/* lineNo */
  25,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_rj_emlrtRTEI = { 204,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_sj_emlrtRTEI = { 30,/* lineNo */
  9,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_tj_emlrtRTEI = { 131,/* lineNo */
  9,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_uj_emlrtRTEI = { 135,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c9_vj_emlrtRTEI = { 69,/* lineNo */
  2,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_wj_emlrtRTEI = { 139,/* lineNo */
  16,                                  /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_xj_emlrtRTEI = { 140,/* lineNo */
  16,                                  /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_yj_emlrtRTEI = { 81,/* lineNo */
  7,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c9_ak_emlrtRTEI = { 28,/* lineNo */
  5,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c9_bk_emlrtRTEI = { 56,/* lineNo */
  1,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo c9_ck_emlrtRTEI = { 61,/* lineNo */
  5,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_dk_emlrtRTEI = { 385,/* lineNo */
  9,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_ek_emlrtRTEI = { 308,/* lineNo */
  1,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_fk_emlrtRTEI = { 386,/* lineNo */
  1,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_gk_emlrtRTEI = { 387,/* lineNo */
  9,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_hk_emlrtRTEI = { 388,/* lineNo */
  1,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_ik_emlrtRTEI = { 308,/* lineNo */
  14,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c9_jk_emlrtRTEI = { 308,/* lineNo */
  20,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static const char_T c9_cv0[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
  'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

static const char_T c9_cv1[39] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'm', 'i', 'n', '_', 'o', 'r', '_',
  'm', 'a', 'x', '_', 'v', 'a', 'r', 'D', 'i', 'm', 'Z', 'e', 'r', 'o' };

static const char_T c9_cv2[30] = { 'C', 'o', 'd', 'e', 'r', ':', 'b', 'u', 'i',
  'l', 't', 'i', 'n', 's', ':', 'A', 's', 's', 'e', 'r', 't', 'i', 'o', 'n', 'F',
  'a', 'i', 'l', 'e', 'd' };

static const char_T c9_cv3[21] = { 'C', 'o', 'd', 'e', 'r', ':', 'M', 'A', 'T',
  'L', 'A', 'B', ':', 'p', 'm', 'a', 'x', 's', 'i', 'z', 'e' };

static const char_T c9_cv4[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'f', 'i', 'n', 'd', '_', 'i', 'n', 'c', 'o', 'm', 'p',
  'a', 't', 'i', 'b', 'l', 'e', 'S', 'h', 'a', 'p', 'e' };

static const char_T c9_cv5[30] = { 'C', 'o', 'd', 'e', 'r', ':', 'F', 'E', ':',
  'P', 'o', 't', 'e', 'n', 't', 'i', 'a', 'l', 'V', 'e', 'c', 't', 'o', 'r', 'V',
  'e', 'c', 't', 'o', 'r' };

static const char_T c9_cv6[46] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N',
  'o', 'n', 'N', 'a', 'N' };

static const char_T c9_cv7[47] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'I',
  'n', 't', 'e', 'g', 'e', 'r' };

static const char_T c9_cv8[15] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'p', 'm',
  'a', 'x', 's', 'i', 'z', 'e' };

static const char_T c9_cv9[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o',
  'u', 'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
  'd', 'F', 'i', 'n', 'i', 't', 'e' };

static const char_T c9_cv10[46] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'F',
  'i', 'n', 'i', 't', 'e' };

static const char_T c9_cv11[33] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o',
  'u', 'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
  'd', 'I', 'n', 't', 'e', 'g', 'e', 'r' };

static const char_T c9_cv12[48] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'P',
  'o', 's', 'i', 't', 'i', 'v', 'e' };

static const char_T c9_cv13[48] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N',
  'o', 'n', 'e', 'm', 'p', 't', 'y' };

static const char_T c9_cv14[19] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm',
  'b', 'e', 'r', ' ', '1', ',', ' ', 'B', 'W', ',' };

static const char_T c9_cv15[25] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'u',
  'b', 's', 'd', 'e', 'l', 'd', 'i', 'm', 'm', 'i', 's', 'm', 'a', 't', 'c', 'h'
};

/* Function Declarations */
static void initialize_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void initialize_params_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance);
static void enable_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void disable_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_update_debugger_state_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance);
static void set_sim_state_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance, const mxArray *c9_st);
static void finalize_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void sf_gateway_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void mdl_start_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_chartstep_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance);
static void initSimStructsc9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c9_machineNumber, uint32_T
  c9_chartNumber, uint32_T c9_instanceNumber);
static const mxArray *c9_sf_marshallOut(void *chartInstanceVoid, void *c9_inData);
static real_T c9_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_b_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData);
static void c9_b_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y);
static void c9_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData);
static const mxArray *c9_c_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData);
static void c9_c_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y);
static void c9_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData);
static const mxArray *c9_d_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_boolean_T *c9_inData);
static void c9_d_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_boolean_T *c9_y);
static void c9_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_boolean_T *c9_outData);
static const mxArray *c9_e_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData);
static void c9_e_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y);
static void c9_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData);
static const mxArray *c9_f_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData);
static void c9_f_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y);
static void c9_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData);
static const mxArray *c9_g_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData);
static void c9_g_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y);
static void c9_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData);
static const mxArray *c9_h_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_inData);
static void c9_h_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_y);
static void c9_i_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId, real_T c9_y[2]);
static void c9_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_outData);
static const mxArray *c9_i_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData);
static void c9_j_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y);
static void c9_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData);
static const mxArray *c9_j_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static const mxArray *c9_k_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static void c9_k_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_rotationMatrix, const char_T *c9_identifier, real_T c9_y[4]);
static void c9_l_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId, real_T c9_y[4]);
static void c9_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static void c9_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, boolean_T c9_idx_data[], int32_T c9_idx_size[1],
  c9_emxArray_real_T *c9_b_x);
static void c9_check_forloop_overflow_error(SFc9_LIDAR_simInstanceStruct
  *chartInstance, boolean_T c9_overflow);
static void c9_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
                     c9_emxArray_real_T *c9_x, c9_emxArray_real_T *c9_b_x);
static boolean_T c9_sortLE(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_v, int32_T c9_dir_data[], int32_T c9_dir_size[2],
  int32_T c9_idx1, int32_T c9_idx2);
static void c9_apply_row_permutation(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_y, c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T
  *c9_b_y);
static boolean_T c9_rows_differ(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_b, int32_T c9_k0, int32_T c9_k);
static void c9_b_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_real_T *c9_b_x);
static void c9_sparse(SFc9_LIDAR_simInstanceStruct *chartInstance,
                      c9_emxArray_real_T *c9_varargin_1, c9_emxArray_real_T
                      *c9_varargin_2, c9_coder_internal_sparse *c9_y);
static void c9_assertValidIndexArg(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_s, c9_emxArray_int32_T *c9_sint);
static void c9_locSortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_a, c9_emxArray_int32_T
  *c9_b, c9_emxArray_int32_T *c9_b_idx, c9_emxArray_int32_T *c9_b_a,
  c9_emxArray_int32_T *c9_b_b);
static void c9_insertionsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x);
static boolean_T c9___anon_fcn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_a, c9_emxArray_int32_T *c9_b, int32_T c9_i, int32_T
  c9_j);
static int32_T c9_nextpow2(SFc9_LIDAR_simInstanceStruct *chartInstance, int32_T
  c9_n);
static void c9_introsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x);
static void c9_stack_stack(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_sBaHy6MF1FZJsDHxMqvBaiH c9_eg, int32_T c9_n, c9_coder_internal_stack
  *c9_this);
static void c9_heapsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x);
static void c9_heapify(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_idx, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x);
static void c9_sortpartition(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, int32_T *c9_p,
  c9_emxArray_int32_T *c9_b_x);
static void c9_permuteVector(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_y, c9_emxArray_int32_T
  *c9_b_y);
static void c9_sparse_fillIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_coder_internal_sparse c9_this, c9_coder_internal_sparse *c9_b_this);
static void c9_sparse_full(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_coder_internal_sparse c9_this, c9_emxArray_boolean_T *c9_y);
static void c9_edge(SFc9_LIDAR_simInstanceStruct *chartInstance,
                    c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_boolean_T *
                    c9_varargout_1);
static void c9_error(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_padImage(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T *c9_a_tmp, real_T c9_pad[2], c9_emxArray_real32_T *c9_a);
static boolean_T c9_all(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T
  c9_a[2]);
static void c9_hypot(SFc9_LIDAR_simInstanceStruct *chartInstance,
                     c9_emxArray_real32_T *c9_x, c9_emxArray_real32_T *c9_y,
                     c9_emxArray_real32_T *c9_r);
static boolean_T c9_dimagree(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T *c9_z, c9_emxArray_real32_T *c9_varargin_1,
  c9_emxArray_real32_T *c9_varargin_2);
static void c9_warning(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_b_warning(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_bwselect(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_2,
  c9_emxArray_real_T *c9_varargin_3, c9_emxArray_boolean_T *c9_varargout_1);
static void c9_c_warning(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_b_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T
  *c9_b_x);
static boolean_T c9_allinrange(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, real_T c9_lo, int32_T c9_hi);
static void c9_imfill(SFc9_LIDAR_simInstanceStruct *chartInstance,
                      c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T
                      *c9_varargin_2, c9_emxArray_boolean_T *c9_I2);
static void c9_d_warning(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void c9_parseInputs(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_3,
  c9_emxArray_boolean_T *c9_BW, c9_emxArray_real_T *c9_theta, c9_emxArray_real_T
  *c9_rho);
static boolean_T c9_b_all(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_a);
static int32_T c9_findFirst(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x);
static void c9_houghpeaks(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_varargin_1, real_T c9_varargin_4, c9_emxArray_real_T
  *c9_peaks);
static void c9_validateattributes(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_a);
static boolean_T c9_c_all(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_a);
static void c9_diff(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T c9_x[179],
                    real_T c9_y[178]);
static void c9_houghlines(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_2,
  c9_emxArray_real_T *c9_varargin_3, c9_emxArray_real_T *c9_varargin_4,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_lines);
static void c9_sortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_y, real_T c9_varargin_1[2], c9_emxArray_int32_T
  *c9_b_y);
static boolean_T c9_b_sortLE(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_v, int32_T c9_dir[2], int32_T c9_idx1, int32_T c9_idx2);
static void c9_sort(SFc9_LIDAR_simInstanceStruct *chartInstance,
                    c9_emxArray_real_T *c9_x, c9_emxArray_real_T *c9_b_x);
static void c9_sortIdx(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T
  *c9_b_x);
static void c9_merge_block(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x, int32_T c9_offset,
  int32_T c9_n, int32_T c9_preSortLevel, c9_emxArray_int32_T *c9_iwork,
  c9_emxArray_real_T *c9_xwork, c9_emxArray_int32_T *c9_b_idx,
  c9_emxArray_real_T *c9_b_x, c9_emxArray_int32_T *c9_b_iwork,
  c9_emxArray_real_T *c9_b_xwork);
static void c9_merge(SFc9_LIDAR_simInstanceStruct *chartInstance,
                     c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x,
                     int32_T c9_offset, int32_T c9_np, int32_T c9_nq,
                     c9_emxArray_int32_T *c9_iwork, c9_emxArray_real_T *c9_xwork,
                     c9_emxArray_int32_T *c9_b_idx, c9_emxArray_real_T *c9_b_x,
                     c9_emxArray_int32_T *c9_b_iwork, c9_emxArray_real_T
                     *c9_b_xwork);
static real_T c9_cosd(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T c9_x);
static real_T c9_sind(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T c9_x);
static const mxArray *c9_emlrt_marshallOut(SFc9_LIDAR_simInstanceStruct
  *chartInstance, const char_T c9_b_u[30]);
static const mxArray *c9_b_emlrt_marshallOut(SFc9_LIDAR_simInstanceStruct
  *chartInstance, const char_T c9_b_u[14]);
static const mxArray *c9_l_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static int32_T c9_m_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_k_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static uint8_T c9_n_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_is_active_c9_LIDAR_sim, const char_T *c9_identifier);
static uint8_T c9_o_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_c_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, boolean_T c9_idx_data[], int32_T c9_idx_size[1]);
static void c9_c_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x);
static void c9_b_apply_row_permutation(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real_T *c9_y, c9_emxArray_int32_T *c9_idx);
static void c9_d_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x);
static void c9_b_locSortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_a, c9_emxArray_int32_T
  *c9_b);
static void c9_b_insertionsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp);
static void c9_b_introsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp);
static void c9_b_heapsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp);
static void c9_b_heapify(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_idx, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp);
static int32_T c9_b_sortpartition(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp);
static void c9_b_permuteVector(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_y);
static void c9_b_sparse_fillIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_coder_internal_sparse *c9_this);
static void c9_d_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx);
static void c9_b_imfill(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_2);
static void c9_b_parseInputs(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_3,
  c9_emxArray_real_T *c9_theta, c9_emxArray_real_T *c9_rho);
static void c9_b_sortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_y, real_T c9_varargin_1[2]);
static void c9_b_sort(SFc9_LIDAR_simInstanceStruct *chartInstance,
                      c9_emxArray_real_T *c9_x);
static void c9_b_sortIdx(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx);
static void c9_b_merge_block(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x, int32_T c9_offset,
  int32_T c9_n, int32_T c9_preSortLevel, c9_emxArray_int32_T *c9_iwork,
  c9_emxArray_real_T *c9_xwork);
static void c9_b_merge(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x, int32_T c9_offset,
  int32_T c9_np, int32_T c9_nq, c9_emxArray_int32_T *c9_iwork,
  c9_emxArray_real_T *c9_xwork);
static void c9_b_cosd(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T *c9_x);
static void c9_b_sind(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T *c9_x);
static void c9_emxEnsureCapacity_real_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_real_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_int32_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_int32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_boolean_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_boolean_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_skoeQIuVNKJRH(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_emxArray, int32_T
  c9_oldNumel, const emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_real_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_boolean_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_skoeQIuVNKJRHNtBIlOCZhD(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_skoeQIuVNKJRHNtBIlOCZh **c9_pEmxArray, int32_T
  c9_numDimensions, const emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_real_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_int32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInitStruct_coder_internal_sp(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_sparse *c9_pStruct, const emlrtRTEInfo
  *c9_srcLocation);
static void c9_emxInit_boolean_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxFree_real_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T **c9_pEmxArray);
static void c9_emxFree_boolean_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_pEmxArray);
static void c9_emxFree_skoeQIuVNKJRHNtBIlOCZhD(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_skoeQIuVNKJRHNtBIlOCZh **c9_pEmxArray);
static void c9_emxFree_int32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_pEmxArray);
static void c9_emxFreeStruct_coder_internal_sp(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_sparse *c9_pStruct);
static void c9_emxEnsureCapacity_boolean_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_boolean_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxCopyStruct_coder_internal_an(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_anonymous_function *c9_dst, const
  c9_coder_internal_anonymous_function *c9_src, const emlrtRTEInfo
  *c9_srcLocation);
static void c9_emxCopyMatrix_real_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const emlrtRTEInfo *c9_srcLocation);
static void c9_emxCopyMatrix_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 c9_dst[2], const c9_cell_wrap_1 c9_src[2],
  const emlrtRTEInfo *c9_srcLocation);
static void c9_emxCopyStruct_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 *c9_dst, const c9_cell_wrap_1 *c9_src, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxCopy_int32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_dst, c9_emxArray_int32_T * const *c9_src, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInitStruct_coder_internal_an(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_anonymous_function *c9_pStruct, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInitMatrix_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 c9_pMatrix[2], const emlrtRTEInfo
  *c9_srcLocation);
static void c9_emxInitStruct_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 *c9_pStruct, const emlrtRTEInfo *c9_srcLocation);
static void c9_emxFreeMatrix_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 c9_pMatrix[2]);
static void c9_emxFreeStruct_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 *c9_pStruct);
static void c9_emxFreeStruct_coder_internal_an(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_anonymous_function *c9_pStruct);
static void c9_emxCopyStruct_coder_internal_sp(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_sparse *c9_dst, const
  c9_coder_internal_sparse *c9_src, const emlrtRTEInfo *c9_srcLocation);
static void c9_emxCopy_boolean_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_dst, c9_emxArray_boolean_T * const *c9_src, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_real32_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_real32_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_real32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_real32_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxFree_real32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T **c9_pEmxArray);
static void c9_emxEnsureCapacity_int32_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_int32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxEnsureCapacity_uint32_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_uint32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_int32_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxInit_uint32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_uint32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation);
static void c9_emxFree_uint32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_uint32_T **c9_pEmxArray);
static int32_T c9_div_nzp_s32(SFc9_LIDAR_simInstanceStruct *chartInstance,
  int32_T c9_numerator, int32_T c9_denominator, uint32_T c9_ssid_src_loc,
  int32_T c9_offset_src_loc, int32_T c9_length_src_loc);
static int32_T c9__s32_s64_(SFc9_LIDAR_simInstanceStruct *chartInstance, int64_T
  c9_b, uint32_T c9_ssid_src_loc, int32_T c9_offset_src_loc, int32_T
  c9_length_src_loc);
static int32_T c9__s32_d_(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T
  c9_b, uint32_T c9_ssid_src_loc, int32_T c9_offset_src_loc, int32_T
  c9_length_src_loc);
static void init_dsm_address_info(SFc9_LIDAR_simInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc9_LIDAR_simInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  emlrtLicenseCheckR2012b(chartInstance->c9_fEmlrtCtx, "Image_Toolbox", 2);
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc9_LIDAR_sim(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  sim_mode_is_external(chartInstance->S);
  chartInstance->c9_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c9_is_active_c9_LIDAR_sim = 0U;
  setLegacyDebuggerFlagForRuntime(chartInstance->S, true);
}

static void initialize_params_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c9_update_debugger_state_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance)
{
  const mxArray *c9_st;
  const mxArray *c9_y = NULL;
  const mxArray *c9_b_y = NULL;
  const mxArray *c9_c_y = NULL;
  uint8_T c9_hoistedGlobal;
  const mxArray *c9_d_y = NULL;
  c9_st = NULL;
  c9_st = NULL;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createcellmatrix(3, 1), false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", *chartInstance->c9_invRotationMatrix,
    0, 0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_setcell(c9_y, 0, c9_b_y);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", *chartInstance->c9_rotationMatrix, 0,
    0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_setcell(c9_y, 1, c9_c_y);
  c9_hoistedGlobal = chartInstance->c9_is_active_c9_LIDAR_sim;
  c9_d_y = NULL;
  sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_hoistedGlobal, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c9_y, 2, c9_d_y);
  sf_mex_assign(&c9_st, c9_y, false);
  return c9_st;
}

static void set_sim_state_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance, const mxArray *c9_st)
{
  const mxArray *c9_b_u;
  real_T c9_dv0[4];
  int32_T c9_i0;
  real_T c9_dv1[4];
  int32_T c9_i1;
  chartInstance->c9_doneDoubleBufferReInit = true;
  c9_b_u = sf_mex_dup(c9_st);
  c9_k_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_b_u, 0)),
                        "invRotationMatrix", c9_dv0);
  for (c9_i0 = 0; c9_i0 < 4; c9_i0++) {
    (*chartInstance->c9_invRotationMatrix)[c9_i0] = c9_dv0[c9_i0];
  }

  c9_k_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c9_b_u, 1)),
                        "rotationMatrix", c9_dv1);
  for (c9_i1 = 0; c9_i1 < 4; c9_i1++) {
    (*chartInstance->c9_rotationMatrix)[c9_i1] = c9_dv1[c9_i1];
  }

  chartInstance->c9_is_active_c9_LIDAR_sim = c9_n_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c9_b_u, 2)), "is_active_c9_LIDAR_sim");
  sf_mex_destroy(&c9_b_u);
  c9_update_debugger_state_c9_LIDAR_sim(chartInstance);
  sf_mex_destroy(&c9_st);
}

static void finalize_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  sfListenerLightTerminate(chartInstance->c9_RuntimeVar);
}

static void sf_gateway_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  int32_T c9_i2;
  int32_T c9_i3;
  int32_T c9_i4;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 8, chartInstance->c9_sfEvent);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_maxRotation, 2U);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_minRotation, 1U);
  for (c9_i2 = 0; c9_i2 < 100000; c9_i2++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c9_b_obstaclesInRegionOfInterest)
                          [c9_i2], 0U);
  }

  chartInstance->c9_sfEvent = CALL_EVENT;
  c9_chartstep_c9_LIDAR_sim(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  for (c9_i3 = 0; c9_i3 < 4; c9_i3++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c9_rotationMatrix)[c9_i3], 3U);
  }

  for (c9_i4 = 0; c9_i4 < 4; c9_i4++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c9_invRotationMatrix)[c9_i4], 4U);
  }
}

static void mdl_start_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  chartInstance->c9_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sim_mode_is_external(chartInstance->S);
}

static void c9_chartstep_c9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance)
{
  c9_emxArray_real_T *c9_obstaclesInRegionOfInterestTemp;
  c9_emxArray_real_T *c9_pictureMatrix;
  c9_emxArray_real_T *c9_x;
  c9_emxArray_real_T *c9_y;
  c9_emxArray_boolean_T *c9_BW;
  c9_emxArray_real_T *c9_H;
  c9_emxArray_real_T *c9_theta;
  c9_emxArray_real_T *c9_rho;
  c9_emxArray_real_T *c9_P;
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_lines;
  c9_emxArray_real_T *c9_orientationsOfLines;
  real_T c9_hoistedGlobal;
  real_T c9_b_hoistedGlobal;
  int32_T c9_i5;
  real_T c9_b_minRotation;
  real_T c9_b_maxRotation;
  uint32_T c9_debug_family_var_map[20];
  real_T c9_rotAngle;
  real_T c9_k;
  real_T c9_nargin = 3.0;
  real_T c9_nargout = 2.0;
  real_T c9_b_rotationMatrix[4];
  real_T c9_b_invRotationMatrix[4];
  int32_T c9_i6;
  int32_T c9_i7;
  c9_emxArray_real_T *c9_b_x;
  int32_T c9_i8;
  int32_T c9_i9;
  int32_T c9_b_size[1];
  int32_T c9_loop_ub;
  int32_T c9_i10;
  c9_emxArray_real_T *c9_varargin_1;
  boolean_T c9_b_data[50000];
  int32_T c9_i11;
  int32_T c9_b_loop_ub;
  int32_T c9_i12;
  int32_T c9_i13;
  int32_T c9_c_loop_ub;
  int32_T c9_i14;
  int32_T c9_i15;
  int32_T c9_i16;
  int32_T c9_d_loop_ub;
  int32_T c9_i17;
  int32_T c9_e_loop_ub;
  int32_T c9_i18;
  int32_T c9_i19;
  int32_T c9_f_loop_ub;
  int32_T c9_i20;
  int32_T c9_i21;
  int32_T c9_g_loop_ub;
  int32_T c9_i22;
  int32_T c9_i23;
  int32_T c9_h_loop_ub;
  int32_T c9_i24;
  boolean_T c9_b0;
  const mxArray *c9_b_y = NULL;
  const mxArray *c9_c_y = NULL;
  const mxArray *c9_d_y = NULL;
  int32_T c9_m;
  const mxArray *c9_e_y = NULL;
  int32_T c9_j;
  real_T c9_minval_data[2];
  int32_T c9_b_j;
  real_T c9_c_x;
  boolean_T c9_b;
  real_T c9_minval;
  int32_T c9_b_b;
  int32_T c9_c_b;
  real_T c9_d_x;
  real_T c9_e_x;
  boolean_T c9_d_b;
  real_T c9_f_x;
  boolean_T c9_overflow;
  real_T c9_g_x;
  real_T c9_f_y;
  int32_T c9_i25;
  int32_T c9_i;
  int32_T c9_i_loop_ub;
  real_T c9_a;
  int32_T c9_i26;
  real_T c9_e_b;
  real_T c9_b_a;
  real_T c9_f_b;
  real_T c9_h_x;
  int32_T c9_i27;
  boolean_T c9_g_b;
  real_T c9_i_x;
  boolean_T c9_p;
  boolean_T c9_h_b;
  int32_T c9_j_loop_ub;
  int32_T c9_i28;
  int32_T c9_i29;
  int32_T c9_k_loop_ub;
  int32_T c9_i30;
  c9_emxArray_real_T *c9_i_b;
  int32_T c9_i31;
  int32_T c9_i32;
  int32_T c9_l_loop_ub;
  int32_T c9_m_loop_ub;
  int32_T c9_i33;
  int32_T c9_i34;
  int32_T c9_b_k;
  c9_emxArray_real_T *c9_cost;
  c9_emxArray_int32_T *c9_idx;
  int32_T c9_c_k;
  int32_T c9_i35;
  int32_T c9_n;
  int32_T c9_col_data[2];
  int32_T c9_i36;
  int32_T c9_i37;
  int32_T c9_n_loop_ub;
  int32_T c9_o_loop_ub;
  int32_T c9_i38;
  int32_T c9_i39;
  c9_emxArray_real_T *c9_g_y;
  c9_emxArray_int32_T *c9_b_idx;
  c9_emxArray_real_T *c9_sint;
  int32_T c9_b_n;
  int32_T c9_i40;
  int32_T c9_i41;
  int32_T c9_i42;
  int32_T c9_p_loop_ub;
  int32_T c9_i43;
  int32_T c9_q_loop_ub;
  int32_T c9_i44;
  c9_emxArray_int32_T *c9_iwork;
  int32_T c9_len;
  int32_T c9_i45;
  int32_T c9_i46;
  int32_T c9_iv0[1];
  int32_T c9_i47;
  int32_T c9_r_loop_ub;
  int32_T c9_i48;
  int32_T c9_np1;
  int32_T c9_i49;
  int32_T c9_i50;
  int32_T c9_j_b;
  int32_T c9_k_b;
  int32_T c9_s_loop_ub;
  boolean_T c9_b_overflow;
  int32_T c9_i51;
  int32_T c9_d_k;
  int32_T c9_i52;
  c9_emxArray_real_T *c9_l_b;
  int32_T c9_i53;
  int32_T c9_i54;
  int32_T c9_t_loop_ub;
  int32_T c9_i55;
  int32_T c9_b_i;
  c9_emxArray_real_T *c9_m_b;
  int32_T c9_u_loop_ub;
  int32_T c9_i56;
  int32_T c9_i57;
  int32_T c9_i58;
  int32_T c9_c_a;
  int32_T c9_i2;
  int32_T c9_col_size[2];
  int32_T c9_c_j;
  int32_T c9_v_loop_ub;
  int32_T c9_i59;
  int32_T c9_pEnd;
  int32_T c9_i60;
  int32_T c9_i61;
  int32_T c9_b_p;
  int32_T c9_b_col_data[2];
  int32_T c9_w_loop_ub;
  int32_T c9_q;
  int32_T c9_i62;
  int32_T c9_qEnd;
  int32_T c9_i63;
  int32_T c9_e_k;
  int32_T c9_x_loop_ub;
  int32_T c9_kEnd;
  int32_T c9_i64;
  int32_T c9_nb;
  int32_T c9_khi;
  int32_T c9_f_k;
  int32_T c9_i65;
  int32_T c9_i66;
  c9_emxArray_real_T *c9_b_pictureMatrix;
  int32_T c9_g_k;
  int32_T c9_k0;
  int32_T c9_y_loop_ub;
  int32_T c9_ab_loop_ub;
  int32_T c9_i67;
  int32_T c9_i68;
  int32_T c9_d_a;
  boolean_T c9_b1;
  int32_T c9_b_col_size[2];
  c9_emxArray_real_T *c9_j_x;
  int32_T c9_i69;
  int32_T c9_i70;
  int32_T c9_e_a;
  int32_T c9_i71;
  int32_T c9_i72;
  c9_emxArray_real_T *c9_c_pictureMatrix;
  int32_T c9_i73;
  int32_T c9_d_j;
  int32_T c9_bb_loop_ub;
  int32_T c9_c_col_data[2];
  int32_T c9_i74;
  int32_T c9_cb_loop_ub;
  int32_T c9_f_a;
  int32_T c9_e_j;
  int32_T c9_i75;
  int32_T c9_i76;
  int32_T c9_i77;
  int32_T c9_g_a;
  int32_T c9_n_b;
  c9_emxArray_real_T *c9_h_y;
  int32_T c9_i78;
  int32_T c9_db_loop_ub;
  int32_T c9_h_a;
  int32_T c9_i79;
  int32_T c9_i80;
  int32_T c9_o_b;
  boolean_T c9_c_overflow;
  int32_T c9_eb_loop_ub;
  int32_T c9_fb_loop_ub;
  int32_T c9_i81;
  int32_T c9_i82;
  c9_coder_internal_sparse c9_r0;
  int32_T c9_p_b;
  c9_emxArray_boolean_T *c9_r1;
  int32_T c9_q_b;
  boolean_T c9_d_overflow;
  int32_T c9_i83;
  int32_T c9_gb_loop_ub;
  int32_T c9_i84;
  c9_emxArray_boolean_T *c9_b_BW;
  int32_T c9_i85;
  int32_T c9_hb_loop_ub;
  int32_T c9_i86;
  c9_emxArray_boolean_T *c9_r2;
  int32_T c9_i87;
  int32_T c9_ib_loop_ub;
  int32_T c9_i88;
  real_T c9_i_a;
  real_T c9_r_b;
  real_T c9_k_x;
  boolean_T c9_s_b;
  real_T c9_l_x;
  boolean_T c9_t_b;
  int32_T c9_i89;
  real_T c9_m_x;
  boolean_T c9_u_b;
  real_T c9_n_x;
  c9_emxArray_boolean_T *c9_b_varargin_1;
  boolean_T c9_v_b;
  int32_T c9_i90;
  int32_T c9_i91;
  real_T c9_j_a;
  real_T c9_w_b;
  real_T c9_k_a;
  int32_T c9_jb_loop_ub;
  real_T c9_x_b;
  int32_T c9_i92;
  real_T c9_anew;
  real_T c9_o_x;
  real_T c9_ndbl;
  c9_emxArray_real_T *c9_b_H;
  c9_emxArray_real_T *c9_b_theta;
  real_T c9_apnd;
  c9_emxArray_real_T *c9_b_rho;
  real_T c9_cdiff;
  real_T c9_p_x;
  int32_T c9_varargin_2;
  real_T c9_q_x;
  int32_T c9_b_varargin_2;
  real_T c9_r_x;
  real_T c9_c_n;
  real_T c9_i_y;
  int32_T c9_rhoLength;
  real_T c9_l_a;
  int32_T c9_c_varargin_2;
  real_T c9_y_b;
  int32_T c9_d_varargin_2;
  real_T c9_s_x;
  real_T c9_d_n;
  real_T c9_t_x;
  int32_T c9_thetaLength;
  real_T c9_u_x;
  real_T c9_absa;
  real_T c9_firstRho;
  real_T c9_v_x;
  real_T c9_numCol;
  real_T c9_w_x;
  real_T c9_numRow;
  real_T c9_x_x;
  int32_T c9_i93;
  real_T c9_absb;
  real_T c9_c;
  int32_T c9_kb_loop_ub;
  int32_T c9_i94;
  real_T c9_bnew;
  boolean_T c9_n_too_large;
  int32_T c9_i95;
  int32_T c9_e_n;
  int32_T c9_i96;
  boolean_T c9_c_p;
  const mxArray *c9_j_y = NULL;
  int32_T c9_ab_b;
  int32_T c9_i97;
  int32_T c9_bb_b;
  const mxArray *c9_k_y = NULL;
  boolean_T c9_e_overflow;
  int32_T c9_c_i;
  int32_T c9_m_a;
  int32_T c9_nm1;
  real_T c9_slope;
  int32_T c9_d_i;
  int32_T c9_n_a;
  int32_T c9_i98;
  real_T c9_y_x;
  int32_T c9_nm1d2;
  int32_T c9_f_n;
  real_T c9_ab_x;
  int32_T c9_o_a;
  int32_T c9_i99;
  int32_T c9_cb_b;
  real_T c9_g_n;
  real_T c9_bb_x;
  int32_T c9_db_b;
  int32_T c9_i100;
  real_T c9_cb_x;
  int32_T c9_i101;
  int32_T c9_b_m;
  boolean_T c9_f_overflow;
  real_T c9_c_m;
  int32_T c9_lb_loop_ub;
  int32_T c9_h_k;
  int32_T c9_i102;
  int32_T c9_eb_b;
  int32_T c9_fb_b;
  int32_T c9_gb_b;
  int32_T c9_i_k;
  boolean_T c9_g_overflow;
  int32_T c9_b_c;
  real_T c9_kd;
  int32_T c9_i103;
  int32_T c9_p_a;
  int32_T c9_q_a;
  int32_T c9_c_c;
  int32_T c9_r_a;
  int32_T c9_d_c;
  int32_T c9_thetaIdx;
  int32_T c9_e_c;
  int32_T c9_s_a;
  int32_T c9_mb_loop_ub;
  int32_T c9_hb_b;
  int32_T c9_i104;
  int32_T c9_t_a;
  int32_T c9_f_c;
  int32_T c9_b_thetaIdx;
  int32_T c9_g_c;
  real_T c9_myRho;
  real_T c9_db_x;
  int32_T c9_i105;
  int32_T c9_l_y;
  int32_T c9_rhoIdx;
  int32_T c9_nb_loop_ub;
  int32_T c9_i106;
  int32_T c9_i107;
  int32_T c9_ob_loop_ub;
  int32_T c9_i108;
  boolean_T c9_b2;
  const mxArray *c9_m_y = NULL;
  const mxArray *c9_n_y = NULL;
  const mxArray *c9_o_y = NULL;
  int32_T c9_h_n;
  const mxArray *c9_p_y = NULL;
  c9_emxArray_real_T *c9_b_cost;
  int32_T c9_i109;
  real_T c9_maxval;
  real_T c9_eb_x;
  boolean_T c9_ib_b;
  int32_T c9_pb_loop_ub;
  c9_emxArray_real_T *c9_c_H;
  int32_T c9_i110;
  real_T c9_fb_x;
  real_T c9_gb_x;
  boolean_T c9_jb_b;
  real_T c9_hb_x;
  int32_T c9_c_idx;
  int32_T c9_i111;
  int32_T c9_first;
  int32_T c9_last;
  int32_T c9_qb_loop_ub;
  real_T c9_ex;
  int32_T c9_i112;
  int32_T c9_i113;
  int32_T c9_u_a;
  int32_T c9_kb_b;
  c9_emxArray_real_T *c9_r3;
  int32_T c9_v_a;
  int32_T c9_lb_b;
  int32_T c9_i114;
  boolean_T c9_h_overflow;
  int32_T c9_rb_loop_ub;
  int32_T c9_j_k;
  int32_T c9_i115;
  c9_emxArray_boolean_T *c9_c_BW;
  int32_T c9_i116;
  int32_T c9_sb_loop_ub;
  int32_T c9_i117;
  c9_emxArray_real_T *c9_c_theta;
  int32_T c9_i118;
  int32_T c9_tb_loop_ub;
  int32_T c9_i119;
  c9_emxArray_real_T *c9_c_rho;
  int32_T c9_i120;
  int32_T c9_ub_loop_ub;
  int32_T c9_i121;
  c9_emxArray_real_T *c9_b_P;
  int32_T c9_i122;
  int32_T c9_vb_loop_ub;
  int32_T c9_i123;
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_r4;
  int32_T c9_i124;
  int32_T c9_wb_loop_ub;
  int32_T c9_i125;
  int32_T c9_e_varargin_2;
  int32_T c9_f_varargin_2;
  real_T c9_i_n;
  int32_T c9_i126;
  real_T c9_b_u;
  real_T c9_q_y;
  real_T c9_c_u;
  real_T c9_r_y;
  int32_T c9_xb_loop_ub;
  int32_T c9_i127;
  int32_T c9_g_varargin_2;
  int32_T c9_h_varargin_2;
  real_T c9_d0;
  int32_T c9_i128;
  int32_T c9_k_k;
  int32_T c9_i129;
  int32_T c9_yb_loop_ub;
  int32_T c9_i130;
  boolean_T c9_b3;
  const mxArray *c9_s_y = NULL;
  const mxArray *c9_t_y = NULL;
  real_T c9_M;
  int32_T c9_F;
  real_T c9_mtmp;
  int32_T c9_ftmp;
  real_T c9_d1;
  real_T c9_d2;
  int32_T c9_i131;
  real_T c9_d3;
  int32_T c9_l_k;
  real_T c9_d4;
  real_T c9_m_k;
  real_T c9_d5;
  int32_T c9_w_a;
  real_T c9_d6;
  real_T c9_d7;
  real_T c9_d8;
  real_T c9_d9;
  int32_T c9_i132;
  int32_T c9_i133;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  int32_T exitg1;
  c9_emxInit_real_T(chartInstance, &c9_obstaclesInRegionOfInterestTemp, 2,
                    &c9_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_pictureMatrix, 2, &c9_i_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_x, 2, &c9_r_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_y, 2, &c9_w_emlrtRTEI);
  c9_emxInit_boolean_T(chartInstance, &c9_BW, 2, &c9_db_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_H, 2, &c9_mb_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_theta, 2, &c9_nb_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_rho, 2, &c9_ob_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_P, 2, &c9_sb_emlrtRTEI);
  c9_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_lines, 2,
    &c9_xb_emlrtRTEI);
  c9_emxInit_real_T1(chartInstance, &c9_orientationsOfLines, 1, &c9_yb_emlrtRTEI);
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
  c9_hoistedGlobal = *chartInstance->c9_minRotation;
  c9_b_hoistedGlobal = *chartInstance->c9_maxRotation;
  for (c9_i5 = 0; c9_i5 < 100000; c9_i5++) {
    chartInstance->c9_obstaclesInRegionOfInterest[c9_i5] =
      (*chartInstance->c9_b_obstaclesInRegionOfInterest)[c9_i5];
  }

  c9_b_minRotation = c9_hoistedGlobal;
  c9_b_maxRotation = c9_b_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 20U, 20U, c9_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_rotAngle, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE
    (c9_obstaclesInRegionOfInterestTemp->data, (const int32_T *)
     c9_obstaclesInRegionOfInterestTemp->size, NULL, 0, 1, (void *)
     c9_b_sf_marshallOut, (void *)c9_b_sf_marshallIn,
     c9_obstaclesInRegionOfInterestTemp, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_pictureMatrix->data, (const
    int32_T *)c9_pictureMatrix->size, NULL, 0, 2, (void *)c9_b_sf_marshallOut,
    (void *)c9_b_sf_marshallIn, c9_pictureMatrix, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_x->data, (const int32_T *)
    c9_x->size, NULL, 0, 3, (void *)c9_c_sf_marshallOut, (void *)
    c9_c_sf_marshallIn, c9_x, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_y->data, (const int32_T *)
    c9_y->size, NULL, 0, 4, (void *)c9_c_sf_marshallOut, (void *)
    c9_c_sf_marshallIn, c9_y, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_BW->data, (const int32_T *)
    c9_BW->size, NULL, 0, 5, (void *)c9_d_sf_marshallOut, (void *)
    c9_d_sf_marshallIn, c9_BW, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_H->data, (const int32_T *)
    c9_H->size, NULL, 0, 6, (void *)c9_e_sf_marshallOut, (void *)
    c9_e_sf_marshallIn, c9_H, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_theta->data, (const int32_T *)
    c9_theta->size, NULL, 0, 7, (void *)c9_f_sf_marshallOut, (void *)
    c9_f_sf_marshallIn, c9_theta, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_rho->data, (const int32_T *)
    c9_rho->size, NULL, 0, 8, (void *)c9_f_sf_marshallOut, (void *)
    c9_f_sf_marshallIn, c9_rho, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_P->data, (const int32_T *)
    c9_P->size, NULL, 0, 9, (void *)c9_g_sf_marshallOut, (void *)
    c9_g_sf_marshallIn, c9_P, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_lines->data, (const int32_T *)
    c9_lines->size, NULL, 0, 10, (void *)c9_h_sf_marshallOut, (void *)
    c9_h_sf_marshallIn, c9_lines, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c9_orientationsOfLines->data, (
    const int32_T *)c9_orientationsOfLines->size, NULL, 0, 11, (void *)
    c9_i_sf_marshallOut, (void *)c9_i_sf_marshallIn, c9_orientationsOfLines,
    true);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_k, 12U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 13U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 14U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(chartInstance->c9_obstaclesInRegionOfInterest, 15U,
    c9_j_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c9_b_minRotation, 16U, c9_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c9_b_maxRotation, 17U, c9_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c9_b_rotationMatrix, 18U,
    c9_k_sf_marshallOut, c9_j_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c9_b_invRotationMatrix, 19U,
    c9_k_sf_marshallOut, c9_j_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 4);
  c9_rotAngle = 0.0;
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 6);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 7);
  c9_i6 = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1];
  c9_obstaclesInRegionOfInterestTemp->size[0] = 50000;
  c9_obstaclesInRegionOfInterestTemp->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_obstaclesInRegionOfInterestTemp,
    c9_i6, &c9_emlrtRTEI);
  for (c9_i7 = 0; c9_i7 < 100000; c9_i7++) {
    c9_obstaclesInRegionOfInterestTemp->data[c9_i7] =
      chartInstance->c9_obstaclesInRegionOfInterest[c9_i7];
  }

  c9_emxInit_real_T1(chartInstance, &c9_b_x, 1, &c9_b_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 9);
  c9_i8 = c9_b_x->size[0];
  c9_b_x->size[0] = 50000;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i8, &c9_b_emlrtRTEI);
  for (c9_i9 = 0; c9_i9 < 50000; c9_i9++) {
    c9_b_x->data[c9_i9] = c9_obstaclesInRegionOfInterestTemp->data[c9_i9];
  }

  c9_b_size[0] = c9_b_x->size[0];
  c9_loop_ub = c9_b_x->size[0] - 1;
  for (c9_i10 = 0; c9_i10 <= c9_loop_ub; c9_i10++) {
    c9_b_data[c9_i10] = muDoubleScalarIsNaN(c9_b_x->data[c9_i10]);
  }

  c9_emxInit_real_T(chartInstance, &c9_varargin_1, 2, &c9_g_emlrtRTEI);
  c9_i11 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
  c9_varargin_1->size[0] = c9_obstaclesInRegionOfInterestTemp->size[0];
  c9_varargin_1->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_varargin_1, c9_i11,
    (emlrtRTEInfo *)NULL);
  c9_b_loop_ub = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1] - 1;
  for (c9_i12 = 0; c9_i12 <= c9_b_loop_ub; c9_i12++) {
    c9_varargin_1->data[c9_i12] = c9_obstaclesInRegionOfInterestTemp->
      data[c9_i12];
  }

  c9_c_nullAssignment(chartInstance, c9_varargin_1, c9_b_data, c9_b_size);
  c9_i13 = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1];
  c9_obstaclesInRegionOfInterestTemp->size[0] = c9_varargin_1->size[0];
  c9_obstaclesInRegionOfInterestTemp->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_obstaclesInRegionOfInterestTemp,
    c9_i13, &c9_d_emlrtRTEI);
  c9_c_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i14 = 0; c9_i14 <= c9_c_loop_ub; c9_i14++) {
    c9_obstaclesInRegionOfInterestTemp->data[c9_i14] = c9_varargin_1->
      data[c9_i14];
  }

  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 10);
  c9_i15 = c9_obstaclesInRegionOfInterestTemp->size[0];
  c9_i16 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_i15;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i16, &c9_e_emlrtRTEI);
  c9_d_loop_ub = c9_i15 - 1;
  for (c9_i17 = 0; c9_i17 <= c9_d_loop_ub; c9_i17++) {
    c9_b_x->data[c9_i17] = c9_obstaclesInRegionOfInterestTemp->data[c9_i17 +
      c9_obstaclesInRegionOfInterestTemp->size[0]];
  }

  c9_b_size[0] = c9_b_x->size[0];
  c9_e_loop_ub = c9_b_x->size[0] - 1;
  for (c9_i18 = 0; c9_i18 <= c9_e_loop_ub; c9_i18++) {
    c9_b_data[c9_i18] = muDoubleScalarIsNaN(c9_b_x->data[c9_i18]);
  }

  c9_emxFree_real_T(chartInstance, &c9_b_x);
  c9_i19 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
  c9_varargin_1->size[0] = c9_obstaclesInRegionOfInterestTemp->size[0];
  c9_varargin_1->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_varargin_1, c9_i19,
    (emlrtRTEInfo *)NULL);
  c9_f_loop_ub = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1] - 1;
  for (c9_i20 = 0; c9_i20 <= c9_f_loop_ub; c9_i20++) {
    c9_varargin_1->data[c9_i20] = c9_obstaclesInRegionOfInterestTemp->
      data[c9_i20];
  }

  c9_c_nullAssignment(chartInstance, c9_varargin_1, c9_b_data, c9_b_size);
  c9_i21 = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1];
  c9_obstaclesInRegionOfInterestTemp->size[0] = c9_varargin_1->size[0];
  c9_obstaclesInRegionOfInterestTemp->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_obstaclesInRegionOfInterestTemp,
    c9_i21, &c9_f_emlrtRTEI);
  c9_g_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i22 = 0; c9_i22 <= c9_g_loop_ub; c9_i22++) {
    c9_obstaclesInRegionOfInterestTemp->data[c9_i22] = c9_varargin_1->
      data[c9_i22];
  }

  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 19);
  c9_i23 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
  c9_varargin_1->size[0] = c9_obstaclesInRegionOfInterestTemp->size[0];
  c9_varargin_1->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_varargin_1, c9_i23,
    &c9_g_emlrtRTEI);
  c9_h_loop_ub = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1] - 1;
  for (c9_i24 = 0; c9_i24 <= c9_h_loop_ub; c9_i24++) {
    c9_varargin_1->data[c9_i24] = c9_obstaclesInRegionOfInterestTemp->
      data[c9_i24];
  }

  if ((real_T)c9_varargin_1->size[0] != 1.0) {
    c9_b0 = true;
  } else {
    c9_b0 = false;
  }

  if (c9_b0) {
  } else {
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_b_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_c_y)));
  }

  if ((real_T)c9_varargin_1->size[0] >= 1.0) {
  } else {
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_e_y)));
  }

  c9_m = c9_varargin_1->size[0];
  for (c9_j = 0; c9_j < 2; c9_j++) {
    c9_b_j = c9_j;
    c9_minval_data[c9_b_j] = c9_varargin_1->data[c9_varargin_1->size[0] * c9_b_j];
    c9_b_b = c9_m;
    c9_c_b = c9_b_b;
    if (2 > c9_c_b) {
      c9_overflow = false;
    } else {
      c9_overflow = (c9_c_b > 2147483646);
    }

    if (c9_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_i = 1; c9_i < c9_m; c9_i++) {
      c9_a = c9_minval_data[c9_b_j];
      c9_e_b = c9_varargin_1->data[c9_i + c9_varargin_1->size[0] * c9_b_j];
      c9_b_a = c9_a;
      c9_f_b = c9_e_b;
      c9_h_x = c9_f_b;
      c9_g_b = muDoubleScalarIsNaN(c9_h_x);
      if (c9_g_b) {
        c9_p = false;
      } else {
        c9_i_x = c9_b_a;
        c9_h_b = muDoubleScalarIsNaN(c9_i_x);
        if (c9_h_b) {
          c9_p = true;
        } else {
          c9_p = (c9_b_a > c9_f_b);
        }
      }

      if (c9_p) {
        c9_minval_data[c9_b_j] = c9_varargin_1->data[c9_i + c9_varargin_1->size
          [0] * c9_b_j];
      }
    }
  }

  if (c9_minval_data[0] > c9_minval_data[1]) {
    c9_minval = c9_minval_data[1];
  } else {
    c9_c_x = c9_minval_data[0];
    c9_b = muDoubleScalarIsNaN(c9_c_x);
    if (c9_b) {
      c9_d_x = c9_minval_data[1];
      c9_d_b = muDoubleScalarIsNaN(c9_d_x);
      if (!c9_d_b) {
        c9_minval = c9_minval_data[1];
      } else {
        c9_minval = c9_minval_data[0];
      }
    } else {
      c9_minval = c9_minval_data[0];
    }
  }

  c9_e_x = c9_minval;
  c9_f_x = c9_e_x;
  c9_g_x = c9_f_x;
  c9_f_y = muDoubleScalarAbs(c9_g_x);
  c9_i25 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
  c9_varargin_1->size[0] = c9_obstaclesInRegionOfInterestTemp->size[0];
  c9_varargin_1->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_varargin_1, c9_i25,
    (emlrtRTEInfo *)NULL);
  c9_i_loop_ub = c9_obstaclesInRegionOfInterestTemp->size[0] *
    c9_obstaclesInRegionOfInterestTemp->size[1] - 1;
  for (c9_i26 = 0; c9_i26 <= c9_i_loop_ub; c9_i26++) {
    c9_varargin_1->data[c9_i26] = ((c9_obstaclesInRegionOfInterestTemp->
      data[c9_i26] + c9_f_y) + 1.0) * 100.0;
  }

  c9_c_round(chartInstance, c9_varargin_1);
  c9_i27 = c9_pictureMatrix->size[0] * c9_pictureMatrix->size[1];
  c9_pictureMatrix->size[0] = c9_varargin_1->size[0];
  c9_pictureMatrix->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_pictureMatrix, c9_i27,
    &c9_i_emlrtRTEI);
  c9_j_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i28 = 0; c9_i28 <= c9_j_loop_ub; c9_i28++) {
    c9_pictureMatrix->data[c9_i28] = c9_varargin_1->data[c9_i28];
  }

  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 20);
  c9_i29 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
  c9_varargin_1->size[0] = c9_pictureMatrix->size[0];
  c9_varargin_1->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_varargin_1, c9_i29,
    &c9_j_emlrtRTEI);
  c9_k_loop_ub = c9_pictureMatrix->size[0] * c9_pictureMatrix->size[1] - 1;
  for (c9_i30 = 0; c9_i30 <= c9_k_loop_ub; c9_i30++) {
    c9_varargin_1->data[c9_i30] = c9_pictureMatrix->data[c9_i30];
  }

  if ((real_T)c9_varargin_1->size[0] == 0.0) {
    c9_i31 = c9_pictureMatrix->size[0] * c9_pictureMatrix->size[1];
    c9_pictureMatrix->size[0] = c9_varargin_1->size[0];
    c9_pictureMatrix->size[1] = 2;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_pictureMatrix, c9_i31,
      &c9_l_emlrtRTEI);
    c9_l_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
    for (c9_i33 = 0; c9_i33 <= c9_l_loop_ub; c9_i33++) {
      c9_pictureMatrix->data[c9_i33] = c9_varargin_1->data[c9_i33];
    }
  } else {
    c9_emxInit_real_T(chartInstance, &c9_i_b, 2, &c9_bc_emlrtRTEI);
    c9_i32 = c9_i_b->size[0] * c9_i_b->size[1];
    c9_i_b->size[0] = c9_varargin_1->size[0];
    c9_i_b->size[1] = 2;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_i_b, c9_i32, &c9_k_emlrtRTEI);
    c9_m_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
    for (c9_i34 = 0; c9_i34 <= c9_m_loop_ub; c9_i34++) {
      c9_i_b->data[c9_i34] = c9_varargin_1->data[c9_i34];
    }

    for (c9_b_k = 0; c9_b_k < 2; c9_b_k++) {
      c9_c_k = c9_b_k;
      c9_col_data[c9_c_k] = c9_c_k + 1;
    }

    c9_emxInit_int32_T(chartInstance, &c9_idx, 1, &c9_o_emlrtRTEI);
    c9_n = c9_i_b->size[0];
    c9_i37 = c9_idx->size[0];
    c9_idx->size[0] = c9_i_b->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_idx, c9_i37, &c9_o_emlrtRTEI);
    c9_o_loop_ub = c9_i_b->size[0] - 1;
    for (c9_i39 = 0; c9_i39 <= c9_o_loop_ub; c9_i39++) {
      c9_idx->data[c9_i39] = 0;
    }

    c9_emxInit_int32_T(chartInstance, &c9_b_idx, 1, &c9_p_emlrtRTEI);
    c9_b_n = c9_n;
    c9_i40 = c9_b_idx->size[0];
    c9_b_idx->size[0] = c9_idx->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i40,
      &c9_p_emlrtRTEI);
    c9_p_loop_ub = c9_idx->size[0] - 1;
    for (c9_i43 = 0; c9_i43 <= c9_p_loop_ub; c9_i43++) {
      c9_b_idx->data[c9_i43] = c9_idx->data[c9_i43];
    }

    c9_emxInit_int32_T(chartInstance, &c9_iwork, 1, &c9_cc_emlrtRTEI);
    c9_len = c9_b_idx->size[0];
    c9_i45 = c9_b_idx->size[0];
    c9_b_idx->size[0] = c9_len;
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i45,
      &c9_q_emlrtRTEI);
    c9_iv0[0] = c9_b_idx->size[0];
    c9_i47 = c9_iwork->size[0];
    c9_iwork->size[0] = c9_iv0[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i47,
      &c9_s_emlrtRTEI);
    c9_np1 = c9_b_n + 1;
    c9_i49 = c9_b_n - 1;
    c9_j_b = c9_i49;
    c9_k_b = c9_j_b;
    c9_emxFree_int32_T(chartInstance, &c9_b_idx);
    if (1 > c9_k_b) {
      c9_b_overflow = false;
    } else {
      c9_b_overflow = (c9_k_b > 2147483645);
    }

    if (c9_b_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    c9_d_k = 1;
    c9_emxInit_real_T(chartInstance, &c9_l_b, 2, &c9_u_emlrtRTEI);
    while (c9_d_k <= c9_i49) {
      c9_i54 = c9_l_b->size[0] * c9_l_b->size[1];
      c9_l_b->size[0] = c9_i_b->size[0];
      c9_l_b->size[1] = 2;
      c9_emxEnsureCapacity_real_T(chartInstance, c9_l_b, c9_i54, &c9_u_emlrtRTEI);
      c9_u_loop_ub = c9_i_b->size[0] * c9_i_b->size[1] - 1;
      for (c9_i56 = 0; c9_i56 <= c9_u_loop_ub; c9_i56++) {
        c9_l_b->data[c9_i56] = c9_i_b->data[c9_i56];
      }

      c9_col_size[0] = 1;
      c9_col_size[1] = 2;
      for (c9_i60 = 0; c9_i60 < 2; c9_i60++) {
        c9_b_col_data[c9_i60] = c9_col_data[c9_i60];
      }

      if (c9_sortLE(chartInstance, c9_l_b, c9_b_col_data, c9_col_size, c9_d_k,
                    c9_d_k + 1)) {
        c9_idx->data[c9_d_k - 1] = c9_d_k;
        c9_idx->data[c9_d_k] = c9_d_k + 1;
      } else {
        c9_idx->data[c9_d_k - 1] = c9_d_k + 1;
        c9_idx->data[c9_d_k] = c9_d_k;
      }

      c9_d_k += 2;
    }

    c9_emxFree_real_T(chartInstance, &c9_l_b);
    if ((c9_b_n & 1) != 0) {
      c9_idx->data[c9_b_n - 1] = c9_b_n;
    }

    c9_b_i = 2;
    c9_emxInit_real_T(chartInstance, &c9_m_b, 2, &c9_u_emlrtRTEI);
    while (c9_b_i < c9_b_n) {
      c9_c_a = c9_b_i;
      c9_i2 = c9_c_a << 1;
      c9_c_j = 1;
      for (c9_pEnd = 1 + c9_b_i; c9_pEnd < c9_np1; c9_pEnd = c9_qEnd + c9_b_i) {
        c9_b_p = c9_c_j - 1;
        c9_q = c9_pEnd - 1;
        c9_qEnd = c9_c_j + c9_i2;
        if (c9_qEnd > c9_np1) {
          c9_qEnd = c9_np1;
        }

        c9_e_k = 0;
        c9_kEnd = c9_qEnd - c9_c_j;
        while (c9_e_k + 1 <= c9_kEnd) {
          c9_i65 = c9_m_b->size[0] * c9_m_b->size[1];
          c9_m_b->size[0] = c9_i_b->size[0];
          c9_m_b->size[1] = 2;
          c9_emxEnsureCapacity_real_T(chartInstance, c9_m_b, c9_i65,
            &c9_u_emlrtRTEI);
          c9_y_loop_ub = c9_i_b->size[0] * c9_i_b->size[1] - 1;
          for (c9_i67 = 0; c9_i67 <= c9_y_loop_ub; c9_i67++) {
            c9_m_b->data[c9_i67] = c9_i_b->data[c9_i67];
          }

          c9_b_col_size[0] = 1;
          c9_b_col_size[1] = 2;
          for (c9_i71 = 0; c9_i71 < 2; c9_i71++) {
            c9_c_col_data[c9_i71] = c9_col_data[c9_i71];
          }

          if (c9_sortLE(chartInstance, c9_m_b, c9_c_col_data, c9_b_col_size,
                        c9_idx->data[c9_b_p], c9_idx->data[c9_q])) {
            c9_iwork->data[c9_e_k] = c9_idx->data[c9_b_p];
            c9_b_p++;
            if (c9_b_p + 1 == c9_pEnd) {
              while (c9_q + 1 < c9_qEnd) {
                c9_e_k++;
                c9_iwork->data[c9_e_k] = c9_idx->data[c9_q];
                c9_q++;
              }
            }
          } else {
            c9_iwork->data[c9_e_k] = c9_idx->data[c9_q];
            c9_q++;
            if (c9_q + 1 == c9_qEnd) {
              while (c9_b_p + 1 < c9_pEnd) {
                c9_e_k++;
                c9_iwork->data[c9_e_k] = c9_idx->data[c9_b_p];
                c9_b_p++;
              }
            }
          }

          c9_e_k++;
        }

        c9_b_p = c9_c_j - 2;
        for (c9_g_k = 0; c9_g_k < c9_kEnd; c9_g_k++) {
          c9_e_k = c9_g_k;
          c9_idx->data[(c9_b_p + c9_e_k) + 1] = c9_iwork->data[c9_e_k];
        }

        c9_c_j = c9_qEnd;
      }

      c9_b_i = c9_i2;
    }

    c9_emxFree_real_T(chartInstance, &c9_m_b);
    c9_emxFree_int32_T(chartInstance, &c9_iwork);
    c9_b_apply_row_permutation(chartInstance, c9_i_b, c9_idx);
    c9_i59 = c9_pictureMatrix->size[0] * c9_pictureMatrix->size[1];
    c9_pictureMatrix->size[0] = c9_i_b->size[0];
    c9_pictureMatrix->size[1] = 2;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_pictureMatrix, c9_i59,
      &c9_l_emlrtRTEI);
    c9_w_loop_ub = c9_i_b->size[0] * c9_i_b->size[1] - 1;
    c9_emxFree_int32_T(chartInstance, &c9_idx);
    for (c9_i63 = 0; c9_i63 <= c9_w_loop_ub; c9_i63++) {
      c9_pictureMatrix->data[c9_i63] = c9_i_b->data[c9_i63];
    }

    c9_emxFree_real_T(chartInstance, &c9_i_b);
    c9_nb = 0;
    c9_khi = c9_varargin_1->size[0];
    c9_f_k = 1;
    c9_emxInit_real_T(chartInstance, &c9_b_pictureMatrix, 2, &c9_y_emlrtRTEI);
    while (c9_f_k <= c9_khi) {
      c9_k0 = c9_f_k;
      do {
        exitg1 = 0;
        c9_d_a = c9_f_k + 1;
        c9_f_k = c9_d_a;
        if (c9_f_k > c9_khi) {
          exitg1 = 1;
        } else {
          c9_i70 = c9_b_pictureMatrix->size[0] * c9_b_pictureMatrix->size[1];
          c9_b_pictureMatrix->size[0] = c9_pictureMatrix->size[0];
          c9_b_pictureMatrix->size[1] = 2;
          c9_emxEnsureCapacity_real_T(chartInstance, c9_b_pictureMatrix, c9_i70,
            &c9_y_emlrtRTEI);
          c9_bb_loop_ub = c9_pictureMatrix->size[0] * c9_pictureMatrix->size[1]
            - 1;
          for (c9_i74 = 0; c9_i74 <= c9_bb_loop_ub; c9_i74++) {
            c9_b_pictureMatrix->data[c9_i74] = c9_pictureMatrix->data[c9_i74];
          }

          if (c9_rows_differ(chartInstance, c9_b_pictureMatrix, c9_k0, c9_f_k))
          {
            exitg1 = 1;
          }
        }
      } while (exitg1 == 0);

      c9_e_a = c9_nb + 1;
      c9_nb = c9_e_a;
      for (c9_d_j = 0; c9_d_j < 2; c9_d_j++) {
        c9_e_j = c9_d_j;
        c9_pictureMatrix->data[(c9_nb + c9_pictureMatrix->size[0] * c9_e_j) - 1]
          = c9_pictureMatrix->data[(c9_k0 + c9_pictureMatrix->size[0] * c9_e_j)
          - 1];
      }

      c9_f_a = c9_f_k;
      c9_i77 = c9_f_a;
      c9_g_a = c9_k0;
      c9_n_b = c9_i77 - 1;
      c9_h_a = c9_g_a;
      c9_o_b = c9_n_b;
      if (c9_h_a > c9_o_b) {
        c9_c_overflow = false;
      } else {
        c9_c_overflow = (c9_o_b > 2147483646);
      }

      if (c9_c_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }
    }

    c9_emxFree_real_T(chartInstance, &c9_b_pictureMatrix);
    if (c9_nb <= c9_varargin_1->size[0]) {
    } else {
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                        c9_emlrt_marshallOut(chartInstance, c9_cv2), 14,
                        sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_emlrt_marshallOut(chartInstance, c9_cv2))));
    }

    c9_b1 = (1 > c9_nb);
    if (c9_b1) {
      c9_i69 = 0;
    } else {
      c9_i69 = c9_nb;
    }

    c9_emxInit_real_T(chartInstance, &c9_c_pictureMatrix, 2, &c9_ab_emlrtRTEI);
    c9_i73 = c9_c_pictureMatrix->size[0] * c9_c_pictureMatrix->size[1];
    c9_c_pictureMatrix->size[0] = c9_i69;
    c9_c_pictureMatrix->size[1] = 2;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_c_pictureMatrix, c9_i73,
      &c9_ab_emlrtRTEI);
    for (c9_i76 = 0; c9_i76 < 2; c9_i76++) {
      c9_db_loop_ub = c9_i69 - 1;
      for (c9_i80 = 0; c9_i80 <= c9_db_loop_ub; c9_i80++) {
        c9_c_pictureMatrix->data[c9_i80 + c9_c_pictureMatrix->size[0] * c9_i76] =
          c9_pictureMatrix->data[c9_i80 + c9_pictureMatrix->size[0] * c9_i76];
      }
    }

    c9_i78 = c9_pictureMatrix->size[0] * c9_pictureMatrix->size[1];
    c9_pictureMatrix->size[0] = c9_c_pictureMatrix->size[0];
    c9_pictureMatrix->size[1] = 2;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_pictureMatrix, c9_i78,
      &c9_l_emlrtRTEI);
    c9_eb_loop_ub = c9_c_pictureMatrix->size[0] * c9_c_pictureMatrix->size[1] -
      1;
    for (c9_i81 = 0; c9_i81 <= c9_eb_loop_ub; c9_i81++) {
      c9_pictureMatrix->data[c9_i81] = c9_c_pictureMatrix->data[c9_i81];
    }

    c9_emxFree_real_T(chartInstance, &c9_c_pictureMatrix);
    c9_p_b = c9_nb;
    c9_q_b = c9_p_b;
    if (1 > c9_q_b) {
      c9_d_overflow = false;
    } else {
      c9_d_overflow = (c9_q_b > 2147483646);
    }

    if (c9_d_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_varargin_1);
  c9_emxInit_real_T1(chartInstance, &c9_cost, 1, &c9_gc_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 21);
  c9_i35 = c9_pictureMatrix->size[0];
  c9_i36 = c9_cost->size[0];
  c9_cost->size[0] = c9_i35;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_cost, c9_i36, &c9_n_emlrtRTEI);
  c9_n_loop_ub = c9_i35 - 1;
  for (c9_i38 = 0; c9_i38 <= c9_n_loop_ub; c9_i38++) {
    c9_cost->data[c9_i38] = c9_pictureMatrix->data[c9_i38];
  }

  c9_emxInit_real_T(chartInstance, &c9_g_y, 2, &c9_dc_emlrtRTEI);
  c9_emxInit_real_T1(chartInstance, &c9_sint, 1, &c9_hc_emlrtRTEI);
  c9_d_round(chartInstance, c9_cost);
  c9_i41 = c9_pictureMatrix->size[0];
  c9_i42 = c9_sint->size[0];
  c9_sint->size[0] = c9_i41;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_sint, c9_i42, &c9_n_emlrtRTEI);
  c9_q_loop_ub = c9_i41 - 1;
  for (c9_i44 = 0; c9_i44 <= c9_q_loop_ub; c9_i44++) {
    c9_sint->data[c9_i44] = c9_pictureMatrix->data[c9_i44];
  }

  c9_d_round(chartInstance, c9_sint);
  c9_i46 = c9_g_y->size[0] * c9_g_y->size[1];
  c9_g_y->size[0] = 1;
  c9_g_y->size[1] = c9_sint->size[0];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_g_y, c9_i46, &c9_n_emlrtRTEI);
  c9_r_loop_ub = c9_sint->size[0] - 1;
  for (c9_i48 = 0; c9_i48 <= c9_r_loop_ub; c9_i48++) {
    c9_g_y->data[c9_i48] = c9_sint->data[c9_i48];
  }

  c9_i50 = c9_x->size[0] * c9_x->size[1];
  c9_x->size[0] = 1;
  c9_x->size[1] = c9_cost->size[0];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_x, c9_i50, &c9_r_emlrtRTEI);
  c9_s_loop_ub = c9_cost->size[0] - 1;
  for (c9_i51 = 0; c9_i51 <= c9_s_loop_ub; c9_i51++) {
    c9_x->data[c9_i51] = c9_g_y->data[c9_i51];
  }

  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 22);
  c9_i52 = c9_pictureMatrix->size[0];
  c9_i53 = c9_cost->size[0];
  c9_cost->size[0] = c9_i52;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_cost, c9_i53, &c9_t_emlrtRTEI);
  c9_t_loop_ub = c9_i52 - 1;
  for (c9_i55 = 0; c9_i55 <= c9_t_loop_ub; c9_i55++) {
    c9_cost->data[c9_i55] = c9_pictureMatrix->data[c9_i55 +
      c9_pictureMatrix->size[0]];
  }

  c9_d_round(chartInstance, c9_cost);
  c9_i57 = c9_pictureMatrix->size[0];
  c9_i58 = c9_sint->size[0];
  c9_sint->size[0] = c9_i57;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_sint, c9_i58, &c9_t_emlrtRTEI);
  c9_v_loop_ub = c9_i57 - 1;
  for (c9_i61 = 0; c9_i61 <= c9_v_loop_ub; c9_i61++) {
    c9_sint->data[c9_i61] = c9_pictureMatrix->data[c9_i61 +
      c9_pictureMatrix->size[0]];
  }

  c9_d_round(chartInstance, c9_sint);
  c9_i62 = c9_g_y->size[0] * c9_g_y->size[1];
  c9_g_y->size[0] = 1;
  c9_g_y->size[1] = c9_sint->size[0];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_g_y, c9_i62, &c9_t_emlrtRTEI);
  c9_x_loop_ub = c9_sint->size[0] - 1;
  for (c9_i64 = 0; c9_i64 <= c9_x_loop_ub; c9_i64++) {
    c9_g_y->data[c9_i64] = c9_sint->data[c9_i64];
  }

  c9_i66 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = 1;
  c9_y->size[1] = c9_cost->size[0];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_y, c9_i66, &c9_w_emlrtRTEI);
  c9_ab_loop_ub = c9_cost->size[0] - 1;
  for (c9_i68 = 0; c9_i68 <= c9_ab_loop_ub; c9_i68++) {
    c9_y->data[c9_i68] = c9_g_y->data[c9_i68];
  }

  c9_emxInit_real_T(chartInstance, &c9_j_x, 2, &c9_x_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 23);
  c9_i72 = c9_j_x->size[0] * c9_j_x->size[1];
  c9_j_x->size[0] = 1;
  c9_j_x->size[1] = c9_x->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_j_x, c9_i72, &c9_x_emlrtRTEI);
  c9_cb_loop_ub = c9_x->size[0] * c9_x->size[1] - 1;
  for (c9_i75 = 0; c9_i75 <= c9_cb_loop_ub; c9_i75++) {
    c9_j_x->data[c9_i75] = c9_x->data[c9_i75];
  }

  c9_emxInit_real_T(chartInstance, &c9_h_y, 2, &c9_bb_emlrtRTEI);
  c9_i79 = c9_h_y->size[0] * c9_h_y->size[1];
  c9_h_y->size[0] = 1;
  c9_h_y->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_h_y, c9_i79, &c9_bb_emlrtRTEI);
  c9_fb_loop_ub = c9_y->size[0] * c9_y->size[1] - 1;
  for (c9_i82 = 0; c9_i82 <= c9_fb_loop_ub; c9_i82++) {
    c9_h_y->data[c9_i82] = c9_y->data[c9_i82];
  }

  c9_emxInitStruct_coder_internal_sp(chartInstance, &c9_r0, &c9_ic_emlrtRTEI);
  c9_emxInit_boolean_T(chartInstance, &c9_r1, 2, &c9_mc_emlrtRTEI);
  c9_sparse(chartInstance, c9_j_x, c9_h_y, &c9_r0);
  c9_sparse_full(chartInstance, c9_r0, c9_r1);
  c9_i83 = c9_BW->size[0] * c9_BW->size[1];
  c9_BW->size[0] = c9_r1->size[0];
  c9_BW->size[1] = c9_r1->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW, c9_i83, &c9_db_emlrtRTEI);
  c9_gb_loop_ub = c9_r1->size[0] * c9_r1->size[1] - 1;
  c9_emxFree_real_T(chartInstance, &c9_h_y);
  c9_emxFree_real_T(chartInstance, &c9_j_x);
  c9_emxFreeStruct_coder_internal_sp(chartInstance, &c9_r0);
  for (c9_i84 = 0; c9_i84 <= c9_gb_loop_ub; c9_i84++) {
    c9_BW->data[c9_i84] = c9_r1->data[c9_i84];
  }

  c9_emxFree_boolean_T(chartInstance, &c9_r1);
  c9_emxInit_boolean_T(chartInstance, &c9_b_BW, 2, &c9_eb_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 28);
  c9_i85 = c9_b_BW->size[0] * c9_b_BW->size[1];
  c9_b_BW->size[0] = c9_BW->size[0];
  c9_b_BW->size[1] = c9_BW->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_b_BW, c9_i85,
    &c9_eb_emlrtRTEI);
  c9_hb_loop_ub = c9_BW->size[0] * c9_BW->size[1] - 1;
  for (c9_i86 = 0; c9_i86 <= c9_hb_loop_ub; c9_i86++) {
    c9_b_BW->data[c9_i86] = c9_BW->data[c9_i86];
  }

  c9_emxInit_boolean_T(chartInstance, &c9_r2, 2, &c9_lc_emlrtRTEI);
  c9_edge(chartInstance, c9_b_BW, c9_r2);
  c9_i87 = c9_BW->size[0] * c9_BW->size[1];
  c9_BW->size[0] = c9_r2->size[0];
  c9_BW->size[1] = c9_r2->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW, c9_i87, &c9_fb_emlrtRTEI);
  c9_ib_loop_ub = c9_r2->size[0] * c9_r2->size[1] - 1;
  c9_emxFree_boolean_T(chartInstance, &c9_b_BW);
  for (c9_i88 = 0; c9_i88 <= c9_ib_loop_ub; c9_i88++) {
    c9_BW->data[c9_i88] = c9_r2->data[c9_i88];
  }

  c9_emxFree_boolean_T(chartInstance, &c9_r2);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 36);
  c9_i_a = c9_b_minRotation;
  c9_r_b = c9_b_maxRotation;
  c9_k_x = c9_i_a;
  c9_s_b = muDoubleScalarIsNaN(c9_k_x);
  guard1 = false;
  guard2 = false;
  guard3 = false;
  if (c9_s_b) {
    guard2 = true;
  } else {
    c9_l_x = c9_r_b;
    c9_t_b = muDoubleScalarIsNaN(c9_l_x);
    if (c9_t_b) {
      guard2 = true;
    } else if (c9_r_b < c9_i_a) {
      c9_g_y->size[0] = 1;
      c9_g_y->size[1] = 0;
    } else {
      c9_m_x = c9_i_a;
      c9_u_b = muDoubleScalarIsInf(c9_m_x);
      if (c9_u_b) {
        guard3 = true;
      } else {
        c9_n_x = c9_r_b;
        c9_v_b = muDoubleScalarIsInf(c9_n_x);
        if (c9_v_b) {
          guard3 = true;
        } else {
          guard1 = true;
        }
      }
    }
  }

  if (guard3) {
    if (c9_i_a == c9_r_b) {
      c9_i91 = c9_g_y->size[0] * c9_g_y->size[1];
      c9_g_y->size[0] = 1;
      c9_g_y->size[1] = 1;
      c9_emxEnsureCapacity_real_T(chartInstance, c9_g_y, c9_i91,
        &c9_ib_emlrtRTEI);
      c9_g_y->data[0] = rtNaN;
    } else {
      guard1 = true;
    }
  }

  if (guard2) {
    c9_i89 = c9_g_y->size[0] * c9_g_y->size[1];
    c9_g_y->size[0] = 1;
    c9_g_y->size[1] = 1;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_g_y, c9_i89, &c9_gb_emlrtRTEI);
    c9_g_y->data[0] = rtNaN;
  }

  if (guard1) {
    c9_j_a = c9_i_a;
    c9_w_b = c9_r_b;
    c9_k_a = c9_j_a;
    c9_x_b = c9_w_b;
    c9_anew = c9_k_a;
    c9_o_x = (c9_x_b - c9_k_a) / 0.5 + 0.5;
    c9_ndbl = c9_o_x;
    c9_ndbl = muDoubleScalarFloor(c9_ndbl);
    c9_apnd = c9_k_a + c9_ndbl * 0.5;
    c9_cdiff = c9_apnd - c9_x_b;
    c9_p_x = c9_cdiff;
    c9_q_x = c9_p_x;
    c9_r_x = c9_q_x;
    c9_i_y = muDoubleScalarAbs(c9_r_x);
    c9_l_a = c9_k_a;
    c9_y_b = c9_x_b;
    c9_s_x = c9_l_a;
    c9_t_x = c9_s_x;
    c9_u_x = c9_t_x;
    c9_absa = muDoubleScalarAbs(c9_u_x);
    c9_v_x = c9_y_b;
    c9_w_x = c9_v_x;
    c9_x_x = c9_w_x;
    c9_absb = muDoubleScalarAbs(c9_x_x);
    if (c9_absa > c9_absb) {
      c9_c = c9_absa;
    } else {
      c9_c = c9_absb;
    }

    if (c9_i_y < 4.4408920985006262E-16 * c9_c) {
      c9_ndbl++;
      c9_bnew = c9_x_b;
    } else if (c9_cdiff > 0.0) {
      c9_bnew = c9_k_a + (c9_ndbl - 1.0) * 0.5;
    } else {
      c9_ndbl++;
      c9_bnew = c9_apnd;
    }

    c9_n_too_large = (c9_ndbl > 2.147483647E+9);
    if (c9_ndbl >= 0.0) {
      c9_e_n = (int32_T)c9_ndbl;
    } else {
      c9_e_n = 0;
    }

    c9_c_p = !c9_n_too_large;
    if (c9_c_p) {
    } else {
      c9_j_y = NULL;
      sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv3, 10, 0U, 1U, 0U, 2, 1, 21),
                    false);
      c9_k_y = NULL;
      sf_mex_assign(&c9_k_y, sf_mex_create("y", c9_cv3, 10, 0U, 1U, 0U, 2, 1, 21),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_j_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_k_y)));
    }

    c9_i97 = c9_g_y->size[0] * c9_g_y->size[1];
    c9_g_y->size[0] = 1;
    c9_g_y->size[1] = c9_e_n;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_g_y, c9_i97, &c9_lb_emlrtRTEI);
    if (c9_e_n > 0) {
      c9_g_y->data[0] = c9_anew;
      if (c9_e_n > 1) {
        c9_g_y->data[c9_e_n - 1] = c9_bnew;
        c9_m_a = c9_e_n - 1;
        c9_nm1 = c9_m_a;
        c9_n_a = c9_nm1;
        c9_nm1d2 = c9_div_nzp_s32(chartInstance, c9_n_a, 2, 1U, 1489, 27);
        c9_o_a = c9_nm1d2 - 2;
        c9_i99 = c9_o_a;
        c9_cb_b = c9_i99 + 1;
        c9_db_b = c9_cb_b;
        if (1 > c9_db_b) {
          c9_f_overflow = false;
        } else {
          c9_f_overflow = (c9_db_b > 2147483646);
        }

        if (c9_f_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_h_k = 1; c9_h_k - 1 <= c9_i99; c9_h_k++) {
          c9_i_k = c9_h_k;
          c9_kd = (real_T)c9_i_k * 0.5;
          c9_p_a = c9_i_k;
          c9_c_c = c9_p_a;
          c9_g_y->data[c9_c_c] = c9_anew + c9_kd;
          c9_s_a = c9_e_n;
          c9_hb_b = c9_i_k;
          c9_f_c = (c9_s_a - c9_hb_b) - 1;
          c9_g_y->data[c9_f_c] = c9_bnew - c9_kd;
        }

        c9_gb_b = c9_nm1d2;
        c9_b_c = c9_gb_b << 1;
        if (c9_b_c == c9_nm1) {
          c9_q_a = c9_nm1d2;
          c9_d_c = c9_q_a;
          c9_g_y->data[c9_d_c] = (c9_anew + c9_bnew) / 2.0;
        } else {
          c9_kd = (real_T)c9_nm1d2 * 0.5;
          c9_r_a = c9_nm1d2;
          c9_e_c = c9_r_a;
          c9_g_y->data[c9_e_c] = c9_anew + c9_kd;
          c9_t_a = c9_nm1d2;
          c9_g_c = c9_t_a;
          c9_g_y->data[c9_g_c + 1] = c9_bnew - c9_kd;
        }
      }
    }
  }

  c9_emxInit_boolean_T(chartInstance, &c9_b_varargin_1, 2, &c9_hb_emlrtRTEI);
  c9_i90 = c9_b_varargin_1->size[0] * c9_b_varargin_1->size[1];
  c9_b_varargin_1->size[0] = c9_BW->size[0];
  c9_b_varargin_1->size[1] = c9_BW->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_b_varargin_1, c9_i90,
    &c9_hb_emlrtRTEI);
  c9_jb_loop_ub = c9_BW->size[0] * c9_BW->size[1] - 1;
  for (c9_i92 = 0; c9_i92 <= c9_jb_loop_ub; c9_i92++) {
    c9_b_varargin_1->data[c9_i92] = c9_BW->data[c9_i92];
  }

  c9_emxInit_real_T(chartInstance, &c9_b_H, 2, (emlrtRTEInfo *)NULL);
  c9_emxInit_real_T(chartInstance, &c9_b_theta, 2, &c9_ec_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_b_rho, 2, &c9_fc_emlrtRTEI);
  c9_b_parseInputs(chartInstance, c9_b_varargin_1, c9_g_y, c9_b_theta, c9_b_rho);
  c9_varargin_2 = c9_b_rho->size[1];
  c9_b_varargin_2 = c9_varargin_2;
  c9_c_n = (real_T)c9_b_varargin_2;
  c9_rhoLength = (int32_T)c9_c_n;
  c9_c_varargin_2 = c9_b_theta->size[1];
  c9_d_varargin_2 = c9_c_varargin_2;
  c9_d_n = (real_T)c9_d_varargin_2;
  c9_thetaLength = (int32_T)c9_d_n;
  sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S, 1U,
    1456, 61, MAX_uint32_T, 1, 1, c9_b_rho->size[1]);
  c9_firstRho = c9_b_rho->data[0];
  c9_numCol = (real_T)c9_b_varargin_1->size[1];
  c9_numRow = (real_T)c9_b_varargin_1->size[0];
  c9_i93 = c9_b_H->size[0] * c9_b_H->size[1];
  c9_b_H->size[0] = c9_rhoLength;
  c9_b_H->size[1] = c9_thetaLength;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_H, c9_i93, &c9_jb_emlrtRTEI);
  c9_kb_loop_ub = c9_rhoLength * c9_thetaLength - 1;
  c9_emxFree_real_T(chartInstance, &c9_g_y);
  for (c9_i94 = 0; c9_i94 <= c9_kb_loop_ub; c9_i94++) {
    c9_b_H->data[c9_i94] = 0.0;
  }

  c9_i95 = c9_cost->size[0];
  c9_cost->size[0] = c9_thetaLength;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_cost, c9_i95, &c9_kb_emlrtRTEI);
  c9_i96 = c9_sint->size[0];
  c9_sint->size[0] = c9_thetaLength;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_sint, c9_i96, &c9_kb_emlrtRTEI);
  c9_ab_b = c9_thetaLength;
  c9_bb_b = c9_ab_b;
  if (1 > c9_bb_b) {
    c9_e_overflow = false;
  } else {
    c9_e_overflow = (c9_bb_b > 2147483646);
  }

  if (c9_e_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_c_i = 1; c9_c_i - 1 < c9_thetaLength; c9_c_i++) {
    c9_d_i = c9_c_i;
    c9_y_x = c9_b_theta->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61, MAX_uint32_T,
       c9_d_i, 1, c9_b_theta->size[1]) - 1] * 3.1415926535897931 / 180.0;
    c9_ab_x = c9_y_x;
    c9_ab_x = muDoubleScalarCos(c9_ab_x);
    c9_cost->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 1456, 61, MAX_uint32_T, c9_d_i, 1, c9_cost->size[0])
      - 1] = c9_ab_x;
    c9_bb_x = c9_b_theta->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61, MAX_uint32_T,
       c9_d_i, 1, c9_b_theta->size[1]) - 1] * 3.1415926535897931 / 180.0;
    c9_cb_x = c9_bb_x;
    c9_cb_x = muDoubleScalarSin(c9_cb_x);
    c9_sint->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 1456, 61, MAX_uint32_T, c9_d_i, 1, c9_sint->size[0])
      - 1] = c9_cb_x;
  }

  c9_slope = (real_T)(c9_rhoLength - 1) / (c9_b_rho->
    data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S,
    1U, 1456, 61, MAX_uint32_T, c9_rhoLength, 1, c9_b_rho->size[1]) - 1] -
    c9_firstRho);
  c9_i98 = (int32_T)c9_numCol - 1;
  for (c9_f_n = 0; c9_f_n <= c9_i98; c9_f_n++) {
    c9_g_n = 1.0 + (real_T)c9_f_n;
    c9_i100 = (int32_T)c9_numRow - 1;
    for (c9_b_m = 0; c9_b_m <= c9_i100; c9_b_m++) {
      c9_c_m = 1.0 + (real_T)c9_b_m;
      if (c9_b_varargin_1->data[(sf_eml_array_bounds_check
           (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61,
            MAX_uint32_T, (int32_T)c9_c_m, 1, c9_b_varargin_1->size[0]) +
           c9_b_varargin_1->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61,
             MAX_uint32_T, (int32_T)c9_g_n, 1, c9_b_varargin_1->size[1]) - 1)) -
          1]) {
        c9_eb_b = c9_thetaLength;
        c9_fb_b = c9_eb_b;
        if (1 > c9_fb_b) {
          c9_g_overflow = false;
        } else {
          c9_g_overflow = (c9_fb_b > 2147483646);
        }

        if (c9_g_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_thetaIdx = 1; c9_thetaIdx - 1 < c9_thetaLength; c9_thetaIdx++) {
          c9_b_thetaIdx = c9_thetaIdx;
          c9_myRho = (c9_g_n - 1.0) * c9_cost->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61,
             MAX_uint32_T, c9_b_thetaIdx, 1, c9_cost->size[0]) - 1] + (c9_c_m -
            1.0) * c9_sint->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61,
             MAX_uint32_T, c9_b_thetaIdx, 1, c9_sint->size[0]) - 1];
          c9_db_x = c9_slope * (c9_myRho - c9_firstRho);
          c9_l_y = (int32_T)(c9_db_x + 0.5) + 1;
          c9_rhoIdx = c9_l_y;
          c9_b_H->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 1456, 61, MAX_uint32_T, c9_rhoIdx, 1,
            c9_b_H->size[0]) + c9_b_H->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61,
             MAX_uint32_T, c9_b_thetaIdx, 1, c9_b_H->size[1]) - 1)) - 1] =
            c9_b_H->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 1456, 61, MAX_uint32_T, c9_rhoIdx, 1,
            c9_b_H->size[0]) + c9_b_H->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1456, 61,
             MAX_uint32_T, c9_b_thetaIdx, 1, c9_b_H->size[1]) - 1)) - 1] + 1.0;
        }
      }
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_sint);
  c9_emxFree_boolean_T(chartInstance, &c9_b_varargin_1);
  c9_i101 = c9_H->size[0] * c9_H->size[1];
  c9_H->size[0] = c9_b_H->size[0];
  c9_H->size[1] = c9_b_H->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_H, c9_i101, &c9_mb_emlrtRTEI);
  c9_lb_loop_ub = c9_b_H->size[0] * c9_b_H->size[1] - 1;
  for (c9_i102 = 0; c9_i102 <= c9_lb_loop_ub; c9_i102++) {
    c9_H->data[c9_i102] = c9_b_H->data[c9_i102];
  }

  c9_emxFree_real_T(chartInstance, &c9_b_H);
  c9_i103 = c9_theta->size[0] * c9_theta->size[1];
  c9_theta->size[0] = 1;
  c9_theta->size[1] = c9_b_theta->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_theta, c9_i103, &c9_nb_emlrtRTEI);
  c9_mb_loop_ub = c9_b_theta->size[0] * c9_b_theta->size[1] - 1;
  for (c9_i104 = 0; c9_i104 <= c9_mb_loop_ub; c9_i104++) {
    c9_theta->data[c9_i104] = c9_b_theta->data[c9_i104];
  }

  c9_emxFree_real_T(chartInstance, &c9_b_theta);
  c9_i105 = c9_rho->size[0] * c9_rho->size[1];
  c9_rho->size[0] = 1;
  c9_rho->size[1] = c9_b_rho->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_rho, c9_i105, &c9_ob_emlrtRTEI);
  c9_nb_loop_ub = c9_b_rho->size[0] * c9_b_rho->size[1] - 1;
  for (c9_i106 = 0; c9_i106 <= c9_nb_loop_ub; c9_i106++) {
    c9_rho->data[c9_i106] = c9_b_rho->data[c9_i106];
  }

  c9_emxFree_real_T(chartInstance, &c9_b_rho);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 38);
  c9_i107 = c9_cost->size[0];
  c9_cost->size[0] = c9__s32_s64_(chartInstance, (int64_T)c9_H->size[0] *
    (int64_T)c9_H->size[1], 1U, 1619, 4);
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_cost, c9_i107, &c9_pb_emlrtRTEI);
  c9_ob_loop_ub = c9__s32_s64_(chartInstance, (int64_T)c9_H->size[0] * (int64_T)
    c9_H->size[1], 1U, 1619, 4) - 1;
  for (c9_i108 = 0; c9_i108 <= c9_ob_loop_ub; c9_i108++) {
    c9_cost->data[c9_i108] = c9_H->data[c9_i108];
  }

  if ((c9_cost->size[0] == 1) || ((real_T)c9_cost->size[0] != 1.0)) {
    c9_b2 = true;
  } else {
    c9_b2 = false;
  }

  if (c9_b2) {
  } else {
    c9_m_y = NULL;
    sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c9_n_y = NULL;
    sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_m_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_n_y)));
  }

  if ((real_T)c9_cost->size[0] >= 1.0) {
  } else {
    c9_o_y = NULL;
    sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    c9_p_y = NULL;
    sf_mex_assign(&c9_p_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_o_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_p_y)));
  }

  c9_h_n = c9_cost->size[0];
  if (c9_h_n <= 2) {
    if (c9_h_n == 1) {
      c9_maxval = c9_cost->data[0];
    } else if (c9_cost->data[0] < c9_cost->data[1]) {
      c9_maxval = c9_cost->data[1];
    } else {
      c9_eb_x = c9_cost->data[0];
      c9_ib_b = muDoubleScalarIsNaN(c9_eb_x);
      if (c9_ib_b) {
        c9_fb_x = c9_cost->data[1];
        c9_jb_b = muDoubleScalarIsNaN(c9_fb_x);
        if (!c9_jb_b) {
          c9_maxval = c9_cost->data[1];
        } else {
          c9_maxval = c9_cost->data[0];
        }
      } else {
        c9_maxval = c9_cost->data[0];
      }
    }
  } else {
    c9_emxInit_real_T1(chartInstance, &c9_b_cost, 1, &c9_qb_emlrtRTEI);
    c9_i109 = c9_b_cost->size[0];
    c9_b_cost->size[0] = c9_cost->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_cost, c9_i109,
      &c9_qb_emlrtRTEI);
    c9_pb_loop_ub = c9_cost->size[0] - 1;
    for (c9_i110 = 0; c9_i110 <= c9_pb_loop_ub; c9_i110++) {
      c9_b_cost->data[c9_i110] = c9_cost->data[c9_i110];
    }

    c9_c_idx = c9_findFirst(chartInstance, c9_b_cost);
    c9_emxFree_real_T(chartInstance, &c9_b_cost);
    if (c9_c_idx == 0) {
      c9_maxval = c9_cost->data[0];
    } else {
      c9_first = c9_c_idx - 1;
      c9_last = c9_h_n;
      c9_ex = c9_cost->data[c9_first];
      c9_i113 = c9_first + 2;
      c9_u_a = c9_i113;
      c9_kb_b = c9_last;
      c9_v_a = c9_u_a;
      c9_lb_b = c9_kb_b;
      if (c9_v_a > c9_lb_b) {
        c9_h_overflow = false;
      } else {
        c9_h_overflow = (c9_lb_b > 2147483646);
      }

      if (c9_h_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }

      for (c9_j_k = c9_i113 - 1; c9_j_k < c9_last; c9_j_k++) {
        if (c9_ex < c9_cost->data[c9_j_k]) {
          c9_ex = c9_cost->data[c9_j_k];
        }
      }

      c9_maxval = c9_ex;
    }
  }

  c9_emxInit_real_T(chartInstance, &c9_c_H, 2, &c9_rb_emlrtRTEI);
  c9_gb_x = 0.3 * c9_maxval;
  c9_hb_x = c9_gb_x;
  c9_hb_x = muDoubleScalarCeil(c9_hb_x);
  c9_i111 = c9_c_H->size[0] * c9_c_H->size[1];
  c9_c_H->size[0] = c9_H->size[0];
  c9_c_H->size[1] = c9_H->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_c_H, c9_i111, &c9_rb_emlrtRTEI);
  c9_qb_loop_ub = c9_H->size[0] * c9_H->size[1] - 1;
  for (c9_i112 = 0; c9_i112 <= c9_qb_loop_ub; c9_i112++) {
    c9_c_H->data[c9_i112] = c9_H->data[c9_i112];
  }

  c9_emxInit_real_T(chartInstance, &c9_r3, 2, &c9_kc_emlrtRTEI);
  c9_houghpeaks(chartInstance, c9_c_H, c9_hb_x, c9_r3);
  c9_i114 = c9_P->size[0] * c9_P->size[1];
  c9_P->size[0] = c9_r3->size[0];
  c9_P->size[1] = c9_r3->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_P, c9_i114, &c9_sb_emlrtRTEI);
  c9_rb_loop_ub = c9_r3->size[0] * c9_r3->size[1] - 1;
  c9_emxFree_real_T(chartInstance, &c9_c_H);
  for (c9_i115 = 0; c9_i115 <= c9_rb_loop_ub; c9_i115++) {
    c9_P->data[c9_i115] = c9_r3->data[c9_i115];
  }

  c9_emxFree_real_T(chartInstance, &c9_r3);
  c9_emxInit_boolean_T(chartInstance, &c9_c_BW, 2, &c9_tb_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 39);
  c9_i116 = c9_c_BW->size[0] * c9_c_BW->size[1];
  c9_c_BW->size[0] = c9_BW->size[0];
  c9_c_BW->size[1] = c9_BW->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_c_BW, c9_i116,
    &c9_tb_emlrtRTEI);
  c9_sb_loop_ub = c9_BW->size[0] * c9_BW->size[1] - 1;
  for (c9_i117 = 0; c9_i117 <= c9_sb_loop_ub; c9_i117++) {
    c9_c_BW->data[c9_i117] = c9_BW->data[c9_i117];
  }

  c9_emxInit_real_T(chartInstance, &c9_c_theta, 2, &c9_ub_emlrtRTEI);
  c9_i118 = c9_c_theta->size[0] * c9_c_theta->size[1];
  c9_c_theta->size[0] = 1;
  c9_c_theta->size[1] = c9_theta->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_c_theta, c9_i118,
    &c9_ub_emlrtRTEI);
  c9_tb_loop_ub = c9_theta->size[0] * c9_theta->size[1] - 1;
  for (c9_i119 = 0; c9_i119 <= c9_tb_loop_ub; c9_i119++) {
    c9_c_theta->data[c9_i119] = c9_theta->data[c9_i119];
  }

  c9_emxInit_real_T(chartInstance, &c9_c_rho, 2, &c9_vb_emlrtRTEI);
  c9_i120 = c9_c_rho->size[0] * c9_c_rho->size[1];
  c9_c_rho->size[0] = 1;
  c9_c_rho->size[1] = c9_rho->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_c_rho, c9_i120, &c9_vb_emlrtRTEI);
  c9_ub_loop_ub = c9_rho->size[0] * c9_rho->size[1] - 1;
  for (c9_i121 = 0; c9_i121 <= c9_ub_loop_ub; c9_i121++) {
    c9_c_rho->data[c9_i121] = c9_rho->data[c9_i121];
  }

  c9_emxInit_real_T(chartInstance, &c9_b_P, 2, &c9_wb_emlrtRTEI);
  c9_i122 = c9_b_P->size[0] * c9_b_P->size[1];
  c9_b_P->size[0] = c9_P->size[0];
  c9_b_P->size[1] = c9_P->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_P, c9_i122, &c9_wb_emlrtRTEI);
  c9_vb_loop_ub = c9_P->size[0] * c9_P->size[1] - 1;
  for (c9_i123 = 0; c9_i123 <= c9_vb_loop_ub; c9_i123++) {
    c9_b_P->data[c9_i123] = c9_P->data[c9_i123];
  }

  c9_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_r4, 2, &c9_jc_emlrtRTEI);
  c9_houghlines(chartInstance, c9_c_BW, c9_c_theta, c9_c_rho, c9_b_P, c9_r4);
  c9_i124 = c9_lines->size[0] * c9_lines->size[1];
  c9_lines->size[0] = 1;
  c9_lines->size[1] = c9_r4->size[1];
  c9_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c9_lines, c9_i124,
    &c9_xb_emlrtRTEI);
  c9_wb_loop_ub = c9_r4->size[0] * c9_r4->size[1] - 1;
  c9_emxFree_real_T(chartInstance, &c9_b_P);
  c9_emxFree_real_T(chartInstance, &c9_c_rho);
  c9_emxFree_real_T(chartInstance, &c9_c_theta);
  c9_emxFree_boolean_T(chartInstance, &c9_c_BW);
  for (c9_i125 = 0; c9_i125 <= c9_wb_loop_ub; c9_i125++) {
    c9_lines->data[c9_i125] = c9_r4->data[c9_i125];
  }

  c9_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_r4);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 43);
  c9_e_varargin_2 = c9_lines->size[1];
  c9_f_varargin_2 = c9_e_varargin_2;
  c9_i_n = (real_T)c9_f_varargin_2;
  c9_i126 = c9_orientationsOfLines->size[0];
  c9_b_u = c9_i_n;
  if (c9_b_u < 0.0) {
    c9_q_y = c9_b_u;
  } else {
    c9_q_y = c9_b_u;
  }

  c9_orientationsOfLines->size[0] = c9__s32_d_(chartInstance, c9_q_y, 1U, 1813,
    42);
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_orientationsOfLines, c9_i126,
    &c9_yb_emlrtRTEI);
  c9_c_u = c9_i_n;
  if (c9_c_u < 0.0) {
    c9_r_y = c9_c_u;
  } else {
    c9_r_y = c9_c_u;
  }

  c9_xb_loop_ub = c9__s32_d_(chartInstance, c9_r_y, 1U, 1813, 42) - 1;
  for (c9_i127 = 0; c9_i127 <= c9_xb_loop_ub; c9_i127++) {
    c9_orientationsOfLines->data[c9_i127] = 0.0;
  }

  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 44);
  c9_g_varargin_2 = c9_lines->size[1];
  c9_h_varargin_2 = c9_g_varargin_2;
  c9_d0 = (real_T)c9_h_varargin_2;
  c9_i128 = (int32_T)c9_d0 - 1;
  c9_k = 1.0;
  c9_k_k = 0;
  while (c9_k_k <= c9_i128) {
    c9_k = 1.0 + (real_T)c9_k_k;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 45);
    c9_orientationsOfLines->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1885, 22, MAX_uint32_T,
       (int32_T)c9_k, 1, c9_orientationsOfLines->size[0]) - 1] = c9_lines->
      data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 1908, 8, MAX_uint32_T, (int32_T)c9_k, 1,
      c9_lines->size[1]) - 1].theta;
    c9_k_k++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 48);
  c9_i129 = c9_cost->size[0];
  c9_cost->size[0] = c9_orientationsOfLines->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_cost, c9_i129, &c9_ac_emlrtRTEI);
  c9_yb_loop_ub = c9_orientationsOfLines->size[0] - 1;
  for (c9_i130 = 0; c9_i130 <= c9_yb_loop_ub; c9_i130++) {
    c9_cost->data[c9_i130] = c9_orientationsOfLines->data[c9_i130];
  }

  if ((c9_cost->size[0] == 1) || ((real_T)c9_cost->size[0] != 1.0)) {
    c9_b3 = true;
  } else {
    c9_b3 = false;
  }

  if (c9_b3) {
  } else {
    c9_s_y = NULL;
    sf_mex_assign(&c9_s_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c9_t_y = NULL;
    sf_mex_assign(&c9_t_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_s_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_t_y)));
  }

  if (c9_cost->size[0] == 0) {
    c9_rotAngle = rtNaN;
  } else {
    c9_b_sort(chartInstance, c9_cost);
    c9_M = c9_cost->data[0];
    c9_F = 1;
    c9_mtmp = c9_M;
    c9_ftmp = 1;
    c9_d2 = (real_T)c9_cost->size[0];
    c9_i131 = (int32_T)c9_d2 - 2;
    for (c9_l_k = 0; c9_l_k <= c9_i131; c9_l_k++) {
      c9_m_k = 2.0 + (real_T)c9_l_k;
      if (c9_cost->data[(int32_T)c9_m_k - 1] == c9_mtmp) {
        c9_w_a = c9_ftmp + 1;
        c9_ftmp = c9_w_a;
      } else {
        if (c9_ftmp > c9_F) {
          c9_M = c9_mtmp;
          c9_F = c9_ftmp;
        }

        c9_mtmp = c9_cost->data[(int32_T)c9_m_k - 1];
        c9_ftmp = 1;
      }
    }

    if (c9_ftmp > c9_F) {
      c9_M = c9_mtmp;
    }

    c9_rotAngle = c9_M;
  }

  c9_emxFree_real_T(chartInstance, &c9_cost);
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 51);
  c9_d1 = -c9_rotAngle;
  c9_b_cosd(chartInstance, &c9_d1);
  c9_d3 = -c9_rotAngle;
  c9_b_sind(chartInstance, &c9_d3);
  c9_d4 = -c9_rotAngle;
  c9_b_sind(chartInstance, &c9_d4);
  c9_d5 = -c9_rotAngle;
  c9_b_cosd(chartInstance, &c9_d5);
  c9_b_rotationMatrix[0] = c9_d1;
  c9_b_rotationMatrix[2] = c9_d3;
  c9_b_rotationMatrix[1] = -c9_d4;
  c9_b_rotationMatrix[3] = c9_d5;
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, 54);
  c9_d6 = c9_rotAngle;
  c9_b_cosd(chartInstance, &c9_d6);
  c9_d7 = c9_rotAngle;
  c9_b_sind(chartInstance, &c9_d7);
  c9_d8 = c9_rotAngle;
  c9_b_sind(chartInstance, &c9_d8);
  c9_d9 = c9_rotAngle;
  c9_b_cosd(chartInstance, &c9_d9);
  c9_b_invRotationMatrix[0] = c9_d6;
  c9_b_invRotationMatrix[2] = c9_d7;
  c9_b_invRotationMatrix[1] = -c9_d8;
  c9_b_invRotationMatrix[3] = c9_d9;
  _SFD_EML_CALL(0U, chartInstance->c9_sfEvent, -54);
  _SFD_SYMBOL_SCOPE_POP();
  for (c9_i132 = 0; c9_i132 < 4; c9_i132++) {
    (*chartInstance->c9_rotationMatrix)[c9_i132] = c9_b_rotationMatrix[c9_i132];
  }

  for (c9_i133 = 0; c9_i133 < 4; c9_i133++) {
    (*chartInstance->c9_invRotationMatrix)[c9_i133] =
      c9_b_invRotationMatrix[c9_i133];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
  c9_emxFree_real_T(chartInstance, &c9_orientationsOfLines);
  c9_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_lines);
  c9_emxFree_real_T(chartInstance, &c9_P);
  c9_emxFree_real_T(chartInstance, &c9_rho);
  c9_emxFree_real_T(chartInstance, &c9_theta);
  c9_emxFree_real_T(chartInstance, &c9_H);
  c9_emxFree_boolean_T(chartInstance, &c9_BW);
  c9_emxFree_real_T(chartInstance, &c9_y);
  c9_emxFree_real_T(chartInstance, &c9_x);
  c9_emxFree_real_T(chartInstance, &c9_pictureMatrix);
  c9_emxFree_real_T(chartInstance, &c9_obstaclesInRegionOfInterestTemp);
}

static void initSimStructsc9_LIDAR_sim(SFc9_LIDAR_simInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c9_machineNumber, uint32_T
  c9_chartNumber, uint32_T c9_instanceNumber)
{
  (void)(c9_machineNumber);
  (void)(c9_chartNumber);
  (void)(c9_instanceNumber);
}

static const mxArray *c9_sf_marshallOut(void *chartInstanceVoid, void *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  real_T c9_b_u;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_b_u = *(real_T *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static real_T c9_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId)
{
  real_T c9_y;
  real_T c9_d10;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_b_u), &c9_d10, 1, 0, 0U, 0, 0U, 0);
  c9_y = c9_d10;
  sf_mex_destroy(&c9_b_u);
  return c9_y;
}

static void c9_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_rotAngle;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  real_T c9_y;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_rotAngle = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_y = c9_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_rotAngle), &c9_thisId);
  sf_mex_destroy(&c9_rotAngle);
  *(real_T *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_b_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_real_T *c9_b_u;
  int32_T c9_i134;
  int32_T c9_i135;
  int32_T c9_i136;
  int32_T c9_loop_ub;
  int32_T c9_i137;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i134 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = c9_inData->size[0];
  c9_b_u->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_u, c9_i134, (emlrtRTEInfo *)
    NULL);
  c9_i135 = c9_b_u->size[0];
  c9_i136 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i137 = 0; c9_i137 <= c9_loop_ub; c9_i137++) {
    c9_b_u->data[c9_i137] = c9_inData->data[c9_i137];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 0, 0U, 1U, 0U, 2,
    c9_b_u->size[0], c9_b_u->size[1]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_real_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_b_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y)
{
  c9_emxArray_real_T *c9_r5;
  int32_T c9_i138;
  int32_T c9_i139;
  uint32_T c9_uv0[2];
  int32_T c9_i140;
  boolean_T c9_bv0[2];
  static boolean_T c9_bv1[2] = { true, false };

  int32_T c9_i141;
  int32_T c9_i142;
  int32_T c9_i143;
  int32_T c9_loop_ub;
  int32_T c9_i144;
  c9_emxInit_real_T(chartInstance, &c9_r5, 2, (emlrtRTEInfo *)NULL);
  for (c9_i138 = 0; c9_i138 < 2; c9_i138++) {
    c9_uv0[c9_i138] = 50000U + (uint32_T)(-49998 * c9_i138);
  }

  c9_i139 = c9_r5->size[0] * c9_r5->size[1];
  c9_r5->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_r5->size[1] = sf_mex_get_dimension(c9_b_u, 1);
  c9_emxEnsureCapacity_real_T(chartInstance, c9_r5, c9_i139, (emlrtRTEInfo *)
    NULL);
  for (c9_i140 = 0; c9_i140 < 2; c9_i140++) {
    c9_bv0[c9_i140] = c9_bv1[c9_i140];
  }

  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r5->data, 1, 0, 0U, 1, 0U,
                   2, c9_bv0, c9_uv0, c9_r5->size);
  c9_i141 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = c9_r5->size[0];
  c9_y->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_y, c9_i141, (emlrtRTEInfo *)NULL);
  c9_i142 = c9_y->size[0];
  c9_i143 = c9_y->size[1];
  c9_loop_ub = c9_r5->size[0] * c9_r5->size[1] - 1;
  for (c9_i144 = 0; c9_i144 <= c9_loop_ub; c9_i144++) {
    c9_y->data[c9_i144] = c9_r5->data[c9_i144];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_real_T(chartInstance, &c9_r5);
}

static void c9_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData)
{
  c9_emxArray_real_T *c9_y;
  const mxArray *c9_obstaclesInRegionOfInterestTemp;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i145;
  int32_T c9_i146;
  int32_T c9_loop_ub;
  int32_T c9_i147;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_y, 2, (emlrtRTEInfo *)NULL);
  c9_obstaclesInRegionOfInterestTemp = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_b_emlrt_marshallIn(chartInstance, sf_mex_dup
                        (c9_obstaclesInRegionOfInterestTemp), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_obstaclesInRegionOfInterestTemp);
  c9_i145 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = c9_y->size[0];
  c9_outData->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_outData, c9_i145, (emlrtRTEInfo *)
    NULL);
  for (c9_i146 = 0; c9_i146 < 2; c9_i146++) {
    c9_loop_ub = c9_y->size[0] - 1;
    for (c9_i147 = 0; c9_i147 <= c9_loop_ub; c9_i147++) {
      c9_outData->data[c9_i147 + c9_outData->size[0] * c9_i146] = c9_y->
        data[c9_i147 + c9_y->size[0] * c9_i146];
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_c_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_real_T *c9_b_u;
  int32_T c9_i148;
  int32_T c9_i149;
  int32_T c9_i150;
  int32_T c9_loop_ub;
  int32_T c9_i151;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i148 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = 1;
  c9_b_u->size[1] = c9_inData->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_u, c9_i148, (emlrtRTEInfo *)
    NULL);
  c9_i149 = c9_b_u->size[0];
  c9_i150 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i151 = 0; c9_i151 <= c9_loop_ub; c9_i151++) {
    c9_b_u->data[c9_i151] = c9_inData->data[c9_i151];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 0, 0U, 1U, 0U, 2,
    c9_b_u->size[0], c9_b_u->size[1]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_real_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_c_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y)
{
  c9_emxArray_real_T *c9_r6;
  int32_T c9_i152;
  int32_T c9_i153;
  uint32_T c9_uv1[2];
  int32_T c9_i154;
  boolean_T c9_bv2[2];
  static boolean_T c9_bv3[2] = { false, true };

  int32_T c9_i155;
  int32_T c9_i156;
  int32_T c9_i157;
  int32_T c9_loop_ub;
  int32_T c9_i158;
  c9_emxInit_real_T(chartInstance, &c9_r6, 2, (emlrtRTEInfo *)NULL);
  for (c9_i152 = 0; c9_i152 < 2; c9_i152++) {
    c9_uv1[c9_i152] = 1U + 49999U * (uint32_T)c9_i152;
  }

  c9_i153 = c9_r6->size[0] * c9_r6->size[1];
  c9_r6->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_r6->size[1] = sf_mex_get_dimension(c9_b_u, 1);
  c9_emxEnsureCapacity_real_T(chartInstance, c9_r6, c9_i153, (emlrtRTEInfo *)
    NULL);
  for (c9_i154 = 0; c9_i154 < 2; c9_i154++) {
    c9_bv2[c9_i154] = c9_bv3[c9_i154];
  }

  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r6->data, 1, 0, 0U, 1, 0U,
                   2, c9_bv2, c9_uv1, c9_r6->size);
  c9_i155 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = 1;
  c9_y->size[1] = c9_r6->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_y, c9_i155, (emlrtRTEInfo *)NULL);
  c9_i156 = c9_y->size[0];
  c9_i157 = c9_y->size[1];
  c9_loop_ub = c9_r6->size[0] * c9_r6->size[1] - 1;
  for (c9_i158 = 0; c9_i158 <= c9_loop_ub; c9_i158++) {
    c9_y->data[c9_i158] = c9_r6->data[c9_i158];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_real_T(chartInstance, &c9_r6);
}

static void c9_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData)
{
  c9_emxArray_real_T *c9_y;
  const mxArray *c9_x;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i159;
  int32_T c9_loop_ub;
  int32_T c9_i160;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_y, 2, (emlrtRTEInfo *)NULL);
  c9_x = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_x), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_x);
  c9_i159 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = 1;
  c9_outData->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_outData, c9_i159, (emlrtRTEInfo *)
    NULL);
  c9_loop_ub = c9_y->size[1] - 1;
  for (c9_i160 = 0; c9_i160 <= c9_loop_ub; c9_i160++) {
    c9_outData->data[c9_i160] = c9_y->data[c9_i160];
  }

  c9_emxFree_real_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_d_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_boolean_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_boolean_T *c9_b_u;
  int32_T c9_i161;
  int32_T c9_i162;
  int32_T c9_i163;
  int32_T c9_loop_ub;
  int32_T c9_i164;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_boolean_T(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i161 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = c9_inData->size[0];
  c9_b_u->size[1] = c9_inData->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_b_u, c9_i161, (emlrtRTEInfo *)
    NULL);
  c9_i162 = c9_b_u->size[0];
  c9_i163 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i164 = 0; c9_i164 <= c9_loop_ub; c9_i164++) {
    c9_b_u->data[c9_i164] = c9_inData->data[c9_i164];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 11, 0U, 1U, 0U, 2,
    c9_b_u->size[0], c9_b_u->size[1]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_boolean_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_d_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_boolean_T *c9_y)
{
  c9_emxArray_boolean_T *c9_r7;
  int32_T c9_i165;
  int32_T c9_i166;
  uint32_T c9_uv2[2];
  int32_T c9_i167;
  boolean_T c9_bv4[2];
  int32_T c9_i168;
  int32_T c9_i169;
  int32_T c9_i170;
  int32_T c9_loop_ub;
  int32_T c9_i171;
  c9_emxInit_boolean_T(chartInstance, &c9_r7, 2, (emlrtRTEInfo *)NULL);
  for (c9_i165 = 0; c9_i165 < 2; c9_i165++) {
    c9_uv2[c9_i165] = MAX_uint32_T;
  }

  c9_i166 = c9_r7->size[0] * c9_r7->size[1];
  c9_r7->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_r7->size[1] = sf_mex_get_dimension(c9_b_u, 1);
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_r7, c9_i166, (emlrtRTEInfo *)
    NULL);
  for (c9_i167 = 0; c9_i167 < 2; c9_i167++) {
    c9_bv4[c9_i167] = true;
  }

  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r7->data, 1, 11, 0U, 1,
                   0U, 2, c9_bv4, c9_uv2, c9_r7->size);
  c9_i168 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = c9_r7->size[0];
  c9_y->size[1] = c9_r7->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_y, c9_i168, (emlrtRTEInfo *)
    NULL);
  c9_i169 = c9_y->size[0];
  c9_i170 = c9_y->size[1];
  c9_loop_ub = c9_r7->size[0] * c9_r7->size[1] - 1;
  for (c9_i171 = 0; c9_i171 <= c9_loop_ub; c9_i171++) {
    c9_y->data[c9_i171] = c9_r7->data[c9_i171];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_boolean_T(chartInstance, &c9_r7);
}

static void c9_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_boolean_T *c9_outData)
{
  c9_emxArray_boolean_T *c9_y;
  const mxArray *c9_BW;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i172;
  int32_T c9_loop_ub;
  int32_T c9_i173;
  int32_T c9_b_loop_ub;
  int32_T c9_i174;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_boolean_T(chartInstance, &c9_y, 2, (emlrtRTEInfo *)NULL);
  c9_BW = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_BW), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_BW);
  c9_i172 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = c9_y->size[0];
  c9_outData->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_outData, c9_i172,
    (emlrtRTEInfo *)NULL);
  c9_loop_ub = c9_y->size[1] - 1;
  for (c9_i173 = 0; c9_i173 <= c9_loop_ub; c9_i173++) {
    c9_b_loop_ub = c9_y->size[0] - 1;
    for (c9_i174 = 0; c9_i174 <= c9_b_loop_ub; c9_i174++) {
      c9_outData->data[c9_i174 + c9_outData->size[0] * c9_i173] = c9_y->
        data[c9_i174 + c9_y->size[0] * c9_i173];
    }
  }

  c9_emxFree_boolean_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_e_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_real_T *c9_b_u;
  int32_T c9_i175;
  int32_T c9_i176;
  int32_T c9_i177;
  int32_T c9_loop_ub;
  int32_T c9_i178;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i175 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = c9_inData->size[0];
  c9_b_u->size[1] = c9_inData->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_u, c9_i175, (emlrtRTEInfo *)
    NULL);
  c9_i176 = c9_b_u->size[0];
  c9_i177 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i178 = 0; c9_i178 <= c9_loop_ub; c9_i178++) {
    c9_b_u->data[c9_i178] = c9_inData->data[c9_i178];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 0, 0U, 1U, 0U, 2,
    c9_b_u->size[0], c9_b_u->size[1]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_real_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_e_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y)
{
  c9_emxArray_real_T *c9_r8;
  int32_T c9_i179;
  int32_T c9_i180;
  uint32_T c9_uv3[2];
  int32_T c9_i181;
  boolean_T c9_bv5[2];
  int32_T c9_i182;
  int32_T c9_i183;
  int32_T c9_i184;
  int32_T c9_loop_ub;
  int32_T c9_i185;
  c9_emxInit_real_T(chartInstance, &c9_r8, 2, (emlrtRTEInfo *)NULL);
  for (c9_i179 = 0; c9_i179 < 2; c9_i179++) {
    c9_uv3[c9_i179] = MAX_uint32_T;
  }

  c9_i180 = c9_r8->size[0] * c9_r8->size[1];
  c9_r8->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_r8->size[1] = sf_mex_get_dimension(c9_b_u, 1);
  c9_emxEnsureCapacity_real_T(chartInstance, c9_r8, c9_i180, (emlrtRTEInfo *)
    NULL);
  for (c9_i181 = 0; c9_i181 < 2; c9_i181++) {
    c9_bv5[c9_i181] = true;
  }

  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r8->data, 1, 0, 0U, 1, 0U,
                   2, c9_bv5, c9_uv3, c9_r8->size);
  c9_i182 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = c9_r8->size[0];
  c9_y->size[1] = c9_r8->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_y, c9_i182, (emlrtRTEInfo *)NULL);
  c9_i183 = c9_y->size[0];
  c9_i184 = c9_y->size[1];
  c9_loop_ub = c9_r8->size[0] * c9_r8->size[1] - 1;
  for (c9_i185 = 0; c9_i185 <= c9_loop_ub; c9_i185++) {
    c9_y->data[c9_i185] = c9_r8->data[c9_i185];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_real_T(chartInstance, &c9_r8);
}

static void c9_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData)
{
  c9_emxArray_real_T *c9_y;
  const mxArray *c9_H;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i186;
  int32_T c9_loop_ub;
  int32_T c9_i187;
  int32_T c9_b_loop_ub;
  int32_T c9_i188;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_y, 2, (emlrtRTEInfo *)NULL);
  c9_H = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_H), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_H);
  c9_i186 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = c9_y->size[0];
  c9_outData->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_outData, c9_i186, (emlrtRTEInfo *)
    NULL);
  c9_loop_ub = c9_y->size[1] - 1;
  for (c9_i187 = 0; c9_i187 <= c9_loop_ub; c9_i187++) {
    c9_b_loop_ub = c9_y->size[0] - 1;
    for (c9_i188 = 0; c9_i188 <= c9_b_loop_ub; c9_i188++) {
      c9_outData->data[c9_i188 + c9_outData->size[0] * c9_i187] = c9_y->
        data[c9_i188 + c9_y->size[0] * c9_i187];
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_f_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_real_T *c9_b_u;
  int32_T c9_i189;
  int32_T c9_i190;
  int32_T c9_i191;
  int32_T c9_loop_ub;
  int32_T c9_i192;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i189 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = 1;
  c9_b_u->size[1] = c9_inData->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_u, c9_i189, (emlrtRTEInfo *)
    NULL);
  c9_i190 = c9_b_u->size[0];
  c9_i191 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i192 = 0; c9_i192 <= c9_loop_ub; c9_i192++) {
    c9_b_u->data[c9_i192] = c9_inData->data[c9_i192];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 0, 0U, 1U, 0U, 2,
    c9_b_u->size[0], c9_b_u->size[1]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_real_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_f_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y)
{
  c9_emxArray_real_T *c9_r9;
  int32_T c9_i193;
  int32_T c9_i194;
  uint32_T c9_uv4[2];
  int32_T c9_i195;
  boolean_T c9_bv6[2];
  static boolean_T c9_bv7[2] = { false, true };

  int32_T c9_i196;
  int32_T c9_i197;
  int32_T c9_i198;
  int32_T c9_loop_ub;
  int32_T c9_i199;
  c9_emxInit_real_T(chartInstance, &c9_r9, 2, (emlrtRTEInfo *)NULL);
  for (c9_i193 = 0; c9_i193 < 2; c9_i193++) {
    c9_uv4[c9_i193] = 1U + 4294967294U * (uint32_T)c9_i193;
  }

  c9_i194 = c9_r9->size[0] * c9_r9->size[1];
  c9_r9->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_r9->size[1] = sf_mex_get_dimension(c9_b_u, 1);
  c9_emxEnsureCapacity_real_T(chartInstance, c9_r9, c9_i194, (emlrtRTEInfo *)
    NULL);
  for (c9_i195 = 0; c9_i195 < 2; c9_i195++) {
    c9_bv6[c9_i195] = c9_bv7[c9_i195];
  }

  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r9->data, 1, 0, 0U, 1, 0U,
                   2, c9_bv6, c9_uv4, c9_r9->size);
  c9_i196 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = 1;
  c9_y->size[1] = c9_r9->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_y, c9_i196, (emlrtRTEInfo *)NULL);
  c9_i197 = c9_y->size[0];
  c9_i198 = c9_y->size[1];
  c9_loop_ub = c9_r9->size[0] * c9_r9->size[1] - 1;
  for (c9_i199 = 0; c9_i199 <= c9_loop_ub; c9_i199++) {
    c9_y->data[c9_i199] = c9_r9->data[c9_i199];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_real_T(chartInstance, &c9_r9);
}

static void c9_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData)
{
  c9_emxArray_real_T *c9_y;
  const mxArray *c9_theta;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i200;
  int32_T c9_loop_ub;
  int32_T c9_i201;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_y, 2, (emlrtRTEInfo *)NULL);
  c9_theta = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_theta), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_theta);
  c9_i200 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = 1;
  c9_outData->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_outData, c9_i200, (emlrtRTEInfo *)
    NULL);
  c9_loop_ub = c9_y->size[1] - 1;
  for (c9_i201 = 0; c9_i201 <= c9_loop_ub; c9_i201++) {
    c9_outData->data[c9_i201] = c9_y->data[c9_i201];
  }

  c9_emxFree_real_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_g_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_real_T *c9_b_u;
  int32_T c9_i202;
  int32_T c9_i203;
  int32_T c9_i204;
  int32_T c9_loop_ub;
  int32_T c9_i205;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i202 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = c9_inData->size[0];
  c9_b_u->size[1] = c9_inData->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_u, c9_i202, (emlrtRTEInfo *)
    NULL);
  c9_i203 = c9_b_u->size[0];
  c9_i204 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i205 = 0; c9_i205 <= c9_loop_ub; c9_i205++) {
    c9_b_u->data[c9_i205] = c9_inData->data[c9_i205];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 0, 0U, 1U, 0U, 2,
    c9_b_u->size[0], c9_b_u->size[1]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_real_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_g_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y)
{
  c9_emxArray_real_T *c9_r10;
  int32_T c9_i206;
  int32_T c9_i207;
  uint32_T c9_uv5[2];
  static uint32_T c9_uv6[2] = { MAX_uint32_T, 2U };

  int32_T c9_i208;
  boolean_T c9_bv8[2];
  int32_T c9_i209;
  int32_T c9_i210;
  int32_T c9_i211;
  int32_T c9_loop_ub;
  int32_T c9_i212;
  c9_emxInit_real_T(chartInstance, &c9_r10, 2, (emlrtRTEInfo *)NULL);
  for (c9_i206 = 0; c9_i206 < 2; c9_i206++) {
    c9_uv5[c9_i206] = c9_uv6[c9_i206];
  }

  c9_i207 = c9_r10->size[0] * c9_r10->size[1];
  c9_r10->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_r10->size[1] = sf_mex_get_dimension(c9_b_u, 1);
  c9_emxEnsureCapacity_real_T(chartInstance, c9_r10, c9_i207, (emlrtRTEInfo *)
    NULL);
  for (c9_i208 = 0; c9_i208 < 2; c9_i208++) {
    c9_bv8[c9_i208] = true;
  }

  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r10->data, 1, 0, 0U, 1,
                   0U, 2, c9_bv8, c9_uv5, c9_r10->size);
  c9_i209 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = c9_r10->size[0];
  c9_y->size[1] = c9_r10->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_y, c9_i209, (emlrtRTEInfo *)NULL);
  c9_i210 = c9_y->size[0];
  c9_i211 = c9_y->size[1];
  c9_loop_ub = c9_r10->size[0] * c9_r10->size[1] - 1;
  for (c9_i212 = 0; c9_i212 <= c9_loop_ub; c9_i212++) {
    c9_y->data[c9_i212] = c9_r10->data[c9_i212];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_real_T(chartInstance, &c9_r10);
}

static void c9_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData)
{
  c9_emxArray_real_T *c9_y;
  const mxArray *c9_P;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i213;
  int32_T c9_loop_ub;
  int32_T c9_i214;
  int32_T c9_b_loop_ub;
  int32_T c9_i215;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T(chartInstance, &c9_y, 2, (emlrtRTEInfo *)NULL);
  c9_P = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_P), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_P);
  c9_i213 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = c9_y->size[0];
  c9_outData->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_outData, c9_i213, (emlrtRTEInfo *)
    NULL);
  c9_loop_ub = c9_y->size[1] - 1;
  for (c9_i214 = 0; c9_i214 <= c9_loop_ub; c9_i214++) {
    c9_b_loop_ub = c9_y->size[0] - 1;
    for (c9_i215 = 0; c9_i215 <= c9_b_loop_ub; c9_i215++) {
      c9_outData->data[c9_i215 + c9_outData->size[0] * c9_i214] = c9_y->
        data[c9_i215 + c9_y->size[0] * c9_i214];
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_h_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_b_u;
  int32_T c9_i216;
  int32_T c9_i217;
  int32_T c9_i218;
  int32_T c9_loop_ub;
  int32_T c9_i219;
  const mxArray *c9_y = NULL;
  int32_T c9_i220;
  int32_T c9_iv1[2];
  static const char * c9_sv0[4] = { "point1", "point2", "theta", "rho" };

  int32_T c9_i;
  int32_T c9_j1;
  int32_T c9_i221;
  const mxArray *c9_b_y = NULL;
  real_T c9_c_u[2];
  int32_T c9_i222;
  const mxArray *c9_c_y = NULL;
  real_T c9_d_u;
  const mxArray *c9_d_y = NULL;
  real_T c9_e_u;
  const mxArray *c9_e_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_b_u, 2, (emlrtRTEInfo *)
    NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i216 = c9_b_u->size[0] * c9_b_u->size[1];
  c9_b_u->size[0] = 1;
  c9_b_u->size[1] = c9_inData->size[1];
  c9_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c9_b_u, c9_i216,
    (emlrtRTEInfo *)NULL);
  c9_i217 = c9_b_u->size[0];
  c9_i218 = c9_b_u->size[1];
  c9_loop_ub = c9_inData->size[0] * c9_inData->size[1] - 1;
  for (c9_i219 = 0; c9_i219 <= c9_loop_ub; c9_i219++) {
    c9_b_u->data[c9_i219] = c9_inData->data[c9_i219];
  }

  c9_y = NULL;
  for (c9_i220 = 0; c9_i220 < 2; c9_i220++) {
    c9_iv1[c9_i220] = c9_b_u->size[c9_i220];
  }

  sf_mex_assign(&c9_y, sf_mex_createstructarray("structure", 2, c9_iv1, 4,
    c9_sv0), false);
  sf_mex_createfield(c9_y, "point1", "point1");
  sf_mex_createfield(c9_y, "point2", "point2");
  sf_mex_createfield(c9_y, "theta", "theta");
  sf_mex_createfield(c9_y, "rho", "rho");
  c9_i = 0;
  for (c9_j1 = 0; c9_j1 < c9_b_u->size[1U]; c9_j1++) {
    for (c9_i221 = 0; c9_i221 < 2; c9_i221++) {
      c9_c_u[c9_i221] = c9_b_u->data[c9_j1].point1[c9_i221];
    }

    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_c_u, 0, 0U, 1U, 0U, 2, 1, 2),
                  false);
    sf_mex_setfieldbynum(c9_y, c9_i, "point1", c9_b_y, 0);
    for (c9_i222 = 0; c9_i222 < 2; c9_i222++) {
      c9_c_u[c9_i222] = c9_b_u->data[c9_j1].point2[c9_i222];
    }

    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_c_u, 0, 0U, 1U, 0U, 2, 1, 2),
                  false);
    sf_mex_setfieldbynum(c9_y, c9_i, "point2", c9_c_y, 1);
    c9_d_u = c9_b_u->data[c9_j1].theta;
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", &c9_d_u, 0, 0U, 0U, 0U, 0), false);
    sf_mex_setfieldbynum(c9_y, c9_i, "theta", c9_d_y, 2);
    c9_e_u = c9_b_u->data[c9_j1].rho;
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", &c9_e_u, 0, 0U, 0U, 0U, 0), false);
    sf_mex_setfieldbynum(c9_y, c9_i, "rho", c9_e_y, 3);
    c9_i++;
  }

  c9_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_b_u);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static void c9_h_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_y)
{
  int32_T c9_i223;
  int32_T c9_i224;
  uint32_T c9_uv7[2];
  emlrtMsgIdentifier c9_thisId;
  uint32_T c9_uv8[2];
  int32_T c9_i225;
  static const char * c9_fieldNames[4] = { "point1", "point2", "theta", "rho" };

  boolean_T c9_bv9[2];
  static boolean_T c9_bv10[2] = { false, true };

  int32_T c9_i226;
  int32_T c9_n;
  int32_T c9_i227;
  int32_T c9_b_n[1];
  int32_T c9_c_n[1];
  for (c9_i223 = 0; c9_i223 < 2; c9_i223++) {
    c9_uv7[c9_i223] = 1U + 4294967294U * (uint32_T)c9_i223;
  }

  for (c9_i224 = 0; c9_i224 < 2; c9_i224++) {
    c9_uv8[c9_i224] = 1U + 4294967294U * (uint32_T)c9_i224;
  }

  c9_thisId.fParent = c9_parentId;
  c9_thisId.bParentIsCell = false;
  for (c9_i225 = 0; c9_i225 < 2; c9_i225++) {
    c9_bv9[c9_i225] = c9_bv10[c9_i225];
  }

  sf_mex_check_struct_vs(c9_parentId, c9_b_u, 4, c9_fieldNames, 2U, c9_bv9,
    c9_uv7, c9_uv8);
  c9_i226 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = (int32_T)c9_uv8[0];
  c9_y->size[1] = (int32_T)c9_uv8[1];
  c9_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c9_y, c9_i226, (emlrtRTEInfo
    *)NULL);
  c9_n = c9_y->size[1];
  for (c9_i227 = 0; c9_i227 < c9_n; c9_i227++) {
    c9_thisId.fIdentifier = "point1";
    c9_b_n[0] = c9_n;
    c9_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c9_b_u,
      "point1", "point1", c9_i227)), &c9_thisId, c9_y->data[c9_i227].point1);
    c9_thisId.fIdentifier = "point2";
    c9_c_n[0] = c9_n;
    c9_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c9_b_u,
      "point2", "point2", c9_i227)), &c9_thisId, c9_y->data[c9_i227].point2);
    c9_thisId.fIdentifier = "theta";
    c9_y->data[c9_i227].theta = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_b_u, "theta", "theta", c9_i227)), &c9_thisId);
    c9_thisId.fIdentifier = "rho";
    c9_y->data[c9_i227].rho = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c9_b_u, "rho", "rho", c9_i227)), &c9_thisId);
  }

  sf_mex_destroy(&c9_b_u);
}

static void c9_i_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId, real_T c9_y[2])
{
  real_T c9_dv2[2];
  int32_T c9_i228;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_b_u), c9_dv2, 1, 0, 0U, 1, 0U, 2, 1,
                2);
  for (c9_i228 = 0; c9_i228 < 2; c9_i228++) {
    c9_y[c9_i228] = c9_dv2[c9_i228];
  }

  sf_mex_destroy(&c9_b_u);
}

static void c9_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_outData)
{
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_y;
  const mxArray *c9_lines;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i229;
  int32_T c9_loop_ub;
  int32_T c9_i230;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_y, 2, (emlrtRTEInfo *)
    NULL);
  c9_lines = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_lines), &c9_thisId, c9_y);
  sf_mex_destroy(&c9_lines);
  c9_i229 = c9_outData->size[0] * c9_outData->size[1];
  c9_outData->size[0] = 1;
  c9_outData->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c9_outData, c9_i229,
    (emlrtRTEInfo *)NULL);
  c9_loop_ub = c9_y->size[1] - 1;
  for (c9_i230 = 0; c9_i230 <= c9_loop_ub; c9_i230++) {
    c9_outData->data[c9_i230] = c9_y->data[c9_i230];
  }

  c9_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_i_sf_marshallOut(void *chartInstanceVoid,
  c9_emxArray_real_T *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  c9_emxArray_real_T *c9_b_u;
  int32_T c9_i231;
  int32_T c9_loop_ub;
  int32_T c9_i232;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T1(chartInstance, &c9_b_u, 1, (emlrtRTEInfo *)NULL);
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i231 = c9_b_u->size[0];
  c9_b_u->size[0] = c9_inData->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_u, c9_i231, (emlrtRTEInfo *)
    NULL);
  c9_loop_ub = c9_inData->size[0] - 1;
  for (c9_i232 = 0; c9_i232 <= c9_loop_ub; c9_i232++) {
    c9_b_u->data[c9_i232] = c9_inData->data[c9_i232];
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u->data, 0, 0U, 1U, 0U, 1,
    c9_b_u->size[0]), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  c9_emxFree_real_T(chartInstance, &c9_b_u);
  return c9_mxArrayOutData;
}

static void c9_j_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId,
  c9_emxArray_real_T *c9_y)
{
  c9_emxArray_real_T *c9_r11;
  uint32_T c9_uv9[1];
  int32_T c9_i233;
  boolean_T c9_bv11[1];
  int32_T c9_i234;
  int32_T c9_loop_ub;
  int32_T c9_i235;
  c9_emxInit_real_T1(chartInstance, &c9_r11, 1, (emlrtRTEInfo *)NULL);
  c9_uv9[0] = MAX_uint32_T;
  c9_i233 = c9_r11->size[0];
  c9_r11->size[0] = sf_mex_get_dimension(c9_b_u, 0);
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_r11, c9_i233, (emlrtRTEInfo *)
    NULL);
  c9_bv11[0] = true;
  sf_mex_import_vs(c9_parentId, sf_mex_dup(c9_b_u), c9_r11->data, 1, 0, 0U, 1,
                   0U, 1, c9_bv11, c9_uv9, c9_r11->size);
  c9_i234 = c9_y->size[0];
  c9_y->size[0] = c9_r11->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_y, c9_i234, (emlrtRTEInfo *)
    NULL);
  c9_loop_ub = c9_r11->size[0] - 1;
  for (c9_i235 = 0; c9_i235 <= c9_loop_ub; c9_i235++) {
    c9_y->data[c9_i235] = c9_r11->data[c9_i235];
  }

  sf_mex_destroy(&c9_b_u);
  c9_emxFree_real_T(chartInstance, &c9_r11);
}

static void c9_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, c9_emxArray_real_T *c9_outData)
{
  c9_emxArray_real_T *c9_y;
  const mxArray *c9_orientationsOfLines;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_i236;
  int32_T c9_loop_ub;
  int32_T c9_i237;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_emxInit_real_T1(chartInstance, &c9_y, 1, (emlrtRTEInfo *)NULL);
  c9_orientationsOfLines = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_orientationsOfLines),
                        &c9_thisId, c9_y);
  sf_mex_destroy(&c9_orientationsOfLines);
  c9_i236 = c9_outData->size[0];
  c9_outData->size[0] = c9_y->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_outData, c9_i236, (emlrtRTEInfo
    *)NULL);
  c9_loop_ub = c9_y->size[0] - 1;
  for (c9_i237 = 0; c9_i237 <= c9_loop_ub; c9_i237++) {
    c9_outData->data[c9_i237] = c9_y->data[c9_i237];
  }

  c9_emxFree_real_T(chartInstance, &c9_y);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_j_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  int32_T c9_i238;
  int32_T c9_i239;
  const mxArray *c9_y = NULL;
  int32_T c9_i240;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i238 = 0;
  for (c9_i239 = 0; c9_i239 < 2; c9_i239++) {
    for (c9_i240 = 0; c9_i240 < 50000; c9_i240++) {
      chartInstance->c9_u[c9_i240 + c9_i238] = (*(real_T (*)[100000])c9_inData)
        [c9_i240 + c9_i238];
    }

    c9_i238 += 50000;
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", chartInstance->c9_u, 0, 0U, 1U, 0U, 2,
    50000, 2), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static const mxArray *c9_k_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  int32_T c9_i241;
  int32_T c9_i242;
  const mxArray *c9_y = NULL;
  int32_T c9_i243;
  real_T c9_b_u[4];
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_i241 = 0;
  for (c9_i242 = 0; c9_i242 < 2; c9_i242++) {
    for (c9_i243 = 0; c9_i243 < 2; c9_i243++) {
      c9_b_u[c9_i243 + c9_i241] = (*(real_T (*)[4])c9_inData)[c9_i243 + c9_i241];
    }

    c9_i241 += 2;
  }

  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u, 0, 0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static void c9_k_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_rotationMatrix, const char_T *c9_identifier, real_T c9_y[4])
{
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_rotationMatrix),
                        &c9_thisId, c9_y);
  sf_mex_destroy(&c9_b_rotationMatrix);
}

static void c9_l_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId, real_T c9_y[4])
{
  real_T c9_dv3[4];
  int32_T c9_i244;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_b_u), c9_dv3, 1, 0, 0U, 1, 0U, 2, 2,
                2);
  for (c9_i244 = 0; c9_i244 < 4; c9_i244++) {
    c9_y[c9_i244] = c9_dv3[c9_i244];
  }

  sf_mex_destroy(&c9_b_u);
}

static void c9_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_b_rotationMatrix;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  real_T c9_y[4];
  int32_T c9_i245;
  int32_T c9_i246;
  int32_T c9_i247;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_b_rotationMatrix = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_rotationMatrix),
                        &c9_thisId, c9_y);
  sf_mex_destroy(&c9_b_rotationMatrix);
  c9_i245 = 0;
  for (c9_i246 = 0; c9_i246 < 2; c9_i246++) {
    for (c9_i247 = 0; c9_i247 < 2; c9_i247++) {
      (*(real_T (*)[4])c9_outData)[c9_i247 + c9_i245] = c9_y[c9_i247 + c9_i245];
    }

    c9_i245 += 2;
  }

  sf_mex_destroy(&c9_mxArrayInData);
}

const mxArray *sf_c9_LIDAR_sim_get_eml_resolved_functions_info(void)
{
  const mxArray *c9_nameCaptureInfo = NULL;
  c9_nameCaptureInfo = NULL;
  sf_mex_assign(&c9_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c9_nameCaptureInfo;
}

static void c9_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, boolean_T c9_idx_data[], int32_T c9_idx_size[1],
  c9_emxArray_real_T *c9_b_x)
{
  int32_T c9_i248;
  int32_T c9_i249;
  int32_T c9_i250;
  int32_T c9_loop_ub;
  int32_T c9_i251;
  int32_T c9_b_idx_size[1];
  int32_T c9_b_loop_ub;
  int32_T c9_i252;
  boolean_T c9_b_idx_data[50000];
  c9_i248 = c9_b_x->size[0] * c9_b_x->size[1];
  c9_b_x->size[0] = c9_x->size[0];
  c9_b_x->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_x, c9_i248, &c9_nc_emlrtRTEI);
  c9_i249 = c9_b_x->size[0];
  c9_i250 = c9_b_x->size[1];
  c9_loop_ub = c9_x->size[0] * c9_x->size[1] - 1;
  for (c9_i251 = 0; c9_i251 <= c9_loop_ub; c9_i251++) {
    c9_b_x->data[c9_i251] = c9_x->data[c9_i251];
  }

  c9_b_idx_size[0] = c9_idx_size[0];
  c9_b_loop_ub = c9_idx_size[0] - 1;
  for (c9_i252 = 0; c9_i252 <= c9_b_loop_ub; c9_i252++) {
    c9_b_idx_data[c9_i252] = c9_idx_data[c9_i252];
  }

  c9_c_nullAssignment(chartInstance, c9_b_x, c9_b_idx_data, c9_b_idx_size);
}

static void c9_check_forloop_overflow_error(SFc9_LIDAR_simInstanceStruct
  *chartInstance, boolean_T c9_overflow)
{
  const mxArray *c9_y = NULL;
  static char_T c9_cv16[34] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'i', 'n', 't', '_', 'f', 'o', 'r', 'l', 'o', 'o', 'p',
    '_', 'o', 'v', 'e', 'r', 'f', 'l', 'o', 'w' };

  const mxArray *c9_b_y = NULL;
  const mxArray *c9_c_y = NULL;
  static char_T c9_cv17[5] = { 'i', 'n', 't', '3', '2' };

  (void)chartInstance;
  (void)c9_overflow;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv16, 10, 0U, 1U, 0U, 2, 1, 34),
                false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv16, 10, 0U, 1U, 0U, 2, 1, 34),
                false);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv17, 10, 0U, 1U, 0U, 2, 1, 5),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
    1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U, 2U,
    14, c9_b_y, 14, c9_c_y)));
}

static void c9_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
                     c9_emxArray_real_T *c9_x, c9_emxArray_real_T *c9_b_x)
{
  int32_T c9_i253;
  int32_T c9_i254;
  int32_T c9_i255;
  int32_T c9_loop_ub;
  int32_T c9_i256;
  c9_i253 = c9_b_x->size[0] * c9_b_x->size[1];
  c9_b_x->size[0] = c9_x->size[0];
  c9_b_x->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_x, c9_i253, &c9_oc_emlrtRTEI);
  c9_i254 = c9_b_x->size[0];
  c9_i255 = c9_b_x->size[1];
  c9_loop_ub = c9_x->size[0] * c9_x->size[1] - 1;
  for (c9_i256 = 0; c9_i256 <= c9_loop_ub; c9_i256++) {
    c9_b_x->data[c9_i256] = c9_x->data[c9_i256];
  }

  c9_c_round(chartInstance, c9_b_x);
}

static boolean_T c9_sortLE(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_v, int32_T c9_dir_data[], int32_T c9_dir_size[2],
  int32_T c9_idx1, int32_T c9_idx2)
{
  boolean_T c9_p;
  int32_T c9_irow1;
  int32_T c9_irow2;
  int32_T c9_k;
  int32_T c9_b_k;
  int32_T c9_colk;
  int32_T c9_x;
  int32_T c9_b_x;
  int32_T c9_c_x;
  int32_T c9_abscolk;
  real_T c9_v1;
  real_T c9_v2;
  boolean_T c9_v1eqv2;
  real_T c9_d_x;
  boolean_T c9_b;
  boolean_T c9_b4;
  real_T c9_e_x;
  boolean_T c9_b_b;
  real_T c9_a;
  real_T c9_c_b;
  real_T c9_b_a;
  real_T c9_d_b;
  real_T c9_c_a;
  real_T c9_e_b;
  boolean_T c9_b_p;
  real_T c9_f_x;
  boolean_T c9_f_b;
  boolean_T c9_c_p;
  boolean_T exitg1;
  (void)chartInstance;
  (void)c9_dir_size;
  c9_irow1 = c9_idx1 - 1;
  c9_irow2 = c9_idx2 - 1;
  c9_p = true;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k < 2)) {
    c9_b_k = c9_k;
    c9_colk = c9_dir_data[c9_b_k];
    c9_x = c9_colk;
    c9_b_x = c9_x;
    c9_c_x = c9_b_x;
    c9_abscolk = c9_c_x - 1;
    c9_v1 = c9_v->data[c9_irow1 + c9_v->size[0] * c9_abscolk];
    c9_v2 = c9_v->data[c9_irow2 + c9_v->size[0] * c9_abscolk];
    c9_v1eqv2 = (c9_v1 == c9_v2);
    if (c9_v1eqv2) {
      c9_b4 = true;
    } else {
      c9_d_x = c9_v1;
      c9_b = muDoubleScalarIsNaN(c9_d_x);
      if (c9_b) {
        c9_e_x = c9_v2;
        c9_b_b = muDoubleScalarIsNaN(c9_e_x);
        if (c9_b_b) {
          c9_b4 = true;
        } else {
          c9_b4 = false;
        }
      } else {
        c9_b4 = false;
      }
    }

    if (!c9_b4) {
      c9_a = c9_v1;
      c9_c_b = c9_v2;
      c9_b_a = c9_a;
      c9_d_b = c9_c_b;
      c9_c_a = c9_b_a;
      c9_e_b = c9_d_b;
      c9_b_p = (c9_c_a <= c9_e_b);
      if (c9_b_p) {
        c9_c_p = true;
      } else {
        c9_f_x = c9_d_b;
        c9_f_b = muDoubleScalarIsNaN(c9_f_x);
        if (c9_f_b) {
          c9_c_p = true;
        } else {
          c9_c_p = false;
        }
      }

      c9_p = c9_c_p;
      exitg1 = true;
    } else {
      c9_k++;
    }
  }

  return c9_p;
}

static void c9_apply_row_permutation(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_y, c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T
  *c9_b_y)
{
  int32_T c9_i257;
  int32_T c9_i258;
  int32_T c9_i259;
  int32_T c9_loop_ub;
  int32_T c9_i260;
  c9_emxArray_int32_T *c9_b_idx;
  int32_T c9_i261;
  int32_T c9_b_loop_ub;
  int32_T c9_i262;
  c9_i257 = c9_b_y->size[0] * c9_b_y->size[1];
  c9_b_y->size[0] = c9_y->size[0];
  c9_b_y->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_y, c9_i257, &c9_pc_emlrtRTEI);
  c9_i258 = c9_b_y->size[0];
  c9_i259 = c9_b_y->size[1];
  c9_loop_ub = c9_y->size[0] * c9_y->size[1] - 1;
  for (c9_i260 = 0; c9_i260 <= c9_loop_ub; c9_i260++) {
    c9_b_y->data[c9_i260] = c9_y->data[c9_i260];
  }

  c9_emxInit_int32_T(chartInstance, &c9_b_idx, 1, &c9_pc_emlrtRTEI);
  c9_i261 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i261,
    &c9_pc_emlrtRTEI);
  c9_b_loop_ub = c9_idx->size[0] - 1;
  for (c9_i262 = 0; c9_i262 <= c9_b_loop_ub; c9_i262++) {
    c9_b_idx->data[c9_i262] = c9_idx->data[c9_i262];
  }

  c9_b_apply_row_permutation(chartInstance, c9_b_y, c9_b_idx);
  c9_emxFree_int32_T(chartInstance, &c9_b_idx);
}

static boolean_T c9_rows_differ(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_b, int32_T c9_k0, int32_T c9_k)
{
  boolean_T c9_p;
  int32_T c9_j;
  real_T c9_b_j;
  real_T c9_a;
  real_T c9_b_b;
  real_T c9_x;
  real_T c9_b_x;
  real_T c9_c_x;
  real_T c9_y;
  real_T c9_d_x;
  real_T c9_e_x;
  real_T c9_f_x;
  real_T c9_g_x;
  real_T c9_absxk;
  real_T c9_h_x;
  real_T c9_i_x;
  boolean_T c9_c_b;
  boolean_T c9_b5;
  real_T c9_j_x;
  boolean_T c9_d_b;
  boolean_T c9_b6;
  boolean_T c9_e_b;
  real_T c9_r;
  int32_T c9_exponent;
  int32_T c9_b_exponent;
  real_T c9_k_x;
  boolean_T c9_f_b;
  boolean_T c9_b_p;
  real_T c9_l_x;
  boolean_T c9_c_p;
  boolean_T c9_g_b;
  boolean_T exitg1;
  (void)chartInstance;
  c9_p = false;
  c9_j = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_j < 2)) {
    c9_b_j = 1.0 + (real_T)c9_j;
    c9_a = c9_b->data[(c9_k0 + c9_b->size[0] * ((int32_T)c9_b_j - 1)) - 1];
    c9_b_b = c9_b->data[(c9_k + c9_b->size[0] * ((int32_T)c9_b_j - 1)) - 1];
    c9_x = c9_b_b - c9_a;
    c9_b_x = c9_x;
    c9_c_x = c9_b_x;
    c9_y = muDoubleScalarAbs(c9_c_x);
    c9_d_x = c9_b_b / 2.0;
    c9_e_x = c9_d_x;
    c9_f_x = c9_e_x;
    c9_g_x = c9_f_x;
    c9_absxk = muDoubleScalarAbs(c9_g_x);
    c9_h_x = c9_absxk;
    c9_i_x = c9_h_x;
    c9_c_b = muDoubleScalarIsInf(c9_i_x);
    c9_b5 = !c9_c_b;
    c9_j_x = c9_h_x;
    c9_d_b = muDoubleScalarIsNaN(c9_j_x);
    c9_b6 = !c9_d_b;
    c9_e_b = (c9_b5 && c9_b6);
    if (c9_e_b) {
      if (c9_absxk <= 2.2250738585072014E-308) {
        c9_r = 4.94065645841247E-324;
      } else {
        frexp(c9_absxk, &c9_exponent);
        c9_b_exponent = c9_exponent;
        c9_r = ldexp(1.0, c9_b_exponent - 53);
      }
    } else {
      c9_r = rtNaN;
    }

    if (c9_y < c9_r) {
      c9_b_p = true;
    } else {
      c9_k_x = c9_a;
      c9_f_b = muDoubleScalarIsInf(c9_k_x);
      if (c9_f_b) {
        c9_l_x = c9_b_b;
        c9_g_b = muDoubleScalarIsInf(c9_l_x);
        if (c9_g_b && ((c9_a > 0.0) == (c9_b_b > 0.0))) {
          c9_b_p = true;
        } else {
          c9_b_p = false;
        }
      } else {
        c9_b_p = false;
      }
    }

    c9_c_p = c9_b_p;
    if (!c9_c_p) {
      c9_p = true;
      exitg1 = true;
    } else {
      c9_j++;
    }
  }

  return c9_p;
}

static void c9_b_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_real_T *c9_b_x)
{
  int32_T c9_i263;
  int32_T c9_loop_ub;
  int32_T c9_i264;
  c9_i263 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i263, &c9_oc_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i264 = 0; c9_i264 <= c9_loop_ub; c9_i264++) {
    c9_b_x->data[c9_i264] = c9_x->data[c9_i264];
  }

  c9_d_round(chartInstance, c9_b_x);
}

static void c9_sparse(SFc9_LIDAR_simInstanceStruct *chartInstance,
                      c9_emxArray_real_T *c9_varargin_1, c9_emxArray_real_T
                      *c9_varargin_2, c9_coder_internal_sparse *c9_y)
{
  int32_T c9_nc;
  int32_T c9_nr;
  boolean_T c9_b7;
  static char_T c9_cv18[14] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'a', 'm',
    'e', 'l', 'e', 'n' };

  c9_emxArray_real_T *c9_b_varargin_1;
  int32_T c9_i265;
  int32_T c9_i266;
  int32_T c9_i267;
  int32_T c9_loop_ub;
  int32_T c9_i268;
  c9_emxArray_int32_T *c9_ridxInt;
  c9_emxArray_real_T *c9_b_varargin_2;
  int32_T c9_i269;
  int32_T c9_i270;
  int32_T c9_i271;
  int32_T c9_b_loop_ub;
  int32_T c9_i272;
  c9_emxArray_int32_T *c9_cidxInt;
  c9_emxArray_int32_T *c9_sortedIndices;
  int32_T c9_i273;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  int32_T c9_istop;
  int32_T c9_thism;
  int32_T c9_thisn;
  int32_T c9_c_b;
  int32_T c9_d_b;
  boolean_T c9_b_overflow;
  int32_T c9_a;
  int32_T c9_numalloc;
  int32_T c9_c_k;
  int32_T c9_i274;
  int32_T c9_c_loop_ub;
  int32_T c9_i275;
  int32_T c9_iv2[1];
  int32_T c9_i276;
  int32_T c9_i277;
  int32_T c9_d_loop_ub;
  int32_T c9_i278;
  int32_T c9_cptr;
  int32_T c9_i279;
  int32_T c9_e_b;
  int32_T c9_f_b;
  boolean_T c9_c_overflow;
  int32_T c9_c;
  int32_T c9_b_c;
  int32_T c9_g_b;
  int32_T c9_h_b;
  boolean_T c9_d_overflow;
  int32_T c9_d_k;
  c9_nc = c9_varargin_2->size[1];
  c9_nr = c9_varargin_1->size[1];
  if (c9_nr == c9_nc) {
    c9_b7 = true;
  } else {
    c9_b7 = false;
  }

  if (c9_b7) {
  } else {
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                      c9_b_emlrt_marshallOut(chartInstance, c9_cv18), 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c9_b_emlrt_marshallOut(chartInstance, c9_cv18))));
  }

  c9_emxInit_real_T(chartInstance, &c9_b_varargin_1, 2, &c9_qc_emlrtRTEI);
  c9_i265 = c9_b_varargin_1->size[0] * c9_b_varargin_1->size[1];
  c9_b_varargin_1->size[0] = 1;
  c9_b_varargin_1->size[1] = c9_varargin_1->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_1, c9_i265,
    &c9_qc_emlrtRTEI);
  c9_i266 = c9_b_varargin_1->size[0];
  c9_i267 = c9_b_varargin_1->size[1];
  c9_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i268 = 0; c9_i268 <= c9_loop_ub; c9_i268++) {
    c9_b_varargin_1->data[c9_i268] = c9_varargin_1->data[c9_i268];
  }

  c9_emxInit_int32_T(chartInstance, &c9_ridxInt, 1, &c9_uc_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_b_varargin_2, 2, &c9_rc_emlrtRTEI);
  c9_assertValidIndexArg(chartInstance, c9_b_varargin_1, c9_ridxInt);
  c9_i269 = c9_b_varargin_2->size[0] * c9_b_varargin_2->size[1];
  c9_b_varargin_2->size[0] = 1;
  c9_b_varargin_2->size[1] = c9_varargin_2->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_2, c9_i269,
    &c9_rc_emlrtRTEI);
  c9_i270 = c9_b_varargin_2->size[0];
  c9_i271 = c9_b_varargin_2->size[1];
  c9_b_loop_ub = c9_varargin_2->size[0] * c9_varargin_2->size[1] - 1;
  c9_emxFree_real_T(chartInstance, &c9_b_varargin_1);
  for (c9_i272 = 0; c9_i272 <= c9_b_loop_ub; c9_i272++) {
    c9_b_varargin_2->data[c9_i272] = c9_varargin_2->data[c9_i272];
  }

  c9_emxInit_int32_T(chartInstance, &c9_cidxInt, 1, &c9_vc_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_sortedIndices, 1, &c9_wc_emlrtRTEI);
  c9_assertValidIndexArg(chartInstance, c9_b_varargin_2, c9_cidxInt);
  c9_i273 = c9_sortedIndices->size[0];
  c9_sortedIndices->size[0] = c9_nc;
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_sortedIndices, c9_i273,
    &c9_sc_emlrtRTEI);
  c9_b = c9_nc;
  c9_b_b = c9_b;
  c9_emxFree_real_T(chartInstance, &c9_b_varargin_2);
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_nc; c9_k++) {
    c9_b_k = c9_k;
    c9_sortedIndices->data[c9_b_k] = c9_b_k + 1;
  }

  c9_b_locSortrows(chartInstance, c9_sortedIndices, c9_cidxInt, c9_ridxInt);
  c9_emxFree_int32_T(chartInstance, &c9_sortedIndices);
  if ((c9_ridxInt->size[0] == 0) || (c9_cidxInt->size[0] == 0)) {
    c9_thism = 0;
    c9_thisn = 0;
  } else {
    c9_istop = c9_ridxInt->size[0];
    c9_thism = c9_ridxInt->data[0];
    c9_c_b = c9_istop;
    c9_d_b = c9_c_b;
    if (2 > c9_d_b) {
      c9_b_overflow = false;
    } else {
      c9_b_overflow = (c9_d_b > 2147483646);
    }

    if (c9_b_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_c_k = 1; c9_c_k < c9_istop; c9_c_k++) {
      if (c9_thism < c9_ridxInt->data[c9_c_k]) {
        c9_thism = c9_ridxInt->data[c9_c_k];
      }
    }

    c9_thisn = c9_cidxInt->data[c9_cidxInt->size[0] - 1];
  }

  c9_y->m = c9_thism;
  c9_y->n = c9_thisn;
  c9_a = c9_nc;
  if (c9_a >= 1) {
    c9_numalloc = c9_a;
  } else {
    c9_numalloc = 1;
  }

  c9_i274 = c9_y->d->size[0];
  c9_y->d->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_numalloc);
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_y->d, c9_i274,
    &c9_tc_emlrtRTEI);
  c9_c_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_numalloc) - 1;
  for (c9_i275 = 0; c9_i275 <= c9_c_loop_ub; c9_i275++) {
    c9_y->d->data[c9_i275] = false;
  }

  c9_y->maxnz = c9_numalloc;
  c9_iv2[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)(c9_y->n + 1));
  c9_i276 = c9_y->colidx->size[0];
  c9_y->colidx->size[0] = c9_iv2[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_y->colidx, c9_i276,
    &c9_sc_emlrtRTEI);
  c9_y->colidx->data[0] = 1;
  c9_i277 = c9_y->rowidx->size[0];
  c9_y->rowidx->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)
    c9_numalloc);
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_y->rowidx, c9_i277,
    &c9_tc_emlrtRTEI);
  c9_d_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_numalloc) - 1;
  for (c9_i278 = 0; c9_i278 <= c9_d_loop_ub; c9_i278++) {
    c9_y->rowidx->data[c9_i278] = 0;
  }

  c9_cptr = 0;
  c9_i279 = c9_y->n;
  c9_e_b = c9_i279;
  c9_f_b = c9_e_b;
  if (1 > c9_f_b) {
    c9_c_overflow = false;
  } else {
    c9_c_overflow = (c9_f_b > 2147483646);
  }

  if (c9_c_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_c = 1; c9_c - 1 < c9_i279; c9_c++) {
    c9_b_c = c9_c;
    while ((c9_cptr + 1 <= c9_nc) && (c9_cidxInt->data[c9_cptr] == c9_b_c)) {
      c9_y->rowidx->data[c9_cptr] = c9_ridxInt->data[c9_cptr];
      c9_cptr++;
    }

    c9_y->colidx->data[c9_b_c] = c9_cptr + 1;
  }

  c9_emxFree_int32_T(chartInstance, &c9_cidxInt);
  c9_emxFree_int32_T(chartInstance, &c9_ridxInt);
  c9_g_b = c9_nc;
  c9_h_b = c9_g_b;
  if (1 > c9_h_b) {
    c9_d_overflow = false;
  } else {
    c9_d_overflow = (c9_h_b > 2147483646);
  }

  if (c9_d_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_d_k = 0; c9_d_k < c9_nc; c9_d_k++) {
    c9_b_k = c9_d_k;
    c9_y->d->data[c9_b_k] = true;
  }

  c9_b_sparse_fillIn(chartInstance, c9_y);
}

static void c9_assertValidIndexArg(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_s, c9_emxArray_int32_T *c9_sint)
{
  int32_T c9_ns;
  int32_T c9_i280;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  real_T c9_sk;
  real_T c9_x;
  real_T c9_b_x;
  const mxArray *c9_y = NULL;
  static char_T c9_cv19[31] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 'n', 'o', 'n', 'I', 'n', 't', 'e', 'g', 'e',
    'r', 'I', 'n', 'd', 'e', 'x' };

  const mxArray *c9_b_y = NULL;
  const mxArray *c9_c_y = NULL;
  static char_T c9_cv20[26] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 'l', 'a', 'r', 'g', 'e', 'I', 'n', 'd', 'e',
    'x' };

  const mxArray *c9_d_y = NULL;
  const mxArray *c9_e_y = NULL;
  static char_T c9_cv21[27] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 'n', 'o', 'n', 'P', 'o', 's', 'I', 'n', 'd',
    'e', 'x' };

  const mxArray *c9_f_y = NULL;
  c9_ns = c9_s->size[1];
  c9_i280 = c9_sint->size[0];
  c9_sint->size[0] = c9_ns;
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_sint, c9_i280, &c9_xc_emlrtRTEI);
  c9_b = c9_ns;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_ns; c9_k++) {
    c9_b_k = c9_k;
    c9_sk = c9_s->data[c9_b_k];
    c9_x = c9_sk;
    c9_b_x = c9_x;
    c9_b_x = muDoubleScalarFloor(c9_b_x);
    if (c9_b_x == c9_sk) {
    } else {
      c9_y = NULL;
      sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv19, 10, 0U, 1U, 0U, 2, 1, 31),
                    false);
      c9_b_y = NULL;
      sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv19, 10, 0U, 1U, 0U, 2, 1,
        31), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_b_y)));
    }

    if (c9_sk < 2.147483647E+9) {
    } else {
      c9_c_y = NULL;
      sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv20, 10, 0U, 1U, 0U, 2, 1,
        26), false);
      c9_d_y = NULL;
      sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv20, 10, 0U, 1U, 0U, 2, 1,
        26), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_d_y)));
    }

    if (0.0 < c9_sk) {
    } else {
      c9_e_y = NULL;
      sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv21, 10, 0U, 1U, 0U, 2, 1,
        27), false);
      c9_f_y = NULL;
      sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv21, 10, 0U, 1U, 0U, 2, 1,
        27), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_e_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_f_y)));
    }

    c9_sint->data[c9_b_k] = (int32_T)c9_sk;
  }
}

static void c9_locSortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_a, c9_emxArray_int32_T
  *c9_b, c9_emxArray_int32_T *c9_b_idx, c9_emxArray_int32_T *c9_b_a,
  c9_emxArray_int32_T *c9_b_b)
{
  int32_T c9_i281;
  int32_T c9_loop_ub;
  int32_T c9_i282;
  int32_T c9_i283;
  int32_T c9_b_loop_ub;
  int32_T c9_i284;
  int32_T c9_i285;
  int32_T c9_c_loop_ub;
  int32_T c9_i286;
  c9_i281 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i281,
    &c9_yc_emlrtRTEI);
  c9_loop_ub = c9_idx->size[0] - 1;
  for (c9_i282 = 0; c9_i282 <= c9_loop_ub; c9_i282++) {
    c9_b_idx->data[c9_i282] = c9_idx->data[c9_i282];
  }

  c9_i283 = c9_b_a->size[0];
  c9_b_a->size[0] = c9_a->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_a, c9_i283, &c9_yc_emlrtRTEI);
  c9_b_loop_ub = c9_a->size[0] - 1;
  for (c9_i284 = 0; c9_i284 <= c9_b_loop_ub; c9_i284++) {
    c9_b_a->data[c9_i284] = c9_a->data[c9_i284];
  }

  c9_i285 = c9_b_b->size[0];
  c9_b_b->size[0] = c9_b->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_b, c9_i285, &c9_yc_emlrtRTEI);
  c9_c_loop_ub = c9_b->size[0] - 1;
  for (c9_i286 = 0; c9_i286 <= c9_c_loop_ub; c9_i286++) {
    c9_b_b->data[c9_i286] = c9_b->data[c9_i286];
  }

  c9_b_locSortrows(chartInstance, c9_b_idx, c9_b_a, c9_b_b);
}

static void c9_insertionsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  int32_T c9_i287;
  int32_T c9_loop_ub;
  int32_T c9_i288;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_ad_emlrtRTEI);
  c9_i287 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_x, c9_i287, &c9_ad_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i288 = 0; c9_i288 <= c9_loop_ub; c9_i288++) {
    c9_b_x->data[c9_i288] = c9_x->data[c9_i288];
  }

  c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
    &c9_ad_emlrtRTEI);
  c9_b_insertionsort(chartInstance, c9_b_x, c9_xstart, c9_xend, c9_b_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static boolean_T c9___anon_fcn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_a, c9_emxArray_int32_T *c9_b, int32_T c9_i, int32_T
  c9_j)
{
  boolean_T c9_varargout_1;
  int32_T c9_b_i;
  int32_T c9_b_j;
  int32_T c9_ai;
  int32_T c9_aj;
  (void)chartInstance;
  c9_b_i = c9_i - 1;
  c9_b_j = c9_j - 1;
  c9_ai = c9_a->data[c9_b_i];
  c9_aj = c9_a->data[c9_b_j];
  if (c9_ai < c9_aj) {
    c9_varargout_1 = true;
  } else if (c9_ai == c9_aj) {
    c9_varargout_1 = (c9_b->data[c9_b_i] < c9_b->data[c9_b_j]);
  } else {
    c9_varargout_1 = false;
  }

  return c9_varargout_1;
}

static int32_T c9_nextpow2(SFc9_LIDAR_simInstanceStruct *chartInstance, int32_T
  c9_n)
{
  int32_T c9_p;
  int32_T c9_x;
  int32_T c9_b_n;
  int32_T c9_c_n;
  int32_T c9_pmax;
  int32_T c9_pmin;
  int32_T c9_pow2p;
  boolean_T exitg1;
  (void)chartInstance;
  c9_x = c9_n;
  c9_p = c9_x;
  c9_b_n = c9_p;
  c9_c_n = c9_b_n;
  c9_pmax = 31;
  c9_pmin = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_pmax - c9_pmin > 1)) {
    c9_p = (c9_pmin + c9_pmax) >> 1;
    c9_pow2p = 1 << c9_p;
    if (c9_pow2p == c9_c_n) {
      c9_pmax = c9_p;
      exitg1 = true;
    } else if (c9_pow2p > c9_c_n) {
      c9_pmax = c9_p;
    } else {
      c9_pmin = c9_p;
    }
  }

  return c9_pmax;
}

static void c9_introsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  int32_T c9_i289;
  int32_T c9_loop_ub;
  int32_T c9_i290;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_bd_emlrtRTEI);
  c9_i289 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_x, c9_i289, &c9_bd_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i290 = 0; c9_i290 <= c9_loop_ub; c9_i290++) {
    c9_b_x->data[c9_i290] = c9_x->data[c9_i290];
  }

  c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
    &c9_bd_emlrtRTEI);
  c9_b_introsort(chartInstance, c9_b_x, c9_xend, c9_b_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static void c9_stack_stack(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_sBaHy6MF1FZJsDHxMqvBaiH c9_eg, int32_T c9_n, c9_coder_internal_stack
  *c9_this)
{
  int32_T c9_iv3[2];
  int32_T c9_loop_ub;
  int32_T c9_i291;
  (void)chartInstance;
  c9_this->n = 0;
  c9_iv3[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_n);
  c9_iv3[1] = 1;
  c9_this->d.size[0] = c9_iv3[0];
  c9_loop_ub = c9_iv3[0] - 1;
  for (c9_i291 = 0; c9_i291 <= c9_loop_ub; c9_i291++) {
    c9_this->d.data[c9_i291] = c9_eg;
  }
}

static void c9_heapsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  int32_T c9_i292;
  int32_T c9_loop_ub;
  int32_T c9_i293;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_dd_emlrtRTEI);
  c9_i292 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_x, c9_i292, &c9_dd_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i293 = 0; c9_i293 <= c9_loop_ub; c9_i293++) {
    c9_b_x->data[c9_i293] = c9_x->data[c9_i293];
  }

  c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
    &c9_dd_emlrtRTEI);
  c9_b_heapsort(chartInstance, c9_b_x, c9_xstart, c9_xend, c9_b_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static void c9_heapify(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_idx, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, c9_emxArray_int32_T *c9_b_x)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  int32_T c9_i294;
  int32_T c9_loop_ub;
  int32_T c9_i295;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_ed_emlrtRTEI);
  c9_i294 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_x, c9_i294, &c9_ed_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i295 = 0; c9_i295 <= c9_loop_ub; c9_i295++) {
    c9_b_x->data[c9_i295] = c9_x->data[c9_i295];
  }

  c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
    &c9_ed_emlrtRTEI);
  c9_b_heapify(chartInstance, c9_b_x, c9_idx, c9_xstart, c9_xend, c9_b_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static void c9_sortpartition(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp, int32_T *c9_p,
  c9_emxArray_int32_T *c9_b_x)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  int32_T c9_i296;
  int32_T c9_loop_ub;
  int32_T c9_i297;
  int32_T c9_b_p;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_fd_emlrtRTEI);
  c9_i296 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_x, c9_i296, &c9_fd_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i297 = 0; c9_i297 <= c9_loop_ub; c9_i297++) {
    c9_b_x->data[c9_i297] = c9_x->data[c9_i297];
  }

  c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
    &c9_fd_emlrtRTEI);
  c9_b_p = c9_b_sortpartition(chartInstance, c9_b_x, c9_xstart, c9_xend,
    c9_b_cmp);
  *c9_p = c9_b_p;
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static void c9_permuteVector(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_y, c9_emxArray_int32_T
  *c9_b_y)
{
  int32_T c9_i298;
  int32_T c9_loop_ub;
  int32_T c9_i299;
  c9_emxArray_int32_T *c9_b_idx;
  int32_T c9_i300;
  int32_T c9_b_loop_ub;
  int32_T c9_i301;
  c9_i298 = c9_b_y->size[0];
  c9_b_y->size[0] = c9_y->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_y, c9_i298, &c9_gd_emlrtRTEI);
  c9_loop_ub = c9_y->size[0] - 1;
  for (c9_i299 = 0; c9_i299 <= c9_loop_ub; c9_i299++) {
    c9_b_y->data[c9_i299] = c9_y->data[c9_i299];
  }

  c9_emxInit_int32_T(chartInstance, &c9_b_idx, 1, &c9_gd_emlrtRTEI);
  c9_i300 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i300,
    &c9_gd_emlrtRTEI);
  c9_b_loop_ub = c9_idx->size[0] - 1;
  for (c9_i301 = 0; c9_i301 <= c9_b_loop_ub; c9_i301++) {
    c9_b_idx->data[c9_i301] = c9_idx->data[c9_i301];
  }

  c9_b_permuteVector(chartInstance, c9_b_idx, c9_b_y);
  c9_emxFree_int32_T(chartInstance, &c9_b_idx);
}

static void c9_sparse_fillIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_coder_internal_sparse c9_this, c9_coder_internal_sparse *c9_b_this)
{
  c9_emxCopyStruct_coder_internal_sp(chartInstance, c9_b_this, &c9_this,
    &c9_hd_emlrtRTEI);
  c9_b_sparse_fillIn(chartInstance, c9_b_this);
}

static void c9_sparse_full(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_coder_internal_sparse c9_this, c9_emxArray_boolean_T *c9_y)
{
  real_T c9_dv4[2];
  int32_T c9_i302;
  int32_T c9_i303;
  int32_T c9_i304;
  int32_T c9_loop_ub;
  int32_T c9_i305;
  int32_T c9_i306;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_c;
  int32_T c9_b_c;
  int32_T c9_cend;
  int32_T c9_i307;
  int32_T c9_a;
  int32_T c9_c_b;
  int32_T c9_b_a;
  int32_T c9_d_b;
  boolean_T c9_b_overflow;
  int32_T c9_idx;
  c9_dv4[0] = (real_T)c9_this.m;
  c9_dv4[1] = (real_T)c9_this.n;
  c9_i302 = c9_y->size[0] * c9_y->size[1];
  c9_y->size[0] = (int32_T)c9_dv4[0];
  c9_y->size[1] = (int32_T)c9_dv4[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_y, c9_i302, &c9_id_emlrtRTEI);
  c9_i303 = c9_y->size[0];
  c9_i304 = c9_y->size[1];
  c9_loop_ub = (int32_T)c9_dv4[0] * (int32_T)c9_dv4[1] - 1;
  for (c9_i305 = 0; c9_i305 <= c9_loop_ub; c9_i305++) {
    c9_y->data[c9_i305] = false;
  }

  c9_i306 = c9_this.n;
  c9_b = c9_i306;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_c = 0; c9_c < c9_i306; c9_c++) {
    c9_b_c = c9_c;
    c9_cend = c9_this.colidx->data[c9_b_c + 1] - 1;
    c9_i307 = c9_this.colidx->data[c9_b_c];
    c9_a = c9_i307;
    c9_c_b = c9_cend;
    c9_b_a = c9_a;
    c9_d_b = c9_c_b;
    if (c9_b_a > c9_d_b) {
      c9_b_overflow = false;
    } else {
      c9_b_overflow = (c9_d_b > 2147483646);
    }

    if (c9_b_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_idx = c9_i307 - 1; c9_idx < c9_cend; c9_idx++) {
      c9_y->data[(c9_this.rowidx->data[c9_idx] + c9_y->size[0] * c9_b_c) - 1] =
        c9_this.d->data[c9_idx];
    }
  }
}

static void c9_edge(SFc9_LIDAR_simInstanceStruct *chartInstance,
                    c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_boolean_T *
                    c9_varargout_1)
{
  c9_emxArray_real32_T *c9_a;
  int32_T c9_i308;
  int32_T c9_i309;
  int32_T c9_i310;
  int32_T c9_loop_ub;
  int32_T c9_i311;
  boolean_T c9_b8;
  boolean_T c9_b9;
  real_T c9_n;
  int32_T c9_i312;
  real_T c9_m;
  int32_T c9_i313;
  int32_T c9_i314;
  real_T c9_connDimsT[2];
  int32_T c9_i315;
  real_T c9_derivGaussKernel[13];
  static real_T c9_dv5[13] = { 0.00050975920363612861, 0.0025659736304223,
    0.014594040963812248, 0.049305200293708981, 0.09498179875488523,
    0.089159205155936727, 0.0, -0.089159205155936727, -0.09498179875488523,
    -0.049305200293708981, -0.014594040963812248, -0.0025659736304223,
    -0.00050975920363612861 };

  int32_T c9_i316;
  int32_T c9_i317;
  static real_T c9_dv6[6] = { 0.0020299751839417141, 0.010218281014351701,
    0.058116735860084041, 0.19634433732941295, 0.37823877042972093,
    0.35505190018248872 };

  int32_T c9_i318;
  real_T c9_y;
  real_T c9_x[6];
  int32_T c9_i319;
  int32_T c9_k;
  int32_T c9_b_loop_ub;
  int32_T c9_i320;
  real_T c9_b_x;
  int32_T c9_xoffset;
  real_T c9_c_x;
  int32_T c9_ix;
  real_T c9_d_x;
  real_T c9_b_y;
  int32_T c9_i321;
  real_T c9_outSizeT[2];
  real_T c9_startT[2];
  boolean_T c9_b10;
  boolean_T c9_b11;
  c9_emxArray_real32_T *c9_b_a;
  c9_emxArray_real32_T *c9_i1;
  c9_emxArray_real32_T *c9_c_a;
  int32_T c9_i322;
  int32_T c9_i323;
  int32_T c9_i324;
  int32_T c9_i325;
  int32_T c9_i326;
  int32_T c9_i327;
  int32_T c9_c_loop_ub;
  int32_T c9_d_loop_ub;
  int32_T c9_i328;
  int32_T c9_i329;
  int32_T c9_i330;
  real_T c9_b_startT[2];
  boolean_T c9_tooBig;
  boolean_T c9_b12;
  boolean_T c9_b13;
  int32_T c9_i;
  int32_T c9_i331;
  real_T c9_b_i;
  boolean_T c9_modeFlag;
  real_T c9_c_startT[2];
  int32_T c9_trueCount;
  boolean_T c9_b_tooBig;
  int32_T c9_c_i;
  boolean_T c9_b14;
  boolean_T c9_b_modeFlag;
  boolean_T c9_b15;
  boolean_T c9_c_modeFlag;
  int32_T c9_tmp_size[1];
  int32_T c9_i332;
  int32_T c9_i333;
  int32_T c9_partialTrueCount;
  int32_T c9_i334;
  int32_T c9_d_i;
  real_T c9_d_startT[2];
  boolean_T c9_c_tooBig;
  int32_T c9_nonzero_h_size[1];
  int32_T c9_e_i;
  int32_T c9_i335;
  int32_T c9_i336;
  int32_T c9_e_loop_ub;
  int32_T c9_tmp_data[13];
  int32_T c9_i337;
  real_T c9_f_i;
  boolean_T c9_b16;
  int32_T c9_i338;
  real_T c9_padSizeT[2];
  int32_T c9_i339;
  boolean_T c9_d_modeFlag;
  boolean_T c9_b17;
  int32_T c9_i340;
  real_T c9_nonzero_h_data[13];
  boolean_T c9_d_tooBig;
  int32_T c9_i341;
  int32_T c9_i342;
  int32_T c9_i343;
  boolean_T c9_e_modeFlag;
  c9_emxArray_real32_T *c9_b_i1;
  boolean_T c9_densityFlag;
  boolean_T c9_conn[13];
  int32_T c9_i344;
  boolean_T c9_f_modeFlag;
  real_T c9_e_startT[2];
  int32_T c9_i345;
  static real_T c9_kernel[13] = { 3.4813359214923066E-5, 0.00054457256285105169,
    0.0051667606200595231, 0.029732654490475546, 0.10377716120747749,
    0.219696252000246, 0.28209557151935094, 0.219696252000246,
    0.10377716120747749, 0.029732654490475546, 0.0051667606200595231,
    0.00054457256285105169, 3.4813359214923066E-5 };

  int32_T c9_i346;
  int32_T c9_i347;
  boolean_T c9_e_tooBig;
  int32_T c9_g_i;
  int32_T c9_i348;
  int32_T c9_b_trueCount;
  int32_T c9_i349;
  int32_T c9_h_i;
  int32_T c9_f_loop_ub;
  real_T c9_i_i;
  int32_T c9_i350;
  int32_T c9_i351;
  int32_T c9_i352;
  boolean_T c9_g_modeFlag;
  int32_T c9_b_tmp_size[1];
  boolean_T c9_f_tooBig;
  int32_T c9_i353;
  int32_T c9_i354;
  int32_T c9_b_partialTrueCount;
  c9_emxArray_real32_T *c9_d_a;
  int32_T c9_j_i;
  int32_T c9_i355;
  boolean_T c9_h_modeFlag;
  int32_T c9_i356;
  int32_T c9_i357;
  int32_T c9_i358;
  int32_T c9_i359;
  int32_T c9_b_tmp_data[13];
  int32_T c9_i360;
  int32_T c9_i361;
  static real_T c9_b_kernel[13] = { 3.4813359214923066E-5,
    0.00054457256285105169, 0.0051667606200595231, 0.029732654490475546,
    0.10377716120747749, 0.219696252000246, 0.28209557151935094,
    0.219696252000246, 0.10377716120747749, 0.029732654490475546,
    0.0051667606200595231, 0.00054457256285105169, 3.4813359214923066E-5 };

  int32_T c9_i362;
  boolean_T c9_i_modeFlag;
  int32_T c9_g_loop_ub;
  int32_T c9_i363;
  int32_T c9_h_loop_ub;
  int32_T c9_i364;
  int32_T c9_i365;
  c9_emxArray_real32_T *c9_b_varargin_1;
  int32_T c9_i366;
  int32_T c9_i367;
  int32_T c9_i368;
  int32_T c9_i369;
  boolean_T c9_b_densityFlag;
  int32_T c9_i370;
  int32_T c9_i371;
  int32_T c9_i_loop_ub;
  boolean_T c9_g_tooBig;
  real_T c9_numKernElem;
  int32_T c9_i372;
  int32_T c9_k_i;
  int32_T c9_i373;
  int32_T c9_i374;
  real_T c9_l_i;
  int32_T c9_i375;
  const mxArray *c9_c_y = NULL;
  boolean_T c9_j_modeFlag;
  int32_T c9_b_n;
  boolean_T c9_h_tooBig;
  const mxArray *c9_d_y = NULL;
  int32_T c9_c_n;
  boolean_T c9_k_modeFlag;
  real32_T c9_e_x;
  real32_T c9_magmax;
  real32_T c9_f_x;
  real32_T c9_g_x;
  int32_T c9_i376;
  boolean_T c9_b;
  boolean_T c9_b_b;
  boolean_T c9_p;
  real32_T c9_h_x;
  boolean_T c9_l_modeFlag;
  int32_T c9_idx;
  boolean_T c9_c_b;
  real32_T c9_e_y;
  int32_T c9_i377;
  int32_T c9_d_b;
  real_T c9_d_n;
  real32_T c9_f_y;
  int32_T c9_e_b;
  real_T c9_b_m;
  int32_T c9_i378;
  int32_T c9_first;
  boolean_T c9_b18;
  int32_T c9_i379;
  boolean_T c9_overflow;
  int32_T c9_last;
  boolean_T c9_b19;
  real32_T c9_ex;
  int32_T c9_i380;
  int32_T c9_i381;
  int32_T c9_i382;
  real_T c9_numCores;
  int32_T c9_i383;
  int32_T c9_e_a;
  int32_T c9_i384;
  int32_T c9_b_k;
  int32_T c9_f_b;
  int32_T c9_i385;
  int32_T c9_i386;
  int32_T c9_i387;
  int32_T c9_f_a;
  real_T c9_counts[64];
  int32_T c9_i388;
  int32_T c9_g_b;
  boolean_T c9_useParallel;
  int32_T c9_c_k;
  int32_T c9_j_loop_ub;
  real32_T c9_i_x;
  int32_T c9_i389;
  real_T c9_b_numKernElem;
  int32_T c9_i390;
  real32_T c9_j_x;
  boolean_T c9_b_overflow;
  real_T c9_rowsA;
  int32_T c9_i391;
  boolean_T c9_h_b;
  real_T c9_colsAEtc;
  real_T c9_d11;
  int32_T c9_d_k;
  boolean_T c9_b_p;
  int32_T c9_i392;
  int32_T c9_i393;
  int32_T c9_i394;
  real_T c9_numelA;
  int32_T c9_e_k;
  real_T c9_numRows;
  real_T c9_numCols;
  int32_T c9_b_idx;
  boolean_T c9_k_x[64];
  boolean_T c9_nanFlag;
  boolean_T c9_b_nanFlag;
  int32_T c9_ii_size[1];
  real_T c9_d12;
  boolean_T c9_rngFlag;
  int32_T c9_ii;
  int32_T c9_i395;
  int32_T c9_m_i;
  boolean_T c9_b_rngFlag;
  boolean_T c9_c_nanFlag;
  int32_T c9_b_ii;
  real_T c9_n_i;
  int32_T c9_i396;
  real32_T c9_l_x;
  int32_T c9_highThreshTemp_size[1];
  int32_T c9_ii_data[1];
  boolean_T c9_i_b;
  int32_T c9_k_loop_ub;
  int32_T c9_i397;
  real32_T c9_c_idx;
  real_T c9_highThreshTemp_data[1];
  real32_T c9_m_x;
  int32_T c9_l_loop_ub;
  boolean_T c9_j_b;
  int32_T c9_i398;
  real32_T c9_g_a;
  int32_T c9_c;
  real32_T c9_h_a;
  real_T c9_d13;
  int32_T c9_i399;
  int32_T c9_b_c;
  int32_T c9_i400;
  int32_T c9_lowThresh_size[1];
  int32_T c9_b_size[1];
  int32_T c9_m_loop_ub;
  int32_T c9_i401;
  int32_T c9_i402;
  real_T c9_b_data[1];
  int32_T c9_i403;
  int32_T c9_i404;
  real_T c9_lowThresh_data[1];
  int32_T c9_n_loop_ub;
  int32_T c9_i405;
  boolean_T c9_b20;
  boolean_T c9_b21;
  c9_emxArray_boolean_T *c9_E;
  int32_T c9_i406;
  int32_T c9_i407;
  int32_T c9_i408;
  int32_T c9_o_loop_ub;
  int32_T c9_i409;
  c9_emxArray_boolean_T *c9_n_x;
  real_T c9_e_n;
  real_T c9_c_m;
  real_T c9_lowThresh;
  real_T c9_b_lowThresh;
  int32_T c9_i410;
  real_T c9_highThreshTemp;
  int32_T c9_i411;
  int32_T c9_i412;
  int32_T c9_p_loop_ub;
  int32_T c9_i413;
  int32_T c9_i414;
  int32_T c9_i415;
  int32_T c9_o_x[2];
  int32_T c9_b_E[2];
  int32_T c9_i416;
  int32_T c9_i417;
  int32_T c9_i418;
  int32_T c9_i419;
  int32_T c9_i420;
  int32_T c9_i421;
  int32_T c9_i422;
  int32_T c9_q_loop_ub;
  int32_T c9_i423;
  int32_T c9_nx;
  boolean_T c9_b22;
  boolean_T c9_b23;
  boolean_T c9_b24;
  const mxArray *c9_g_y = NULL;
  real_T c9_dn;
  const mxArray *c9_h_y = NULL;
  real_T c9_dm;
  int32_T c9_d_m;
  int32_T c9_f_n;
  c9_emxArray_int32_T *c9_o_i;
  c9_emxArray_int32_T *c9_j;
  c9_emxArray_boolean_T *c9_v;
  int32_T c9_i424;
  int32_T c9_f_k;
  int32_T c9_e_m;
  int32_T c9_i425;
  int32_T c9_g_n;
  int32_T c9_d_idx;
  int32_T c9_i426;
  c9_emxArray_real_T *c9_idxStrongR;
  int32_T c9_i427;
  int32_T c9_i428;
  int32_T c9_r_loop_ub;
  int32_T c9_indexSize[2];
  int32_T c9_i429;
  int32_T c9_i430;
  c9_emxArray_real_T *c9_idxStrongC;
  int32_T c9_c_ii;
  int32_T c9_i431;
  int32_T c9_jj;
  int32_T c9_s_loop_ub;
  int32_T c9_i432;
  const mxArray *c9_i_y = NULL;
  const mxArray *c9_j_y = NULL;
  boolean_T c9_b25;
  int32_T c9_i433;
  int32_T c9_i434;
  int32_T c9_i435;
  int32_T c9_i436;
  c9_emxArray_int32_T *c9_r12;
  int32_T c9_i437;
  int32_T c9_i438;
  int32_T c9_i439;
  int32_T c9_t_loop_ub;
  int32_T c9_u_loop_ub;
  int32_T c9_i440;
  int32_T c9_i441;
  int32_T c9_matrixSize;
  int32_T c9_size1;
  boolean_T c9_k_b;
  boolean_T c9_nonSingletonDimFound;
  boolean_T c9_c_c;
  boolean_T c9_d_c;
  boolean_T c9_l_b;
  const mxArray *c9_k_y = NULL;
  int32_T c9_i442;
  const mxArray *c9_l_y = NULL;
  boolean_T c9_b26;
  int32_T c9_i443;
  int32_T c9_i444;
  int32_T c9_v_loop_ub;
  int32_T c9_i445;
  int32_T c9_b_matrixSize;
  int32_T c9_b_size1;
  boolean_T c9_m_b;
  boolean_T c9_b_nonSingletonDimFound;
  boolean_T c9_e_c;
  boolean_T c9_f_c;
  boolean_T c9_n_b;
  const mxArray *c9_m_y = NULL;
  int32_T c9_i446;
  const mxArray *c9_n_y = NULL;
  boolean_T c9_b27;
  int32_T c9_i447;
  int32_T c9_i448;
  int32_T c9_w_loop_ub;
  int32_T c9_i449;
  int32_T c9_c_matrixSize;
  int32_T c9_c_size1;
  boolean_T c9_o_b;
  boolean_T c9_c_nonSingletonDimFound;
  boolean_T c9_g_c;
  boolean_T c9_h_c;
  boolean_T c9_p_b;
  const mxArray *c9_o_y = NULL;
  const mxArray *c9_p_y = NULL;
  boolean_T exitg1;
  boolean_T guard1 = false;
  c9_emxInit_real32_T(chartInstance, &c9_a, 2, &c9_jd_emlrtRTEI);
  c9_i308 = c9_a->size[0] * c9_a->size[1];
  c9_a->size[0] = c9_varargin_1->size[0];
  c9_a->size[1] = c9_varargin_1->size[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_a, c9_i308, &c9_jd_emlrtRTEI);
  c9_i309 = c9_a->size[0];
  c9_i310 = c9_a->size[1];
  c9_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i311 = 0; c9_i311 <= c9_loop_ub; c9_i311++) {
    c9_a->data[c9_i311] = (real32_T)c9_varargin_1->data[c9_i311];
  }

  c9_b8 = (c9_a->size[0] == 0);
  c9_b9 = (c9_a->size[1] == 0);
  if (c9_b8 || c9_b9) {
    for (c9_i312 = 0; c9_i312 < 2; c9_i312++) {
      c9_connDimsT[c9_i312] = (real_T)c9_a->size[c9_i312];
    }

    for (c9_i314 = 0; c9_i314 < 2; c9_i314++) {
      c9_connDimsT[c9_i314];
    }

    c9_i316 = c9_varargout_1->size[0] * c9_varargout_1->size[1];
    c9_varargout_1->size[0] = (int32_T)c9_connDimsT[0];
    c9_varargout_1->size[1] = (int32_T)c9_connDimsT[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_varargout_1, c9_i316,
      &c9_kd_emlrtRTEI);
    c9_i318 = c9_varargout_1->size[0];
    c9_i319 = c9_varargout_1->size[1];
    c9_b_loop_ub = (int32_T)c9_connDimsT[0] * (int32_T)c9_connDimsT[1] - 1;
    for (c9_i320 = 0; c9_i320 <= c9_b_loop_ub; c9_i320++) {
      c9_varargout_1->data[c9_i320] = false;
    }
  } else {
    c9_n = (real_T)c9_a->size[1];
    c9_m = (real_T)c9_a->size[0];
    for (c9_i313 = 0; c9_i313 < 13; c9_i313++) {
      c9_derivGaussKernel[c9_i313] = c9_dv5[c9_i313];
    }

    for (c9_i315 = 0; c9_i315 < 6; c9_i315++) {
      c9_derivGaussKernel[c9_i315] = c9_dv6[c9_i315];
    }

    for (c9_i317 = 0; c9_i317 < 6; c9_i317++) {
      c9_x[c9_i317] = c9_derivGaussKernel[c9_i317 + 7];
    }

    c9_y = c9_x[0];
    for (c9_k = 0; c9_k < 5; c9_k++) {
      c9_xoffset = c9_k;
      c9_ix = c9_xoffset + 1;
      c9_y += c9_x[c9_ix];
    }

    c9_b_x = c9_y;
    c9_c_x = c9_b_x;
    c9_d_x = c9_c_x;
    c9_b_y = muDoubleScalarAbs(c9_d_x);
    for (c9_i321 = 0; c9_i321 < 6; c9_i321++) {
      c9_derivGaussKernel[c9_i321 + 7] /= c9_b_y;
    }

    c9_outSizeT[0] = (real_T)c9_a->size[0];
    c9_startT[0] = 6.0;
    c9_outSizeT[1] = (real_T)c9_a->size[1];
    c9_startT[1] = 0.0;
    c9_b10 = (c9_a->size[0] == 0);
    c9_b11 = (c9_a->size[1] == 0);
    c9_emxInit_real32_T(chartInstance, &c9_b_a, 2, &c9_oe_emlrtRTEI);
    c9_emxInit_real32_T(chartInstance, &c9_i1, 2, &c9_qe_emlrtRTEI);
    if (c9_b10 || c9_b11) {
      c9_i322 = c9_i1->size[0] * c9_i1->size[1];
      c9_i1->size[0] = c9_a->size[0];
      c9_i1->size[1] = c9_a->size[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_i1, c9_i322,
        &c9_ld_emlrtRTEI);
      c9_i324 = c9_i1->size[0];
      c9_i326 = c9_i1->size[1];
      c9_c_loop_ub = c9_a->size[0] * c9_a->size[1] - 1;
      for (c9_i328 = 0; c9_i328 <= c9_c_loop_ub; c9_i328++) {
        c9_i1->data[c9_i328] = c9_a->data[c9_i328];
      }
    } else {
      c9_emxInit_real32_T(chartInstance, &c9_c_a, 2, &c9_md_emlrtRTEI);
      c9_i323 = c9_c_a->size[0] * c9_c_a->size[1];
      c9_c_a->size[0] = c9_a->size[0];
      c9_c_a->size[1] = c9_a->size[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_c_a, c9_i323,
        &c9_md_emlrtRTEI);
      c9_i325 = c9_c_a->size[0];
      c9_i327 = c9_c_a->size[1];
      c9_d_loop_ub = c9_a->size[0] * c9_a->size[1] - 1;
      for (c9_i329 = 0; c9_i329 <= c9_d_loop_ub; c9_i329++) {
        c9_c_a->data[c9_i329] = c9_a->data[c9_i329];
      }

      for (c9_i330 = 0; c9_i330 < 2; c9_i330++) {
        c9_b_startT[c9_i330] = c9_startT[c9_i330];
      }

      c9_padImage(chartInstance, c9_c_a, c9_b_startT, c9_b_a);
      c9_tooBig = true;
      c9_emxFree_real32_T(chartInstance, &c9_c_a);
      for (c9_i = 0; c9_i < 2; c9_i++) {
        c9_b_i = 1.0 + (real_T)c9_i;
        if (c9_tooBig && (c9_outSizeT[(int32_T)c9_b_i - 1] > 65500.0)) {
          c9_b_tooBig = true;
        } else {
          c9_b_tooBig = false;
        }

        c9_tooBig = c9_b_tooBig;
      }

      if (!c9_tooBig) {
        c9_modeFlag = true;
      } else {
        c9_modeFlag = false;
      }

      if (c9_modeFlag) {
        c9_b_modeFlag = true;
      } else {
        c9_b_modeFlag = false;
      }

      c9_c_modeFlag = c9_b_modeFlag;
      c9_i333 = c9_i1->size[0] * c9_i1->size[1];
      c9_i1->size[0] = (int32_T)c9_outSizeT[0];
      c9_i1->size[1] = (int32_T)c9_outSizeT[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_i1, c9_i333,
        &c9_nd_emlrtRTEI);
      if (c9_c_modeFlag) {
        for (c9_i336 = 0; c9_i336 < 2; c9_i336++) {
          c9_padSizeT[c9_i336] = (real_T)c9_b_a->size[c9_i336];
        }

        for (c9_i339 = 0; c9_i339 < 2; c9_i339++) {
          c9_padSizeT[c9_i339];
        }

        for (c9_i343 = 0; c9_i343 < 2; c9_i343++) {
          c9_connDimsT[c9_i343] = 13.0 + -12.0 * (real_T)c9_i343;
        }

        ippfilter_real32(&c9_b_a->data[0], &c9_i1->data[0], c9_outSizeT, 2.0,
                         c9_padSizeT, c9_kernel, c9_connDimsT, true);
      } else {
        for (c9_i335 = 0; c9_i335 < 2; c9_i335++) {
          c9_padSizeT[c9_i335] = (real_T)c9_b_a->size[c9_i335];
        }

        for (c9_i338 = 0; c9_i338 < 2; c9_i338++) {
          c9_padSizeT[c9_i338];
        }

        for (c9_i342 = 0; c9_i342 < 13; c9_i342++) {
          c9_conn[c9_i342] = true;
        }

        for (c9_i345 = 0; c9_i345 < 2; c9_i345++) {
          c9_connDimsT[c9_i345] = 13.0 + -12.0 * (real_T)c9_i345;
        }

        imfilter_real32(&c9_b_a->data[0], &c9_i1->data[0], 2.0, c9_outSizeT, 2.0,
                        c9_padSizeT, c9_kernel, 13.0, c9_conn, 2.0, c9_connDimsT,
                        c9_startT, 2.0, true, true);
      }
    }

    c9_outSizeT[0] = (real_T)c9_i1->size[0];
    c9_startT[0] = 0.0;
    c9_outSizeT[1] = (real_T)c9_i1->size[1];
    c9_startT[1] = 6.0;
    c9_b12 = (c9_i1->size[0] == 0);
    c9_b13 = (c9_i1->size[1] == 0);
    if (c9_b12 || c9_b13) {
    } else {
      for (c9_i331 = 0; c9_i331 < 2; c9_i331++) {
        c9_c_startT[c9_i331] = c9_startT[c9_i331];
      }

      c9_padImage(chartInstance, c9_i1, c9_c_startT, c9_b_a);
      c9_trueCount = 0;
      for (c9_c_i = 0; c9_c_i < 13; c9_c_i++) {
        if (c9_derivGaussKernel[c9_c_i] != 0.0) {
          c9_trueCount++;
        }
      }

      c9_tmp_size[0] = c9_trueCount;
      c9_partialTrueCount = 0;
      for (c9_d_i = 0; c9_d_i < 13; c9_d_i++) {
        if (c9_derivGaussKernel[c9_d_i] != 0.0) {
          c9_tmp_data[c9_partialTrueCount] = c9_d_i + 1;
          c9_partialTrueCount++;
        }
      }

      c9_nonzero_h_size[0] = c9_tmp_size[0];
      c9_e_loop_ub = c9_tmp_size[0] - 1;
      for (c9_i337 = 0; c9_i337 <= c9_e_loop_ub; c9_i337++) {
        c9_nonzero_h_data[c9_i337] = c9_derivGaussKernel[c9_tmp_data[c9_i337] -
          1];
      }

      for (c9_i340 = 0; c9_i340 < 13; c9_i340++) {
        c9_conn[c9_i340] = (c9_derivGaussKernel[c9_i340] != 0.0);
      }

      c9_densityFlag = false;
      if ((real_T)c9_nonzero_h_size[0] / 13.0 > 0.05) {
        c9_densityFlag = true;
      }

      c9_e_tooBig = true;
      for (c9_g_i = 0; c9_g_i < 2; c9_g_i++) {
        c9_i_i = 1.0 + (real_T)c9_g_i;
        if (c9_e_tooBig && (c9_outSizeT[(int32_T)c9_i_i - 1] > 65500.0)) {
          c9_f_tooBig = true;
        } else {
          c9_f_tooBig = false;
        }

        c9_e_tooBig = c9_f_tooBig;
      }

      if (c9_densityFlag && (!c9_e_tooBig)) {
        c9_g_modeFlag = true;
      } else {
        c9_g_modeFlag = false;
      }

      if (c9_g_modeFlag) {
        c9_h_modeFlag = true;
      } else {
        c9_h_modeFlag = false;
      }

      for (c9_i358 = 0; c9_i358 < 13; c9_i358++) {
        c9_conn[c9_i358];
      }

      c9_i_modeFlag = c9_h_modeFlag;
      c9_i363 = c9_i1->size[0] * c9_i1->size[1];
      c9_i1->size[0] = (int32_T)c9_outSizeT[0];
      c9_i1->size[1] = (int32_T)c9_outSizeT[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_i1, c9_i363,
        &c9_nd_emlrtRTEI);
      if (c9_i_modeFlag) {
        for (c9_i368 = 0; c9_i368 < 2; c9_i368++) {
          c9_padSizeT[c9_i368] = (real_T)c9_b_a->size[c9_i368];
        }

        for (c9_i371 = 0; c9_i371 < 2; c9_i371++) {
          c9_padSizeT[c9_i371];
        }

        for (c9_i372 = 0; c9_i372 < 2; c9_i372++) {
          c9_connDimsT[c9_i372] = 1.0 + 12.0 * (real_T)c9_i372;
        }

        ippfilter_real32(&c9_b_a->data[0], &c9_i1->data[0], c9_outSizeT, 2.0,
                         c9_padSizeT, c9_derivGaussKernel, c9_connDimsT, true);
      } else {
        for (c9_i367 = 0; c9_i367 < 2; c9_i367++) {
          c9_padSizeT[c9_i367] = (real_T)c9_b_a->size[c9_i367];
        }

        for (c9_i370 = 0; c9_i370 < 2; c9_i370++) {
          c9_padSizeT[c9_i370];
        }

        c9_numKernElem = (real_T)c9_nonzero_h_size[0];
        for (c9_i374 = 0; c9_i374 < 13; c9_i374++) {
          c9_conn[c9_i374];
        }

        for (c9_i375 = 0; c9_i375 < 2; c9_i375++) {
          c9_connDimsT[c9_i375] = 1.0 + 12.0 * (real_T)c9_i375;
        }

        imfilter_real32(&c9_b_a->data[0], &c9_i1->data[0], 2.0, c9_outSizeT, 2.0,
                        c9_padSizeT, &c9_nonzero_h_data[0], c9_numKernElem,
                        c9_conn, 2.0, c9_connDimsT, c9_startT, 2.0, true, true);
      }
    }

    c9_outSizeT[0] = (real_T)c9_a->size[0];
    c9_startT[0] = 0.0;
    c9_outSizeT[1] = (real_T)c9_a->size[1];
    c9_startT[1] = 6.0;
    c9_b14 = (c9_a->size[0] == 0);
    c9_b15 = (c9_a->size[1] == 0);
    if (c9_b14 || c9_b15) {
    } else {
      for (c9_i332 = 0; c9_i332 < 2; c9_i332++) {
        c9_d_startT[c9_i332] = c9_startT[c9_i332];
      }

      c9_padImage(chartInstance, c9_a, c9_d_startT, c9_b_a);
      c9_c_tooBig = true;
      for (c9_e_i = 0; c9_e_i < 2; c9_e_i++) {
        c9_f_i = 1.0 + (real_T)c9_e_i;
        if (c9_c_tooBig && (c9_outSizeT[(int32_T)c9_f_i - 1] > 65500.0)) {
          c9_d_tooBig = true;
        } else {
          c9_d_tooBig = false;
        }

        c9_c_tooBig = c9_d_tooBig;
      }

      if (!c9_c_tooBig) {
        c9_d_modeFlag = true;
      } else {
        c9_d_modeFlag = false;
      }

      if (c9_d_modeFlag) {
        c9_e_modeFlag = true;
      } else {
        c9_e_modeFlag = false;
      }

      c9_f_modeFlag = c9_e_modeFlag;
      c9_i346 = c9_a->size[0] * c9_a->size[1];
      c9_a->size[0] = (int32_T)c9_outSizeT[0];
      c9_a->size[1] = (int32_T)c9_outSizeT[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_a, c9_i346,
        &c9_nd_emlrtRTEI);
      if (c9_f_modeFlag) {
        for (c9_i351 = 0; c9_i351 < 2; c9_i351++) {
          c9_padSizeT[c9_i351] = (real_T)c9_b_a->size[c9_i351];
        }

        for (c9_i354 = 0; c9_i354 < 2; c9_i354++) {
          c9_padSizeT[c9_i354];
        }

        for (c9_i357 = 0; c9_i357 < 2; c9_i357++) {
          c9_connDimsT[c9_i357] = 1.0 + 12.0 * (real_T)c9_i357;
        }

        ippfilter_real32(&c9_b_a->data[0], &c9_a->data[0], c9_outSizeT, 2.0,
                         c9_padSizeT, c9_b_kernel, c9_connDimsT, true);
      } else {
        for (c9_i350 = 0; c9_i350 < 2; c9_i350++) {
          c9_padSizeT[c9_i350] = (real_T)c9_b_a->size[c9_i350];
        }

        for (c9_i353 = 0; c9_i353 < 2; c9_i353++) {
          c9_padSizeT[c9_i353];
        }

        for (c9_i356 = 0; c9_i356 < 13; c9_i356++) {
          c9_conn[c9_i356] = true;
        }

        for (c9_i361 = 0; c9_i361 < 2; c9_i361++) {
          c9_connDimsT[c9_i361] = 1.0 + 12.0 * (real_T)c9_i361;
        }

        imfilter_real32(&c9_b_a->data[0], &c9_a->data[0], 2.0, c9_outSizeT, 2.0,
                        c9_padSizeT, c9_kernel, 13.0, c9_conn, 2.0, c9_connDimsT,
                        c9_startT, 2.0, true, true);
      }
    }

    for (c9_i334 = 0; c9_i334 < 13; c9_i334++) {
      c9_derivGaussKernel[c9_i334];
    }

    c9_outSizeT[0] = (real_T)c9_a->size[0];
    c9_startT[0] = 6.0;
    c9_outSizeT[1] = (real_T)c9_a->size[1];
    c9_startT[1] = 0.0;
    c9_b16 = (c9_a->size[0] == 0);
    c9_b17 = (c9_a->size[1] == 0);
    if (c9_b16 || c9_b17) {
    } else {
      for (c9_i341 = 0; c9_i341 < 2; c9_i341++) {
        c9_e_startT[c9_i341] = c9_startT[c9_i341];
      }

      c9_padImage(chartInstance, c9_a, c9_e_startT, c9_b_a);
      for (c9_i347 = 0; c9_i347 < 13; c9_i347++) {
        c9_derivGaussKernel[c9_i347];
      }

      c9_b_trueCount = 0;
      for (c9_h_i = 0; c9_h_i < 13; c9_h_i++) {
        if (c9_derivGaussKernel[c9_h_i] != 0.0) {
          c9_b_trueCount++;
        }
      }

      c9_b_tmp_size[0] = c9_b_trueCount;
      c9_b_partialTrueCount = 0;
      for (c9_j_i = 0; c9_j_i < 13; c9_j_i++) {
        if (c9_derivGaussKernel[c9_j_i] != 0.0) {
          c9_b_tmp_data[c9_b_partialTrueCount] = c9_j_i + 1;
          c9_b_partialTrueCount++;
        }
      }

      for (c9_i359 = 0; c9_i359 < 13; c9_i359++) {
        c9_derivGaussKernel[c9_i359];
      }

      c9_nonzero_h_size[0] = c9_b_tmp_size[0];
      c9_h_loop_ub = c9_b_tmp_size[0] - 1;
      for (c9_i365 = 0; c9_i365 <= c9_h_loop_ub; c9_i365++) {
        c9_nonzero_h_data[c9_i365] = c9_derivGaussKernel[c9_b_tmp_data[c9_i365]
          - 1];
      }

      for (c9_i366 = 0; c9_i366 < 13; c9_i366++) {
        c9_conn[c9_i366] = (c9_derivGaussKernel[c9_i366] != 0.0);
      }

      c9_b_densityFlag = false;
      if ((real_T)c9_nonzero_h_size[0] / 13.0 > 0.05) {
        c9_b_densityFlag = true;
      }

      c9_g_tooBig = true;
      for (c9_k_i = 0; c9_k_i < 2; c9_k_i++) {
        c9_l_i = 1.0 + (real_T)c9_k_i;
        if (c9_g_tooBig && (c9_outSizeT[(int32_T)c9_l_i - 1] > 65500.0)) {
          c9_h_tooBig = true;
        } else {
          c9_h_tooBig = false;
        }

        c9_g_tooBig = c9_h_tooBig;
      }

      if (c9_b_densityFlag && (!c9_g_tooBig)) {
        c9_j_modeFlag = true;
      } else {
        c9_j_modeFlag = false;
      }

      if (c9_j_modeFlag) {
        c9_k_modeFlag = true;
      } else {
        c9_k_modeFlag = false;
      }

      for (c9_i376 = 0; c9_i376 < 13; c9_i376++) {
        c9_derivGaussKernel[c9_i376];
      }

      c9_l_modeFlag = c9_k_modeFlag;
      c9_i377 = c9_a->size[0] * c9_a->size[1];
      c9_a->size[0] = (int32_T)c9_outSizeT[0];
      c9_a->size[1] = (int32_T)c9_outSizeT[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_a, c9_i377,
        &c9_nd_emlrtRTEI);
      if (c9_l_modeFlag) {
        for (c9_i381 = 0; c9_i381 < 2; c9_i381++) {
          c9_padSizeT[c9_i381] = (real_T)c9_b_a->size[c9_i381];
        }

        for (c9_i387 = 0; c9_i387 < 2; c9_i387++) {
          c9_padSizeT[c9_i387];
        }

        for (c9_i390 = 0; c9_i390 < 13; c9_i390++) {
          c9_derivGaussKernel[c9_i390];
        }

        for (c9_i393 = 0; c9_i393 < 2; c9_i393++) {
          c9_connDimsT[c9_i393] = 13.0 + -12.0 * (real_T)c9_i393;
        }

        ippfilter_real32(&c9_b_a->data[0], &c9_a->data[0], c9_outSizeT, 2.0,
                         c9_padSizeT, c9_derivGaussKernel, c9_connDimsT, true);
      } else {
        for (c9_i380 = 0; c9_i380 < 2; c9_i380++) {
          c9_padSizeT[c9_i380] = (real_T)c9_b_a->size[c9_i380];
        }

        for (c9_i386 = 0; c9_i386 < 2; c9_i386++) {
          c9_padSizeT[c9_i386];
        }

        c9_b_numKernElem = (real_T)c9_nonzero_h_size[0];
        for (c9_i391 = 0; c9_i391 < 2; c9_i391++) {
          c9_connDimsT[c9_i391] = 13.0 + -12.0 * (real_T)c9_i391;
        }

        imfilter_real32(&c9_b_a->data[0], &c9_a->data[0], 2.0, c9_outSizeT, 2.0,
                        c9_padSizeT, &c9_nonzero_h_data[0], c9_b_numKernElem,
                        c9_conn, 2.0, c9_connDimsT, c9_startT, 2.0, true, true);
      }
    }

    c9_emxInit_real32_T(chartInstance, &c9_b_i1, 2, &c9_qd_emlrtRTEI);
    c9_i344 = c9_b_i1->size[0] * c9_b_i1->size[1];
    c9_b_i1->size[0] = c9_i1->size[0];
    c9_b_i1->size[1] = c9_i1->size[1];
    c9_emxEnsureCapacity_real32_T(chartInstance, c9_b_i1, c9_i344,
      &c9_qd_emlrtRTEI);
    c9_i348 = c9_b_i1->size[0];
    c9_i349 = c9_b_i1->size[1];
    c9_f_loop_ub = c9_i1->size[0] * c9_i1->size[1] - 1;
    for (c9_i352 = 0; c9_i352 <= c9_f_loop_ub; c9_i352++) {
      c9_b_i1->data[c9_i352] = c9_i1->data[c9_i352];
    }

    c9_emxInit_real32_T(chartInstance, &c9_d_a, 2, &c9_rd_emlrtRTEI);
    c9_i355 = c9_d_a->size[0] * c9_d_a->size[1];
    c9_d_a->size[0] = c9_a->size[0];
    c9_d_a->size[1] = c9_a->size[1];
    c9_emxEnsureCapacity_real32_T(chartInstance, c9_d_a, c9_i355,
      &c9_rd_emlrtRTEI);
    c9_i360 = c9_d_a->size[0];
    c9_i362 = c9_d_a->size[1];
    c9_g_loop_ub = c9_a->size[0] * c9_a->size[1] - 1;
    for (c9_i364 = 0; c9_i364 <= c9_g_loop_ub; c9_i364++) {
      c9_d_a->data[c9_i364] = c9_a->data[c9_i364];
    }

    c9_emxInit_real32_T1(chartInstance, &c9_b_varargin_1, 1, &c9_sd_emlrtRTEI);
    c9_hypot(chartInstance, c9_b_i1, c9_d_a, c9_b_a);
    c9_i369 = c9_b_varargin_1->size[0];
    c9_b_varargin_1->size[0] = c9_b_a->size[0] * c9_b_a->size[1];
    c9_emxEnsureCapacity_real32_T1(chartInstance, c9_b_varargin_1, c9_i369,
      &c9_sd_emlrtRTEI);
    c9_i_loop_ub = c9_b_a->size[0] * c9_b_a->size[1] - 1;
    c9_emxFree_real32_T(chartInstance, &c9_d_a);
    c9_emxFree_real32_T(chartInstance, &c9_b_i1);
    for (c9_i373 = 0; c9_i373 <= c9_i_loop_ub; c9_i373++) {
      c9_b_varargin_1->data[c9_i373] = c9_b_a->data[c9_i373];
    }

    if ((real_T)c9_b_varargin_1->size[0] >= 1.0) {
    } else {
      c9_c_y = NULL;
      sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                    false);
      c9_d_y = NULL;
      sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_d_y)));
    }

    c9_b_n = c9_b_varargin_1->size[0];
    if (c9_b_n <= 2) {
      if (c9_b_n == 1) {
        c9_magmax = c9_b_varargin_1->data[0];
      } else if (c9_b_varargin_1->data[0] < c9_b_varargin_1->data[1]) {
        c9_magmax = c9_b_varargin_1->data[1];
      } else {
        c9_g_x = c9_b_varargin_1->data[0];
        c9_b_b = muSingleScalarIsNaN(c9_g_x);
        if (c9_b_b) {
          c9_h_x = c9_b_varargin_1->data[1];
          c9_c_b = muSingleScalarIsNaN(c9_h_x);
          if (!c9_c_b) {
            c9_magmax = c9_b_varargin_1->data[1];
          } else {
            c9_magmax = c9_b_varargin_1->data[0];
          }
        } else {
          c9_magmax = c9_b_varargin_1->data[0];
        }
      }
    } else {
      c9_c_n = c9_b_varargin_1->size[0];
      c9_e_x = c9_b_varargin_1->data[0];
      c9_f_x = c9_e_x;
      c9_b = muSingleScalarIsNaN(c9_f_x);
      c9_p = !c9_b;
      if (c9_p) {
        c9_idx = 1;
      } else {
        c9_idx = 0;
        c9_d_b = c9_c_n;
        c9_e_b = c9_d_b;
        if (2 > c9_e_b) {
          c9_overflow = false;
        } else {
          c9_overflow = (c9_e_b > 2147483646);
        }

        if (c9_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        c9_b_k = 2;
        exitg1 = false;
        while ((!exitg1) && (c9_b_k <= c9_c_n)) {
          c9_i_x = c9_b_varargin_1->data[c9_b_k - 1];
          c9_j_x = c9_i_x;
          c9_h_b = muSingleScalarIsNaN(c9_j_x);
          c9_b_p = !c9_h_b;
          if (c9_b_p) {
            c9_idx = c9_b_k;
            exitg1 = true;
          } else {
            c9_b_k++;
          }
        }
      }

      if (c9_idx == 0) {
        c9_magmax = c9_b_varargin_1->data[0];
      } else {
        c9_first = c9_idx - 1;
        c9_last = c9_b_n;
        c9_ex = c9_b_varargin_1->data[c9_first];
        c9_i382 = c9_first + 2;
        c9_e_a = c9_i382;
        c9_f_b = c9_last;
        c9_f_a = c9_e_a;
        c9_g_b = c9_f_b;
        if (c9_f_a > c9_g_b) {
          c9_b_overflow = false;
        } else {
          c9_b_overflow = (c9_g_b > 2147483646);
        }

        if (c9_b_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_e_k = c9_i382 - 1; c9_e_k < c9_last; c9_e_k++) {
          if (c9_ex < c9_b_varargin_1->data[c9_e_k]) {
            c9_ex = c9_b_varargin_1->data[c9_e_k];
          }
        }

        c9_magmax = c9_ex;
      }
    }

    c9_emxFree_real32_T(chartInstance, &c9_b_varargin_1);
    if (c9_magmax > 0.0F) {
      c9_e_y = c9_magmax;
      c9_f_y = c9_e_y;
      c9_i378 = c9_b_a->size[0] * c9_b_a->size[1];
      c9_i379 = c9_b_a->size[0] * c9_b_a->size[1];
      c9_b_a->size[0];
      c9_b_a->size[1];
      c9_emxEnsureCapacity_real32_T(chartInstance, c9_b_a, c9_i379,
        &c9_td_emlrtRTEI);
      c9_i384 = c9_b_a->size[0];
      c9_i385 = c9_b_a->size[1];
      c9_i388 = c9_i378;
      c9_j_loop_ub = c9_i388 - 1;
      for (c9_i389 = 0; c9_i389 <= c9_j_loop_ub; c9_i389++) {
        c9_b_a->data[c9_i389] /= c9_f_y;
      }
    }

    c9_d_n = (real_T)c9_b_a->size[1];
    c9_b_m = (real_T)c9_b_a->size[0];
    c9_b18 = (c9_b_a->size[0] == 0);
    c9_b19 = (c9_b_a->size[1] == 0);
    if (c9_b18 || c9_b19) {
      for (c9_i383 = 0; c9_i383 < 64; c9_i383++) {
        c9_counts[c9_i383] = 0.0;
      }
    } else {
      c9_numCores = 1.0;
      getnumcores(&c9_numCores);
      if (((real_T)(c9_b_a->size[0] * c9_b_a->size[1]) > 500000.0) &&
          (c9_numCores > 1.0)) {
        c9_useParallel = true;
      } else {
        c9_useParallel = false;
      }

      c9_rowsA = (real_T)c9_b_a->size[0];
      c9_colsAEtc = (real_T)(c9_b_a->size[0] * c9_b_a->size[1]) / (real_T)
        c9_b_a->size[0];
      if (c9_useParallel) {
        c9_numelA = (real_T)(c9_b_a->size[0] * c9_b_a->size[1]);
        c9_numRows = c9_rowsA;
        c9_numCols = c9_colsAEtc;
        c9_b_nanFlag = false;
        c9_rngFlag = false;
        tbbhist_real32_scaled(&c9_b_a->data[0], c9_numelA, c9_numRows,
                              c9_numCols, c9_counts, 64.0, 1.0, 64.0,
                              &c9_rngFlag, &c9_b_nanFlag);
        c9_b_rngFlag = c9_rngFlag;
        c9_c_nanFlag = c9_b_nanFlag;
      } else {
        for (c9_i394 = 0; c9_i394 < 64; c9_i394++) {
          c9_counts[c9_i394] = 0.0;
        }

        c9_nanFlag = false;
        c9_d12 = (real_T)(c9_b_a->size[0] * c9_b_a->size[1]);
        c9_i395 = (int32_T)c9_d12 - 1;
        for (c9_m_i = 0; c9_m_i <= c9_i395; c9_m_i++) {
          c9_n_i = 1.0 + (real_T)c9_m_i;
          c9_l_x = c9_b_a->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_n_i, 1, c9_b_a->size[0] * c9_b_a->size[1])
            - 1];
          c9_i_b = muSingleScalarIsNaN(c9_l_x);
          if (c9_i_b) {
            c9_nanFlag = true;
            c9_c_idx = 0.0F;
          } else {
            c9_c_idx = c9_b_a->data[sf_eml_array_bounds_check
              (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
               MAX_uint32_T, (int32_T)c9_n_i, 1, c9_b_a->size[0] * c9_b_a->size
               [1]) - 1] * 63.0F + 0.5F;
          }

          if (c9_c_idx > 63.0F) {
            c9_counts[63]++;
          } else {
            c9_m_x = c9_b_a->data[sf_eml_array_bounds_check
              (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
               MAX_uint32_T, (int32_T)c9_n_i, 1, c9_b_a->size[0] * c9_b_a->size
               [1]) - 1];
            c9_j_b = muSingleScalarIsInf(c9_m_x);
            if (c9_j_b) {
              c9_counts[63]++;
            } else {
              c9_g_a = c9_c_idx;
              c9_c = (int32_T)c9_g_a;
              c9_h_a = c9_c_idx;
              c9_b_c = (int32_T)c9_h_a;
              c9_counts[c9_c] = c9_counts[c9_b_c] + 1.0;
            }
          }
        }

        c9_b_rngFlag = false;
        c9_c_nanFlag = c9_nanFlag;
      }

      if (c9_b_rngFlag) {
        c9_warning(chartInstance);
      }

      if (c9_c_nanFlag) {
        c9_b_warning(chartInstance);
      }
    }

    for (c9_c_k = 0; c9_c_k < 63; c9_c_k++) {
      c9_d_k = c9_c_k;
      c9_counts[c9_d_k + 1] += c9_counts[c9_d_k];
    }

    c9_d11 = 0.7 * c9_b_m * c9_d_n;
    for (c9_i392 = 0; c9_i392 < 64; c9_i392++) {
      c9_k_x[c9_i392] = (c9_counts[c9_i392] > c9_d11);
    }

    c9_b_idx = 0;
    c9_ii_size[0] = 1;
    c9_ii = 1;
    exitg1 = false;
    while ((!exitg1) && (c9_ii - 1 < 64)) {
      c9_b_ii = c9_ii;
      if (c9_k_x[c9_b_ii - 1]) {
        c9_b_idx = 1;
        c9_ii_data[0] = c9_b_ii;
        exitg1 = true;
      } else {
        c9_ii++;
      }
    }

    if (c9_b_idx == 0) {
      c9_i396 = c9_ii_size[0];
      c9_ii_size[0] = 0;
    }

    c9_highThreshTemp_size[0] = c9_ii_size[0];
    c9_k_loop_ub = c9_ii_size[0] - 1;
    for (c9_i397 = 0; c9_i397 <= c9_k_loop_ub; c9_i397++) {
      c9_highThreshTemp_data[c9_i397] = (real_T)c9_ii_data[c9_i397];
    }

    c9_highThreshTemp_size[0];
    c9_l_loop_ub = c9_highThreshTemp_size[0] - 1;
    for (c9_i398 = 0; c9_i398 <= c9_l_loop_ub; c9_i398++) {
      c9_highThreshTemp_data[c9_i398] /= 64.0;
    }

    if (c9_highThreshTemp_size[0] == 0) {
      c9_i399 = c9_highThreshTemp_size[0];
      c9_highThreshTemp_size[0] = 0;
      c9_i400 = c9_lowThresh_size[0];
      c9_lowThresh_size[0] = 0;
    } else {
      c9_d13 = c9_highThreshTemp_data[0];
      c9_highThreshTemp_size[0] = 1;
      c9_highThreshTemp_data[0] = c9_d13;
      c9_b_size[0] = c9_highThreshTemp_size[0];
      c9_m_loop_ub = c9_highThreshTemp_size[0] - 1;
      for (c9_i401 = 0; c9_i401 <= c9_m_loop_ub; c9_i401++) {
        c9_b_data[c9_i401] = c9_highThreshTemp_data[c9_i401];
      }

      c9_b_size[0] = 1;
      c9_b_data[0] *= 0.4;
      c9_lowThresh_size[0] = 1;
      c9_lowThresh_data[0] = c9_b_data[0];
    }

    c9_i402 = c9_varargout_1->size[0] * c9_varargout_1->size[1];
    c9_varargout_1->size[0] = (int32_T)c9_m;
    c9_varargout_1->size[1] = (int32_T)c9_n;
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_varargout_1, c9_i402,
      &c9_yd_emlrtRTEI);
    c9_i403 = c9_varargout_1->size[0];
    c9_i404 = c9_varargout_1->size[1];
    c9_n_loop_ub = (int32_T)c9_m * (int32_T)c9_n - 1;
    for (c9_i405 = 0; c9_i405 <= c9_n_loop_ub; c9_i405++) {
      c9_varargout_1->data[c9_i405] = false;
    }

    c9_b20 = (c9_varargout_1->size[0] == 1);
    c9_b21 = (c9_varargout_1->size[1] == 1);
    if ((!c9_b20) && (!c9_b21)) {
      c9_emxInit_boolean_T(chartInstance, &c9_E, 2, &c9_de_emlrtRTEI);
      c9_i406 = c9_E->size[0] * c9_E->size[1];
      c9_E->size[0] = c9_varargout_1->size[0];
      c9_E->size[1] = c9_varargout_1->size[1];
      c9_emxEnsureCapacity_boolean_T(chartInstance, c9_E, c9_i406,
        &c9_de_emlrtRTEI);
      c9_i407 = c9_E->size[0];
      c9_i408 = c9_E->size[1];
      c9_o_loop_ub = c9_varargout_1->size[0] * c9_varargout_1->size[1] - 1;
      for (c9_i409 = 0; c9_i409 <= c9_o_loop_ub; c9_i409++) {
        c9_E->data[c9_i409] = c9_varargout_1->data[c9_i409];
      }

      c9_emxInit_boolean_T(chartInstance, &c9_n_x, 2, &c9_ee_emlrtRTEI);
      c9_e_n = (real_T)c9_E->size[1];
      c9_c_m = (real_T)c9_E->size[0];
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_lowThresh_size[0]);
      c9_connDimsT[0] = c9_c_m;
      c9_connDimsT[1] = c9_e_n;
      c9_lowThresh = c9_lowThresh_data[0];
      c9_b_lowThresh = c9_lowThresh;
      cannythresholding_real32_tbb(&c9_i1->data[0], &c9_a->data[0],
        &c9_b_a->data[0], c9_connDimsT, c9_b_lowThresh, &c9_E->data[0]);
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_highThreshTemp_size[0]);
      c9_i410 = c9_n_x->size[0] * c9_n_x->size[1];
      c9_n_x->size[0] = c9_b_a->size[0];
      c9_n_x->size[1] = c9_b_a->size[1];
      c9_emxEnsureCapacity_boolean_T(chartInstance, c9_n_x, c9_i410,
        &c9_ee_emlrtRTEI);
      c9_highThreshTemp = c9_highThreshTemp_data[0];
      c9_i411 = c9_n_x->size[0];
      c9_i412 = c9_n_x->size[1];
      c9_p_loop_ub = c9_b_a->size[0] * c9_b_a->size[1] - 1;
      for (c9_i413 = 0; c9_i413 <= c9_p_loop_ub; c9_i413++) {
        c9_n_x->data[c9_i413] = ((real_T)c9_b_a->data[c9_i413] >
          c9_highThreshTemp);
      }

      for (c9_i414 = 0; c9_i414 < 2; c9_i414++) {
        c9_o_x[c9_i414] = c9_n_x->size[c9_i414];
      }

      for (c9_i415 = 0; c9_i415 < 2; c9_i415++) {
        c9_b_E[c9_i415] = c9_E->size[c9_i415];
      }

      _SFD_SIZE_EQ_CHECK_ND(c9_o_x, c9_b_E, 2);
      c9_i416 = c9_n_x->size[0] * c9_n_x->size[1];
      c9_i417 = c9_E->size[0] * c9_E->size[1];
      c9_i418 = c9_n_x->size[0] * c9_n_x->size[1];
      c9_n_x->size[0];
      c9_n_x->size[1];
      c9_emxEnsureCapacity_boolean_T(chartInstance, c9_n_x, c9_i418,
        &c9_ee_emlrtRTEI);
      c9_i419 = c9_n_x->size[0];
      c9_i420 = c9_n_x->size[1];
      c9_i421 = c9_i416;
      c9_i422 = c9_i417;
      c9_q_loop_ub = c9_i421 - 1;
      for (c9_i423 = 0; c9_i423 <= c9_q_loop_ub; c9_i423++) {
        c9_n_x->data[c9_i423] = (c9_n_x->data[c9_i423] && c9_E->data[c9_i423]);
      }

      c9_nx = c9_n_x->size[0] * c9_n_x->size[1];
      c9_b22 = (c9_n_x->size[0] == 1);
      c9_b23 = (c9_n_x->size[1] == 1);
      if (((!c9_b22) && (!c9_b23)) || ((real_T)c9_n_x->size[0] != 1.0) ||
          ((real_T)c9_n_x->size[1] <= 1.0)) {
        c9_b24 = true;
      } else {
        c9_b24 = false;
      }

      if (c9_b24) {
      } else {
        c9_g_y = NULL;
        sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv4, 10, 0U, 1U, 0U, 2, 1,
          36), false);
        c9_h_y = NULL;
        sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv4, 10, 0U, 1U, 0U, 2, 1,
          36), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                          c9_g_y, 14, sf_mex_call_debug
                          (sfGlobalDebugInstanceStruct, "getString", 1U, 1U, 14,
                           sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "message", 1U, 1U, 14, c9_h_y)));
      }

      c9_dn = (real_T)c9_n_x->size[1];
      c9_dm = (real_T)c9_n_x->size[0];
      c9_d_m = (int32_T)c9_dm;
      c9_f_n = (int32_T)c9_dn;
      c9_emxInit_int32_T(chartInstance, &c9_o_i, 1, &c9_pe_emlrtRTEI);
      c9_emxInit_int32_T(chartInstance, &c9_j, 1, &c9_pe_emlrtRTEI);
      if (c9_nx == 0) {
        c9_i424 = c9_o_i->size[0];
        c9_o_i->size[0] = 0;
        c9_i425 = c9_j->size[0];
        c9_j->size[0] = 0;
      } else {
        c9_emxInit_boolean_T1(chartInstance, &c9_v, 1, &c9_pe_emlrtRTEI);
        c9_f_k = c9_nx;
        c9_e_m = c9_d_m;
        c9_g_n = c9_f_n;
        c9_d_idx = 0;
        c9_i426 = c9_o_i->size[0];
        c9_o_i->size[0] = c9_f_k;
        c9_emxEnsureCapacity_int32_T(chartInstance, c9_o_i, c9_i426,
          &c9_ge_emlrtRTEI);
        c9_i428 = c9_j->size[0];
        c9_j->size[0] = c9_f_k;
        c9_emxEnsureCapacity_int32_T(chartInstance, c9_j, c9_i428,
          &c9_ge_emlrtRTEI);
        c9_indexSize[0] = c9_f_k;
        c9_indexSize[1] = 1;
        c9_i430 = c9_v->size[0];
        c9_v->size[0] = c9_indexSize[0];
        c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_v, c9_i430,
          &c9_ge_emlrtRTEI);
        c9_c_ii = 0;
        c9_jj = 0;
        exitg1 = false;
        while ((!exitg1) && (c9_jj + 1 <= c9_g_n)) {
          guard1 = false;
          if (c9_n_x->data[c9_c_ii + c9_n_x->size[0] * c9_jj]) {
            c9_d_idx++;
            c9_o_i->data[c9_d_idx - 1] = c9_c_ii + 1;
            c9_j->data[c9_d_idx - 1] = c9_jj + 1;
            c9_v->data[c9_d_idx - 1] = c9_n_x->data[c9_c_ii + c9_n_x->size[0] *
              c9_jj];
            if (c9_d_idx >= c9_f_k) {
              exitg1 = true;
            } else {
              guard1 = true;
            }
          } else {
            guard1 = true;
          }

          if (guard1) {
            c9_c_ii++;
            if (c9_c_ii + 1 > c9_e_m) {
              c9_c_ii = 0;
              c9_jj++;
            }
          }
        }

        if (c9_d_idx <= c9_f_k) {
        } else {
          c9_i_y = NULL;
          sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          c9_j_y = NULL;
          sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c9_i_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c9_j_y)));
        }

        if (c9_f_k == 1) {
          if (c9_d_idx == 0) {
            c9_i433 = c9_o_i->size[0];
            c9_o_i->size[0] = 0;
            c9_i436 = c9_j->size[0];
            c9_j->size[0] = 0;
          }
        } else {
          c9_b25 = (1 > c9_d_idx);
          if (c9_b25) {
            c9_i435 = 0;
          } else {
            c9_i435 = c9_d_idx;
          }

          c9_emxInit_int32_T(chartInstance, &c9_r12, 1, &c9_je_emlrtRTEI);
          c9_i437 = c9_r12->size[0];
          c9_r12->size[0] = c9_i435;
          c9_emxEnsureCapacity_int32_T(chartInstance, c9_r12, c9_i437,
            &c9_je_emlrtRTEI);
          c9_u_loop_ub = c9_i435 - 1;
          for (c9_i441 = 0; c9_i441 <= c9_u_loop_ub; c9_i441++) {
            c9_r12->data[c9_i441] = 1 + c9_i441;
          }

          c9_matrixSize = c9_o_i->size[0];
          c9_indexSize[0] = 1;
          c9_indexSize[1] = c9_r12->size[0];
          c9_size1 = c9_matrixSize;
          if (c9_size1 != 1) {
            c9_k_b = false;
          } else {
            c9_k_b = true;
          }

          if (c9_k_b) {
            c9_nonSingletonDimFound = false;
            if (c9_indexSize[1] != 1) {
              c9_nonSingletonDimFound = true;
            }

            c9_l_b = c9_nonSingletonDimFound;
            if (c9_l_b) {
              c9_c_c = true;
            } else {
              c9_c_c = false;
            }
          } else {
            c9_c_c = false;
          }

          c9_d_c = c9_c_c;
          if (!c9_d_c) {
          } else {
            c9_k_y = NULL;
            sf_mex_assign(&c9_k_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            c9_l_y = NULL;
            sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                              c9_k_y, 14, sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                               14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
              "message", 1U, 1U, 14, c9_l_y)));
          }

          c9_i442 = c9_o_i->size[0];
          c9_o_i->size[0] = c9_i435;
          c9_emxEnsureCapacity_int32_T(chartInstance, c9_o_i, c9_i442,
            &c9_ke_emlrtRTEI);
          c9_b26 = (1 > c9_d_idx);
          if (c9_b26) {
            c9_i443 = 0;
          } else {
            c9_i443 = c9_d_idx;
          }

          c9_i444 = c9_r12->size[0];
          c9_r12->size[0] = c9_i443;
          c9_emxEnsureCapacity_int32_T(chartInstance, c9_r12, c9_i444,
            &c9_le_emlrtRTEI);
          c9_v_loop_ub = c9_i443 - 1;
          for (c9_i445 = 0; c9_i445 <= c9_v_loop_ub; c9_i445++) {
            c9_r12->data[c9_i445] = 1 + c9_i445;
          }

          c9_b_matrixSize = c9_j->size[0];
          c9_indexSize[0] = 1;
          c9_indexSize[1] = c9_r12->size[0];
          c9_b_size1 = c9_b_matrixSize;
          if (c9_b_size1 != 1) {
            c9_m_b = false;
          } else {
            c9_m_b = true;
          }

          if (c9_m_b) {
            c9_b_nonSingletonDimFound = false;
            if (c9_indexSize[1] != 1) {
              c9_b_nonSingletonDimFound = true;
            }

            c9_n_b = c9_b_nonSingletonDimFound;
            if (c9_n_b) {
              c9_e_c = true;
            } else {
              c9_e_c = false;
            }
          } else {
            c9_e_c = false;
          }

          c9_f_c = c9_e_c;
          if (!c9_f_c) {
          } else {
            c9_m_y = NULL;
            sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            c9_n_y = NULL;
            sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                              c9_m_y, 14, sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                               14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
              "message", 1U, 1U, 14, c9_n_y)));
          }

          c9_i446 = c9_j->size[0];
          c9_j->size[0] = c9_i443;
          c9_emxEnsureCapacity_int32_T(chartInstance, c9_j, c9_i446,
            &c9_me_emlrtRTEI);
          c9_b27 = (1 > c9_d_idx);
          if (c9_b27) {
            c9_i447 = 0;
          } else {
            c9_i447 = c9_d_idx;
          }

          c9_i448 = c9_r12->size[0];
          c9_r12->size[0] = c9_i447;
          c9_emxEnsureCapacity_int32_T(chartInstance, c9_r12, c9_i448,
            &c9_ne_emlrtRTEI);
          c9_w_loop_ub = c9_i447 - 1;
          for (c9_i449 = 0; c9_i449 <= c9_w_loop_ub; c9_i449++) {
            c9_r12->data[c9_i449] = 1 + c9_i449;
          }

          c9_c_matrixSize = c9_v->size[0];
          c9_indexSize[0] = 1;
          c9_indexSize[1] = c9_r12->size[0];
          c9_c_size1 = c9_c_matrixSize;
          c9_emxFree_int32_T(chartInstance, &c9_r12);
          if (c9_c_size1 != 1) {
            c9_o_b = false;
          } else {
            c9_o_b = true;
          }

          if (c9_o_b) {
            c9_c_nonSingletonDimFound = false;
            if (c9_indexSize[1] != 1) {
              c9_c_nonSingletonDimFound = true;
            }

            c9_p_b = c9_c_nonSingletonDimFound;
            if (c9_p_b) {
              c9_g_c = true;
            } else {
              c9_g_c = false;
            }
          } else {
            c9_g_c = false;
          }

          c9_h_c = c9_g_c;
          if (!c9_h_c) {
          } else {
            c9_o_y = NULL;
            sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            c9_p_y = NULL;
            sf_mex_assign(&c9_p_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                              c9_o_y, 14, sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                               14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
              "message", 1U, 1U, 14, c9_p_y)));
          }
        }

        c9_emxFree_boolean_T(chartInstance, &c9_v);
      }

      c9_emxFree_boolean_T(chartInstance, &c9_n_x);
      c9_emxInit_real_T1(chartInstance, &c9_idxStrongR, 1, &c9_pe_emlrtRTEI);
      c9_i427 = c9_idxStrongR->size[0];
      c9_idxStrongR->size[0] = c9_o_i->size[0];
      c9_emxEnsureCapacity_real_T1(chartInstance, c9_idxStrongR, c9_i427,
        &c9_fe_emlrtRTEI);
      c9_r_loop_ub = c9_o_i->size[0] - 1;
      for (c9_i429 = 0; c9_i429 <= c9_r_loop_ub; c9_i429++) {
        c9_idxStrongR->data[c9_i429] = (real_T)c9_o_i->data[c9_i429];
      }

      c9_emxFree_int32_T(chartInstance, &c9_o_i);
      c9_emxInit_real_T1(chartInstance, &c9_idxStrongC, 1, &c9_pe_emlrtRTEI);
      c9_i431 = c9_idxStrongC->size[0];
      c9_idxStrongC->size[0] = c9_j->size[0];
      c9_emxEnsureCapacity_real_T1(chartInstance, c9_idxStrongC, c9_i431,
        &c9_he_emlrtRTEI);
      c9_s_loop_ub = c9_j->size[0] - 1;
      for (c9_i432 = 0; c9_i432 <= c9_s_loop_ub; c9_i432++) {
        c9_idxStrongC->data[c9_i432] = (real_T)c9_j->data[c9_i432];
      }

      c9_emxFree_int32_T(chartInstance, &c9_j);
      if (c9_idxStrongC->size[0] != 0) {
        c9_bwselect(chartInstance, c9_E, c9_idxStrongC, c9_idxStrongR,
                    c9_varargout_1);
      } else {
        c9_i434 = c9_varargout_1->size[0] * c9_varargout_1->size[1];
        c9_varargout_1->size[0] = (int32_T)c9_c_m;
        c9_varargout_1->size[1] = (int32_T)c9_e_n;
        c9_emxEnsureCapacity_boolean_T(chartInstance, c9_varargout_1, c9_i434,
          &c9_ie_emlrtRTEI);
        c9_i438 = c9_varargout_1->size[0];
        c9_i439 = c9_varargout_1->size[1];
        c9_t_loop_ub = (int32_T)c9_c_m * (int32_T)c9_e_n - 1;
        for (c9_i440 = 0; c9_i440 <= c9_t_loop_ub; c9_i440++) {
          c9_varargout_1->data[c9_i440] = false;
        }
      }

      c9_emxFree_real_T(chartInstance, &c9_idxStrongC);
      c9_emxFree_real_T(chartInstance, &c9_idxStrongR);
      c9_emxFree_boolean_T(chartInstance, &c9_E);
    }

    c9_emxFree_real32_T(chartInstance, &c9_i1);
    c9_emxFree_real32_T(chartInstance, &c9_b_a);
    (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_lowThresh_size[0]);
    (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_highThreshTemp_size[0]);
  }

  c9_emxFree_real32_T(chartInstance, &c9_a);
}

static void c9_error(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c9_y = NULL;
  static char_T c9_cv22[30] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'E', 'l', 'F', 'u', 'n', 'D', 'o', 'm', 'a', 'i', 'n',
    'E', 'r', 'r', 'o', 'r' };

  const mxArray *c9_b_y = NULL;
  const mxArray *c9_c_y = NULL;
  static char_T c9_cv23[4] = { 's', 'q', 'r', 't' };

  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv22, 10, 0U, 1U, 0U, 2, 1, 30),
                false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv22, 10, 0U, 1U, 0U, 2, 1, 30),
                false);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv23, 10, 0U, 1U, 0U, 2, 1, 4),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
    1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U, 2U,
    14, c9_b_y, 14, c9_c_y)));
}

static void c9_padImage(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T *c9_a_tmp, real_T c9_pad[2], c9_emxArray_real32_T *c9_a)
{
  boolean_T c9_p;
  int32_T c9_k;
  real_T c9_b_k;
  real_T c9_x;
  boolean_T c9_b28;
  real_T c9_b_x;
  boolean_T c9_b;
  boolean_T c9_b_p;
  const mxArray *c9_y = NULL;
  static char_T c9_cv24[30] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'p', 'a', 'd',
    'a', 'r', 'r', 'a', 'y', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N',
    'o', 'n', 'N', 'a', 'N' };

  int32_T c9_i450;
  const mxArray *c9_b_y = NULL;
  const mxArray *c9_c_y = NULL;
  real_T c9_b_pad[2];
  static char_T c9_cv25[24] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'P', 'A', 'D', 'S', 'I', 'Z', 'E', ',' };

  boolean_T c9_b29;
  const mxArray *c9_d_y = NULL;
  static char_T c9_cv26[31] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'p', 'a', 'd',
    'a', 'r', 'r', 'a', 'y', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'I',
    'n', 't', 'e', 'g', 'e', 'r' };

  boolean_T c9_b30;
  const mxArray *c9_e_y = NULL;
  boolean_T c9_b31;
  const mxArray *c9_f_y = NULL;
  int32_T c9_i451;
  int32_T c9_i452;
  static char_T c9_cv27[24] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'P', 'A', 'D', 'S', 'I', 'Z', 'E', ',' };

  int32_T c9_i453;
  real_T c9_sizeA[2];
  int32_T c9_i454;
  real_T c9_b_b[2];
  int32_T c9_i455;
  int32_T c9_i456;
  int32_T c9_i457;
  real_T c9_b_sizeA[2];
  real_T c9_varargin_1[2];
  int32_T c9_i458;
  int32_T c9_c_k;
  real_T c9_c_x;
  boolean_T c9_c_p;
  real_T c9_d_k;
  boolean_T c9_c_b;
  real_T c9_maxval;
  real_T c9_d_x;
  real_T c9_e_x;
  c9_emxArray_int32_T *c9_idxA;
  int32_T c9_e_k;
  boolean_T c9_d_b;
  boolean_T c9_e_b;
  int32_T c9_outsize[2];
  boolean_T c9_b32;
  int32_T c9_i459;
  boolean_T c9_d_p;
  real_T c9_f_k;
  const mxArray *c9_g_y = NULL;
  static char_T c9_cv28[57] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'a', 's', 's', 'e', 'r', 't', '_',
    'v', 'a', 'l', 'i', 'd', '_', 's', 'i', 'z', 'e', '_', 'a', 'r', 'g', '_',
    'i', 'n', 'v', 'a', 'l', 'i', 'd', 'S', 'i', 'z', 'e', 'V', 'e', 'c', 't',
    'o', 'r' };

  real_T c9_n;
  const mxArray *c9_h_y = NULL;
  int32_T c9_g_k;
  int32_T c9_onesVector_size[2];
  int32_T c9_b_u;
  int32_T c9_i460;
  const mxArray *c9_i_y = NULL;
  real_T c9_h_k;
  int32_T c9_i461;
  const mxArray *c9_j_y = NULL;
  int32_T c9_loop_ub;
  int32_T c9_c_u;
  c9_emxArray_boolean_T *c9_tile;
  int32_T c9_i462;
  const mxArray *c9_k_y = NULL;
  const mxArray *c9_l_y = NULL;
  int32_T c9_i463;
  c9_emxArray_real_T *c9_m_y;
  real_T c9_d14;
  real_T c9_d;
  int32_T c9_i464;
  real_T c9_f_b;
  int32_T c9_i465;
  real_T c9_f_x;
  uint32_T c9_u0;
  int32_T c9_b_loop_ub;
  boolean_T c9_g_b;
  int32_T c9_i466;
  int32_T c9_i467;
  uint32_T c9_onesVector_data[6];
  real_T c9_g_x;
  int32_T c9_i468;
  boolean_T c9_h_b;
  const mxArray *c9_n_y = NULL;
  int32_T c9_i469;
  int32_T c9_i470;
  int32_T c9_i471;
  int32_T c9_i472;
  int32_T c9_i473;
  int32_T c9_i474;
  const mxArray *c9_o_y = NULL;
  real_T c9_b_a;
  int32_T c9_i475;
  int32_T c9_c_loop_ub;
  int32_T c9_i476;
  int32_T c9_y_size[2];
  int32_T c9_i477;
  int32_T c9_i478;
  int32_T c9_i479;
  int32_T c9_i480;
  int32_T c9_i481;
  int32_T c9_d_loop_ub;
  int32_T c9_i482;
  int32_T c9_i483;
  int32_T c9_e_loop_ub;
  int32_T c9_i484;
  c9_emxArray_uint32_T *c9_idxDir;
  real_T c9_d15;
  int32_T c9_i485;
  uint32_T c9_u1;
  int32_T c9_f_loop_ub;
  int32_T c9_i486;
  uint32_T c9_y_data[6];
  int32_T c9_g_loop_ub;
  int32_T c9_i487;
  int32_T c9_h_loop_ub;
  real_T c9_d16;
  int32_T c9_i488;
  uint32_T c9_u2;
  real_T c9_d17;
  boolean_T c9_b33;
  int32_T c9_i489;
  c9_emxArray_int32_T *c9_r13;
  int32_T c9_i490;
  int32_T c9_i_loop_ub;
  int32_T c9_i491;
  c9_emxArray_int32_T *c9_b_idxDir;
  int32_T c9_iv4[1];
  int32_T c9_i492;
  int32_T c9_i493;
  int32_T c9_i494;
  int32_T c9_j_loop_ub;
  int32_T c9_i495;
  int32_T c9_iv5[1];
  int32_T c9_k_loop_ub;
  int32_T c9_i496;
  int32_T c9_i497;
  int32_T c9_i498;
  int32_T c9_l_loop_ub;
  int32_T c9_i499;
  real_T c9_b_d;
  real_T c9_d18;
  real_T c9_i_b;
  real_T c9_h_x;
  boolean_T c9_j_b;
  uint32_T c9_u3;
  int32_T c9_i500;
  real_T c9_i_x;
  int32_T c9_i501;
  boolean_T c9_k_b;
  int32_T c9_i502;
  int32_T c9_i503;
  int32_T c9_i504;
  int32_T c9_i505;
  int32_T c9_i506;
  int32_T c9_i507;
  real_T c9_c_a;
  int32_T c9_m_loop_ub;
  int32_T c9_i508;
  int32_T c9_i509;
  int32_T c9_i510;
  int32_T c9_i511;
  int32_T c9_i512;
  int32_T c9_n_loop_ub;
  int32_T c9_i513;
  int32_T c9_i514;
  real_T c9_d19;
  uint32_T c9_u4;
  int32_T c9_o_loop_ub;
  int32_T c9_i515;
  int32_T c9_p_loop_ub;
  int32_T c9_i516;
  int32_T c9_q_loop_ub;
  real_T c9_d20;
  int32_T c9_i517;
  uint32_T c9_u5;
  real_T c9_d21;
  boolean_T c9_b34;
  int32_T c9_i518;
  int32_T c9_i519;
  int32_T c9_r_loop_ub;
  int32_T c9_i520;
  int32_T c9_iv6[1];
  int32_T c9_i521;
  int32_T c9_i522;
  int32_T c9_i523;
  int32_T c9_s_loop_ub;
  int32_T c9_i524;
  int32_T c9_iv7[1];
  int32_T c9_t_loop_ub;
  int32_T c9_i525;
  int32_T c9_i526;
  int32_T c9_i527;
  int32_T c9_i528;
  int32_T c9_i529;
  real_T c9_d22;
  int32_T c9_i530;
  int32_T c9_j;
  real_T c9_b_j;
  real_T c9_d23;
  int32_T c9_i531;
  int32_T c9_i;
  real_T c9_b_i;
  boolean_T exitg1;
  int32_T exitg2;
  c9_p = true;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k < 2)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_pad[(int32_T)c9_b_k - 1];
    c9_b_x = c9_x;
    c9_b = muDoubleScalarIsNaN(c9_b_x);
    c9_b_p = !c9_b;
    if (c9_b_p) {
      c9_k++;
    } else {
      c9_p = false;
      exitg1 = true;
    }
  }

  if (c9_p) {
    c9_b28 = true;
  } else {
    c9_b28 = false;
  }

  if (c9_b28) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv24, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv6, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv25, 10, 0U, 1U, 0U, 2, 1, 24),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c9_b_y, 14, c9_c_y)));
  }

  for (c9_i450 = 0; c9_i450 < 2; c9_i450++) {
    c9_b_pad[c9_i450] = c9_pad[c9_i450];
  }

  if (c9_all(chartInstance, c9_b_pad)) {
    c9_b29 = true;
  } else {
    c9_b29 = false;
  }

  if (c9_b29) {
  } else {
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv26, 10, 0U, 1U, 0U, 2, 1, 31),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv7, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv27, 10, 0U, 1U, 0U, 2, 1, 24),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_e_y, 14, c9_f_y)));
  }

  c9_b30 = (c9_a_tmp->size[0] == 0);
  c9_b31 = (c9_a_tmp->size[1] == 0);
  if (c9_b30 || c9_b31) {
    for (c9_i452 = 0; c9_i452 < 2; c9_i452++) {
      c9_sizeA[c9_i452] = (real_T)c9_a_tmp->size[c9_i452];
    }

    for (c9_i454 = 0; c9_i454 < 2; c9_i454++) {
      c9_sizeA[c9_i454];
    }

    for (c9_i455 = 0; c9_i455 < 2; c9_i455++) {
      c9_b_b[c9_i455] = 2.0 * c9_pad[c9_i455];
    }

    for (c9_i457 = 0; c9_i457 < 2; c9_i457++) {
      c9_sizeA[c9_i457] += c9_b_b[c9_i457];
    }

    c9_varargin_1[0] = c9_sizeA[0];
    c9_varargin_1[1] = c9_sizeA[1];
    c9_c_k = 0;
    do {
      exitg2 = 0;
      if (c9_c_k < 2) {
        c9_d_k = 1.0 + (real_T)c9_c_k;
        if (c9_varargin_1[(int32_T)c9_d_k - 1] != c9_varargin_1[(int32_T)c9_d_k
            - 1]) {
          c9_c_p = false;
          exitg2 = 1;
        } else {
          c9_d_x = c9_varargin_1[(int32_T)c9_d_k - 1];
          c9_d_b = muDoubleScalarIsInf(c9_d_x);
          if (c9_d_b) {
            c9_c_p = false;
            exitg2 = 1;
          } else {
            c9_c_k++;
          }
        }
      } else {
        c9_c_p = true;
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    if (c9_c_p) {
      c9_e_k = 0;
      do {
        exitg2 = 0;
        if (c9_e_k < 2) {
          c9_f_k = 1.0 + (real_T)c9_e_k;
          if (c9_varargin_1[(int32_T)c9_f_k - 1] > 2.147483647E+9) {
            c9_d_p = false;
            exitg2 = 1;
          } else {
            c9_e_k++;
          }
        } else {
          c9_d_p = true;
          exitg2 = 1;
        }
      } while (exitg2 == 0);

      if (c9_d_p) {
        c9_b32 = true;
      } else {
        c9_b32 = false;
      }
    } else {
      c9_b32 = false;
    }

    if (c9_b32) {
    } else {
      c9_g_y = NULL;
      sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv28, 10, 0U, 1U, 0U, 2, 1,
        57), false);
      c9_h_y = NULL;
      sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv28, 10, 0U, 1U, 0U, 2, 1,
        57), false);
      c9_b_u = MIN_int32_T;
      c9_i_y = NULL;
      sf_mex_assign(&c9_i_y, sf_mex_create("y", &c9_b_u, 6, 0U, 0U, 0U, 0),
                    false);
      c9_c_u = MAX_int32_T;
      c9_k_y = NULL;
      sf_mex_assign(&c9_k_y, sf_mex_create("y", &c9_c_u, 6, 0U, 0U, 0U, 0),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_g_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 3U, 14, c9_h_y, 14, c9_i_y, 14, c9_k_y)));
    }

    c9_n = 1.0;
    for (c9_g_k = 0; c9_g_k < 2; c9_g_k++) {
      c9_h_k = 1.0 + (real_T)c9_g_k;
      if (c9_varargin_1[(int32_T)c9_h_k - 1] <= 0.0) {
        c9_n = 0.0;
      } else {
        c9_n *= c9_varargin_1[(int32_T)c9_h_k - 1];
      }
    }

    if (c9_n <= 2.147483647E+9) {
    } else {
      c9_j_y = NULL;
      sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv3, 10, 0U, 1U, 0U, 2, 1, 21),
                    false);
      c9_l_y = NULL;
      sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv3, 10, 0U, 1U, 0U, 2, 1, 21),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_j_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_l_y)));
    }

    c9_emxInit_boolean_T(chartInstance, &c9_tile, 2, &c9_te_emlrtRTEI);
    c9_i463 = c9_tile->size[0] * c9_tile->size[1];
    c9_tile->size[0] = (int32_T)c9_varargin_1[0];
    c9_tile->size[1] = (int32_T)c9_varargin_1[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_tile, c9_i463,
      &c9_te_emlrtRTEI);
    c9_i464 = c9_tile->size[0];
    c9_i465 = c9_tile->size[1];
    c9_b_loop_ub = (int32_T)c9_varargin_1[0] * (int32_T)c9_varargin_1[1] - 1;
    for (c9_i466 = 0; c9_i466 <= c9_b_loop_ub; c9_i466++) {
      c9_tile->data[c9_i466] = false;
    }

    c9_outsize[0] = c9_tile->size[0];
    if ((real_T)c9_outsize[0] == (real_T)c9_tile->size[0]) {
    } else {
      c9_n_y = NULL;
      sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv8, 10, 0U, 1U, 0U, 2, 1, 15),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c9_n_y);
    }

    c9_outsize[1] = c9_tile->size[1];
    if ((real_T)c9_outsize[1] == (real_T)c9_tile->size[1]) {
    } else {
      c9_o_y = NULL;
      sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv8, 10, 0U, 1U, 0U, 2, 1, 15),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c9_o_y);
    }

    c9_emxFree_boolean_T(chartInstance, &c9_tile);
    c9_i475 = c9_a->size[0] * c9_a->size[1];
    c9_a->size[0] = c9_outsize[0];
    c9_a->size[1] = c9_outsize[1];
    c9_emxEnsureCapacity_real32_T(chartInstance, c9_a, c9_i475, &c9_ve_emlrtRTEI);
    c9_i481 = c9_a->size[0];
    c9_i482 = c9_a->size[1];
    c9_e_loop_ub = c9_outsize[0] * c9_outsize[1] - 1;
    for (c9_i484 = 0; c9_i484 <= c9_e_loop_ub; c9_i484++) {
      c9_a->data[c9_i484] = 0.0F;
    }
  } else {
    for (c9_i451 = 0; c9_i451 < 2; c9_i451++) {
      c9_sizeA[c9_i451] = (real_T)c9_a_tmp->size[c9_i451];
    }

    for (c9_i453 = 0; c9_i453 < 2; c9_i453++) {
      c9_sizeA[c9_i453];
    }

    c9_b_b[0] = c9_pad[0];
    c9_b_b[1] = c9_pad[1];
    for (c9_i456 = 0; c9_i456 < 2; c9_i456++) {
      c9_b_b[c9_i456] *= 2.0;
    }

    c9_b_sizeA[0] = c9_sizeA[0];
    c9_b_sizeA[1] = c9_sizeA[1];
    for (c9_i458 = 0; c9_i458 < 2; c9_i458++) {
      c9_varargin_1[c9_i458] = c9_b_b[c9_i458] + c9_b_sizeA[c9_i458];
    }

    if (c9_varargin_1[0] < c9_varargin_1[1]) {
      c9_maxval = c9_varargin_1[1];
    } else {
      c9_c_x = c9_varargin_1[0];
      c9_c_b = muDoubleScalarIsNaN(c9_c_x);
      if (c9_c_b) {
        c9_e_x = c9_varargin_1[1];
        c9_e_b = muDoubleScalarIsNaN(c9_e_x);
        if (!c9_e_b) {
          c9_maxval = c9_varargin_1[1];
        } else {
          c9_maxval = c9_varargin_1[0];
        }
      } else {
        c9_maxval = c9_varargin_1[0];
      }
    }

    c9_emxInit_int32_T1(chartInstance, &c9_idxA, 2, &c9_re_emlrtRTEI);
    c9_outsize[0] = (int32_T)sf_integer_check(chartInstance->S, 1U, 0U, 0U,
      c9_maxval);
    c9_outsize[1] = 2;
    c9_i459 = c9_idxA->size[0] * c9_idxA->size[1];
    c9_idxA->size[0] = c9_outsize[0];
    c9_idxA->size[1] = c9_outsize[1];
    c9_emxEnsureCapacity_int32_T1(chartInstance, c9_idxA, c9_i459,
      &c9_re_emlrtRTEI);
    c9_onesVector_size[0] = 1;
    c9_onesVector_size[1] = (int32_T)c9_pad[0];
    c9_i460 = c9_onesVector_size[0];
    c9_i461 = c9_onesVector_size[1];
    c9_loop_ub = (int32_T)c9_pad[0] - 1;
    for (c9_i462 = 0; c9_i462 <= c9_loop_ub; c9_i462++) {
      c9_d14 = muDoubleScalarRound(1.0);
      if (c9_d14 < 4.294967296E+9) {
        if (c9_d14 >= 0.0) {
          c9_u0 = (uint32_T)c9_d14;
        } else {
          c9_u0 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c9_d14 >= 4.294967296E+9) {
        c9_u0 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c9_u0 = 0U;
      }

      c9_onesVector_data[c9_i462] = c9_u0;
    }

    c9_emxInit_real_T(chartInstance, &c9_m_y, 2, &c9_df_emlrtRTEI);
    c9_d = c9_sizeA[0];
    c9_f_b = c9_d;
    c9_f_x = c9_f_b;
    c9_g_b = muDoubleScalarIsNaN(c9_f_x);
    if (c9_g_b) {
      c9_i467 = c9_m_y->size[0] * c9_m_y->size[1];
      c9_m_y->size[0] = 1;
      c9_m_y->size[1] = 1;
      c9_emxEnsureCapacity_real_T(chartInstance, c9_m_y, c9_i467,
        &c9_ue_emlrtRTEI);
      c9_i470 = c9_m_y->size[0];
      c9_i474 = c9_m_y->size[1];
      c9_m_y->data[0] = rtNaN;
    } else if (c9_f_b < 1.0) {
      c9_i468 = c9_m_y->size[0] * c9_m_y->size[1];
      c9_m_y->size[0] = 1;
      c9_m_y->size[1] = 0;
      c9_i469 = c9_m_y->size[0];
      c9_i473 = c9_m_y->size[1];
    } else {
      c9_g_x = c9_f_b;
      c9_h_b = muDoubleScalarIsInf(c9_g_x);
      if (c9_h_b && (1.0 == c9_f_b)) {
        c9_i472 = c9_m_y->size[0] * c9_m_y->size[1];
        c9_m_y->size[0] = 1;
        c9_m_y->size[1] = 1;
        c9_emxEnsureCapacity_real_T(chartInstance, c9_m_y, c9_i472,
          &c9_ue_emlrtRTEI);
        c9_i476 = c9_m_y->size[0];
        c9_i478 = c9_m_y->size[1];
        c9_m_y->data[0] = rtNaN;
      } else {
        c9_i471 = c9_m_y->size[0] * c9_m_y->size[1];
        c9_m_y->size[0] = 1;
        c9_m_y->size[1] = (int32_T)muDoubleScalarFloor(c9_f_b - 1.0) + 1;
        c9_emxEnsureCapacity_real_T(chartInstance, c9_m_y, c9_i471,
          &c9_ue_emlrtRTEI);
        c9_c_loop_ub = (int32_T)muDoubleScalarFloor(c9_f_b - 1.0);
        for (c9_i477 = 0; c9_i477 <= c9_c_loop_ub; c9_i477++) {
          c9_m_y->data[c9_i477] = 1.0 + (real_T)c9_i477;
        }
      }
    }

    c9_b_a = c9_sizeA[0];
    c9_y_size[0] = 1;
    c9_y_size[1] = c9_onesVector_size[1];
    c9_i479 = c9_y_size[0];
    c9_i480 = c9_y_size[1];
    c9_d_loop_ub = c9_onesVector_size[0] * c9_onesVector_size[1] - 1;
    for (c9_i483 = 0; c9_i483 <= c9_d_loop_ub; c9_i483++) {
      c9_d15 = muDoubleScalarRound(c9_b_a * (real_T)c9_onesVector_data[c9_i483]);
      if (c9_d15 < 4.294967296E+9) {
        if (c9_d15 >= 0.0) {
          c9_u1 = (uint32_T)c9_d15;
        } else {
          c9_u1 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c9_d15 >= 4.294967296E+9) {
        c9_u1 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c9_u1 = 0U;
      }

      c9_y_data[c9_i483] = c9_u1;
    }

    c9_emxInit_uint32_T(chartInstance, &c9_idxDir, 2, &c9_bf_emlrtRTEI);
    c9_i485 = c9_idxDir->size[0] * c9_idxDir->size[1];
    c9_idxDir->size[0] = 1;
    c9_idxDir->size[1] = (c9_onesVector_size[1] + c9_m_y->size[1]) + c9_y_size[1];
    c9_emxEnsureCapacity_uint32_T(chartInstance, c9_idxDir, c9_i485,
      &c9_we_emlrtRTEI);
    c9_f_loop_ub = c9_onesVector_size[1] - 1;
    for (c9_i486 = 0; c9_i486 <= c9_f_loop_ub; c9_i486++) {
      c9_idxDir->data[c9_i486] = c9_onesVector_data[c9_i486];
    }

    c9_g_loop_ub = c9_m_y->size[1] - 1;
    for (c9_i487 = 0; c9_i487 <= c9_g_loop_ub; c9_i487++) {
      c9_d16 = muDoubleScalarRound(c9_m_y->data[c9_i487]);
      if (c9_d16 < 4.294967296E+9) {
        if (c9_d16 >= 0.0) {
          c9_u2 = (uint32_T)c9_d16;
        } else {
          c9_u2 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c9_d16 >= 4.294967296E+9) {
        c9_u2 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c9_u2 = 0U;
      }

      c9_idxDir->data[c9_i487 + c9_onesVector_size[1]] = c9_u2;
    }

    c9_h_loop_ub = c9_y_size[1] - 1;
    for (c9_i488 = 0; c9_i488 <= c9_h_loop_ub; c9_i488++) {
      c9_idxDir->data[(c9_i488 + c9_onesVector_size[1]) + c9_m_y->size[1]] =
        c9_y_data[c9_i488];
    }

    c9_d17 = (real_T)c9_idxDir->size[1];
    c9_b33 = (1.0 > c9_d17);
    if (c9_b33) {
      c9_i489 = 0;
    } else {
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_idxA->size[0]);
      c9_i489 = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_d17, 1,
        c9_idxA->size[0]);
    }

    c9_emxInit_int32_T(chartInstance, &c9_r13, 1, &c9_cf_emlrtRTEI);
    c9_i490 = c9_r13->size[0];
    c9_r13->size[0] = c9_i489;
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r13, c9_i490,
      &c9_xe_emlrtRTEI);
    c9_i_loop_ub = c9_i489 - 1;
    for (c9_i491 = 0; c9_i491 <= c9_i_loop_ub; c9_i491++) {
      c9_r13->data[c9_i491] = c9_i491;
    }

    c9_emxInit_int32_T1(chartInstance, &c9_b_idxDir, 2, &c9_ye_emlrtRTEI);
    c9_iv4[0] = c9_r13->size[0];
    _SFD_SUB_ASSIGN_SIZE_CHECK_ND(c9_iv4, 1, *(int32_T (*)[2])c9_idxDir->size, 2);
    c9_i492 = c9_b_idxDir->size[0] * c9_b_idxDir->size[1];
    c9_b_idxDir->size[0] = 1;
    c9_b_idxDir->size[1] = c9_idxDir->size[1];
    c9_emxEnsureCapacity_int32_T1(chartInstance, c9_b_idxDir, c9_i492,
      &c9_ye_emlrtRTEI);
    c9_i493 = c9_b_idxDir->size[0];
    c9_i494 = c9_b_idxDir->size[1];
    c9_j_loop_ub = c9_idxDir->size[0] * c9_idxDir->size[1] - 1;
    for (c9_i495 = 0; c9_i495 <= c9_j_loop_ub; c9_i495++) {
      c9_b_idxDir->data[c9_i495] = (int32_T)c9_idxDir->data[c9_i495];
    }

    c9_iv5[0] = c9_r13->size[0];
    c9_k_loop_ub = c9_iv5[0] - 1;
    for (c9_i496 = 0; c9_i496 <= c9_k_loop_ub; c9_i496++) {
      c9_idxA->data[c9_r13->data[c9_i496]] = c9_b_idxDir->data[c9_i496];
    }

    c9_onesVector_size[0] = 1;
    c9_onesVector_size[1] = (int32_T)c9_pad[1];
    c9_i497 = c9_onesVector_size[0];
    c9_i498 = c9_onesVector_size[1];
    c9_l_loop_ub = (int32_T)c9_pad[1] - 1;
    for (c9_i499 = 0; c9_i499 <= c9_l_loop_ub; c9_i499++) {
      c9_d18 = muDoubleScalarRound(1.0);
      if (c9_d18 < 4.294967296E+9) {
        if (c9_d18 >= 0.0) {
          c9_u3 = (uint32_T)c9_d18;
        } else {
          c9_u3 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c9_d18 >= 4.294967296E+9) {
        c9_u3 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c9_u3 = 0U;
      }

      c9_onesVector_data[c9_i499] = c9_u3;
    }

    c9_b_d = c9_sizeA[1];
    c9_i_b = c9_b_d;
    c9_h_x = c9_i_b;
    c9_j_b = muDoubleScalarIsNaN(c9_h_x);
    if (c9_j_b) {
      c9_i500 = c9_m_y->size[0] * c9_m_y->size[1];
      c9_m_y->size[0] = 1;
      c9_m_y->size[1] = 1;
      c9_emxEnsureCapacity_real_T(chartInstance, c9_m_y, c9_i500,
        &c9_ue_emlrtRTEI);
      c9_i503 = c9_m_y->size[0];
      c9_i507 = c9_m_y->size[1];
      c9_m_y->data[0] = rtNaN;
    } else if (c9_i_b < 1.0) {
      c9_i501 = c9_m_y->size[0] * c9_m_y->size[1];
      c9_m_y->size[0] = 1;
      c9_m_y->size[1] = 0;
      c9_i502 = c9_m_y->size[0];
      c9_i506 = c9_m_y->size[1];
    } else {
      c9_i_x = c9_i_b;
      c9_k_b = muDoubleScalarIsInf(c9_i_x);
      if (c9_k_b && (1.0 == c9_i_b)) {
        c9_i505 = c9_m_y->size[0] * c9_m_y->size[1];
        c9_m_y->size[0] = 1;
        c9_m_y->size[1] = 1;
        c9_emxEnsureCapacity_real_T(chartInstance, c9_m_y, c9_i505,
          &c9_ue_emlrtRTEI);
        c9_i508 = c9_m_y->size[0];
        c9_i510 = c9_m_y->size[1];
        c9_m_y->data[0] = rtNaN;
      } else {
        c9_i504 = c9_m_y->size[0] * c9_m_y->size[1];
        c9_m_y->size[0] = 1;
        c9_m_y->size[1] = (int32_T)muDoubleScalarFloor(c9_i_b - 1.0) + 1;
        c9_emxEnsureCapacity_real_T(chartInstance, c9_m_y, c9_i504,
          &c9_ue_emlrtRTEI);
        c9_m_loop_ub = (int32_T)muDoubleScalarFloor(c9_i_b - 1.0);
        for (c9_i509 = 0; c9_i509 <= c9_m_loop_ub; c9_i509++) {
          c9_m_y->data[c9_i509] = 1.0 + (real_T)c9_i509;
        }
      }
    }

    c9_c_a = c9_sizeA[1];
    c9_y_size[0] = 1;
    c9_y_size[1] = c9_onesVector_size[1];
    c9_i511 = c9_y_size[0];
    c9_i512 = c9_y_size[1];
    c9_n_loop_ub = c9_onesVector_size[0] * c9_onesVector_size[1] - 1;
    for (c9_i513 = 0; c9_i513 <= c9_n_loop_ub; c9_i513++) {
      c9_d19 = muDoubleScalarRound(c9_c_a * (real_T)c9_onesVector_data[c9_i513]);
      if (c9_d19 < 4.294967296E+9) {
        if (c9_d19 >= 0.0) {
          c9_u4 = (uint32_T)c9_d19;
        } else {
          c9_u4 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c9_d19 >= 4.294967296E+9) {
        c9_u4 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c9_u4 = 0U;
      }

      c9_y_data[c9_i513] = c9_u4;
    }

    c9_i514 = c9_idxDir->size[0] * c9_idxDir->size[1];
    c9_idxDir->size[0] = 1;
    c9_idxDir->size[1] = (c9_onesVector_size[1] + c9_m_y->size[1]) + c9_y_size[1];
    c9_emxEnsureCapacity_uint32_T(chartInstance, c9_idxDir, c9_i514,
      &c9_we_emlrtRTEI);
    c9_o_loop_ub = c9_onesVector_size[1] - 1;
    for (c9_i515 = 0; c9_i515 <= c9_o_loop_ub; c9_i515++) {
      c9_idxDir->data[c9_i515] = c9_onesVector_data[c9_i515];
    }

    c9_p_loop_ub = c9_m_y->size[1] - 1;
    for (c9_i516 = 0; c9_i516 <= c9_p_loop_ub; c9_i516++) {
      c9_d20 = muDoubleScalarRound(c9_m_y->data[c9_i516]);
      if (c9_d20 < 4.294967296E+9) {
        if (c9_d20 >= 0.0) {
          c9_u5 = (uint32_T)c9_d20;
        } else {
          c9_u5 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c9_d20 >= 4.294967296E+9) {
        c9_u5 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c9_u5 = 0U;
      }

      c9_idxDir->data[c9_i516 + c9_onesVector_size[1]] = c9_u5;
    }

    c9_q_loop_ub = c9_y_size[1] - 1;
    for (c9_i517 = 0; c9_i517 <= c9_q_loop_ub; c9_i517++) {
      c9_idxDir->data[(c9_i517 + c9_onesVector_size[1]) + c9_m_y->size[1]] =
        c9_y_data[c9_i517];
    }

    c9_emxFree_real_T(chartInstance, &c9_m_y);
    c9_d21 = (real_T)c9_idxDir->size[1];
    c9_b34 = (1.0 > c9_d21);
    if (c9_b34) {
      c9_i518 = 0;
    } else {
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_idxA->size[0]);
      c9_i518 = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_d21, 1,
        c9_idxA->size[0]);
    }

    c9_i519 = c9_r13->size[0];
    c9_r13->size[0] = c9_i518;
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r13, c9_i519,
      &c9_xe_emlrtRTEI);
    c9_r_loop_ub = c9_i518 - 1;
    for (c9_i520 = 0; c9_i520 <= c9_r_loop_ub; c9_i520++) {
      c9_r13->data[c9_i520] = c9_i520;
    }

    c9_iv6[0] = c9_r13->size[0];
    _SFD_SUB_ASSIGN_SIZE_CHECK_ND(c9_iv6, 1, *(int32_T (*)[2])c9_idxDir->size, 2);
    c9_i521 = c9_b_idxDir->size[0] * c9_b_idxDir->size[1];
    c9_b_idxDir->size[0] = 1;
    c9_b_idxDir->size[1] = c9_idxDir->size[1];
    c9_emxEnsureCapacity_int32_T1(chartInstance, c9_b_idxDir, c9_i521,
      &c9_ye_emlrtRTEI);
    c9_i522 = c9_b_idxDir->size[0];
    c9_i523 = c9_b_idxDir->size[1];
    c9_s_loop_ub = c9_idxDir->size[0] * c9_idxDir->size[1] - 1;
    for (c9_i524 = 0; c9_i524 <= c9_s_loop_ub; c9_i524++) {
      c9_b_idxDir->data[c9_i524] = (int32_T)c9_idxDir->data[c9_i524];
    }

    c9_emxFree_uint32_T(chartInstance, &c9_idxDir);
    c9_iv7[0] = c9_r13->size[0];
    c9_t_loop_ub = c9_iv7[0] - 1;
    for (c9_i525 = 0; c9_i525 <= c9_t_loop_ub; c9_i525++) {
      c9_idxA->data[c9_r13->data[c9_i525] + c9_idxA->size[0]] =
        c9_b_idxDir->data[c9_i525];
    }

    c9_emxFree_int32_T(chartInstance, &c9_b_idxDir);
    c9_emxFree_int32_T(chartInstance, &c9_r13);
    for (c9_i526 = 0; c9_i526 < 2; c9_i526++) {
      c9_varargin_1[c9_i526] = (real_T)c9_a_tmp->size[c9_i526];
    }

    for (c9_i527 = 0; c9_i527 < 2; c9_i527++) {
      c9_b_b[c9_i527] = 2.0 * c9_pad[c9_i527];
    }

    for (c9_i528 = 0; c9_i528 < 2; c9_i528++) {
      c9_varargin_1[c9_i528] = sf_integer_check(chartInstance->S, 1U, 0U, 0U,
        c9_varargin_1[c9_i528] + c9_b_b[c9_i528]);
    }

    c9_i529 = c9_a->size[0] * c9_a->size[1];
    c9_a->size[0] = (int32_T)c9_varargin_1[0];
    c9_a->size[1] = (int32_T)c9_varargin_1[1];
    c9_emxEnsureCapacity_real32_T(chartInstance, c9_a, c9_i529, &c9_af_emlrtRTEI);
    c9_d22 = (real_T)c9_a->size[1];
    c9_i530 = (int32_T)c9_d22 - 1;
    for (c9_j = 0; c9_j <= c9_i530; c9_j++) {
      c9_b_j = 1.0 + (real_T)c9_j;
      c9_d23 = (real_T)c9_a->size[0];
      c9_i531 = (int32_T)c9_d23 - 1;
      for (c9_i = 0; c9_i <= c9_i531; c9_i++) {
        c9_b_i = 1.0 + (real_T)c9_i;
        c9_a->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_b_i, 1,
          c9_a->size[0]) + c9_a->size[0] * (sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_b_j, 1, c9_a->size[1]) - 1)) - 1] = c9_a_tmp->data
          [(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
             chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_idxA->
             data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
              chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_b_i, 1,
              c9_idxA->size[0]) - 1], 1, c9_a_tmp->size[0]) + c9_a_tmp->size[0] *
            (sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
              chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_idxA->data
              [(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
                chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_b_j, 1,
                c9_idxA->size[0]) + c9_idxA->size[0]) - 1], 1, c9_a_tmp->size[1])
             - 1)) - 1];
      }
    }

    c9_emxFree_int32_T(chartInstance, &c9_idxA);
  }
}

static boolean_T c9_all(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T
  c9_a[2])
{
  boolean_T c9_p;
  int32_T c9_k;
  real_T c9_b_k;
  real_T c9_x;
  real_T c9_b_x;
  real_T c9_c_x;
  boolean_T c9_b;
  boolean_T c9_b35;
  real_T c9_d_x;
  boolean_T c9_b_b;
  boolean_T c9_b36;
  boolean_T c9_c_b;
  real_T c9_e_x;
  boolean_T c9_b_p;
  real_T c9_f_x;
  boolean_T c9_c_p;
  boolean_T exitg1;
  (void)chartInstance;
  c9_p = true;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k < 2)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_a[(int32_T)c9_b_k - 1];
    c9_b_x = c9_x;
    c9_c_x = c9_b_x;
    c9_b = muDoubleScalarIsInf(c9_c_x);
    c9_b35 = !c9_b;
    c9_d_x = c9_b_x;
    c9_b_b = muDoubleScalarIsNaN(c9_d_x);
    c9_b36 = !c9_b_b;
    c9_c_b = (c9_b35 && c9_b36);
    if (c9_c_b) {
      c9_e_x = c9_x;
      c9_f_x = c9_e_x;
      c9_f_x = muDoubleScalarFloor(c9_f_x);
      if (c9_f_x == c9_x) {
        c9_b_p = true;
      } else {
        c9_b_p = false;
      }
    } else {
      c9_b_p = false;
    }

    c9_c_p = c9_b_p;
    if (c9_c_p) {
      c9_k++;
    } else {
      c9_p = false;
      exitg1 = true;
    }
  }

  return c9_p;
}

static void c9_hypot(SFc9_LIDAR_simInstanceStruct *chartInstance,
                     c9_emxArray_real32_T *c9_x, c9_emxArray_real32_T *c9_y,
                     c9_emxArray_real32_T *c9_r)
{
  c9_emxArray_real32_T *c9_r14;
  int32_T c9_sak;
  int32_T c9_sbk;
  int32_T c9_a;
  int32_T c9_b;
  int32_T c9_c;
  int32_T c9_csz[2];
  int32_T c9_b_a;
  int32_T c9_b_b;
  int32_T c9_b_c;
  c9_emxArray_real32_T *c9_ztemp;
  int32_T c9_i532;
  int32_T c9_i533;
  int32_T c9_i534;
  int32_T c9_i535;
  int32_T c9_i536;
  c9_emxArray_real32_T *c9_r15;
  int32_T c9_i537;
  int32_T c9_i538;
  int32_T c9_i539;
  int32_T c9_i540;
  int32_T c9_loop_ub;
  int32_T c9_i541;
  c9_emxArray_real32_T *c9_b_x;
  int32_T c9_i542;
  int32_T c9_i543;
  int32_T c9_i544;
  int32_T c9_b_loop_ub;
  int32_T c9_i545;
  c9_emxArray_real32_T *c9_b_y;
  int32_T c9_i546;
  int32_T c9_i547;
  int32_T c9_i548;
  int32_T c9_c_loop_ub;
  int32_T c9_i549;
  const mxArray *c9_c_y = NULL;
  static char_T c9_cv29[15] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'd', 'i', 'm',
    'a', 'g', 'r', 'e', 'e' };

  const mxArray *c9_d_y = NULL;
  int32_T c9_i550;
  int32_T c9_i551;
  int32_T c9_nx;
  int32_T c9_c_b;
  int32_T c9_d_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  real32_T c9_c_x;
  real32_T c9_e_y;
  real32_T c9_x1;
  real32_T c9_x2;
  real32_T c9_z;
  c9_emxInit_real32_T(chartInstance, &c9_r14, 2, &c9_gf_emlrtRTEI);
  c9_sak = c9_x->size[0];
  c9_sbk = c9_y->size[0];
  c9_a = c9_sak;
  c9_b = c9_sbk;
  if (c9_a <= c9_b) {
    c9_c = c9_a;
  } else {
    c9_c = c9_b;
  }

  c9_csz[0] = c9_c;
  c9_sak = c9_x->size[1];
  c9_sbk = c9_y->size[1];
  c9_b_a = c9_sak;
  c9_b_b = c9_sbk;
  if (c9_b_a <= c9_b_b) {
    c9_b_c = c9_b_a;
  } else {
    c9_b_c = c9_b_b;
  }

  c9_emxInit_real32_T(chartInstance, &c9_ztemp, 2, &c9_kf_emlrtRTEI);
  c9_csz[1] = c9_b_c;
  c9_i532 = c9_ztemp->size[0] * c9_ztemp->size[1];
  c9_ztemp->size[0] = c9_csz[0];
  c9_ztemp->size[1] = c9_csz[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_ztemp, c9_i532,
    &c9_ef_emlrtRTEI);
  c9_i533 = c9_r14->size[0] * c9_r14->size[1];
  c9_r14->size[0] = c9_ztemp->size[0];
  c9_r14->size[1] = c9_ztemp->size[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_r14, c9_i533, &c9_ef_emlrtRTEI);
  c9_emxFree_real32_T(chartInstance, &c9_ztemp);
  for (c9_i534 = 0; c9_i534 < 2; c9_i534++) {
    c9_csz[c9_i534] = c9_r14->size[c9_i534];
  }

  c9_i535 = c9_r14->size[0] * c9_r14->size[1];
  c9_r14->size[0] = c9_csz[0];
  c9_r14->size[1] = c9_csz[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_r14, c9_i535, &c9_ff_emlrtRTEI);
  for (c9_i536 = 0; c9_i536 < 2; c9_i536++) {
    c9_csz[c9_i536] = c9_r14->size[c9_i536];
  }

  c9_emxInit_real32_T(chartInstance, &c9_r15, 2, &c9_hf_emlrtRTEI);
  c9_i537 = c9_r14->size[0] * c9_r14->size[1];
  c9_r14->size[0] = c9_csz[0];
  c9_r14->size[1] = c9_csz[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_r14, c9_i537, &c9_gf_emlrtRTEI);
  c9_i538 = c9_r15->size[0] * c9_r15->size[1];
  c9_r15->size[0] = c9_r14->size[0];
  c9_r15->size[1] = c9_r14->size[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_r15, c9_i538, &c9_hf_emlrtRTEI);
  c9_i539 = c9_r15->size[0];
  c9_i540 = c9_r15->size[1];
  c9_loop_ub = c9_r14->size[0] * c9_r14->size[1] - 1;
  for (c9_i541 = 0; c9_i541 <= c9_loop_ub; c9_i541++) {
    c9_r15->data[c9_i541] = c9_r14->data[c9_i541];
  }

  c9_emxInit_real32_T(chartInstance, &c9_b_x, 2, &c9_if_emlrtRTEI);
  c9_i542 = c9_b_x->size[0] * c9_b_x->size[1];
  c9_b_x->size[0] = c9_x->size[0];
  c9_b_x->size[1] = c9_x->size[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_b_x, c9_i542, &c9_if_emlrtRTEI);
  c9_i543 = c9_b_x->size[0];
  c9_i544 = c9_b_x->size[1];
  c9_b_loop_ub = c9_x->size[0] * c9_x->size[1] - 1;
  for (c9_i545 = 0; c9_i545 <= c9_b_loop_ub; c9_i545++) {
    c9_b_x->data[c9_i545] = c9_x->data[c9_i545];
  }

  c9_emxInit_real32_T(chartInstance, &c9_b_y, 2, &c9_if_emlrtRTEI);
  c9_i546 = c9_b_y->size[0] * c9_b_y->size[1];
  c9_b_y->size[0] = c9_y->size[0];
  c9_b_y->size[1] = c9_y->size[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_b_y, c9_i546, &c9_if_emlrtRTEI);
  c9_i547 = c9_b_y->size[0];
  c9_i548 = c9_b_y->size[1];
  c9_c_loop_ub = c9_y->size[0] * c9_y->size[1] - 1;
  for (c9_i549 = 0; c9_i549 <= c9_c_loop_ub; c9_i549++) {
    c9_b_y->data[c9_i549] = c9_y->data[c9_i549];
  }

  if (c9_dimagree(chartInstance, c9_r15, c9_b_x, c9_b_y)) {
  } else {
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv29, 10, 0U, 1U, 0U, 2, 1, 15),
                  false);
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv29, 10, 0U, 1U, 0U, 2, 1, 15),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_d_y)));
  }

  c9_emxFree_real32_T(chartInstance, &c9_b_y);
  c9_emxFree_real32_T(chartInstance, &c9_b_x);
  c9_emxFree_real32_T(chartInstance, &c9_r15);
  for (c9_i550 = 0; c9_i550 < 2; c9_i550++) {
    c9_csz[c9_i550] = c9_r14->size[c9_i550];
  }

  c9_emxFree_real32_T(chartInstance, &c9_r14);
  c9_i551 = c9_r->size[0] * c9_r->size[1];
  c9_r->size[0] = c9_csz[0];
  c9_r->size[1] = c9_csz[1];
  c9_emxEnsureCapacity_real32_T(chartInstance, c9_r, c9_i551, &c9_jf_emlrtRTEI);
  c9_nx = c9_r->size[0] * c9_r->size[1];
  c9_c_b = c9_nx;
  c9_d_b = c9_c_b;
  if (1 > c9_d_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_d_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_nx; c9_k++) {
    c9_b_k = c9_k;
    c9_c_x = c9_x->data[c9_b_k];
    c9_e_y = c9_y->data[c9_b_k];
    c9_x1 = c9_c_x;
    c9_x2 = c9_e_y;
    c9_z = muSingleScalarHypot(c9_x1, c9_x2);
    c9_r->data[c9_b_k] = c9_z;
  }
}

static boolean_T c9_dimagree(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T *c9_z, c9_emxArray_real32_T *c9_varargin_1,
  c9_emxArray_real32_T *c9_varargin_2)
{
  boolean_T c9_p;
  boolean_T c9_b_p;
  int32_T c9_k;
  int32_T c9_b_k;
  real_T c9_d24;
  boolean_T c9_c_p;
  real_T c9_d25;
  int32_T c9_c_k;
  int32_T c9_d_k;
  real_T c9_d26;
  real_T c9_d27;
  boolean_T exitg1;
  (void)chartInstance;
  c9_p = true;
  c9_b_p = true;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k < 2)) {
    c9_b_k = c9_k;
    c9_d24 = (real_T)c9_z->size[c9_b_k];
    c9_d25 = (real_T)c9_varargin_1->size[c9_b_k];
    if ((int32_T)c9_d24 != (int32_T)c9_d25) {
      c9_b_p = false;
      exitg1 = true;
    } else {
      c9_k++;
    }
  }

  if (c9_b_p) {
    c9_c_p = true;
    c9_c_k = 0;
    exitg1 = false;
    while ((!exitg1) && (c9_c_k < 2)) {
      c9_d_k = c9_c_k;
      c9_d26 = (real_T)c9_z->size[c9_d_k];
      c9_d27 = (real_T)c9_varargin_2->size[c9_d_k];
      if ((int32_T)c9_d26 != (int32_T)c9_d27) {
        c9_c_p = false;
        exitg1 = true;
      } else {
        c9_c_k++;
      }
    }

    if (c9_c_p) {
    } else {
      c9_p = false;
    }
  } else {
    c9_p = false;
  }

  return c9_p;
}

static void c9_warning(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c9_y = NULL;
  static char_T c9_cv30[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c9_b_y = NULL;
  static char_T c9_cv31[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c9_c_y = NULL;
  static char_T c9_msgID[25] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm',
    'h', 'i', 's', 't', 'c', ':', 'o', 'u', 't', 'O', 'f', 'R', 'a', 'n', 'g',
    'e' };

  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv30, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv31, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_msgID, 10, 0U, 1U, 0U, 2, 1, 25),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c9_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c9_b_y, 14, c9_c_y));
}

static void c9_b_warning(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c9_y = NULL;
  static char_T c9_cv32[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c9_b_y = NULL;
  static char_T c9_cv33[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c9_c_y = NULL;
  static char_T c9_msgID[27] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm',
    'h', 'i', 's', 't', 'c', ':', 'i', 'n', 'p', 'u', 't', 'H', 'a', 's', 'N',
    'a', 'N', 's' };

  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv32, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv33, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_msgID, 10, 0U, 1U, 0U, 2, 1, 27),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c9_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c9_b_y, 14, c9_c_y));
}

static void c9_bwselect(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_2,
  c9_emxArray_real_T *c9_varargin_3, c9_emxArray_boolean_T *c9_varargout_1)
{
  c9_emxArray_real_T *c9_r;
  int32_T c9_i552;
  int32_T c9_loop_ub;
  int32_T c9_i553;
  c9_emxArray_real_T *c9_c;
  int32_T c9_i554;
  int32_T c9_b_loop_ub;
  int32_T c9_i555;
  real_T c9_b_varargin_1;
  real_T c9_c_varargin_1;
  boolean_T c9_p;
  real_T c9_x1;
  boolean_T c9_b_p;
  real_T c9_b_x1;
  boolean_T c9_c_p;
  c9_emxArray_boolean_T *c9_BW;
  c9_emxArray_real_T *c9_tmp;
  boolean_T c9_column_BW;
  int32_T c9_i556;
  int32_T c9_i557;
  int32_T c9_i558;
  int32_T c9_c_loop_ub;
  int32_T c9_i559;
  int32_T c9_i560;
  int32_T c9_d_loop_ub;
  int32_T c9_i561;
  int32_T c9_i562;
  int32_T c9_e_loop_ub;
  int32_T c9_i563;
  c9_emxArray_boolean_T *c9_x;
  int32_T c9_f_loop_ub;
  int32_T c9_i564;
  int32_T c9_i565;
  int32_T c9_g_loop_ub;
  int32_T c9_i566;
  int32_T c9_i567;
  int32_T c9_h_loop_ub;
  c9_emxArray_boolean_T *c9_r16;
  int32_T c9_i568;
  real_T c9_b_BW;
  int32_T c9_i569;
  int32_T c9_i570;
  int32_T c9_i_loop_ub;
  int32_T c9_i571;
  int32_T c9_j_loop_ub;
  int32_T c9_i572;
  int32_T c9_i573;
  int32_T c9_k_loop_ub;
  int32_T c9_i574;
  int32_T c9_i575;
  int32_T c9_l_loop_ub;
  int32_T c9_i576;
  int32_T c9_i577;
  int32_T c9_m_loop_ub;
  int32_T c9_i578;
  real_T c9_c_BW;
  int32_T c9_i579;
  int32_T c9_n_loop_ub;
  int32_T c9_i580;
  int32_T c9_i581;
  int32_T c9_o_loop_ub;
  int32_T c9_i582;
  c9_emxArray_int32_T *c9_ii;
  int32_T c9_nx;
  int32_T c9_k;
  int32_T c9_b_nx;
  int32_T c9_idx;
  int32_T c9_i583;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_b_ii;
  int32_T c9_c_ii;
  const mxArray *c9_y = NULL;
  c9_emxArray_int32_T *c9_c_b;
  const mxArray *c9_b_y = NULL;
  boolean_T c9_b37;
  int32_T c9_i584;
  int32_T c9_i585;
  int32_T c9_i586;
  int32_T c9_i587;
  int32_T c9_p_loop_ub;
  int32_T c9_q_loop_ub;
  int32_T c9_i588;
  int32_T c9_i589;
  int32_T c9_matrixSize;
  c9_emxArray_int32_T *c9_b_tmp;
  int32_T c9_indexSize[2];
  int32_T c9_i590;
  int32_T c9_i591;
  int32_T c9_size1;
  int32_T c9_i592;
  real_T c9_d_BW[2];
  boolean_T c9_d_b;
  int32_T c9_r_loop_ub;
  int32_T c9_i593;
  int32_T c9_i594;
  boolean_T c9_nonSingletonDimFound;
  boolean_T c9_b_c;
  c9_emxArray_int32_T *c9_c_tmp;
  c9_emxArray_real_T *c9_b_r;
  boolean_T c9_c_c;
  boolean_T c9_e_b;
  int32_T c9_hi;
  int32_T c9_i595;
  int32_T c9_i596;
  const mxArray *c9_c_y = NULL;
  int32_T c9_i597;
  int32_T c9_s_loop_ub;
  const mxArray *c9_d_y = NULL;
  int32_T c9_t_loop_ub;
  int32_T c9_i598;
  int32_T c9_i599;
  const mxArray *c9_e_y = NULL;
  static char_T c9_cv34[30] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'u', 'b',
    '2', 'i', 'n', 'd', ':', 'I', 'n', 'd', 'e', 'x', 'O', 'u', 't', 'O', 'f',
    'R', 'a', 'n', 'g', 'e' };

  const mxArray *c9_f_y = NULL;
  real_T c9_b_varargin_2[2];
  int32_T c9_i600;
  int32_T c9_i601;
  int32_T c9_i602;
  int32_T c9_i603;
  boolean_T c9_d_p;
  int32_T c9_i604;
  int32_T c9_i605;
  boolean_T c9_e_p;
  int32_T c9_b_k;
  real_T c9_c_k;
  real_T c9_c_x1;
  real_T c9_x2;
  boolean_T c9_f_p;
  const mxArray *c9_g_y = NULL;
  static char_T c9_cv35[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'u', 'b',
    '2', 'i', 'n', 'd', ':', 'S', 'u', 'b', 's', 'c', 'r', 'i', 'p', 't', 'V',
    'e', 'c', 't', 'o', 'r', 'S', 'i', 'z', 'e' };

  int32_T c9_i606;
  const mxArray *c9_h_y = NULL;
  c9_emxArray_real_T *c9_d_c;
  int32_T c9_i607;
  int32_T c9_u_loop_ub;
  int32_T c9_i608;
  const mxArray *c9_i_y = NULL;
  const mxArray *c9_j_y = NULL;
  int32_T c9_psiz;
  int32_T c9_i609;
  int32_T c9_v_loop_ub;
  int32_T c9_i610;
  int32_T c9_a;
  int32_T c9_i611;
  int32_T c9_w_loop_ub;
  int32_T c9_i612;
  int32_T c9_i613;
  int32_T c9_x_loop_ub;
  int32_T c9_i614;
  int32_T c9_i615;
  int32_T c9_y_loop_ub;
  int32_T c9_i616;
  int32_T c9_i617;
  int32_T c9_ab_loop_ub;
  int32_T c9_i618;
  c9_emxArray_boolean_T *c9_BW2_temp;
  int32_T c9_i619;
  int32_T c9_i620;
  int32_T c9_i621;
  int32_T c9_bb_loop_ub;
  int32_T c9_i622;
  int32_T c9_i623;
  int32_T c9_i624;
  int32_T c9_b_BW2_temp[2];
  int32_T c9_e_BW[2];
  int32_T c9_i625;
  int32_T c9_i626;
  int32_T c9_i627;
  int32_T c9_i628;
  int32_T c9_i629;
  int32_T c9_i630;
  int32_T c9_i631;
  int32_T c9_cb_loop_ub;
  int32_T c9_i632;
  boolean_T c9_b38;
  boolean_T c9_b39;
  int32_T c9_c_nx;
  boolean_T c9_b40;
  boolean_T c9_b41;
  int32_T c9_i633;
  int32_T c9_i634;
  boolean_T c9_b42;
  int32_T c9_i635;
  int32_T c9_db_loop_ub;
  const mxArray *c9_k_y = NULL;
  int32_T c9_i636;
  int32_T c9_i637;
  int32_T c9_d_k;
  int32_T c9_eb_loop_ub;
  const mxArray *c9_l_y = NULL;
  int32_T c9_d_nx;
  int32_T c9_i638;
  int32_T c9_b_idx;
  int32_T c9_fb_loop_ub;
  int32_T c9_i639;
  int32_T c9_i640;
  int32_T c9_f_b;
  int32_T c9_g_b;
  boolean_T c9_b_overflow;
  int32_T c9_d_ii;
  int32_T c9_e_ii;
  const mxArray *c9_m_y = NULL;
  const mxArray *c9_n_y = NULL;
  boolean_T c9_b43;
  int32_T c9_i641;
  int32_T c9_i642;
  int32_T c9_gb_loop_ub;
  int32_T c9_i643;
  int32_T c9_b_matrixSize;
  int32_T c9_b_size1;
  boolean_T c9_h_b;
  boolean_T c9_b_nonSingletonDimFound;
  boolean_T c9_e_c;
  boolean_T c9_f_c;
  boolean_T c9_i_b;
  const mxArray *c9_o_y = NULL;
  const mxArray *c9_p_y = NULL;
  boolean_T exitg1;
  c9_emxInit_real_T1(chartInstance, &c9_r, 1, &c9_og_emlrtRTEI);
  c9_i552 = c9_r->size[0];
  c9_r->size[0] = c9_varargin_3->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_r, c9_i552, &c9_lf_emlrtRTEI);
  c9_loop_ub = c9_varargin_3->size[0] - 1;
  for (c9_i553 = 0; c9_i553 <= c9_loop_ub; c9_i553++) {
    c9_r->data[c9_i553] = c9_varargin_3->data[c9_i553];
  }

  c9_emxInit_real_T1(chartInstance, &c9_c, 1, &c9_pg_emlrtRTEI);
  c9_d_round(chartInstance, c9_r);
  c9_i554 = c9_c->size[0];
  c9_c->size[0] = c9_varargin_2->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_c, c9_i554, &c9_mf_emlrtRTEI);
  c9_b_loop_ub = c9_varargin_2->size[0] - 1;
  for (c9_i555 = 0; c9_i555 <= c9_b_loop_ub; c9_i555++) {
    c9_c->data[c9_i555] = c9_varargin_2->data[c9_i555];
  }

  c9_d_round(chartInstance, c9_c);
  c9_b_varargin_1 = (real_T)c9_varargin_1->size[0];
  c9_c_varargin_1 = c9_b_varargin_1;
  c9_p = false;
  c9_x1 = c9_c_varargin_1;
  c9_b_p = true;
  c9_b_x1 = c9_x1;
  c9_c_p = (c9_b_x1 == 1.0);
  if (!c9_c_p) {
    c9_b_p = false;
  }

  if (!c9_b_p) {
  } else {
    c9_p = true;
  }

  c9_emxInit_boolean_T(chartInstance, &c9_BW, 2, &c9_ng_emlrtRTEI);
  c9_emxInit_real_T1(chartInstance, &c9_tmp, 1, &c9_pf_emlrtRTEI);
  if (c9_p && ((real_T)c9_varargin_1->size[1] > 1.0)) {
    c9_column_BW = true;
    c9_i557 = c9_BW->size[0] * c9_BW->size[1];
    c9_BW->size[0] = c9_varargin_1->size[1];
    c9_BW->size[1] = c9_varargin_1->size[0];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW, c9_i557,
      &c9_of_emlrtRTEI);
    c9_c_loop_ub = c9_varargin_1->size[0] - 1;
    for (c9_i560 = 0; c9_i560 <= c9_c_loop_ub; c9_i560++) {
      c9_e_loop_ub = c9_varargin_1->size[1] - 1;
      for (c9_i563 = 0; c9_i563 <= c9_e_loop_ub; c9_i563++) {
        c9_BW->data[c9_i563 + c9_BW->size[0] * c9_i560] = c9_varargin_1->
          data[c9_i560 + c9_varargin_1->size[0] * c9_i563];
      }
    }

    c9_i562 = c9_tmp->size[0];
    c9_tmp->size[0] = c9_r->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_tmp, c9_i562,
      &c9_pf_emlrtRTEI);
    c9_f_loop_ub = c9_r->size[0] - 1;
    for (c9_i565 = 0; c9_i565 <= c9_f_loop_ub; c9_i565++) {
      c9_tmp->data[c9_i565] = c9_r->data[c9_i565];
    }

    c9_i566 = c9_r->size[0];
    c9_r->size[0] = c9_c->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_r, c9_i566, &c9_rf_emlrtRTEI);
    c9_h_loop_ub = c9_c->size[0] - 1;
    for (c9_i568 = 0; c9_i568 <= c9_h_loop_ub; c9_i568++) {
      c9_r->data[c9_i568] = c9_c->data[c9_i568];
    }

    c9_i570 = c9_c->size[0];
    c9_c->size[0] = c9_tmp->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_c, c9_i570, &c9_tf_emlrtRTEI);
    c9_j_loop_ub = c9_tmp->size[0] - 1;
    for (c9_i572 = 0; c9_i572 <= c9_j_loop_ub; c9_i572++) {
      c9_c->data[c9_i572] = c9_tmp->data[c9_i572];
    }
  } else {
    c9_column_BW = false;
    c9_i556 = c9_BW->size[0] * c9_BW->size[1];
    c9_BW->size[0] = c9_varargin_1->size[0];
    c9_BW->size[1] = c9_varargin_1->size[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW, c9_i556,
      &c9_nf_emlrtRTEI);
    c9_i558 = c9_BW->size[0];
    c9_i559 = c9_BW->size[1];
    c9_d_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
    for (c9_i561 = 0; c9_i561 <= c9_d_loop_ub; c9_i561++) {
      c9_BW->data[c9_i561] = c9_varargin_1->data[c9_i561];
    }
  }

  c9_emxInit_boolean_T1(chartInstance, &c9_x, 1, &c9_uf_emlrtRTEI);
  c9_i564 = c9_x->size[0];
  c9_x->size[0] = c9_r->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_x, c9_i564, &c9_qf_emlrtRTEI);
  c9_g_loop_ub = c9_r->size[0] - 1;
  for (c9_i567 = 0; c9_i567 <= c9_g_loop_ub; c9_i567++) {
    c9_x->data[c9_i567] = (c9_r->data[c9_i567] < 1.0);
  }

  c9_emxInit_boolean_T1(chartInstance, &c9_r16, 1, &c9_qg_emlrtRTEI);
  c9_b_BW = (real_T)c9_BW->size[0];
  c9_i569 = c9_r16->size[0];
  c9_r16->size[0] = c9_r->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_r16, c9_i569,
    &c9_sf_emlrtRTEI);
  c9_i_loop_ub = c9_r->size[0] - 1;
  for (c9_i571 = 0; c9_i571 <= c9_i_loop_ub; c9_i571++) {
    c9_r16->data[c9_i571] = (c9_r->data[c9_i571] > c9_b_BW);
  }

  _SFD_SIZE_EQ_CHECK_1D(c9_x->size[0], c9_r16->size[0]);
  c9_i573 = c9_x->size[0];
  c9_x->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_x, c9_i573, &c9_uf_emlrtRTEI);
  c9_k_loop_ub = c9_x->size[0] - 1;
  for (c9_i574 = 0; c9_i574 <= c9_k_loop_ub; c9_i574++) {
    c9_x->data[c9_i574] = (c9_x->data[c9_i574] || c9_r16->data[c9_i574]);
  }

  c9_i575 = c9_r16->size[0];
  c9_r16->size[0] = c9_c->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_r16, c9_i575,
    &c9_vf_emlrtRTEI);
  c9_l_loop_ub = c9_c->size[0] - 1;
  for (c9_i576 = 0; c9_i576 <= c9_l_loop_ub; c9_i576++) {
    c9_r16->data[c9_i576] = (c9_c->data[c9_i576] < 1.0);
  }

  _SFD_SIZE_EQ_CHECK_1D(c9_x->size[0], c9_r16->size[0]);
  c9_i577 = c9_x->size[0];
  c9_x->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_x, c9_i577, &c9_uf_emlrtRTEI);
  c9_m_loop_ub = c9_x->size[0] - 1;
  for (c9_i578 = 0; c9_i578 <= c9_m_loop_ub; c9_i578++) {
    c9_x->data[c9_i578] = (c9_x->data[c9_i578] || c9_r16->data[c9_i578]);
  }

  c9_c_BW = (real_T)c9_BW->size[1];
  c9_i579 = c9_r16->size[0];
  c9_r16->size[0] = c9_c->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_r16, c9_i579,
    &c9_wf_emlrtRTEI);
  c9_n_loop_ub = c9_c->size[0] - 1;
  for (c9_i580 = 0; c9_i580 <= c9_n_loop_ub; c9_i580++) {
    c9_r16->data[c9_i580] = (c9_c->data[c9_i580] > c9_c_BW);
  }

  _SFD_SIZE_EQ_CHECK_1D(c9_x->size[0], c9_r16->size[0]);
  c9_i581 = c9_x->size[0];
  c9_x->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_x, c9_i581, &c9_uf_emlrtRTEI);
  c9_o_loop_ub = c9_x->size[0] - 1;
  for (c9_i582 = 0; c9_i582 <= c9_o_loop_ub; c9_i582++) {
    c9_x->data[c9_i582] = (c9_x->data[c9_i582] || c9_r16->data[c9_i582]);
  }

  c9_emxFree_boolean_T(chartInstance, &c9_r16);
  c9_emxInit_int32_T(chartInstance, &c9_ii, 1, &c9_rg_emlrtRTEI);
  c9_nx = c9_x->size[0];
  c9_k = c9_nx;
  c9_b_nx = c9_nx;
  c9_idx = 0;
  c9_i583 = c9_ii->size[0];
  c9_ii->size[0] = c9_k;
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_ii, c9_i583, &c9_ud_emlrtRTEI);
  c9_b = c9_b_nx;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  c9_b_ii = 1;
  exitg1 = false;
  while ((!exitg1) && (c9_b_ii - 1 <= c9_b_nx - 1)) {
    c9_c_ii = c9_b_ii;
    if (c9_x->data[c9_c_ii - 1]) {
      c9_idx++;
      c9_ii->data[c9_idx - 1] = c9_c_ii;
      if (c9_idx >= c9_k) {
        exitg1 = true;
      } else {
        c9_b_ii++;
      }
    } else {
      c9_b_ii++;
    }
  }

  c9_emxFree_boolean_T(chartInstance, &c9_x);
  if (c9_idx <= c9_k) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c9_b_y)));
  }

  c9_emxInit_int32_T(chartInstance, &c9_c_b, 1, &c9_fg_emlrtRTEI);
  if (c9_k == 1) {
    if (c9_idx == 0) {
      c9_i584 = c9_ii->size[0];
      c9_ii->size[0] = 0;
    }
  } else {
    c9_b37 = (1 > c9_idx);
    if (c9_b37) {
      c9_i585 = 0;
    } else {
      c9_i585 = c9_idx;
    }

    c9_i587 = c9_c_b->size[0];
    c9_c_b->size[0] = c9_i585;
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_b, c9_i587,
      &c9_yf_emlrtRTEI);
    c9_q_loop_ub = c9_i585 - 1;
    for (c9_i589 = 0; c9_i589 <= c9_q_loop_ub; c9_i589++) {
      c9_c_b->data[c9_i589] = 1 + c9_i589;
    }

    c9_matrixSize = c9_ii->size[0];
    c9_indexSize[0] = 1;
    c9_indexSize[1] = c9_c_b->size[0];
    c9_size1 = c9_matrixSize;
    if (c9_size1 != 1) {
      c9_d_b = false;
    } else {
      c9_d_b = true;
    }

    if (c9_d_b) {
      c9_nonSingletonDimFound = false;
      if (c9_indexSize[1] != 1) {
        c9_nonSingletonDimFound = true;
      }

      c9_e_b = c9_nonSingletonDimFound;
      if (c9_e_b) {
        c9_b_c = true;
      } else {
        c9_b_c = false;
      }
    } else {
      c9_b_c = false;
    }

    c9_c_c = c9_b_c;
    if (!c9_c_c) {
    } else {
      c9_c_y = NULL;
      sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      c9_d_y = NULL;
      sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_d_y)));
    }

    c9_i597 = c9_ii->size[0];
    c9_ii->size[0] = c9_i585;
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_ii, c9_i597, &c9_dg_emlrtRTEI);
  }

  c9_i586 = c9_tmp->size[0];
  c9_tmp->size[0] = c9_ii->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_tmp, c9_i586, &c9_xf_emlrtRTEI);
  c9_p_loop_ub = c9_ii->size[0] - 1;
  for (c9_i588 = 0; c9_i588 <= c9_p_loop_ub; c9_i588++) {
    c9_tmp->data[c9_i588] = (real_T)c9_ii->data[c9_i588];
  }

  if (c9_tmp->size[0] != 0) {
    c9_emxInit_int32_T(chartInstance, &c9_b_tmp, 1, &c9_ag_emlrtRTEI);
    c9_c_warning(chartInstance);
    c9_i591 = c9_b_tmp->size[0];
    c9_b_tmp->size[0] = c9_tmp->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_tmp, c9_i591,
      &c9_ag_emlrtRTEI);
    c9_r_loop_ub = c9_tmp->size[0] - 1;
    for (c9_i593 = 0; c9_i593 <= c9_r_loop_ub; c9_i593++) {
      c9_b_tmp->data[c9_i593] = (int32_T)c9_tmp->data[c9_i593];
    }

    c9_emxInit_int32_T(chartInstance, &c9_c_tmp, 1, &c9_bg_emlrtRTEI);
    c9_d_nullAssignment(chartInstance, c9_r, c9_b_tmp);
    c9_i595 = c9_c_tmp->size[0];
    c9_c_tmp->size[0] = c9_tmp->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_tmp, c9_i595,
      &c9_bg_emlrtRTEI);
    c9_s_loop_ub = c9_tmp->size[0] - 1;
    c9_emxFree_int32_T(chartInstance, &c9_b_tmp);
    for (c9_i599 = 0; c9_i599 <= c9_s_loop_ub; c9_i599++) {
      c9_c_tmp->data[c9_i599] = (int32_T)c9_tmp->data[c9_i599];
    }

    c9_d_nullAssignment(chartInstance, c9_c, c9_c_tmp);
    c9_emxFree_int32_T(chartInstance, &c9_c_tmp);
  }

  for (c9_i590 = 0; c9_i590 < 2; c9_i590++) {
    c9_d_BW[c9_i590] = (real_T)c9_BW->size[c9_i590];
  }

  for (c9_i592 = 0; c9_i592 < 2; c9_i592++) {
    c9_d_BW[c9_i592];
  }

  for (c9_i594 = 0; c9_i594 < 2; c9_i594++) {
    c9_indexSize[c9_i594] = (int32_T)c9_d_BW[c9_i594];
  }

  c9_emxInit_real_T1(chartInstance, &c9_b_r, 1, &c9_cg_emlrtRTEI);
  c9_hi = c9_indexSize[0];
  c9_i596 = c9_b_r->size[0];
  c9_b_r->size[0] = c9_r->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_r, c9_i596, &c9_cg_emlrtRTEI);
  c9_t_loop_ub = c9_r->size[0] - 1;
  for (c9_i598 = 0; c9_i598 <= c9_t_loop_ub; c9_i598++) {
    c9_b_r->data[c9_i598] = c9_r->data[c9_i598];
  }

  if (c9_allinrange(chartInstance, c9_b_r, 1.0, c9_hi)) {
  } else {
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv34, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv34, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_e_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_f_y)));
  }

  c9_emxFree_real_T(chartInstance, &c9_b_r);
  c9_d_BW[0] = (real_T)c9_r->size[0];
  c9_d_BW[1] = 1.0;
  c9_b_varargin_2[0] = (real_T)c9_c->size[0];
  c9_b_varargin_2[1] = 1.0;
  for (c9_i600 = 0; c9_i600 < 2; c9_i600++) {
    c9_d_BW[c9_i600];
  }

  for (c9_i601 = 0; c9_i601 < 2; c9_i601++) {
    c9_b_varargin_2[c9_i601];
  }

  for (c9_i602 = 0; c9_i602 < 2; c9_i602++) {
    c9_d_BW[c9_i602];
  }

  for (c9_i603 = 0; c9_i603 < 2; c9_i603++) {
    c9_b_varargin_2[c9_i603];
  }

  c9_d_p = false;
  for (c9_i604 = 0; c9_i604 < 2; c9_i604++) {
    c9_d_BW[c9_i604];
  }

  for (c9_i605 = 0; c9_i605 < 2; c9_i605++) {
    c9_b_varargin_2[c9_i605];
  }

  c9_e_p = true;
  c9_b_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_b_k < 2)) {
    c9_c_k = 1.0 + (real_T)c9_b_k;
    c9_c_x1 = c9_d_BW[(int32_T)c9_c_k - 1];
    c9_x2 = c9_b_varargin_2[(int32_T)c9_c_k - 1];
    c9_f_p = (c9_c_x1 == c9_x2);
    if (!c9_f_p) {
      c9_e_p = false;
      exitg1 = true;
    } else {
      c9_b_k++;
    }
  }

  if (!c9_e_p) {
  } else {
    c9_d_p = true;
  }

  if (c9_d_p) {
  } else {
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv35, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv35, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_h_y)));
  }

  for (c9_i606 = 0; c9_i606 < 2; c9_i606++) {
    c9_indexSize[c9_i606];
  }

  c9_emxInit_real_T1(chartInstance, &c9_d_c, 1, &c9_cg_emlrtRTEI);
  c9_hi = c9_indexSize[1];
  c9_i607 = c9_d_c->size[0];
  c9_d_c->size[0] = c9_c->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_d_c, c9_i607, &c9_cg_emlrtRTEI);
  c9_u_loop_ub = c9_c->size[0] - 1;
  for (c9_i608 = 0; c9_i608 <= c9_u_loop_ub; c9_i608++) {
    c9_d_c->data[c9_i608] = c9_c->data[c9_i608];
  }

  if (c9_allinrange(chartInstance, c9_d_c, 1.0, c9_hi)) {
  } else {
    c9_i_y = NULL;
    sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv34, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_j_y = NULL;
    sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv34, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_i_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_j_y)));
  }

  c9_emxFree_real_T(chartInstance, &c9_d_c);
  c9_psiz = c9_indexSize[0];
  c9_i609 = c9_ii->size[0];
  c9_ii->size[0] = c9_r->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_ii, c9_i609, &c9_eg_emlrtRTEI);
  c9_v_loop_ub = c9_r->size[0] - 1;
  for (c9_i610 = 0; c9_i610 <= c9_v_loop_ub; c9_i610++) {
    c9_ii->data[c9_i610] = (int32_T)c9_r->data[c9_i610];
  }

  c9_emxFree_real_T(chartInstance, &c9_r);
  c9_a = c9_psiz;
  c9_i611 = c9_c_b->size[0];
  c9_c_b->size[0] = c9_c->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_b, c9_i611, &c9_fg_emlrtRTEI);
  c9_w_loop_ub = c9_c->size[0] - 1;
  for (c9_i612 = 0; c9_i612 <= c9_w_loop_ub; c9_i612++) {
    c9_c_b->data[c9_i612] = (int32_T)c9_c->data[c9_i612] - 1;
  }

  c9_emxFree_real_T(chartInstance, &c9_c);
  c9_i613 = c9_c_b->size[0];
  c9_c_b->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_b, c9_i613, &c9_gg_emlrtRTEI);
  c9_x_loop_ub = c9_c_b->size[0] - 1;
  for (c9_i614 = 0; c9_i614 <= c9_x_loop_ub; c9_i614++) {
    c9_c_b->data[c9_i614] *= c9_a;
  }

  c9_i615 = c9_ii->size[0];
  c9_ii->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_ii, c9_i615, &c9_hg_emlrtRTEI);
  c9_y_loop_ub = c9_ii->size[0] - 1;
  for (c9_i616 = 0; c9_i616 <= c9_y_loop_ub; c9_i616++) {
    c9_ii->data[c9_i616] += c9_c_b->data[c9_i616];
  }

  c9_i617 = c9_tmp->size[0];
  c9_tmp->size[0] = c9_ii->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_tmp, c9_i617, &c9_ig_emlrtRTEI);
  c9_ab_loop_ub = c9_ii->size[0] - 1;
  for (c9_i618 = 0; c9_i618 <= c9_ab_loop_ub; c9_i618++) {
    c9_tmp->data[c9_i618] = (real_T)c9_ii->data[c9_i618];
  }

  c9_emxInit_boolean_T(chartInstance, &c9_BW2_temp, 2, &c9_jg_emlrtRTEI);
  c9_i619 = c9_BW2_temp->size[0] * c9_BW2_temp->size[1];
  c9_BW2_temp->size[0] = c9_BW->size[0];
  c9_BW2_temp->size[1] = c9_BW->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW2_temp, c9_i619,
    &c9_jg_emlrtRTEI);
  c9_i620 = c9_BW2_temp->size[0];
  c9_i621 = c9_BW2_temp->size[1];
  c9_bb_loop_ub = c9_BW->size[0] * c9_BW->size[1] - 1;
  for (c9_i622 = 0; c9_i622 <= c9_bb_loop_ub; c9_i622++) {
    c9_BW2_temp->data[c9_i622] = !c9_BW->data[c9_i622];
  }

  c9_b_imfill(chartInstance, c9_BW2_temp, c9_tmp);
  c9_emxFree_real_T(chartInstance, &c9_tmp);
  for (c9_i623 = 0; c9_i623 < 2; c9_i623++) {
    c9_b_BW2_temp[c9_i623] = c9_BW2_temp->size[c9_i623];
  }

  for (c9_i624 = 0; c9_i624 < 2; c9_i624++) {
    c9_e_BW[c9_i624] = c9_BW->size[c9_i624];
  }

  _SFD_SIZE_EQ_CHECK_ND(c9_b_BW2_temp, c9_e_BW, 2);
  c9_i625 = c9_BW2_temp->size[0] * c9_BW2_temp->size[1];
  c9_i626 = c9_BW->size[0] * c9_BW->size[1];
  c9_i627 = c9_BW2_temp->size[0] * c9_BW2_temp->size[1];
  c9_BW2_temp->size[0];
  c9_BW2_temp->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW2_temp, c9_i627,
    &c9_kg_emlrtRTEI);
  c9_i628 = c9_BW2_temp->size[0];
  c9_i629 = c9_BW2_temp->size[1];
  c9_i630 = c9_i625;
  c9_i631 = c9_i626;
  c9_cb_loop_ub = c9_i630 - 1;
  for (c9_i632 = 0; c9_i632 <= c9_cb_loop_ub; c9_i632++) {
    c9_BW2_temp->data[c9_i632] = (c9_BW2_temp->data[c9_i632] && c9_BW->
      data[c9_i632]);
  }

  c9_emxFree_boolean_T(chartInstance, &c9_BW);
  c9_b38 = (c9_BW2_temp->size[0] == 0);
  c9_b39 = (c9_BW2_temp->size[1] == 0);
  if (c9_b38 || c9_b39) {
  } else {
    c9_c_nx = c9_BW2_temp->size[0] * c9_BW2_temp->size[1];
    c9_b40 = (c9_BW2_temp->size[0] == 1);
    c9_b41 = (c9_BW2_temp->size[1] == 1);
    if (((!c9_b40) && (!c9_b41)) || ((real_T)c9_BW2_temp->size[0] != 1.0) ||
        ((real_T)c9_BW2_temp->size[1] <= 1.0)) {
      c9_b42 = true;
    } else {
      c9_b42 = false;
    }

    if (c9_b42) {
    } else {
      c9_k_y = NULL;
      sf_mex_assign(&c9_k_y, sf_mex_create("y", c9_cv4, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      c9_l_y = NULL;
      sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv4, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_k_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_l_y)));
    }

    c9_d_k = c9_c_nx;
    c9_d_nx = c9_c_nx;
    c9_b_idx = 0;
    c9_i639 = c9_ii->size[0];
    c9_ii->size[0] = c9_d_k;
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_ii, c9_i639, &c9_ud_emlrtRTEI);
    c9_f_b = c9_d_nx;
    c9_g_b = c9_f_b;
    if (1 > c9_g_b) {
      c9_b_overflow = false;
    } else {
      c9_b_overflow = (c9_g_b > 2147483646);
    }

    if (c9_b_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    c9_d_ii = 1;
    exitg1 = false;
    while ((!exitg1) && (c9_d_ii - 1 <= c9_d_nx - 1)) {
      c9_e_ii = c9_d_ii;
      if (c9_BW2_temp->data[c9_e_ii - 1]) {
        c9_b_idx++;
        c9_ii->data[c9_b_idx - 1] = c9_e_ii;
        if (c9_b_idx >= c9_d_k) {
          exitg1 = true;
        } else {
          c9_d_ii++;
        }
      } else {
        c9_d_ii++;
      }
    }

    if (c9_b_idx <= c9_d_k) {
    } else {
      c9_m_y = NULL;
      sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      c9_n_y = NULL;
      sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_m_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_n_y)));
    }

    if (c9_d_k == 1) {
    } else {
      c9_b43 = (1 > c9_b_idx);
      if (c9_b43) {
        c9_i641 = 0;
      } else {
        c9_i641 = c9_b_idx;
      }

      c9_i642 = c9_c_b->size[0];
      c9_c_b->size[0] = c9_i641;
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_b, c9_i642,
        &c9_yf_emlrtRTEI);
      c9_gb_loop_ub = c9_i641 - 1;
      for (c9_i643 = 0; c9_i643 <= c9_gb_loop_ub; c9_i643++) {
        c9_c_b->data[c9_i643] = 1 + c9_i643;
      }

      c9_b_matrixSize = c9_ii->size[0];
      c9_indexSize[0] = 1;
      c9_indexSize[1] = c9_c_b->size[0];
      c9_b_size1 = c9_b_matrixSize;
      if (c9_b_size1 != 1) {
        c9_h_b = false;
      } else {
        c9_h_b = true;
      }

      if (c9_h_b) {
        c9_b_nonSingletonDimFound = false;
        if (c9_indexSize[1] != 1) {
          c9_b_nonSingletonDimFound = true;
        }

        c9_i_b = c9_b_nonSingletonDimFound;
        if (c9_i_b) {
          c9_e_c = true;
        } else {
          c9_e_c = false;
        }
      } else {
        c9_e_c = false;
      }

      c9_f_c = c9_e_c;
      if (!c9_f_c) {
      } else {
        c9_o_y = NULL;
        sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2, 1,
          30), false);
        c9_p_y = NULL;
        sf_mex_assign(&c9_p_y, sf_mex_create("y", c9_cv5, 10, 0U, 1U, 0U, 2, 1,
          30), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                          c9_o_y, 14, sf_mex_call_debug
                          (sfGlobalDebugInstanceStruct, "getString", 1U, 1U, 14,
                           sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "message", 1U, 1U, 14, c9_p_y)));
      }
    }
  }

  c9_emxFree_int32_T(chartInstance, &c9_c_b);
  c9_emxFree_int32_T(chartInstance, &c9_ii);
  if (c9_column_BW) {
    c9_i634 = c9_varargout_1->size[0] * c9_varargout_1->size[1];
    c9_varargout_1->size[0] = c9_BW2_temp->size[1];
    c9_varargout_1->size[1] = c9_BW2_temp->size[0];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_varargout_1, c9_i634,
      &c9_mg_emlrtRTEI);
    c9_db_loop_ub = c9_BW2_temp->size[0] - 1;
    for (c9_i637 = 0; c9_i637 <= c9_db_loop_ub; c9_i637++) {
      c9_fb_loop_ub = c9_BW2_temp->size[1] - 1;
      for (c9_i640 = 0; c9_i640 <= c9_fb_loop_ub; c9_i640++) {
        c9_varargout_1->data[c9_i640 + c9_varargout_1->size[0] * c9_i637] =
          c9_BW2_temp->data[c9_i637 + c9_BW2_temp->size[0] * c9_i640];
      }
    }
  } else {
    c9_i633 = c9_varargout_1->size[0] * c9_varargout_1->size[1];
    c9_varargout_1->size[0] = c9_BW2_temp->size[0];
    c9_varargout_1->size[1] = c9_BW2_temp->size[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_varargout_1, c9_i633,
      &c9_lg_emlrtRTEI);
    c9_i635 = c9_varargout_1->size[0];
    c9_i636 = c9_varargout_1->size[1];
    c9_eb_loop_ub = c9_BW2_temp->size[0] * c9_BW2_temp->size[1] - 1;
    for (c9_i638 = 0; c9_i638 <= c9_eb_loop_ub; c9_i638++) {
      c9_varargout_1->data[c9_i638] = c9_BW2_temp->data[c9_i638];
    }
  }

  c9_emxFree_boolean_T(chartInstance, &c9_BW2_temp);
}

static void c9_c_warning(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c9_y = NULL;
  static char_T c9_cv36[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c9_b_y = NULL;
  static char_T c9_cv37[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c9_c_y = NULL;
  static char_T c9_msgID[26] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'b', 'w',
    's', 'e', 'l', 'e', 'c', 't', ':', 'o', 'u', 't', 'O', 'f', 'R', 'a', 'n',
    'g', 'e' };

  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv36, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv37, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_msgID, 10, 0U, 1U, 0U, 2, 1, 26),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c9_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c9_b_y, 14, c9_c_y));
}

static void c9_b_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T
  *c9_b_x)
{
  int32_T c9_i644;
  int32_T c9_loop_ub;
  int32_T c9_i645;
  c9_emxArray_int32_T *c9_b_idx;
  int32_T c9_i646;
  int32_T c9_b_loop_ub;
  int32_T c9_i647;
  c9_i644 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i644, &c9_nc_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i645 = 0; c9_i645 <= c9_loop_ub; c9_i645++) {
    c9_b_x->data[c9_i645] = c9_x->data[c9_i645];
  }

  c9_emxInit_int32_T(chartInstance, &c9_b_idx, 1, &c9_nc_emlrtRTEI);
  c9_i646 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i646,
    &c9_nc_emlrtRTEI);
  c9_b_loop_ub = c9_idx->size[0] - 1;
  for (c9_i647 = 0; c9_i647 <= c9_b_loop_ub; c9_i647++) {
    c9_b_idx->data[c9_i647] = c9_idx->data[c9_i647];
  }

  c9_d_nullAssignment(chartInstance, c9_b_x, c9_b_idx);
  c9_emxFree_int32_T(chartInstance, &c9_b_idx);
}

static boolean_T c9_allinrange(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, real_T c9_lo, int32_T c9_hi)
{
  boolean_T c9_p;
  real_T c9_d28;
  int32_T c9_i648;
  int32_T c9_k;
  real_T c9_b_k;
  boolean_T c9_b44;
  int32_T exitg1;
  (void)chartInstance;
  (void)c9_lo;
  c9_d28 = (real_T)c9_x->size[0];
  c9_i648 = (int32_T)c9_d28 - 1;
  c9_k = 0;
  do {
    exitg1 = 0;
    if (c9_k <= c9_i648) {
      c9_b_k = 1.0 + (real_T)c9_k;
      if ((c9_x->data[(int32_T)c9_b_k - 1] >= 1.0) && (c9_x->data[(int32_T)
           c9_b_k - 1] <= (real_T)c9_hi)) {
        c9_b44 = true;
      } else {
        c9_b44 = false;
      }

      if (!c9_b44) {
        c9_p = false;
        exitg1 = 1;
      } else {
        c9_k++;
      }
    } else {
      c9_p = true;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return c9_p;
}

static void c9_imfill(SFc9_LIDAR_simInstanceStruct *chartInstance,
                      c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T
                      *c9_varargin_2, c9_emxArray_boolean_T *c9_I2)
{
  int32_T c9_i649;
  int32_T c9_i650;
  int32_T c9_i651;
  int32_T c9_loop_ub;
  int32_T c9_i652;
  c9_emxArray_real_T *c9_b_varargin_2;
  int32_T c9_i653;
  int32_T c9_b_loop_ub;
  int32_T c9_i654;
  c9_i649 = c9_I2->size[0] * c9_I2->size[1];
  c9_I2->size[0] = c9_varargin_1->size[0];
  c9_I2->size[1] = c9_varargin_1->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_I2, c9_i649, &c9_sg_emlrtRTEI);
  c9_i650 = c9_I2->size[0];
  c9_i651 = c9_I2->size[1];
  c9_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i652 = 0; c9_i652 <= c9_loop_ub; c9_i652++) {
    c9_I2->data[c9_i652] = c9_varargin_1->data[c9_i652];
  }

  c9_emxInit_real_T1(chartInstance, &c9_b_varargin_2, 1, &c9_sg_emlrtRTEI);
  c9_i653 = c9_b_varargin_2->size[0];
  c9_b_varargin_2->size[0] = c9_varargin_2->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_varargin_2, c9_i653,
    &c9_sg_emlrtRTEI);
  c9_b_loop_ub = c9_varargin_2->size[0] - 1;
  for (c9_i654 = 0; c9_i654 <= c9_b_loop_ub; c9_i654++) {
    c9_b_varargin_2->data[c9_i654] = c9_varargin_2->data[c9_i654];
  }

  c9_b_imfill(chartInstance, c9_I2, c9_b_varargin_2);
  c9_emxFree_real_T(chartInstance, &c9_b_varargin_2);
}

static void c9_d_warning(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c9_y = NULL;
  static char_T c9_cv38[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c9_b_y = NULL;
  static char_T c9_cv39[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c9_c_y = NULL;
  static char_T c9_msgID[24] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm',
    'f', 'i', 'l', 'l', ':', 'o', 'u', 't', 'O', 'f', 'R', 'a', 'n', 'g', 'e' };

  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv38, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_b_y = NULL;
  sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv39, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c9_c_y = NULL;
  sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_msgID, 10, 0U, 1U, 0U, 2, 1, 24),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c9_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c9_b_y, 14, c9_c_y));
}

static void c9_parseInputs(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_3,
  c9_emxArray_boolean_T *c9_BW, c9_emxArray_real_T *c9_theta, c9_emxArray_real_T
  *c9_rho)
{
  int32_T c9_i655;
  int32_T c9_i656;
  int32_T c9_i657;
  int32_T c9_loop_ub;
  int32_T c9_i658;
  c9_emxArray_real_T *c9_b_varargin_3;
  int32_T c9_i659;
  int32_T c9_i660;
  int32_T c9_i661;
  int32_T c9_b_loop_ub;
  int32_T c9_i662;
  c9_i655 = c9_BW->size[0] * c9_BW->size[1];
  c9_BW->size[0] = c9_varargin_1->size[0];
  c9_BW->size[1] = c9_varargin_1->size[1];
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_BW, c9_i655, &c9_tg_emlrtRTEI);
  c9_i656 = c9_BW->size[0];
  c9_i657 = c9_BW->size[1];
  c9_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i658 = 0; c9_i658 <= c9_loop_ub; c9_i658++) {
    c9_BW->data[c9_i658] = c9_varargin_1->data[c9_i658];
  }

  c9_emxInit_real_T(chartInstance, &c9_b_varargin_3, 2, &c9_tg_emlrtRTEI);
  c9_i659 = c9_b_varargin_3->size[0] * c9_b_varargin_3->size[1];
  c9_b_varargin_3->size[0] = 1;
  c9_b_varargin_3->size[1] = c9_varargin_3->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_3, c9_i659,
    &c9_tg_emlrtRTEI);
  c9_i660 = c9_b_varargin_3->size[0];
  c9_i661 = c9_b_varargin_3->size[1];
  c9_b_loop_ub = c9_varargin_3->size[0] * c9_varargin_3->size[1] - 1;
  for (c9_i662 = 0; c9_i662 <= c9_b_loop_ub; c9_i662++) {
    c9_b_varargin_3->data[c9_i662] = c9_varargin_3->data[c9_i662];
  }

  c9_b_parseInputs(chartInstance, c9_BW, c9_b_varargin_3, c9_theta, c9_rho);
  c9_emxFree_real_T(chartInstance, &c9_b_varargin_3);
}

static boolean_T c9_b_all(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_a)
{
  boolean_T c9_p;
  real_T c9_d29;
  int32_T c9_i663;
  int32_T c9_k;
  real_T c9_b_k;
  real_T c9_x;
  real_T c9_b_x;
  boolean_T c9_b;
  boolean_T c9_b45;
  real_T c9_c_x;
  boolean_T c9_b_b;
  boolean_T c9_b46;
  boolean_T c9_c_b;
  boolean_T exitg1;
  (void)chartInstance;
  c9_p = true;
  c9_d29 = (real_T)c9_a->size[1];
  c9_i663 = (int32_T)c9_d29 - 1;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k <= c9_i663)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_a->data[(int32_T)c9_b_k - 1];
    c9_b_x = c9_x;
    c9_b = muDoubleScalarIsInf(c9_b_x);
    c9_b45 = !c9_b;
    c9_c_x = c9_x;
    c9_b_b = muDoubleScalarIsNaN(c9_c_x);
    c9_b46 = !c9_b_b;
    c9_c_b = (c9_b45 && c9_b46);
    if (c9_c_b) {
      c9_k++;
    } else {
      c9_p = false;
      exitg1 = true;
    }
  }

  return c9_p;
}

static int32_T c9_findFirst(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x)
{
  int32_T c9_idx;
  int32_T c9_n;
  real_T c9_b_x;
  real_T c9_c_x;
  boolean_T c9_b;
  boolean_T c9_p;
  int32_T c9_b_b;
  int32_T c9_c_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  real_T c9_d_x;
  real_T c9_e_x;
  boolean_T c9_d_b;
  boolean_T c9_b_p;
  boolean_T exitg1;
  c9_n = c9_x->size[0];
  c9_b_x = c9_x->data[0];
  c9_c_x = c9_b_x;
  c9_b = muDoubleScalarIsNaN(c9_c_x);
  c9_p = !c9_b;
  if (c9_p) {
    c9_idx = 1;
  } else {
    c9_idx = 0;
    c9_b_b = c9_n;
    c9_c_b = c9_b_b;
    if (2 > c9_c_b) {
      c9_overflow = false;
    } else {
      c9_overflow = (c9_c_b > 2147483646);
    }

    if (c9_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    c9_k = 2;
    exitg1 = false;
    while ((!exitg1) && (c9_k <= c9_n)) {
      c9_d_x = c9_x->data[c9_k - 1];
      c9_e_x = c9_d_x;
      c9_d_b = muDoubleScalarIsNaN(c9_e_x);
      c9_b_p = !c9_d_b;
      if (c9_b_p) {
        c9_idx = c9_k;
        exitg1 = true;
      } else {
        c9_k++;
      }
    }
  }

  return c9_idx;
}

static void c9_houghpeaks(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_varargin_1, real_T c9_varargin_4, c9_emxArray_real_T
  *c9_peaks)
{
  c9_emxArray_real_T *c9_b_varargin_1;
  real_T c9_b_varargin_4;
  int32_T c9_i664;
  int32_T c9_i665;
  int32_T c9_i666;
  int32_T c9_loop_ub;
  int32_T c9_i667;
  real_T c9_numElems;
  int32_T c9_i668;
  int32_T c9_k;
  real_T c9_N;
  real_T c9_b_k;
  real_T c9_M;
  real_T c9_nhoodSizeDefault[2];
  int32_T c9_i669;
  int32_T c9_c_k;
  int32_T c9_i670;
  int32_T c9_d_k;
  real_T c9_x;
  real_T c9_b_x;
  int32_T c9_i671;
  int32_T c9_e_k;
  real_T c9_c_varargin_1[2];
  int32_T c9_f_k;
  real_T c9_c_x;
  real_T c9_d_x;
  real_T c9_ex;
  real_T c9_varargin_2;
  real_T c9_b_varargin_2;
  real_T c9_c_varargin_2;
  real_T c9_threshold;
  real_T c9_b_threshold;
  real_T c9_c_threshold;
  real_T c9_a;
  real_T c9_b_a;
  real_T c9_c_a;
  boolean_T c9_p;
  real_T c9_e_x;
  real_T c9_f_x;
  boolean_T c9_b;
  boolean_T c9_b_p;
  boolean_T c9_b47;
  const mxArray *c9_y = NULL;
  static char_T c9_cv40[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'N', 'a', 'N' };

  boolean_T c9_c_p;
  const mxArray *c9_b_y = NULL;
  int32_T c9_g_k;
  const mxArray *c9_c_y = NULL;
  static char_T c9_cv41[9] = { 'T', 'h', 'r', 'e', 's', 'h', 'o', 'l', 'd' };

  real_T c9_h_k;
  real_T c9_g_x;
  boolean_T c9_b48;
  real_T c9_h_x;
  boolean_T c9_b_b;
  boolean_T c9_b49;
  const mxArray *c9_d_y = NULL;
  real_T c9_i_x;
  int32_T c9_i672;
  boolean_T c9_c_b;
  const mxArray *c9_e_y = NULL;
  boolean_T c9_b50;
  boolean_T c9_d_b;
  const mxArray *c9_f_y = NULL;
  real_T c9_b_nhoodSizeDefault[2];
  static char_T c9_cv42[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  boolean_T c9_b51;
  const mxArray *c9_g_y = NULL;
  boolean_T c9_d_p;
  const mxArray *c9_h_y = NULL;
  int32_T c9_i_k;
  const mxArray *c9_i_y = NULL;
  static char_T c9_cv43[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c9_j_k;
  real_T c9_j_x;
  boolean_T c9_b52;
  boolean_T c9_e_p;
  const mxArray *c9_j_y = NULL;
  static char_T c9_cv44[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'P', 'o', 's', 'i', 't', 'i', 'v', 'e' };

  boolean_T c9_f_p;
  const mxArray *c9_k_y = NULL;
  int32_T c9_k_k;
  const mxArray *c9_l_y = NULL;
  static char_T c9_cv45[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c9_l_k;
  real_T c9_k_x;
  boolean_T c9_b53;
  real_T c9_l_x;
  real_T c9_m_x;
  real_T c9_n_x;
  const mxArray *c9_m_y = NULL;
  real_T c9_o_x;
  static char_T c9_cv46[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'O', 'd', 'd' };

  int32_T c9_i673;
  real_T c9_p_x;
  const mxArray *c9_n_y = NULL;
  real_T c9_q_x;
  static char_T c9_cv47[43] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
    'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd',
    'O', 'd', 'd' };

  boolean_T c9_e_b;
  const mxArray *c9_o_y = NULL;
  int32_T c9_i674;
  boolean_T c9_b54;
  static char_T c9_cv48[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c9_r_x;
  boolean_T c9_f_b;
  boolean_T c9_p_y;
  boolean_T c9_s_x[2];
  boolean_T c9_b55;
  int32_T c9_m_k;
  boolean_T c9_g_b;
  real_T c9_r;
  real_T c9_n_k;
  boolean_T c9_rEQ0;
  boolean_T c9_g_p;
  const mxArray *c9_q_y = NULL;
  boolean_T c9_b56;
  static char_T c9_cv49[33] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 't', 'o', 'o', 'B', 'i', 'g', 'N',
    'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  boolean_T c9_h_p;
  const mxArray *c9_r_y = NULL;
  int32_T c9_o_k;
  const mxArray *c9_s_y = NULL;
  static char_T c9_cv50[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c9_p_k;
  real_T c9_t_x;
  boolean_T c9_b57;
  real_T c9_u_x;
  boolean_T c9_h_b;
  boolean_T c9_b58;
  const mxArray *c9_t_y = NULL;
  real_T c9_v_x;
  int32_T c9_ixLead;
  boolean_T c9_i_b;
  const mxArray *c9_u_y = NULL;
  int32_T c9_iyLead;
  boolean_T c9_b59;
  real_T c9_work;
  boolean_T c9_j_b;
  const mxArray *c9_v_y = NULL;
  int32_T c9_m;
  static char_T c9_cv51[5] = { 'T', 'h', 'e', 't', 'a' };

  real_T c9_y1[179];
  real_T c9_w_x[178];
  real_T c9_tmp1;
  real_T c9_w_y;
  real_T c9_tmp2;
  int32_T c9_q_k;
  real_T c9_x_x;
  int32_T c9_xoffset;
  real_T c9_y_x;
  int32_T c9_ix;
  real_T c9_ab_x;
  real_T c9_x_y;
  const mxArray *c9_y_y = NULL;
  static char_T c9_cv52[43] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd',
    'T', 'h', 'e', 't', 'a', 'V', 'e', 'c', 't', 'o', 'r', 'S', 'p', 'a', 'c',
    'i', 'n', 'g' };

  const mxArray *c9_ab_y = NULL;
  c9_emxArray_real_T *c9_hnew;
  int32_T c9_i675;
  int32_T c9_numRowH;
  int32_T c9_numColH;
  boolean_T c9_isDone;
  int32_T c9_i676;
  int32_T c9_i677;
  int32_T c9_i678;
  int32_T c9_i679;
  int32_T c9_i680;
  int32_T c9_b_loop_ub;
  int32_T c9_i681;
  c9_emxArray_real_T *c9_peakCoordinates;
  int32_T c9_nhoodCenter_i;
  int32_T c9_nhoodCenter_j;
  int32_T c9_i682;
  int32_T c9_peakIdx;
  real_T c9_b_ex;
  int32_T c9_r_k;
  real_T c9_minTheta;
  real_T c9_c_ex;
  int32_T c9_s_k;
  real_T c9_maxTheta;
  real_T c9_bb_x;
  real_T c9_cb_x;
  real_T c9_db_x;
  real_T c9_bb_y;
  real_T c9_thetaResolution;
  real_T c9_eb_x;
  real_T c9_fb_x;
  real_T c9_gb_x;
  real_T c9_cb_y;
  boolean_T c9_isThetaAntisymmetric;
  int32_T c9_iPeak;
  int32_T c9_jPeak;
  int32_T c9_i683;
  real_T c9_maxVal;
  int32_T c9_i684;
  int32_T c9_b_M;
  int32_T c9_i685;
  int32_T c9_b_N;
  int32_T c9_i686;
  int32_T c9_k_b;
  int32_T c9_i687;
  int32_T c9_l_b;
  int32_T c9_i688;
  boolean_T c9_overflow;
  int32_T c9_c_loop_ub;
  int32_T c9_i689;
  int32_T c9_j;
  int32_T c9_b_j;
  int32_T c9_m_b;
  int32_T c9_n_b;
  int32_T c9_rhoMin;
  boolean_T c9_b_overflow;
  int32_T c9_rhoMax;
  int32_T c9_thetaMin;
  int32_T c9_thetaMax;
  int32_T c9_i;
  int32_T c9_b_i;
  int32_T c9_d_a;
  real_T c9_val;
  int32_T c9_o_b;
  int32_T c9_e_a;
  int32_T c9_p_b;
  boolean_T c9_c_overflow;
  int32_T c9_theta;
  int32_T c9_f_a;
  int32_T c9_q_b;
  int32_T c9_g_a;
  int32_T c9_r_b;
  boolean_T c9_d_overflow;
  int32_T c9_rho;
  int32_T c9_rhoToRemove;
  int32_T c9_thetaToRemove;
  boolean_T exitg1;
  c9_emxInit_real_T(chartInstance, &c9_b_varargin_1, 2, &c9_ug_emlrtRTEI);
  c9_b_varargin_4 = c9_varargin_4;
  c9_i664 = c9_b_varargin_1->size[0] * c9_b_varargin_1->size[1];
  c9_b_varargin_1->size[0] = c9_varargin_1->size[0];
  c9_b_varargin_1->size[1] = c9_varargin_1->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_1, c9_i664,
    &c9_ug_emlrtRTEI);
  c9_i665 = c9_b_varargin_1->size[0];
  c9_i666 = c9_b_varargin_1->size[1];
  c9_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
  for (c9_i667 = 0; c9_i667 <= c9_loop_ub; c9_i667++) {
    c9_b_varargin_1->data[c9_i667] = c9_varargin_1->data[c9_i667];
  }

  c9_validateattributes(chartInstance, c9_b_varargin_1);
  c9_numElems = (real_T)(c9_varargin_1->size[0] * c9_varargin_1->size[1]);
  c9_i668 = (int32_T)c9_numElems - 1;
  c9_k = 0;
  c9_emxFree_real_T(chartInstance, &c9_b_varargin_1);
  while (c9_k <= c9_i668) {
    c9_b_k = 1.0 + (real_T)c9_k;
    (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_b_k, 1,
      c9_varargin_1->size[0] * c9_varargin_1->size[1]);
    c9_k++;
  }

  c9_N = (real_T)c9_varargin_1->size[1];
  c9_M = (real_T)c9_varargin_1->size[0];
  c9_nhoodSizeDefault[0] = c9_M / 50.0;
  c9_nhoodSizeDefault[1] = c9_N / 50.0;
  for (c9_i669 = 0; c9_i669 < 2; c9_i669++) {
    c9_nhoodSizeDefault[c9_i669] /= 2.0;
  }

  for (c9_c_k = 0; c9_c_k < 2; c9_c_k++) {
    c9_d_k = c9_c_k;
    c9_x = c9_nhoodSizeDefault[c9_d_k];
    c9_b_x = c9_x;
    c9_b_x = muDoubleScalarCeil(c9_b_x);
    c9_nhoodSizeDefault[c9_d_k] = c9_b_x;
  }

  for (c9_i670 = 0; c9_i670 < 2; c9_i670++) {
    c9_nhoodSizeDefault[c9_i670] *= 2.0;
  }

  for (c9_i671 = 0; c9_i671 < 2; c9_i671++) {
    c9_c_varargin_1[c9_i671] = c9_nhoodSizeDefault[c9_i671] + 1.0;
  }

  for (c9_e_k = 0; c9_e_k < 2; c9_e_k++) {
    c9_f_k = c9_e_k;
    c9_c_x = c9_c_varargin_1[c9_f_k];
    c9_d_x = c9_c_x;
    c9_ex = c9_d_x;
    c9_nhoodSizeDefault[c9_f_k] = c9_ex;
  }

  if (c9_M < 3.0) {
    c9_nhoodSizeDefault[0] = 1.0;
  }

  if (c9_N < 3.0) {
    c9_nhoodSizeDefault[1] = 1.0;
  }

  c9_varargin_2 = c9_b_varargin_4;
  c9_b_varargin_2 = c9_varargin_2;
  c9_c_varargin_2 = c9_b_varargin_2;
  c9_threshold = c9_c_varargin_2;
  c9_b_threshold = c9_threshold;
  c9_c_threshold = c9_threshold;
  c9_a = c9_c_threshold;
  c9_b_a = c9_a;
  c9_c_a = c9_b_a;
  c9_p = true;
  c9_e_x = c9_c_a;
  c9_f_x = c9_e_x;
  c9_b = muDoubleScalarIsNaN(c9_f_x);
  c9_b_p = !c9_b;
  if (c9_b_p) {
  } else {
    c9_p = false;
  }

  if (c9_p) {
    c9_b47 = true;
  } else {
    c9_b47 = false;
  }

  if (c9_b47) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv40, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv6, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv41, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c9_b_y, 14, c9_c_y)));
  }

  c9_c_p = true;
  c9_g_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_g_k < 2)) {
    c9_h_k = 1.0 + (real_T)c9_g_k;
    c9_g_x = c9_nhoodSizeDefault[(int32_T)c9_h_k - 1];
    c9_h_x = c9_g_x;
    c9_b_b = muDoubleScalarIsInf(c9_h_x);
    c9_b49 = !c9_b_b;
    c9_i_x = c9_g_x;
    c9_c_b = muDoubleScalarIsNaN(c9_i_x);
    c9_b50 = !c9_c_b;
    c9_d_b = (c9_b49 && c9_b50);
    if (c9_d_b) {
      c9_g_k++;
    } else {
      c9_c_p = false;
      exitg1 = true;
    }
  }

  if (c9_c_p) {
    c9_b48 = true;
  } else {
    c9_b48 = false;
  }

  if (c9_b48) {
  } else {
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv9, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv42, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_e_y, 14, c9_f_y)));
  }

  for (c9_i672 = 0; c9_i672 < 2; c9_i672++) {
    c9_b_nhoodSizeDefault[c9_i672] = c9_nhoodSizeDefault[c9_i672];
  }

  if (c9_all(chartInstance, c9_b_nhoodSizeDefault)) {
    c9_b51 = true;
  } else {
    c9_b51 = false;
  }

  if (c9_b51) {
  } else {
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv11, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv7, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c9_i_y = NULL;
    sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv43, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_h_y, 14, c9_i_y)));
  }

  c9_d_p = true;
  c9_i_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_i_k < 2)) {
    c9_j_k = 1.0 + (real_T)c9_i_k;
    c9_j_x = c9_nhoodSizeDefault[(int32_T)c9_j_k - 1];
    c9_e_p = !(c9_j_x <= 0.0);
    if (c9_e_p) {
      c9_i_k++;
    } else {
      c9_d_p = false;
      exitg1 = true;
    }
  }

  if (c9_d_p) {
    c9_b52 = true;
  } else {
    c9_b52 = false;
  }

  if (c9_b52) {
  } else {
    c9_j_y = NULL;
    sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv44, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_k_y = NULL;
    sf_mex_assign(&c9_k_y, sf_mex_create("y", c9_cv12, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_l_y = NULL;
    sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv45, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_k_y, 14, c9_l_y)));
  }

  c9_f_p = true;
  c9_k_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k_k < 2)) {
    c9_l_k = 1.0 + (real_T)c9_k_k;
    c9_k_x = c9_nhoodSizeDefault[(int32_T)c9_l_k - 1];
    c9_l_x = c9_k_x;
    c9_m_x = c9_l_x;
    c9_n_x = c9_m_x;
    c9_o_x = c9_n_x;
    c9_p_x = c9_o_x;
    c9_q_x = c9_p_x;
    c9_e_b = muDoubleScalarIsInf(c9_q_x);
    c9_b54 = !c9_e_b;
    c9_r_x = c9_p_x;
    c9_f_b = muDoubleScalarIsNaN(c9_r_x);
    c9_b55 = !c9_f_b;
    c9_g_b = (c9_b54 && c9_b55);
    if (c9_g_b) {
      c9_r = muDoubleScalarRem(c9_o_x, 2.0);
      c9_rEQ0 = (c9_r == 0.0);
      if (c9_rEQ0) {
        c9_r = 0.0;
      }
    } else {
      c9_r = rtNaN;
    }

    c9_g_p = (c9_r == 1.0);
    if (c9_g_p) {
      c9_k_k++;
    } else {
      c9_f_p = false;
      exitg1 = true;
    }
  }

  if (c9_f_p) {
    c9_b53 = true;
  } else {
    c9_b53 = false;
  }

  if (c9_b53) {
  } else {
    c9_m_y = NULL;
    sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv46, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c9_n_y = NULL;
    sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv47, 10, 0U, 1U, 0U, 2, 1, 43),
                  false);
    c9_o_y = NULL;
    sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv48, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_m_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_n_y, 14, c9_o_y)));
  }

  for (c9_i673 = 0; c9_i673 < 2; c9_i673++) {
    c9_c_varargin_1[c9_i673] = (real_T)c9_varargin_1->size[c9_i673];
  }

  for (c9_i674 = 0; c9_i674 < 2; c9_i674++) {
    c9_s_x[c9_i674] = (c9_nhoodSizeDefault[c9_i674] > c9_c_varargin_1[c9_i674]);
  }

  c9_p_y = false;
  c9_m_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_m_k < 2)) {
    c9_n_k = 1.0 + (real_T)c9_m_k;
    if (!c9_s_x[(int32_T)c9_n_k - 1]) {
      c9_b56 = true;
    } else {
      c9_b56 = false;
    }

    if (!c9_b56) {
      c9_p_y = true;
      exitg1 = true;
    } else {
      c9_m_k++;
    }
  }

  if (!c9_p_y) {
  } else {
    c9_q_y = NULL;
    sf_mex_assign(&c9_q_y, sf_mex_create("y", c9_cv49, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c9_r_y = NULL;
    sf_mex_assign(&c9_r_y, sf_mex_create("y", c9_cv49, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c9_s_y = NULL;
    sf_mex_assign(&c9_s_y, sf_mex_create("y", c9_cv50, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_q_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_r_y, 14, c9_s_y)));
  }

  c9_h_p = true;
  c9_o_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_o_k < 180)) {
    c9_p_k = 1.0 + (real_T)c9_o_k;
    c9_t_x = -90.0 + (real_T)((int32_T)c9_p_k - 1);
    c9_u_x = c9_t_x;
    c9_h_b = muDoubleScalarIsInf(c9_u_x);
    c9_b58 = !c9_h_b;
    c9_v_x = c9_t_x;
    c9_i_b = muDoubleScalarIsNaN(c9_v_x);
    c9_b59 = !c9_i_b;
    c9_j_b = (c9_b58 && c9_b59);
    if (c9_j_b) {
      c9_o_k++;
    } else {
      c9_h_p = false;
      exitg1 = true;
    }
  }

  if (c9_h_p) {
    c9_b57 = true;
  } else {
    c9_b57 = false;
  }

  if (c9_b57) {
  } else {
    c9_t_y = NULL;
    sf_mex_assign(&c9_t_y, sf_mex_create("y", c9_cv9, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_u_y = NULL;
    sf_mex_assign(&c9_u_y, sf_mex_create("y", c9_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_v_y = NULL;
    sf_mex_assign(&c9_v_y, sf_mex_create("y", c9_cv51, 10, 0U, 1U, 0U, 2, 1, 5),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_t_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_u_y, 14, c9_v_y)));
  }

  c9_ixLead = 1;
  c9_iyLead = 0;
  c9_work = -90.0;
  for (c9_m = 0; c9_m < 179; c9_m++) {
    c9_tmp1 = -90.0 + (real_T)c9_ixLead;
    c9_tmp2 = c9_work;
    c9_work = c9_tmp1;
    c9_tmp1 -= c9_tmp2;
    c9_ixLead++;
    c9_y1[c9_iyLead] = c9_tmp1;
    c9_iyLead++;
  }

  c9_diff(chartInstance, c9_y1, c9_w_x);
  c9_w_y = c9_w_x[0];
  for (c9_q_k = 0; c9_q_k < 177; c9_q_k++) {
    c9_xoffset = c9_q_k;
    c9_ix = c9_xoffset;
    c9_w_y += c9_w_x[c9_ix + 1];
  }

  c9_x_x = c9_w_y;
  c9_y_x = c9_x_x;
  c9_ab_x = c9_y_x;
  c9_x_y = muDoubleScalarAbs(c9_ab_x);
  if (!(c9_x_y > 1.4901161193847656E-8)) {
  } else {
    c9_y_y = NULL;
    sf_mex_assign(&c9_y_y, sf_mex_create("y", c9_cv52, 10, 0U, 1U, 0U, 2, 1, 43),
                  false);
    c9_ab_y = NULL;
    sf_mex_assign(&c9_ab_y, sf_mex_create("y", c9_cv52, 10, 0U, 1U, 0U, 2, 1, 43),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_ab_y)));
  }

  if ((real_T)(c9_varargin_1->size[0] * c9_varargin_1->size[1]) < 1.0) {
    c9_i675 = c9_peaks->size[0] * c9_peaks->size[1];
    c9_peaks->size[0] = 0;
    c9_peaks->size[1] = 0;
    c9_i676 = c9_peaks->size[0];
    c9_i678 = c9_peaks->size[1];
  } else {
    c9_emxInit_real_T(chartInstance, &c9_hnew, 2, &c9_vg_emlrtRTEI);
    c9_numRowH = c9_varargin_1->size[0];
    c9_numColH = c9_varargin_1->size[1];
    c9_isDone = false;
    c9_i677 = c9_hnew->size[0] * c9_hnew->size[1];
    c9_hnew->size[0] = c9_varargin_1->size[0];
    c9_hnew->size[1] = c9_varargin_1->size[1];
    c9_emxEnsureCapacity_real_T(chartInstance, c9_hnew, c9_i677,
      &c9_vg_emlrtRTEI);
    c9_i679 = c9_hnew->size[0];
    c9_i680 = c9_hnew->size[1];
    c9_b_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
    for (c9_i681 = 0; c9_i681 <= c9_b_loop_ub; c9_i681++) {
      c9_hnew->data[c9_i681] = c9_varargin_1->data[c9_i681];
    }

    c9_emxInit_real_T(chartInstance, &c9_peakCoordinates, 2, &c9_yg_emlrtRTEI);
    c9_nhoodCenter_i = (int32_T)((c9_nhoodSizeDefault[0] - 1.0) / 2.0);
    c9_nhoodCenter_j = (int32_T)((c9_nhoodSizeDefault[1] - 1.0) / 2.0);
    c9_i682 = c9_peakCoordinates->size[0] * c9_peakCoordinates->size[1];
    c9_peakCoordinates->size[0] = c9_varargin_1->size[0] * c9_varargin_1->size[1];
    c9_peakCoordinates->size[1] = 2;
    c9_emxEnsureCapacity_real_T(chartInstance, c9_peakCoordinates, c9_i682,
      &c9_wg_emlrtRTEI);
    c9_peakIdx = 0;
    c9_b_ex = -90.0;
    for (c9_r_k = 1; c9_r_k + 1 < 181; c9_r_k++) {
      if (c9_b_ex > -90.0 + (real_T)c9_r_k) {
        c9_b_ex = -90.0 + (real_T)c9_r_k;
      }
    }

    c9_minTheta = c9_b_ex;
    c9_c_ex = -90.0;
    for (c9_s_k = 1; c9_s_k + 1 < 181; c9_s_k++) {
      if (c9_c_ex < -90.0 + (real_T)c9_s_k) {
        c9_c_ex = -90.0 + (real_T)c9_s_k;
      }
    }

    c9_maxTheta = c9_c_ex;
    c9_bb_x = c9_maxTheta - c9_minTheta;
    c9_cb_x = c9_bb_x;
    c9_db_x = c9_cb_x;
    c9_bb_y = muDoubleScalarAbs(c9_db_x);
    c9_thetaResolution = c9_bb_y / 179.0;
    c9_eb_x = c9_minTheta + c9_thetaResolution * c9_nhoodSizeDefault[1];
    c9_fb_x = c9_eb_x;
    c9_gb_x = c9_fb_x;
    c9_cb_y = muDoubleScalarAbs(c9_gb_x);
    c9_isThetaAntisymmetric = (c9_cb_y <= c9_maxTheta);
    while (!c9_isDone) {
      c9_iPeak = 0;
      c9_jPeak = 0;
      c9_maxVal = -1.0;
      c9_b_M = c9_hnew->size[0];
      c9_b_N = c9_hnew->size[1];
      c9_k_b = c9_b_N;
      c9_l_b = c9_k_b;
      if (1 > c9_l_b) {
        c9_overflow = false;
      } else {
        c9_overflow = (c9_l_b > 2147483646);
      }

      if (c9_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }

      for (c9_j = 1; c9_j - 1 < c9_b_N; c9_j++) {
        c9_b_j = c9_j;
        c9_m_b = c9_b_M;
        c9_n_b = c9_m_b;
        if (1 > c9_n_b) {
          c9_b_overflow = false;
        } else {
          c9_b_overflow = (c9_n_b > 2147483646);
        }

        if (c9_b_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_i = 1; c9_i - 1 < c9_b_M; c9_i++) {
          c9_b_i = c9_i;
          c9_val = c9_hnew->data[(sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c9_b_i, 1, c9_hnew->size[0]) + c9_hnew->size[0] *
            (sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_b_j, 1, c9_hnew->size[1])
             - 1)) - 1];
          if (c9_val > c9_maxVal) {
            c9_iPeak = c9_b_i;
            c9_jPeak = c9_b_j;
            c9_maxVal = c9_val;
          }
        }
      }

      if (c9_hnew->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_iPeak, 1, c9_hnew->
            size[0]) + c9_hnew->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c9_jPeak, 1, c9_hnew->size[1]) - 1)) - 1] >=
          c9_b_threshold) {
        c9_peakIdx++;
        c9_peakCoordinates->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c9_peakIdx, 1, c9_peakCoordinates->size[0]) - 1] = (real_T)c9_iPeak;
        c9_peakCoordinates->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c9_peakIdx, 1, c9_peakCoordinates->size[0]) +
          c9_peakCoordinates->size[0]) - 1] = (real_T)c9_jPeak;
        c9_rhoMin = c9_iPeak - c9_nhoodCenter_i;
        c9_rhoMax = c9_iPeak + c9_nhoodCenter_i;
        c9_thetaMin = c9_jPeak - c9_nhoodCenter_j;
        c9_thetaMax = c9_jPeak + c9_nhoodCenter_j;
        if (c9_rhoMin < 1) {
          c9_rhoMin = 1;
        }

        if (c9_rhoMax > c9_numRowH) {
          c9_rhoMax = c9_numRowH;
        }

        c9_d_a = c9_thetaMin;
        c9_o_b = c9_thetaMax;
        c9_e_a = c9_d_a;
        c9_p_b = c9_o_b;
        if (c9_e_a > c9_p_b) {
          c9_c_overflow = false;
        } else {
          c9_c_overflow = (c9_p_b > 2147483646);
        }

        if (c9_c_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_theta = c9_thetaMin; c9_theta <= c9_thetaMax; c9_theta++) {
          c9_f_a = c9_rhoMin;
          c9_q_b = c9_rhoMax;
          c9_g_a = c9_f_a;
          c9_r_b = c9_q_b;
          if (c9_g_a > c9_r_b) {
            c9_d_overflow = false;
          } else {
            c9_d_overflow = (c9_r_b > 2147483646);
          }

          if (c9_d_overflow) {
            c9_check_forloop_overflow_error(chartInstance, true);
          }

          for (c9_rho = c9_rhoMin; c9_rho <= c9_rhoMax; c9_rho++) {
            c9_rhoToRemove = c9_rho - 1;
            c9_thetaToRemove = c9_theta;
            if (c9_isThetaAntisymmetric) {
              if (c9_theta > c9_numColH) {
                c9_rhoToRemove = c9_numRowH - c9_rho;
                c9_thetaToRemove = c9_theta - c9_numColH;
              } else {
                if (c9_theta < 1) {
                  c9_rhoToRemove = c9_numRowH - c9_rho;
                  c9_thetaToRemove = c9_theta + c9_numColH;
                }
              }
            }

            if ((c9_thetaToRemove > c9_numColH) || (c9_thetaToRemove < 1)) {
            } else {
              c9_hnew->data[(sf_eml_array_bounds_check
                             (sfGlobalDebugInstanceStruct, chartInstance->S, 1U,
                              0, 0, MAX_uint32_T, c9_rhoToRemove + 1, 1,
                              c9_hnew->size[0]) + c9_hnew->size[0] *
                             (sf_eml_array_bounds_check
                              (sfGlobalDebugInstanceStruct, chartInstance->S, 1U,
                               0, 0, MAX_uint32_T, c9_thetaToRemove, 1,
                               c9_hnew->size[1]) - 1)) - 1] = 0.0;
            }
          }
        }

        c9_isDone = (c9_peakIdx == 30);
      } else {
        c9_isDone = true;
      }
    }

    c9_emxFree_real_T(chartInstance, &c9_hnew);
    if (c9_peakIdx == 0) {
      c9_i683 = c9_peaks->size[0] * c9_peaks->size[1];
      c9_peaks->size[0] = 0;
      c9_peaks->size[1] = 0;
      c9_i686 = c9_peaks->size[0];
      c9_i687 = c9_peaks->size[1];
    } else {
      sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S,
        1U, 0, 0, MAX_uint32_T, 1, 1, c9_peakCoordinates->size[0]);
      c9_i684 = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_peakIdx, 1,
        c9_peakCoordinates->size[0]);
      c9_i685 = c9_peaks->size[0] * c9_peaks->size[1];
      c9_peaks->size[0] = c9_i684;
      c9_peaks->size[1] = 2;
      c9_emxEnsureCapacity_real_T(chartInstance, c9_peaks, c9_i685,
        &c9_xg_emlrtRTEI);
      for (c9_i688 = 0; c9_i688 < 2; c9_i688++) {
        c9_c_loop_ub = c9_i684 - 1;
        for (c9_i689 = 0; c9_i689 <= c9_c_loop_ub; c9_i689++) {
          c9_peaks->data[c9_i689 + c9_peaks->size[0] * c9_i688] =
            c9_peakCoordinates->data[c9_i689 + c9_peakCoordinates->size[0] *
            c9_i688];
        }
      }
    }

    c9_emxFree_real_T(chartInstance, &c9_peakCoordinates);
  }
}

static void c9_validateattributes(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_a)
{
  boolean_T c9_b60;
  boolean_T c9_b61;
  const mxArray *c9_y = NULL;
  static char_T c9_cv53[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  boolean_T c9_p;
  const mxArray *c9_b_y = NULL;
  real_T c9_d30;
  int32_T c9_i690;
  const mxArray *c9_c_y = NULL;
  int32_T c9_k;
  static char_T c9_cv54[18] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '1', ',', ' ', 'H', ',' };

  real_T c9_b_k;
  real_T c9_x;
  boolean_T c9_b62;
  real_T c9_b_x;
  boolean_T c9_b;
  boolean_T c9_b63;
  const mxArray *c9_d_y = NULL;
  real_T c9_c_x;
  c9_emxArray_real_T *c9_b_a;
  boolean_T c9_b_b;
  const mxArray *c9_e_y = NULL;
  int32_T c9_i691;
  boolean_T c9_b64;
  boolean_T c9_c_b;
  const mxArray *c9_f_y = NULL;
  static char_T c9_cv55[18] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '1', ',', ' ', 'H', ',' };

  int32_T c9_i692;
  int32_T c9_i693;
  int32_T c9_loop_ub;
  int32_T c9_i694;
  boolean_T c9_b65;
  const mxArray *c9_g_y = NULL;
  const mxArray *c9_h_y = NULL;
  const mxArray *c9_i_y = NULL;
  static char_T c9_cv56[18] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '1', ',', ' ', 'H', ',' };

  boolean_T exitg1;
  c9_b60 = (c9_a->size[0] == 0);
  c9_b61 = (c9_a->size[1] == 0);
  if ((!c9_b60) && (!c9_b61)) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv53, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv13, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv54, 10, 0U, 1U, 0U, 2, 1, 18),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c9_b_y, 14, c9_c_y)));
  }

  c9_p = true;
  c9_d30 = (real_T)(c9_a->size[0] * c9_a->size[1]);
  c9_i690 = (int32_T)c9_d30 - 1;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k <= c9_i690)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_a->data[(int32_T)c9_b_k - 1];
    c9_b_x = c9_x;
    c9_b = muDoubleScalarIsInf(c9_b_x);
    c9_b63 = !c9_b;
    c9_c_x = c9_x;
    c9_b_b = muDoubleScalarIsNaN(c9_c_x);
    c9_b64 = !c9_b_b;
    c9_c_b = (c9_b63 && c9_b64);
    if (c9_c_b) {
      c9_k++;
    } else {
      c9_p = false;
      exitg1 = true;
    }
  }

  if (c9_p) {
    c9_b62 = true;
  } else {
    c9_b62 = false;
  }

  if (c9_b62) {
  } else {
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv9, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv55, 10, 0U, 1U, 0U, 2, 1, 18),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_e_y, 14, c9_f_y)));
  }

  c9_emxInit_real_T(chartInstance, &c9_b_a, 2, &c9_ah_emlrtRTEI);
  c9_i691 = c9_b_a->size[0] * c9_b_a->size[1];
  c9_b_a->size[0] = c9_a->size[0];
  c9_b_a->size[1] = c9_a->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_a, c9_i691, &c9_ah_emlrtRTEI);
  c9_i692 = c9_b_a->size[0];
  c9_i693 = c9_b_a->size[1];
  c9_loop_ub = c9_a->size[0] * c9_a->size[1] - 1;
  for (c9_i694 = 0; c9_i694 <= c9_loop_ub; c9_i694++) {
    c9_b_a->data[c9_i694] = c9_a->data[c9_i694];
  }

  if (c9_c_all(chartInstance, c9_b_a)) {
    c9_b65 = true;
  } else {
    c9_b65 = false;
  }

  c9_emxFree_real_T(chartInstance, &c9_b_a);
  if (c9_b65) {
  } else {
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv11, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv7, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c9_i_y = NULL;
    sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv56, 10, 0U, 1U, 0U, 2, 1, 18),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_h_y, 14, c9_i_y)));
  }
}

static boolean_T c9_c_all(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_a)
{
  boolean_T c9_p;
  real_T c9_d31;
  int32_T c9_i695;
  int32_T c9_k;
  real_T c9_b_k;
  real_T c9_x;
  real_T c9_b_x;
  real_T c9_c_x;
  boolean_T c9_b;
  boolean_T c9_b66;
  real_T c9_d_x;
  boolean_T c9_b_b;
  boolean_T c9_b67;
  boolean_T c9_c_b;
  real_T c9_e_x;
  boolean_T c9_b_p;
  real_T c9_f_x;
  boolean_T c9_c_p;
  boolean_T exitg1;
  (void)chartInstance;
  c9_p = true;
  c9_d31 = (real_T)(c9_a->size[0] * c9_a->size[1]);
  c9_i695 = (int32_T)c9_d31 - 1;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k <= c9_i695)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_a->data[(int32_T)c9_b_k - 1];
    c9_b_x = c9_x;
    c9_c_x = c9_b_x;
    c9_b = muDoubleScalarIsInf(c9_c_x);
    c9_b66 = !c9_b;
    c9_d_x = c9_b_x;
    c9_b_b = muDoubleScalarIsNaN(c9_d_x);
    c9_b67 = !c9_b_b;
    c9_c_b = (c9_b66 && c9_b67);
    if (c9_c_b) {
      c9_e_x = c9_x;
      c9_f_x = c9_e_x;
      c9_f_x = muDoubleScalarFloor(c9_f_x);
      if (c9_f_x == c9_x) {
        c9_b_p = true;
      } else {
        c9_b_p = false;
      }
    } else {
      c9_b_p = false;
    }

    c9_c_p = c9_b_p;
    if (c9_c_p) {
      c9_k++;
    } else {
      c9_p = false;
      exitg1 = true;
    }
  }

  return c9_p;
}

static void c9_diff(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T c9_x[179],
                    real_T c9_y[178])
{
  int32_T c9_ixLead;
  int32_T c9_iyLead;
  real_T c9_work;
  int32_T c9_m;
  real_T c9_tmp1;
  real_T c9_tmp2;
  (void)chartInstance;
  c9_ixLead = 1;
  c9_iyLead = 0;
  c9_work = c9_x[0];
  for (c9_m = 0; c9_m < 178; c9_m++) {
    c9_tmp1 = c9_x[c9_ixLead];
    c9_tmp2 = c9_work;
    c9_work = c9_tmp1;
    c9_tmp1 -= c9_tmp2;
    c9_ixLead++;
    c9_y[c9_iyLead] = c9_tmp1;
    c9_iyLead++;
  }
}

static void c9_houghlines(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_2,
  c9_emxArray_real_T *c9_varargin_3, c9_emxArray_real_T *c9_varargin_4,
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_lines)
{
  boolean_T c9_b68;
  boolean_T c9_b69;
  const mxArray *c9_y = NULL;
  static char_T c9_cv57[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  c9_emxArray_real_T *c9_b_varargin_2;
  const mxArray *c9_b_y = NULL;
  int32_T c9_i696;
  const mxArray *c9_c_y = NULL;
  int32_T c9_i697;
  int32_T c9_i698;
  int32_T c9_loop_ub;
  int32_T c9_i699;
  boolean_T c9_b70;
  const mxArray *c9_d_y = NULL;
  static char_T c9_cv58[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'F', 'i', 'n', 'i', 't', 'e' };

  boolean_T c9_b71;
  const mxArray *c9_e_y = NULL;
  const mxArray *c9_f_y = NULL;
  const mxArray *c9_g_y = NULL;
  static char_T c9_cv59[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  c9_emxArray_real_T *c9_b_varargin_3;
  static char_T c9_cv60[22] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'T', 'H', 'E', 'T', 'A', ',' };

  const mxArray *c9_h_y = NULL;
  int32_T c9_i700;
  const mxArray *c9_i_y = NULL;
  static char_T c9_cv61[22] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'T', 'H', 'E', 'T', 'A', ',' };

  int32_T c9_i701;
  int32_T c9_i702;
  int32_T c9_b_loop_ub;
  int32_T c9_i703;
  boolean_T c9_b72;
  const mxArray *c9_j_y = NULL;
  static char_T c9_cv62[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'F', 'i', 'n', 'i', 't', 'e' };

  boolean_T c9_b73;
  const mxArray *c9_k_y = NULL;
  const mxArray *c9_l_y = NULL;
  const mxArray *c9_m_y = NULL;
  static char_T c9_cv63[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  boolean_T c9_p;
  static char_T c9_cv64[20] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '3', ',', ' ', 'R', 'H', 'O', ',' };

  const mxArray *c9_n_y = NULL;
  real_T c9_d32;
  int32_T c9_i704;
  const mxArray *c9_o_y = NULL;
  int32_T c9_k;
  static char_T c9_cv65[20] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '3', ',', ' ', 'R', 'H', 'O', ',' };

  real_T c9_b_k;
  real_T c9_x;
  boolean_T c9_b74;
  boolean_T c9_b_p;
  const mxArray *c9_p_y = NULL;
  static char_T c9_cv66[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'P', 'o', 's', 'i', 't', 'i', 'v', 'e' };

  c9_emxArray_real_T *c9_b_varargin_4;
  const mxArray *c9_q_y = NULL;
  int32_T c9_i705;
  const mxArray *c9_r_y = NULL;
  static char_T c9_cv67[22] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '4', ',', ' ', 'P', 'E', 'A', 'K', 'S', ',' };

  int32_T c9_i706;
  int32_T c9_i707;
  int32_T c9_c_loop_ub;
  int32_T c9_i708;
  boolean_T c9_b75;
  const mxArray *c9_s_y = NULL;
  static char_T c9_cv68[33] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'I', 'n', 't', 'e', 'g', 'e', 'r' };

  const mxArray *c9_t_y = NULL;
  const mxArray *c9_u_y = NULL;
  static char_T c9_cv69[30] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd',
    'P', 'E', 'A', 'K', 'S' };

  c9_emxArray_real_T *c9_peaks;
  const mxArray *c9_v_y = NULL;
  const mxArray *c9_w_y = NULL;
  int32_T c9_i709;
  static char_T c9_cv70[22] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '4', ',', ' ', 'P', 'E', 'A', 'K', 'S', ',' };

  int32_T c9_i710;
  int32_T c9_i711;
  int32_T c9_d_loop_ub;
  int32_T c9_i712;
  real_T c9_numCol;
  real_T c9_numRow;
  int32_T c9_numNonZero;
  int32_T c9_i713;
  int32_T c9_j;
  c9_emxArray_int32_T *c9_nonZeroPixels;
  real_T c9_b_j;
  c9_emxArray_real_T *c9_r17;
  int32_T c9_i714;
  int32_T c9_i715;
  int32_T c9_i;
  real_T c9_b_i;
  int32_T c9_i716;
  int32_T c9_i717;
  int32_T c9_e_loop_ub;
  int32_T c9_i718;
  int32_T c9_i719;
  real_T c9_c_k;
  int32_T c9_i720;
  int32_T c9_c_j;
  c9_emxArray_int32_T *c9_point1Array;
  c9_emxArray_int32_T *c9_point2Array;
  int32_T c9_i721;
  c9_emxArray_real32_T *c9_thetaArray;
  int32_T c9_c_i;
  c9_emxArray_real32_T *c9_rhoArray;
  int32_T c9_numLines;
  int32_T c9_i722;
  int32_T c9_i723;
  int32_T c9_i724;
  int32_T c9_i725;
  int32_T c9_i726;
  int32_T c9_i727;
  int32_T c9_i728;
  int32_T c9_i729;
  real_T c9_firstRho;
  real_T c9_numRho;
  real_T c9_lastRho;
  real_T c9_slope;
  real_T c9_numPeaks;
  int32_T c9_i730;
  int32_T c9_peakIdx;
  c9_emxArray_real_T *c9_indices;
  c9_emxArray_int32_T *c9_houghPix;
  c9_emxArray_int32_T *c9_rhoBinIdx;
  c9_emxArray_int32_T *c9_b_houghPix;
  c9_emxArray_real_T *c9_distances2;
  c9_emxArray_int32_T *c9_b_point1Array;
  c9_emxArray_int32_T *c9_b_point2Array;
  c9_emxArray_real32_T *c9_b_thetaArray;
  c9_emxArray_real32_T *c9_b_rhoArray;
  real_T c9_b_peakIdx;
  int32_T c9_peak1;
  int32_T c9_peak2;
  real_T c9_b_firstRho;
  real_T c9_b_slope;
  int32_T c9_b_peak1;
  int32_T c9_b_peak2;
  real_T c9_b_numNonZero;
  int32_T c9_i731;
  int32_T c9_f_loop_ub;
  c9_emxArray_boolean_T *c9_tile;
  int32_T c9_i732;
  int32_T c9_b_numLines;
  int32_T c9_c_numLines;
  int32_T c9_c_varargin_2;
  int32_T c9_i733;
  int32_T c9_i734;
  int32_T c9_numHoughPix;
  real_T c9_thetaVal;
  int32_T c9_i735;
  real_T c9_b_x;
  int32_T c9_i736;
  real_T c9_cosTheta;
  int32_T c9_g_loop_ub;
  int32_T c9_i737;
  real_T c9_c_x;
  real_T c9_sinTheta;
  int32_T c9_outsize[2];
  int32_T c9_i738;
  int32_T c9_d_k;
  const mxArray *c9_x_y = NULL;
  int32_T c9_i739;
  real_T c9_e_k;
  int32_T c9_i740;
  int32_T c9_i741;
  real_T c9_rhoVal;
  real_T c9_d_x;
  int32_T c9_i742;
  c9_skoeQIuVNKJRHNtBIlOCZhD c9_s;
  int32_T c9_y_y;
  int32_T c9_i743;
  int32_T c9_i744;
  int32_T c9_i745;
  int32_T c9_i746;
  int32_T c9_h_loop_ub;
  int32_T c9_i747;
  int32_T c9_i748;
  real_T c9_b_numHoughPix;
  int32_T c9_i749;
  int32_T c9_i750;
  int32_T c9_i751;
  real_T c9_numPairs;
  int32_T c9_i752;
  real_T c9_d33;
  int32_T c9_i_loop_ub;
  int32_T c9_i753;
  int32_T c9_i754;
  real_T c9_n;
  int32_T c9_f_k;
  int32_T c9_i755;
  int32_T c9_g_k;
  int32_T c9_b;
  int32_T c9_i756;
  real_T c9_h_k;
  int32_T c9_b_b;
  real_T c9_rowMax;
  boolean_T c9_overflow;
  real_T c9_rowMin;
  real_T c9_colMax;
  real_T c9_colMin;
  real_T c9_b_n;
  int32_T c9_a;
  int32_T c9_c_b;
  real_T c9_d34;
  int32_T c9_b_a;
  int32_T c9_i_k;
  int32_T c9_d_b;
  int32_T c9_i757;
  int32_T c9_c_a;
  int32_T c9_j_k;
  int32_T c9_e_x;
  boolean_T c9_b_overflow;
  int32_T c9_d_a;
  int32_T c9_k_k;
  int32_T c9_c;
  int32_T c9_i758;
  real_T c9_d35;
  int32_T c9_bu;
  int32_T c9_iv8[1];
  int32_T c9_i759;
  int32_T c9_i760;
  int32_T c9_l_k;
  int32_T c9_m_k;
  int32_T c9_n_k;
  int32_T c9_i761;
  real_T c9_rowRange;
  int32_T c9_o_k;
  real_T c9_p_k;
  real_T c9_colRange;
  real_T c9_r;
  int32_T c9_i762;
  int32_T c9_e_a;
  int32_T c9_i763;
  int32_T c9_b_indices;
  int32_T c9_f_a;
  int32_T c9_iv9[1];
  int32_T c9_i764;
  int32_T c9_i765;
  int32_T c9_point1_size[2];
  int32_T c9_g_a;
  int32_T c9_i766;
  int32_T c9_f_x;
  int32_T c9_q_k;
  int32_T c9_j_loop_ub;
  int32_T c9_h_a;
  int32_T c9_i767;
  real_T c9_sortingOrder[2];
  real_T c9_b_c;
  int32_T c9_i768;
  int32_T c9_c_c;
  int32_T c9_b_bu;
  int32_T c9_i769;
  int32_T c9_i770;
  int32_T c9_point1_data[2];
  int32_T c9_c_indices;
  int32_T c9_point2_size[2];
  int32_T c9_i771;
  int32_T c9_i772;
  int32_T c9_k_loop_ub;
  int32_T c9_l_loop_ub;
  int32_T c9_i773;
  int32_T c9_i774;
  int32_T c9_point2_data[2];
  int32_T c9_i_a;
  int32_T c9_j_a;
  int32_T c9_k_a;
  int32_T c9_g_x;
  int32_T c9_l_a;
  int32_T c9_d_c;
  int32_T c9_c_bu;
  int32_T c9_m_a;
  int32_T c9_n_a;
  int32_T c9_o_a;
  int32_T c9_h_x;
  int32_T c9_p_a;
  int32_T c9_e_c;
  int32_T c9_d_bu;
  int32_T c9_lineLength2;
  int32_T c9_point1[1];
  int32_T c9_b_point1[1];
  int32_T c9_i775;
  int32_T c9_i776;
  int32_T c9_m_loop_ub;
  int32_T c9_i777;
  int32_T c9_i778;
  int32_T c9_i779;
  int32_T c9_i780;
  int32_T c9_n_loop_ub;
  int32_T c9_i781;
  int32_T c9_point2[1];
  int32_T c9_b_point2[1];
  int32_T c9_i782;
  int32_T c9_i783;
  int32_T c9_o_loop_ub;
  int32_T c9_i784;
  int32_T c9_i785;
  int32_T c9_i786;
  int32_T c9_i787;
  int32_T c9_p_loop_ub;
  int32_T c9_i788;
  int32_T c9_d_varargin_2[1];
  int32_T c9_i789;
  int32_T c9_q_loop_ub;
  int32_T c9_i790;
  int32_T c9_i791;
  int32_T c9_r_loop_ub;
  int32_T c9_i792;
  int32_T c9_c_varargin_3[1];
  int32_T c9_i793;
  int32_T c9_s_loop_ub;
  int32_T c9_i794;
  int32_T c9_i795;
  int32_T c9_t_loop_ub;
  int32_T c9_i796;
  boolean_T exitg1;
  int32_T exitg2;
  c9_b68 = (c9_varargin_1->size[0] == 0);
  c9_b69 = (c9_varargin_1->size[1] == 0);
  if ((!c9_b68) && (!c9_b69)) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv57, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv13, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv14, 10, 0U, 1U, 0U, 2, 1, 19),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c9_b_y, 14, c9_c_y)));
  }

  c9_emxInit_real_T(chartInstance, &c9_b_varargin_2, 2, &c9_bh_emlrtRTEI);
  c9_i696 = c9_b_varargin_2->size[0] * c9_b_varargin_2->size[1];
  c9_b_varargin_2->size[0] = 1;
  c9_b_varargin_2->size[1] = c9_varargin_2->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_2, c9_i696,
    &c9_bh_emlrtRTEI);
  c9_i697 = c9_b_varargin_2->size[0];
  c9_i698 = c9_b_varargin_2->size[1];
  c9_loop_ub = c9_varargin_2->size[0] * c9_varargin_2->size[1] - 1;
  for (c9_i699 = 0; c9_i699 <= c9_loop_ub; c9_i699++) {
    c9_b_varargin_2->data[c9_i699] = c9_varargin_2->data[c9_i699];
  }

  if (c9_b_all(chartInstance, c9_b_varargin_2)) {
    c9_b70 = true;
  } else {
    c9_b70 = false;
  }

  c9_emxFree_real_T(chartInstance, &c9_b_varargin_2);
  if (c9_b70) {
  } else {
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv58, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv60, 10, 0U, 1U, 0U, 2, 1, 22),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_e_y, 14, c9_g_y)));
  }

  c9_b71 = (c9_varargin_2->size[1] == 0);
  if (!c9_b71) {
  } else {
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv59, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv13, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_i_y = NULL;
    sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv61, 10, 0U, 1U, 0U, 2, 1, 22),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_f_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_h_y, 14, c9_i_y)));
  }

  c9_emxInit_real_T(chartInstance, &c9_b_varargin_3, 2, &c9_bh_emlrtRTEI);
  c9_i700 = c9_b_varargin_3->size[0] * c9_b_varargin_3->size[1];
  c9_b_varargin_3->size[0] = 1;
  c9_b_varargin_3->size[1] = c9_varargin_3->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_3, c9_i700,
    &c9_bh_emlrtRTEI);
  c9_i701 = c9_b_varargin_3->size[0];
  c9_i702 = c9_b_varargin_3->size[1];
  c9_b_loop_ub = c9_varargin_3->size[0] * c9_varargin_3->size[1] - 1;
  for (c9_i703 = 0; c9_i703 <= c9_b_loop_ub; c9_i703++) {
    c9_b_varargin_3->data[c9_i703] = c9_varargin_3->data[c9_i703];
  }

  if (c9_b_all(chartInstance, c9_b_varargin_3)) {
    c9_b72 = true;
  } else {
    c9_b72 = false;
  }

  c9_emxFree_real_T(chartInstance, &c9_b_varargin_3);
  if (c9_b72) {
  } else {
    c9_j_y = NULL;
    sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv62, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_k_y = NULL;
    sf_mex_assign(&c9_k_y, sf_mex_create("y", c9_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_m_y = NULL;
    sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv64, 10, 0U, 1U, 0U, 2, 1, 20),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_k_y, 14, c9_m_y)));
  }

  c9_b73 = (c9_varargin_3->size[1] == 0);
  if (!c9_b73) {
  } else {
    c9_l_y = NULL;
    sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv63, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_n_y = NULL;
    sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv13, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_o_y = NULL;
    sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv65, 10, 0U, 1U, 0U, 2, 1, 20),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_l_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_n_y, 14, c9_o_y)));
  }

  c9_p = true;
  c9_d32 = (real_T)(c9_varargin_4->size[0] * c9_varargin_4->size[1]);
  c9_i704 = (int32_T)c9_d32 - 1;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k <= c9_i704)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_varargin_4->data[(int32_T)c9_b_k - 1];
    c9_b_p = !(c9_x <= 0.0);
    if (c9_b_p) {
      c9_k++;
    } else {
      c9_p = false;
      exitg1 = true;
    }
  }

  if (c9_p) {
    c9_b74 = true;
  } else {
    c9_b74 = false;
  }

  if (c9_b74) {
  } else {
    c9_p_y = NULL;
    sf_mex_assign(&c9_p_y, sf_mex_create("y", c9_cv66, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c9_q_y = NULL;
    sf_mex_assign(&c9_q_y, sf_mex_create("y", c9_cv12, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_r_y = NULL;
    sf_mex_assign(&c9_r_y, sf_mex_create("y", c9_cv67, 10, 0U, 1U, 0U, 2, 1, 22),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_p_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_q_y, 14, c9_r_y)));
  }

  c9_emxInit_real_T(chartInstance, &c9_b_varargin_4, 2, &c9_ah_emlrtRTEI);
  c9_i705 = c9_b_varargin_4->size[0] * c9_b_varargin_4->size[1];
  c9_b_varargin_4->size[0] = c9_varargin_4->size[0];
  c9_b_varargin_4->size[1] = c9_varargin_4->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_4, c9_i705,
    &c9_ah_emlrtRTEI);
  c9_i706 = c9_b_varargin_4->size[0];
  c9_i707 = c9_b_varargin_4->size[1];
  c9_c_loop_ub = c9_varargin_4->size[0] * c9_varargin_4->size[1] - 1;
  for (c9_i708 = 0; c9_i708 <= c9_c_loop_ub; c9_i708++) {
    c9_b_varargin_4->data[c9_i708] = c9_varargin_4->data[c9_i708];
  }

  if (c9_c_all(chartInstance, c9_b_varargin_4)) {
    c9_b75 = true;
  } else {
    c9_b75 = false;
  }

  c9_emxFree_real_T(chartInstance, &c9_b_varargin_4);
  if (c9_b75) {
  } else {
    c9_s_y = NULL;
    sf_mex_assign(&c9_s_y, sf_mex_create("y", c9_cv68, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c9_t_y = NULL;
    sf_mex_assign(&c9_t_y, sf_mex_create("y", c9_cv7, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c9_v_y = NULL;
    sf_mex_assign(&c9_v_y, sf_mex_create("y", c9_cv70, 10, 0U, 1U, 0U, 2, 1, 22),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_s_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_t_y, 14, c9_v_y)));
  }

  if (!((real_T)c9_varargin_4->size[1] != 2.0)) {
  } else {
    c9_u_y = NULL;
    sf_mex_assign(&c9_u_y, sf_mex_create("y", c9_cv69, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_w_y = NULL;
    sf_mex_assign(&c9_w_y, sf_mex_create("y", c9_cv69, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_u_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_w_y)));
  }

  c9_emxInit_real_T(chartInstance, &c9_peaks, 2, &c9_bi_emlrtRTEI);
  c9_i709 = c9_peaks->size[0] * c9_peaks->size[1];
  c9_peaks->size[0] = c9_varargin_4->size[0];
  c9_peaks->size[1] = c9_varargin_4->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_peaks, c9_i709, &c9_ch_emlrtRTEI);
  c9_i710 = c9_peaks->size[0];
  c9_i711 = c9_peaks->size[1];
  c9_d_loop_ub = c9_varargin_4->size[0] * c9_varargin_4->size[1] - 1;
  for (c9_i712 = 0; c9_i712 <= c9_d_loop_ub; c9_i712++) {
    c9_peaks->data[c9_i712] = c9_varargin_4->data[c9_i712];
  }

  c9_numCol = (real_T)c9_varargin_1->size[1];
  c9_numRow = (real_T)c9_varargin_1->size[0];
  c9_numNonZero = 0;
  c9_i713 = (int32_T)c9_numCol - 1;
  for (c9_j = 0; c9_j <= c9_i713; c9_j++) {
    c9_b_j = 1.0 + (real_T)c9_j;
    c9_i714 = (int32_T)c9_numRow - 1;
    for (c9_i = 0; c9_i <= c9_i714; c9_i++) {
      c9_b_i = 1.0 + (real_T)c9_i;
      c9_numNonZero += ((real_T)c9_varargin_1->data[(sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c9_b_i, 1, c9_varargin_1->size[0]) + c9_varargin_1->size[0] *
        (sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S,
        1U, 0, 0, MAX_uint32_T, (int32_T)c9_b_j, 1, c9_varargin_1->size[1]) - 1))
                        - 1] > 0.0);
    }
  }

  c9_emxInit_int32_T1(chartInstance, &c9_nonZeroPixels, 2, &c9_eh_emlrtRTEI);
  c9_emxInit_real_T(chartInstance, &c9_r17, 2, &c9_dh_emlrtRTEI);
  c9_i715 = c9_r17->size[0] * c9_r17->size[1];
  c9_r17->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_numNonZero);
  c9_r17->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_r17, c9_i715, &c9_dh_emlrtRTEI);
  c9_i716 = c9_r17->size[0];
  c9_i717 = c9_r17->size[1];
  c9_e_loop_ub = ((int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_numNonZero) <<
                  1) - 1;
  for (c9_i718 = 0; c9_i718 <= c9_e_loop_ub; c9_i718++) {
    c9_r17->data[c9_i718] = 0.0;
  }

  c9_i719 = c9_nonZeroPixels->size[0] * c9_nonZeroPixels->size[1];
  c9_nonZeroPixels->size[0] = c9_r17->size[0];
  c9_nonZeroPixels->size[1] = c9_r17->size[1];
  c9_emxEnsureCapacity_int32_T1(chartInstance, c9_nonZeroPixels, c9_i719,
    &c9_eh_emlrtRTEI);
  c9_c_k = 0.0;
  c9_i720 = (int32_T)c9_numCol - 1;
  for (c9_c_j = 0; c9_c_j <= c9_i720; c9_c_j++) {
    c9_b_j = 1.0 + (real_T)c9_c_j;
    c9_i721 = (int32_T)c9_numRow - 1;
    for (c9_c_i = 0; c9_c_i <= c9_i721; c9_c_i++) {
      c9_b_i = 1.0 + (real_T)c9_c_i;
      if ((real_T)c9_varargin_1->data[(sf_eml_array_bounds_check
           (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
            MAX_uint32_T, (int32_T)c9_b_i, 1, c9_varargin_1->size[0]) +
           c9_varargin_1->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_b_j, 1, c9_varargin_1->size[1]) - 1)) - 1]
          > 0.0) {
        c9_c_k++;
        c9_nonZeroPixels->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_c_k, 1, c9_nonZeroPixels->size[0]) - 1] = (int32_T)
          (c9_b_j - 1.0);
        c9_nonZeroPixels->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_c_k, 1, c9_nonZeroPixels->size[0]) +
          c9_nonZeroPixels->size[0]) - 1] = (int32_T)(c9_b_i - 1.0);
      }
    }
  }

  c9_emxInit_int32_T1(chartInstance, &c9_point1Array, 2, &c9_vh_emlrtRTEI);
  c9_emxInit_int32_T1(chartInstance, &c9_point2Array, 2, &c9_wh_emlrtRTEI);
  c9_emxInit_real32_T1(chartInstance, &c9_thetaArray, 1, &c9_xh_emlrtRTEI);
  c9_emxInit_real32_T1(chartInstance, &c9_rhoArray, 1, &c9_yh_emlrtRTEI);
  c9_numLines = 0;
  c9_i722 = c9_point1Array->size[0] * c9_point1Array->size[1];
  c9_point1Array->size[0] = 0;
  c9_point1Array->size[1] = 2;
  c9_i723 = c9_point1Array->size[0];
  c9_i724 = c9_point1Array->size[1];
  c9_i725 = c9_point2Array->size[0] * c9_point2Array->size[1];
  c9_point2Array->size[0] = 0;
  c9_point2Array->size[1] = 2;
  c9_i726 = c9_point2Array->size[0];
  c9_i727 = c9_point2Array->size[1];
  c9_i728 = c9_thetaArray->size[0];
  c9_thetaArray->size[0] = 0;
  c9_i729 = c9_rhoArray->size[0];
  c9_rhoArray->size[0] = 0;
  (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
    chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_varargin_3->size[1]);
  c9_firstRho = c9_varargin_3->data[0];
  c9_numRho = (real_T)c9_varargin_3->size[1];
  c9_lastRho = c9_varargin_3->data[sf_eml_array_bounds_check
    (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
     (int32_T)c9_numRho, 1, c9_varargin_3->size[1]) - 1];
  c9_slope = (c9_numRho - 1.0) / (c9_lastRho - c9_firstRho);
  c9_numPeaks = (real_T)c9_peaks->size[0];
  c9_i730 = (int32_T)c9_numPeaks - 1;
  c9_peakIdx = 0;
  c9_emxInit_real_T1(chartInstance, &c9_indices, 1, &c9_ai_emlrtRTEI);
  c9_emxInit_int32_T1(chartInstance, &c9_houghPix, 2, &c9_ci_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_rhoBinIdx, 1, &c9_di_emlrtRTEI);
  c9_emxInit_int32_T1(chartInstance, &c9_b_houghPix, 2, &c9_ei_emlrtRTEI);
  c9_emxInit_real_T1(chartInstance, &c9_distances2, 1, &c9_fi_emlrtRTEI);
  c9_emxInit_int32_T1(chartInstance, &c9_b_point1Array, 2, &c9_nh_emlrtRTEI);
  c9_emxInit_int32_T1(chartInstance, &c9_b_point2Array, 2, &c9_ph_emlrtRTEI);
  c9_emxInit_real32_T1(chartInstance, &c9_b_thetaArray, 1, &c9_rh_emlrtRTEI);
  c9_emxInit_real32_T1(chartInstance, &c9_b_rhoArray, 1, &c9_th_emlrtRTEI);
  while (c9_peakIdx <= c9_i730) {
    c9_b_peakIdx = 1.0 + (real_T)c9_peakIdx;
    c9_peak1 = (int32_T)c9_peaks->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       (int32_T)c9_b_peakIdx, 1, c9_peaks->size[0]) - 1];
    c9_peak2 = (int32_T)c9_peaks->data[(sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       (int32_T)c9_b_peakIdx, 1, c9_peaks->size[0]) + c9_peaks->size[0]) - 1];
    c9_b_firstRho = c9_firstRho;
    c9_b_slope = c9_slope;
    c9_b_peak1 = c9_peak1;
    c9_b_peak2 = c9_peak2;
    c9_b_numNonZero = (real_T)c9_nonZeroPixels->size[0];
    c9_i731 = c9_distances2->size[0];
    c9_distances2->size[0] = (int32_T)c9_b_numNonZero;
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_distances2, c9_i731,
      &c9_fh_emlrtRTEI);
    c9_f_loop_ub = (int32_T)c9_b_numNonZero - 1;
    for (c9_i732 = 0; c9_i732 <= c9_f_loop_ub; c9_i732++) {
      c9_distances2->data[c9_i732] = 0.0;
    }

    c9_i733 = c9_rhoBinIdx->size[0];
    c9_rhoBinIdx->size[0] = c9_distances2->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_rhoBinIdx, c9_i733,
      &c9_gh_emlrtRTEI);
    c9_numHoughPix = 0;
    c9_thetaVal = c9_varargin_2->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       c9_b_peak2, 1, c9_varargin_2->size[1]) - 1] * 3.1415926535897931 / 180.0;
    c9_b_x = c9_thetaVal;
    c9_cosTheta = c9_b_x;
    c9_cosTheta = muDoubleScalarCos(c9_cosTheta);
    c9_c_x = c9_thetaVal;
    c9_sinTheta = c9_c_x;
    c9_sinTheta = muDoubleScalarSin(c9_sinTheta);
    c9_i738 = (int32_T)c9_b_numNonZero - 1;
    for (c9_d_k = 0; c9_d_k <= c9_i738; c9_d_k++) {
      c9_e_k = 1.0 + (real_T)c9_d_k;
      c9_rhoVal = (real_T)c9_nonZeroPixels->data[sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c9_e_k, 1, c9_nonZeroPixels->size[0]) - 1] * c9_cosTheta +
        (real_T)c9_nonZeroPixels->data[(sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c9_e_k, 1, c9_nonZeroPixels->size[0]) + c9_nonZeroPixels->
        size[0]) - 1] * c9_sinTheta;
      c9_d_x = c9_b_slope * (c9_rhoVal - c9_b_firstRho) + 1.0;
      c9_y_y = (int32_T)(c9_d_x + 0.5);
      c9_rhoBinIdx->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_e_k, 1,
        c9_rhoBinIdx->size[0]) - 1] = c9_y_y;
      if (c9_rhoBinIdx->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_e_k, 1, c9_rhoBinIdx->size[0]) - 1] == c9_b_peak1) {
        c9_numHoughPix++;
      }
    }

    if (c9_numHoughPix < 1) {
      c9_i741 = c9_houghPix->size[0] * c9_houghPix->size[1];
      c9_houghPix->size[0] = 0;
      c9_houghPix->size[1] = 0;
      c9_i743 = c9_houghPix->size[0];
      c9_i745 = c9_houghPix->size[1];
    } else {
      c9_i740 = c9_r17->size[0] * c9_r17->size[1];
      c9_r17->size[0] = c9_numHoughPix;
      c9_r17->size[1] = 2;
      c9_emxEnsureCapacity_real_T(chartInstance, c9_r17, c9_i740,
        &c9_ih_emlrtRTEI);
      c9_i744 = c9_r17->size[0];
      c9_i746 = c9_r17->size[1];
      c9_h_loop_ub = (c9_numHoughPix << 1) - 1;
      for (c9_i748 = 0; c9_i748 <= c9_h_loop_ub; c9_i748++) {
        c9_r17->data[c9_i748] = 0.0;
      }

      c9_i750 = c9_b_houghPix->size[0] * c9_b_houghPix->size[1];
      c9_b_houghPix->size[0] = c9_r17->size[0];
      c9_b_houghPix->size[1] = c9_r17->size[1];
      c9_emxEnsureCapacity_int32_T1(chartInstance, c9_b_houghPix, c9_i750,
        &c9_gh_emlrtRTEI);
      c9_n = 0.0;
      c9_i755 = (int32_T)c9_b_numNonZero - 1;
      for (c9_g_k = 0; c9_g_k <= c9_i755; c9_g_k++) {
        c9_e_k = 1.0 + (real_T)c9_g_k;
        if (c9_rhoBinIdx->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_e_k, 1, c9_rhoBinIdx->size[0]) - 1] ==
            c9_b_peak1) {
          c9_n++;
          c9_b_houghPix->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_n, 1, c9_b_houghPix->size[0]) - 1] =
            c9_nonZeroPixels->data[(sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_e_k, 1, c9_nonZeroPixels->size[0]) +
            c9_nonZeroPixels->size[0]) - 1] + 1;
          c9_b_houghPix->data[(sf_eml_array_bounds_check
                               (sfGlobalDebugInstanceStruct, chartInstance->S,
                                1U, 0, 0, MAX_uint32_T, (int32_T)c9_n, 1,
                                c9_b_houghPix->size[0]) + c9_b_houghPix->size[0])
            - 1] = c9_nonZeroPixels->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_e_k, 1, c9_nonZeroPixels->size[0]) - 1] +
            1;
        }
      }

      c9_rowMax = 0.0;
      c9_rowMin = rtInf;
      c9_colMax = 0.0;
      c9_colMin = rtInf;
      c9_c_b = c9_numHoughPix;
      c9_d_b = c9_c_b;
      if (1 > c9_d_b) {
        c9_b_overflow = false;
      } else {
        c9_b_overflow = (c9_d_b > 2147483646);
      }

      if (c9_b_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }

      for (c9_l_k = 1; c9_l_k - 1 < c9_numHoughPix; c9_l_k++) {
        c9_o_k = c9_l_k;
        c9_r = (real_T)c9_b_houghPix->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c9_o_k, 1, c9_b_houghPix->size[0]) - 1];
        if (c9_r > c9_rowMax) {
          c9_rowMax = c9_r;
        }

        if (c9_rowMin > c9_r) {
          c9_rowMin = c9_r;
        }

        c9_b_c = (real_T)c9_b_houghPix->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c9_o_k, 1, c9_b_houghPix->size[0]) + c9_b_houghPix->size[0]) - 1];
        if (c9_b_c > c9_colMax) {
          c9_colMax = c9_b_c;
        }

        if (c9_colMin > c9_b_c) {
          c9_colMin = c9_b_c;
        }
      }

      c9_rowRange = c9_rowMax - c9_rowMin;
      c9_colRange = c9_colMax - c9_colMin;
      if (c9_rowRange > c9_colRange) {
        for (c9_i765 = 0; c9_i765 < 2; c9_i765++) {
          c9_sortingOrder[c9_i765] = 1.0 + (real_T)c9_i765;
        }
      } else {
        for (c9_i764 = 0; c9_i764 < 2; c9_i764++) {
          c9_sortingOrder[c9_i764] = 2.0 - (real_T)c9_i764;
        }
      }

      c9_b_sortrows(chartInstance, c9_b_houghPix, c9_sortingOrder);
      c9_i769 = c9_houghPix->size[0] * c9_houghPix->size[1];
      c9_houghPix->size[0] = c9_b_houghPix->size[0];
      c9_houghPix->size[1] = 2;
      c9_emxEnsureCapacity_int32_T1(chartInstance, c9_houghPix, c9_i769,
        &c9_lh_emlrtRTEI);
      c9_i771 = c9_houghPix->size[0];
      c9_i772 = c9_houghPix->size[1];
      c9_l_loop_ub = c9_b_houghPix->size[0] * c9_b_houghPix->size[1] - 1;
      for (c9_i774 = 0; c9_i774 <= c9_l_loop_ub; c9_i774++) {
        c9_houghPix->data[c9_i774] = c9_b_houghPix->data[c9_i774];
      }
    }

    if (c9_numHoughPix < 1) {
    } else {
      c9_b_numHoughPix = (real_T)c9_houghPix->size[0];
      c9_i749 = c9_distances2->size[0];
      c9_distances2->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("",
        c9_b_numHoughPix - 1.0);
      c9_emxEnsureCapacity_real_T1(chartInstance, c9_distances2, c9_i749,
        &c9_jh_emlrtRTEI);
      c9_numPairs = 0.0;
      c9_d33 = c9_b_numHoughPix - 1.0;
      c9_i753 = (int32_T)c9_d33 - 1;
      for (c9_f_k = 0; c9_f_k <= c9_i753; c9_f_k++) {
        c9_h_k = 1.0 + (real_T)c9_f_k;
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_houghPix->size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_houghPix->size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c9_houghPix->size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c9_houghPix->size[1]);
        c9_a = c9_houghPix->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)(c9_h_k + 1.0), 1, c9_houghPix->size[0]) - 1] -
          c9_houghPix->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_h_k, 1, c9_houghPix->size[0]) - 1];
        c9_b_a = c9_a;
        c9_c_a = c9_b_a;
        c9_e_x = c9_c_a;
        c9_d_a = c9_e_x;
        c9_c = 1;
        c9_bu = 2;
        do {
          exitg2 = 0;
          if ((c9_bu & 1) != 0) {
            c9_c *= c9_d_a;
          }

          c9_bu >>= 1;
          if (c9_bu == 0) {
            exitg2 = 1;
          } else {
            c9_d_a *= c9_d_a;
          }
        } while (exitg2 == 0);

        c9_e_a = c9_houghPix->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)(c9_h_k + 1.0), 1, c9_houghPix->size[0]) + c9_houghPix->
          size[0]) - 1] - c9_houghPix->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_h_k, 1, c9_houghPix->size[0]) + c9_houghPix->size[0]) - 1];
        c9_f_a = c9_e_a;
        c9_g_a = c9_f_a;
        c9_f_x = c9_g_a;
        c9_h_a = c9_f_x;
        c9_c_c = 1;
        c9_b_bu = 2;
        do {
          exitg2 = 0;
          if ((c9_b_bu & 1) != 0) {
            c9_c_c *= c9_h_a;
          }

          c9_b_bu >>= 1;
          if (c9_b_bu == 0) {
            exitg2 = 1;
          } else {
            c9_h_a *= c9_h_a;
          }
        } while (exitg2 == 0);

        c9_distances2->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_h_k, 1, c9_distances2->size[0]) - 1] = (real_T)(c9_c +
          c9_c_c);
        if (c9_distances2->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_h_k, 1, c9_distances2->size[0]) - 1] >
            25.0) {
          c9_numPairs++;
        }
      }

      c9_i756 = c9_indices->size[0];
      c9_indices->size[0] = (int32_T)sf_integer_check(chartInstance->S, 1U, 0U,
        0U, c9_numPairs + 2.0);
      c9_emxEnsureCapacity_real_T1(chartInstance, c9_indices, c9_i756,
        &c9_jh_emlrtRTEI);
      c9_indices->data[0] = 0.0;
      c9_indices->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_indices->size[0], 1,
        c9_indices->size[0]) - 1] = c9_b_numHoughPix;
      c9_b_n = 1.0;
      c9_d34 = c9_b_numHoughPix - 1.0;
      c9_i757 = (int32_T)c9_d34 - 1;
      for (c9_j_k = 0; c9_j_k <= c9_i757; c9_j_k++) {
        c9_h_k = 1.0 + (real_T)c9_j_k;
        if (c9_distances2->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c9_h_k, 1, c9_distances2->size[0]) - 1] >
            25.0) {
          c9_b_n++;
          c9_indices->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_b_n, 1,
            c9_indices->size[0]) - 1] = c9_h_k;
        }
      }

      c9_d35 = (real_T)c9_indices->size[0] - 1.0;
      c9_i759 = (int32_T)c9_d35 - 1;
      for (c9_m_k = 0; c9_m_k <= c9_i759; c9_m_k++) {
        c9_p_k = 1.0 + (real_T)c9_m_k;
        c9_i762 = c9_houghPix->size[1];
        c9_b_indices = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)(c9_indices->
          data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_p_k, 1,
          c9_indices->size[0]) - 1] + 1.0), 1, c9_houghPix->size[0]) - 1;
        c9_point1_size[0] = 1;
        c9_point1_size[1] = c9_i762;
        c9_j_loop_ub = c9_i762 - 1;
        for (c9_i768 = 0; c9_i768 <= c9_j_loop_ub; c9_i768++) {
          c9_point1_data[c9_i768] = c9_houghPix->data[c9_b_indices +
            c9_houghPix->size[0] * c9_i768];
        }

        c9_i770 = c9_houghPix->size[1];
        c9_c_indices = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c9_indices->
          data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)(c9_p_k + 1.0), 1,
          c9_indices->size[0]) - 1], 1, c9_houghPix->size[0]) - 1;
        c9_point2_size[0] = 1;
        c9_point2_size[1] = c9_i770;
        c9_k_loop_ub = c9_i770 - 1;
        for (c9_i773 = 0; c9_i773 <= c9_k_loop_ub; c9_i773++) {
          c9_point2_data[c9_i773] = c9_houghPix->data[c9_c_indices +
            c9_houghPix->size[0] * c9_i773];
        }

        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_point2_size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_point1_size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c9_point2_size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c9_point1_size[1]);
        c9_i_a = c9_point2_data[0] - c9_point1_data[0];
        c9_j_a = c9_i_a;
        c9_k_a = c9_j_a;
        c9_g_x = c9_k_a;
        c9_l_a = c9_g_x;
        c9_d_c = 1;
        c9_c_bu = 2;
        do {
          exitg2 = 0;
          if ((c9_c_bu & 1) != 0) {
            c9_d_c *= c9_l_a;
          }

          c9_c_bu >>= 1;
          if (c9_c_bu == 0) {
            exitg2 = 1;
          } else {
            c9_l_a *= c9_l_a;
          }
        } while (exitg2 == 0);

        c9_m_a = c9_point2_data[1] - c9_point1_data[1];
        c9_n_a = c9_m_a;
        c9_o_a = c9_n_a;
        c9_h_x = c9_o_a;
        c9_p_a = c9_h_x;
        c9_e_c = 1;
        c9_d_bu = 2;
        do {
          exitg2 = 0;
          if ((c9_d_bu & 1) != 0) {
            c9_e_c *= c9_p_a;
          }

          c9_d_bu >>= 1;
          if (c9_d_bu == 0) {
            exitg2 = 1;
          } else {
            c9_p_a *= c9_p_a;
          }
        } while (exitg2 == 0);

        c9_lineLength2 = c9_d_c + c9_e_c;
        if (c9_lineLength2 >= 49) {
          c9_numLines++;
          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c9_point1_size[1]);
          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_point1_size[1]);
          c9_point1[0] = c9_point1_size[1];
          c9_b_point1[0] = c9_point1_size[1];
          c9_i775 = c9_b_point1Array->size[0] * c9_b_point1Array->size[1];
          c9_b_point1Array->size[0] = c9_point1Array->size[0] + 1;
          c9_b_point1Array->size[1] = 2;
          c9_emxEnsureCapacity_int32_T1(chartInstance, c9_b_point1Array, c9_i775,
            &c9_nh_emlrtRTEI);
          for (c9_i776 = 0; c9_i776 < 2; c9_i776++) {
            c9_m_loop_ub = c9_point1Array->size[0] - 1;
            for (c9_i777 = 0; c9_i777 <= c9_m_loop_ub; c9_i777++) {
              c9_b_point1Array->data[c9_i777 + c9_b_point1Array->size[0] *
                c9_i776] = c9_point1Array->data[c9_i777 + c9_point1Array->size[0]
                * c9_i776];
            }
          }

          c9_b_point1Array->data[c9_point1Array->size[0]] = c9_point1_data[1];
          c9_b_point1Array->data[c9_point1Array->size[0] +
            c9_b_point1Array->size[0]] = c9_point1_data[0];
          c9_i778 = c9_point1Array->size[0] * c9_point1Array->size[1];
          c9_point1Array->size[0] = c9_b_point1Array->size[0];
          c9_point1Array->size[1] = 2;
          c9_emxEnsureCapacity_int32_T1(chartInstance, c9_point1Array, c9_i778,
            &c9_oh_emlrtRTEI);
          c9_i779 = c9_point1Array->size[0];
          c9_i780 = c9_point1Array->size[1];
          c9_n_loop_ub = c9_b_point1Array->size[0] * c9_b_point1Array->size[1] -
            1;
          for (c9_i781 = 0; c9_i781 <= c9_n_loop_ub; c9_i781++) {
            c9_point1Array->data[c9_i781] = c9_b_point1Array->data[c9_i781];
          }

          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c9_point2_size[1]);
          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c9_point2_size[1]);
          c9_point2[0] = c9_point2_size[1];
          c9_b_point2[0] = c9_point2_size[1];
          c9_i782 = c9_b_point2Array->size[0] * c9_b_point2Array->size[1];
          c9_b_point2Array->size[0] = c9_point2Array->size[0] + 1;
          c9_b_point2Array->size[1] = 2;
          c9_emxEnsureCapacity_int32_T1(chartInstance, c9_b_point2Array, c9_i782,
            &c9_ph_emlrtRTEI);
          for (c9_i783 = 0; c9_i783 < 2; c9_i783++) {
            c9_o_loop_ub = c9_point2Array->size[0] - 1;
            for (c9_i784 = 0; c9_i784 <= c9_o_loop_ub; c9_i784++) {
              c9_b_point2Array->data[c9_i784 + c9_b_point2Array->size[0] *
                c9_i783] = c9_point2Array->data[c9_i784 + c9_point2Array->size[0]
                * c9_i783];
            }
          }

          c9_b_point2Array->data[c9_point2Array->size[0]] = c9_point2_data[1];
          c9_b_point2Array->data[c9_point2Array->size[0] +
            c9_b_point2Array->size[0]] = c9_point2_data[0];
          c9_i785 = c9_point2Array->size[0] * c9_point2Array->size[1];
          c9_point2Array->size[0] = c9_b_point2Array->size[0];
          c9_point2Array->size[1] = 2;
          c9_emxEnsureCapacity_int32_T1(chartInstance, c9_point2Array, c9_i785,
            &c9_qh_emlrtRTEI);
          c9_i786 = c9_point2Array->size[0];
          c9_i787 = c9_point2Array->size[1];
          c9_p_loop_ub = c9_b_point2Array->size[0] * c9_b_point2Array->size[1] -
            1;
          for (c9_i788 = 0; c9_i788 <= c9_p_loop_ub; c9_i788++) {
            c9_point2Array->data[c9_i788] = c9_b_point2Array->data[c9_i788];
          }

          c9_d_varargin_2[0] = c9_varargin_2->size[1];
          c9_i789 = c9_b_thetaArray->size[0];
          c9_b_thetaArray->size[0] = c9_thetaArray->size[0] + 1;
          c9_emxEnsureCapacity_real32_T1(chartInstance, c9_b_thetaArray, c9_i789,
            &c9_rh_emlrtRTEI);
          c9_q_loop_ub = c9_thetaArray->size[0] - 1;
          for (c9_i790 = 0; c9_i790 <= c9_q_loop_ub; c9_i790++) {
            c9_b_thetaArray->data[c9_i790] = c9_thetaArray->data[c9_i790];
          }

          c9_b_thetaArray->data[c9_thetaArray->size[0]] = (real32_T)
            c9_varargin_2->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c9_peak2, 1, c9_varargin_2->size[1]) - 1];
          c9_i791 = c9_thetaArray->size[0];
          c9_thetaArray->size[0] = c9_b_thetaArray->size[0];
          c9_emxEnsureCapacity_real32_T1(chartInstance, c9_thetaArray, c9_i791,
            &c9_sh_emlrtRTEI);
          c9_r_loop_ub = c9_b_thetaArray->size[0] - 1;
          for (c9_i792 = 0; c9_i792 <= c9_r_loop_ub; c9_i792++) {
            c9_thetaArray->data[c9_i792] = c9_b_thetaArray->data[c9_i792];
          }

          c9_c_varargin_3[0] = c9_varargin_3->size[1];
          c9_i793 = c9_b_rhoArray->size[0];
          c9_b_rhoArray->size[0] = c9_rhoArray->size[0] + 1;
          c9_emxEnsureCapacity_real32_T1(chartInstance, c9_b_rhoArray, c9_i793,
            &c9_th_emlrtRTEI);
          c9_s_loop_ub = c9_rhoArray->size[0] - 1;
          for (c9_i794 = 0; c9_i794 <= c9_s_loop_ub; c9_i794++) {
            c9_b_rhoArray->data[c9_i794] = c9_rhoArray->data[c9_i794];
          }

          c9_b_rhoArray->data[c9_rhoArray->size[0]] = (real32_T)
            c9_varargin_3->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c9_peak1, 1, c9_varargin_3->size[1]) - 1];
          c9_i795 = c9_rhoArray->size[0];
          c9_rhoArray->size[0] = c9_b_rhoArray->size[0];
          c9_emxEnsureCapacity_real32_T1(chartInstance, c9_rhoArray, c9_i795,
            &c9_uh_emlrtRTEI);
          c9_t_loop_ub = c9_b_rhoArray->size[0] - 1;
          for (c9_i796 = 0; c9_i796 <= c9_t_loop_ub; c9_i796++) {
            c9_rhoArray->data[c9_i796] = c9_b_rhoArray->data[c9_i796];
          }
        }
      }
    }

    c9_peakIdx++;
  }

  c9_emxFree_real32_T(chartInstance, &c9_b_rhoArray);
  c9_emxFree_real32_T(chartInstance, &c9_b_thetaArray);
  c9_emxFree_int32_T(chartInstance, &c9_b_point2Array);
  c9_emxFree_int32_T(chartInstance, &c9_b_point1Array);
  c9_emxFree_real_T(chartInstance, &c9_distances2);
  c9_emxFree_int32_T(chartInstance, &c9_b_houghPix);
  c9_emxFree_int32_T(chartInstance, &c9_rhoBinIdx);
  c9_emxFree_real_T(chartInstance, &c9_r17);
  c9_emxFree_int32_T(chartInstance, &c9_houghPix);
  c9_emxFree_real_T(chartInstance, &c9_peaks);
  c9_emxFree_real_T(chartInstance, &c9_indices);
  c9_emxFree_int32_T(chartInstance, &c9_nonZeroPixels);
  c9_emxInit_boolean_T(chartInstance, &c9_tile, 2, &c9_te_emlrtRTEI);
  c9_b_numLines = c9_numLines;
  c9_c_numLines = c9_b_numLines;
  c9_c_varargin_2 = c9_c_numLines;
  c9_i734 = c9_tile->size[0] * c9_tile->size[1];
  c9_tile->size[0] = 1;
  c9_tile->size[1] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)
    c9_c_varargin_2);
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_tile, c9_i734,
    &c9_te_emlrtRTEI);
  c9_i735 = c9_tile->size[0];
  c9_i736 = c9_tile->size[1];
  c9_g_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c9_c_varargin_2) -
    1;
  for (c9_i737 = 0; c9_i737 <= c9_g_loop_ub; c9_i737++) {
    c9_tile->data[c9_i737] = false;
  }

  c9_outsize[1] = c9_tile->size[1];
  if ((real_T)c9_outsize[1] == (real_T)c9_tile->size[1]) {
  } else {
    c9_x_y = NULL;
    sf_mex_assign(&c9_x_y, sf_mex_create("y", c9_cv8, 10, 0U, 1U, 0U, 2, 1, 15),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c9_x_y);
  }

  c9_emxFree_boolean_T(chartInstance, &c9_tile);
  for (c9_i739 = 0; c9_i739 < 2; c9_i739++) {
    c9_s.point1[c9_i739] = 0.0;
  }

  for (c9_i742 = 0; c9_i742 < 2; c9_i742++) {
    c9_s.point2[c9_i742] = 0.0;
  }

  c9_s.theta = 0.0;
  c9_s.rho = 0.0;
  c9_i747 = c9_lines->size[0] * c9_lines->size[1];
  c9_lines->size[0] = 1;
  c9_lines->size[1] = c9_outsize[1];
  c9_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c9_lines, c9_i747,
    &c9_hh_emlrtRTEI);
  c9_i751 = c9_lines->size[0];
  c9_i752 = c9_lines->size[1];
  c9_i_loop_ub = c9_outsize[1] - 1;
  for (c9_i754 = 0; c9_i754 <= c9_i_loop_ub; c9_i754++) {
    c9_lines->data[c9_i754] = c9_s;
  }

  c9_b = c9_b_numLines;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_i_k = 1; c9_i_k - 1 < c9_b_numLines; c9_i_k++) {
    c9_k_k = c9_i_k;
    c9_i758 = c9_lines->size[1];
    c9_iv8[0] = c9_i758;
    c9_i760 = c9_lines->size[1];
    c9_n_k = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_k_k, 1, c9_point1Array->size
      [0]) - 1;
    for (c9_i761 = 0; c9_i761 < 2; c9_i761++) {
      c9_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_k_k, 1, c9_i760) - 1].
        point1[c9_i761] = (real_T)c9_point1Array->data[c9_n_k +
        c9_point1Array->size[0] * c9_i761];
    }

    c9_i763 = c9_lines->size[1];
    c9_iv9[0] = c9_i763;
    c9_i766 = c9_lines->size[1];
    c9_q_k = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_k_k, 1, c9_point2Array->size
      [0]) - 1;
    for (c9_i767 = 0; c9_i767 < 2; c9_i767++) {
      c9_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_k_k, 1, c9_i766) - 1].
        point2[c9_i767] = (real_T)c9_point2Array->data[c9_q_k +
        c9_point2Array->size[0] * c9_i767];
    }

    c9_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_k_k, 1, c9_lines->size[1]) -
      1].theta = c9_thetaArray->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       c9_k_k, 1, c9_thetaArray->size[0]) - 1];
    c9_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c9_k_k, 1, c9_lines->size[1]) -
      1].rho = c9_rhoArray->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       c9_k_k, 1, c9_rhoArray->size[0]) - 1];
  }

  c9_emxFree_real32_T(chartInstance, &c9_rhoArray);
  c9_emxFree_real32_T(chartInstance, &c9_thetaArray);
  c9_emxFree_int32_T(chartInstance, &c9_point2Array);
  c9_emxFree_int32_T(chartInstance, &c9_point1Array);
}

static void c9_sortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_y, real_T c9_varargin_1[2], c9_emxArray_int32_T
  *c9_b_y)
{
  int32_T c9_i797;
  int32_T c9_i798;
  int32_T c9_i799;
  int32_T c9_loop_ub;
  int32_T c9_i800;
  int32_T c9_i801;
  real_T c9_b_varargin_1[2];
  c9_i797 = c9_b_y->size[0] * c9_b_y->size[1];
  c9_b_y->size[0] = c9_y->size[0];
  c9_b_y->size[1] = 2;
  c9_emxEnsureCapacity_int32_T1(chartInstance, c9_b_y, c9_i797, &c9_gi_emlrtRTEI);
  c9_i798 = c9_b_y->size[0];
  c9_i799 = c9_b_y->size[1];
  c9_loop_ub = c9_y->size[0] * c9_y->size[1] - 1;
  for (c9_i800 = 0; c9_i800 <= c9_loop_ub; c9_i800++) {
    c9_b_y->data[c9_i800] = c9_y->data[c9_i800];
  }

  for (c9_i801 = 0; c9_i801 < 2; c9_i801++) {
    c9_b_varargin_1[c9_i801] = c9_varargin_1[c9_i801];
  }

  c9_b_sortrows(chartInstance, c9_b_y, c9_b_varargin_1);
}

static boolean_T c9_b_sortLE(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_v, int32_T c9_dir[2], int32_T c9_idx1, int32_T c9_idx2)
{
  boolean_T c9_p;
  int32_T c9_irow1;
  int32_T c9_irow2;
  int32_T c9_k;
  int32_T c9_b_k;
  int32_T c9_colk;
  int32_T c9_x;
  int32_T c9_b_x;
  int32_T c9_c_x;
  int32_T c9_abscolk;
  int32_T c9_v1;
  int32_T c9_v2;
  boolean_T c9_v1eqv2;
  boolean_T c9_b76;
  int32_T c9_a;
  int32_T c9_b;
  int32_T c9_b_a;
  int32_T c9_b_b;
  boolean_T exitg1;
  (void)chartInstance;
  c9_irow1 = c9_idx1 - 1;
  c9_irow2 = c9_idx2 - 1;
  c9_p = true;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k < 2)) {
    c9_b_k = c9_k;
    c9_colk = c9_dir[c9_b_k];
    c9_x = c9_colk;
    c9_b_x = c9_x;
    c9_c_x = c9_b_x;
    c9_abscolk = c9_c_x - 1;
    c9_v1 = c9_v->data[c9_irow1 + c9_v->size[0] * c9_abscolk];
    c9_v2 = c9_v->data[c9_irow2 + c9_v->size[0] * c9_abscolk];
    c9_v1eqv2 = (c9_v1 == c9_v2);
    if (c9_v1eqv2) {
      c9_b76 = true;
    } else {
      c9_b76 = false;
    }

    if (!c9_b76) {
      c9_a = c9_v1;
      c9_b = c9_v2;
      c9_b_a = c9_a;
      c9_b_b = c9_b;
      c9_p = (c9_b_a <= c9_b_b);
      exitg1 = true;
    } else {
      c9_k++;
    }
  }

  return c9_p;
}

static void c9_sort(SFc9_LIDAR_simInstanceStruct *chartInstance,
                    c9_emxArray_real_T *c9_x, c9_emxArray_real_T *c9_b_x)
{
  int32_T c9_i802;
  int32_T c9_loop_ub;
  int32_T c9_i803;
  c9_i802 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i802, &c9_hi_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i803 = 0; c9_i803 <= c9_loop_ub; c9_i803++) {
    c9_b_x->data[c9_i803] = c9_x->data[c9_i803];
  }

  c9_b_sort(chartInstance, c9_b_x);
}

static void c9_sortIdx(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T
  *c9_b_x)
{
  int32_T c9_i804;
  int32_T c9_loop_ub;
  int32_T c9_i805;
  c9_i804 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i804, &c9_ii_emlrtRTEI);
  c9_loop_ub = c9_x->size[0] - 1;
  for (c9_i805 = 0; c9_i805 <= c9_loop_ub; c9_i805++) {
    c9_b_x->data[c9_i805] = c9_x->data[c9_i805];
  }

  c9_b_sortIdx(chartInstance, c9_b_x, c9_idx);
}

static void c9_merge_block(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x, int32_T c9_offset,
  int32_T c9_n, int32_T c9_preSortLevel, c9_emxArray_int32_T *c9_iwork,
  c9_emxArray_real_T *c9_xwork, c9_emxArray_int32_T *c9_b_idx,
  c9_emxArray_real_T *c9_b_x, c9_emxArray_int32_T *c9_b_iwork,
  c9_emxArray_real_T *c9_b_xwork)
{
  int32_T c9_i806;
  int32_T c9_loop_ub;
  int32_T c9_i807;
  int32_T c9_i808;
  int32_T c9_b_loop_ub;
  int32_T c9_i809;
  int32_T c9_i810;
  int32_T c9_c_loop_ub;
  int32_T c9_i811;
  int32_T c9_i812;
  int32_T c9_d_loop_ub;
  int32_T c9_i813;
  c9_i806 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i806,
    &c9_ji_emlrtRTEI);
  c9_loop_ub = c9_idx->size[0] - 1;
  for (c9_i807 = 0; c9_i807 <= c9_loop_ub; c9_i807++) {
    c9_b_idx->data[c9_i807] = c9_idx->data[c9_i807];
  }

  c9_i808 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i808, &c9_ji_emlrtRTEI);
  c9_b_loop_ub = c9_x->size[0] - 1;
  for (c9_i809 = 0; c9_i809 <= c9_b_loop_ub; c9_i809++) {
    c9_b_x->data[c9_i809] = c9_x->data[c9_i809];
  }

  c9_i810 = c9_b_iwork->size[0];
  c9_b_iwork->size[0] = c9_iwork->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_iwork, c9_i810,
    &c9_ji_emlrtRTEI);
  c9_c_loop_ub = c9_iwork->size[0] - 1;
  for (c9_i811 = 0; c9_i811 <= c9_c_loop_ub; c9_i811++) {
    c9_b_iwork->data[c9_i811] = c9_iwork->data[c9_i811];
  }

  c9_i812 = c9_b_xwork->size[0];
  c9_b_xwork->size[0] = c9_xwork->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_xwork, c9_i812,
    &c9_ji_emlrtRTEI);
  c9_d_loop_ub = c9_xwork->size[0] - 1;
  for (c9_i813 = 0; c9_i813 <= c9_d_loop_ub; c9_i813++) {
    c9_b_xwork->data[c9_i813] = c9_xwork->data[c9_i813];
  }

  c9_b_merge_block(chartInstance, c9_b_idx, c9_b_x, c9_offset, c9_n,
                   c9_preSortLevel, c9_b_iwork, c9_b_xwork);
}

static void c9_merge(SFc9_LIDAR_simInstanceStruct *chartInstance,
                     c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x,
                     int32_T c9_offset, int32_T c9_np, int32_T c9_nq,
                     c9_emxArray_int32_T *c9_iwork, c9_emxArray_real_T *c9_xwork,
                     c9_emxArray_int32_T *c9_b_idx, c9_emxArray_real_T *c9_b_x,
                     c9_emxArray_int32_T *c9_b_iwork, c9_emxArray_real_T
                     *c9_b_xwork)
{
  int32_T c9_i814;
  int32_T c9_loop_ub;
  int32_T c9_i815;
  int32_T c9_i816;
  int32_T c9_b_loop_ub;
  int32_T c9_i817;
  int32_T c9_i818;
  int32_T c9_c_loop_ub;
  int32_T c9_i819;
  int32_T c9_i820;
  int32_T c9_d_loop_ub;
  int32_T c9_i821;
  c9_i814 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i814,
    &c9_ki_emlrtRTEI);
  c9_loop_ub = c9_idx->size[0] - 1;
  for (c9_i815 = 0; c9_i815 <= c9_loop_ub; c9_i815++) {
    c9_b_idx->data[c9_i815] = c9_idx->data[c9_i815];
  }

  c9_i816 = c9_b_x->size[0];
  c9_b_x->size[0] = c9_x->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_x, c9_i816, &c9_ki_emlrtRTEI);
  c9_b_loop_ub = c9_x->size[0] - 1;
  for (c9_i817 = 0; c9_i817 <= c9_b_loop_ub; c9_i817++) {
    c9_b_x->data[c9_i817] = c9_x->data[c9_i817];
  }

  c9_i818 = c9_b_iwork->size[0];
  c9_b_iwork->size[0] = c9_iwork->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_iwork, c9_i818,
    &c9_ki_emlrtRTEI);
  c9_c_loop_ub = c9_iwork->size[0] - 1;
  for (c9_i819 = 0; c9_i819 <= c9_c_loop_ub; c9_i819++) {
    c9_b_iwork->data[c9_i819] = c9_iwork->data[c9_i819];
  }

  c9_i820 = c9_b_xwork->size[0];
  c9_b_xwork->size[0] = c9_xwork->size[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_xwork, c9_i820,
    &c9_ki_emlrtRTEI);
  c9_d_loop_ub = c9_xwork->size[0] - 1;
  for (c9_i821 = 0; c9_i821 <= c9_d_loop_ub; c9_i821++) {
    c9_b_xwork->data[c9_i821] = c9_xwork->data[c9_i821];
  }

  c9_b_merge(chartInstance, c9_b_idx, c9_b_x, c9_offset, c9_np, c9_nq,
             c9_b_iwork, c9_b_xwork);
}

static real_T c9_cosd(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T c9_x)
{
  real_T c9_b_x;
  c9_b_x = c9_x;
  c9_b_cosd(chartInstance, &c9_b_x);
  return c9_b_x;
}

static real_T c9_sind(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T c9_x)
{
  real_T c9_b_x;
  c9_b_x = c9_x;
  c9_b_sind(chartInstance, &c9_b_x);
  return c9_b_x;
}

static const mxArray *c9_emlrt_marshallOut(SFc9_LIDAR_simInstanceStruct
  *chartInstance, const char_T c9_b_u[30])
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u, 10, 0U, 1U, 0U, 2, 1, 30),
                false);
  return c9_y;
}

static const mxArray *c9_b_emlrt_marshallOut(SFc9_LIDAR_simInstanceStruct
  *chartInstance, const char_T c9_b_u[14])
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_b_u, 10, 0U, 1U, 0U, 2, 1, 14),
                false);
  return c9_y;
}

static const mxArray *c9_l_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData;
  int32_T c9_b_u;
  const mxArray *c9_y = NULL;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  c9_mxArrayOutData = NULL;
  c9_b_u = *(int32_T *)c9_inData;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c9_mxArrayOutData, c9_y, false);
  return c9_mxArrayOutData;
}

static int32_T c9_m_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId)
{
  int32_T c9_y;
  int32_T c9_i822;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_b_u), &c9_i822, 1, 6, 0U, 0, 0U, 0);
  c9_y = c9_i822;
  sf_mex_destroy(&c9_b_u);
  return c9_y;
}

static void c9_k_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  const mxArray *c9_b_sfEvent;
  const char_T *c9_identifier;
  emlrtMsgIdentifier c9_thisId;
  int32_T c9_y;
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c9_b_sfEvent = sf_mex_dup(c9_mxArrayInData);
  c9_identifier = c9_varName;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_y = c9_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_sfEvent),
    &c9_thisId);
  sf_mex_destroy(&c9_b_sfEvent);
  *(int32_T *)c9_outData = c9_y;
  sf_mex_destroy(&c9_mxArrayInData);
}

static uint8_T c9_n_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_is_active_c9_LIDAR_sim, const char_T *c9_identifier)
{
  uint8_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = (const char *)c9_identifier;
  c9_thisId.fParent = NULL;
  c9_thisId.bParentIsCell = false;
  c9_y = c9_o_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_b_is_active_c9_LIDAR_sim), &c9_thisId);
  sf_mex_destroy(&c9_b_is_active_c9_LIDAR_sim);
  return c9_y;
}

static uint8_T c9_o_emlrt_marshallIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c9_b_u, const emlrtMsgIdentifier *c9_parentId)
{
  uint8_T c9_y;
  uint8_T c9_u6;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_b_u), &c9_u6, 1, 3, 0U, 0, 0U, 0);
  c9_y = c9_u6;
  sf_mex_destroy(&c9_b_u);
  return c9_y;
}

static void c9_c_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, boolean_T c9_idx_data[], int32_T c9_idx_size[1])
{
  real_T c9_d36;
  int32_T c9_n;
  int32_T c9_k;
  boolean_T c9_p;
  const mxArray *c9_y = NULL;
  int32_T c9_nrowx;
  const mxArray *c9_b_y = NULL;
  int32_T c9_b_n;
  int32_T c9_i823;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_b_k;
  int32_T c9_nrows;
  int32_T c9_c_k;
  int32_T c9_nb;
  int32_T c9_i;
  int32_T c9_c_b;
  int32_T c9_d_b;
  boolean_T c9_b_overflow;
  int32_T c9_d_k;
  int32_T c9_e_k;
  const mxArray *c9_c_y = NULL;
  boolean_T c9_b77;
  const mxArray *c9_d_y = NULL;
  int32_T c9_j;
  int32_T c9_i824;
  c9_emxArray_real_T *c9_b_x;
  int32_T c9_b_j;
  int32_T c9_i825;
  int32_T c9_i826;
  int32_T c9_i827;
  int32_T c9_loop_ub;
  int32_T c9_i828;
  int32_T c9_i829;
  int32_T c9_b_loop_ub;
  int32_T c9_i830;
  c9_d36 = (real_T)c9_x->size[0];
  c9_n = (int32_T)c9_d36;
  c9_k = c9_idx_size[0];
  while ((c9_k >= 1) && (!c9_idx_data[c9_k - 1])) {
    c9_k--;
  }

  c9_p = (c9_k <= c9_n);
  if (c9_p) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv15, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv15, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c9_b_y)));
  }

  c9_nrowx = c9_x->size[0];
  c9_b_n = 0;
  c9_i823 = c9_idx_size[0];
  c9_b = c9_i823;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_b_k = 0; c9_b_k < c9_i823; c9_b_k++) {
    c9_c_k = c9_b_k;
    c9_b_n += (int32_T)c9_idx_data[c9_c_k];
  }

  c9_nrows = c9_nrowx - c9_b_n;
  c9_nb = c9_idx_size[0];
  c9_i = 0;
  c9_c_b = c9_nrowx;
  c9_d_b = c9_c_b;
  if (1 > c9_d_b) {
    c9_b_overflow = false;
  } else {
    c9_b_overflow = (c9_d_b > 2147483646);
  }

  if (c9_b_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_d_k = 0; c9_d_k < c9_nrowx; c9_d_k++) {
    c9_e_k = c9_d_k;
    if ((c9_e_k + 1 > c9_nb) || (!c9_idx_data[c9_e_k])) {
      for (c9_j = 0; c9_j < 2; c9_j++) {
        c9_b_j = c9_j;
        c9_x->data[c9_i + c9_x->size[0] * c9_b_j] = c9_x->data[c9_e_k +
          c9_x->size[0] * c9_b_j];
      }

      c9_i++;
    }
  }

  if (c9_nrows <= c9_nrowx) {
  } else {
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_d_y)));
  }

  c9_b77 = (1 > c9_nrows);
  if (c9_b77) {
    c9_i824 = 0;
  } else {
    c9_i824 = c9_nrows;
  }

  c9_emxInit_real_T(chartInstance, &c9_b_x, 2, &c9_li_emlrtRTEI);
  c9_i825 = c9_b_x->size[0] * c9_b_x->size[1];
  c9_b_x->size[0] = c9_i824;
  c9_b_x->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_x, c9_i825, &c9_li_emlrtRTEI);
  for (c9_i826 = 0; c9_i826 < 2; c9_i826++) {
    c9_loop_ub = c9_i824 - 1;
    for (c9_i828 = 0; c9_i828 <= c9_loop_ub; c9_i828++) {
      c9_b_x->data[c9_i828 + c9_b_x->size[0] * c9_i826] = c9_x->data[c9_i828 +
        c9_x->size[0] * c9_i826];
    }
  }

  c9_i827 = c9_x->size[0] * c9_x->size[1];
  c9_x->size[0] = c9_b_x->size[0];
  c9_x->size[1] = 2;
  c9_emxEnsureCapacity_real_T(chartInstance, c9_x, c9_i827, &c9_mi_emlrtRTEI);
  for (c9_i829 = 0; c9_i829 < 2; c9_i829++) {
    c9_b_loop_ub = c9_b_x->size[0] - 1;
    for (c9_i830 = 0; c9_i830 <= c9_b_loop_ub; c9_i830++) {
      c9_x->data[c9_i830 + c9_x->size[0] * c9_i829] = c9_b_x->data[c9_i830 +
        c9_b_x->size[0] * c9_i829];
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_b_x);
}

static void c9_c_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x)
{
  int32_T c9_nx;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  real_T c9_b_x;
  real_T c9_c_x;
  int32_T c9_d_x[1];
  c9_nx = c9_x->size[0] << 1;
  c9_b = c9_nx;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_nx; c9_k++) {
    c9_b_k = c9_k;
    c9_b_x = c9_x->data[c9_b_k];
    c9_c_x = c9_b_x;
    c9_c_x = muDoubleScalarRound(c9_c_x);
    c9_d_x[0] = c9_x->size[0] << 1;
    c9_x->data[c9_b_k] = c9_c_x;
  }
}

static void c9_b_apply_row_permutation(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real_T *c9_y, c9_emxArray_int32_T *c9_idx)
{
  c9_emxArray_real_T *c9_ycol;
  int32_T c9_m;
  int32_T c9_iv10[2];
  int32_T c9_i831;
  int32_T c9_j;
  int32_T c9_b_j;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_i;
  int32_T c9_c_b;
  int32_T c9_b_i;
  int32_T c9_d_b;
  boolean_T c9_b_overflow;
  int32_T c9_c_i;
  c9_emxInit_real_T1(chartInstance, &c9_ycol, 1, &c9_ni_emlrtRTEI);
  c9_m = c9_y->size[0];
  c9_iv10[0] = c9_m;
  c9_iv10[1] = 1;
  c9_i831 = c9_ycol->size[0];
  c9_ycol->size[0] = c9_iv10[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_ycol, c9_i831, &c9_pc_emlrtRTEI);
  for (c9_j = 0; c9_j < 2; c9_j++) {
    c9_b_j = c9_j;
    c9_b = c9_m;
    c9_b_b = c9_b;
    if (1 > c9_b_b) {
      c9_overflow = false;
    } else {
      c9_overflow = (c9_b_b > 2147483646);
    }

    if (c9_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_i = 0; c9_i < c9_m; c9_i++) {
      c9_b_i = c9_i;
      c9_ycol->data[c9_b_i] = c9_y->data[(c9_idx->data[c9_b_i] + c9_y->size[0] *
        c9_b_j) - 1];
    }

    c9_c_b = c9_m;
    c9_d_b = c9_c_b;
    if (1 > c9_d_b) {
      c9_b_overflow = false;
    } else {
      c9_b_overflow = (c9_d_b > 2147483646);
    }

    if (c9_b_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_c_i = 0; c9_c_i < c9_m; c9_c_i++) {
      c9_b_i = c9_c_i;
      c9_y->data[c9_b_i + c9_y->size[0] * c9_b_j] = c9_ycol->data[c9_b_i];
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_ycol);
}

static void c9_d_round(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x)
{
  int32_T c9_nx;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  real_T c9_b_x;
  real_T c9_c_x;
  c9_nx = c9_x->size[0];
  c9_b = c9_nx;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_nx; c9_k++) {
    c9_b_k = c9_k;
    c9_b_x = c9_x->data[c9_b_k];
    c9_c_x = c9_b_x;
    c9_c_x = muDoubleScalarRound(c9_c_x);
    c9_x->data[c9_b_k] = c9_c_x;
  }
}

static void c9_b_locSortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_a, c9_emxArray_int32_T
  *c9_b)
{
  c9_coder_internal_anonymous_function c9_this;
  c9_cell_wrap_1 c9_tunableEnvironment[2];
  int32_T c9_i832;
  int32_T c9_loop_ub;
  int32_T c9_i833;
  int32_T c9_i834;
  int32_T c9_b_loop_ub;
  int32_T c9_i835;
  int32_T c9_i836;
  c9_emxArray_int32_T *c9_b_idx;
  int32_T c9_i837;
  int32_T c9_c_loop_ub;
  int32_T c9_i838;
  c9_emxArray_int32_T *c9_c_idx;
  int32_T c9_i839;
  int32_T c9_d_loop_ub;
  int32_T c9_i840;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_this, &c9_si_emlrtRTEI);
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_tunableEnvironment,
    &c9_ti_emlrtRTEI);
  c9_i832 = c9_tunableEnvironment[0].f1->size[0];
  c9_tunableEnvironment[0].f1->size[0] = c9_a->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_tunableEnvironment[0].f1,
    c9_i832, &c9_oi_emlrtRTEI);
  c9_loop_ub = c9_a->size[0] - 1;
  for (c9_i833 = 0; c9_i833 <= c9_loop_ub; c9_i833++) {
    c9_tunableEnvironment[0].f1->data[c9_i833] = c9_a->data[c9_i833];
  }

  c9_i834 = c9_tunableEnvironment[1].f1->size[0];
  c9_tunableEnvironment[1].f1->size[0] = c9_b->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_tunableEnvironment[1].f1,
    c9_i834, &c9_oi_emlrtRTEI);
  c9_b_loop_ub = c9_b->size[0] - 1;
  for (c9_i835 = 0; c9_i835 <= c9_b_loop_ub; c9_i835++) {
    c9_tunableEnvironment[1].f1->data[c9_i835] = c9_b->data[c9_i835];
  }

  for (c9_i836 = 0; c9_i836 < 2; c9_i836++) {
    c9_emxCopyStruct_cell_wrap_1(chartInstance,
      &c9_this.tunableEnvironment[c9_i836], &c9_tunableEnvironment[c9_i836],
      &c9_pi_emlrtRTEI);
  }

  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_tunableEnvironment);
  c9_emxInit_int32_T(chartInstance, &c9_b_idx, 1, &c9_qi_emlrtRTEI);
  c9_b_introsort(chartInstance, c9_idx, c9_a->size[0], c9_this);
  c9_i837 = c9_b_idx->size[0];
  c9_b_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_idx, c9_i837,
    &c9_qi_emlrtRTEI);
  c9_c_loop_ub = c9_idx->size[0] - 1;
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_this);
  for (c9_i838 = 0; c9_i838 <= c9_c_loop_ub; c9_i838++) {
    c9_b_idx->data[c9_i838] = c9_idx->data[c9_i838];
  }

  c9_emxInit_int32_T(chartInstance, &c9_c_idx, 1, &c9_ri_emlrtRTEI);
  c9_b_permuteVector(chartInstance, c9_b_idx, c9_a);
  c9_i839 = c9_c_idx->size[0];
  c9_c_idx->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_idx, c9_i839,
    &c9_ri_emlrtRTEI);
  c9_d_loop_ub = c9_idx->size[0] - 1;
  c9_emxFree_int32_T(chartInstance, &c9_b_idx);
  for (c9_i840 = 0; c9_i840 <= c9_d_loop_ub; c9_i840++) {
    c9_c_idx->data[c9_i840] = c9_idx->data[c9_i840];
  }

  c9_b_permuteVector(chartInstance, c9_c_idx, c9_b);
  c9_emxFree_int32_T(chartInstance, &c9_c_idx);
}

static void c9_b_insertionsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp)
{
  int32_T c9_i841;
  int32_T c9_a;
  int32_T c9_b;
  int32_T c9_b_a;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  c9_cell_wrap_1 c9_environment[2];
  c9_emxArray_int32_T *c9_r18;
  c9_emxArray_int32_T *c9_r19;
  c9_emxArray_int32_T *c9_b_environment;
  c9_emxArray_int32_T *c9_c_environment;
  int32_T c9_xc;
  int32_T c9_idx;
  int32_T c9_varargin_1;
  int32_T c9_varargin_2;
  int32_T c9_i842;
  int32_T c9_loop_ub;
  int32_T c9_i843;
  int32_T c9_i844;
  int32_T c9_b_loop_ub;
  int32_T c9_i845;
  int32_T c9_i846;
  int32_T c9_c_loop_ub;
  int32_T c9_i847;
  int32_T c9_i848;
  int32_T c9_d_loop_ub;
  int32_T c9_i849;
  int32_T c9_i850;
  int32_T c9_e_loop_ub;
  int32_T c9_i851;
  int32_T c9_i852;
  int32_T c9_f_loop_ub;
  int32_T c9_i853;
  boolean_T c9_varargout_1;
  boolean_T exitg1;
  c9_i841 = c9_xstart + 1;
  c9_a = c9_i841;
  c9_b = c9_xend;
  c9_b_a = c9_a;
  c9_b_b = c9_b;
  if (c9_b_a > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  c9_k = c9_i841 - 1;
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_environment, &c9_wi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_r18, 1, &c9_ad_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_r19, 1, &c9_ad_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_b_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_c_environment, 1, &c9_vi_emlrtRTEI);
  while (c9_k + 1 <= c9_xend) {
    c9_xc = c9_x->data[c9_k];
    c9_idx = c9_k;
    exitg1 = false;
    while ((!exitg1) && (c9_idx >= c9_xstart)) {
      c9_varargin_1 = c9_xc;
      c9_varargin_2 = c9_x->data[c9_idx - 1];
      c9_i842 = c9_r18->size[0];
      c9_r18->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_r18, c9_i842,
        &c9_ui_emlrtRTEI);
      c9_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
      for (c9_i843 = 0; c9_i843 <= c9_loop_ub; c9_i843++) {
        c9_r18->data[c9_i843] = c9_cmp.tunableEnvironment[0].f1->data[c9_i843];
      }

      c9_i844 = c9_r19->size[0];
      c9_r19->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_r19, c9_i844,
        &c9_ui_emlrtRTEI);
      c9_b_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
      for (c9_i845 = 0; c9_i845 <= c9_b_loop_ub; c9_i845++) {
        c9_r19->data[c9_i845] = c9_cmp.tunableEnvironment[1].f1->data[c9_i845];
      }

      c9_i846 = c9_environment[0].f1->size[0];
      c9_environment[0].f1->size[0] = c9_r18->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_environment[0].f1, c9_i846,
        &c9_ui_emlrtRTEI);
      c9_c_loop_ub = c9_r18->size[0] - 1;
      for (c9_i847 = 0; c9_i847 <= c9_c_loop_ub; c9_i847++) {
        c9_environment[0].f1->data[c9_i847] = c9_r18->data[c9_i847];
      }

      c9_i848 = c9_environment[1].f1->size[0];
      c9_environment[1].f1->size[0] = c9_r19->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_environment[1].f1, c9_i848,
        &c9_ui_emlrtRTEI);
      c9_d_loop_ub = c9_r19->size[0] - 1;
      for (c9_i849 = 0; c9_i849 <= c9_d_loop_ub; c9_i849++) {
        c9_environment[1].f1->data[c9_i849] = c9_r19->data[c9_i849];
      }

      c9_i850 = c9_b_environment->size[0];
      c9_b_environment->size[0] = c9_environment[0].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_environment, c9_i850,
        &c9_vi_emlrtRTEI);
      c9_e_loop_ub = c9_environment[0].f1->size[0] - 1;
      for (c9_i851 = 0; c9_i851 <= c9_e_loop_ub; c9_i851++) {
        c9_b_environment->data[c9_i851] = c9_environment[0].f1->data[c9_i851];
      }

      c9_i852 = c9_c_environment->size[0];
      c9_c_environment->size[0] = c9_environment[1].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_environment, c9_i852,
        &c9_vi_emlrtRTEI);
      c9_f_loop_ub = c9_environment[1].f1->size[0] - 1;
      for (c9_i853 = 0; c9_i853 <= c9_f_loop_ub; c9_i853++) {
        c9_c_environment->data[c9_i853] = c9_environment[1].f1->data[c9_i853];
      }

      c9_varargout_1 = c9___anon_fcn(chartInstance, c9_b_environment,
        c9_c_environment, c9_varargin_1, c9_varargin_2);
      if (c9_varargout_1) {
        c9_x->data[c9_idx] = c9_x->data[c9_idx - 1];
        c9_idx--;
      } else {
        exitg1 = true;
      }
    }

    c9_x->data[c9_idx] = c9_xc;
    c9_k++;
  }

  c9_emxFree_int32_T(chartInstance, &c9_c_environment);
  c9_emxFree_int32_T(chartInstance, &c9_b_environment);
  c9_emxFree_int32_T(chartInstance, &c9_r19);
  c9_emxFree_int32_T(chartInstance, &c9_r18);
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_environment);
}

static void c9_b_introsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  c9_coder_internal_anonymous_function c9_c_cmp;
  c9_coder_internal_anonymous_function c9_d_cmp;
  c9_coder_internal_anonymous_function c9_e_cmp;
  int32_T c9_b;
  int32_T c9_MAXDEPTH;
  c9_sBaHy6MF1FZJsDHxMqvBaiH c9_frame;
  int32_T c9_b_b;
  int32_T c9_y;
  c9_coder_internal_stack c9_st;
  int32_T c9_nd;
  const mxArray *c9_b_y = NULL;
  static char_T c9_cv71[28] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'u', 's', 'h', 'L', 'i',
    'm', 'i', 't' };

  const mxArray *c9_c_y = NULL;
  int32_T c9_n;
  const mxArray *c9_d_y = NULL;
  static char_T c9_cv72[27] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'o', 'p', 'E', 'm', 'p',
    't', 'y' };

  const mxArray *c9_e_y = NULL;
  int32_T c9_xstart;
  int32_T c9_depth;
  int32_T c9_p;
  c9_sBaHy6MF1FZJsDHxMqvBaiH c9_b_x;
  c9_sBaHy6MF1FZJsDHxMqvBaiH c9_c_x;
  int32_T c9_b_nd;
  int32_T c9_c_nd;
  const mxArray *c9_f_y = NULL;
  static char_T c9_cv73[28] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'u', 's', 'h', 'L', 'i',
    'm', 'i', 't' };

  const mxArray *c9_g_y = NULL;
  const mxArray *c9_h_y = NULL;
  static char_T c9_cv74[28] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'u', 's', 'h', 'L', 'i',
    'm', 'i', 't' };

  const mxArray *c9_i_y = NULL;
  int32_T exitg1;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_aj_emlrtRTEI);
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_c_cmp, &c9_bj_emlrtRTEI);
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_d_cmp, &c9_yi_emlrtRTEI);
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_e_cmp, &c9_xi_emlrtRTEI);
  if (1 >= c9_xend) {
  } else if (c9_xend <= 32) {
    c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_e_cmp, &c9_cmp,
      &c9_xi_emlrtRTEI);
    c9_b_insertionsort(chartInstance, c9_x, 1, c9_xend, c9_e_cmp);
  } else {
    c9_b = c9_nextpow2(chartInstance, c9_xend);
    c9_MAXDEPTH = (c9_b - 1) << 1;
    c9_frame.xstart = 1;
    c9_frame.xend = c9_xend;
    c9_frame.depth = 0;
    c9_b_b = c9_MAXDEPTH;
    c9_y = c9_b_b << 1;
    c9_stack_stack(chartInstance, c9_frame, c9_y, &c9_st);
    c9_nd = c9_st.d.size[0];
    if (c9_st.n < c9_nd) {
    } else {
      c9_b_y = NULL;
      sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv71, 10, 0U, 1U, 0U, 2, 1,
        28), false);
      c9_c_y = NULL;
      sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv71, 10, 0U, 1U, 0U, 2, 1,
        28), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_b_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_c_y)));
    }

    c9_st.d.data[c9_st.n] = c9_frame;
    c9_st.n++;
    do {
      exitg1 = 0;
      c9_n = c9_st.n;
      if (c9_n > 0) {
        if (c9_st.n > 0) {
        } else {
          c9_d_y = NULL;
          sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv72, 10, 0U, 1U, 0U, 2,
            1, 27), false);
          c9_e_y = NULL;
          sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv72, 10, 0U, 1U, 0U, 2,
            1, 27), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c9_d_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c9_e_y)));
        }

        c9_frame = c9_st.d.data[c9_st.n - 1];
        c9_st.n--;
        c9_xstart = c9_frame.xstart;
        c9_xend = c9_frame.xend;
        c9_depth = c9_frame.depth + 1;
        if ((c9_xend - c9_xstart) + 1 <= 32) {
          c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_d_cmp, &c9_cmp,
            &c9_yi_emlrtRTEI);
          c9_b_insertionsort(chartInstance, c9_x, c9_xstart, c9_xend, c9_d_cmp);
        } else if (c9_depth - 1 == c9_MAXDEPTH) {
          c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_c_cmp, &c9_cmp,
            &c9_bj_emlrtRTEI);
          c9_b_heapsort(chartInstance, c9_x, c9_xstart, c9_xend, c9_c_cmp);
        } else {
          c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
            &c9_aj_emlrtRTEI);
          c9_p = c9_b_sortpartition(chartInstance, c9_x, c9_xstart, c9_xend,
            c9_b_cmp);
          if (c9_p + 1 < c9_xend) {
            c9_b_x.xstart = c9_p + 1;
            c9_b_x.xend = c9_xend;
            c9_b_x.depth = c9_depth;
            c9_b_nd = c9_st.d.size[0];
            if (c9_st.n < c9_b_nd) {
            } else {
              c9_f_y = NULL;
              sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv73, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              c9_h_y = NULL;
              sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv73, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                                c9_f_y, 14, sf_mex_call_debug
                                (sfGlobalDebugInstanceStruct, "getString", 1U,
                                 1U, 14, sf_mex_call_debug
                                 (sfGlobalDebugInstanceStruct, "message", 1U, 1U,
                                  14, c9_h_y)));
            }

            c9_st.d.data[c9_st.n] = c9_b_x;
            c9_st.n++;
          }

          if (c9_xstart < c9_p) {
            c9_c_x.xstart = c9_xstart;
            c9_c_x.xend = c9_p;
            c9_c_x.depth = c9_depth;
            c9_c_nd = c9_st.d.size[0];
            if (c9_st.n < c9_c_nd) {
            } else {
              c9_g_y = NULL;
              sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv74, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              c9_i_y = NULL;
              sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv74, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                                c9_g_y, 14, sf_mex_call_debug
                                (sfGlobalDebugInstanceStruct, "getString", 1U,
                                 1U, 14, sf_mex_call_debug
                                 (sfGlobalDebugInstanceStruct, "message", 1U, 1U,
                                  14, c9_i_y)));
            }

            c9_st.d.data[c9_st.n] = c9_c_x;
            c9_st.n++;
          }
        }
      } else {
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_e_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_d_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_c_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static void c9_b_heapsort(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp)
{
  c9_coder_internal_anonymous_function c9_b_cmp;
  c9_coder_internal_anonymous_function c9_c_cmp;
  int32_T c9_n;
  int32_T c9_b_xstart;
  int32_T c9_b_xend;
  int32_T c9_b_n;
  int32_T c9_idx;
  int32_T c9_i854;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_t;
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cj_emlrtRTEI);
  c9_emxInitStruct_coder_internal_an(chartInstance, &c9_c_cmp, &c9_dj_emlrtRTEI);
  c9_n = c9_xend - c9_xstart;
  c9_b_xstart = c9_xstart;
  c9_b_xend = c9_xend;
  c9_b_n = c9_n + 1;
  for (c9_idx = c9_b_n; c9_idx > 0; c9_idx--) {
    c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_b_cmp, &c9_cmp,
      &c9_cj_emlrtRTEI);
    c9_b_heapify(chartInstance, c9_x, c9_idx, c9_b_xstart, c9_b_xend, c9_b_cmp);
  }

  c9_i854 = c9_n;
  c9_b = c9_i854;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_i854; c9_k++) {
    c9_t = c9_x->data[c9_xend - 1];
    c9_x->data[c9_xend - 1] = c9_x->data[c9_xstart - 1];
    c9_x->data[c9_xstart - 1] = c9_t;
    c9_xend--;
    c9_emxCopyStruct_coder_internal_an(chartInstance, &c9_c_cmp, &c9_cmp,
      &c9_dj_emlrtRTEI);
    c9_b_heapify(chartInstance, c9_x, 1, c9_xstart, c9_xend, c9_c_cmp);
  }

  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_c_cmp);
  c9_emxFreeStruct_coder_internal_an(chartInstance, &c9_b_cmp);
}

static void c9_b_heapify(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_idx, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp)
{
  boolean_T c9_changed;
  int32_T c9_xoff;
  int32_T c9_extremumIdx;
  int32_T c9_b;
  int32_T c9_y;
  int32_T c9_leftIdx;
  c9_emxArray_int32_T *c9_r20;
  c9_emxArray_int32_T *c9_r21;
  c9_cell_wrap_1 c9_environment[2];
  c9_cell_wrap_1 c9_b_environment[2];
  c9_emxArray_int32_T *c9_c_environment;
  c9_emxArray_int32_T *c9_d_environment;
  c9_emxArray_int32_T *c9_e_environment;
  c9_emxArray_int32_T *c9_f_environment;
  int32_T c9_rightIdx;
  int32_T c9_extremum;
  int32_T c9_cmpIdx;
  int32_T c9_xcmp;
  int32_T c9_xr;
  c9_cell_wrap_1 c9_g_environment[2];
  int32_T c9_varargin_1;
  c9_emxArray_int32_T *c9_h_environment;
  int32_T c9_varargin_2;
  c9_emxArray_int32_T *c9_i_environment;
  int32_T c9_i855;
  int32_T c9_loop_ub;
  int32_T c9_i856;
  int32_T c9_b_varargin_1;
  int32_T c9_b_varargin_2;
  int32_T c9_i857;
  int32_T c9_i858;
  int32_T c9_b_loop_ub;
  int32_T c9_c_loop_ub;
  int32_T c9_i859;
  int32_T c9_i860;
  int32_T c9_i861;
  int32_T c9_i862;
  int32_T c9_d_loop_ub;
  int32_T c9_e_loop_ub;
  int32_T c9_i863;
  int32_T c9_i864;
  int32_T c9_i865;
  int32_T c9_i866;
  int32_T c9_f_loop_ub;
  int32_T c9_g_loop_ub;
  int32_T c9_i867;
  int32_T c9_i868;
  int32_T c9_i869;
  int32_T c9_i870;
  int32_T c9_h_loop_ub;
  int32_T c9_i_loop_ub;
  int32_T c9_i871;
  int32_T c9_i872;
  int32_T c9_i873;
  int32_T c9_i874;
  int32_T c9_j_loop_ub;
  int32_T c9_k_loop_ub;
  int32_T c9_i875;
  int32_T c9_i876;
  int32_T c9_i877;
  boolean_T c9_varargout_1;
  int32_T c9_l_loop_ub;
  int32_T c9_c_varargin_1;
  int32_T c9_i878;
  int32_T c9_c_varargin_2;
  int32_T c9_i879;
  boolean_T c9_b_varargout_1;
  int32_T c9_m_loop_ub;
  int32_T c9_i880;
  int32_T c9_i881;
  int32_T c9_n_loop_ub;
  int32_T c9_i882;
  int32_T c9_i883;
  int32_T c9_o_loop_ub;
  int32_T c9_i884;
  int32_T c9_i885;
  int32_T c9_p_loop_ub;
  int32_T c9_i886;
  int32_T c9_i887;
  int32_T c9_q_loop_ub;
  int32_T c9_i888;
  int32_T c9_i889;
  int32_T c9_r_loop_ub;
  int32_T c9_i890;
  boolean_T c9_c_varargout_1;
  int32_T c9_b_b;
  int32_T c9_b_y;
  c9_changed = true;
  c9_xoff = c9_xstart - 1;
  c9_extremumIdx = (c9_idx + c9_xoff) - 1;
  c9_b = c9_idx;
  c9_y = c9_b << 1;
  c9_leftIdx = c9_y + c9_xoff;
  c9_emxInit_int32_T(chartInstance, &c9_r20, 1, &c9_ed_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_r21, 1, &c9_ed_emlrtRTEI);
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_environment, &c9_wi_emlrtRTEI);
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_b_environment, &c9_wi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_c_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_d_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_e_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_f_environment, 1, &c9_vi_emlrtRTEI);
  while (c9_changed && (c9_leftIdx < c9_xend)) {
    c9_changed = false;
    c9_rightIdx = c9_leftIdx;
    c9_extremum = c9_x->data[c9_extremumIdx];
    c9_cmpIdx = c9_leftIdx;
    c9_xcmp = c9_x->data[c9_leftIdx - 1];
    c9_xr = c9_x->data[c9_rightIdx];
    c9_varargin_1 = c9_xcmp;
    c9_varargin_2 = c9_xr;
    c9_i855 = c9_r20->size[0];
    c9_r20->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r20, c9_i855,
      &c9_ui_emlrtRTEI);
    c9_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
    for (c9_i856 = 0; c9_i856 <= c9_loop_ub; c9_i856++) {
      c9_r20->data[c9_i856] = c9_cmp.tunableEnvironment[0].f1->data[c9_i856];
    }

    c9_i858 = c9_r21->size[0];
    c9_r21->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r21, c9_i858,
      &c9_ui_emlrtRTEI);
    c9_c_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
    for (c9_i860 = 0; c9_i860 <= c9_c_loop_ub; c9_i860++) {
      c9_r21->data[c9_i860] = c9_cmp.tunableEnvironment[1].f1->data[c9_i860];
    }

    c9_i862 = c9_environment[0].f1->size[0];
    c9_environment[0].f1->size[0] = c9_r20->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_environment[0].f1, c9_i862,
      &c9_ui_emlrtRTEI);
    c9_e_loop_ub = c9_r20->size[0] - 1;
    for (c9_i864 = 0; c9_i864 <= c9_e_loop_ub; c9_i864++) {
      c9_environment[0].f1->data[c9_i864] = c9_r20->data[c9_i864];
    }

    c9_i866 = c9_environment[1].f1->size[0];
    c9_environment[1].f1->size[0] = c9_r21->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_environment[1].f1, c9_i866,
      &c9_ui_emlrtRTEI);
    c9_g_loop_ub = c9_r21->size[0] - 1;
    for (c9_i868 = 0; c9_i868 <= c9_g_loop_ub; c9_i868++) {
      c9_environment[1].f1->data[c9_i868] = c9_r21->data[c9_i868];
    }

    c9_i870 = c9_e_environment->size[0];
    c9_e_environment->size[0] = c9_environment[0].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_e_environment, c9_i870,
      &c9_vi_emlrtRTEI);
    c9_i_loop_ub = c9_environment[0].f1->size[0] - 1;
    for (c9_i872 = 0; c9_i872 <= c9_i_loop_ub; c9_i872++) {
      c9_e_environment->data[c9_i872] = c9_environment[0].f1->data[c9_i872];
    }

    c9_i874 = c9_f_environment->size[0];
    c9_f_environment->size[0] = c9_environment[1].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_f_environment, c9_i874,
      &c9_vi_emlrtRTEI);
    c9_k_loop_ub = c9_environment[1].f1->size[0] - 1;
    for (c9_i876 = 0; c9_i876 <= c9_k_loop_ub; c9_i876++) {
      c9_f_environment->data[c9_i876] = c9_environment[1].f1->data[c9_i876];
    }

    c9_varargout_1 = c9___anon_fcn(chartInstance, c9_e_environment,
      c9_f_environment, c9_varargin_1, c9_varargin_2);
    if (c9_varargout_1) {
      c9_cmpIdx = c9_leftIdx + 1;
      c9_xcmp = c9_xr;
    }

    c9_c_varargin_1 = c9_extremum;
    c9_c_varargin_2 = c9_xcmp;
    c9_i879 = c9_r20->size[0];
    c9_r20->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r20, c9_i879,
      &c9_ui_emlrtRTEI);
    c9_m_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
    for (c9_i880 = 0; c9_i880 <= c9_m_loop_ub; c9_i880++) {
      c9_r20->data[c9_i880] = c9_cmp.tunableEnvironment[0].f1->data[c9_i880];
    }

    c9_i881 = c9_r21->size[0];
    c9_r21->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r21, c9_i881,
      &c9_ui_emlrtRTEI);
    c9_n_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
    for (c9_i882 = 0; c9_i882 <= c9_n_loop_ub; c9_i882++) {
      c9_r21->data[c9_i882] = c9_cmp.tunableEnvironment[1].f1->data[c9_i882];
    }

    c9_i883 = c9_b_environment[0].f1->size[0];
    c9_b_environment[0].f1->size[0] = c9_r20->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_environment[0].f1, c9_i883,
      &c9_ui_emlrtRTEI);
    c9_o_loop_ub = c9_r20->size[0] - 1;
    for (c9_i884 = 0; c9_i884 <= c9_o_loop_ub; c9_i884++) {
      c9_b_environment[0].f1->data[c9_i884] = c9_r20->data[c9_i884];
    }

    c9_i885 = c9_b_environment[1].f1->size[0];
    c9_b_environment[1].f1->size[0] = c9_r21->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_environment[1].f1, c9_i885,
      &c9_ui_emlrtRTEI);
    c9_p_loop_ub = c9_r21->size[0] - 1;
    for (c9_i886 = 0; c9_i886 <= c9_p_loop_ub; c9_i886++) {
      c9_b_environment[1].f1->data[c9_i886] = c9_r21->data[c9_i886];
    }

    c9_i887 = c9_c_environment->size[0];
    c9_c_environment->size[0] = c9_b_environment[0].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_environment, c9_i887,
      &c9_vi_emlrtRTEI);
    c9_q_loop_ub = c9_b_environment[0].f1->size[0] - 1;
    for (c9_i888 = 0; c9_i888 <= c9_q_loop_ub; c9_i888++) {
      c9_c_environment->data[c9_i888] = c9_b_environment[0].f1->data[c9_i888];
    }

    c9_i889 = c9_d_environment->size[0];
    c9_d_environment->size[0] = c9_b_environment[1].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_d_environment, c9_i889,
      &c9_vi_emlrtRTEI);
    c9_r_loop_ub = c9_b_environment[1].f1->size[0] - 1;
    for (c9_i890 = 0; c9_i890 <= c9_r_loop_ub; c9_i890++) {
      c9_d_environment->data[c9_i890] = c9_b_environment[1].f1->data[c9_i890];
    }

    c9_c_varargout_1 = c9___anon_fcn(chartInstance, c9_c_environment,
      c9_d_environment, c9_c_varargin_1, c9_c_varargin_2);
    if (c9_c_varargout_1) {
      c9_x->data[c9_extremumIdx] = c9_xcmp;
      c9_x->data[c9_cmpIdx - 1] = c9_extremum;
      c9_extremumIdx = c9_cmpIdx - 1;
      c9_b_b = c9_cmpIdx - c9_xoff;
      c9_b_y = c9_b_b << 1;
      c9_leftIdx = c9_b_y + c9_xoff;
      c9_changed = true;
    }
  }

  c9_emxFree_int32_T(chartInstance, &c9_f_environment);
  c9_emxFree_int32_T(chartInstance, &c9_e_environment);
  c9_emxFree_int32_T(chartInstance, &c9_d_environment);
  c9_emxFree_int32_T(chartInstance, &c9_c_environment);
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_b_environment);
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_environment);
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_g_environment, &c9_wi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_h_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_i_environment, 1, &c9_vi_emlrtRTEI);
  if (c9_changed && (c9_leftIdx <= c9_xend)) {
    c9_extremum = c9_x->data[c9_extremumIdx];
    c9_xcmp = c9_x->data[c9_leftIdx - 1];
    c9_b_varargin_1 = c9_extremum;
    c9_b_varargin_2 = c9_xcmp;
    c9_i857 = c9_r20->size[0];
    c9_r20->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r20, c9_i857,
      &c9_ui_emlrtRTEI);
    c9_b_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
    for (c9_i859 = 0; c9_i859 <= c9_b_loop_ub; c9_i859++) {
      c9_r20->data[c9_i859] = c9_cmp.tunableEnvironment[0].f1->data[c9_i859];
    }

    c9_i861 = c9_r21->size[0];
    c9_r21->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r21, c9_i861,
      &c9_ui_emlrtRTEI);
    c9_d_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
    for (c9_i863 = 0; c9_i863 <= c9_d_loop_ub; c9_i863++) {
      c9_r21->data[c9_i863] = c9_cmp.tunableEnvironment[1].f1->data[c9_i863];
    }

    c9_i865 = c9_g_environment[0].f1->size[0];
    c9_g_environment[0].f1->size[0] = c9_r20->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_g_environment[0].f1, c9_i865,
      &c9_ui_emlrtRTEI);
    c9_f_loop_ub = c9_r20->size[0] - 1;
    for (c9_i867 = 0; c9_i867 <= c9_f_loop_ub; c9_i867++) {
      c9_g_environment[0].f1->data[c9_i867] = c9_r20->data[c9_i867];
    }

    c9_i869 = c9_g_environment[1].f1->size[0];
    c9_g_environment[1].f1->size[0] = c9_r21->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_g_environment[1].f1, c9_i869,
      &c9_ui_emlrtRTEI);
    c9_h_loop_ub = c9_r21->size[0] - 1;
    for (c9_i871 = 0; c9_i871 <= c9_h_loop_ub; c9_i871++) {
      c9_g_environment[1].f1->data[c9_i871] = c9_r21->data[c9_i871];
    }

    c9_i873 = c9_h_environment->size[0];
    c9_h_environment->size[0] = c9_g_environment[0].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_h_environment, c9_i873,
      &c9_vi_emlrtRTEI);
    c9_j_loop_ub = c9_g_environment[0].f1->size[0] - 1;
    for (c9_i875 = 0; c9_i875 <= c9_j_loop_ub; c9_i875++) {
      c9_h_environment->data[c9_i875] = c9_g_environment[0].f1->data[c9_i875];
    }

    c9_i877 = c9_i_environment->size[0];
    c9_i_environment->size[0] = c9_g_environment[1].f1->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_i_environment, c9_i877,
      &c9_vi_emlrtRTEI);
    c9_l_loop_ub = c9_g_environment[1].f1->size[0] - 1;
    for (c9_i878 = 0; c9_i878 <= c9_l_loop_ub; c9_i878++) {
      c9_i_environment->data[c9_i878] = c9_g_environment[1].f1->data[c9_i878];
    }

    c9_b_varargout_1 = c9___anon_fcn(chartInstance, c9_h_environment,
      c9_i_environment, c9_b_varargin_1, c9_b_varargin_2);
    if (c9_b_varargout_1) {
      c9_x->data[c9_extremumIdx] = c9_xcmp;
      c9_x->data[c9_leftIdx - 1] = c9_extremum;
    }
  }

  c9_emxFree_int32_T(chartInstance, &c9_i_environment);
  c9_emxFree_int32_T(chartInstance, &c9_h_environment);
  c9_emxFree_int32_T(chartInstance, &c9_r21);
  c9_emxFree_int32_T(chartInstance, &c9_r20);
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_g_environment);
}

static int32_T c9_b_sortpartition(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_x, int32_T c9_xstart, int32_T c9_xend,
  c9_coder_internal_anonymous_function c9_cmp)
{
  int32_T c9_p;
  c9_emxArray_int32_T *c9_r22;
  int32_T c9_a;
  int32_T c9_c;
  int32_T c9_xmid;
  int32_T c9_varargin_1;
  int32_T c9_varargin_2;
  int32_T c9_i891;
  int32_T c9_loop_ub;
  int32_T c9_i892;
  c9_emxArray_int32_T *c9_r23;
  int32_T c9_i893;
  int32_T c9_b_loop_ub;
  int32_T c9_i894;
  c9_cell_wrap_1 c9_environment[2];
  int32_T c9_i895;
  int32_T c9_c_loop_ub;
  int32_T c9_i896;
  int32_T c9_i897;
  int32_T c9_d_loop_ub;
  int32_T c9_i898;
  c9_emxArray_int32_T *c9_b_environment;
  int32_T c9_i899;
  int32_T c9_e_loop_ub;
  int32_T c9_i900;
  c9_emxArray_int32_T *c9_c_environment;
  int32_T c9_i901;
  int32_T c9_f_loop_ub;
  int32_T c9_i902;
  boolean_T c9_varargout_1;
  int32_T c9_t;
  int32_T c9_b_varargin_1;
  int32_T c9_b_varargin_2;
  int32_T c9_i903;
  int32_T c9_g_loop_ub;
  int32_T c9_i904;
  int32_T c9_i905;
  int32_T c9_h_loop_ub;
  int32_T c9_i906;
  c9_cell_wrap_1 c9_d_environment[2];
  int32_T c9_i907;
  int32_T c9_i_loop_ub;
  int32_T c9_i908;
  int32_T c9_i909;
  int32_T c9_j_loop_ub;
  int32_T c9_i910;
  c9_emxArray_int32_T *c9_e_environment;
  int32_T c9_i911;
  int32_T c9_k_loop_ub;
  int32_T c9_i912;
  c9_emxArray_int32_T *c9_f_environment;
  int32_T c9_i913;
  int32_T c9_l_loop_ub;
  int32_T c9_i914;
  boolean_T c9_b_varargout_1;
  int32_T c9_c_varargin_1;
  int32_T c9_c_varargin_2;
  int32_T c9_i915;
  int32_T c9_m_loop_ub;
  int32_T c9_i916;
  int32_T c9_i917;
  int32_T c9_n_loop_ub;
  int32_T c9_i918;
  c9_cell_wrap_1 c9_g_environment[2];
  int32_T c9_i919;
  int32_T c9_o_loop_ub;
  int32_T c9_i920;
  int32_T c9_i921;
  int32_T c9_p_loop_ub;
  int32_T c9_i922;
  c9_emxArray_int32_T *c9_h_environment;
  int32_T c9_i923;
  int32_T c9_q_loop_ub;
  int32_T c9_i924;
  c9_emxArray_int32_T *c9_i_environment;
  int32_T c9_i925;
  int32_T c9_r_loop_ub;
  int32_T c9_i926;
  boolean_T c9_c_varargout_1;
  int32_T c9_pivot;
  int32_T c9_i;
  int32_T c9_j;
  c9_cell_wrap_1 c9_j_environment[2];
  c9_cell_wrap_1 c9_k_environment[2];
  c9_emxArray_int32_T *c9_l_environment;
  c9_emxArray_int32_T *c9_m_environment;
  c9_emxArray_int32_T *c9_n_environment;
  c9_emxArray_int32_T *c9_o_environment;
  int32_T c9_d_varargin_1;
  int32_T c9_d_varargin_2;
  int32_T c9_i927;
  int32_T c9_s_loop_ub;
  int32_T c9_i928;
  int32_T c9_i929;
  int32_T c9_t_loop_ub;
  int32_T c9_i930;
  int32_T c9_i931;
  int32_T c9_u_loop_ub;
  int32_T c9_i932;
  int32_T c9_i933;
  int32_T c9_v_loop_ub;
  int32_T c9_i934;
  int32_T c9_i935;
  int32_T c9_w_loop_ub;
  int32_T c9_i936;
  int32_T c9_i937;
  int32_T c9_x_loop_ub;
  int32_T c9_i938;
  boolean_T c9_d_varargout_1;
  int32_T c9_e_varargin_1;
  int32_T c9_e_varargin_2;
  int32_T c9_i939;
  int32_T c9_y_loop_ub;
  int32_T c9_i940;
  int32_T c9_i941;
  int32_T c9_ab_loop_ub;
  int32_T c9_i942;
  int32_T c9_i943;
  int32_T c9_bb_loop_ub;
  int32_T c9_i944;
  int32_T c9_i945;
  int32_T c9_cb_loop_ub;
  int32_T c9_i946;
  int32_T c9_i947;
  int32_T c9_db_loop_ub;
  int32_T c9_i948;
  int32_T c9_i949;
  int32_T c9_eb_loop_ub;
  int32_T c9_i950;
  boolean_T c9_e_varargout_1;
  int32_T exitg1;
  int32_T exitg2;
  c9_emxInit_int32_T(chartInstance, &c9_r22, 1, &c9_fd_emlrtRTEI);
  c9_a = c9_xend - c9_xstart;
  c9_c = c9_div_nzp_s32(chartInstance, c9_a, 2, 1U, 0, 0);
  c9_xmid = (c9_xstart + c9_c) - 1;
  c9_varargin_1 = c9_x->data[c9_xmid];
  c9_varargin_2 = c9_x->data[c9_xstart - 1];
  c9_i891 = c9_r22->size[0];
  c9_r22->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_r22, c9_i891, &c9_ui_emlrtRTEI);
  c9_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
  for (c9_i892 = 0; c9_i892 <= c9_loop_ub; c9_i892++) {
    c9_r22->data[c9_i892] = c9_cmp.tunableEnvironment[0].f1->data[c9_i892];
  }

  c9_emxInit_int32_T(chartInstance, &c9_r23, 1, &c9_fd_emlrtRTEI);
  c9_i893 = c9_r23->size[0];
  c9_r23->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_r23, c9_i893, &c9_ui_emlrtRTEI);
  c9_b_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
  for (c9_i894 = 0; c9_i894 <= c9_b_loop_ub; c9_i894++) {
    c9_r23->data[c9_i894] = c9_cmp.tunableEnvironment[1].f1->data[c9_i894];
  }

  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_environment, &c9_wi_emlrtRTEI);
  c9_i895 = c9_environment[0].f1->size[0];
  c9_environment[0].f1->size[0] = c9_r22->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_environment[0].f1, c9_i895,
    &c9_ui_emlrtRTEI);
  c9_c_loop_ub = c9_r22->size[0] - 1;
  for (c9_i896 = 0; c9_i896 <= c9_c_loop_ub; c9_i896++) {
    c9_environment[0].f1->data[c9_i896] = c9_r22->data[c9_i896];
  }

  c9_i897 = c9_environment[1].f1->size[0];
  c9_environment[1].f1->size[0] = c9_r23->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_environment[1].f1, c9_i897,
    &c9_ui_emlrtRTEI);
  c9_d_loop_ub = c9_r23->size[0] - 1;
  for (c9_i898 = 0; c9_i898 <= c9_d_loop_ub; c9_i898++) {
    c9_environment[1].f1->data[c9_i898] = c9_r23->data[c9_i898];
  }

  c9_emxInit_int32_T(chartInstance, &c9_b_environment, 1, &c9_vi_emlrtRTEI);
  c9_i899 = c9_b_environment->size[0];
  c9_b_environment->size[0] = c9_environment[0].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_b_environment, c9_i899,
    &c9_vi_emlrtRTEI);
  c9_e_loop_ub = c9_environment[0].f1->size[0] - 1;
  for (c9_i900 = 0; c9_i900 <= c9_e_loop_ub; c9_i900++) {
    c9_b_environment->data[c9_i900] = c9_environment[0].f1->data[c9_i900];
  }

  c9_emxInit_int32_T(chartInstance, &c9_c_environment, 1, &c9_vi_emlrtRTEI);
  c9_i901 = c9_c_environment->size[0];
  c9_c_environment->size[0] = c9_environment[1].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_c_environment, c9_i901,
    &c9_vi_emlrtRTEI);
  c9_f_loop_ub = c9_environment[1].f1->size[0] - 1;
  for (c9_i902 = 0; c9_i902 <= c9_f_loop_ub; c9_i902++) {
    c9_c_environment->data[c9_i902] = c9_environment[1].f1->data[c9_i902];
  }

  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_environment);
  c9_varargout_1 = c9___anon_fcn(chartInstance, c9_b_environment,
    c9_c_environment, c9_varargin_1, c9_varargin_2);
  c9_emxFree_int32_T(chartInstance, &c9_c_environment);
  c9_emxFree_int32_T(chartInstance, &c9_b_environment);
  if (c9_varargout_1) {
    c9_t = c9_x->data[c9_xstart - 1];
    c9_x->data[c9_xstart - 1] = c9_x->data[c9_xmid];
    c9_x->data[c9_xmid] = c9_t;
  }

  c9_b_varargin_1 = c9_x->data[c9_xend - 1];
  c9_b_varargin_2 = c9_x->data[c9_xstart - 1];
  c9_i903 = c9_r22->size[0];
  c9_r22->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_r22, c9_i903, &c9_ui_emlrtRTEI);
  c9_g_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
  for (c9_i904 = 0; c9_i904 <= c9_g_loop_ub; c9_i904++) {
    c9_r22->data[c9_i904] = c9_cmp.tunableEnvironment[0].f1->data[c9_i904];
  }

  c9_i905 = c9_r23->size[0];
  c9_r23->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_r23, c9_i905, &c9_ui_emlrtRTEI);
  c9_h_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
  for (c9_i906 = 0; c9_i906 <= c9_h_loop_ub; c9_i906++) {
    c9_r23->data[c9_i906] = c9_cmp.tunableEnvironment[1].f1->data[c9_i906];
  }

  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_d_environment, &c9_wi_emlrtRTEI);
  c9_i907 = c9_d_environment[0].f1->size[0];
  c9_d_environment[0].f1->size[0] = c9_r22->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_d_environment[0].f1, c9_i907,
    &c9_ui_emlrtRTEI);
  c9_i_loop_ub = c9_r22->size[0] - 1;
  for (c9_i908 = 0; c9_i908 <= c9_i_loop_ub; c9_i908++) {
    c9_d_environment[0].f1->data[c9_i908] = c9_r22->data[c9_i908];
  }

  c9_i909 = c9_d_environment[1].f1->size[0];
  c9_d_environment[1].f1->size[0] = c9_r23->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_d_environment[1].f1, c9_i909,
    &c9_ui_emlrtRTEI);
  c9_j_loop_ub = c9_r23->size[0] - 1;
  for (c9_i910 = 0; c9_i910 <= c9_j_loop_ub; c9_i910++) {
    c9_d_environment[1].f1->data[c9_i910] = c9_r23->data[c9_i910];
  }

  c9_emxInit_int32_T(chartInstance, &c9_e_environment, 1, &c9_vi_emlrtRTEI);
  c9_i911 = c9_e_environment->size[0];
  c9_e_environment->size[0] = c9_d_environment[0].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_e_environment, c9_i911,
    &c9_vi_emlrtRTEI);
  c9_k_loop_ub = c9_d_environment[0].f1->size[0] - 1;
  for (c9_i912 = 0; c9_i912 <= c9_k_loop_ub; c9_i912++) {
    c9_e_environment->data[c9_i912] = c9_d_environment[0].f1->data[c9_i912];
  }

  c9_emxInit_int32_T(chartInstance, &c9_f_environment, 1, &c9_vi_emlrtRTEI);
  c9_i913 = c9_f_environment->size[0];
  c9_f_environment->size[0] = c9_d_environment[1].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_f_environment, c9_i913,
    &c9_vi_emlrtRTEI);
  c9_l_loop_ub = c9_d_environment[1].f1->size[0] - 1;
  for (c9_i914 = 0; c9_i914 <= c9_l_loop_ub; c9_i914++) {
    c9_f_environment->data[c9_i914] = c9_d_environment[1].f1->data[c9_i914];
  }

  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_d_environment);
  c9_b_varargout_1 = c9___anon_fcn(chartInstance, c9_e_environment,
    c9_f_environment, c9_b_varargin_1, c9_b_varargin_2);
  c9_emxFree_int32_T(chartInstance, &c9_f_environment);
  c9_emxFree_int32_T(chartInstance, &c9_e_environment);
  if (c9_b_varargout_1) {
    c9_t = c9_x->data[c9_xstart - 1];
    c9_x->data[c9_xstart - 1] = c9_x->data[c9_xend - 1];
    c9_x->data[c9_xend - 1] = c9_t;
  }

  c9_c_varargin_1 = c9_x->data[c9_xend - 1];
  c9_c_varargin_2 = c9_x->data[c9_xmid];
  c9_i915 = c9_r22->size[0];
  c9_r22->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_r22, c9_i915, &c9_ui_emlrtRTEI);
  c9_m_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
  for (c9_i916 = 0; c9_i916 <= c9_m_loop_ub; c9_i916++) {
    c9_r22->data[c9_i916] = c9_cmp.tunableEnvironment[0].f1->data[c9_i916];
  }

  c9_i917 = c9_r23->size[0];
  c9_r23->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_r23, c9_i917, &c9_ui_emlrtRTEI);
  c9_n_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
  for (c9_i918 = 0; c9_i918 <= c9_n_loop_ub; c9_i918++) {
    c9_r23->data[c9_i918] = c9_cmp.tunableEnvironment[1].f1->data[c9_i918];
  }

  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_g_environment, &c9_wi_emlrtRTEI);
  c9_i919 = c9_g_environment[0].f1->size[0];
  c9_g_environment[0].f1->size[0] = c9_r22->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_g_environment[0].f1, c9_i919,
    &c9_ui_emlrtRTEI);
  c9_o_loop_ub = c9_r22->size[0] - 1;
  for (c9_i920 = 0; c9_i920 <= c9_o_loop_ub; c9_i920++) {
    c9_g_environment[0].f1->data[c9_i920] = c9_r22->data[c9_i920];
  }

  c9_i921 = c9_g_environment[1].f1->size[0];
  c9_g_environment[1].f1->size[0] = c9_r23->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_g_environment[1].f1, c9_i921,
    &c9_ui_emlrtRTEI);
  c9_p_loop_ub = c9_r23->size[0] - 1;
  for (c9_i922 = 0; c9_i922 <= c9_p_loop_ub; c9_i922++) {
    c9_g_environment[1].f1->data[c9_i922] = c9_r23->data[c9_i922];
  }

  c9_emxInit_int32_T(chartInstance, &c9_h_environment, 1, &c9_vi_emlrtRTEI);
  c9_i923 = c9_h_environment->size[0];
  c9_h_environment->size[0] = c9_g_environment[0].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_h_environment, c9_i923,
    &c9_vi_emlrtRTEI);
  c9_q_loop_ub = c9_g_environment[0].f1->size[0] - 1;
  for (c9_i924 = 0; c9_i924 <= c9_q_loop_ub; c9_i924++) {
    c9_h_environment->data[c9_i924] = c9_g_environment[0].f1->data[c9_i924];
  }

  c9_emxInit_int32_T(chartInstance, &c9_i_environment, 1, &c9_vi_emlrtRTEI);
  c9_i925 = c9_i_environment->size[0];
  c9_i_environment->size[0] = c9_g_environment[1].f1->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_i_environment, c9_i925,
    &c9_vi_emlrtRTEI);
  c9_r_loop_ub = c9_g_environment[1].f1->size[0] - 1;
  for (c9_i926 = 0; c9_i926 <= c9_r_loop_ub; c9_i926++) {
    c9_i_environment->data[c9_i926] = c9_g_environment[1].f1->data[c9_i926];
  }

  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_g_environment);
  c9_c_varargout_1 = c9___anon_fcn(chartInstance, c9_h_environment,
    c9_i_environment, c9_c_varargin_1, c9_c_varargin_2);
  c9_emxFree_int32_T(chartInstance, &c9_i_environment);
  c9_emxFree_int32_T(chartInstance, &c9_h_environment);
  if (c9_c_varargout_1) {
    c9_t = c9_x->data[c9_xmid];
    c9_x->data[c9_xmid] = c9_x->data[c9_xend - 1];
    c9_x->data[c9_xend - 1] = c9_t;
  }

  c9_pivot = c9_x->data[c9_xmid];
  c9_x->data[c9_xmid] = c9_x->data[c9_xend - 2];
  c9_x->data[c9_xend - 2] = c9_pivot;
  c9_i = c9_xstart - 1;
  c9_j = c9_xend - 2;
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_j_environment, &c9_wi_emlrtRTEI);
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_k_environment, &c9_wi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_l_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_m_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_n_environment, 1, &c9_vi_emlrtRTEI);
  c9_emxInit_int32_T(chartInstance, &c9_o_environment, 1, &c9_vi_emlrtRTEI);
  do {
    exitg1 = 0;
    c9_i++;
    do {
      exitg2 = 0;
      c9_d_varargin_1 = c9_x->data[c9_i];
      c9_d_varargin_2 = c9_pivot;
      c9_i927 = c9_r22->size[0];
      c9_r22->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_r22, c9_i927,
        &c9_ui_emlrtRTEI);
      c9_s_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
      for (c9_i928 = 0; c9_i928 <= c9_s_loop_ub; c9_i928++) {
        c9_r22->data[c9_i928] = c9_cmp.tunableEnvironment[0].f1->data[c9_i928];
      }

      c9_i929 = c9_r23->size[0];
      c9_r23->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_r23, c9_i929,
        &c9_ui_emlrtRTEI);
      c9_t_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
      for (c9_i930 = 0; c9_i930 <= c9_t_loop_ub; c9_i930++) {
        c9_r23->data[c9_i930] = c9_cmp.tunableEnvironment[1].f1->data[c9_i930];
      }

      c9_i931 = c9_j_environment[0].f1->size[0];
      c9_j_environment[0].f1->size[0] = c9_r22->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_j_environment[0].f1,
        c9_i931, &c9_ui_emlrtRTEI);
      c9_u_loop_ub = c9_r22->size[0] - 1;
      for (c9_i932 = 0; c9_i932 <= c9_u_loop_ub; c9_i932++) {
        c9_j_environment[0].f1->data[c9_i932] = c9_r22->data[c9_i932];
      }

      c9_i933 = c9_j_environment[1].f1->size[0];
      c9_j_environment[1].f1->size[0] = c9_r23->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_j_environment[1].f1,
        c9_i933, &c9_ui_emlrtRTEI);
      c9_v_loop_ub = c9_r23->size[0] - 1;
      for (c9_i934 = 0; c9_i934 <= c9_v_loop_ub; c9_i934++) {
        c9_j_environment[1].f1->data[c9_i934] = c9_r23->data[c9_i934];
      }

      c9_i935 = c9_n_environment->size[0];
      c9_n_environment->size[0] = c9_j_environment[0].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_n_environment, c9_i935,
        &c9_vi_emlrtRTEI);
      c9_w_loop_ub = c9_j_environment[0].f1->size[0] - 1;
      for (c9_i936 = 0; c9_i936 <= c9_w_loop_ub; c9_i936++) {
        c9_n_environment->data[c9_i936] = c9_j_environment[0].f1->data[c9_i936];
      }

      c9_i937 = c9_o_environment->size[0];
      c9_o_environment->size[0] = c9_j_environment[1].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_o_environment, c9_i937,
        &c9_vi_emlrtRTEI);
      c9_x_loop_ub = c9_j_environment[1].f1->size[0] - 1;
      for (c9_i938 = 0; c9_i938 <= c9_x_loop_ub; c9_i938++) {
        c9_o_environment->data[c9_i938] = c9_j_environment[1].f1->data[c9_i938];
      }

      c9_d_varargout_1 = c9___anon_fcn(chartInstance, c9_n_environment,
        c9_o_environment, c9_d_varargin_1, c9_d_varargin_2);
      if (c9_d_varargout_1) {
        c9_i++;
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    c9_j--;
    do {
      exitg2 = 0;
      c9_e_varargin_1 = c9_pivot;
      c9_e_varargin_2 = c9_x->data[c9_j];
      c9_i939 = c9_r22->size[0];
      c9_r22->size[0] = c9_cmp.tunableEnvironment[0].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_r22, c9_i939,
        &c9_ui_emlrtRTEI);
      c9_y_loop_ub = c9_cmp.tunableEnvironment[0].f1->size[0] - 1;
      for (c9_i940 = 0; c9_i940 <= c9_y_loop_ub; c9_i940++) {
        c9_r22->data[c9_i940] = c9_cmp.tunableEnvironment[0].f1->data[c9_i940];
      }

      c9_i941 = c9_r23->size[0];
      c9_r23->size[0] = c9_cmp.tunableEnvironment[1].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_r23, c9_i941,
        &c9_ui_emlrtRTEI);
      c9_ab_loop_ub = c9_cmp.tunableEnvironment[1].f1->size[0] - 1;
      for (c9_i942 = 0; c9_i942 <= c9_ab_loop_ub; c9_i942++) {
        c9_r23->data[c9_i942] = c9_cmp.tunableEnvironment[1].f1->data[c9_i942];
      }

      c9_i943 = c9_k_environment[0].f1->size[0];
      c9_k_environment[0].f1->size[0] = c9_r22->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_k_environment[0].f1,
        c9_i943, &c9_ui_emlrtRTEI);
      c9_bb_loop_ub = c9_r22->size[0] - 1;
      for (c9_i944 = 0; c9_i944 <= c9_bb_loop_ub; c9_i944++) {
        c9_k_environment[0].f1->data[c9_i944] = c9_r22->data[c9_i944];
      }

      c9_i945 = c9_k_environment[1].f1->size[0];
      c9_k_environment[1].f1->size[0] = c9_r23->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_k_environment[1].f1,
        c9_i945, &c9_ui_emlrtRTEI);
      c9_cb_loop_ub = c9_r23->size[0] - 1;
      for (c9_i946 = 0; c9_i946 <= c9_cb_loop_ub; c9_i946++) {
        c9_k_environment[1].f1->data[c9_i946] = c9_r23->data[c9_i946];
      }

      c9_i947 = c9_l_environment->size[0];
      c9_l_environment->size[0] = c9_k_environment[0].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_l_environment, c9_i947,
        &c9_vi_emlrtRTEI);
      c9_db_loop_ub = c9_k_environment[0].f1->size[0] - 1;
      for (c9_i948 = 0; c9_i948 <= c9_db_loop_ub; c9_i948++) {
        c9_l_environment->data[c9_i948] = c9_k_environment[0].f1->data[c9_i948];
      }

      c9_i949 = c9_m_environment->size[0];
      c9_m_environment->size[0] = c9_k_environment[1].f1->size[0];
      c9_emxEnsureCapacity_int32_T(chartInstance, c9_m_environment, c9_i949,
        &c9_vi_emlrtRTEI);
      c9_eb_loop_ub = c9_k_environment[1].f1->size[0] - 1;
      for (c9_i950 = 0; c9_i950 <= c9_eb_loop_ub; c9_i950++) {
        c9_m_environment->data[c9_i950] = c9_k_environment[1].f1->data[c9_i950];
      }

      c9_e_varargout_1 = c9___anon_fcn(chartInstance, c9_l_environment,
        c9_m_environment, c9_e_varargin_1, c9_e_varargin_2);
      if (c9_e_varargout_1) {
        c9_j--;
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    if (c9_i + 1 >= c9_j + 1) {
      exitg1 = 1;
    } else {
      c9_t = c9_x->data[c9_i];
      c9_x->data[c9_i] = c9_x->data[c9_j];
      c9_x->data[c9_j] = c9_t;
    }
  } while (exitg1 == 0);

  c9_emxFree_int32_T(chartInstance, &c9_o_environment);
  c9_emxFree_int32_T(chartInstance, &c9_n_environment);
  c9_emxFree_int32_T(chartInstance, &c9_m_environment);
  c9_emxFree_int32_T(chartInstance, &c9_l_environment);
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_k_environment);
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_j_environment);
  c9_emxFree_int32_T(chartInstance, &c9_r23);
  c9_emxFree_int32_T(chartInstance, &c9_r22);
  c9_p = c9_i + 1;
  c9_x->data[c9_xend - 2] = c9_x->data[c9_i];
  c9_x->data[c9_i] = c9_pivot;
  return c9_p;
}

static void c9_b_permuteVector(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_int32_T *c9_y)
{
  c9_emxArray_int32_T *c9_t;
  int32_T c9_ny;
  int32_T c9_i951;
  int32_T c9_loop_ub;
  int32_T c9_i952;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  c9_emxInit_int32_T(chartInstance, &c9_t, 1, &c9_ej_emlrtRTEI);
  c9_ny = c9_y->size[0];
  c9_i951 = c9_t->size[0];
  c9_t->size[0] = c9_y->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_t, c9_i951, &c9_ej_emlrtRTEI);
  c9_loop_ub = c9_y->size[0] - 1;
  for (c9_i952 = 0; c9_i952 <= c9_loop_ub; c9_i952++) {
    c9_t->data[c9_i952] = c9_y->data[c9_i952];
  }

  c9_b = c9_ny;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_ny; c9_k++) {
    c9_b_k = c9_k;
    c9_y->data[c9_b_k] = c9_t->data[c9_idx->data[c9_b_k] - 1];
  }

  c9_emxFree_int32_T(chartInstance, &c9_t);
}

static void c9_b_sparse_fillIn(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_coder_internal_sparse *c9_this)
{
  int32_T c9_idx;
  int32_T c9_i953;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_c;
  int32_T c9_b_c;
  int32_T c9_ridx;
  int32_T c9_lastRowIdx;
  int32_T c9_currRowIdx;
  const mxArray *c9_y = NULL;
  static char_T c9_cv75[40] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 's', 'p', 'L', 'o', 'g', 'i', 'c', 'a', 'l',
    'R', 'e', 'p', 'e', 'a', 't', 'e', 'd', 'I', 'n', 'd', 'i', 'c', 'e', 's' };

  boolean_T c9_val;
  const mxArray *c9_b_y = NULL;
  c9_idx = 0;
  c9_i953 = c9_this->colidx->size[0] - 2;
  c9_b = c9_i953 + 1;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_c = 0; c9_c <= c9_i953; c9_c++) {
    c9_b_c = c9_c;
    c9_ridx = c9_this->colidx->data[c9_b_c] - 1;
    c9_this->colidx->data[c9_b_c] = c9_idx + 1;
    c9_lastRowIdx = 0;
    while (c9_ridx + 1 < c9_this->colidx->data[c9_b_c + 1]) {
      c9_currRowIdx = c9_this->rowidx->data[c9_ridx];
      if (c9_currRowIdx != c9_lastRowIdx) {
      } else {
        c9_y = NULL;
        sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv75, 10, 0U, 1U, 0U, 2, 1,
          40), false);
        c9_b_y = NULL;
        sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv75, 10, 0U, 1U, 0U, 2, 1,
          40), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y,
                          14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "message", 1U, 1U, 14, c9_b_y)));
      }

      c9_val = c9_this->d->data[c9_ridx];
      c9_lastRowIdx = c9_currRowIdx;
      c9_ridx++;
      if (c9_val) {
        c9_this->d->data[c9_idx] = true;
        c9_this->rowidx->data[c9_idx] = c9_currRowIdx;
        c9_idx++;
      }
    }
  }

  c9_this->colidx->data[c9_this->colidx->size[0] - 1] = c9_idx + 1;
}

static void c9_d_nullAssignment(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx)
{
  int32_T c9_n;
  boolean_T c9_p;
  real_T c9_d37;
  int32_T c9_i954;
  int32_T c9_k;
  real_T c9_b_k;
  const mxArray *c9_y = NULL;
  c9_emxArray_boolean_T *c9_b;
  int32_T c9_b_x;
  const mxArray *c9_b_y = NULL;
  int32_T c9_nxin;
  int32_T c9_c_x;
  int32_T c9_nrowx;
  int32_T c9_b_n;
  int32_T c9_i955;
  int32_T c9_i956;
  int32_T c9_i957;
  int32_T c9_loop_ub;
  int32_T c9_i958;
  int32_T c9_i959;
  int32_T c9_b_b;
  int32_T c9_c_b;
  boolean_T c9_overflow;
  int32_T c9_c_k;
  int32_T c9_c_n;
  int32_T c9_d_k;
  int32_T c9_i960;
  int32_T c9_d_b;
  int32_T c9_e_b;
  boolean_T c9_b_overflow;
  int32_T c9_e_k;
  int32_T c9_nxout;
  int32_T c9_f_k;
  int32_T c9_nb;
  int32_T c9_i961;
  int32_T c9_k0;
  int32_T c9_i962;
  int32_T c9_f_b;
  int32_T c9_g_b;
  boolean_T c9_c_overflow;
  int32_T c9_g_k;
  int32_T c9_h_k;
  const mxArray *c9_c_y = NULL;
  boolean_T c9_b78;
  const mxArray *c9_d_y = NULL;
  int32_T c9_i963;
  int32_T c9_i964;
  boolean_T exitg1;
  c9_n = c9_x->size[0];
  c9_p = true;
  c9_d37 = (real_T)c9_idx->size[0];
  c9_i954 = (int32_T)c9_d37 - 1;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k <= c9_i954)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    if ((c9_idx->data[(int32_T)c9_b_k - 1] < 1) || (c9_idx->data[(int32_T)c9_b_k
         - 1] > c9_n)) {
      c9_p = false;
      exitg1 = true;
    } else {
      c9_b_x = c9_idx->data[(int32_T)c9_b_k - 1];
      c9_c_x = c9_b_x;
      if (c9_idx->data[(int32_T)c9_b_k - 1] != c9_c_x) {
        c9_p = false;
        exitg1 = true;
      } else {
        c9_k++;
      }
    }
  }

  if (c9_p) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv15, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv15, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c9_b_y)));
  }

  c9_emxInit_boolean_T(chartInstance, &c9_b, 2, &c9_hj_emlrtRTEI);
  c9_nxin = c9_x->size[0];
  c9_nrowx = c9_x->size[0];
  c9_b_n = c9_nxin;
  c9_i955 = c9_b->size[0] * c9_b->size[1];
  c9_b->size[0] = 1;
  c9_b->size[1] = c9_b_n;
  c9_emxEnsureCapacity_boolean_T(chartInstance, c9_b, c9_i955, &c9_fj_emlrtRTEI);
  c9_i956 = c9_b->size[0];
  c9_i957 = c9_b->size[1];
  c9_loop_ub = c9_b_n - 1;
  for (c9_i958 = 0; c9_i958 <= c9_loop_ub; c9_i958++) {
    c9_b->data[c9_i958] = false;
  }

  c9_i959 = c9_idx->size[0];
  c9_b_b = c9_i959;
  c9_c_b = c9_b_b;
  if (1 > c9_c_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_c_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_c_k = 0; c9_c_k < c9_i959; c9_c_k++) {
    c9_d_k = c9_c_k;
    c9_b->data[c9_idx->data[c9_d_k] - 1] = true;
  }

  c9_c_n = 0;
  c9_i960 = c9_b->size[1];
  c9_d_b = c9_i960;
  c9_e_b = c9_d_b;
  if (1 > c9_e_b) {
    c9_b_overflow = false;
  } else {
    c9_b_overflow = (c9_e_b > 2147483646);
  }

  if (c9_b_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_e_k = 0; c9_e_k < c9_i960; c9_e_k++) {
    c9_f_k = c9_e_k;
    c9_i961 = c9_b->size[1];
    c9_i962 = c9_i961;
    c9_c_n += (int32_T)c9_b->data[c9_f_k];
  }

  c9_nxout = c9_nxin - c9_c_n;
  c9_nb = c9_b->size[1];
  c9_k0 = -1;
  c9_f_b = c9_nxin;
  c9_g_b = c9_f_b;
  if (1 > c9_g_b) {
    c9_c_overflow = false;
  } else {
    c9_c_overflow = (c9_g_b > 2147483646);
  }

  if (c9_c_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_g_k = 0; c9_g_k < c9_nxin; c9_g_k++) {
    c9_h_k = c9_g_k;
    if ((c9_h_k + 1 > c9_nb) || (!c9_b->data[c9_h_k])) {
      c9_k0++;
      c9_x->data[c9_k0] = c9_x->data[c9_h_k];
    }
  }

  c9_emxFree_boolean_T(chartInstance, &c9_b);
  if (c9_nxout <= c9_nrowx) {
  } else {
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_d_y)));
  }

  c9_b78 = (1 > c9_nxout);
  if (c9_b78) {
    c9_i963 = 0;
  } else {
    c9_i963 = c9_nxout;
  }

  c9_i964 = c9_x->size[0];
  c9_x->size[0] = c9_i963;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_x, c9_i964, &c9_gj_emlrtRTEI);
}

static void c9_b_imfill(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_2)
{
  boolean_T c9_p;
  boolean_T c9_b_p;
  boolean_T c9_c_p;
  boolean_T c9_d_p;
  real_T c9_x1;
  boolean_T c9_e_p;
  const mxArray *c9_y = NULL;
  static char_T c9_cv76[36] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm', 'f',
    'i', 'l', 'l', ':', 'n', 'o', 'I', 'n', 't', 'e', 'r', 'a', 'c', 't', 'i',
    'v', 'e', 'I', 'n', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  int32_T c9_i965;
  const mxArray *c9_b_y = NULL;
  int32_T c9_i966;
  real_T c9_imSizeT[2];
  boolean_T c9_f_p;
  real_T c9_d38;
  int32_T c9_i967;
  int32_T c9_k;
  real_T c9_b_k;
  real_T c9_x;
  boolean_T c9_b79;
  boolean_T c9_g_p;
  const mxArray *c9_c_y = NULL;
  static char_T c9_cv77[30] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'i', 'm', 'f',
    'i', 'l', 'l', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'P', 'o', 's',
    'i', 't', 'i', 'v', 'e' };

  boolean_T c9_h_p;
  const mxArray *c9_d_y = NULL;
  real_T c9_d39;
  int32_T c9_i968;
  const mxArray *c9_e_y = NULL;
  int32_T c9_c_k;
  static char_T c9_cv78[26] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'L', 'O', 'C', 'A', 'T', 'I', 'O', 'N', 'S',
    ',' };

  real_T c9_d_k;
  real_T c9_b_x;
  boolean_T c9_b80;
  real_T c9_c_x;
  real_T c9_d_x;
  boolean_T c9_b;
  const mxArray *c9_f_y = NULL;
  boolean_T c9_b81;
  static char_T c9_cv79[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'i', 'm', 'f',
    'i', 'l', 'l', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'I', 'n', 't',
    'e', 'g', 'e', 'r' };

  c9_emxArray_real_T *c9_locationsVar;
  real_T c9_e_x;
  const mxArray *c9_g_y = NULL;
  int32_T c9_i969;
  boolean_T c9_b_b;
  boolean_T c9_b82;
  const mxArray *c9_h_y = NULL;
  boolean_T c9_c_b;
  static char_T c9_cv80[26] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'L', 'O', 'C', 'A', 'T', 'I', 'O', 'N', 'S',
    ',' };

  c9_emxArray_real_T *c9_b_varargin_2;
  c9_emxArray_boolean_T *c9_badPixels;
  int32_T c9_i970;
  real_T c9_f_x;
  int32_T c9_i971;
  boolean_T c9_i_p;
  real_T c9_g_x;
  int32_T c9_loop_ub;
  boolean_T c9_j_p;
  int32_T c9_b_loop_ub;
  int32_T c9_i972;
  int32_T c9_i973;
  int32_T c9_c_loop_ub;
  c9_emxArray_boolean_T *c9_r24;
  int32_T c9_i974;
  real_T c9_i_y;
  int32_T c9_i975;
  int32_T c9_i976;
  int32_T c9_d_loop_ub;
  int32_T c9_e_loop_ub;
  int32_T c9_i977;
  int32_T c9_i978;
  int32_T c9_i979;
  int32_T c9_f_loop_ub;
  int32_T c9_i980;
  boolean_T c9_b83;
  const mxArray *c9_j_y = NULL;
  static char_T c9_cv81[51] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'a', 'l', 'l', '_', 'o', 'r', '_',
    'a', 'n', 'y', '_', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  boolean_T c9_k_y;
  const mxArray *c9_l_y = NULL;
  real_T c9_vlen;
  real_T c9_a;
  int32_T c9_c;
  int32_T c9_b_a;
  int32_T c9_vspread;
  int32_T c9_d_b;
  int32_T c9_i2;
  int32_T c9_c_a;
  int32_T c9_e_b;
  int32_T c9_f_b;
  boolean_T c9_overflow;
  int32_T c9_ix;
  boolean_T c9_b84;
  real_T c9_numelBadPix;
  boolean_T c9_b85;
  int32_T c9_i981;
  boolean_T c9_b86;
  int32_T c9_s;
  c9_emxArray_boolean_T *c9_mask;
  int32_T c9_i982;
  real_T c9_b_s;
  int32_T c9_idx;
  int32_T c9_i983;
  int32_T c9_b_idx;
  int32_T c9_i984;
  int32_T c9_c_idx;
  int32_T c9_g_loop_ub;
  real_T c9_d40;
  int32_T c9_i985;
  int32_T c9_n;
  boolean_T c9_k_p;
  int32_T c9_i986;
  int32_T c9_h_x;
  int32_T c9_i987;
  int32_T c9_i_x;
  const mxArray *c9_m_y = NULL;
  int32_T c9_i988;
  int32_T c9_d_idx;
  int32_T c9_i989;
  const mxArray *c9_n_y = NULL;
  int32_T c9_nrowx;
  int32_T c9_i990;
  int32_T c9_nrows;
  int32_T c9_h_loop_ub;
  int32_T c9_d_a;
  int32_T c9_i991;
  int32_T c9_g_b;
  int32_T c9_e_a;
  int32_T c9_h_b;
  c9_emxArray_boolean_T *c9_marker;
  int32_T c9_i992;
  boolean_T c9_b_overflow;
  int32_T c9_i993;
  int32_T c9_i;
  int32_T c9_i994;
  int32_T c9_i_loop_ub;
  int32_T c9_i995;
  int32_T c9_b_locationsVar[2];
  const mxArray *c9_o_y = NULL;
  boolean_T c9_b87;
  c9_emxArray_int32_T *c9_r25;
  const mxArray *c9_p_y = NULL;
  int32_T c9_b_marker;
  int32_T c9_i996;
  int32_T c9_i997;
  int32_T c9_i998;
  int32_T c9_j_loop_ub;
  int32_T c9_i999;
  int32_T c9_b_mask;
  int32_T c9_i1000;
  int32_T c9_k_loop_ub;
  int32_T c9_i1001;
  int32_T c9_i1002;
  int32_T c9_i1003;
  int32_T c9_l_loop_ub;
  int32_T c9_i1004;
  int32_T c9_i1005;
  int32_T c9_i1006;
  int32_T c9_i1007;
  real_T c9_c_mask[2];
  int32_T c9_i1008;
  int32_T c9_i1009;
  boolean_T c9_l_p;
  int32_T c9_i1010;
  boolean_T c9_m_p;
  int32_T c9_e_k;
  real_T c9_f_k;
  real_T c9_b_x1;
  real_T c9_x2;
  boolean_T c9_n_p;
  const mxArray *c9_q_y = NULL;
  static char_T c9_cv82[32] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm', 'r',
    'e', 'c', 'o', 'n', 's', 't', 'r', 'u', 'c', 't', ':', 'n', 'o', 't', 'S',
    'a', 'm', 'e', 'S', 'i', 'z', 'e' };

  int32_T c9_i1011;
  const mxArray *c9_r_y = NULL;
  int32_T c9_i1012;
  int32_T c9_i1013;
  int32_T c9_i1014;
  int32_T c9_b_varargin_1[2];
  int32_T c9_c_marker[2];
  int32_T c9_i1015;
  int32_T c9_i1016;
  int32_T c9_i1017;
  int32_T c9_i1018;
  int32_T c9_i1019;
  int32_T c9_m_loop_ub;
  int32_T c9_i1020;
  int32_T c9_n_loop_ub;
  int32_T c9_i1021;
  boolean_T exitg1;
  c9_p = false;
  c9_b_p = false;
  if ((real_T)c9_varargin_2->size[0] != 1.0) {
  } else {
    c9_b_p = true;
  }

  if (c9_b_p) {
    c9_c_p = true;
  } else {
    c9_c_p = false;
  }

  c9_d_p = c9_c_p;
  if (c9_c_p && (c9_varargin_2->size[0] != 0)) {
    c9_x1 = c9_varargin_2->data[0];
    c9_e_p = (c9_x1 == 0.0);
    if (!c9_e_p) {
      c9_d_p = false;
    }
  }

  if (!c9_d_p) {
  } else {
    c9_p = true;
  }

  if (!c9_p) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv76, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv76, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c9_b_y)));
  }

  for (c9_i965 = 0; c9_i965 < 2; c9_i965++) {
    c9_imSizeT[c9_i965] = (real_T)c9_varargin_1->size[c9_i965];
  }

  for (c9_i966 = 0; c9_i966 < 2; c9_i966++) {
    c9_imSizeT[c9_i966];
  }

  c9_f_p = true;
  c9_d38 = (real_T)c9_varargin_2->size[0];
  c9_i967 = (int32_T)c9_d38 - 1;
  c9_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_k <= c9_i967)) {
    c9_b_k = 1.0 + (real_T)c9_k;
    c9_x = c9_varargin_2->data[(int32_T)c9_b_k - 1];
    c9_g_p = !(c9_x <= 0.0);
    if (c9_g_p) {
      c9_k++;
    } else {
      c9_f_p = false;
      exitg1 = true;
    }
  }

  if (c9_f_p) {
    c9_b79 = true;
  } else {
    c9_b79 = false;
  }

  if (c9_b79) {
  } else {
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv77, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv12, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv78, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_d_y, 14, c9_e_y)));
  }

  c9_h_p = true;
  c9_d39 = (real_T)c9_varargin_2->size[0];
  c9_i968 = (int32_T)c9_d39 - 1;
  c9_c_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_c_k <= c9_i968)) {
    c9_d_k = 1.0 + (real_T)c9_c_k;
    c9_b_x = c9_varargin_2->data[(int32_T)c9_d_k - 1];
    c9_c_x = c9_b_x;
    c9_d_x = c9_c_x;
    c9_b = muDoubleScalarIsInf(c9_d_x);
    c9_b81 = !c9_b;
    c9_e_x = c9_c_x;
    c9_b_b = muDoubleScalarIsNaN(c9_e_x);
    c9_b82 = !c9_b_b;
    c9_c_b = (c9_b81 && c9_b82);
    if (c9_c_b) {
      c9_f_x = c9_b_x;
      c9_g_x = c9_f_x;
      c9_g_x = muDoubleScalarFloor(c9_g_x);
      if (c9_g_x == c9_b_x) {
        c9_i_p = true;
      } else {
        c9_i_p = false;
      }
    } else {
      c9_i_p = false;
    }

    c9_j_p = c9_i_p;
    if (c9_j_p) {
      c9_c_k++;
    } else {
      c9_h_p = false;
      exitg1 = true;
    }
  }

  if (c9_h_p) {
    c9_b80 = true;
  } else {
    c9_b80 = false;
  }

  if (c9_b80) {
  } else {
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv79, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv7, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv80, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_f_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_g_y, 14, c9_h_y)));
  }

  c9_emxInit_real_T1(chartInstance, &c9_locationsVar, 1, &c9_sj_emlrtRTEI);
  c9_i969 = c9_locationsVar->size[0];
  c9_locationsVar->size[0] = 0;
  if (c9_varargin_2->size[0] != 0) {
    c9_emxInit_real_T1(chartInstance, &c9_b_varargin_2, 1, &c9_jj_emlrtRTEI);
    c9_i970 = c9_b_varargin_2->size[0];
    c9_b_varargin_2->size[0] = c9_varargin_2->size[0] + c9_locationsVar->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_varargin_2, c9_i970,
      &c9_jj_emlrtRTEI);
    c9_loop_ub = c9_varargin_2->size[0] - 1;
    for (c9_i972 = 0; c9_i972 <= c9_loop_ub; c9_i972++) {
      c9_b_varargin_2->data[c9_i972] = c9_varargin_2->data[c9_i972];
    }

    c9_c_loop_ub = c9_locationsVar->size[0] - 1;
    for (c9_i974 = 0; c9_i974 <= c9_c_loop_ub; c9_i974++) {
      c9_b_varargin_2->data[c9_i974 + c9_varargin_2->size[0]] =
        c9_locationsVar->data[c9_i974];
    }

    c9_i976 = c9_locationsVar->size[0];
    c9_locationsVar->size[0] = c9_b_varargin_2->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_locationsVar, c9_i976,
      &c9_lj_emlrtRTEI);
    c9_e_loop_ub = c9_b_varargin_2->size[0] - 1;
    for (c9_i978 = 0; c9_i978 <= c9_e_loop_ub; c9_i978++) {
      c9_locationsVar->data[c9_i978] = c9_b_varargin_2->data[c9_i978];
    }

    c9_emxFree_real_T(chartInstance, &c9_b_varargin_2);
  }

  c9_emxInit_boolean_T1(chartInstance, &c9_badPixels, 1, &c9_mj_emlrtRTEI);
  c9_i971 = c9_badPixels->size[0];
  c9_badPixels->size[0] = c9_varargin_2->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_badPixels, c9_i971,
    &c9_ij_emlrtRTEI);
  c9_b_loop_ub = c9_varargin_2->size[0] - 1;
  for (c9_i973 = 0; c9_i973 <= c9_b_loop_ub; c9_i973++) {
    c9_badPixels->data[c9_i973] = (c9_varargin_2->data[c9_i973] < 1.0);
  }

  c9_emxInit_boolean_T1(chartInstance, &c9_r24, 1, &c9_sg_emlrtRTEI);
  c9_i_y = c9_imSizeT[0];
  c9_i_y *= c9_imSizeT[1];
  c9_i975 = c9_r24->size[0];
  c9_r24->size[0] = c9_varargin_2->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_r24, c9_i975,
    &c9_kj_emlrtRTEI);
  c9_d_loop_ub = c9_varargin_2->size[0] - 1;
  for (c9_i977 = 0; c9_i977 <= c9_d_loop_ub; c9_i977++) {
    c9_r24->data[c9_i977] = (c9_varargin_2->data[c9_i977] > c9_i_y);
  }

  _SFD_SIZE_EQ_CHECK_1D(c9_badPixels->size[0], c9_r24->size[0]);
  c9_i979 = c9_badPixels->size[0];
  c9_badPixels->size[0];
  c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_badPixels, c9_i979,
    &c9_mj_emlrtRTEI);
  c9_f_loop_ub = c9_badPixels->size[0] - 1;
  for (c9_i980 = 0; c9_i980 <= c9_f_loop_ub; c9_i980++) {
    c9_badPixels->data[c9_i980] = (c9_badPixels->data[c9_i980] || c9_r24->
      data[c9_i980]);
  }

  if ((c9_badPixels->size[0] == 1) || ((real_T)c9_badPixels->size[0] != 1.0)) {
    c9_b83 = true;
  } else {
    c9_b83 = false;
  }

  if (c9_b83) {
  } else {
    c9_j_y = NULL;
    sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv81, 10, 0U, 1U, 0U, 2, 1, 51),
                  false);
    c9_l_y = NULL;
    sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv81, 10, 0U, 1U, 0U, 2, 1, 51),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_l_y)));
  }

  c9_k_y = false;
  c9_vlen = (real_T)c9_badPixels->size[0];
  c9_a = c9_vlen;
  c9_c = (int32_T)c9_a;
  c9_b_a = c9_c - 1;
  c9_vspread = c9_b_a;
  c9_d_b = c9_vspread;
  c9_i2 = c9_d_b;
  c9_c_a = c9_i2 + 1;
  c9_i2 = c9_c_a;
  c9_e_b = c9_i2;
  c9_f_b = c9_e_b;
  if (1 > c9_f_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_f_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  c9_ix = 0;
  exitg1 = false;
  while ((!exitg1) && (c9_ix + 1 <= c9_i2)) {
    if (!c9_badPixels->data[c9_ix]) {
      c9_b84 = true;
    } else {
      c9_b84 = false;
    }

    if (!c9_b84) {
      c9_k_y = true;
      exitg1 = true;
    } else {
      c9_ix++;
    }
  }

  if (c9_k_y) {
    c9_numelBadPix = (real_T)c9_badPixels->size[0];
    c9_d_warning(chartInstance);
    c9_i981 = (int32_T)((1.0 + (-1.0 - c9_numelBadPix)) / -1.0);
    _SFD_FOR_LOOP_VECTOR_CHECK(c9_numelBadPix, -1.0, 1.0, mxDOUBLE_CLASS,
      c9_i981);
    for (c9_s = 0; c9_s < c9_i981; c9_s++) {
      c9_b_s = c9_numelBadPix + -(real_T)c9_s;
      if (c9_badPixels->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c9_b_s, 1, c9_badPixels->size[0]) - 1]) {
        c9_idx = (int32_T)c9_b_s;
        c9_b_idx = c9_idx;
        c9_c_idx = c9_b_idx;
        c9_d40 = (real_T)c9_locationsVar->size[0];
        c9_n = (int32_T)c9_d40;
        c9_k_p = true;
        if (c9_c_idx > c9_n) {
          c9_k_p = false;
        } else {
          c9_h_x = c9_c_idx;
          c9_i_x = c9_h_x;
          if (c9_c_idx != c9_i_x) {
            c9_k_p = false;
          }
        }

        if (c9_k_p) {
        } else {
          c9_m_y = NULL;
          sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv15, 10, 0U, 1U, 0U, 2,
            1, 25), false);
          c9_n_y = NULL;
          sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv15, 10, 0U, 1U, 0U, 2,
            1, 25), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c9_m_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c9_n_y)));
        }

        c9_d_idx = c9_idx;
        c9_nrowx = c9_locationsVar->size[0];
        c9_nrows = c9_nrowx - 1;
        c9_d_a = c9_d_idx;
        c9_g_b = c9_nrows;
        c9_e_a = c9_d_a;
        c9_h_b = c9_g_b;
        if (c9_e_a > c9_h_b) {
          c9_b_overflow = false;
        } else {
          c9_b_overflow = (c9_h_b > 2147483646);
        }

        if (c9_b_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_i = c9_d_idx; c9_i <= c9_nrows; c9_i++) {
          c9_b_locationsVar[0] = c9_locationsVar->size[0];
          c9_b_locationsVar[1] = 1;
          c9_locationsVar->data[c9_i - 1] = c9_locationsVar->data[c9_i];
        }

        if (c9_nrows <= c9_nrowx) {
        } else {
          c9_o_y = NULL;
          sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          c9_p_y = NULL;
          sf_mex_assign(&c9_p_y, sf_mex_create("y", c9_cv2, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c9_o_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c9_p_y)));
        }

        c9_b87 = (1 > c9_nrows);
        if (c9_b87) {
          c9_i996 = 0;
        } else {
          c9_i996 = c9_nrows;
        }

        c9_i998 = c9_locationsVar->size[0];
        c9_locationsVar->size[0] = c9_i996;
        c9_emxEnsureCapacity_real_T1(chartInstance, c9_locationsVar, c9_i998,
          &c9_lj_emlrtRTEI);
      }
    }
  }

  c9_emxFree_boolean_T(chartInstance, &c9_badPixels);
  c9_b85 = (c9_varargin_1->size[0] == 0);
  c9_b86 = (c9_varargin_1->size[1] == 0);
  if (c9_b85 || c9_b86) {
  } else {
    c9_emxInit_boolean_T(chartInstance, &c9_mask, 2, &c9_tj_emlrtRTEI);
    c9_i982 = c9_mask->size[0] * c9_mask->size[1];
    c9_mask->size[0] = c9_varargin_1->size[0];
    c9_mask->size[1] = c9_varargin_1->size[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_mask, c9_i982,
      &c9_nj_emlrtRTEI);
    c9_i983 = c9_mask->size[0];
    c9_i984 = c9_mask->size[1];
    c9_g_loop_ub = c9_varargin_1->size[0] * c9_varargin_1->size[1] - 1;
    for (c9_i985 = 0; c9_i985 <= c9_g_loop_ub; c9_i985++) {
      c9_mask->data[c9_i985] = c9_varargin_1->data[c9_i985];
    }

    c9_i986 = c9_mask->size[0] * c9_mask->size[1];
    c9_i987 = c9_mask->size[0] * c9_mask->size[1];
    c9_mask->size[0];
    c9_mask->size[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_mask, c9_i987,
      &c9_nj_emlrtRTEI);
    c9_i988 = c9_mask->size[0];
    c9_i989 = c9_mask->size[1];
    c9_i990 = c9_i986;
    c9_h_loop_ub = c9_i990 - 1;
    for (c9_i991 = 0; c9_i991 <= c9_h_loop_ub; c9_i991++) {
      c9_mask->data[c9_i991] = !c9_mask->data[c9_i991];
    }

    c9_emxInit_boolean_T(chartInstance, &c9_marker, 2, &c9_uj_emlrtRTEI);
    c9_i992 = c9_marker->size[0] * c9_marker->size[1];
    c9_marker->size[0] = c9_mask->size[0];
    c9_marker->size[1] = c9_mask->size[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_marker, c9_i992,
      &c9_oj_emlrtRTEI);
    c9_i993 = c9_marker->size[0];
    c9_i994 = c9_marker->size[1];
    c9_i_loop_ub = c9_mask->size[0] * c9_mask->size[1] - 1;
    for (c9_i995 = 0; c9_i995 <= c9_i_loop_ub; c9_i995++) {
      c9_marker->data[c9_i995] = false;
    }

    c9_emxInit_int32_T(chartInstance, &c9_r25, 1, &c9_sg_emlrtRTEI);
    c9_b_marker = c9_marker->size[0] * c9_marker->size[1];
    c9_i997 = c9_r25->size[0];
    c9_r25->size[0] = c9_locationsVar->size[0];
    c9_emxEnsureCapacity_int32_T(chartInstance, c9_r25, c9_i997,
      &c9_pj_emlrtRTEI);
    c9_j_loop_ub = c9_locationsVar->size[0] - 1;
    for (c9_i999 = 0; c9_i999 <= c9_j_loop_ub; c9_i999++) {
      c9_r25->data[c9_i999] = sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c9_locationsVar->data[c9_i999], 1, c9_b_marker);
    }

    c9_b_mask = c9_mask->size[0] * c9_mask->size[1];
    c9_i1000 = c9_r24->size[0];
    c9_r24->size[0] = c9_locationsVar->size[0];
    c9_emxEnsureCapacity_boolean_T1(chartInstance, c9_r24, c9_i1000,
      &c9_qj_emlrtRTEI);
    c9_k_loop_ub = c9_locationsVar->size[0] - 1;
    for (c9_i1001 = 0; c9_i1001 <= c9_k_loop_ub; c9_i1001++) {
      c9_r24->data[c9_i1001] = c9_mask->data[sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c9_locationsVar->data[c9_i1001], 1, c9_b_mask) - 1];
    }

    _SFD_SUB_ASSIGN_SIZE_CHECK_1D(c9_r25->size[0], c9_r24->size[0]);
    c9_i1002 = c9_marker->size[0];
    c9_i1003 = c9_marker->size[1];
    c9_l_loop_ub = c9_r24->size[0] - 1;
    for (c9_i1004 = 0; c9_i1004 <= c9_l_loop_ub; c9_i1004++) {
      c9_marker->data[c9_r25->data[c9_i1004] - 1] = c9_r24->data[c9_i1004];
    }

    c9_emxFree_int32_T(chartInstance, &c9_r25);
    for (c9_i1005 = 0; c9_i1005 < 2; c9_i1005++) {
      c9_imSizeT[c9_i1005] = (real_T)c9_marker->size[c9_i1005];
    }

    for (c9_i1006 = 0; c9_i1006 < 2; c9_i1006++) {
      c9_c_mask[c9_i1006] = (real_T)c9_mask->size[c9_i1006];
    }

    for (c9_i1007 = 0; c9_i1007 < 2; c9_i1007++) {
      c9_imSizeT[c9_i1007];
    }

    for (c9_i1008 = 0; c9_i1008 < 2; c9_i1008++) {
      c9_c_mask[c9_i1008];
    }

    for (c9_i1009 = 0; c9_i1009 < 2; c9_i1009++) {
      c9_c_mask[c9_i1009];
    }

    c9_l_p = false;
    for (c9_i1010 = 0; c9_i1010 < 2; c9_i1010++) {
      c9_c_mask[c9_i1010];
    }

    c9_m_p = true;
    c9_e_k = 0;
    exitg1 = false;
    while ((!exitg1) && (c9_e_k < 2)) {
      c9_f_k = 1.0 + (real_T)c9_e_k;
      c9_b_x1 = c9_imSizeT[(int32_T)c9_f_k - 1];
      c9_x2 = c9_c_mask[(int32_T)c9_f_k - 1];
      c9_n_p = (c9_b_x1 == c9_x2);
      if (!c9_n_p) {
        c9_m_p = false;
        exitg1 = true;
      } else {
        c9_e_k++;
      }
    }

    if (!c9_m_p) {
    } else {
      c9_l_p = true;
    }

    if (c9_l_p) {
    } else {
      c9_q_y = NULL;
      sf_mex_assign(&c9_q_y, sf_mex_create("y", c9_cv82, 10, 0U, 1U, 0U, 2, 1,
        32), false);
      c9_r_y = NULL;
      sf_mex_assign(&c9_r_y, sf_mex_create("y", c9_cv82, 10, 0U, 1U, 0U, 2, 1,
        32), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_q_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c9_r_y)));
    }

    for (c9_i1011 = 0; c9_i1011 < 2; c9_i1011++) {
      c9_imSizeT[c9_i1011] = (real_T)c9_marker->size[c9_i1011];
    }

    for (c9_i1012 = 0; c9_i1012 < 2; c9_i1012++) {
      c9_imSizeT[c9_i1012];
    }

    ippreconstruct_uint8(&c9_marker->data[0], &c9_mask->data[0], c9_imSizeT, 2.0);
    c9_emxFree_boolean_T(chartInstance, &c9_mask);
    for (c9_i1013 = 0; c9_i1013 < 2; c9_i1013++) {
      c9_b_varargin_1[c9_i1013] = c9_varargin_1->size[c9_i1013];
    }

    for (c9_i1014 = 0; c9_i1014 < 2; c9_i1014++) {
      c9_c_marker[c9_i1014] = c9_marker->size[c9_i1014];
    }

    _SFD_SIZE_EQ_CHECK_ND(c9_b_varargin_1, c9_c_marker, 2);
    c9_i1015 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
    c9_i1016 = c9_marker->size[0] * c9_marker->size[1];
    c9_i1017 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
    c9_i1018 = c9_marker->size[0] * c9_marker->size[1];
    c9_i1019 = c9_varargin_1->size[0] * c9_varargin_1->size[1];
    c9_varargin_1->size[0];
    c9_varargin_1->size[1];
    c9_emxEnsureCapacity_boolean_T(chartInstance, c9_varargin_1, c9_i1019,
      &c9_rj_emlrtRTEI);
    c9_m_loop_ub = c9_varargin_1->size[1] - 1;
    for (c9_i1020 = 0; c9_i1020 <= c9_m_loop_ub; c9_i1020++) {
      c9_n_loop_ub = c9_varargin_1->size[0] - 1;
      for (c9_i1021 = 0; c9_i1021 <= c9_n_loop_ub; c9_i1021++) {
        c9_varargin_1->data[c9_i1021 + c9_varargin_1->size[0] * c9_i1020] =
          (c9_varargin_1->data[c9_i1021 + c9_varargin_1->size[0] * c9_i1020] ||
           c9_marker->data[c9_i1021 + c9_marker->size[0] * c9_i1020]);
      }
    }

    c9_emxFree_boolean_T(chartInstance, &c9_marker);
  }

  c9_emxFree_boolean_T(chartInstance, &c9_r24);
  c9_emxFree_real_T(chartInstance, &c9_locationsVar);
}

static void c9_b_parseInputs(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T *c9_varargin_1, c9_emxArray_real_T *c9_varargin_3,
  c9_emxArray_real_T *c9_theta, c9_emxArray_real_T *c9_rho)
{
  boolean_T c9_b88;
  boolean_T c9_b89;
  const mxArray *c9_y = NULL;
  static char_T c9_cv83[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N', 'o', 'n', 'e',
    'm', 'p', 't', 'y' };

  real_T c9_N;
  const mxArray *c9_b_y = NULL;
  real_T c9_M;
  int32_T c9_i1022;
  const mxArray *c9_c_y = NULL;
  int32_T c9_i1023;
  int32_T c9_i1024;
  int32_T c9_loop_ub;
  int32_T c9_i1025;
  boolean_T c9_b90;
  const mxArray *c9_d_y = NULL;
  static char_T c9_cv84[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N', 'o', 'n', 'e',
    'm', 'p', 't', 'y' };

  c9_emxArray_real_T *c9_b_varargin_3;
  const mxArray *c9_e_y = NULL;
  int32_T c9_i1026;
  const mxArray *c9_f_y = NULL;
  static char_T c9_cv85[5] = { 'T', 'h', 'e', 't', 'a' };

  int32_T c9_i1027;
  int32_T c9_i1028;
  int32_T c9_b_loop_ub;
  int32_T c9_i1029;
  boolean_T c9_b91;
  const mxArray *c9_g_y = NULL;
  static char_T c9_cv86[27] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'F', 'i', 'n', 'i',
    't', 'e' };

  c9_emxArray_real_T *c9_b_varargin_1;
  const mxArray *c9_h_y = NULL;
  int32_T c9_i1030;
  const mxArray *c9_i_y = NULL;
  static char_T c9_cv87[5] = { 'T', 'h', 'e', 't', 'a' };

  int32_T c9_c_loop_ub;
  int32_T c9_i1031;
  boolean_T c9_b92;
  const mxArray *c9_j_y = NULL;
  const mxArray *c9_k_y = NULL;
  const mxArray *c9_l_y = NULL;
  int32_T c9_n;
  const mxArray *c9_m_y = NULL;
  c9_emxArray_real_T *c9_c_varargin_1;
  int32_T c9_i1032;
  real_T c9_minTheta;
  real_T c9_x;
  boolean_T c9_b;
  int32_T c9_d_loop_ub;
  int32_T c9_i1033;
  int32_T c9_i1034;
  real_T c9_b_x;
  boolean_T c9_b_b;
  int32_T c9_e_loop_ub;
  int32_T c9_idx;
  int32_T c9_i1035;
  int32_T c9_first;
  int32_T c9_last;
  real_T c9_ex;
  boolean_T c9_b93;
  int32_T c9_i1036;
  int32_T c9_a;
  int32_T c9_c_b;
  const mxArray *c9_n_y = NULL;
  int32_T c9_b_a;
  int32_T c9_d_b;
  const mxArray *c9_o_y = NULL;
  const mxArray *c9_p_y = NULL;
  int32_T c9_b_n;
  boolean_T c9_overflow;
  const mxArray *c9_q_y = NULL;
  c9_emxArray_real_T *c9_d_varargin_1;
  int32_T c9_i1037;
  real_T c9_maxTheta;
  real_T c9_c_x;
  int32_T c9_k;
  boolean_T c9_e_b;
  int32_T c9_f_loop_ub;
  int32_T c9_i1038;
  real_T c9_d_x;
  boolean_T c9_f_b;
  const mxArray *c9_r_y = NULL;
  static char_T c9_cv88[28] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd', 'T', 'h', 'e', 't', 'a',
    'M', 'i', 'n' };

  int32_T c9_b_idx;
  const mxArray *c9_s_y = NULL;
  const mxArray *c9_t_y = NULL;
  static char_T c9_cv89[28] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd', 'T', 'h', 'e', 't', 'a',
    'M', 'a', 'x' };

  real_T c9_b_M;
  const mxArray *c9_u_y = NULL;
  const mxArray *c9_v_y = NULL;
  real_T c9_b_N;
  int32_T c9_b_first;
  static char_T c9_cv90[5] = { 'T', 'h', 'e', 't', 'a' };

  real_T c9_normSquared;
  int32_T c9_b_last;
  const mxArray *c9_w_y = NULL;
  real_T c9_b_ex;
  const mxArray *c9_x_y = NULL;
  int32_T c9_i1039;
  static char_T c9_cv91[26] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd', 'R', 'h', 'o', 'R', 'e',
    's' };

  real_T c9_c_a;
  int32_T c9_d_a;
  const mxArray *c9_y_y = NULL;
  real_T c9_e_a;
  int32_T c9_g_b;
  real_T c9_f_a;
  int32_T c9_g_a;
  const mxArray *c9_ab_y = NULL;
  real_T c9_e_x;
  int32_T c9_h_b;
  static char_T c9_cv92[13] = { 'R', 'h', 'o', 'R', 'e', 's', 'o', 'l', 'u', 't',
    'i', 'o', 'n' };

  real_T c9_h_a;
  real_T c9_c;
  boolean_T c9_b_overflow;
  real_T c9_i_a;
  real_T c9_j_a;
  real_T c9_k_a;
  real_T c9_f_x;
  int32_T c9_b_k;
  real_T c9_l_a;
  real_T c9_b_c;
  real_T c9_g_x;
  real_T c9_D;
  real_T c9_h_x;
  boolean_T c9_p;
  boolean_T c9_b_p;
  real_T c9_i_x;
  real_T c9_q;
  real_T c9_nrho;
  real_T c9_d1;
  real_T c9_d2;
  real_T c9_n1;
  real_T c9_j_x;
  real_T c9_k_x;
  real_T c9_dv7[2];
  int32_T c9_i1040;
  int32_T c9_c_n;
  real_T c9_delta1;
  real_T c9_d41;
  int32_T c9_i1041;
  real_T c9_delta2;
  int32_T c9_c_k;
  real_T c9_d42;
  int32_T c9_i1042;
  int32_T c9_d_k;
  real_T c9_e_k;
  c9_b88 = (c9_varargin_1->size[0] == 0);
  c9_b89 = (c9_varargin_1->size[1] == 0);
  if ((!c9_b88) && (!c9_b89)) {
  } else {
    c9_y = NULL;
    sf_mex_assign(&c9_y, sf_mex_create("y", c9_cv83, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv13, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv14, 10, 0U, 1U, 0U, 2, 1, 19),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c9_b_y, 14, c9_c_y)));
  }

  c9_N = (real_T)c9_varargin_1->size[1];
  c9_M = (real_T)c9_varargin_1->size[0];
  c9_i1022 = c9_theta->size[0] * c9_theta->size[1];
  c9_theta->size[0] = 1;
  c9_theta->size[1] = c9_varargin_3->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_theta, c9_i1022,
    &c9_vj_emlrtRTEI);
  c9_i1023 = c9_theta->size[0];
  c9_i1024 = c9_theta->size[1];
  c9_loop_ub = c9_varargin_3->size[0] * c9_varargin_3->size[1] - 1;
  for (c9_i1025 = 0; c9_i1025 <= c9_loop_ub; c9_i1025++) {
    c9_theta->data[c9_i1025] = c9_varargin_3->data[c9_i1025];
  }

  c9_b90 = (c9_varargin_3->size[1] == 0);
  if (!c9_b90) {
  } else {
    c9_d_y = NULL;
    sf_mex_assign(&c9_d_y, sf_mex_create("y", c9_cv84, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c9_e_y = NULL;
    sf_mex_assign(&c9_e_y, sf_mex_create("y", c9_cv13, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c9_f_y = NULL;
    sf_mex_assign(&c9_f_y, sf_mex_create("y", c9_cv85, 10, 0U, 1U, 0U, 2, 1, 5),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_e_y, 14, c9_f_y)));
  }

  c9_emxInit_real_T(chartInstance, &c9_b_varargin_3, 2, &c9_bh_emlrtRTEI);
  c9_i1026 = c9_b_varargin_3->size[0] * c9_b_varargin_3->size[1];
  c9_b_varargin_3->size[0] = 1;
  c9_b_varargin_3->size[1] = c9_varargin_3->size[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_b_varargin_3, c9_i1026,
    &c9_bh_emlrtRTEI);
  c9_i1027 = c9_b_varargin_3->size[0];
  c9_i1028 = c9_b_varargin_3->size[1];
  c9_b_loop_ub = c9_varargin_3->size[0] * c9_varargin_3->size[1] - 1;
  for (c9_i1029 = 0; c9_i1029 <= c9_b_loop_ub; c9_i1029++) {
    c9_b_varargin_3->data[c9_i1029] = c9_varargin_3->data[c9_i1029];
  }

  if (c9_b_all(chartInstance, c9_b_varargin_3)) {
    c9_b91 = true;
  } else {
    c9_b91 = false;
  }

  c9_emxFree_real_T(chartInstance, &c9_b_varargin_3);
  if (c9_b91) {
  } else {
    c9_g_y = NULL;
    sf_mex_assign(&c9_g_y, sf_mex_create("y", c9_cv86, 10, 0U, 1U, 0U, 2, 1, 27),
                  false);
    c9_h_y = NULL;
    sf_mex_assign(&c9_h_y, sf_mex_create("y", c9_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c9_i_y = NULL;
    sf_mex_assign(&c9_i_y, sf_mex_create("y", c9_cv87, 10, 0U, 1U, 0U, 2, 1, 5),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_h_y, 14, c9_i_y)));
  }

  c9_emxInit_real_T1(chartInstance, &c9_b_varargin_1, 1, &c9_wj_emlrtRTEI);
  c9_i1030 = c9_b_varargin_1->size[0];
  c9_b_varargin_1->size[0] = c9_varargin_3->size[1];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_varargin_1, c9_i1030,
    &c9_wj_emlrtRTEI);
  c9_c_loop_ub = c9_varargin_3->size[1] - 1;
  for (c9_i1031 = 0; c9_i1031 <= c9_c_loop_ub; c9_i1031++) {
    c9_b_varargin_1->data[c9_i1031] = c9_varargin_3->data[c9_i1031];
  }

  if ((c9_b_varargin_1->size[0] == 1) || ((real_T)c9_b_varargin_1->size[0] !=
       1.0)) {
    c9_b92 = true;
  } else {
    c9_b92 = false;
  }

  if (c9_b92) {
  } else {
    c9_j_y = NULL;
    sf_mex_assign(&c9_j_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c9_k_y = NULL;
    sf_mex_assign(&c9_k_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_k_y)));
  }

  if ((real_T)c9_b_varargin_1->size[0] >= 1.0) {
  } else {
    c9_l_y = NULL;
    sf_mex_assign(&c9_l_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    c9_m_y = NULL;
    sf_mex_assign(&c9_m_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_l_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_m_y)));
  }

  c9_n = c9_b_varargin_1->size[0];
  if (c9_n <= 2) {
    if (c9_n == 1) {
      c9_minTheta = c9_b_varargin_1->data[0];
    } else if (c9_b_varargin_1->data[0] > c9_b_varargin_1->data[1]) {
      c9_minTheta = c9_b_varargin_1->data[1];
    } else {
      c9_x = c9_b_varargin_1->data[0];
      c9_b = muDoubleScalarIsNaN(c9_x);
      if (c9_b) {
        c9_b_x = c9_b_varargin_1->data[1];
        c9_b_b = muDoubleScalarIsNaN(c9_b_x);
        if (!c9_b_b) {
          c9_minTheta = c9_b_varargin_1->data[1];
        } else {
          c9_minTheta = c9_b_varargin_1->data[0];
        }
      } else {
        c9_minTheta = c9_b_varargin_1->data[0];
      }
    }
  } else {
    c9_emxInit_real_T1(chartInstance, &c9_c_varargin_1, 1, &c9_qb_emlrtRTEI);
    c9_i1032 = c9_c_varargin_1->size[0];
    c9_c_varargin_1->size[0] = c9_b_varargin_1->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_c_varargin_1, c9_i1032,
      &c9_qb_emlrtRTEI);
    c9_d_loop_ub = c9_b_varargin_1->size[0] - 1;
    for (c9_i1034 = 0; c9_i1034 <= c9_d_loop_ub; c9_i1034++) {
      c9_c_varargin_1->data[c9_i1034] = c9_b_varargin_1->data[c9_i1034];
    }

    c9_idx = c9_findFirst(chartInstance, c9_c_varargin_1);
    c9_emxFree_real_T(chartInstance, &c9_c_varargin_1);
    if (c9_idx == 0) {
      c9_minTheta = c9_b_varargin_1->data[0];
    } else {
      c9_first = c9_idx - 1;
      c9_last = c9_n;
      c9_ex = c9_b_varargin_1->data[c9_first];
      c9_i1036 = c9_first + 2;
      c9_a = c9_i1036;
      c9_c_b = c9_last;
      c9_b_a = c9_a;
      c9_d_b = c9_c_b;
      if (c9_b_a > c9_d_b) {
        c9_overflow = false;
      } else {
        c9_overflow = (c9_d_b > 2147483646);
      }

      if (c9_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }

      for (c9_k = c9_i1036 - 1; c9_k < c9_last; c9_k++) {
        if (c9_ex > c9_b_varargin_1->data[c9_k]) {
          c9_ex = c9_b_varargin_1->data[c9_k];
        }
      }

      c9_minTheta = c9_ex;
    }
  }

  c9_i1033 = c9_b_varargin_1->size[0];
  c9_b_varargin_1->size[0] = c9_varargin_3->size[1];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_b_varargin_1, c9_i1033,
    &c9_xj_emlrtRTEI);
  c9_e_loop_ub = c9_varargin_3->size[1] - 1;
  for (c9_i1035 = 0; c9_i1035 <= c9_e_loop_ub; c9_i1035++) {
    c9_b_varargin_1->data[c9_i1035] = c9_varargin_3->data[c9_i1035];
  }

  if ((c9_b_varargin_1->size[0] == 1) || ((real_T)c9_b_varargin_1->size[0] !=
       1.0)) {
    c9_b93 = true;
  } else {
    c9_b93 = false;
  }

  if (c9_b93) {
  } else {
    c9_n_y = NULL;
    sf_mex_assign(&c9_n_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c9_o_y = NULL;
    sf_mex_assign(&c9_o_y, sf_mex_create("y", c9_cv0, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_n_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_o_y)));
  }

  if ((real_T)c9_b_varargin_1->size[0] >= 1.0) {
  } else {
    c9_p_y = NULL;
    sf_mex_assign(&c9_p_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    c9_q_y = NULL;
    sf_mex_assign(&c9_q_y, sf_mex_create("y", c9_cv1, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_p_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_q_y)));
  }

  c9_b_n = c9_b_varargin_1->size[0];
  if (c9_b_n <= 2) {
    if (c9_b_n == 1) {
      c9_maxTheta = c9_b_varargin_1->data[0];
    } else if (c9_b_varargin_1->data[0] < c9_b_varargin_1->data[1]) {
      c9_maxTheta = c9_b_varargin_1->data[1];
    } else {
      c9_c_x = c9_b_varargin_1->data[0];
      c9_e_b = muDoubleScalarIsNaN(c9_c_x);
      if (c9_e_b) {
        c9_d_x = c9_b_varargin_1->data[1];
        c9_f_b = muDoubleScalarIsNaN(c9_d_x);
        if (!c9_f_b) {
          c9_maxTheta = c9_b_varargin_1->data[1];
        } else {
          c9_maxTheta = c9_b_varargin_1->data[0];
        }
      } else {
        c9_maxTheta = c9_b_varargin_1->data[0];
      }
    }
  } else {
    c9_emxInit_real_T1(chartInstance, &c9_d_varargin_1, 1, &c9_qb_emlrtRTEI);
    c9_i1037 = c9_d_varargin_1->size[0];
    c9_d_varargin_1->size[0] = c9_b_varargin_1->size[0];
    c9_emxEnsureCapacity_real_T1(chartInstance, c9_d_varargin_1, c9_i1037,
      &c9_qb_emlrtRTEI);
    c9_f_loop_ub = c9_b_varargin_1->size[0] - 1;
    for (c9_i1038 = 0; c9_i1038 <= c9_f_loop_ub; c9_i1038++) {
      c9_d_varargin_1->data[c9_i1038] = c9_b_varargin_1->data[c9_i1038];
    }

    c9_b_idx = c9_findFirst(chartInstance, c9_d_varargin_1);
    c9_emxFree_real_T(chartInstance, &c9_d_varargin_1);
    if (c9_b_idx == 0) {
      c9_maxTheta = c9_b_varargin_1->data[0];
    } else {
      c9_b_first = c9_b_idx - 1;
      c9_b_last = c9_b_n;
      c9_b_ex = c9_b_varargin_1->data[c9_b_first];
      c9_i1039 = c9_b_first + 2;
      c9_d_a = c9_i1039;
      c9_g_b = c9_b_last;
      c9_g_a = c9_d_a;
      c9_h_b = c9_g_b;
      if (c9_g_a > c9_h_b) {
        c9_b_overflow = false;
      } else {
        c9_b_overflow = (c9_h_b > 2147483646);
      }

      if (c9_b_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }

      for (c9_b_k = c9_i1039 - 1; c9_b_k < c9_b_last; c9_b_k++) {
        if (c9_b_ex < c9_b_varargin_1->data[c9_b_k]) {
          c9_b_ex = c9_b_varargin_1->data[c9_b_k];
        }
      }

      c9_maxTheta = c9_b_ex;
    }
  }

  c9_emxFree_real_T(chartInstance, &c9_b_varargin_1);
  if (!(c9_minTheta < -90.0)) {
  } else {
    c9_r_y = NULL;
    sf_mex_assign(&c9_r_y, sf_mex_create("y", c9_cv88, 10, 0U, 1U, 0U, 2, 1, 28),
                  false);
    c9_s_y = NULL;
    sf_mex_assign(&c9_s_y, sf_mex_create("y", c9_cv88, 10, 0U, 1U, 0U, 2, 1, 28),
                  false);
    c9_u_y = NULL;
    sf_mex_assign(&c9_u_y, sf_mex_create("y", c9_cv90, 10, 0U, 1U, 0U, 2, 1, 5),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_r_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_s_y, 14, c9_u_y)));
  }

  if (!(c9_maxTheta >= 90.0)) {
  } else {
    c9_t_y = NULL;
    sf_mex_assign(&c9_t_y, sf_mex_create("y", c9_cv89, 10, 0U, 1U, 0U, 2, 1, 28),
                  false);
    c9_v_y = NULL;
    sf_mex_assign(&c9_v_y, sf_mex_create("y", c9_cv89, 10, 0U, 1U, 0U, 2, 1, 28),
                  false);
    c9_w_y = NULL;
    sf_mex_assign(&c9_w_y, sf_mex_create("y", c9_cv90, 10, 0U, 1U, 0U, 2, 1, 5),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_t_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_v_y, 14, c9_w_y)));
  }

  c9_b_M = c9_M;
  c9_b_N = c9_N;
  c9_normSquared = c9_b_N * c9_b_N + c9_b_M * c9_b_M;
  if (!(1.0 >= c9_normSquared)) {
  } else {
    c9_x_y = NULL;
    sf_mex_assign(&c9_x_y, sf_mex_create("y", c9_cv91, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    c9_y_y = NULL;
    sf_mex_assign(&c9_y_y, sf_mex_create("y", c9_cv91, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    c9_ab_y = NULL;
    sf_mex_assign(&c9_ab_y, sf_mex_create("y", c9_cv92, 10, 0U, 1U, 0U, 2, 1, 13),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_x_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c9_y_y, 14, c9_ab_y)));
  }

  c9_c_a = c9_M - 1.0;
  c9_e_a = c9_c_a;
  c9_f_a = c9_e_a;
  c9_e_x = c9_f_a;
  c9_h_a = c9_e_x;
  c9_c = c9_h_a * c9_h_a;
  c9_i_a = c9_N - 1.0;
  c9_j_a = c9_i_a;
  c9_k_a = c9_j_a;
  c9_f_x = c9_k_a;
  c9_l_a = c9_f_x;
  c9_b_c = c9_l_a * c9_l_a;
  c9_g_x = c9_c + c9_b_c;
  c9_D = c9_g_x;
  c9_h_x = c9_D;
  if (c9_h_x < 0.0) {
    c9_p = true;
  } else {
    c9_p = false;
  }

  c9_b_p = c9_p;
  if (c9_b_p) {
    c9_error(chartInstance);
  }

  c9_D = muDoubleScalarSqrt(c9_D);
  c9_i_x = c9_D;
  c9_q = c9_i_x;
  c9_q = muDoubleScalarCeil(c9_q);
  c9_nrho = 2.0 * c9_q + 1.0;
  c9_d1 = -c9_q;
  c9_d2 = c9_q;
  c9_n1 = c9_nrho;
  if (c9_n1 < 0.0) {
    c9_n1 = 0.0;
  }

  c9_j_x = c9_n1;
  c9_k_x = c9_j_x;
  c9_k_x = muDoubleScalarFloor(c9_k_x);
  c9_dv7[0] = 1.0;
  c9_dv7[1] = _SFD_NON_NEGATIVE_CHECK("", c9_k_x);
  c9_i1040 = c9_rho->size[0] * c9_rho->size[1];
  c9_rho->size[0] = 1;
  c9_rho->size[1] = (int32_T)c9_dv7[1];
  c9_emxEnsureCapacity_real_T(chartInstance, c9_rho, c9_i1040, &c9_yj_emlrtRTEI);
  c9_c_n = c9_rho->size[1] - 1;
  if ((real_T)c9_rho->size[1] >= 1.0) {
    c9_rho->data[c9_c_n] = c9_d2;
    if ((real_T)c9_rho->size[1] >= 2.0) {
      c9_rho->data[0] = c9_d1;
      if ((real_T)c9_rho->size[1] >= 3.0) {
        if (((c9_d1 < 0.0) != (c9_d2 < 0.0)) && ((muDoubleScalarAbs(c9_d1) >
              8.9884656743115785E+307) || (muDoubleScalarAbs(c9_d2) >
              8.9884656743115785E+307))) {
          c9_delta1 = c9_d1 / ((real_T)c9_rho->size[1] - 1.0);
          c9_delta2 = c9_d2 / ((real_T)c9_rho->size[1] - 1.0);
          c9_d42 = (real_T)c9_rho->size[1] - 2.0;
          c9_i1042 = (int32_T)c9_d42 - 1;
          for (c9_d_k = 0; c9_d_k <= c9_i1042; c9_d_k++) {
            c9_e_k = 1.0 + (real_T)c9_d_k;
            c9_rho->data[(int32_T)(c9_e_k + 1.0) - 1] = (c9_d1 + c9_delta2 *
              c9_e_k) - c9_delta1 * c9_e_k;
          }
        } else {
          c9_delta1 = (c9_d2 - c9_d1) / ((real_T)c9_rho->size[1] - 1.0);
          c9_d41 = (real_T)c9_rho->size[1] - 2.0;
          c9_i1041 = (int32_T)c9_d41 - 1;
          for (c9_c_k = 0; c9_c_k <= c9_i1041; c9_c_k++) {
            c9_e_k = 1.0 + (real_T)c9_c_k;
            c9_rho->data[(int32_T)(c9_e_k + 1.0) - 1] = c9_d1 + c9_e_k *
              c9_delta1;
          }
        }
      }
    }
  }
}

static void c9_b_sortrows(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_y, real_T c9_varargin_1[2])
{
  int32_T c9_k;
  boolean_T c9_p;
  real_T c9_b_k;
  real_T c9_x;
  real_T c9_b_x;
  const mxArray *c9_b_y = NULL;
  real_T c9_c_x;
  static char_T c9_cv93[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'i', 's', 's',
    'o', 'r', 't', 'e', 'd', 'r', 'o', 'w', 's', ':', 'C', 'o', 'l', 'M', 'i',
    's', 'm', 'a', 't', 'c', 'h', 'X' };

  int32_T c9_i1043;
  real_T c9_ck;
  const mxArray *c9_c_y = NULL;
  real_T c9_d_x;
  real_T c9_e_x;
  c9_emxArray_int32_T *c9_idx;
  int32_T c9_col[2];
  int32_T c9_n;
  int32_T c9_i1044;
  int32_T c9_loop_ub;
  int32_T c9_i1045;
  c9_emxArray_int32_T *c9_iwork;
  int32_T c9_b_n;
  int32_T c9_len;
  int32_T c9_i1046;
  int32_T c9_iv11[1];
  int32_T c9_i1047;
  int32_T c9_np1;
  int32_T c9_i1048;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_c_k;
  c9_emxArray_int32_T *c9_d_y;
  int32_T c9_i1049;
  int32_T c9_i;
  c9_emxArray_int32_T *c9_e_y;
  int32_T c9_i1050;
  int32_T c9_i1051;
  int32_T c9_b_loop_ub;
  int32_T c9_a;
  int32_T c9_i1052;
  int32_T c9_m;
  int32_T c9_i2;
  int32_T c9_j;
  int32_T c9_pEnd;
  int32_T c9_i1053;
  int32_T c9_i1054;
  int32_T c9_b_p;
  int32_T c9_b_col[2];
  int32_T c9_b_j;
  int32_T c9_q;
  int32_T c9_qEnd;
  int32_T c9_c_j;
  int32_T c9_c_b;
  int32_T c9_d_k;
  int32_T c9_d_b;
  int32_T c9_kEnd;
  boolean_T c9_b_overflow;
  int32_T c9_i1055;
  int32_T c9_e_b;
  int32_T c9_f_b;
  int32_T c9_b_i;
  boolean_T c9_c_overflow;
  int32_T c9_i1056;
  int32_T c9_i1057;
  int32_T c9_g_b;
  int32_T c9_c_i;
  int32_T c9_c_loop_ub;
  int32_T c9_h_b;
  int32_T c9_i1058;
  int32_T c9_e_k;
  boolean_T c9_d_overflow;
  int32_T c9_i1059;
  int32_T c9_d_i;
  int32_T c9_c_col[2];
  int32_T exitg1;
  c9_k = 0;
  do {
    exitg1 = 0;
    if (c9_k < 2) {
      c9_b_k = 1.0 + (real_T)c9_k;
      c9_x = c9_varargin_1[(int32_T)c9_b_k - 1];
      c9_b_x = c9_x;
      c9_c_x = c9_b_x;
      c9_ck = muDoubleScalarAbs(c9_c_x);
      c9_d_x = c9_ck;
      c9_e_x = c9_d_x;
      c9_e_x = muDoubleScalarFloor(c9_e_x);
      if ((c9_e_x != c9_ck) || (c9_ck < 1.0) || (c9_ck > 2.0)) {
        c9_p = false;
        exitg1 = 1;
      } else {
        c9_k++;
      }
    } else {
      c9_p = true;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  if (c9_p) {
  } else {
    c9_b_y = NULL;
    sf_mex_assign(&c9_b_y, sf_mex_create("y", c9_cv93, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c9_c_y = NULL;
    sf_mex_assign(&c9_c_y, sf_mex_create("y", c9_cv93, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c9_b_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c9_c_y)));
  }

  for (c9_i1043 = 0; c9_i1043 < 2; c9_i1043++) {
    c9_col[c9_i1043] = (int32_T)c9_varargin_1[c9_i1043];
  }

  c9_emxInit_int32_T(chartInstance, &c9_idx, 1, &c9_o_emlrtRTEI);
  c9_n = c9_y->size[0];
  c9_i1044 = c9_idx->size[0];
  c9_idx->size[0] = c9_y->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_idx, c9_i1044, &c9_o_emlrtRTEI);
  c9_loop_ub = c9_y->size[0] - 1;
  for (c9_i1045 = 0; c9_i1045 <= c9_loop_ub; c9_i1045++) {
    c9_idx->data[c9_i1045] = 0;
  }

  c9_emxInit_int32_T(chartInstance, &c9_iwork, 1, &c9_cc_emlrtRTEI);
  c9_b_n = c9_n;
  c9_len = c9_idx->size[0];
  c9_i1046 = c9_iwork->size[0];
  c9_iwork->size[0] = c9_len;
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i1046,
    &c9_q_emlrtRTEI);
  c9_iv11[0] = c9_iwork->size[0];
  c9_i1047 = c9_iwork->size[0];
  c9_iwork->size[0] = c9_iv11[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i1047,
    &c9_s_emlrtRTEI);
  c9_np1 = c9_b_n + 1;
  c9_i1048 = c9_b_n - 1;
  c9_b = c9_i1048;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483645);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  c9_c_k = 1;
  c9_emxInit_int32_T1(chartInstance, &c9_d_y, 2, &c9_u_emlrtRTEI);
  while (c9_c_k <= c9_i1048) {
    c9_i1049 = c9_d_y->size[0] * c9_d_y->size[1];
    c9_d_y->size[0] = c9_y->size[0];
    c9_d_y->size[1] = 2;
    c9_emxEnsureCapacity_int32_T1(chartInstance, c9_d_y, c9_i1049,
      &c9_u_emlrtRTEI);
    c9_i1050 = c9_d_y->size[0];
    c9_i1051 = c9_d_y->size[1];
    c9_b_loop_ub = c9_y->size[0] * c9_y->size[1] - 1;
    for (c9_i1052 = 0; c9_i1052 <= c9_b_loop_ub; c9_i1052++) {
      c9_d_y->data[c9_i1052] = c9_y->data[c9_i1052];
    }

    for (c9_i1053 = 0; c9_i1053 < 2; c9_i1053++) {
      c9_b_col[c9_i1053] = c9_col[c9_i1053];
    }

    if (c9_b_sortLE(chartInstance, c9_d_y, c9_b_col, c9_c_k, c9_c_k + 1)) {
      c9_idx->data[c9_c_k - 1] = c9_c_k;
      c9_idx->data[c9_c_k] = c9_c_k + 1;
    } else {
      c9_idx->data[c9_c_k - 1] = c9_c_k + 1;
      c9_idx->data[c9_c_k] = c9_c_k;
    }

    c9_c_k += 2;
  }

  c9_emxFree_int32_T(chartInstance, &c9_d_y);
  if ((c9_b_n & 1) != 0) {
    c9_idx->data[c9_b_n - 1] = c9_b_n;
  }

  c9_i = 2;
  c9_emxInit_int32_T1(chartInstance, &c9_e_y, 2, &c9_u_emlrtRTEI);
  while (c9_i < c9_b_n) {
    c9_a = c9_i;
    c9_i2 = c9_a << 1;
    c9_j = 1;
    for (c9_pEnd = 1 + c9_i; c9_pEnd < c9_np1; c9_pEnd = c9_qEnd + c9_i) {
      c9_b_p = c9_j - 1;
      c9_q = c9_pEnd - 1;
      c9_qEnd = c9_j + c9_i2;
      if (c9_qEnd > c9_np1) {
        c9_qEnd = c9_np1;
      }

      c9_d_k = 0;
      c9_kEnd = c9_qEnd - c9_j;
      while (c9_d_k + 1 <= c9_kEnd) {
        c9_i1055 = c9_e_y->size[0] * c9_e_y->size[1];
        c9_e_y->size[0] = c9_y->size[0];
        c9_e_y->size[1] = 2;
        c9_emxEnsureCapacity_int32_T1(chartInstance, c9_e_y, c9_i1055,
          &c9_u_emlrtRTEI);
        c9_i1056 = c9_e_y->size[0];
        c9_i1057 = c9_e_y->size[1];
        c9_c_loop_ub = c9_y->size[0] * c9_y->size[1] - 1;
        for (c9_i1058 = 0; c9_i1058 <= c9_c_loop_ub; c9_i1058++) {
          c9_e_y->data[c9_i1058] = c9_y->data[c9_i1058];
        }

        for (c9_i1059 = 0; c9_i1059 < 2; c9_i1059++) {
          c9_c_col[c9_i1059] = c9_col[c9_i1059];
        }

        if (c9_b_sortLE(chartInstance, c9_e_y, c9_c_col, c9_idx->data[c9_b_p],
                        c9_idx->data[c9_q])) {
          c9_iwork->data[c9_d_k] = c9_idx->data[c9_b_p];
          c9_b_p++;
          if (c9_b_p + 1 == c9_pEnd) {
            while (c9_q + 1 < c9_qEnd) {
              c9_d_k++;
              c9_iwork->data[c9_d_k] = c9_idx->data[c9_q];
              c9_q++;
            }
          }
        } else {
          c9_iwork->data[c9_d_k] = c9_idx->data[c9_q];
          c9_q++;
          if (c9_q + 1 == c9_qEnd) {
            while (c9_b_p + 1 < c9_pEnd) {
              c9_d_k++;
              c9_iwork->data[c9_d_k] = c9_idx->data[c9_b_p];
              c9_b_p++;
            }
          }
        }

        c9_d_k++;
      }

      c9_b_p = c9_j - 2;
      c9_e_b = c9_kEnd;
      c9_f_b = c9_e_b;
      if (1 > c9_f_b) {
        c9_c_overflow = false;
      } else {
        c9_c_overflow = (c9_f_b > 2147483646);
      }

      if (c9_c_overflow) {
        c9_check_forloop_overflow_error(chartInstance, true);
      }

      for (c9_e_k = 0; c9_e_k < c9_kEnd; c9_e_k++) {
        c9_d_k = c9_e_k;
        c9_idx->data[(c9_b_p + c9_d_k) + 1] = c9_iwork->data[c9_d_k];
      }

      c9_j = c9_qEnd;
    }

    c9_i = c9_i2;
  }

  c9_emxFree_int32_T(chartInstance, &c9_e_y);
  c9_m = c9_y->size[0];
  c9_col[0] = c9_m;
  c9_col[1] = 1;
  c9_i1054 = c9_iwork->size[0];
  c9_iwork->size[0] = c9_col[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i1054,
    &c9_ak_emlrtRTEI);
  for (c9_b_j = 0; c9_b_j < 2; c9_b_j++) {
    c9_c_j = c9_b_j;
    c9_c_b = c9_m;
    c9_d_b = c9_c_b;
    if (1 > c9_d_b) {
      c9_b_overflow = false;
    } else {
      c9_b_overflow = (c9_d_b > 2147483646);
    }

    if (c9_b_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_b_i = 0; c9_b_i < c9_m; c9_b_i++) {
      c9_c_i = c9_b_i;
      c9_iwork->data[c9_c_i] = c9_y->data[(c9_idx->data[c9_c_i] + c9_y->size[0] *
        c9_c_j) - 1];
    }

    c9_g_b = c9_m;
    c9_h_b = c9_g_b;
    if (1 > c9_h_b) {
      c9_d_overflow = false;
    } else {
      c9_d_overflow = (c9_h_b > 2147483646);
    }

    if (c9_d_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_d_i = 0; c9_d_i < c9_m; c9_d_i++) {
      c9_c_i = c9_d_i;
      c9_y->data[c9_c_i + c9_y->size[0] * c9_c_j] = c9_iwork->data[c9_c_i];
    }
  }

  c9_emxFree_int32_T(chartInstance, &c9_iwork);
  c9_emxFree_int32_T(chartInstance, &c9_idx);
}

static void c9_b_sort(SFc9_LIDAR_simInstanceStruct *chartInstance,
                      c9_emxArray_real_T *c9_x)
{
  int32_T c9_dim;
  real_T c9_d43;
  c9_emxArray_real_T *c9_vwork;
  int32_T c9_vlen;
  int32_T c9_iv12[2];
  int32_T c9_i1060;
  int32_T c9_b_dim;
  int32_T c9_vstride;
  int32_T c9_i1061;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_c_b;
  real_T c9_d44;
  int32_T c9_d_b;
  boolean_T c9_b_overflow;
  int32_T c9_j;
  c9_emxArray_int32_T *c9_b_vwork;
  int32_T c9_b_j;
  int32_T c9_e_b;
  int32_T c9_f_b;
  boolean_T c9_c_overflow;
  int32_T c9_b_k;
  int32_T c9_c_k;
  int32_T c9_g_b;
  int32_T c9_h_b;
  boolean_T c9_d_overflow;
  int32_T c9_d_k;
  c9_dim = 2;
  if ((real_T)c9_x->size[0] != 1.0) {
    c9_dim = 1;
  }

  if (c9_dim <= 1) {
    c9_d43 = (real_T)c9_x->size[0];
  } else {
    c9_d43 = 1.0;
  }

  c9_emxInit_real_T1(chartInstance, &c9_vwork, 1, &c9_bk_emlrtRTEI);
  c9_vlen = (int32_T)c9_d43;
  c9_iv12[0] = c9_vlen;
  c9_iv12[1] = 1;
  c9_i1060 = c9_vwork->size[0];
  c9_vwork->size[0] = c9_iv12[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_vwork, c9_i1060,
    &c9_hi_emlrtRTEI);
  c9_b_dim = c9_dim - 1;
  c9_vstride = 1;
  c9_i1061 = c9_b_dim;
  c9_b = c9_i1061;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_i1061; c9_k++) {
    c9_d44 = (real_T)c9_x->size[0];
    c9_vstride *= (int32_T)c9_d44;
  }

  c9_c_b = c9_vstride;
  c9_d_b = c9_c_b;
  if (1 > c9_d_b) {
    c9_b_overflow = false;
  } else {
    c9_b_overflow = (c9_d_b > 2147483646);
  }

  if (c9_b_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  c9_j = 1;
  c9_emxInit_int32_T(chartInstance, &c9_b_vwork, 1, &c9_hi_emlrtRTEI);
  while (c9_j - 1 <= c9_vstride - 1) {
    c9_b_j = c9_j - 1;
    c9_e_b = c9_vlen;
    c9_f_b = c9_e_b;
    if (1 > c9_f_b) {
      c9_c_overflow = false;
    } else {
      c9_c_overflow = (c9_f_b > 2147483646);
    }

    if (c9_c_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_b_k = 0; c9_b_k < c9_vlen; c9_b_k++) {
      c9_c_k = c9_b_k;
      c9_vwork->data[c9_c_k] = c9_x->data[c9_b_j + c9_c_k * c9_vstride];
    }

    c9_b_sortIdx(chartInstance, c9_vwork, c9_b_vwork);
    c9_g_b = c9_vlen;
    c9_h_b = c9_g_b;
    if (1 > c9_h_b) {
      c9_d_overflow = false;
    } else {
      c9_d_overflow = (c9_h_b > 2147483646);
    }

    if (c9_d_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_d_k = 0; c9_d_k < c9_vlen; c9_d_k++) {
      c9_c_k = c9_d_k;
      c9_x->data[c9_b_j + c9_c_k * c9_vstride] = c9_vwork->data[c9_c_k];
    }

    c9_j++;
  }

  c9_emxFree_int32_T(chartInstance, &c9_b_vwork);
  c9_emxFree_real_T(chartInstance, &c9_vwork);
}

static void c9_b_sortIdx(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T *c9_x, c9_emxArray_int32_T *c9_idx)
{
  real_T c9_b_x[2];
  int32_T c9_i1062;
  int32_T c9_i1063;
  int32_T c9_loop_ub;
  int32_T c9_i1064;
  int32_T c9_n;
  int32_T c9_b_n;
  int32_T c9_i1065;
  int32_T c9_i1066;
  real_T c9_x4[4];
  c9_emxArray_int32_T *c9_iwork;
  int32_T c9_idx4[4];
  int32_T c9_i1067;
  int32_T c9_iv13[1];
  int32_T c9_i1068;
  int32_T c9_i1069;
  int32_T c9_i1070;
  int32_T c9_b_loop_ub;
  int32_T c9_i1071;
  c9_emxArray_real_T *c9_xwork;
  int32_T c9_i1072;
  int32_T c9_i1073;
  int32_T c9_i1074;
  int32_T c9_i1075;
  int32_T c9_c_loop_ub;
  int32_T c9_i1076;
  int32_T c9_nNaNs;
  int32_T c9_ib;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_wOffset;
  int32_T c9_b_k;
  real_T c9_c_x;
  int32_T c9_c_n;
  boolean_T c9_c_b;
  int32_T c9_m;
  int32_T c9_i1077;
  int32_T c9_d_b;
  int32_T c9_e_b;
  int32_T c9_perm[4];
  boolean_T c9_b_overflow;
  int32_T c9_quartetOffset;
  int32_T c9_f_b;
  int32_T c9_g_b;
  int32_T c9_i1;
  int32_T c9_c_k;
  int32_T c9_i2;
  boolean_T c9_c_overflow;
  int32_T c9_i3;
  int32_T c9_itmp;
  int32_T c9_i4;
  int32_T c9_nNonNaN;
  int32_T c9_d_k;
  int32_T c9_preSortLevel;
  int32_T c9_nBlocks;
  int32_T c9_h_b;
  int32_T c9_i_b;
  boolean_T c9_d_overflow;
  int32_T c9_j_b;
  int32_T c9_tailOffset;
  int32_T c9_k_b;
  int32_T c9_nLastBlock;
  int32_T c9_offset;
  int32_T c9_l_b;
  int32_T c9_bLen;
  int32_T c9_bLen2;
  int32_T c9_nPairs;
  int32_T c9_m_b;
  int32_T c9_n_b;
  boolean_T c9_e_overflow;
  int32_T c9_e_k;
  int32_T c9_f_k;
  int32_T c9_blockOffset;
  int32_T c9_o_b;
  int32_T c9_p_b;
  boolean_T c9_f_overflow;
  int32_T c9_j;
  int32_T c9_p;
  int32_T c9_b_j;
  int32_T c9_q;
  int32_T c9_b_iwork[256];
  int32_T c9_iout;
  real_T c9_b_xwork[256];
  int32_T c9_offset1;
  int32_T c9_a;
  int32_T c9_q_b;
  int32_T c9_b_a;
  int32_T c9_r_b;
  boolean_T c9_g_overflow;
  int32_T c9_c_j;
  int32_T exitg1;
  c9_b_x[0] = (real_T)c9_x->size[0];
  c9_b_x[1] = 1.0;
  for (c9_i1062 = 0; c9_i1062 < 2; c9_i1062++) {
    c9_b_x[c9_i1062];
  }

  c9_i1063 = c9_idx->size[0];
  c9_idx->size[0] = (int32_T)c9_b_x[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_idx, c9_i1063, &c9_ck_emlrtRTEI);
  c9_loop_ub = (int32_T)c9_b_x[0] - 1;
  for (c9_i1064 = 0; c9_i1064 <= c9_loop_ub; c9_i1064++) {
    c9_idx->data[c9_i1064] = 0;
  }

  c9_n = c9_x->size[0];
  c9_b_n = c9_x->size[0];
  for (c9_i1065 = 0; c9_i1065 < 4; c9_i1065++) {
    c9_x4[c9_i1065] = 0.0;
  }

  for (c9_i1066 = 0; c9_i1066 < 4; c9_i1066++) {
    c9_idx4[c9_i1066] = 0;
  }

  c9_emxInit_int32_T(chartInstance, &c9_iwork, 1, &c9_ik_emlrtRTEI);
  c9_i1067 = c9_iwork->size[0];
  c9_iwork->size[0] = c9_idx->size[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i1067,
    &c9_dk_emlrtRTEI);
  c9_iv13[0] = c9_iwork->size[0];
  c9_i1068 = c9_iwork->size[0];
  c9_iwork->size[0] = c9_iv13[0];
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i1068,
    &c9_ek_emlrtRTEI);
  c9_i1069 = c9_iwork->size[0];
  c9_i1070 = c9_iwork->size[0];
  c9_iwork->size[0] = c9_i1069;
  c9_emxEnsureCapacity_int32_T(chartInstance, c9_iwork, c9_i1070,
    &c9_fk_emlrtRTEI);
  c9_b_loop_ub = c9_i1069 - 1;
  for (c9_i1071 = 0; c9_i1071 <= c9_b_loop_ub; c9_i1071++) {
    c9_iwork->data[c9_i1071] = 0;
  }

  c9_emxInit_real_T1(chartInstance, &c9_xwork, 1, &c9_jk_emlrtRTEI);
  c9_b_x[0] = (real_T)c9_x->size[0];
  c9_b_x[1] = 1.0;
  c9_i1072 = c9_xwork->size[0];
  c9_xwork->size[0] = (int32_T)c9_b_x[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_xwork, c9_i1072,
    &c9_gk_emlrtRTEI);
  c9_iv13[0] = c9_xwork->size[0];
  c9_i1073 = c9_xwork->size[0];
  c9_xwork->size[0] = c9_iv13[0];
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_xwork, c9_i1073,
    &c9_ek_emlrtRTEI);
  c9_i1074 = c9_xwork->size[0];
  c9_i1075 = c9_xwork->size[0];
  c9_xwork->size[0] = c9_i1074;
  c9_emxEnsureCapacity_real_T1(chartInstance, c9_xwork, c9_i1075,
    &c9_hk_emlrtRTEI);
  c9_c_loop_ub = c9_i1074 - 1;
  for (c9_i1076 = 0; c9_i1076 <= c9_c_loop_ub; c9_i1076++) {
    c9_xwork->data[c9_i1076] = 0.0;
  }

  c9_nNaNs = 0;
  c9_ib = 0;
  c9_b = c9_b_n;
  c9_b_b = c9_b;
  if (1 > c9_b_b) {
    c9_overflow = false;
  } else {
    c9_overflow = (c9_b_b > 2147483646);
  }

  if (c9_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_k = 0; c9_k < c9_b_n; c9_k++) {
    c9_b_k = c9_k;
    c9_c_x = c9_x->data[c9_b_k];
    c9_c_b = muDoubleScalarIsNaN(c9_c_x);
    if (c9_c_b) {
      c9_idx->data[(c9_b_n - c9_nNaNs) - 1] = c9_b_k + 1;
      c9_xwork->data[(c9_b_n - c9_nNaNs) - 1] = c9_x->data[c9_b_k];
      c9_nNaNs++;
    } else {
      c9_ib++;
      c9_idx4[c9_ib - 1] = c9_b_k + 1;
      c9_x4[c9_ib - 1] = c9_x->data[c9_b_k];
      if (c9_ib == 4) {
        c9_quartetOffset = c9_b_k - c9_nNaNs;
        if (c9_x4[0] <= c9_x4[1]) {
          c9_i1 = 1;
          c9_i2 = 2;
        } else {
          c9_i1 = 2;
          c9_i2 = 1;
        }

        if (c9_x4[2] <= c9_x4[3]) {
          c9_i3 = 3;
          c9_i4 = 4;
        } else {
          c9_i3 = 4;
          c9_i4 = 3;
        }

        if (c9_x4[c9_i1 - 1] <= c9_x4[c9_i3 - 1]) {
          if (c9_x4[c9_i2 - 1] <= c9_x4[c9_i3 - 1]) {
            c9_perm[0] = c9_i1;
            c9_perm[1] = c9_i2;
            c9_perm[2] = c9_i3;
            c9_perm[3] = c9_i4;
          } else if (c9_x4[c9_i2 - 1] <= c9_x4[c9_i4 - 1]) {
            c9_perm[0] = c9_i1;
            c9_perm[1] = c9_i3;
            c9_perm[2] = c9_i2;
            c9_perm[3] = c9_i4;
          } else {
            c9_perm[0] = c9_i1;
            c9_perm[1] = c9_i3;
            c9_perm[2] = c9_i4;
            c9_perm[3] = c9_i2;
          }
        } else if (c9_x4[c9_i1 - 1] <= c9_x4[c9_i4 - 1]) {
          if (c9_x4[c9_i2 - 1] <= c9_x4[c9_i4 - 1]) {
            c9_perm[0] = c9_i3;
            c9_perm[1] = c9_i1;
            c9_perm[2] = c9_i2;
            c9_perm[3] = c9_i4;
          } else {
            c9_perm[0] = c9_i3;
            c9_perm[1] = c9_i1;
            c9_perm[2] = c9_i4;
            c9_perm[3] = c9_i2;
          }
        } else {
          c9_perm[0] = c9_i3;
          c9_perm[1] = c9_i4;
          c9_perm[2] = c9_i1;
          c9_perm[3] = c9_i2;
        }

        c9_idx->data[c9_quartetOffset - 3] = c9_idx4[c9_perm[0] - 1];
        c9_idx->data[c9_quartetOffset - 2] = c9_idx4[c9_perm[1] - 1];
        c9_idx->data[c9_quartetOffset - 1] = c9_idx4[c9_perm[2] - 1];
        c9_idx->data[c9_quartetOffset] = c9_idx4[c9_perm[3] - 1];
        c9_x->data[c9_quartetOffset - 3] = c9_x4[c9_perm[0] - 1];
        c9_x->data[c9_quartetOffset - 2] = c9_x4[c9_perm[1] - 1];
        c9_x->data[c9_quartetOffset - 1] = c9_x4[c9_perm[2] - 1];
        c9_x->data[c9_quartetOffset] = c9_x4[c9_perm[3] - 1];
        c9_ib = 0;
      }
    }
  }

  c9_wOffset = (c9_b_n - c9_nNaNs) - 1;
  if (c9_ib > 0) {
    c9_c_n = c9_ib;
    for (c9_i1077 = 0; c9_i1077 < 4; c9_i1077++) {
      c9_perm[c9_i1077] = 0;
    }

    if (c9_c_n == 1) {
      c9_perm[0] = 1;
    } else if (c9_c_n == 2) {
      if (c9_x4[0] <= c9_x4[1]) {
        c9_perm[0] = 1;
        c9_perm[1] = 2;
      } else {
        c9_perm[0] = 2;
        c9_perm[1] = 1;
      }
    } else if (c9_x4[0] <= c9_x4[1]) {
      if (c9_x4[1] <= c9_x4[2]) {
        c9_perm[0] = 1;
        c9_perm[1] = 2;
        c9_perm[2] = 3;
      } else if (c9_x4[0] <= c9_x4[2]) {
        c9_perm[0] = 1;
        c9_perm[1] = 3;
        c9_perm[2] = 2;
      } else {
        c9_perm[0] = 3;
        c9_perm[1] = 1;
        c9_perm[2] = 2;
      }
    } else if (c9_x4[0] <= c9_x4[2]) {
      c9_perm[0] = 2;
      c9_perm[1] = 1;
      c9_perm[2] = 3;
    } else if (c9_x4[1] <= c9_x4[2]) {
      c9_perm[0] = 2;
      c9_perm[1] = 3;
      c9_perm[2] = 1;
    } else {
      c9_perm[0] = 3;
      c9_perm[1] = 2;
      c9_perm[2] = 1;
    }

    c9_f_b = c9_ib;
    c9_g_b = c9_f_b;
    if (1 > c9_g_b) {
      c9_c_overflow = false;
    } else {
      c9_c_overflow = (c9_g_b > 2147483646);
    }

    if (c9_c_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_d_k = 0; c9_d_k < c9_ib; c9_d_k++) {
      c9_b_k = c9_d_k;
      c9_idx->data[((c9_wOffset - c9_ib) + c9_b_k) + 1] = c9_idx4[c9_perm[c9_b_k]
        - 1];
      c9_x->data[((c9_wOffset - c9_ib) + c9_b_k) + 1] = c9_x4[c9_perm[c9_b_k] -
        1];
    }
  }

  c9_m = c9_nNaNs >> 1;
  c9_d_b = c9_m;
  c9_e_b = c9_d_b;
  if (1 > c9_e_b) {
    c9_b_overflow = false;
  } else {
    c9_b_overflow = (c9_e_b > 2147483646);
  }

  if (c9_b_overflow) {
    c9_check_forloop_overflow_error(chartInstance, true);
  }

  for (c9_c_k = 1; c9_c_k - 1 < c9_m; c9_c_k++) {
    c9_b_k = c9_c_k;
    c9_itmp = c9_idx->data[c9_wOffset + c9_b_k];
    c9_idx->data[c9_wOffset + c9_b_k] = c9_idx->data[c9_b_n - c9_b_k];
    c9_idx->data[c9_b_n - c9_b_k] = c9_itmp;
    c9_x->data[c9_wOffset + c9_b_k] = c9_xwork->data[c9_b_n - c9_b_k];
    c9_x->data[c9_b_n - c9_b_k] = c9_xwork->data[c9_wOffset + c9_b_k];
  }

  if ((c9_nNaNs & 1) != 0) {
    c9_x->data[(c9_wOffset + c9_m) + 1] = c9_xwork->data[(c9_wOffset + c9_m) + 1];
  }

  c9_nNonNaN = c9_n - c9_nNaNs;
  c9_preSortLevel = 2;
  if (c9_nNonNaN > 1) {
    if (c9_n >= 256) {
      c9_nBlocks = c9_nNonNaN >> 8;
      if (c9_nBlocks > 0) {
        c9_h_b = c9_nBlocks;
        c9_i_b = c9_h_b;
        if (1 > c9_i_b) {
          c9_d_overflow = false;
        } else {
          c9_d_overflow = (c9_i_b > 2147483646);
        }

        if (c9_d_overflow) {
          c9_check_forloop_overflow_error(chartInstance, true);
        }

        for (c9_j_b = 0; c9_j_b < c9_nBlocks; c9_j_b++) {
          c9_k_b = c9_j_b;
          c9_offset = c9_k_b << 8;
          for (c9_l_b = 0; c9_l_b < 6; c9_l_b++) {
            c9_bLen = 1 << (c9_l_b + 2);
            c9_bLen2 = c9_bLen << 1;
            c9_nPairs = 256 >> (c9_l_b + 3);
            c9_m_b = c9_nPairs;
            c9_n_b = c9_m_b;
            if (1 > c9_n_b) {
              c9_e_overflow = false;
            } else {
              c9_e_overflow = (c9_n_b > 2147483646);
            }

            if (c9_e_overflow) {
              c9_check_forloop_overflow_error(chartInstance, true);
            }

            for (c9_e_k = 0; c9_e_k < c9_nPairs; c9_e_k++) {
              c9_f_k = c9_e_k;
              c9_blockOffset = (c9_offset + c9_f_k * c9_bLen2) - 1;
              c9_o_b = c9_bLen2;
              c9_p_b = c9_o_b;
              if (1 > c9_p_b) {
                c9_f_overflow = false;
              } else {
                c9_f_overflow = (c9_p_b > 2147483646);
              }

              if (c9_f_overflow) {
                c9_check_forloop_overflow_error(chartInstance, true);
              }

              for (c9_j = 0; c9_j < c9_bLen2; c9_j++) {
                c9_b_j = c9_j;
                c9_b_iwork[c9_b_j] = c9_idx->data[(c9_blockOffset + c9_b_j) + 1];
                c9_b_xwork[c9_b_j] = c9_x->data[(c9_blockOffset + c9_b_j) + 1];
              }

              c9_p = 0;
              c9_q = c9_bLen;
              c9_iout = c9_blockOffset;
              do {
                exitg1 = 0;
                c9_iout++;
                if (c9_b_xwork[c9_p] <= c9_b_xwork[c9_q]) {
                  c9_idx->data[c9_iout] = c9_b_iwork[c9_p];
                  c9_x->data[c9_iout] = c9_b_xwork[c9_p];
                  if (c9_p + 1 < c9_bLen) {
                    c9_p++;
                  } else {
                    exitg1 = 1;
                  }
                } else {
                  c9_idx->data[c9_iout] = c9_b_iwork[c9_q];
                  c9_x->data[c9_iout] = c9_b_xwork[c9_q];
                  if (c9_q + 1 < c9_bLen2) {
                    c9_q++;
                  } else {
                    c9_offset1 = c9_iout - c9_p;
                    c9_a = c9_p + 1;
                    c9_q_b = c9_bLen;
                    c9_b_a = c9_a;
                    c9_r_b = c9_q_b;
                    if (c9_b_a > c9_r_b) {
                      c9_g_overflow = false;
                    } else {
                      c9_g_overflow = (c9_r_b > 2147483646);
                    }

                    if (c9_g_overflow) {
                      c9_check_forloop_overflow_error(chartInstance, true);
                    }

                    for (c9_c_j = c9_p + 1; c9_c_j <= c9_bLen; c9_c_j++) {
                      c9_idx->data[c9_offset1 + c9_c_j] = c9_b_iwork[c9_c_j - 1];
                      c9_x->data[c9_offset1 + c9_c_j] = c9_b_xwork[c9_c_j - 1];
                    }

                    exitg1 = 1;
                  }
                }
              } while (exitg1 == 0);
            }
          }
        }

        c9_tailOffset = c9_nBlocks << 8;
        c9_nLastBlock = c9_nNonNaN - c9_tailOffset;
        if (c9_nLastBlock > 0) {
          c9_b_merge_block(chartInstance, c9_idx, c9_x, c9_tailOffset,
                           c9_nLastBlock, 2, c9_iwork, c9_xwork);
        }

        c9_preSortLevel = 8;
      }
    }

    c9_b_merge_block(chartInstance, c9_idx, c9_x, 0, c9_nNonNaN, c9_preSortLevel,
                     c9_iwork, c9_xwork);
  }

  c9_emxFree_real_T(chartInstance, &c9_xwork);
  c9_emxFree_int32_T(chartInstance, &c9_iwork);
}

static void c9_b_merge_block(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x, int32_T c9_offset,
  int32_T c9_n, int32_T c9_preSortLevel, c9_emxArray_int32_T *c9_iwork,
  c9_emxArray_real_T *c9_xwork)
{
  int32_T c9_nBlocks;
  int32_T c9_bLen;
  int32_T c9_bLen2;
  int32_T c9_tailOffset;
  int32_T c9_nPairs;
  int32_T c9_nTail;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_k;
  int32_T c9_b_k;
  c9_nBlocks = c9_n >> c9_preSortLevel;
  c9_bLen = 1 << c9_preSortLevel;
  while (c9_nBlocks > 1) {
    if ((c9_nBlocks & 1) != 0) {
      c9_nBlocks--;
      c9_tailOffset = c9_bLen * c9_nBlocks;
      c9_nTail = c9_n - c9_tailOffset;
      if (c9_nTail > c9_bLen) {
        c9_b_merge(chartInstance, c9_idx, c9_x, c9_offset + c9_tailOffset,
                   c9_bLen, c9_nTail - c9_bLen, c9_iwork, c9_xwork);
      }
    }

    c9_bLen2 = c9_bLen << 1;
    c9_nPairs = c9_nBlocks >> 1;
    c9_b = c9_nPairs;
    c9_b_b = c9_b;
    if (1 > c9_b_b) {
      c9_overflow = false;
    } else {
      c9_overflow = (c9_b_b > 2147483646);
    }

    if (c9_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_k = 0; c9_k < c9_nPairs; c9_k++) {
      c9_b_k = c9_k;
      c9_b_merge(chartInstance, c9_idx, c9_x, c9_offset + c9_b_k * c9_bLen2,
                 c9_bLen, c9_bLen, c9_iwork, c9_xwork);
    }

    c9_bLen = c9_bLen2;
    c9_nBlocks = c9_nPairs;
  }

  if (c9_n > c9_bLen) {
    c9_b_merge(chartInstance, c9_idx, c9_x, c9_offset, c9_bLen, c9_n - c9_bLen,
               c9_iwork, c9_xwork);
  }
}

static void c9_b_merge(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T *c9_idx, c9_emxArray_real_T *c9_x, int32_T c9_offset,
  int32_T c9_np, int32_T c9_nq, c9_emxArray_int32_T *c9_iwork,
  c9_emxArray_real_T *c9_xwork)
{
  int32_T c9_n;
  int32_T c9_b;
  int32_T c9_b_b;
  boolean_T c9_overflow;
  int32_T c9_j;
  int32_T c9_p;
  int32_T c9_b_j;
  int32_T c9_q;
  int32_T c9_qend;
  int32_T c9_iout;
  int32_T c9_offset1;
  int32_T c9_a;
  int32_T c9_c_b;
  int32_T c9_b_a;
  int32_T c9_d_b;
  boolean_T c9_b_overflow;
  int32_T c9_c_j;
  int32_T exitg1;
  if (c9_nq == 0) {
  } else {
    c9_n = c9_np + c9_nq;
    c9_b = c9_n;
    c9_b_b = c9_b;
    if (1 > c9_b_b) {
      c9_overflow = false;
    } else {
      c9_overflow = (c9_b_b > 2147483646);
    }

    if (c9_overflow) {
      c9_check_forloop_overflow_error(chartInstance, true);
    }

    for (c9_j = 0; c9_j < c9_n; c9_j++) {
      c9_b_j = c9_j;
      c9_iwork->data[c9_b_j] = c9_idx->data[c9_offset + c9_b_j];
      c9_xwork->data[c9_b_j] = c9_x->data[c9_offset + c9_b_j];
    }

    c9_p = 1;
    c9_q = c9_np;
    c9_qend = c9_np + c9_nq;
    c9_iout = c9_offset - 1;
    do {
      exitg1 = 0;
      c9_iout++;
      if (c9_xwork->data[c9_p - 1] <= c9_xwork->data[c9_q]) {
        c9_idx->data[c9_iout] = c9_iwork->data[c9_p - 1];
        c9_x->data[c9_iout] = c9_xwork->data[c9_p - 1];
        if (c9_p < c9_np) {
          c9_p++;
        } else {
          exitg1 = 1;
        }
      } else {
        c9_idx->data[c9_iout] = c9_iwork->data[c9_q];
        c9_x->data[c9_iout] = c9_xwork->data[c9_q];
        if (c9_q + 1 < c9_qend) {
          c9_q++;
        } else {
          c9_offset1 = (c9_iout - c9_p) + 1;
          c9_a = c9_p;
          c9_c_b = c9_np;
          c9_b_a = c9_a;
          c9_d_b = c9_c_b;
          if (c9_b_a > c9_d_b) {
            c9_b_overflow = false;
          } else {
            c9_b_overflow = (c9_d_b > 2147483646);
          }

          if (c9_b_overflow) {
            c9_check_forloop_overflow_error(chartInstance, true);
          }

          for (c9_c_j = c9_p; c9_c_j <= c9_np; c9_c_j++) {
            c9_idx->data[c9_offset1 + c9_c_j] = c9_iwork->data[c9_c_j - 1];
            c9_x->data[c9_offset1 + c9_c_j] = c9_xwork->data[c9_c_j - 1];
          }

          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);
  }
}

static void c9_b_cosd(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T *c9_x)
{
  real_T c9_b_x;
  real_T c9_c_x;
  real_T c9_d_x;
  boolean_T c9_b;
  boolean_T c9_b94;
  real_T c9_e_x;
  boolean_T c9_b_b;
  boolean_T c9_b95;
  boolean_T c9_c_b;
  real_T c9_f_x;
  real_T c9_g_x;
  real_T c9_h_x;
  real_T c9_i_x;
  real_T c9_j_x;
  real_T c9_k_x;
  real_T c9_absx;
  int8_T c9_n;
  real_T c9_l_x;
  real_T c9_m_x;
  real_T c9_n_x;
  (void)chartInstance;
  c9_b_x = *c9_x;
  c9_c_x = c9_b_x;
  c9_d_x = c9_c_x;
  c9_b = muDoubleScalarIsInf(c9_d_x);
  c9_b94 = !c9_b;
  c9_e_x = c9_c_x;
  c9_b_b = muDoubleScalarIsNaN(c9_e_x);
  c9_b95 = !c9_b_b;
  c9_c_b = (c9_b94 && c9_b95);
  if (!c9_c_b) {
    *c9_x = rtNaN;
  } else {
    c9_f_x = c9_b_x;
    c9_g_x = c9_f_x;
    c9_h_x = c9_g_x;
    c9_i_x = c9_h_x;
    c9_j_x = c9_i_x;
    c9_g_x = muDoubleScalarRem(c9_j_x, 360.0);
    c9_k_x = c9_g_x;
    c9_absx = muDoubleScalarAbs(c9_k_x);
    if (c9_absx > 180.0) {
      if (c9_g_x > 0.0) {
        c9_g_x -= 360.0;
      } else {
        c9_g_x += 360.0;
      }

      c9_l_x = c9_g_x;
      c9_m_x = c9_l_x;
      c9_n_x = c9_m_x;
      c9_absx = muDoubleScalarAbs(c9_n_x);
    }

    if (c9_absx <= 45.0) {
      c9_g_x *= 0.017453292519943295;
      c9_n = 0;
    } else if (c9_absx <= 135.0) {
      if (c9_g_x > 0.0) {
        c9_g_x = 0.017453292519943295 * (c9_g_x - 90.0);
        c9_n = 1;
      } else {
        c9_g_x = 0.017453292519943295 * (c9_g_x + 90.0);
        c9_n = -1;
      }
    } else if (c9_g_x > 0.0) {
      c9_g_x = 0.017453292519943295 * (c9_g_x - 180.0);
      c9_n = 2;
    } else {
      c9_g_x = 0.017453292519943295 * (c9_g_x + 180.0);
      c9_n = -2;
    }

    if ((real_T)c9_n == 0.0) {
      *c9_x = muDoubleScalarCos(c9_g_x);
    } else if ((real_T)c9_n == 1.0) {
      *c9_x = -muDoubleScalarSin(c9_g_x);
    } else if ((real_T)c9_n == -1.0) {
      *c9_x = muDoubleScalarSin(c9_g_x);
    } else {
      *c9_x = -muDoubleScalarCos(c9_g_x);
    }
  }
}

static void c9_b_sind(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T *c9_x)
{
  real_T c9_b_x;
  real_T c9_c_x;
  real_T c9_d_x;
  boolean_T c9_b;
  boolean_T c9_b96;
  real_T c9_e_x;
  boolean_T c9_b_b;
  boolean_T c9_b97;
  boolean_T c9_c_b;
  real_T c9_f_x;
  real_T c9_g_x;
  real_T c9_h_x;
  real_T c9_i_x;
  real_T c9_j_x;
  real_T c9_k_x;
  real_T c9_l_x;
  real_T c9_absx;
  int8_T c9_n;
  real_T c9_m_x;
  real_T c9_n_x;
  real_T c9_o_x;
  (void)chartInstance;
  c9_b_x = *c9_x;
  c9_c_x = c9_b_x;
  c9_d_x = c9_c_x;
  c9_b = muDoubleScalarIsInf(c9_d_x);
  c9_b96 = !c9_b;
  c9_e_x = c9_c_x;
  c9_b_b = muDoubleScalarIsNaN(c9_e_x);
  c9_b97 = !c9_b_b;
  c9_c_b = (c9_b96 && c9_b97);
  if (!c9_c_b) {
    c9_g_x = rtNaN;
  } else {
    c9_f_x = c9_b_x;
    c9_h_x = c9_f_x;
    c9_i_x = c9_h_x;
    c9_j_x = c9_i_x;
    c9_k_x = c9_j_x;
    c9_h_x = muDoubleScalarRem(c9_k_x, 360.0);
    c9_l_x = c9_h_x;
    c9_absx = muDoubleScalarAbs(c9_l_x);
    if (c9_absx > 180.0) {
      if (c9_h_x > 0.0) {
        c9_h_x -= 360.0;
      } else {
        c9_h_x += 360.0;
      }

      c9_m_x = c9_h_x;
      c9_n_x = c9_m_x;
      c9_o_x = c9_n_x;
      c9_absx = muDoubleScalarAbs(c9_o_x);
    }

    if (c9_absx <= 45.0) {
      c9_h_x *= 0.017453292519943295;
      c9_n = 0;
    } else if (c9_absx <= 135.0) {
      if (c9_h_x > 0.0) {
        c9_h_x = 0.017453292519943295 * (c9_h_x - 90.0);
        c9_n = 1;
      } else {
        c9_h_x = 0.017453292519943295 * (c9_h_x + 90.0);
        c9_n = -1;
      }
    } else if (c9_h_x > 0.0) {
      c9_h_x = 0.017453292519943295 * (c9_h_x - 180.0);
      c9_n = 2;
    } else {
      c9_h_x = 0.017453292519943295 * (c9_h_x + 180.0);
      c9_n = -2;
    }

    if ((real_T)c9_n == 0.0) {
      c9_g_x = muDoubleScalarSin(c9_h_x);
    } else if ((real_T)c9_n == 1.0) {
      c9_g_x = muDoubleScalarCos(c9_h_x);
    } else if ((real_T)c9_n == -1.0) {
      c9_g_x = -muDoubleScalarCos(c9_h_x);
    } else {
      c9_g_x = -muDoubleScalarSin(c9_h_x);
    }
  }

  *c9_x = c9_g_x;
}

static void c9_emxEnsureCapacity_real_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(real_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(real_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (real_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxEnsureCapacity_real_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(real_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(real_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (real_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxEnsureCapacity_int32_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_int32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(int32_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(int32_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (int32_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxEnsureCapacity_boolean_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_boolean_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(boolean_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(boolean_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (boolean_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxEnsureCapacity_skoeQIuVNKJRH(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_emxArray, int32_T
  c9_oldNumel, const emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof
      (c9_skoeQIuVNKJRHNtBIlOCZhD));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(c9_skoeQIuVNKJRHNtBIlOCZhD) *
             (uint32_T)c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (c9_skoeQIuVNKJRHNtBIlOCZhD *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxInit_real_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_real_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_real_T *)emlrtMallocMex(sizeof(c9_emxArray_real_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (real_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInit_boolean_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_boolean_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_boolean_T *)emlrtMallocMex(sizeof
    (c9_emxArray_boolean_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (boolean_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInit_skoeQIuVNKJRHNtBIlOCZhD(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_skoeQIuVNKJRHNtBIlOCZh **c9_pEmxArray, int32_T
  c9_numDimensions, const emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *)emlrtMallocMex(sizeof
    (c9_emxArray_skoeQIuVNKJRHNtBIlOCZh));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (c9_skoeQIuVNKJRHNtBIlOCZhD *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInit_real_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_real_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_real_T *)emlrtMallocMex(sizeof(c9_emxArray_real_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (real_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInit_int32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_int32_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_int32_T *)emlrtMallocMex(sizeof
    (c9_emxArray_int32_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (int32_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInitStruct_coder_internal_sp(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_sparse *c9_pStruct, const emlrtRTEInfo
  *c9_srcLocation)
{
  c9_emxInit_boolean_T1(chartInstance, &c9_pStruct->d, 1, c9_srcLocation);
  c9_emxInit_int32_T(chartInstance, &c9_pStruct->colidx, 1, c9_srcLocation);
  c9_emxInit_int32_T(chartInstance, &c9_pStruct->rowidx, 1, c9_srcLocation);
}

static void c9_emxInit_boolean_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_boolean_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_boolean_T *)emlrtMallocMex(sizeof
    (c9_emxArray_boolean_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (boolean_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxFree_real_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real_T **c9_pEmxArray)
{
  (void)chartInstance;
  if (*c9_pEmxArray != (c9_emxArray_real_T *)NULL) {
    if (((*c9_pEmxArray)->data != (real_T *)NULL) && (*c9_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c9_pEmxArray)->data);
    }

    emlrtFreeMex((*c9_pEmxArray)->size);
    emlrtFreeMex(*c9_pEmxArray);
    *c9_pEmxArray = (c9_emxArray_real_T *)NULL;
  }
}

static void c9_emxFree_boolean_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_pEmxArray)
{
  (void)chartInstance;
  if (*c9_pEmxArray != (c9_emxArray_boolean_T *)NULL) {
    if (((*c9_pEmxArray)->data != (boolean_T *)NULL) && (*c9_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c9_pEmxArray)->data);
    }

    emlrtFreeMex((*c9_pEmxArray)->size);
    emlrtFreeMex(*c9_pEmxArray);
    *c9_pEmxArray = (c9_emxArray_boolean_T *)NULL;
  }
}

static void c9_emxFree_skoeQIuVNKJRHNtBIlOCZhD(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_skoeQIuVNKJRHNtBIlOCZh **c9_pEmxArray)
{
  (void)chartInstance;
  if (*c9_pEmxArray != (c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *)NULL) {
    if (((*c9_pEmxArray)->data != (c9_skoeQIuVNKJRHNtBIlOCZhD *)NULL) &&
        (*c9_pEmxArray)->canFreeData) {
      emlrtFreeMex((*c9_pEmxArray)->data);
    }

    emlrtFreeMex((*c9_pEmxArray)->size);
    emlrtFreeMex(*c9_pEmxArray);
    *c9_pEmxArray = (c9_emxArray_skoeQIuVNKJRHNtBIlOCZh *)NULL;
  }
}

static void c9_emxFree_int32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_pEmxArray)
{
  (void)chartInstance;
  if (*c9_pEmxArray != (c9_emxArray_int32_T *)NULL) {
    if (((*c9_pEmxArray)->data != (int32_T *)NULL) && (*c9_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c9_pEmxArray)->data);
    }

    emlrtFreeMex((*c9_pEmxArray)->size);
    emlrtFreeMex(*c9_pEmxArray);
    *c9_pEmxArray = (c9_emxArray_int32_T *)NULL;
  }
}

static void c9_emxFreeStruct_coder_internal_sp(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_sparse *c9_pStruct)
{
  c9_emxFree_boolean_T(chartInstance, &c9_pStruct->d);
  c9_emxFree_int32_T(chartInstance, &c9_pStruct->colidx);
  c9_emxFree_int32_T(chartInstance, &c9_pStruct->rowidx);
}

static void c9_emxEnsureCapacity_boolean_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_boolean_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(boolean_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(boolean_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (boolean_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxCopyStruct_coder_internal_an(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_anonymous_function *c9_dst, const
  c9_coder_internal_anonymous_function *c9_src, const emlrtRTEInfo
  *c9_srcLocation)
{
  c9_emxCopyMatrix_real_T(chartInstance, c9_srcLocation);
  c9_emxCopyMatrix_cell_wrap_1(chartInstance, c9_dst->tunableEnvironment,
    c9_src->tunableEnvironment, c9_srcLocation);
}

static void c9_emxCopyMatrix_real_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  const emlrtRTEInfo *c9_srcLocation)
{
  (void)chartInstance;
  (void)c9_srcLocation;
}

static void c9_emxCopyMatrix_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 c9_dst[2], const c9_cell_wrap_1 c9_src[2],
  const emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_i;
  for (c9_i = 0; c9_i < 2; c9_i++) {
    c9_emxCopyStruct_cell_wrap_1(chartInstance, &c9_dst[c9_i], &c9_src[c9_i],
      c9_srcLocation);
  }
}

static void c9_emxCopyStruct_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 *c9_dst, const c9_cell_wrap_1 *c9_src, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxCopy_int32_T(chartInstance, &c9_dst->f1, &c9_src->f1, c9_srcLocation);
}

static void c9_emxCopy_int32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_dst, c9_emxArray_int32_T * const *c9_src, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_numElDst;
  int32_T c9_numElSrc;
  int32_T c9_i;
  c9_numElDst = 1;
  c9_numElSrc = 1;
  for (c9_i = 0; c9_i < (*c9_dst)->numDimensions; c9_i++) {
    c9_numElDst *= (*c9_dst)->size[c9_i];
    c9_numElSrc *= (*c9_src)->size[c9_i];
  }

  for (c9_i = 0; c9_i < (*c9_dst)->numDimensions; c9_i++) {
    (*c9_dst)->size[c9_i] = (*c9_src)->size[c9_i];
  }

  c9_emxEnsureCapacity_int32_T(chartInstance, *c9_dst, c9_numElDst,
    c9_srcLocation);
  for (c9_i = 0; c9_i < c9_numElSrc; c9_i++) {
    (*c9_dst)->data[c9_i] = (*c9_src)->data[c9_i];
  }
}

static void c9_emxInitStruct_coder_internal_an(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_anonymous_function *c9_pStruct, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxInitMatrix_cell_wrap_1(chartInstance, c9_pStruct->tunableEnvironment,
    c9_srcLocation);
}

static void c9_emxInitMatrix_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 c9_pMatrix[2], const emlrtRTEInfo
  *c9_srcLocation)
{
  int32_T c9_i;
  for (c9_i = 0; c9_i < 2; c9_i++) {
    c9_emxInitStruct_cell_wrap_1(chartInstance, &c9_pMatrix[c9_i],
      c9_srcLocation);
  }
}

static void c9_emxInitStruct_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 *c9_pStruct, const emlrtRTEInfo *c9_srcLocation)
{
  c9_emxInit_int32_T(chartInstance, &c9_pStruct->f1, 1, c9_srcLocation);
}

static void c9_emxFreeMatrix_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 c9_pMatrix[2])
{
  int32_T c9_i;
  for (c9_i = 0; c9_i < 2; c9_i++) {
    c9_emxFreeStruct_cell_wrap_1(chartInstance, &c9_pMatrix[c9_i]);
  }
}

static void c9_emxFreeStruct_cell_wrap_1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_cell_wrap_1 *c9_pStruct)
{
  c9_emxFree_int32_T(chartInstance, &c9_pStruct->f1);
}

static void c9_emxFreeStruct_coder_internal_an(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_anonymous_function *c9_pStruct)
{
  c9_emxFreeMatrix_cell_wrap_1(chartInstance, c9_pStruct->tunableEnvironment);
}

static void c9_emxCopyStruct_coder_internal_sp(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_coder_internal_sparse *c9_dst, const
  c9_coder_internal_sparse *c9_src, const emlrtRTEInfo *c9_srcLocation)
{
  c9_emxCopy_boolean_T(chartInstance, &c9_dst->d, &c9_src->d, c9_srcLocation);
  c9_emxCopy_int32_T(chartInstance, &c9_dst->colidx, &c9_src->colidx,
                     c9_srcLocation);
  c9_emxCopy_int32_T(chartInstance, &c9_dst->rowidx, &c9_src->rowidx,
                     c9_srcLocation);
  c9_dst->m = c9_src->m;
  c9_dst->n = c9_src->n;
  c9_dst->maxnz = c9_src->maxnz;
}

static void c9_emxCopy_boolean_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_boolean_T **c9_dst, c9_emxArray_boolean_T * const *c9_src, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_numElDst;
  int32_T c9_numElSrc;
  int32_T c9_i;
  c9_numElDst = 1;
  c9_numElSrc = 1;
  for (c9_i = 0; c9_i < (*c9_dst)->numDimensions; c9_i++) {
    c9_numElDst *= (*c9_dst)->size[c9_i];
    c9_numElSrc *= (*c9_src)->size[c9_i];
  }

  for (c9_i = 0; c9_i < (*c9_dst)->numDimensions; c9_i++) {
    (*c9_dst)->size[c9_i] = (*c9_src)->size[c9_i];
  }

  c9_emxEnsureCapacity_boolean_T1(chartInstance, *c9_dst, c9_numElDst,
    c9_srcLocation);
  for (c9_i = 0; c9_i < c9_numElSrc; c9_i++) {
    (*c9_dst)->data[c9_i] = (*c9_src)->data[c9_i];
  }
}

static void c9_emxEnsureCapacity_real32_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(real32_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(real32_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (real32_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxEnsureCapacity_real32_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_real32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(real32_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(real32_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (real32_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxInit_real32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_real32_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_real32_T *)emlrtMallocMex(sizeof
    (c9_emxArray_real32_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (real32_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInit_real32_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_real32_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_real32_T *)emlrtMallocMex(sizeof
    (c9_emxArray_real32_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (real32_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxFree_real32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_real32_T **c9_pEmxArray)
{
  (void)chartInstance;
  if (*c9_pEmxArray != (c9_emxArray_real32_T *)NULL) {
    if (((*c9_pEmxArray)->data != (real32_T *)NULL) && (*c9_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c9_pEmxArray)->data);
    }

    emlrtFreeMex((*c9_pEmxArray)->size);
    emlrtFreeMex(*c9_pEmxArray);
    *c9_pEmxArray = (c9_emxArray_real32_T *)NULL;
  }
}

static void c9_emxEnsureCapacity_int32_T1(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_int32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(int32_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(int32_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (int32_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxEnsureCapacity_uint32_T(SFc9_LIDAR_simInstanceStruct
  *chartInstance, c9_emxArray_uint32_T *c9_emxArray, int32_T c9_oldNumel, const
  emlrtRTEInfo *c9_srcLocation)
{
  int32_T c9_newNumel;
  int32_T c9_i;
  int32_T c9_newCapacity;
  void *c9_newData;
  if (c9_oldNumel < 0) {
    c9_oldNumel = 0;
  }

  c9_newNumel = 1;
  for (c9_i = 0; c9_i < c9_emxArray->numDimensions; c9_i++) {
    c9_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c9_newNumel, (uint32_T)
      c9_emxArray->size[c9_i], c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  if (c9_newNumel > c9_emxArray->allocatedSize) {
    c9_newCapacity = c9_emxArray->allocatedSize;
    if (c9_newCapacity < 16) {
      c9_newCapacity = 16;
    }

    while (c9_newCapacity < c9_newNumel) {
      if (c9_newCapacity > 1073741823) {
        c9_newCapacity = MAX_int32_T;
      } else {
        c9_newCapacity <<= 1;
      }
    }

    c9_newData = emlrtCallocMex((uint32_T)c9_newCapacity, sizeof(uint32_T));
    if (c9_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
    }

    if (c9_emxArray->data != NULL) {
      memcpy(c9_newData, c9_emxArray->data, sizeof(uint32_T) * (uint32_T)
             c9_oldNumel);
      if (c9_emxArray->canFreeData) {
        emlrtFreeMex(c9_emxArray->data);
      }
    }

    c9_emxArray->data = (uint32_T *)c9_newData;
    c9_emxArray->allocatedSize = c9_newCapacity;
    c9_emxArray->canFreeData = true;
  }
}

static void c9_emxInit_int32_T1(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_int32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_int32_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_int32_T *)emlrtMallocMex(sizeof
    (c9_emxArray_int32_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (int32_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxInit_uint32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_uint32_T **c9_pEmxArray, int32_T c9_numDimensions, const
  emlrtRTEInfo *c9_srcLocation)
{
  c9_emxArray_uint32_T *c9_emxArray;
  int32_T c9_i;
  *c9_pEmxArray = (c9_emxArray_uint32_T *)emlrtMallocMex(sizeof
    (c9_emxArray_uint32_T));
  if ((void *)*c9_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray = *c9_pEmxArray;
  c9_emxArray->data = (uint32_T *)NULL;
  c9_emxArray->numDimensions = c9_numDimensions;
  c9_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c9_numDimensions);
  if ((void *)c9_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c9_srcLocation, chartInstance->c9_fEmlrtCtx);
  }

  c9_emxArray->allocatedSize = 0;
  c9_emxArray->canFreeData = true;
  for (c9_i = 0; c9_i < c9_numDimensions; c9_i++) {
    c9_emxArray->size[c9_i] = 0;
  }
}

static void c9_emxFree_uint32_T(SFc9_LIDAR_simInstanceStruct *chartInstance,
  c9_emxArray_uint32_T **c9_pEmxArray)
{
  (void)chartInstance;
  if (*c9_pEmxArray != (c9_emxArray_uint32_T *)NULL) {
    if (((*c9_pEmxArray)->data != (uint32_T *)NULL) && (*c9_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c9_pEmxArray)->data);
    }

    emlrtFreeMex((*c9_pEmxArray)->size);
    emlrtFreeMex(*c9_pEmxArray);
    *c9_pEmxArray = (c9_emxArray_uint32_T *)NULL;
  }
}

static int32_T c9_div_nzp_s32(SFc9_LIDAR_simInstanceStruct *chartInstance,
  int32_T c9_numerator, int32_T c9_denominator, uint32_T c9_ssid_src_loc,
  int32_T c9_offset_src_loc, int32_T c9_length_src_loc)
{
  int32_T c9_quotient;
  uint32_T c9_absNumerator;
  uint32_T c9_absDenominator;
  boolean_T c9_quotientNeedsNegation;
  uint32_T c9_tempAbsQuotient;
  (void)chartInstance;
  (void)c9_ssid_src_loc;
  (void)c9_offset_src_loc;
  (void)c9_length_src_loc;
  if (c9_numerator < 0) {
    c9_absNumerator = ~(uint32_T)c9_numerator + 1U;
  } else {
    c9_absNumerator = (uint32_T)c9_numerator;
  }

  if (c9_denominator < 0) {
    c9_absDenominator = ~(uint32_T)c9_denominator + 1U;
  } else {
    c9_absDenominator = (uint32_T)c9_denominator;
  }

  c9_quotientNeedsNegation = ((c9_numerator < 0) != (c9_denominator < 0));
  c9_tempAbsQuotient = c9_absNumerator / c9_absDenominator;
  if (c9_quotientNeedsNegation) {
    c9_quotient = -(int32_T)c9_tempAbsQuotient;
  } else {
    c9_quotient = (int32_T)c9_tempAbsQuotient;
  }

  return c9_quotient;
}

static int32_T c9__s32_s64_(SFc9_LIDAR_simInstanceStruct *chartInstance, int64_T
  c9_b, uint32_T c9_ssid_src_loc, int32_T c9_offset_src_loc, int32_T
  c9_length_src_loc)
{
  int32_T c9_a;
  (void)chartInstance;
  c9_a = (int32_T)c9_b;
  if ((int64_T)c9_a != c9_b) {
    _SFD_OVERFLOW_DETECTION(SFDB_OVERFLOW, c9_ssid_src_loc, c9_offset_src_loc,
      c9_length_src_loc);
  }

  return c9_a;
}

static int32_T c9__s32_d_(SFc9_LIDAR_simInstanceStruct *chartInstance, real_T
  c9_b, uint32_T c9_ssid_src_loc, int32_T c9_offset_src_loc, int32_T
  c9_length_src_loc)
{
  int32_T c9_a;
  real_T c9_b_b;
  (void)chartInstance;
  c9_a = (int32_T)c9_b;
  if (c9_b < 0.0) {
    c9_b_b = muDoubleScalarCeil(c9_b);
  } else {
    c9_b_b = muDoubleScalarFloor(c9_b);
  }

  if ((real_T)c9_a != c9_b_b) {
    _SFD_OVERFLOW_DETECTION(SFDB_OVERFLOW, c9_ssid_src_loc, c9_offset_src_loc,
      c9_length_src_loc);
  }

  return c9_a;
}

static void init_dsm_address_info(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc9_LIDAR_simInstanceStruct *chartInstance)
{
  chartInstance->c9_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c9_b_obstaclesInRegionOfInterest = (real_T (*)[100000])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c9_rotationMatrix = (real_T (*)[4])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c9_minRotation = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c9_maxRotation = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c9_invRotationMatrix = (real_T (*)[4])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 2);
}

/* SFunction Glue Code */
void sf_c9_LIDAR_sim_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3007899347U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(95021189U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3944978455U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(859500683U);
}

mxArray* sf_c9_LIDAR_sim_get_post_codegen_info(void);
mxArray *sf_c9_LIDAR_sim_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("2OuS9HssHg9rkZVnsbjVeB");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(50000);
      pr[1] = (double)(2);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(2);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(2);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c9_LIDAR_sim_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c9_LIDAR_sim_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,6);
  mxSetCell(mxcell3p, 0, mxCreateString(
             "images.internal.coder.buildable.IppfilterBuildable"));
  mxSetCell(mxcell3p, 1, mxCreateString(
             "images.internal.coder.buildable.ImfilterBuildable"));
  mxSetCell(mxcell3p, 2, mxCreateString(
             "images.internal.coder.buildable.GetnumcoresBuildable"));
  mxSetCell(mxcell3p, 3, mxCreateString(
             "images.internal.coder.buildable.TbbhistBuildable"));
  mxSetCell(mxcell3p, 4, mxCreateString(
             "images.internal.coder.buildable.CannyThresholdingTbbBuildable"));
  mxSetCell(mxcell3p, 5, mxCreateString(
             "images.internal.coder.buildable.IppreconstructBuildable"));
  return(mxcell3p);
}

mxArray *sf_c9_LIDAR_sim_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("ir_function_calls");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("ippfilter_real32");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c9_LIDAR_sim_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c9_LIDAR_sim_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c9_LIDAR_sim(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[8],T\"invRotationMatrix\",},{M[1],M[5],T\"rotationMatrix\",},{M[8],M[0],T\"is_active_c9_LIDAR_sim\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c9_LIDAR_sim_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc9_LIDAR_simInstanceStruct *chartInstance = (SFc9_LIDAR_simInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LIDAR_simMachineNumber_,
           9,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LIDAR_simMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LIDAR_simMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LIDAR_simMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"obstaclesInRegionOfInterest");
          _SFD_SET_DATA_PROPS(1,1,1,0,"minRotation");
          _SFD_SET_DATA_PROPS(2,1,1,0,"maxRotation");
          _SFD_SET_DATA_PROPS(3,2,0,1,"rotationMatrix");
          _SFD_SET_DATA_PROPS(4,2,0,1,"invRotationMatrix");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,2226);
        _SFD_CV_INIT_EML_FOR(0,1,0,1857,1881,1927);

        {
          unsigned int dimVector[2];
          dimVector[0]= 50000U;
          dimVector[1]= 2U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c9_j_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 2U;
          dimVector[1]= 2U;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c9_k_sf_marshallOut,(MexInFcnForType)
            c9_j_sf_marshallIn);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 2U;
          dimVector[1]= 2U;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c9_k_sf_marshallOut,(MexInFcnForType)
            c9_j_sf_marshallIn);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LIDAR_simMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc9_LIDAR_simInstanceStruct *chartInstance = (SFc9_LIDAR_simInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U,
          chartInstance->c9_b_obstaclesInRegionOfInterest);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c9_rotationMatrix);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c9_minRotation);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c9_maxRotation);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c9_invRotationMatrix);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "s8QPnViQ2QBC1XLjw8YZSyG";
}

static void sf_opaque_initialize_c9_LIDAR_sim(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*)
    chartInstanceVar);
  initialize_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c9_LIDAR_sim(void *chartInstanceVar)
{
  enable_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c9_LIDAR_sim(void *chartInstanceVar)
{
  disable_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c9_LIDAR_sim(void *chartInstanceVar)
{
  sf_gateway_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c9_LIDAR_sim(SimStruct* S)
{
  return get_sim_state_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c9_LIDAR_sim(SimStruct* S, const mxArray *st)
{
  set_sim_state_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c9_LIDAR_sim(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LIDAR_sim_optimization_info();
    }

    finalize_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c9_LIDAR_sim(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c9_LIDAR_sim((SFc9_LIDAR_simInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c9_LIDAR_sim(SimStruct *S)
{
  /* Set overwritable ports for inplace optimization */
  ssSetInputPortDirectFeedThrough(S, 0, 1);
  ssSetInputPortDirectFeedThrough(S, 1, 1);
  ssSetInputPortDirectFeedThrough(S, 2, 1);
  ssSetStatesModifiedOnlyInUpdate(S, 1);
  ssSetBlockIsPurelyCombinatorial_wrapper(S, 1);
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LIDAR_sim_optimization_info(sim_mode_is_rtw_gen(S),
      sim_mode_is_modelref_sim(S), sim_mode_is_external(S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,9);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetSupportedForRowMajorCodeGen(S, 1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,9,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 9);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,9);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,9,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,9,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,9);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2672657182U));
  ssSetChecksum1(S,(4212395902U));
  ssSetChecksum2(S,(3534558841U));
  ssSetChecksum3(S,(2234966345U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c9_LIDAR_sim(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c9_LIDAR_sim(SimStruct *S)
{
  SFc9_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc9_LIDAR_simInstanceStruct *)utMalloc(sizeof
    (SFc9_LIDAR_simInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc9_LIDAR_simInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c9_LIDAR_sim;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c9_LIDAR_sim;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c9_LIDAR_sim;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c9_LIDAR_sim;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c9_LIDAR_sim;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c9_LIDAR_sim;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c9_LIDAR_sim;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c9_LIDAR_sim;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c9_LIDAR_sim;
  chartInstance->chartInfo.mdlStart = mdlStart_c9_LIDAR_sim;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c9_LIDAR_sim;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0, NULL, NULL);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c9_LIDAR_sim(chartInstance);
}

void c9_LIDAR_sim_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c9_LIDAR_sim(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c9_LIDAR_sim(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c9_LIDAR_sim(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c9_LIDAR_sim_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
