#ifndef __c9_LIDAR_sim_h__
#define __c9_LIDAR_sim_h__

/* Type Definitions */
#ifndef struct_tag_skA4KFEZ4HPkJJBOYCrevdH
#define struct_tag_skA4KFEZ4HPkJJBOYCrevdH

struct tag_skA4KFEZ4HPkJJBOYCrevdH
{
  uint32_T SafeEq;
  uint32_T Absolute;
  uint32_T NaNBias;
  uint32_T NaNWithFinite;
  uint32_T FiniteWithNaN;
  uint32_T NaNWithNaN;
};

#endif                                 /*struct_tag_skA4KFEZ4HPkJJBOYCrevdH*/

#ifndef typedef_c9_skA4KFEZ4HPkJJBOYCrevdH
#define typedef_c9_skA4KFEZ4HPkJJBOYCrevdH

typedef struct tag_skA4KFEZ4HPkJJBOYCrevdH c9_skA4KFEZ4HPkJJBOYCrevdH;

#endif                                 /*typedef_c9_skA4KFEZ4HPkJJBOYCrevdH*/

#ifndef struct_tag_spGKsvEVm7uA89hv31XX4LH
#define struct_tag_spGKsvEVm7uA89hv31XX4LH

struct tag_spGKsvEVm7uA89hv31XX4LH
{
  uint32_T MissingPlacement;
  uint32_T ComparisonMethod;
};

#endif                                 /*struct_tag_spGKsvEVm7uA89hv31XX4LH*/

#ifndef typedef_c9_spGKsvEVm7uA89hv31XX4LH
#define typedef_c9_spGKsvEVm7uA89hv31XX4LH

typedef struct tag_spGKsvEVm7uA89hv31XX4LH c9_spGKsvEVm7uA89hv31XX4LH;

#endif                                 /*typedef_c9_spGKsvEVm7uA89hv31XX4LH*/

#ifndef struct_tag_sBaHy6MF1FZJsDHxMqvBaiH
#define struct_tag_sBaHy6MF1FZJsDHxMqvBaiH

struct tag_sBaHy6MF1FZJsDHxMqvBaiH
{
  int32_T xstart;
  int32_T xend;
  int32_T depth;
};

#endif                                 /*struct_tag_sBaHy6MF1FZJsDHxMqvBaiH*/

#ifndef typedef_c9_sBaHy6MF1FZJsDHxMqvBaiH
#define typedef_c9_sBaHy6MF1FZJsDHxMqvBaiH

typedef struct tag_sBaHy6MF1FZJsDHxMqvBaiH c9_sBaHy6MF1FZJsDHxMqvBaiH;

#endif                                 /*typedef_c9_sBaHy6MF1FZJsDHxMqvBaiH*/

#ifndef struct_tag_szuUIsjSIN6GWJW5YzVNVmG
#define struct_tag_szuUIsjSIN6GWJW5YzVNVmG

struct tag_szuUIsjSIN6GWJW5YzVNVmG
{
  uint32_T Theta;
  uint32_T RhoResolution;
};

#endif                                 /*struct_tag_szuUIsjSIN6GWJW5YzVNVmG*/

#ifndef typedef_c9_szuUIsjSIN6GWJW5YzVNVmG
#define typedef_c9_szuUIsjSIN6GWJW5YzVNVmG

typedef struct tag_szuUIsjSIN6GWJW5YzVNVmG c9_szuUIsjSIN6GWJW5YzVNVmG;

#endif                                 /*typedef_c9_szuUIsjSIN6GWJW5YzVNVmG*/

#ifndef struct_tag_s9s8BC13iTohZXRbLMSIDHE
#define struct_tag_s9s8BC13iTohZXRbLMSIDHE

struct tag_s9s8BC13iTohZXRbLMSIDHE
{
  boolean_T CaseSensitivity;
  boolean_T StructExpand;
  boolean_T PartialMatching;
};

#endif                                 /*struct_tag_s9s8BC13iTohZXRbLMSIDHE*/

#ifndef typedef_c9_s9s8BC13iTohZXRbLMSIDHE
#define typedef_c9_s9s8BC13iTohZXRbLMSIDHE

typedef struct tag_s9s8BC13iTohZXRbLMSIDHE c9_s9s8BC13iTohZXRbLMSIDHE;

#endif                                 /*typedef_c9_s9s8BC13iTohZXRbLMSIDHE*/

#ifndef struct_tag_skZBV47kFp42DPFCMXy0uBE
#define struct_tag_skZBV47kFp42DPFCMXy0uBE

struct tag_skZBV47kFp42DPFCMXy0uBE
{
  uint32_T Threshold;
  uint32_T NHoodSize;
  uint32_T Theta;
};

#endif                                 /*struct_tag_skZBV47kFp42DPFCMXy0uBE*/

#ifndef typedef_c9_skZBV47kFp42DPFCMXy0uBE
#define typedef_c9_skZBV47kFp42DPFCMXy0uBE

typedef struct tag_skZBV47kFp42DPFCMXy0uBE c9_skZBV47kFp42DPFCMXy0uBE;

#endif                                 /*typedef_c9_skZBV47kFp42DPFCMXy0uBE*/

#ifndef struct_tag_sQBCJjKKgS8ZYkH5STYFEHG
#define struct_tag_sQBCJjKKgS8ZYkH5STYFEHG

struct tag_sQBCJjKKgS8ZYkH5STYFEHG
{
  uint32_T FillGap;
  uint32_T MinLength;
};

#endif                                 /*struct_tag_sQBCJjKKgS8ZYkH5STYFEHG*/

#ifndef typedef_c9_sQBCJjKKgS8ZYkH5STYFEHG
#define typedef_c9_sQBCJjKKgS8ZYkH5STYFEHG

typedef struct tag_sQBCJjKKgS8ZYkH5STYFEHG c9_sQBCJjKKgS8ZYkH5STYFEHG;

#endif                                 /*typedef_c9_sQBCJjKKgS8ZYkH5STYFEHG*/

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_c9_emxArray_real_T
#define typedef_c9_emxArray_real_T

typedef struct emxArray_real_T c9_emxArray_real_T;

#endif                                 /*typedef_c9_emxArray_real_T*/

#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T

struct emxArray_boolean_T
{
  boolean_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_boolean_T*/

#ifndef typedef_c9_emxArray_boolean_T
#define typedef_c9_emxArray_boolean_T

typedef struct emxArray_boolean_T c9_emxArray_boolean_T;

#endif                                 /*typedef_c9_emxArray_boolean_T*/

#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_int32_T*/

#ifndef typedef_c9_emxArray_int32_T
#define typedef_c9_emxArray_int32_T

typedef struct emxArray_int32_T c9_emxArray_int32_T;

#endif                                 /*typedef_c9_emxArray_int32_T*/

#ifndef typedef_c9_coder_internal_sparse
#define typedef_c9_coder_internal_sparse

typedef struct {
  c9_emxArray_boolean_T *d;
  c9_emxArray_int32_T *colidx;
  c9_emxArray_int32_T *rowidx;
  int32_T m;
  int32_T n;
  int32_T maxnz;
} c9_coder_internal_sparse;

#endif                                 /*typedef_c9_coder_internal_sparse*/

#ifndef struct_s1NkunOY9kgapStCcmh6fID_tag
#define struct_s1NkunOY9kgapStCcmh6fID_tag

struct s1NkunOY9kgapStCcmh6fID_tag
{
  c9_emxArray_int32_T *f1;
};

#endif                                 /*struct_s1NkunOY9kgapStCcmh6fID_tag*/

#ifndef typedef_c9_cell_wrap_1
#define typedef_c9_cell_wrap_1

typedef struct s1NkunOY9kgapStCcmh6fID_tag c9_cell_wrap_1;

#endif                                 /*typedef_c9_cell_wrap_1*/

#ifndef struct_emxArray_tag_sBaHy6MF1FZJsDHxMq
#define struct_emxArray_tag_sBaHy6MF1FZJsDHxMq

struct emxArray_tag_sBaHy6MF1FZJsDHxMq
{
  c9_sBaHy6MF1FZJsDHxMqvBaiH data[120];
  int32_T size[1];
};

#endif                                 /*struct_emxArray_tag_sBaHy6MF1FZJsDHxMq*/

#ifndef typedef_c9_emxArray_sBaHy6MF1FZJsDHxMqvBai
#define typedef_c9_emxArray_sBaHy6MF1FZJsDHxMqvBai

typedef struct emxArray_tag_sBaHy6MF1FZJsDHxMq
  c9_emxArray_sBaHy6MF1FZJsDHxMqvBai;

#endif                                 /*typedef_c9_emxArray_sBaHy6MF1FZJsDHxMqvBai*/

#ifndef struct_sbJTpX3y0TlnCbnE98JLarG_tag
#define struct_sbJTpX3y0TlnCbnE98JLarG_tag

struct sbJTpX3y0TlnCbnE98JLarG_tag
{
  c9_emxArray_sBaHy6MF1FZJsDHxMqvBai d;
  int32_T n;
};

#endif                                 /*struct_sbJTpX3y0TlnCbnE98JLarG_tag*/

#ifndef typedef_c9_coder_internal_stack
#define typedef_c9_coder_internal_stack

typedef struct sbJTpX3y0TlnCbnE98JLarG_tag c9_coder_internal_stack;

#endif                                 /*typedef_c9_coder_internal_stack*/

#ifndef struct_emxArray_real32_T
#define struct_emxArray_real32_T

struct emxArray_real32_T
{
  real32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real32_T*/

#ifndef typedef_c9_emxArray_real32_T
#define typedef_c9_emxArray_real32_T

typedef struct emxArray_real32_T c9_emxArray_real32_T;

#endif                                 /*typedef_c9_emxArray_real32_T*/

#ifndef struct_emxArray_uint32_T
#define struct_emxArray_uint32_T

struct emxArray_uint32_T
{
  uint32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_uint32_T*/

#ifndef typedef_c9_emxArray_uint32_T
#define typedef_c9_emxArray_uint32_T

typedef struct emxArray_uint32_T c9_emxArray_uint32_T;

#endif                                 /*typedef_c9_emxArray_uint32_T*/

#ifndef struct_tag_skoeQIuVNKJRHNtBIlOCZhD
#define struct_tag_skoeQIuVNKJRHNtBIlOCZhD

struct tag_skoeQIuVNKJRHNtBIlOCZhD
{
  real_T point1[2];
  real_T point2[2];
  real_T theta;
  real_T rho;
};

#endif                                 /*struct_tag_skoeQIuVNKJRHNtBIlOCZhD*/

#ifndef typedef_c9_skoeQIuVNKJRHNtBIlOCZhD
#define typedef_c9_skoeQIuVNKJRHNtBIlOCZhD

typedef struct tag_skoeQIuVNKJRHNtBIlOCZhD c9_skoeQIuVNKJRHNtBIlOCZhD;

#endif                                 /*typedef_c9_skoeQIuVNKJRHNtBIlOCZhD*/

#ifndef struct_emxArray_tag_skoeQIuVNKJRHNtBIl
#define struct_emxArray_tag_skoeQIuVNKJRHNtBIl

struct emxArray_tag_skoeQIuVNKJRHNtBIl
{
  c9_skoeQIuVNKJRHNtBIlOCZhD *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_tag_skoeQIuVNKJRHNtBIl*/

#ifndef typedef_c9_emxArray_skoeQIuVNKJRHNtBIlOCZh
#define typedef_c9_emxArray_skoeQIuVNKJRHNtBIlOCZh

typedef struct emxArray_tag_skoeQIuVNKJRHNtBIl
  c9_emxArray_skoeQIuVNKJRHNtBIlOCZh;

#endif                                 /*typedef_c9_emxArray_skoeQIuVNKJRHNtBIlOCZh*/

#ifndef struct_tag_sJCxfmxS8gBOONUZjbjUd9E
#define struct_tag_sJCxfmxS8gBOONUZjbjUd9E

struct tag_sJCxfmxS8gBOONUZjbjUd9E
{
  boolean_T CaseSensitivity;
  boolean_T StructExpand;
  char_T PartialMatching[6];
  boolean_T IgnoreNulls;
};

#endif                                 /*struct_tag_sJCxfmxS8gBOONUZjbjUd9E*/

#ifndef typedef_c9_sJCxfmxS8gBOONUZjbjUd9E
#define typedef_c9_sJCxfmxS8gBOONUZjbjUd9E

typedef struct tag_sJCxfmxS8gBOONUZjbjUd9E c9_sJCxfmxS8gBOONUZjbjUd9E;

#endif                                 /*typedef_c9_sJCxfmxS8gBOONUZjbjUd9E*/

#ifndef struct_s1dpkzbGdozyyqRnMzaebiC_tag
#define struct_s1dpkzbGdozyyqRnMzaebiC_tag

struct s1dpkzbGdozyyqRnMzaebiC_tag
{
  c9_cell_wrap_1 tunableEnvironment[2];
};

#endif                                 /*struct_s1dpkzbGdozyyqRnMzaebiC_tag*/

#ifndef typedef_c9_coder_internal_anonymous_function
#define typedef_c9_coder_internal_anonymous_function

typedef struct s1dpkzbGdozyyqRnMzaebiC_tag c9_coder_internal_anonymous_function;

#endif                                 /*typedef_c9_coder_internal_anonymous_function*/

#ifndef typedef_SFc9_LIDAR_simInstanceStruct
#define typedef_SFc9_LIDAR_simInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c9_sfEvent;
  boolean_T c9_doneDoubleBufferReInit;
  uint8_T c9_is_active_c9_LIDAR_sim;
  void *c9_RuntimeVar;
  uint32_T c9_mlFcnLineNumber;
  real_T c9_obstaclesInRegionOfInterest[100000];
  real_T c9_u[100000];
  void *c9_fEmlrtCtx;
  real_T (*c9_b_obstaclesInRegionOfInterest)[100000];
  real_T (*c9_rotationMatrix)[4];
  real_T *c9_minRotation;
  real_T *c9_maxRotation;
  real_T (*c9_invRotationMatrix)[4];
} SFc9_LIDAR_simInstanceStruct;

#endif                                 /*typedef_SFc9_LIDAR_simInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c9_LIDAR_sim_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c9_LIDAR_sim_get_check_sum(mxArray *plhs[]);
extern void c9_LIDAR_sim_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
