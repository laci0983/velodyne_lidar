/* Include files */

#include "LIDAR_sim_sfun.h"
#include "c1_LIDAR_sim.h"
#include <math.h>
#include <string.h>
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "LIDAR_sim_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_debug_family_names[19] = { "matrOfObstaclestemp",
  "drawAllignmentFigures", "obstacleMatrix", "x", "y", "BW", "H", "theta", "rho",
  "P", "lines", "orientationsOfLines", "k", "rotAngle", "nargin", "nargout",
  "matrOfObstacles", "rotationMatrix", "invRotationMatrix" };

static emlrtRTEInfo c1_emlrtRTEI = { 4,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_b_emlrtRTEI = { 6,/* lineNo */
  27,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_c_emlrtRTEI = { 14,/* lineNo */
  9,                                   /* colNo */
  "isnan",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\isnan.m"/* pName */
};

static emlrtRTEInfo c1_d_emlrtRTEI = { 6,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_e_emlrtRTEI = { 7,/* lineNo */
  27,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_f_emlrtRTEI = { 7,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_g_emlrtRTEI = { 34,/* lineNo */
  55,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_h_emlrtRTEI = { 13,/* lineNo */
  5,                                   /* colNo */
  "min",                               /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\min.m"/* pName */
};

static emlrtRTEInfo c1_i_emlrtRTEI = { 34,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_j_emlrtRTEI = { 35,/* lineNo */
  25,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_k_emlrtRTEI = { 1,/* lineNo */
  29,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_l_emlrtRTEI = { 35,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_m_emlrtRTEI = { 24,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_n_emlrtRTEI = { 36,/* lineNo */
  3,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_o_emlrtRTEI = { 27,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_p_emlrtRTEI = { 52,/* lineNo */
  20,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c1_q_emlrtRTEI = { 52,/* lineNo */
  9,                                   /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c1_r_emlrtRTEI = { 36,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_s_emlrtRTEI = { 86,/* lineNo */
  15,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_t_emlrtRTEI = { 37,/* lineNo */
  3,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_u_emlrtRTEI = { 1,/* lineNo */
  34,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c1_v_emlrtRTEI = { 58,/* lineNo */
  32,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c1_w_emlrtRTEI = { 37,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_x_emlrtRTEI = { 38,/* lineNo */
  16,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_y_emlrtRTEI = { 286,/* lineNo */
  35,                                  /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo c1_ab_emlrtRTEI = { 306,/* lineNo */
  5,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo c1_bb_emlrtRTEI = { 38,/* lineNo */
  18,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_cb_emlrtRTEI = { 85,/* lineNo */
  40,                                  /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c1_db_emlrtRTEI = { 38,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_eb_emlrtRTEI = { 43,/* lineNo */
  11,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_fb_emlrtRTEI = { 43,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_gb_emlrtRTEI = { 55,/* lineNo */
  23,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_hb_emlrtRTEI = { 55,/* lineNo */
  2,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_ib_emlrtRTEI = { 55,/* lineNo */
  10,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_jb_emlrtRTEI = { 57,/* lineNo */
  46,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_kb_emlrtRTEI = { 57,/* lineNo */
  17,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_lb_emlrtRTEI = { 57,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_mb_emlrtRTEI = { 58,/* lineNo */
  20,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_nb_emlrtRTEI = { 58,/* lineNo */
  29,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_ob_emlrtRTEI = { 58,/* lineNo */
  33,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_pb_emlrtRTEI = { 58,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_qb_emlrtRTEI = { 69,/* lineNo */
  1,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_rb_emlrtRTEI = { 74,/* lineNo */
  15,                                  /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_sb_emlrtRTEI = { 27,/* lineNo */
  30,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_tb_emlrtRTEI = { 52,/* lineNo */
  1,                                   /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo c1_ub_emlrtRTEI = { 38,/* lineNo */
  9,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_vb_emlrtRTEI = { 58,/* lineNo */
  9,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_wb_emlrtRTEI = { 57,/* lineNo */
  6,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_xb_emlrtRTEI = { 43,/* lineNo */
  6,                                   /* colNo */
  "AllignCoordinateSystem/MATLAB Function1",/* fName */
  "#LIDAR_sim:12"                      /* pName */
};

static emlrtRTEInfo c1_yb_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c1_ac_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "round",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elfun\\round.m"/* pName */
};

static emlrtRTEInfo c1_bc_emlrtRTEI = { 33,/* lineNo */
  14,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_cc_emlrtRTEI = { 120,/* lineNo */
  43,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_dc_emlrtRTEI = { 121,/* lineNo */
  43,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_ec_emlrtRTEI = { 13,/* lineNo */
  5,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\sparfun\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_fc_emlrtRTEI = { 13,/* lineNo */
  1,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\sparfun\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_gc_emlrtRTEI = { 120,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_hc_emlrtRTEI = { 121,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_ic_emlrtRTEI = { 126,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_jc_emlrtRTEI = { 1647,/* lineNo */
  17,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_kc_emlrtRTEI = { 1679,/* lineNo */
  22,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_lc_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "insertionsort",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\insertionsort.m"/* pName */
};

static emlrtRTEInfo c1_mc_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c1_nc_emlrtRTEI = { 48,/* lineNo */
  21,                                  /* colNo */
  "stack",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\stack.m"/* pName */
};

static emlrtRTEInfo c1_oc_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c1_pc_emlrtRTEI = { 40,/* lineNo */
  14,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c1_qc_emlrtRTEI = { 1,/* lineNo */
  18,                                  /* colNo */
  "sortpartition",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\private\\sortpartition.m"/* pName */
};

static emlrtRTEInfo c1_rc_emlrtRTEI = { 1689,/* lineNo */
  14,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_sc_emlrtRTEI = { 1,/* lineNo */
  17,                                  /* colNo */
  "fillIn",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\fillIn.m"/* pName */
};

static emlrtRTEInfo c1_tc_emlrtRTEI = { 69,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_uc_emlrtRTEI = { 75,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_vc_emlrtRTEI = { 673,/* lineNo */
  1,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_wc_emlrtRTEI = { 15,/* lineNo */
  9,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_xc_emlrtRTEI = { 917,/* lineNo */
  11,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_yc_emlrtRTEI = { 110,/* lineNo */
  17,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_ad_emlrtRTEI = { 820,/* lineNo */
  21,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_bd_emlrtRTEI = { 102,/* lineNo */
  6,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_cd_emlrtRTEI = { 102,/* lineNo */
  10,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_dd_emlrtRTEI = { 108,/* lineNo */
  18,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_ed_emlrtRTEI = { 110,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_fd_emlrtRTEI = { 153,/* lineNo */
  13,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_gd_emlrtRTEI = { 42,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_hd_emlrtRTEI = { 690,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_id_emlrtRTEI = { 696,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_jd_emlrtRTEI = { 118,/* lineNo */
  5,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_kd_emlrtRTEI = { 697,/* lineNo */
  37,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_ld_emlrtRTEI = { 50,/* lineNo */
  5,                                   /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo c1_md_emlrtRTEI = { 697,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_nd_emlrtRTEI = { 120,/* lineNo */
  30,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_od_emlrtRTEI = { 723,/* lineNo */
  32,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_pd_emlrtRTEI = { 38,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_qd_emlrtRTEI = { 147,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_rd_emlrtRTEI = { 39,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_sd_emlrtRTEI = { 120,/* lineNo */
  9,                                   /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_td_emlrtRTEI = { 250,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_ud_emlrtRTEI = { 250,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_vd_emlrtRTEI = { 251,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_wd_emlrtRTEI = { 251,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_xd_emlrtRTEI = { 252,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_yd_emlrtRTEI = { 59,/* lineNo */
  9,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_ae_emlrtRTEI = { 1,/* lineNo */
  22,                                  /* colNo */
  "edge",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\edge.m"/* pName */
};

static emlrtRTEInfo c1_be_emlrtRTEI = { 88,/* lineNo */
  17,                                  /* colNo */
  "CannyThresholdingTbbBuildable",     /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\+images\\+internal\\+coder\\+buildable\\CannyThresholdingTbbBuildable.m"/* pName */
};

static emlrtRTEInfo c1_ce_emlrtRTEI = { 80,/* lineNo */
  5,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_de_emlrtRTEI = { 836,/* lineNo */
  5,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_ee_emlrtRTEI = { 30,/* lineNo */
  1,                                   /* colNo */
  "repmat",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\repmat.m"/* pName */
};

static emlrtRTEInfo c1_fe_emlrtRTEI = { 25,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo c1_ge_emlrtRTEI = { 769,/* lineNo */
  5,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_he_emlrtRTEI = { 845,/* lineNo */
  9,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_ie_emlrtRTEI = { 846,/* lineNo */
  14,                                  /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_je_emlrtRTEI = { 846,/* lineNo */
  36,                                  /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_ke_emlrtRTEI = { 769,/* lineNo */
  9,                                   /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_le_emlrtRTEI = { 839,/* lineNo */
  9,                                   /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_me_emlrtRTEI = { 762,/* lineNo */
  14,                                  /* colNo */
  "imfilter",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfilter.m"/* pName */
};

static emlrtRTEInfo c1_ne_emlrtRTEI = { 845,/* lineNo */
  30,                                  /* colNo */
  "padarray",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\padarray.m"/* pName */
};

static emlrtRTEInfo c1_oe_emlrtRTEI = { 19,/* lineNo */
  24,                                  /* colNo */
  "scalexpAllocNoCheck",               /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAllocNoCheck.m"/* pName */
};

static emlrtRTEInfo c1_pe_emlrtRTEI = { 16,/* lineNo */
  9,                                   /* colNo */
  "scalexpAlloc",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAlloc.m"/* pName */
};

static emlrtRTEInfo c1_qe_emlrtRTEI = { 45,/* lineNo */
  6,                                   /* colNo */
  "applyBinaryScalarFunction",         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\applyBinaryScalarFunction.m"/* pName */
};

static emlrtRTEInfo c1_re_emlrtRTEI = { 18,/* lineNo */
  32,                                  /* colNo */
  "scalexpAlloc",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAlloc.m"/* pName */
};

static emlrtRTEInfo c1_se_emlrtRTEI = { 18,/* lineNo */
  34,                                  /* colNo */
  "scalexpAlloc",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAlloc.m"/* pName */
};

static emlrtRTEInfo c1_te_emlrtRTEI = { 12,/* lineNo */
  1,                                   /* colNo */
  "hypot",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elfun\\hypot.m"/* pName */
};

static emlrtRTEInfo c1_ue_emlrtRTEI = { 35,/* lineNo */
  9,                                   /* colNo */
  "scalexpAllocNoCheck",               /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\scalexpAllocNoCheck.m"/* pName */
};

static emlrtRTEInfo c1_ve_emlrtRTEI = { 27,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_we_emlrtRTEI = { 28,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_xe_emlrtRTEI = { 57,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_ye_emlrtRTEI = { 50,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_af_emlrtRTEI = { 52,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_bf_emlrtRTEI = { 60,/* lineNo */
  16,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_cf_emlrtRTEI = { 53,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_df_emlrtRTEI = { 60,/* lineNo */
  26,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_ef_emlrtRTEI = { 54,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_ff_emlrtRTEI = { 60,/* lineNo */
  15,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_gf_emlrtRTEI = { 61,/* lineNo */
  16,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_hf_emlrtRTEI = { 61,/* lineNo */
  26,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_if_emlrtRTEI = { 60,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_jf_emlrtRTEI = { 397,/* lineNo */
  11,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_kf_emlrtRTEI = { 65,/* lineNo */
  7,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_lf_emlrtRTEI = { 66,/* lineNo */
  7,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_mf_emlrtRTEI = { 41,/* lineNo */
  30,                                  /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c1_nf_emlrtRTEI = { 41,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_of_emlrtRTEI = { 46,/* lineNo */
  1,                                   /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c1_pf_emlrtRTEI = { 48,/* lineNo */
  23,                                  /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c1_qf_emlrtRTEI = { 48,/* lineNo */
  17,                                  /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c1_rf_emlrtRTEI = { 48,/* lineNo */
  5,                                   /* colNo */
  "sub2ind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo c1_sf_emlrtRTEI = { 70,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_tf_emlrtRTEI = { 71,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_uf_emlrtRTEI = { 72,/* lineNo */
  1,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_vf_emlrtRTEI = { 85,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_wf_emlrtRTEI = { 82,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_xf_emlrtRTEI = { 11,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_yf_emlrtRTEI = { 16,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_ag_emlrtRTEI = { 17,/* lineNo */
  5,                                   /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_bg_emlrtRTEI = { 1,/* lineNo */
  24,                                  /* colNo */
  "bwselect",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\bwselect.m"/* pName */
};

static emlrtRTEInfo c1_cg_emlrtRTEI = { 33,/* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c1_dg_emlrtRTEI = { 1,/* lineNo */
  27,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_eg_emlrtRTEI = { 81,/* lineNo */
  7,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c1_fg_emlrtRTEI = { 46,/* lineNo */
  1,                                   /* colNo */
  "hough",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\hough.m"/* pName */
};

static emlrtRTEInfo c1_gg_emlrtRTEI = { 210,/* lineNo */
  5,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c1_hg_emlrtRTEI = { 74,/* lineNo */
  1,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c1_ig_emlrtRTEI = { 1,/* lineNo */
  18,                                  /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c1_jg_emlrtRTEI = { 173,/* lineNo */
  5,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c1_kg_emlrtRTEI = { 82,/* lineNo */
  5,                                   /* colNo */
  "houghpeaks",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghpeaks.m"/* pName */
};

static emlrtRTEInfo c1_lg_emlrtRTEI = { 74,/* lineNo */
  15,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_mg_emlrtRTEI = { 159,/* lineNo */
  60,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_ng_emlrtRTEI = { 76,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_og_emlrtRTEI = { 213,/* lineNo */
  52,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_pg_emlrtRTEI = { 108,/* lineNo */
  5,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_qg_emlrtRTEI = { 137,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_rg_emlrtRTEI = { 240,/* lineNo */
  51,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_sg_emlrtRTEI = { 115,/* lineNo */
  15,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_tg_emlrtRTEI = { 120,/* lineNo */
  9,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_ug_emlrtRTEI = { 286,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_vg_emlrtRTEI = { 121,/* lineNo */
  9,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_wg_emlrtRTEI = { 128,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_xg_emlrtRTEI = { 128,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_yg_emlrtRTEI = { 129,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_ah_emlrtRTEI = { 129,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_bh_emlrtRTEI = { 130,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_ch_emlrtRTEI = { 130,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_dh_emlrtRTEI = { 131,/* lineNo */
  27,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_eh_emlrtRTEI = { 131,/* lineNo */
  13,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_fh_emlrtRTEI = { 84,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_gh_emlrtRTEI = { 87,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_hh_emlrtRTEI = { 90,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_ih_emlrtRTEI = { 93,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_jh_emlrtRTEI = { 115,/* lineNo */
  5,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_kh_emlrtRTEI = { 1,/* lineNo */
  18,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_lh_emlrtRTEI = { 108,/* lineNo */
  18,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_mh_emlrtRTEI = { 213,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_nh_emlrtRTEI = { 203,/* lineNo */
  23,                                  /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_oh_emlrtRTEI = { 302,/* lineNo */
  1,                                   /* colNo */
  "houghlines",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\houghlines.m"/* pName */
};

static emlrtRTEInfo c1_ph_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_qh_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo c1_rh_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_sh_emlrtRTEI = { 482,/* lineNo */
  32,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_th_emlrtRTEI = { 520,/* lineNo */
  32,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_uh_emlrtRTEI = { 297,/* lineNo */
  5,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c1_vh_emlrtRTEI = { 25,/* lineNo */
  13,                                  /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c1_wh_emlrtRTEI = { 38,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_xh_emlrtRTEI = { 19,/* lineNo */
  14,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c1_yh_emlrtRTEI = { 14,/* lineNo */
  25,                                  /* colNo */
  "anonymous_function",                /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\anonymous_function.m"/* pName */
};

static emlrtRTEInfo c1_ai_emlrtRTEI = { 1683,/* lineNo */
  23,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_bi_emlrtRTEI = { 1684,/* lineNo */
  23,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_ci_emlrtRTEI = { 1682,/* lineNo */
  36,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_di_emlrtRTEI = { 18,/* lineNo */
  13,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c1_ei_emlrtRTEI = { 36,/* lineNo */
  14,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c1_fi_emlrtRTEI = { 37,/* lineNo */
  50,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c1_gi_emlrtRTEI = { 34,/* lineNo */
  13,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo c1_hi_emlrtRTEI = { 34,/* lineNo */
  52,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c1_ii_emlrtRTEI = { 47,/* lineNo */
  56,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c1_ji_emlrtRTEI = { 51,/* lineNo */
  45,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c1_ki_emlrtRTEI = { 49,/* lineNo */
  51,                                  /* colNo */
  "introsort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\introsort.m"/* pName */
};

static emlrtRTEInfo c1_li_emlrtRTEI = { 35,/* lineNo */
  35,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c1_mi_emlrtRTEI = { 26,/* lineNo */
  58,                                  /* colNo */
  "heapsort",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\heapsort.m"/* pName */
};

static emlrtRTEInfo c1_ni_emlrtRTEI = { 1691,/* lineNo */
  5,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo c1_oi_emlrtRTEI = { 166,/* lineNo */
  9,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c1_pi_emlrtRTEI = { 16,/* lineNo */
  9,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c1_qi_emlrtRTEI = { 164,/* lineNo */
  9,                                   /* colNo */
  "nullAssignment",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\nullAssignment.m"/* pName */
};

static emlrtRTEInfo c1_ri_emlrtRTEI = { 243,/* lineNo */
  18,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_si_emlrtRTEI = { 238,/* lineNo */
  20,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_ti_emlrtRTEI = { 243,/* lineNo */
  36,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_ui_emlrtRTEI = { 94,/* lineNo */
  17,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_vi_emlrtRTEI = { 243,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_wi_emlrtRTEI = { 199,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_xi_emlrtRTEI = { 201,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_yi_emlrtRTEI = { 202,/* lineNo */
  12,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_aj_emlrtRTEI = { 202,/* lineNo */
  25,                                  /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_bj_emlrtRTEI = { 204,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_cj_emlrtRTEI = { 30,/* lineNo */
  9,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_dj_emlrtRTEI = { 131,/* lineNo */
  9,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_ej_emlrtRTEI = { 135,/* lineNo */
  5,                                   /* colNo */
  "imfill",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\images\\images\\eml\\imfill.m"/* pName */
};

static emlrtRTEInfo c1_fj_emlrtRTEI = { 28,/* lineNo */
  5,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo c1_gj_emlrtRTEI = { 56,/* lineNo */
  1,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo c1_hj_emlrtRTEI = { 61,/* lineNo */
  5,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_ij_emlrtRTEI = { 385,/* lineNo */
  9,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_jj_emlrtRTEI = { 308,/* lineNo */
  1,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_kj_emlrtRTEI = { 386,/* lineNo */
  1,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_lj_emlrtRTEI = { 387,/* lineNo */
  9,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_mj_emlrtRTEI = { 388,/* lineNo */
  1,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_nj_emlrtRTEI = { 308,/* lineNo */
  14,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo c1_oj_emlrtRTEI = { 308,/* lineNo */
  20,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static const char_T c1_cv0[39] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'm', 'i', 'n', '_', 'o', 'r', '_',
  'm', 'a', 'x', '_', 'v', 'a', 'r', 'D', 'i', 'm', 'Z', 'e', 'r', 'o' };

static const char_T c1_cv1[30] = { 'C', 'o', 'd', 'e', 'r', ':', 'b', 'u', 'i',
  'l', 't', 'i', 'n', 's', ':', 'A', 's', 's', 'e', 'r', 't', 'i', 'o', 'n', 'F',
  'a', 'i', 'l', 'e', 'd' };

static const char_T c1_cv2[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'f', 'i', 'n', 'd', '_', 'i', 'n', 'c', 'o', 'm', 'p',
  'a', 't', 'i', 'b', 'l', 'e', 'S', 'h', 'a', 'p', 'e' };

static const char_T c1_cv3[30] = { 'C', 'o', 'd', 'e', 'r', ':', 'F', 'E', ':',
  'P', 'o', 't', 'e', 'n', 't', 'i', 'a', 'l', 'V', 'e', 'c', 't', 'o', 'r', 'V',
  'e', 'c', 't', 'o', 'r' };

static const char_T c1_cv4[46] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N',
  'o', 'n', 'N', 'a', 'N' };

static const char_T c1_cv5[47] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'I',
  'n', 't', 'e', 'g', 'e', 'r' };

static const char_T c1_cv6[15] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'p', 'm',
  'a', 'x', 's', 'i', 'z', 'e' };

static const char_T c1_cv7[48] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N',
  'o', 'n', 'e', 'm', 'p', 't', 'y' };

static const char_T c1_cv8[19] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm',
  'b', 'e', 'r', ' ', '1', ',', ' ', 'B', 'W', ',' };

static const char_T c1_cv9[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o',
  'u', 'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
  'd', 'F', 'i', 'n', 'i', 't', 'e' };

static const char_T c1_cv10[46] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'F',
  'i', 'n', 'i', 't', 'e' };

static const char_T c1_cv11[33] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o',
  'u', 'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
  'd', 'I', 'n', 't', 'e', 'g', 'e', 'r' };

static const char_T c1_cv12[48] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o',
  'l', 'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
  'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'P',
  'o', 's', 'i', 't', 'i', 'v', 'e' };

static const char_T c1_cv13[25] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'u',
  'b', 's', 'd', 'e', 'l', 'd', 'i', 'm', 'm', 'i', 's', 'm', 'a', 't', 'c', 'h'
};

/* Function Declarations */
static void initialize_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void initialize_params_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance);
static void enable_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void disable_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance);
static void set_sim_state_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance, const mxArray *c1_st);
static void finalize_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void sf_gateway_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void mdl_start_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_chartstep_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance);
static void initSimStructsc1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData);
static void c1_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData);
static void c1_b_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_boolean_T *c1_inData);
static void c1_c_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_boolean_T *c1_y);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_boolean_T *c1_outData);
static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData);
static void c1_d_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData);
static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData);
static void c1_e_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y);
static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData);
static const mxArray *c1_h_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData);
static void c1_f_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y);
static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData);
static const mxArray *c1_i_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_inData);
static void c1_g_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_y);
static void c1_h_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[2]);
static real_T c1_i_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName,
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_outData);
static const mxArray *c1_j_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData);
static void c1_j_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y);
static void c1_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData);
static void c1_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_k_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_l_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_k_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_rotationMatrix, const char_T *c1_identifier, real_T c1_y[4]);
static void c1_l_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[4]);
static void c1_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static void c1_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, boolean_T c1_idx_data[], int32_T c1_idx_size[1],
  c1_emxArray_real_T *c1_b_x);
static void c1_check_forloop_overflow_error(SFc1_LIDAR_simInstanceStruct
  *chartInstance, boolean_T c1_overflow);
static void c1_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_real_T *c1_x, c1_emxArray_real_T *c1_b_x);
static boolean_T c1_sortLE(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_v, int32_T c1_dir_data[], int32_T c1_dir_size[2],
  int32_T c1_idx1, int32_T c1_idx2);
static void c1_apply_row_permutation(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_y, c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T
  *c1_b_y);
static boolean_T c1_rows_differ(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_b, int32_T c1_k0, int32_T c1_k);
static void c1_b_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_real_T *c1_b_x);
static void c1_sparse(SFc1_LIDAR_simInstanceStruct *chartInstance,
                      c1_emxArray_real_T *c1_varargin_1, c1_emxArray_real_T
                      *c1_varargin_2, c1_coder_internal_sparse *c1_y);
static void c1_assertValidIndexArg(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_s, c1_emxArray_int32_T *c1_sint);
static void c1_locSortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_a, c1_emxArray_int32_T
  *c1_b, c1_emxArray_int32_T *c1_b_idx, c1_emxArray_int32_T *c1_b_a,
  c1_emxArray_int32_T *c1_b_b);
static void c1_insertionsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x);
static boolean_T c1___anon_fcn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_a, c1_emxArray_int32_T *c1_b, int32_T c1_i, int32_T
  c1_j);
static int32_T c1_nextpow2(SFc1_LIDAR_simInstanceStruct *chartInstance, int32_T
  c1_n);
static void c1_introsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x);
static void c1_stack_stack(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_sBaHy6MF1FZJsDHxMqvBaiH c1_eg, int32_T c1_n, c1_coder_internal_stack
  *c1_this);
static void c1_heapsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x);
static void c1_heapify(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_idx, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x);
static void c1_sortpartition(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, int32_T *c1_p,
  c1_emxArray_int32_T *c1_b_x);
static void c1_permuteVector(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_y, c1_emxArray_int32_T
  *c1_b_y);
static void c1_sparse_fillIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_coder_internal_sparse c1_this, c1_coder_internal_sparse *c1_b_this);
static void c1_edge(SFc1_LIDAR_simInstanceStruct *chartInstance,
                    c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_boolean_T *
                    c1_varargout_1);
static void c1_error(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_padImage(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T *c1_a_tmp, real_T c1_pad[2], c1_emxArray_real32_T *c1_a);
static boolean_T c1_all(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T
  c1_a[2]);
static void c1_hypot(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_real32_T *c1_x, c1_emxArray_real32_T *c1_y,
                     c1_emxArray_real32_T *c1_r);
static boolean_T c1_dimagree(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T *c1_z, c1_emxArray_real32_T *c1_varargin_1,
  c1_emxArray_real32_T *c1_varargin_2);
static void c1_warning(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_b_warning(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_bwselect(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T *c1_varargin_2,
  c1_emxArray_real_T *c1_varargin_3, c1_emxArray_boolean_T *c1_varargout_1);
static void c1_c_warning(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_b_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T
  *c1_b_x);
static boolean_T c1_allinrange(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, real_T c1_lo, int32_T c1_hi);
static void c1_imfill(SFc1_LIDAR_simInstanceStruct *chartInstance,
                      c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T
                      *c1_varargin_2, c1_emxArray_boolean_T *c1_I2);
static void c1_d_warning(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void c1_hough(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T
                     *c1_h, c1_emxArray_real_T *c1_rho);
static void c1_houghpeaks(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_varargin_1, real_T c1_varargin_4, c1_emxArray_real_T
  *c1_peaks);
static void c1_validateattributes(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_a);
static void c1_diff(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T c1_x[179],
                    real_T c1_y[178]);
static void c1_houghlines(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T *c1_varargin_3,
  c1_emxArray_real_T *c1_varargin_4, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh
  *c1_lines);
static void c1_sortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_y, real_T c1_varargin_1[2], c1_emxArray_int32_T
  *c1_b_y);
static boolean_T c1_b_sortLE(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_v, int32_T c1_dir[2], int32_T c1_idx1, int32_T c1_idx2);
static void c1_sort(SFc1_LIDAR_simInstanceStruct *chartInstance,
                    c1_emxArray_real_T *c1_x, c1_emxArray_real_T *c1_b_x);
static void c1_sortIdx(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T
  *c1_b_x);
static void c1_merge_block(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x, int32_T c1_offset,
  int32_T c1_n, int32_T c1_preSortLevel, c1_emxArray_int32_T *c1_iwork,
  c1_emxArray_real_T *c1_xwork, c1_emxArray_int32_T *c1_b_idx,
  c1_emxArray_real_T *c1_b_x, c1_emxArray_int32_T *c1_b_iwork,
  c1_emxArray_real_T *c1_b_xwork);
static void c1_merge(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x,
                     int32_T c1_offset, int32_T c1_np, int32_T c1_nq,
                     c1_emxArray_int32_T *c1_iwork, c1_emxArray_real_T *c1_xwork,
                     c1_emxArray_int32_T *c1_b_idx, c1_emxArray_real_T *c1_b_x,
                     c1_emxArray_int32_T *c1_b_iwork, c1_emxArray_real_T
                     *c1_b_xwork);
static real_T c1_cosd(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T c1_x);
static real_T c1_sind(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T c1_x);
static const mxArray *c1_emlrt_marshallOut(SFc1_LIDAR_simInstanceStruct
  *chartInstance, const char_T c1_b_u[30]);
static const mxArray *c1_b_emlrt_marshallOut(SFc1_LIDAR_simInstanceStruct
  *chartInstance, const char_T c1_b_u[14]);
static const mxArray *c1_m_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static int32_T c1_m_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_k_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static uint8_T c1_n_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_is_active_c1_LIDAR_sim, const char_T *c1_identifier);
static uint8_T c1_o_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_c_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, boolean_T c1_idx_data[], int32_T c1_idx_size[1]);
static void c1_c_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x);
static void c1_b_apply_row_permutation(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real_T *c1_y, c1_emxArray_int32_T *c1_idx);
static void c1_d_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x);
static void c1_b_locSortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_a, c1_emxArray_int32_T
  *c1_b);
static void c1_b_insertionsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp);
static void c1_b_introsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp);
static void c1_b_heapsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp);
static void c1_b_heapify(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_idx, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp);
static int32_T c1_b_sortpartition(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp);
static void c1_b_permuteVector(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_y);
static void c1_b_sparse_fillIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_coder_internal_sparse *c1_this);
static void c1_d_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx);
static void c1_b_imfill(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T *c1_varargin_2);
static void c1_b_sortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_y, real_T c1_varargin_1[2]);
static void c1_b_sort(SFc1_LIDAR_simInstanceStruct *chartInstance,
                      c1_emxArray_real_T *c1_x);
static void c1_b_sortIdx(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx);
static void c1_b_merge_block(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x, int32_T c1_offset,
  int32_T c1_n, int32_T c1_preSortLevel, c1_emxArray_int32_T *c1_iwork,
  c1_emxArray_real_T *c1_xwork);
static void c1_b_merge(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x, int32_T c1_offset,
  int32_T c1_np, int32_T c1_nq, c1_emxArray_int32_T *c1_iwork,
  c1_emxArray_real_T *c1_xwork);
static void c1_b_cosd(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T *c1_x);
static void c1_b_sind(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T *c1_x);
static void c1_emxEnsureCapacity_real_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_real_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_int32_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_int32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_boolean_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_boolean_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_skoeQIuVNKJRH(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_emxArray, int32_T
  c1_oldNumel, const emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_real_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_boolean_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_skoeQIuVNKJRHNtBIlOCZhD(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh **c1_pEmxArray, int32_T
  c1_numDimensions, const emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_real_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_int32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInitStruct_coder_internal_sp(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_sparse *c1_pStruct, const emlrtRTEInfo
  *c1_srcLocation);
static void c1_emxInit_boolean_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxFree_real_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T **c1_pEmxArray);
static void c1_emxFree_boolean_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_pEmxArray);
static void c1_emxFree_skoeQIuVNKJRHNtBIlOCZhD(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh **c1_pEmxArray);
static void c1_emxFree_int32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_pEmxArray);
static void c1_emxFreeStruct_coder_internal_sp(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_sparse *c1_pStruct);
static void c1_emxEnsureCapacity_boolean_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_boolean_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxCopyStruct_coder_internal_an(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_anonymous_function *c1_dst, const
  c1_coder_internal_anonymous_function *c1_src, const emlrtRTEInfo
  *c1_srcLocation);
static void c1_emxCopyMatrix_real_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const emlrtRTEInfo *c1_srcLocation);
static void c1_emxCopyMatrix_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 c1_dst[2], const c1_cell_wrap_1 c1_src[2],
  const emlrtRTEInfo *c1_srcLocation);
static void c1_emxCopyStruct_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 *c1_dst, const c1_cell_wrap_1 *c1_src, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxCopy_int32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_dst, c1_emxArray_int32_T * const *c1_src, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInitStruct_coder_internal_an(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_anonymous_function *c1_pStruct, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInitMatrix_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 c1_pMatrix[2], const emlrtRTEInfo
  *c1_srcLocation);
static void c1_emxInitStruct_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 *c1_pStruct, const emlrtRTEInfo *c1_srcLocation);
static void c1_emxFreeMatrix_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 c1_pMatrix[2]);
static void c1_emxFreeStruct_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 *c1_pStruct);
static void c1_emxFreeStruct_coder_internal_an(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_anonymous_function *c1_pStruct);
static void c1_emxCopyStruct_coder_internal_sp(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_sparse *c1_dst, const
  c1_coder_internal_sparse *c1_src, const emlrtRTEInfo *c1_srcLocation);
static void c1_emxCopy_boolean_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_dst, c1_emxArray_boolean_T * const *c1_src, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_real32_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_real32_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_real32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_real32_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxFree_real32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T **c1_pEmxArray);
static void c1_emxEnsureCapacity_int32_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_int32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxEnsureCapacity_uint32_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_uint32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_int32_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxInit_uint32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_uint32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation);
static void c1_emxFree_uint32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_uint32_T **c1_pEmxArray);
static int32_T c1_div_nzp_s32(SFc1_LIDAR_simInstanceStruct *chartInstance,
  int32_T c1_numerator, int32_T c1_denominator, uint32_T c1_ssid_src_loc,
  int32_T c1_offset_src_loc, int32_T c1_length_src_loc);
static int32_T c1__s32_s64_(SFc1_LIDAR_simInstanceStruct *chartInstance, int64_T
  c1_b, uint32_T c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T
  c1_length_src_loc);
static int32_T c1__s32_d_(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T
  c1_b, uint32_T c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T
  c1_length_src_loc);
static void init_dsm_address_info(SFc1_LIDAR_simInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc1_LIDAR_simInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  emlrtLicenseCheckR2012b(chartInstance->c1_fEmlrtCtx, "Image_Toolbox", 2);
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc1_LIDAR_sim(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  sim_mode_is_external(chartInstance->S);
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_is_active_c1_LIDAR_sim = 0U;
  setLegacyDebuggerFlagForRuntime(chartInstance->S, true);
}

static void initialize_params_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_debugger_state_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  const mxArray *c1_b_y = NULL;
  const mxArray *c1_c_y = NULL;
  uint8_T c1_hoistedGlobal;
  const mxArray *c1_d_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(3, 1), false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", *chartInstance->c1_invRotationMatrix,
    0, 0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", *chartInstance->c1_rotationMatrix, 0,
    0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_setcell(c1_y, 1, c1_c_y);
  c1_hoistedGlobal = chartInstance->c1_is_active_c1_LIDAR_sim;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_hoistedGlobal, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c1_y, 2, c1_d_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance, const mxArray *c1_st)
{
  const mxArray *c1_b_u;
  real_T c1_dv0[4];
  int32_T c1_i0;
  real_T c1_dv1[4];
  int32_T c1_i1;
  chartInstance->c1_doneDoubleBufferReInit = true;
  c1_b_u = sf_mex_dup(c1_st);
  c1_k_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_b_u, 0)),
                        "invRotationMatrix", c1_dv0);
  for (c1_i0 = 0; c1_i0 < 4; c1_i0++) {
    (*chartInstance->c1_invRotationMatrix)[c1_i0] = c1_dv0[c1_i0];
  }

  c1_k_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_b_u, 1)),
                        "rotationMatrix", c1_dv1);
  for (c1_i1 = 0; c1_i1 < 4; c1_i1++) {
    (*chartInstance->c1_rotationMatrix)[c1_i1] = c1_dv1[c1_i1];
  }

  chartInstance->c1_is_active_c1_LIDAR_sim = c1_n_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_b_u, 2)), "is_active_c1_LIDAR_sim");
  sf_mex_destroy(&c1_b_u);
  c1_update_debugger_state_c1_LIDAR_sim(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void finalize_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  sfListenerLightTerminate(chartInstance->c1_RuntimeVar);
}

static void sf_gateway_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  int32_T c1_i2;
  int32_T c1_i3;
  int32_T c1_i4;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0, chartInstance->c1_sfEvent);
  for (c1_i2 = 0; c1_i2 < 100000; c1_i2++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c1_b_matrOfObstacles)[c1_i2], 0U);
  }

  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_chartstep_c1_LIDAR_sim(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  for (c1_i3 = 0; c1_i3 < 4; c1_i3++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c1_rotationMatrix)[c1_i3], 1U);
  }

  for (c1_i4 = 0; c1_i4 < 4; c1_i4++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c1_invRotationMatrix)[c1_i4], 2U);
  }
}

static void mdl_start_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  chartInstance->c1_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sim_mode_is_external(chartInstance->S);
}

static void c1_chartstep_c1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance)
{
  c1_emxArray_real_T *c1_matrOfObstaclestemp;
  c1_emxArray_real_T *c1_obstacleMatrix;
  c1_emxArray_real_T *c1_x;
  c1_emxArray_real_T *c1_y;
  c1_emxArray_boolean_T *c1_BW;
  c1_emxArray_real_T *c1_H;
  c1_emxArray_real_T *c1_rho;
  c1_emxArray_real_T *c1_P;
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_lines;
  c1_emxArray_real_T *c1_orientationsOfLines;
  int32_T c1_i5;
  uint32_T c1_debug_family_var_map[19];
  real_T c1_drawAllignmentFigures;
  real_T c1_theta[181];
  real_T c1_k;
  real_T c1_rotAngle;
  real_T c1_nargin = 1.0;
  real_T c1_nargout = 2.0;
  real_T c1_b_rotationMatrix[4];
  real_T c1_b_invRotationMatrix[4];
  int32_T c1_i6;
  int32_T c1_i7;
  c1_emxArray_real_T *c1_b_x;
  int32_T c1_i8;
  int32_T c1_i9;
  int32_T c1_b_size[1];
  int32_T c1_loop_ub;
  int32_T c1_i10;
  c1_emxArray_real_T *c1_varargin_1;
  boolean_T c1_b_data[50000];
  int32_T c1_i11;
  int32_T c1_b_loop_ub;
  int32_T c1_i12;
  int32_T c1_i13;
  int32_T c1_c_loop_ub;
  int32_T c1_i14;
  int32_T c1_i15;
  int32_T c1_i16;
  int32_T c1_d_loop_ub;
  int32_T c1_i17;
  int32_T c1_e_loop_ub;
  int32_T c1_i18;
  int32_T c1_i19;
  int32_T c1_f_loop_ub;
  int32_T c1_i20;
  int32_T c1_i21;
  int32_T c1_g_loop_ub;
  int32_T c1_i22;
  int32_T c1_i23;
  int32_T c1_h_loop_ub;
  int32_T c1_i24;
  boolean_T c1_b0;
  const mxArray *c1_b_y = NULL;
  static char_T c1_cv14[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  const mxArray *c1_c_y = NULL;
  const mxArray *c1_d_y = NULL;
  int32_T c1_m;
  const mxArray *c1_e_y = NULL;
  int32_T c1_j;
  real_T c1_minval_data[2];
  int32_T c1_b_j;
  real_T c1_c_x;
  boolean_T c1_b;
  real_T c1_minval;
  int32_T c1_b_b;
  int32_T c1_c_b;
  real_T c1_d_x;
  real_T c1_e_x;
  boolean_T c1_d_b;
  real_T c1_f_x;
  boolean_T c1_overflow;
  real_T c1_g_x;
  real_T c1_f_y;
  int32_T c1_i25;
  int32_T c1_i;
  int32_T c1_i_loop_ub;
  real_T c1_a;
  int32_T c1_i26;
  real_T c1_e_b;
  real_T c1_b_a;
  real_T c1_f_b;
  real_T c1_h_x;
  int32_T c1_i27;
  boolean_T c1_g_b;
  real_T c1_i_x;
  boolean_T c1_p;
  boolean_T c1_h_b;
  int32_T c1_j_loop_ub;
  int32_T c1_i28;
  int32_T c1_i29;
  int32_T c1_k_loop_ub;
  int32_T c1_i30;
  c1_emxArray_real_T *c1_i_b;
  int32_T c1_i31;
  int32_T c1_i32;
  int32_T c1_l_loop_ub;
  int32_T c1_m_loop_ub;
  int32_T c1_i33;
  int32_T c1_i34;
  int32_T c1_b_k;
  c1_emxArray_real_T *c1_b_varargin_1;
  c1_emxArray_int32_T *c1_idx;
  int32_T c1_c_k;
  int32_T c1_i35;
  int32_T c1_n;
  int32_T c1_col_data[2];
  int32_T c1_i36;
  int32_T c1_i37;
  int32_T c1_n_loop_ub;
  int32_T c1_o_loop_ub;
  int32_T c1_i38;
  int32_T c1_i39;
  c1_emxArray_real_T *c1_b_rho;
  c1_emxArray_int32_T *c1_b_idx;
  c1_emxArray_real_T *c1_r0;
  int32_T c1_b_n;
  int32_T c1_i40;
  int32_T c1_i41;
  int32_T c1_i42;
  int32_T c1_p_loop_ub;
  int32_T c1_i43;
  int32_T c1_q_loop_ub;
  int32_T c1_i44;
  c1_emxArray_int32_T *c1_iwork;
  int32_T c1_len;
  int32_T c1_i45;
  int32_T c1_i46;
  int32_T c1_iv0[1];
  int32_T c1_i47;
  int32_T c1_r_loop_ub;
  int32_T c1_i48;
  int32_T c1_np1;
  int32_T c1_i49;
  int32_T c1_i50;
  int32_T c1_j_b;
  int32_T c1_k_b;
  int32_T c1_s_loop_ub;
  boolean_T c1_b_overflow;
  int32_T c1_i51;
  int32_T c1_d_k;
  int32_T c1_i52;
  c1_emxArray_real_T *c1_l_b;
  int32_T c1_i53;
  int32_T c1_i54;
  int32_T c1_t_loop_ub;
  int32_T c1_i55;
  int32_T c1_b_i;
  c1_emxArray_real_T *c1_m_b;
  int32_T c1_u_loop_ub;
  int32_T c1_i56;
  int32_T c1_i57;
  int32_T c1_i58;
  int32_T c1_c_a;
  int32_T c1_i2;
  int32_T c1_col_size[2];
  int32_T c1_c_j;
  int32_T c1_v_loop_ub;
  int32_T c1_i59;
  int32_T c1_pEnd;
  int32_T c1_i60;
  int32_T c1_i61;
  int32_T c1_b_p;
  int32_T c1_b_col_data[2];
  int32_T c1_w_loop_ub;
  int32_T c1_q;
  int32_T c1_i62;
  int32_T c1_qEnd;
  int32_T c1_i63;
  int32_T c1_e_k;
  int32_T c1_x_loop_ub;
  int32_T c1_kEnd;
  int32_T c1_i64;
  int32_T c1_nb;
  int32_T c1_khi;
  int32_T c1_f_k;
  int32_T c1_i65;
  c1_emxArray_real_T *c1_b_obstacleMatrix;
  int32_T c1_g_k;
  int32_T c1_i66;
  int32_T c1_k0;
  int32_T c1_y_loop_ub;
  int32_T c1_i67;
  int32_T c1_ab_loop_ub;
  int32_T c1_d_a;
  int32_T c1_i68;
  boolean_T c1_b1;
  int32_T c1_b_col_size[2];
  int32_T c1_i69;
  int32_T c1_i70;
  c1_emxArray_real_T *c1_j_x;
  int32_T c1_e_a;
  int32_T c1_i71;
  c1_emxArray_real_T *c1_c_obstacleMatrix;
  int32_T c1_i72;
  int32_T c1_i73;
  int32_T c1_d_j;
  int32_T c1_bb_loop_ub;
  int32_T c1_c_col_data[2];
  int32_T c1_i74;
  int32_T c1_f_a;
  int32_T c1_e_j;
  int32_T c1_cb_loop_ub;
  int32_T c1_i75;
  int32_T c1_i76;
  int32_T c1_i77;
  int32_T c1_g_a;
  int32_T c1_n_b;
  int32_T c1_i78;
  int32_T c1_db_loop_ub;
  int32_T c1_h_a;
  c1_emxArray_real_T *c1_g_y;
  int32_T c1_i79;
  int32_T c1_o_b;
  int32_T c1_i80;
  boolean_T c1_c_overflow;
  int32_T c1_eb_loop_ub;
  int32_T c1_i81;
  int32_T c1_fb_loop_ub;
  int32_T c1_i82;
  int32_T c1_p_b;
  c1_coder_internal_sparse c1_this;
  int32_T c1_q_b;
  real_T c1_dv2[2];
  boolean_T c1_d_overflow;
  int32_T c1_i83;
  int32_T c1_gb_loop_ub;
  int32_T c1_i84;
  int32_T c1_i85;
  int32_T c1_r_b;
  int32_T c1_s_b;
  boolean_T c1_e_overflow;
  int32_T c1_c;
  int32_T c1_b_c;
  c1_emxArray_boolean_T *c1_b_BW;
  int32_T c1_cend;
  int32_T c1_i86;
  int32_T c1_i87;
  int32_T c1_i_a;
  int32_T c1_t_b;
  int32_T c1_j_a;
  int32_T c1_u_b;
  int32_T c1_hb_loop_ub;
  int32_T c1_i88;
  boolean_T c1_f_overflow;
  c1_emxArray_boolean_T *c1_r1;
  int32_T c1_c_idx;
  int32_T c1_i89;
  int32_T c1_ib_loop_ub;
  int32_T c1_i90;
  c1_emxArray_boolean_T *c1_c_BW;
  int32_T c1_i91;
  int32_T c1_jb_loop_ub;
  int32_T c1_i92;
  c1_emxArray_real_T *c1_b_H;
  int32_T c1_i93;
  int32_T c1_kb_loop_ub;
  int32_T c1_i94;
  int32_T c1_i95;
  int32_T c1_i96;
  int32_T c1_lb_loop_ub;
  int32_T c1_i97;
  int32_T c1_i98;
  int32_T c1_mb_loop_ub;
  int32_T c1_i99;
  boolean_T c1_b2;
  const mxArray *c1_h_y = NULL;
  static char_T c1_cv15[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  const mxArray *c1_i_y = NULL;
  const mxArray *c1_j_y = NULL;
  int32_T c1_c_n;
  const mxArray *c1_k_y = NULL;
  int32_T c1_d_n;
  real_T c1_k_x;
  real_T c1_maxval;
  real_T c1_l_x;
  real_T c1_m_x;
  boolean_T c1_v_b;
  boolean_T c1_w_b;
  boolean_T c1_c_p;
  c1_emxArray_real_T *c1_c_H;
  real_T c1_n_x;
  real_T c1_o_x;
  int32_T c1_d_idx;
  boolean_T c1_x_b;
  real_T c1_p_x;
  int32_T c1_y_b;
  int32_T c1_ab_b;
  int32_T c1_i100;
  int32_T c1_first;
  boolean_T c1_g_overflow;
  int32_T c1_last;
  real_T c1_ex;
  int32_T c1_i101;
  int32_T c1_nb_loop_ub;
  int32_T c1_k_a;
  int32_T c1_i102;
  int32_T c1_h_k;
  int32_T c1_bb_b;
  int32_T c1_l_a;
  int32_T c1_cb_b;
  c1_emxArray_real_T *c1_r2;
  real_T c1_q_x;
  real_T c1_r_x;
  boolean_T c1_h_overflow;
  int32_T c1_i103;
  boolean_T c1_db_b;
  boolean_T c1_d_p;
  int32_T c1_i_k;
  int32_T c1_ob_loop_ub;
  int32_T c1_i104;
  c1_emxArray_boolean_T *c1_d_BW;
  int32_T c1_i105;
  int32_T c1_pb_loop_ub;
  int32_T c1_i106;
  c1_emxArray_real_T *c1_c_rho;
  int32_T c1_i107;
  int32_T c1_qb_loop_ub;
  int32_T c1_i108;
  c1_emxArray_real_T *c1_b_P;
  int32_T c1_i109;
  int32_T c1_rb_loop_ub;
  int32_T c1_i110;
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_r3;
  int32_T c1_i111;
  int32_T c1_sb_loop_ub;
  int32_T c1_i112;
  int32_T c1_varargin_2;
  int32_T c1_b_varargin_2;
  real_T c1_e_n;
  int32_T c1_c_varargin_2;
  int32_T c1_d_varargin_2;
  real_T c1_f_n;
  int32_T c1_e_varargin_2;
  int32_T c1_i113;
  int32_T c1_f_varargin_2;
  real_T c1_g_n;
  int32_T c1_i114;
  int32_T c1_j_k;
  real_T c1_b_u;
  real_T c1_l_y;
  int32_T c1_k_k;
  int32_T c1_i115;
  real_T c1_c_u;
  int32_T c1_l_k;
  real_T c1_m_y;
  int32_T c1_m_k;
  int32_T c1_tb_loop_ub;
  int32_T c1_i116;
  int32_T c1_i117;
  int32_T c1_g_varargin_2;
  int32_T c1_h_varargin_2;
  real_T c1_d0;
  int32_T c1_i118;
  int32_T c1_i119;
  int32_T c1_n_k;
  int32_T c1_i120;
  int32_T c1_ub_loop_ub;
  int32_T c1_i121;
  boolean_T c1_b3;
  const mxArray *c1_n_y = NULL;
  static char_T c1_cv16[36] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  const mxArray *c1_o_y = NULL;
  real_T c1_M;
  int32_T c1_F;
  real_T c1_mtmp;
  real_T c1_d1;
  int32_T c1_ftmp;
  real_T c1_d2;
  real_T c1_d3;
  int32_T c1_i122;
  int32_T c1_o_k;
  real_T c1_d4;
  real_T c1_d5;
  real_T c1_p_k;
  int32_T c1_m_a;
  real_T c1_d6;
  real_T c1_d7;
  real_T c1_d8;
  real_T c1_d9;
  int32_T exitg1;
  boolean_T exitg2;
  c1_emxInit_real_T(chartInstance, &c1_matrOfObstaclestemp, 2, &c1_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_obstacleMatrix, 2, &c1_i_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_x, 2, &c1_r_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_y, 2, &c1_w_emlrtRTEI);
  c1_emxInit_boolean_T(chartInstance, &c1_BW, 2, &c1_db_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_H, 2, &c1_hb_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_rho, 2, &c1_ib_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_P, 2, &c1_lb_emlrtRTEI);
  c1_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_lines, 2,
    &c1_pb_emlrtRTEI);
  c1_emxInit_real_T1(chartInstance, &c1_orientationsOfLines, 1, &c1_qb_emlrtRTEI);
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  for (c1_i5 = 0; c1_i5 < 100000; c1_i5++) {
    chartInstance->c1_matrOfObstacles[c1_i5] =
      (*chartInstance->c1_b_matrOfObstacles)[c1_i5];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 19U, 19U, c1_debug_family_names,
    c1_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_matrOfObstaclestemp->data, (
    const int32_T *)c1_matrOfObstaclestemp->size, NULL, 0, 0, (void *)
    c1_sf_marshallOut, (void *)c1_sf_marshallIn, c1_matrOfObstaclestemp, true);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_drawAllignmentFigures, 1U, c1_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_obstacleMatrix->data, (const
    int32_T *)c1_obstacleMatrix->size, NULL, 0, 2, (void *)c1_sf_marshallOut,
    (void *)c1_sf_marshallIn, c1_obstacleMatrix, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_x->data, (const int32_T *)
    c1_x->size, NULL, 0, 3, (void *)c1_c_sf_marshallOut, (void *)
    c1_b_sf_marshallIn, c1_x, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_y->data, (const int32_T *)
    c1_y->size, NULL, 0, 4, (void *)c1_c_sf_marshallOut, (void *)
    c1_b_sf_marshallIn, c1_y, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_BW->data, (const int32_T *)
    c1_BW->size, NULL, 0, 5, (void *)c1_d_sf_marshallOut, (void *)
    c1_c_sf_marshallIn, c1_BW, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_H->data, (const int32_T *)
    c1_H->size, NULL, 0, 6, (void *)c1_e_sf_marshallOut, (void *)
    c1_d_sf_marshallIn, c1_H, true);
  _SFD_SYMBOL_SCOPE_ADD_EML(c1_theta, 7U, c1_f_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_rho->data, (const int32_T *)
    c1_rho->size, NULL, 0, 8, (void *)c1_g_sf_marshallOut, (void *)
    c1_e_sf_marshallIn, c1_rho, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_P->data, (const int32_T *)
    c1_P->size, NULL, 0, 9, (void *)c1_h_sf_marshallOut, (void *)
    c1_f_sf_marshallIn, c1_P, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_lines->data, (const int32_T *)
    c1_lines->size, NULL, 0, 10, (void *)c1_i_sf_marshallOut, (void *)
    c1_g_sf_marshallIn, c1_lines, true);
  _SFD_SYMBOL_SCOPE_ADD_EML_DYN_EMX_IMPORTABLE(c1_orientationsOfLines->data, (
    const int32_T *)c1_orientationsOfLines->size, NULL, 0, 11, (void *)
    c1_j_sf_marshallOut, (void *)c1_h_sf_marshallIn, c1_orientationsOfLines,
    true);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k, 12U, c1_b_sf_marshallOut,
    c1_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_rotAngle, 13U, c1_b_sf_marshallOut,
    c1_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 14U, c1_b_sf_marshallOut,
    c1_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 15U, c1_b_sf_marshallOut,
    c1_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(chartInstance->c1_matrOfObstacles, 16U,
    c1_k_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_b_rotationMatrix, 17U,
    c1_l_sf_marshallOut, c1_j_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_b_invRotationMatrix, 18U,
    c1_l_sf_marshallOut, c1_j_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 3);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 4);
  c1_i6 = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size[1];
  c1_matrOfObstaclestemp->size[0] = 50000;
  c1_matrOfObstaclestemp->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_matrOfObstaclestemp, c1_i6,
    &c1_emlrtRTEI);
  for (c1_i7 = 0; c1_i7 < 100000; c1_i7++) {
    c1_matrOfObstaclestemp->data[c1_i7] = chartInstance->
      c1_matrOfObstacles[c1_i7];
  }

  c1_emxInit_real_T1(chartInstance, &c1_b_x, 1, &c1_b_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 6);
  c1_i8 = c1_b_x->size[0];
  c1_b_x->size[0] = 50000;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i8, &c1_b_emlrtRTEI);
  for (c1_i9 = 0; c1_i9 < 50000; c1_i9++) {
    c1_b_x->data[c1_i9] = c1_matrOfObstaclestemp->data[c1_i9];
  }

  c1_b_size[0] = c1_b_x->size[0];
  c1_loop_ub = c1_b_x->size[0] - 1;
  for (c1_i10 = 0; c1_i10 <= c1_loop_ub; c1_i10++) {
    c1_b_data[c1_i10] = muDoubleScalarIsNaN(c1_b_x->data[c1_i10]);
  }

  c1_emxInit_real_T(chartInstance, &c1_varargin_1, 2, &c1_g_emlrtRTEI);
  c1_i11 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
  c1_varargin_1->size[0] = c1_matrOfObstaclestemp->size[0];
  c1_varargin_1->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_varargin_1, c1_i11,
    (emlrtRTEInfo *)NULL);
  c1_b_loop_ub = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size
    [1] - 1;
  for (c1_i12 = 0; c1_i12 <= c1_b_loop_ub; c1_i12++) {
    c1_varargin_1->data[c1_i12] = c1_matrOfObstaclestemp->data[c1_i12];
  }

  c1_c_nullAssignment(chartInstance, c1_varargin_1, c1_b_data, c1_b_size);
  c1_i13 = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size[1];
  c1_matrOfObstaclestemp->size[0] = c1_varargin_1->size[0];
  c1_matrOfObstaclestemp->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_matrOfObstaclestemp, c1_i13,
    &c1_d_emlrtRTEI);
  c1_c_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i14 = 0; c1_i14 <= c1_c_loop_ub; c1_i14++) {
    c1_matrOfObstaclestemp->data[c1_i14] = c1_varargin_1->data[c1_i14];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 7);
  c1_i15 = c1_matrOfObstaclestemp->size[0];
  c1_i16 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_i15;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i16, &c1_e_emlrtRTEI);
  c1_d_loop_ub = c1_i15 - 1;
  for (c1_i17 = 0; c1_i17 <= c1_d_loop_ub; c1_i17++) {
    c1_b_x->data[c1_i17] = c1_matrOfObstaclestemp->data[c1_i17 +
      c1_matrOfObstaclestemp->size[0]];
  }

  c1_b_size[0] = c1_b_x->size[0];
  c1_e_loop_ub = c1_b_x->size[0] - 1;
  for (c1_i18 = 0; c1_i18 <= c1_e_loop_ub; c1_i18++) {
    c1_b_data[c1_i18] = muDoubleScalarIsNaN(c1_b_x->data[c1_i18]);
  }

  c1_emxFree_real_T(chartInstance, &c1_b_x);
  c1_i19 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
  c1_varargin_1->size[0] = c1_matrOfObstaclestemp->size[0];
  c1_varargin_1->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_varargin_1, c1_i19,
    (emlrtRTEInfo *)NULL);
  c1_f_loop_ub = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size
    [1] - 1;
  for (c1_i20 = 0; c1_i20 <= c1_f_loop_ub; c1_i20++) {
    c1_varargin_1->data[c1_i20] = c1_matrOfObstaclestemp->data[c1_i20];
  }

  c1_c_nullAssignment(chartInstance, c1_varargin_1, c1_b_data, c1_b_size);
  c1_i21 = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size[1];
  c1_matrOfObstaclestemp->size[0] = c1_varargin_1->size[0];
  c1_matrOfObstaclestemp->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_matrOfObstaclestemp, c1_i21,
    &c1_f_emlrtRTEI);
  c1_g_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i22 = 0; c1_i22 <= c1_g_loop_ub; c1_i22++) {
    c1_matrOfObstaclestemp->data[c1_i22] = c1_varargin_1->data[c1_i22];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 25);
  c1_drawAllignmentFigures = 0.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 34);
  c1_i23 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
  c1_varargin_1->size[0] = c1_matrOfObstaclestemp->size[0];
  c1_varargin_1->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_varargin_1, c1_i23,
    &c1_g_emlrtRTEI);
  c1_h_loop_ub = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size
    [1] - 1;
  for (c1_i24 = 0; c1_i24 <= c1_h_loop_ub; c1_i24++) {
    c1_varargin_1->data[c1_i24] = c1_matrOfObstaclestemp->data[c1_i24];
  }

  if ((real_T)c1_varargin_1->size[0] != 1.0) {
    c1_b0 = true;
  } else {
    c1_b0 = false;
  }

  if (c1_b0) {
  } else {
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv14, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv14, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_b_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_c_y)));
  }

  if ((real_T)c1_varargin_1->size[0] >= 1.0) {
  } else {
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv0, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv0, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_e_y)));
  }

  c1_m = c1_varargin_1->size[0];
  for (c1_j = 0; c1_j < 2; c1_j++) {
    c1_b_j = c1_j;
    c1_minval_data[c1_b_j] = c1_varargin_1->data[c1_varargin_1->size[0] * c1_b_j];
    c1_b_b = c1_m;
    c1_c_b = c1_b_b;
    if (2 > c1_c_b) {
      c1_overflow = false;
    } else {
      c1_overflow = (c1_c_b > 2147483646);
    }

    if (c1_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_i = 1; c1_i < c1_m; c1_i++) {
      c1_a = c1_minval_data[c1_b_j];
      c1_e_b = c1_varargin_1->data[c1_i + c1_varargin_1->size[0] * c1_b_j];
      c1_b_a = c1_a;
      c1_f_b = c1_e_b;
      c1_h_x = c1_f_b;
      c1_g_b = muDoubleScalarIsNaN(c1_h_x);
      if (c1_g_b) {
        c1_p = false;
      } else {
        c1_i_x = c1_b_a;
        c1_h_b = muDoubleScalarIsNaN(c1_i_x);
        if (c1_h_b) {
          c1_p = true;
        } else {
          c1_p = (c1_b_a > c1_f_b);
        }
      }

      if (c1_p) {
        c1_minval_data[c1_b_j] = c1_varargin_1->data[c1_i + c1_varargin_1->size
          [0] * c1_b_j];
      }
    }
  }

  if (c1_minval_data[0] > c1_minval_data[1]) {
    c1_minval = c1_minval_data[1];
  } else {
    c1_c_x = c1_minval_data[0];
    c1_b = muDoubleScalarIsNaN(c1_c_x);
    if (c1_b) {
      c1_d_x = c1_minval_data[1];
      c1_d_b = muDoubleScalarIsNaN(c1_d_x);
      if (!c1_d_b) {
        c1_minval = c1_minval_data[1];
      } else {
        c1_minval = c1_minval_data[0];
      }
    } else {
      c1_minval = c1_minval_data[0];
    }
  }

  c1_e_x = c1_minval;
  c1_f_x = c1_e_x;
  c1_g_x = c1_f_x;
  c1_f_y = muDoubleScalarAbs(c1_g_x);
  c1_i25 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
  c1_varargin_1->size[0] = c1_matrOfObstaclestemp->size[0];
  c1_varargin_1->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_varargin_1, c1_i25,
    (emlrtRTEInfo *)NULL);
  c1_i_loop_ub = c1_matrOfObstaclestemp->size[0] * c1_matrOfObstaclestemp->size
    [1] - 1;
  for (c1_i26 = 0; c1_i26 <= c1_i_loop_ub; c1_i26++) {
    c1_varargin_1->data[c1_i26] = ((c1_matrOfObstaclestemp->data[c1_i26] +
      c1_f_y) + 1.0) * 10.0;
  }

  c1_c_round(chartInstance, c1_varargin_1);
  c1_i27 = c1_obstacleMatrix->size[0] * c1_obstacleMatrix->size[1];
  c1_obstacleMatrix->size[0] = c1_varargin_1->size[0];
  c1_obstacleMatrix->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_obstacleMatrix, c1_i27,
    &c1_i_emlrtRTEI);
  c1_j_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i28 = 0; c1_i28 <= c1_j_loop_ub; c1_i28++) {
    c1_obstacleMatrix->data[c1_i28] = c1_varargin_1->data[c1_i28];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 35);
  c1_i29 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
  c1_varargin_1->size[0] = c1_obstacleMatrix->size[0];
  c1_varargin_1->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_varargin_1, c1_i29,
    &c1_j_emlrtRTEI);
  c1_k_loop_ub = c1_obstacleMatrix->size[0] * c1_obstacleMatrix->size[1] - 1;
  for (c1_i30 = 0; c1_i30 <= c1_k_loop_ub; c1_i30++) {
    c1_varargin_1->data[c1_i30] = c1_obstacleMatrix->data[c1_i30];
  }

  if ((real_T)c1_varargin_1->size[0] == 0.0) {
    c1_i31 = c1_obstacleMatrix->size[0] * c1_obstacleMatrix->size[1];
    c1_obstacleMatrix->size[0] = c1_varargin_1->size[0];
    c1_obstacleMatrix->size[1] = 2;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_obstacleMatrix, c1_i31,
      &c1_l_emlrtRTEI);
    c1_l_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
    for (c1_i33 = 0; c1_i33 <= c1_l_loop_ub; c1_i33++) {
      c1_obstacleMatrix->data[c1_i33] = c1_varargin_1->data[c1_i33];
    }
  } else {
    c1_emxInit_real_T(chartInstance, &c1_i_b, 2, &c1_sb_emlrtRTEI);
    c1_i32 = c1_i_b->size[0] * c1_i_b->size[1];
    c1_i_b->size[0] = c1_varargin_1->size[0];
    c1_i_b->size[1] = 2;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_i_b, c1_i32, &c1_k_emlrtRTEI);
    c1_m_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
    for (c1_i34 = 0; c1_i34 <= c1_m_loop_ub; c1_i34++) {
      c1_i_b->data[c1_i34] = c1_varargin_1->data[c1_i34];
    }

    for (c1_b_k = 0; c1_b_k < 2; c1_b_k++) {
      c1_c_k = c1_b_k;
      c1_col_data[c1_c_k] = c1_c_k + 1;
    }

    c1_emxInit_int32_T(chartInstance, &c1_idx, 1, &c1_o_emlrtRTEI);
    c1_n = c1_i_b->size[0];
    c1_i37 = c1_idx->size[0];
    c1_idx->size[0] = c1_i_b->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_idx, c1_i37, &c1_o_emlrtRTEI);
    c1_o_loop_ub = c1_i_b->size[0] - 1;
    for (c1_i39 = 0; c1_i39 <= c1_o_loop_ub; c1_i39++) {
      c1_idx->data[c1_i39] = 0;
    }

    c1_emxInit_int32_T(chartInstance, &c1_b_idx, 1, &c1_p_emlrtRTEI);
    c1_b_n = c1_n;
    c1_i40 = c1_b_idx->size[0];
    c1_b_idx->size[0] = c1_idx->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i40,
      &c1_p_emlrtRTEI);
    c1_p_loop_ub = c1_idx->size[0] - 1;
    for (c1_i43 = 0; c1_i43 <= c1_p_loop_ub; c1_i43++) {
      c1_b_idx->data[c1_i43] = c1_idx->data[c1_i43];
    }

    c1_emxInit_int32_T(chartInstance, &c1_iwork, 1, &c1_tb_emlrtRTEI);
    c1_len = c1_b_idx->size[0];
    c1_i45 = c1_b_idx->size[0];
    c1_b_idx->size[0] = c1_len;
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i45,
      &c1_q_emlrtRTEI);
    c1_iv0[0] = c1_b_idx->size[0];
    c1_i47 = c1_iwork->size[0];
    c1_iwork->size[0] = c1_iv0[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i47,
      &c1_s_emlrtRTEI);
    c1_np1 = c1_b_n + 1;
    c1_i49 = c1_b_n - 1;
    c1_j_b = c1_i49;
    c1_k_b = c1_j_b;
    c1_emxFree_int32_T(chartInstance, &c1_b_idx);
    if (1 > c1_k_b) {
      c1_b_overflow = false;
    } else {
      c1_b_overflow = (c1_k_b > 2147483645);
    }

    if (c1_b_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    c1_d_k = 1;
    c1_emxInit_real_T(chartInstance, &c1_l_b, 2, &c1_u_emlrtRTEI);
    while (c1_d_k <= c1_i49) {
      c1_i54 = c1_l_b->size[0] * c1_l_b->size[1];
      c1_l_b->size[0] = c1_i_b->size[0];
      c1_l_b->size[1] = 2;
      c1_emxEnsureCapacity_real_T(chartInstance, c1_l_b, c1_i54, &c1_u_emlrtRTEI);
      c1_u_loop_ub = c1_i_b->size[0] * c1_i_b->size[1] - 1;
      for (c1_i56 = 0; c1_i56 <= c1_u_loop_ub; c1_i56++) {
        c1_l_b->data[c1_i56] = c1_i_b->data[c1_i56];
      }

      c1_col_size[0] = 1;
      c1_col_size[1] = 2;
      for (c1_i60 = 0; c1_i60 < 2; c1_i60++) {
        c1_b_col_data[c1_i60] = c1_col_data[c1_i60];
      }

      if (c1_sortLE(chartInstance, c1_l_b, c1_b_col_data, c1_col_size, c1_d_k,
                    c1_d_k + 1)) {
        c1_idx->data[c1_d_k - 1] = c1_d_k;
        c1_idx->data[c1_d_k] = c1_d_k + 1;
      } else {
        c1_idx->data[c1_d_k - 1] = c1_d_k + 1;
        c1_idx->data[c1_d_k] = c1_d_k;
      }

      c1_d_k += 2;
    }

    c1_emxFree_real_T(chartInstance, &c1_l_b);
    if ((c1_b_n & 1) != 0) {
      c1_idx->data[c1_b_n - 1] = c1_b_n;
    }

    c1_b_i = 2;
    c1_emxInit_real_T(chartInstance, &c1_m_b, 2, &c1_u_emlrtRTEI);
    while (c1_b_i < c1_b_n) {
      c1_c_a = c1_b_i;
      c1_i2 = c1_c_a << 1;
      c1_c_j = 1;
      for (c1_pEnd = 1 + c1_b_i; c1_pEnd < c1_np1; c1_pEnd = c1_qEnd + c1_b_i) {
        c1_b_p = c1_c_j - 1;
        c1_q = c1_pEnd - 1;
        c1_qEnd = c1_c_j + c1_i2;
        if (c1_qEnd > c1_np1) {
          c1_qEnd = c1_np1;
        }

        c1_e_k = 0;
        c1_kEnd = c1_qEnd - c1_c_j;
        while (c1_e_k + 1 <= c1_kEnd) {
          c1_i65 = c1_m_b->size[0] * c1_m_b->size[1];
          c1_m_b->size[0] = c1_i_b->size[0];
          c1_m_b->size[1] = 2;
          c1_emxEnsureCapacity_real_T(chartInstance, c1_m_b, c1_i65,
            &c1_u_emlrtRTEI);
          c1_y_loop_ub = c1_i_b->size[0] * c1_i_b->size[1] - 1;
          for (c1_i67 = 0; c1_i67 <= c1_y_loop_ub; c1_i67++) {
            c1_m_b->data[c1_i67] = c1_i_b->data[c1_i67];
          }

          c1_b_col_size[0] = 1;
          c1_b_col_size[1] = 2;
          for (c1_i71 = 0; c1_i71 < 2; c1_i71++) {
            c1_c_col_data[c1_i71] = c1_col_data[c1_i71];
          }

          if (c1_sortLE(chartInstance, c1_m_b, c1_c_col_data, c1_b_col_size,
                        c1_idx->data[c1_b_p], c1_idx->data[c1_q])) {
            c1_iwork->data[c1_e_k] = c1_idx->data[c1_b_p];
            c1_b_p++;
            if (c1_b_p + 1 == c1_pEnd) {
              while (c1_q + 1 < c1_qEnd) {
                c1_e_k++;
                c1_iwork->data[c1_e_k] = c1_idx->data[c1_q];
                c1_q++;
              }
            }
          } else {
            c1_iwork->data[c1_e_k] = c1_idx->data[c1_q];
            c1_q++;
            if (c1_q + 1 == c1_qEnd) {
              while (c1_b_p + 1 < c1_pEnd) {
                c1_e_k++;
                c1_iwork->data[c1_e_k] = c1_idx->data[c1_b_p];
                c1_b_p++;
              }
            }
          }

          c1_e_k++;
        }

        c1_b_p = c1_c_j - 2;
        for (c1_g_k = 0; c1_g_k < c1_kEnd; c1_g_k++) {
          c1_e_k = c1_g_k;
          c1_idx->data[(c1_b_p + c1_e_k) + 1] = c1_iwork->data[c1_e_k];
        }

        c1_c_j = c1_qEnd;
      }

      c1_b_i = c1_i2;
    }

    c1_emxFree_real_T(chartInstance, &c1_m_b);
    c1_emxFree_int32_T(chartInstance, &c1_iwork);
    c1_b_apply_row_permutation(chartInstance, c1_i_b, c1_idx);
    c1_i59 = c1_obstacleMatrix->size[0] * c1_obstacleMatrix->size[1];
    c1_obstacleMatrix->size[0] = c1_i_b->size[0];
    c1_obstacleMatrix->size[1] = 2;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_obstacleMatrix, c1_i59,
      &c1_l_emlrtRTEI);
    c1_w_loop_ub = c1_i_b->size[0] * c1_i_b->size[1] - 1;
    c1_emxFree_int32_T(chartInstance, &c1_idx);
    for (c1_i63 = 0; c1_i63 <= c1_w_loop_ub; c1_i63++) {
      c1_obstacleMatrix->data[c1_i63] = c1_i_b->data[c1_i63];
    }

    c1_emxFree_real_T(chartInstance, &c1_i_b);
    c1_nb = 0;
    c1_khi = c1_varargin_1->size[0];
    c1_f_k = 1;
    c1_emxInit_real_T(chartInstance, &c1_b_obstacleMatrix, 2, &c1_y_emlrtRTEI);
    while (c1_f_k <= c1_khi) {
      c1_k0 = c1_f_k;
      do {
        exitg1 = 0;
        c1_d_a = c1_f_k + 1;
        c1_f_k = c1_d_a;
        if (c1_f_k > c1_khi) {
          exitg1 = 1;
        } else {
          c1_i70 = c1_b_obstacleMatrix->size[0] * c1_b_obstacleMatrix->size[1];
          c1_b_obstacleMatrix->size[0] = c1_obstacleMatrix->size[0];
          c1_b_obstacleMatrix->size[1] = 2;
          c1_emxEnsureCapacity_real_T(chartInstance, c1_b_obstacleMatrix, c1_i70,
            &c1_y_emlrtRTEI);
          c1_bb_loop_ub = c1_obstacleMatrix->size[0] * c1_obstacleMatrix->size[1]
            - 1;
          for (c1_i74 = 0; c1_i74 <= c1_bb_loop_ub; c1_i74++) {
            c1_b_obstacleMatrix->data[c1_i74] = c1_obstacleMatrix->data[c1_i74];
          }

          if (c1_rows_differ(chartInstance, c1_b_obstacleMatrix, c1_k0, c1_f_k))
          {
            exitg1 = 1;
          }
        }
      } while (exitg1 == 0);

      c1_e_a = c1_nb + 1;
      c1_nb = c1_e_a;
      for (c1_d_j = 0; c1_d_j < 2; c1_d_j++) {
        c1_e_j = c1_d_j;
        c1_obstacleMatrix->data[(c1_nb + c1_obstacleMatrix->size[0] * c1_e_j) -
          1] = c1_obstacleMatrix->data[(c1_k0 + c1_obstacleMatrix->size[0] *
          c1_e_j) - 1];
      }

      c1_f_a = c1_f_k;
      c1_i76 = c1_f_a;
      c1_g_a = c1_k0;
      c1_n_b = c1_i76 - 1;
      c1_h_a = c1_g_a;
      c1_o_b = c1_n_b;
      if (c1_h_a > c1_o_b) {
        c1_c_overflow = false;
      } else {
        c1_c_overflow = (c1_o_b > 2147483646);
      }

      if (c1_c_overflow) {
        c1_check_forloop_overflow_error(chartInstance, true);
      }
    }

    c1_emxFree_real_T(chartInstance, &c1_b_obstacleMatrix);
    if (c1_nb <= c1_varargin_1->size[0]) {
    } else {
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                        c1_emlrt_marshallOut(chartInstance, c1_cv1), 14,
                        sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_emlrt_marshallOut(chartInstance, c1_cv1))));
    }

    c1_b1 = (1 > c1_nb);
    if (c1_b1) {
      c1_i69 = 0;
    } else {
      c1_i69 = c1_nb;
    }

    c1_emxInit_real_T(chartInstance, &c1_c_obstacleMatrix, 2, &c1_ab_emlrtRTEI);
    c1_i73 = c1_c_obstacleMatrix->size[0] * c1_c_obstacleMatrix->size[1];
    c1_c_obstacleMatrix->size[0] = c1_i69;
    c1_c_obstacleMatrix->size[1] = 2;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_c_obstacleMatrix, c1_i73,
      &c1_ab_emlrtRTEI);
    for (c1_i75 = 0; c1_i75 < 2; c1_i75++) {
      c1_db_loop_ub = c1_i69 - 1;
      for (c1_i79 = 0; c1_i79 <= c1_db_loop_ub; c1_i79++) {
        c1_c_obstacleMatrix->data[c1_i79 + c1_c_obstacleMatrix->size[0] * c1_i75]
          = c1_obstacleMatrix->data[c1_i79 + c1_obstacleMatrix->size[0] * c1_i75];
      }
    }

    c1_i78 = c1_obstacleMatrix->size[0] * c1_obstacleMatrix->size[1];
    c1_obstacleMatrix->size[0] = c1_c_obstacleMatrix->size[0];
    c1_obstacleMatrix->size[1] = 2;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_obstacleMatrix, c1_i78,
      &c1_l_emlrtRTEI);
    c1_eb_loop_ub = c1_c_obstacleMatrix->size[0] * c1_c_obstacleMatrix->size[1]
      - 1;
    for (c1_i81 = 0; c1_i81 <= c1_eb_loop_ub; c1_i81++) {
      c1_obstacleMatrix->data[c1_i81] = c1_c_obstacleMatrix->data[c1_i81];
    }

    c1_emxFree_real_T(chartInstance, &c1_c_obstacleMatrix);
    c1_p_b = c1_nb;
    c1_q_b = c1_p_b;
    if (1 > c1_q_b) {
      c1_d_overflow = false;
    } else {
      c1_d_overflow = (c1_q_b > 2147483646);
    }

    if (c1_d_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }
  }

  c1_emxFree_real_T(chartInstance, &c1_varargin_1);
  c1_emxInit_real_T1(chartInstance, &c1_b_varargin_1, 1, &c1_jb_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 36);
  c1_i35 = c1_obstacleMatrix->size[0];
  c1_i36 = c1_b_varargin_1->size[0];
  c1_b_varargin_1->size[0] = c1_i35;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_varargin_1, c1_i36,
    &c1_n_emlrtRTEI);
  c1_n_loop_ub = c1_i35 - 1;
  for (c1_i38 = 0; c1_i38 <= c1_n_loop_ub; c1_i38++) {
    c1_b_varargin_1->data[c1_i38] = c1_obstacleMatrix->data[c1_i38];
  }

  c1_emxInit_real_T(chartInstance, &c1_b_rho, 2, &c1_n_emlrtRTEI);
  c1_emxInit_real_T1(chartInstance, &c1_r0, 1, &c1_n_emlrtRTEI);
  c1_d_round(chartInstance, c1_b_varargin_1);
  c1_i41 = c1_obstacleMatrix->size[0];
  c1_i42 = c1_r0->size[0];
  c1_r0->size[0] = c1_i41;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_r0, c1_i42, &c1_n_emlrtRTEI);
  c1_q_loop_ub = c1_i41 - 1;
  for (c1_i44 = 0; c1_i44 <= c1_q_loop_ub; c1_i44++) {
    c1_r0->data[c1_i44] = c1_obstacleMatrix->data[c1_i44];
  }

  c1_d_round(chartInstance, c1_r0);
  c1_i46 = c1_b_rho->size[0] * c1_b_rho->size[1];
  c1_b_rho->size[0] = 1;
  c1_b_rho->size[1] = c1_r0->size[0];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_rho, c1_i46, &c1_n_emlrtRTEI);
  c1_r_loop_ub = c1_r0->size[0] - 1;
  for (c1_i48 = 0; c1_i48 <= c1_r_loop_ub; c1_i48++) {
    c1_b_rho->data[c1_i48] = c1_r0->data[c1_i48];
  }

  c1_i50 = c1_x->size[0] * c1_x->size[1];
  c1_x->size[0] = 1;
  c1_x->size[1] = c1_b_varargin_1->size[0];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_x, c1_i50, &c1_r_emlrtRTEI);
  c1_s_loop_ub = c1_b_varargin_1->size[0] - 1;
  for (c1_i51 = 0; c1_i51 <= c1_s_loop_ub; c1_i51++) {
    c1_x->data[c1_i51] = c1_b_rho->data[c1_i51];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 37);
  c1_i52 = c1_obstacleMatrix->size[0];
  c1_i53 = c1_b_varargin_1->size[0];
  c1_b_varargin_1->size[0] = c1_i52;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_varargin_1, c1_i53,
    &c1_t_emlrtRTEI);
  c1_t_loop_ub = c1_i52 - 1;
  for (c1_i55 = 0; c1_i55 <= c1_t_loop_ub; c1_i55++) {
    c1_b_varargin_1->data[c1_i55] = c1_obstacleMatrix->data[c1_i55 +
      c1_obstacleMatrix->size[0]];
  }

  c1_d_round(chartInstance, c1_b_varargin_1);
  c1_i57 = c1_obstacleMatrix->size[0];
  c1_i58 = c1_r0->size[0];
  c1_r0->size[0] = c1_i57;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_r0, c1_i58, &c1_t_emlrtRTEI);
  c1_v_loop_ub = c1_i57 - 1;
  for (c1_i61 = 0; c1_i61 <= c1_v_loop_ub; c1_i61++) {
    c1_r0->data[c1_i61] = c1_obstacleMatrix->data[c1_i61 +
      c1_obstacleMatrix->size[0]];
  }

  c1_d_round(chartInstance, c1_r0);
  c1_i62 = c1_b_rho->size[0] * c1_b_rho->size[1];
  c1_b_rho->size[0] = 1;
  c1_b_rho->size[1] = c1_r0->size[0];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_rho, c1_i62, &c1_t_emlrtRTEI);
  c1_x_loop_ub = c1_r0->size[0] - 1;
  for (c1_i64 = 0; c1_i64 <= c1_x_loop_ub; c1_i64++) {
    c1_b_rho->data[c1_i64] = c1_r0->data[c1_i64];
  }

  c1_emxFree_real_T(chartInstance, &c1_r0);
  c1_i66 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = 1;
  c1_y->size[1] = c1_b_varargin_1->size[0];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_y, c1_i66, &c1_w_emlrtRTEI);
  c1_ab_loop_ub = c1_b_varargin_1->size[0] - 1;
  for (c1_i68 = 0; c1_i68 <= c1_ab_loop_ub; c1_i68++) {
    c1_y->data[c1_i68] = c1_b_rho->data[c1_i68];
  }

  c1_emxInit_real_T(chartInstance, &c1_j_x, 2, &c1_x_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 38);
  c1_i72 = c1_j_x->size[0] * c1_j_x->size[1];
  c1_j_x->size[0] = 1;
  c1_j_x->size[1] = c1_x->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_j_x, c1_i72, &c1_x_emlrtRTEI);
  c1_cb_loop_ub = c1_x->size[0] * c1_x->size[1] - 1;
  for (c1_i77 = 0; c1_i77 <= c1_cb_loop_ub; c1_i77++) {
    c1_j_x->data[c1_i77] = c1_x->data[c1_i77];
  }

  c1_emxInit_real_T(chartInstance, &c1_g_y, 2, &c1_bb_emlrtRTEI);
  c1_i80 = c1_g_y->size[0] * c1_g_y->size[1];
  c1_g_y->size[0] = 1;
  c1_g_y->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_g_y, c1_i80, &c1_bb_emlrtRTEI);
  c1_fb_loop_ub = c1_y->size[0] * c1_y->size[1] - 1;
  for (c1_i82 = 0; c1_i82 <= c1_fb_loop_ub; c1_i82++) {
    c1_g_y->data[c1_i82] = c1_y->data[c1_i82];
  }

  c1_emxInitStruct_coder_internal_sp(chartInstance, &c1_this, &c1_ub_emlrtRTEI);
  c1_sparse(chartInstance, c1_j_x, c1_g_y, &c1_this);
  c1_dv2[0] = (real_T)c1_this.m;
  c1_dv2[1] = (real_T)c1_this.n;
  c1_i83 = c1_BW->size[0] * c1_BW->size[1];
  c1_BW->size[0] = (int32_T)c1_dv2[0];
  c1_BW->size[1] = (int32_T)c1_dv2[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_BW, c1_i83, &c1_db_emlrtRTEI);
  c1_gb_loop_ub = (int32_T)c1_dv2[0] * (int32_T)c1_dv2[1] - 1;
  c1_emxFree_real_T(chartInstance, &c1_g_y);
  c1_emxFree_real_T(chartInstance, &c1_j_x);
  for (c1_i84 = 0; c1_i84 <= c1_gb_loop_ub; c1_i84++) {
    c1_BW->data[c1_i84] = false;
  }

  c1_i85 = c1_this.n;
  c1_r_b = c1_i85;
  c1_s_b = c1_r_b;
  if (1 > c1_s_b) {
    c1_e_overflow = false;
  } else {
    c1_e_overflow = (c1_s_b > 2147483646);
  }

  if (c1_e_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_c = 0; c1_c < c1_i85; c1_c++) {
    c1_b_c = c1_c;
    c1_cend = c1_this.colidx->data[c1_b_c + 1] - 1;
    c1_i86 = c1_this.colidx->data[c1_b_c];
    c1_i_a = c1_i86;
    c1_t_b = c1_cend;
    c1_j_a = c1_i_a;
    c1_u_b = c1_t_b;
    if (c1_j_a > c1_u_b) {
      c1_f_overflow = false;
    } else {
      c1_f_overflow = (c1_u_b > 2147483646);
    }

    if (c1_f_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_c_idx = c1_i86 - 1; c1_c_idx < c1_cend; c1_c_idx++) {
      c1_BW->data[(c1_this.rowidx->data[c1_c_idx] + c1_BW->size[0] * c1_b_c) - 1]
        = c1_this.d->data[c1_c_idx];
    }
  }

  c1_emxFreeStruct_coder_internal_sp(chartInstance, &c1_this);
  c1_emxInit_boolean_T(chartInstance, &c1_b_BW, 2, &c1_eb_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 43);
  c1_i87 = c1_b_BW->size[0] * c1_b_BW->size[1];
  c1_b_BW->size[0] = c1_BW->size[0];
  c1_b_BW->size[1] = c1_BW->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_b_BW, c1_i87,
    &c1_eb_emlrtRTEI);
  c1_hb_loop_ub = c1_BW->size[0] * c1_BW->size[1] - 1;
  for (c1_i88 = 0; c1_i88 <= c1_hb_loop_ub; c1_i88++) {
    c1_b_BW->data[c1_i88] = c1_BW->data[c1_i88];
  }

  c1_emxInit_boolean_T(chartInstance, &c1_r1, 2, &c1_xb_emlrtRTEI);
  c1_edge(chartInstance, c1_b_BW, c1_r1);
  c1_i89 = c1_BW->size[0] * c1_BW->size[1];
  c1_BW->size[0] = c1_r1->size[0];
  c1_BW->size[1] = c1_r1->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_BW, c1_i89, &c1_fb_emlrtRTEI);
  c1_ib_loop_ub = c1_r1->size[0] * c1_r1->size[1] - 1;
  c1_emxFree_boolean_T(chartInstance, &c1_b_BW);
  for (c1_i90 = 0; c1_i90 <= c1_ib_loop_ub; c1_i90++) {
    c1_BW->data[c1_i90] = c1_r1->data[c1_i90];
  }

  c1_emxFree_boolean_T(chartInstance, &c1_r1);
  c1_emxInit_boolean_T(chartInstance, &c1_c_BW, 2, &c1_gb_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 45);
  CV_EML_IF(0, 1, 0, false);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 55);
  c1_i91 = c1_c_BW->size[0] * c1_c_BW->size[1];
  c1_c_BW->size[0] = c1_BW->size[0];
  c1_c_BW->size[1] = c1_BW->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_c_BW, c1_i91,
    &c1_gb_emlrtRTEI);
  c1_jb_loop_ub = c1_BW->size[0] * c1_BW->size[1] - 1;
  for (c1_i92 = 0; c1_i92 <= c1_jb_loop_ub; c1_i92++) {
    c1_c_BW->data[c1_i92] = c1_BW->data[c1_i92];
  }

  c1_emxInit_real_T(chartInstance, &c1_b_H, 2, (emlrtRTEInfo *)NULL);
  c1_hough(chartInstance, c1_c_BW, c1_b_H, c1_b_rho);
  c1_i93 = c1_H->size[0] * c1_H->size[1];
  c1_H->size[0] = c1_b_H->size[0];
  c1_H->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_H, c1_i93, &c1_hb_emlrtRTEI);
  c1_kb_loop_ub = c1_b_H->size[0] * c1_b_H->size[1] - 1;
  c1_emxFree_boolean_T(chartInstance, &c1_c_BW);
  for (c1_i94 = 0; c1_i94 <= c1_kb_loop_ub; c1_i94++) {
    c1_H->data[c1_i94] = c1_b_H->data[c1_i94];
  }

  c1_emxFree_real_T(chartInstance, &c1_b_H);
  for (c1_i95 = 0; c1_i95 < 181; c1_i95++) {
    c1_theta[c1_i95] = -45.0 + 0.5 * (real_T)c1_i95;
  }

  c1_i96 = c1_rho->size[0] * c1_rho->size[1];
  c1_rho->size[0] = 1;
  c1_rho->size[1] = c1_b_rho->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_rho, c1_i96, &c1_ib_emlrtRTEI);
  c1_lb_loop_ub = c1_b_rho->size[0] * c1_b_rho->size[1] - 1;
  for (c1_i97 = 0; c1_i97 <= c1_lb_loop_ub; c1_i97++) {
    c1_rho->data[c1_i97] = c1_b_rho->data[c1_i97];
  }

  c1_emxFree_real_T(chartInstance, &c1_b_rho);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 57);
  c1_i98 = c1_b_varargin_1->size[0];
  c1_b_varargin_1->size[0] = c1__s32_s64_(chartInstance, (int64_T)c1_H->size[0] *
    181LL, 1U, 2200, 4);
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_varargin_1, c1_i98,
    &c1_jb_emlrtRTEI);
  c1_mb_loop_ub = c1__s32_s64_(chartInstance, (int64_T)c1_H->size[0] * 181LL, 1U,
    2200, 4) - 1;
  for (c1_i99 = 0; c1_i99 <= c1_mb_loop_ub; c1_i99++) {
    c1_b_varargin_1->data[c1_i99] = c1_H->data[c1_i99];
  }

  if ((c1_b_varargin_1->size[0] == 1) || ((real_T)c1_b_varargin_1->size[0] !=
       1.0)) {
    c1_b2 = true;
  } else {
    c1_b2 = false;
  }

  if (c1_b2) {
  } else {
    c1_h_y = NULL;
    sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv15, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c1_i_y = NULL;
    sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv15, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_h_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_i_y)));
  }

  if ((real_T)c1_b_varargin_1->size[0] >= 1.0) {
  } else {
    c1_j_y = NULL;
    sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv0, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    c1_k_y = NULL;
    sf_mex_assign(&c1_k_y, sf_mex_create("y", c1_cv0, 10, 0U, 1U, 0U, 2, 1, 39),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_k_y)));
  }

  c1_c_n = c1_b_varargin_1->size[0];
  if (c1_c_n <= 2) {
    if (c1_c_n == 1) {
      c1_maxval = c1_b_varargin_1->data[0];
    } else if (c1_b_varargin_1->data[0] < c1_b_varargin_1->data[1]) {
      c1_maxval = c1_b_varargin_1->data[1];
    } else {
      c1_m_x = c1_b_varargin_1->data[0];
      c1_w_b = muDoubleScalarIsNaN(c1_m_x);
      if (c1_w_b) {
        c1_n_x = c1_b_varargin_1->data[1];
        c1_x_b = muDoubleScalarIsNaN(c1_n_x);
        if (!c1_x_b) {
          c1_maxval = c1_b_varargin_1->data[1];
        } else {
          c1_maxval = c1_b_varargin_1->data[0];
        }
      } else {
        c1_maxval = c1_b_varargin_1->data[0];
      }
    }
  } else {
    c1_d_n = c1_b_varargin_1->size[0];
    c1_k_x = c1_b_varargin_1->data[0];
    c1_l_x = c1_k_x;
    c1_v_b = muDoubleScalarIsNaN(c1_l_x);
    c1_c_p = !c1_v_b;
    if (c1_c_p) {
      c1_d_idx = 1;
    } else {
      c1_d_idx = 0;
      c1_y_b = c1_d_n;
      c1_ab_b = c1_y_b;
      if (2 > c1_ab_b) {
        c1_g_overflow = false;
      } else {
        c1_g_overflow = (c1_ab_b > 2147483646);
      }

      if (c1_g_overflow) {
        c1_check_forloop_overflow_error(chartInstance, true);
      }

      c1_h_k = 2;
      exitg2 = false;
      while ((!exitg2) && (c1_h_k <= c1_d_n)) {
        c1_q_x = c1_b_varargin_1->data[c1_h_k - 1];
        c1_r_x = c1_q_x;
        c1_db_b = muDoubleScalarIsNaN(c1_r_x);
        c1_d_p = !c1_db_b;
        if (c1_d_p) {
          c1_d_idx = c1_h_k;
          exitg2 = true;
        } else {
          c1_h_k++;
        }
      }
    }

    if (c1_d_idx == 0) {
      c1_maxval = c1_b_varargin_1->data[0];
    } else {
      c1_first = c1_d_idx - 1;
      c1_last = c1_c_n;
      c1_ex = c1_b_varargin_1->data[c1_first];
      c1_i101 = c1_first + 2;
      c1_k_a = c1_i101;
      c1_bb_b = c1_last;
      c1_l_a = c1_k_a;
      c1_cb_b = c1_bb_b;
      if (c1_l_a > c1_cb_b) {
        c1_h_overflow = false;
      } else {
        c1_h_overflow = (c1_cb_b > 2147483646);
      }

      if (c1_h_overflow) {
        c1_check_forloop_overflow_error(chartInstance, true);
      }

      for (c1_i_k = c1_i101 - 1; c1_i_k < c1_last; c1_i_k++) {
        if (c1_ex < c1_b_varargin_1->data[c1_i_k]) {
          c1_ex = c1_b_varargin_1->data[c1_i_k];
        }
      }

      c1_maxval = c1_ex;
    }
  }

  c1_emxInit_real_T(chartInstance, &c1_c_H, 2, &c1_kb_emlrtRTEI);
  c1_o_x = 0.3 * c1_maxval;
  c1_p_x = c1_o_x;
  c1_p_x = muDoubleScalarCeil(c1_p_x);
  c1_i100 = c1_c_H->size[0] * c1_c_H->size[1];
  c1_c_H->size[0] = c1_H->size[0];
  c1_c_H->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_c_H, c1_i100, &c1_kb_emlrtRTEI);
  c1_nb_loop_ub = c1_H->size[0] * c1_H->size[1] - 1;
  for (c1_i102 = 0; c1_i102 <= c1_nb_loop_ub; c1_i102++) {
    c1_c_H->data[c1_i102] = c1_H->data[c1_i102];
  }

  c1_emxInit_real_T(chartInstance, &c1_r2, 2, &c1_wb_emlrtRTEI);
  c1_houghpeaks(chartInstance, c1_c_H, c1_p_x, c1_r2);
  c1_i103 = c1_P->size[0] * c1_P->size[1];
  c1_P->size[0] = c1_r2->size[0];
  c1_P->size[1] = c1_r2->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_P, c1_i103, &c1_lb_emlrtRTEI);
  c1_ob_loop_ub = c1_r2->size[0] * c1_r2->size[1] - 1;
  c1_emxFree_real_T(chartInstance, &c1_c_H);
  for (c1_i104 = 0; c1_i104 <= c1_ob_loop_ub; c1_i104++) {
    c1_P->data[c1_i104] = c1_r2->data[c1_i104];
  }

  c1_emxFree_real_T(chartInstance, &c1_r2);
  c1_emxInit_boolean_T(chartInstance, &c1_d_BW, 2, &c1_mb_emlrtRTEI);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 58);
  c1_i105 = c1_d_BW->size[0] * c1_d_BW->size[1];
  c1_d_BW->size[0] = c1_BW->size[0];
  c1_d_BW->size[1] = c1_BW->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_d_BW, c1_i105,
    &c1_mb_emlrtRTEI);
  c1_pb_loop_ub = c1_BW->size[0] * c1_BW->size[1] - 1;
  for (c1_i106 = 0; c1_i106 <= c1_pb_loop_ub; c1_i106++) {
    c1_d_BW->data[c1_i106] = c1_BW->data[c1_i106];
  }

  c1_emxInit_real_T(chartInstance, &c1_c_rho, 2, &c1_nb_emlrtRTEI);
  c1_i107 = c1_c_rho->size[0] * c1_c_rho->size[1];
  c1_c_rho->size[0] = 1;
  c1_c_rho->size[1] = c1_rho->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_c_rho, c1_i107, &c1_nb_emlrtRTEI);
  c1_qb_loop_ub = c1_rho->size[0] * c1_rho->size[1] - 1;
  for (c1_i108 = 0; c1_i108 <= c1_qb_loop_ub; c1_i108++) {
    c1_c_rho->data[c1_i108] = c1_rho->data[c1_i108];
  }

  c1_emxInit_real_T(chartInstance, &c1_b_P, 2, &c1_ob_emlrtRTEI);
  c1_i109 = c1_b_P->size[0] * c1_b_P->size[1];
  c1_b_P->size[0] = c1_P->size[0];
  c1_b_P->size[1] = c1_P->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_P, c1_i109, &c1_ob_emlrtRTEI);
  c1_rb_loop_ub = c1_P->size[0] * c1_P->size[1] - 1;
  for (c1_i110 = 0; c1_i110 <= c1_rb_loop_ub; c1_i110++) {
    c1_b_P->data[c1_i110] = c1_P->data[c1_i110];
  }

  c1_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_r3, 2, &c1_vb_emlrtRTEI);
  c1_houghlines(chartInstance, c1_d_BW, c1_c_rho, c1_b_P, c1_r3);
  c1_i111 = c1_lines->size[0] * c1_lines->size[1];
  c1_lines->size[0] = 1;
  c1_lines->size[1] = c1_r3->size[1];
  c1_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c1_lines, c1_i111,
    &c1_pb_emlrtRTEI);
  c1_sb_loop_ub = c1_r3->size[0] * c1_r3->size[1] - 1;
  c1_emxFree_real_T(chartInstance, &c1_b_P);
  c1_emxFree_real_T(chartInstance, &c1_c_rho);
  c1_emxFree_boolean_T(chartInstance, &c1_d_BW);
  for (c1_i112 = 0; c1_i112 <= c1_sb_loop_ub; c1_i112++) {
    c1_lines->data[c1_i112] = c1_r3->data[c1_i112];
  }

  c1_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_r3);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 61);
  c1_varargin_2 = c1_lines->size[1];
  c1_b_varargin_2 = c1_varargin_2;
  c1_e_n = (real_T)c1_b_varargin_2;
  c1_c_varargin_2 = c1_lines->size[1];
  c1_d_varargin_2 = c1_c_varargin_2;
  c1_f_n = (real_T)c1_d_varargin_2;
  if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 0, c1_e_n, 1.0, -1, 2U,
        c1_f_n < 1.0))) {
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 62);
    for (c1_i113 = 0; c1_i113 < 4; c1_i113++) {
      c1_b_rotationMatrix[c1_i113] = 0.0;
    }

    for (c1_j_k = 0; c1_j_k < 2; c1_j_k++) {
      c1_k_k = c1_j_k;
      c1_b_rotationMatrix[c1_k_k + (c1_k_k << 1)] = 1.0;
    }

    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 63);
    for (c1_i115 = 0; c1_i115 < 4; c1_i115++) {
      c1_b_invRotationMatrix[c1_i115] = 0.0;
    }

    for (c1_l_k = 0; c1_l_k < 2; c1_l_k++) {
      c1_m_k = c1_l_k;
      c1_b_invRotationMatrix[c1_m_k + (c1_m_k << 1)] = 1.0;
    }

    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 64);
  } else {
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 69);
    c1_e_varargin_2 = c1_lines->size[1];
    c1_f_varargin_2 = c1_e_varargin_2;
    c1_g_n = (real_T)c1_f_varargin_2;
    c1_i114 = c1_orientationsOfLines->size[0];
    c1_b_u = c1_g_n;
    if (c1_b_u < 0.0) {
      c1_l_y = c1_b_u;
    } else {
      c1_l_y = c1_b_u;
    }

    c1_orientationsOfLines->size[0] = c1__s32_d_(chartInstance, c1_l_y, 1U, 2537,
      42);
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_orientationsOfLines, c1_i114,
      &c1_qb_emlrtRTEI);
    c1_c_u = c1_g_n;
    if (c1_c_u < 0.0) {
      c1_m_y = c1_c_u;
    } else {
      c1_m_y = c1_c_u;
    }

    c1_tb_loop_ub = c1__s32_d_(chartInstance, c1_m_y, 1U, 2537, 42) - 1;
    for (c1_i116 = 0; c1_i116 <= c1_tb_loop_ub; c1_i116++) {
      c1_orientationsOfLines->data[c1_i116] = 0.0;
    }

    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 70);
    c1_g_varargin_2 = c1_lines->size[1];
    c1_h_varargin_2 = c1_g_varargin_2;
    c1_d0 = (real_T)c1_h_varargin_2;
    c1_i119 = (int32_T)c1_d0 - 1;
    c1_k = 1.0;
    c1_n_k = 0;
    while (c1_n_k <= c1_i119) {
      c1_k = 1.0 + (real_T)c1_n_k;
      CV_EML_FOR(0, 1, 0, 1);
      _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 71);
      c1_orientationsOfLines->data[sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 2609, 22,
         MAX_uint32_T, (int32_T)c1_k, 1, c1_orientationsOfLines->size[0]) - 1] =
        c1_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 2632, 8, MAX_uint32_T, (int32_T)c1_k, 1,
        c1_lines->size[1]) - 1].theta;
      c1_n_k++;
      _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
    }

    CV_EML_FOR(0, 1, 0, 0);
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 74);
    c1_i120 = c1_b_varargin_1->size[0];
    c1_b_varargin_1->size[0] = c1_orientationsOfLines->size[0];
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_varargin_1, c1_i120,
      &c1_rb_emlrtRTEI);
    c1_ub_loop_ub = c1_orientationsOfLines->size[0] - 1;
    for (c1_i121 = 0; c1_i121 <= c1_ub_loop_ub; c1_i121++) {
      c1_b_varargin_1->data[c1_i121] = c1_orientationsOfLines->data[c1_i121];
    }

    if ((c1_b_varargin_1->size[0] == 1) || ((real_T)c1_b_varargin_1->size[0] !=
         1.0)) {
      c1_b3 = true;
    } else {
      c1_b3 = false;
    }

    if (c1_b3) {
    } else {
      c1_n_y = NULL;
      sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv16, 10, 0U, 1U, 0U, 2, 1,
        36), false);
      c1_o_y = NULL;
      sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv16, 10, 0U, 1U, 0U, 2, 1,
        36), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_n_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_o_y)));
    }

    if (c1_b_varargin_1->size[0] == 0) {
      c1_rotAngle = rtNaN;
    } else {
      c1_b_sort(chartInstance, c1_b_varargin_1);
      c1_M = c1_b_varargin_1->data[0];
      c1_F = 1;
      c1_mtmp = c1_M;
      c1_ftmp = 1;
      c1_d2 = (real_T)c1_b_varargin_1->size[0];
      c1_i122 = (int32_T)c1_d2 - 2;
      for (c1_o_k = 0; c1_o_k <= c1_i122; c1_o_k++) {
        c1_p_k = 2.0 + (real_T)c1_o_k;
        if (c1_b_varargin_1->data[(int32_T)c1_p_k - 1] == c1_mtmp) {
          c1_m_a = c1_ftmp + 1;
          c1_ftmp = c1_m_a;
        } else {
          if (c1_ftmp > c1_F) {
            c1_M = c1_mtmp;
            c1_F = c1_ftmp;
          }

          c1_mtmp = c1_b_varargin_1->data[(int32_T)c1_p_k - 1];
          c1_ftmp = 1;
        }
      }

      if (c1_ftmp > c1_F) {
        c1_M = c1_mtmp;
      }

      c1_rotAngle = c1_M;
    }

    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 77);
    c1_d1 = -c1_rotAngle;
    c1_b_cosd(chartInstance, &c1_d1);
    c1_d3 = -c1_rotAngle;
    c1_b_sind(chartInstance, &c1_d3);
    c1_d4 = -c1_rotAngle;
    c1_b_sind(chartInstance, &c1_d4);
    c1_d5 = -c1_rotAngle;
    c1_b_cosd(chartInstance, &c1_d5);
    c1_b_rotationMatrix[0] = c1_d1;
    c1_b_rotationMatrix[2] = c1_d3;
    c1_b_rotationMatrix[1] = -c1_d4;
    c1_b_rotationMatrix[3] = c1_d5;
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 81);
    c1_d6 = c1_rotAngle;
    c1_b_cosd(chartInstance, &c1_d6);
    c1_d7 = c1_rotAngle;
    c1_b_sind(chartInstance, &c1_d7);
    c1_d8 = c1_rotAngle;
    c1_b_sind(chartInstance, &c1_d8);
    c1_d9 = c1_rotAngle;
    c1_b_cosd(chartInstance, &c1_d9);
    c1_b_invRotationMatrix[0] = c1_d6;
    c1_b_invRotationMatrix[2] = c1_d7;
    c1_b_invRotationMatrix[1] = -c1_d8;
    c1_b_invRotationMatrix[3] = c1_d9;
  }

  c1_emxFree_real_T(chartInstance, &c1_b_varargin_1);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, -81);
  _SFD_SYMBOL_SCOPE_POP();
  for (c1_i117 = 0; c1_i117 < 4; c1_i117++) {
    (*chartInstance->c1_rotationMatrix)[c1_i117] = c1_b_rotationMatrix[c1_i117];
  }

  for (c1_i118 = 0; c1_i118 < 4; c1_i118++) {
    (*chartInstance->c1_invRotationMatrix)[c1_i118] =
      c1_b_invRotationMatrix[c1_i118];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  c1_emxFree_real_T(chartInstance, &c1_orientationsOfLines);
  c1_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_lines);
  c1_emxFree_real_T(chartInstance, &c1_P);
  c1_emxFree_real_T(chartInstance, &c1_rho);
  c1_emxFree_real_T(chartInstance, &c1_H);
  c1_emxFree_boolean_T(chartInstance, &c1_BW);
  c1_emxFree_real_T(chartInstance, &c1_y);
  c1_emxFree_real_T(chartInstance, &c1_x);
  c1_emxFree_real_T(chartInstance, &c1_obstacleMatrix);
  c1_emxFree_real_T(chartInstance, &c1_matrOfObstaclestemp);
}

static void initSimStructsc1_LIDAR_sim(SFc1_LIDAR_simInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)(c1_machineNumber);
  (void)(c1_chartNumber);
  (void)(c1_instanceNumber);
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_real_T *c1_b_u;
  int32_T c1_i123;
  int32_T c1_i124;
  int32_T c1_i125;
  int32_T c1_loop_ub;
  int32_T c1_i126;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i123 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = c1_inData->size[0];
  c1_b_u->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_u, c1_i123, (emlrtRTEInfo *)
    NULL);
  c1_i124 = c1_b_u->size[0];
  c1_i125 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i126 = 0; c1_i126 <= c1_loop_ub; c1_i126++) {
    c1_b_u->data[c1_i126] = c1_inData->data[c1_i126];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 0, 0U, 1U, 0U, 2,
    c1_b_u->size[0], c1_b_u->size[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_real_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y)
{
  c1_emxArray_real_T *c1_r4;
  int32_T c1_i127;
  int32_T c1_i128;
  uint32_T c1_uv0[2];
  int32_T c1_i129;
  boolean_T c1_bv0[2];
  static boolean_T c1_bv1[2] = { true, false };

  int32_T c1_i130;
  int32_T c1_i131;
  int32_T c1_i132;
  int32_T c1_loop_ub;
  int32_T c1_i133;
  c1_emxInit_real_T(chartInstance, &c1_r4, 2, (emlrtRTEInfo *)NULL);
  for (c1_i127 = 0; c1_i127 < 2; c1_i127++) {
    c1_uv0[c1_i127] = 50000U + (uint32_T)(-49998 * c1_i127);
  }

  c1_i128 = c1_r4->size[0] * c1_r4->size[1];
  c1_r4->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_r4->size[1] = sf_mex_get_dimension(c1_b_u, 1);
  c1_emxEnsureCapacity_real_T(chartInstance, c1_r4, c1_i128, (emlrtRTEInfo *)
    NULL);
  for (c1_i129 = 0; c1_i129 < 2; c1_i129++) {
    c1_bv0[c1_i129] = c1_bv1[c1_i129];
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r4->data, 1, 0, 0U, 1, 0U,
                   2, c1_bv0, c1_uv0, c1_r4->size);
  c1_i130 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = c1_r4->size[0];
  c1_y->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_y, c1_i130, (emlrtRTEInfo *)NULL);
  c1_i131 = c1_y->size[0];
  c1_i132 = c1_y->size[1];
  c1_loop_ub = c1_r4->size[0] * c1_r4->size[1] - 1;
  for (c1_i133 = 0; c1_i133 <= c1_loop_ub; c1_i133++) {
    c1_y->data[c1_i133] = c1_r4->data[c1_i133];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_real_T(chartInstance, &c1_r4);
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData)
{
  c1_emxArray_real_T *c1_y;
  const mxArray *c1_matrOfObstaclestemp;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i134;
  int32_T c1_i135;
  int32_T c1_loop_ub;
  int32_T c1_i136;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_y, 2, (emlrtRTEInfo *)NULL);
  c1_matrOfObstaclestemp = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_matrOfObstaclestemp),
                      &c1_thisId, c1_y);
  sf_mex_destroy(&c1_matrOfObstaclestemp);
  c1_i134 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = c1_y->size[0];
  c1_outData->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_outData, c1_i134, (emlrtRTEInfo *)
    NULL);
  for (c1_i135 = 0; c1_i135 < 2; c1_i135++) {
    c1_loop_ub = c1_y->size[0] - 1;
    for (c1_i136 = 0; c1_i136 <= c1_loop_ub; c1_i136++) {
      c1_outData->data[c1_i136 + c1_outData->size[0] * c1_i135] = c1_y->
        data[c1_i136 + c1_y->size[0] * c1_i135];
    }
  }

  c1_emxFree_real_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  real_T c1_b_u;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_b_u = *(real_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_real_T *c1_b_u;
  int32_T c1_i137;
  int32_T c1_i138;
  int32_T c1_i139;
  int32_T c1_loop_ub;
  int32_T c1_i140;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i137 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = 1;
  c1_b_u->size[1] = c1_inData->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_u, c1_i137, (emlrtRTEInfo *)
    NULL);
  c1_i138 = c1_b_u->size[0];
  c1_i139 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i140 = 0; c1_i140 <= c1_loop_ub; c1_i140++) {
    c1_b_u->data[c1_i140] = c1_inData->data[c1_i140];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 0, 0U, 1U, 0U, 2,
    c1_b_u->size[0], c1_b_u->size[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_real_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_b_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y)
{
  c1_emxArray_real_T *c1_r5;
  int32_T c1_i141;
  int32_T c1_i142;
  uint32_T c1_uv1[2];
  int32_T c1_i143;
  boolean_T c1_bv2[2];
  static boolean_T c1_bv3[2] = { false, true };

  int32_T c1_i144;
  int32_T c1_i145;
  int32_T c1_i146;
  int32_T c1_loop_ub;
  int32_T c1_i147;
  c1_emxInit_real_T(chartInstance, &c1_r5, 2, (emlrtRTEInfo *)NULL);
  for (c1_i141 = 0; c1_i141 < 2; c1_i141++) {
    c1_uv1[c1_i141] = 1U + 49999U * (uint32_T)c1_i141;
  }

  c1_i142 = c1_r5->size[0] * c1_r5->size[1];
  c1_r5->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_r5->size[1] = sf_mex_get_dimension(c1_b_u, 1);
  c1_emxEnsureCapacity_real_T(chartInstance, c1_r5, c1_i142, (emlrtRTEInfo *)
    NULL);
  for (c1_i143 = 0; c1_i143 < 2; c1_i143++) {
    c1_bv2[c1_i143] = c1_bv3[c1_i143];
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r5->data, 1, 0, 0U, 1, 0U,
                   2, c1_bv2, c1_uv1, c1_r5->size);
  c1_i144 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = 1;
  c1_y->size[1] = c1_r5->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_y, c1_i144, (emlrtRTEInfo *)NULL);
  c1_i145 = c1_y->size[0];
  c1_i146 = c1_y->size[1];
  c1_loop_ub = c1_r5->size[0] * c1_r5->size[1] - 1;
  for (c1_i147 = 0; c1_i147 <= c1_loop_ub; c1_i147++) {
    c1_y->data[c1_i147] = c1_r5->data[c1_i147];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_real_T(chartInstance, &c1_r5);
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData)
{
  c1_emxArray_real_T *c1_y;
  const mxArray *c1_x;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i148;
  int32_T c1_loop_ub;
  int32_T c1_i149;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_y, 2, (emlrtRTEInfo *)NULL);
  c1_x = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_x), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_x);
  c1_i148 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = 1;
  c1_outData->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_outData, c1_i148, (emlrtRTEInfo *)
    NULL);
  c1_loop_ub = c1_y->size[1] - 1;
  for (c1_i149 = 0; c1_i149 <= c1_loop_ub; c1_i149++) {
    c1_outData->data[c1_i149] = c1_y->data[c1_i149];
  }

  c1_emxFree_real_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_boolean_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_boolean_T *c1_b_u;
  int32_T c1_i150;
  int32_T c1_i151;
  int32_T c1_i152;
  int32_T c1_loop_ub;
  int32_T c1_i153;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_boolean_T(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i150 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = c1_inData->size[0];
  c1_b_u->size[1] = c1_inData->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_b_u, c1_i150, (emlrtRTEInfo *)
    NULL);
  c1_i151 = c1_b_u->size[0];
  c1_i152 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i153 = 0; c1_i153 <= c1_loop_ub; c1_i153++) {
    c1_b_u->data[c1_i153] = c1_inData->data[c1_i153];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 11, 0U, 1U, 0U, 2,
    c1_b_u->size[0], c1_b_u->size[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_boolean_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_c_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_boolean_T *c1_y)
{
  c1_emxArray_boolean_T *c1_r6;
  int32_T c1_i154;
  int32_T c1_i155;
  uint32_T c1_uv2[2];
  int32_T c1_i156;
  boolean_T c1_bv4[2];
  int32_T c1_i157;
  int32_T c1_i158;
  int32_T c1_i159;
  int32_T c1_loop_ub;
  int32_T c1_i160;
  c1_emxInit_boolean_T(chartInstance, &c1_r6, 2, (emlrtRTEInfo *)NULL);
  for (c1_i154 = 0; c1_i154 < 2; c1_i154++) {
    c1_uv2[c1_i154] = MAX_uint32_T;
  }

  c1_i155 = c1_r6->size[0] * c1_r6->size[1];
  c1_r6->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_r6->size[1] = sf_mex_get_dimension(c1_b_u, 1);
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_r6, c1_i155, (emlrtRTEInfo *)
    NULL);
  for (c1_i156 = 0; c1_i156 < 2; c1_i156++) {
    c1_bv4[c1_i156] = true;
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r6->data, 1, 11, 0U, 1,
                   0U, 2, c1_bv4, c1_uv2, c1_r6->size);
  c1_i157 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = c1_r6->size[0];
  c1_y->size[1] = c1_r6->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_y, c1_i157, (emlrtRTEInfo *)
    NULL);
  c1_i158 = c1_y->size[0];
  c1_i159 = c1_y->size[1];
  c1_loop_ub = c1_r6->size[0] * c1_r6->size[1] - 1;
  for (c1_i160 = 0; c1_i160 <= c1_loop_ub; c1_i160++) {
    c1_y->data[c1_i160] = c1_r6->data[c1_i160];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_boolean_T(chartInstance, &c1_r6);
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_boolean_T *c1_outData)
{
  c1_emxArray_boolean_T *c1_y;
  const mxArray *c1_BW;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i161;
  int32_T c1_loop_ub;
  int32_T c1_i162;
  int32_T c1_b_loop_ub;
  int32_T c1_i163;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_boolean_T(chartInstance, &c1_y, 2, (emlrtRTEInfo *)NULL);
  c1_BW = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_BW), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_BW);
  c1_i161 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = c1_y->size[0];
  c1_outData->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_outData, c1_i161,
    (emlrtRTEInfo *)NULL);
  c1_loop_ub = c1_y->size[1] - 1;
  for (c1_i162 = 0; c1_i162 <= c1_loop_ub; c1_i162++) {
    c1_b_loop_ub = c1_y->size[0] - 1;
    for (c1_i163 = 0; c1_i163 <= c1_b_loop_ub; c1_i163++) {
      c1_outData->data[c1_i163 + c1_outData->size[0] * c1_i162] = c1_y->
        data[c1_i163 + c1_y->size[0] * c1_i162];
    }
  }

  c1_emxFree_boolean_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_real_T *c1_b_u;
  int32_T c1_i164;
  int32_T c1_i165;
  int32_T c1_i166;
  int32_T c1_loop_ub;
  int32_T c1_i167;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i164 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = c1_inData->size[0];
  c1_b_u->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_u, c1_i164, (emlrtRTEInfo *)
    NULL);
  c1_i165 = c1_b_u->size[0];
  c1_i166 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i167 = 0; c1_i167 <= c1_loop_ub; c1_i167++) {
    c1_b_u->data[c1_i167] = c1_inData->data[c1_i167];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 0, 0U, 1U, 0U, 2,
    c1_b_u->size[0], c1_b_u->size[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_real_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_d_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y)
{
  c1_emxArray_real_T *c1_r7;
  int32_T c1_i168;
  int32_T c1_i169;
  uint32_T c1_uv3[2];
  static uint32_T c1_uv4[2] = { MAX_uint32_T, 181U };

  int32_T c1_i170;
  boolean_T c1_bv5[2];
  static boolean_T c1_bv6[2] = { true, false };

  int32_T c1_i171;
  int32_T c1_i172;
  int32_T c1_i173;
  int32_T c1_loop_ub;
  int32_T c1_i174;
  c1_emxInit_real_T(chartInstance, &c1_r7, 2, (emlrtRTEInfo *)NULL);
  for (c1_i168 = 0; c1_i168 < 2; c1_i168++) {
    c1_uv3[c1_i168] = c1_uv4[c1_i168];
  }

  c1_i169 = c1_r7->size[0] * c1_r7->size[1];
  c1_r7->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_r7->size[1] = sf_mex_get_dimension(c1_b_u, 1);
  c1_emxEnsureCapacity_real_T(chartInstance, c1_r7, c1_i169, (emlrtRTEInfo *)
    NULL);
  for (c1_i170 = 0; c1_i170 < 2; c1_i170++) {
    c1_bv5[c1_i170] = c1_bv6[c1_i170];
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r7->data, 1, 0, 0U, 1, 0U,
                   2, c1_bv5, c1_uv3, c1_r7->size);
  c1_i171 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = c1_r7->size[0];
  c1_y->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_y, c1_i171, (emlrtRTEInfo *)NULL);
  c1_i172 = c1_y->size[0];
  c1_i173 = c1_y->size[1];
  c1_loop_ub = c1_r7->size[0] * c1_r7->size[1] - 1;
  for (c1_i174 = 0; c1_i174 <= c1_loop_ub; c1_i174++) {
    c1_y->data[c1_i174] = c1_r7->data[c1_i174];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_real_T(chartInstance, &c1_r7);
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData)
{
  c1_emxArray_real_T *c1_y;
  const mxArray *c1_H;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i175;
  int32_T c1_i176;
  int32_T c1_loop_ub;
  int32_T c1_i177;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_y, 2, (emlrtRTEInfo *)NULL);
  c1_H = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_H), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_H);
  c1_i175 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = c1_y->size[0];
  c1_outData->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_outData, c1_i175, (emlrtRTEInfo *)
    NULL);
  for (c1_i176 = 0; c1_i176 < 181; c1_i176++) {
    c1_loop_ub = c1_y->size[0] - 1;
    for (c1_i177 = 0; c1_i177 <= c1_loop_ub; c1_i177++) {
      c1_outData->data[c1_i177 + c1_outData->size[0] * c1_i176] = c1_y->
        data[c1_i177 + c1_y->size[0] * c1_i176];
    }
  }

  c1_emxFree_real_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int32_T c1_i178;
  const mxArray *c1_y = NULL;
  real_T c1_b_u[181];
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  for (c1_i178 = 0; c1_i178 < 181; c1_i178++) {
    c1_b_u[c1_i178] = (*(real_T (*)[181])c1_inData)[c1_i178];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u, 0, 0U, 1U, 0U, 2, 1, 181),
                false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_real_T *c1_b_u;
  int32_T c1_i179;
  int32_T c1_i180;
  int32_T c1_i181;
  int32_T c1_loop_ub;
  int32_T c1_i182;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i179 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = 1;
  c1_b_u->size[1] = c1_inData->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_u, c1_i179, (emlrtRTEInfo *)
    NULL);
  c1_i180 = c1_b_u->size[0];
  c1_i181 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i182 = 0; c1_i182 <= c1_loop_ub; c1_i182++) {
    c1_b_u->data[c1_i182] = c1_inData->data[c1_i182];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 0, 0U, 1U, 0U, 2,
    c1_b_u->size[0], c1_b_u->size[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_real_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_e_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y)
{
  c1_emxArray_real_T *c1_r8;
  int32_T c1_i183;
  int32_T c1_i184;
  uint32_T c1_uv5[2];
  int32_T c1_i185;
  boolean_T c1_bv7[2];
  static boolean_T c1_bv8[2] = { false, true };

  int32_T c1_i186;
  int32_T c1_i187;
  int32_T c1_i188;
  int32_T c1_loop_ub;
  int32_T c1_i189;
  c1_emxInit_real_T(chartInstance, &c1_r8, 2, (emlrtRTEInfo *)NULL);
  for (c1_i183 = 0; c1_i183 < 2; c1_i183++) {
    c1_uv5[c1_i183] = 1U + 4294967294U * (uint32_T)c1_i183;
  }

  c1_i184 = c1_r8->size[0] * c1_r8->size[1];
  c1_r8->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_r8->size[1] = sf_mex_get_dimension(c1_b_u, 1);
  c1_emxEnsureCapacity_real_T(chartInstance, c1_r8, c1_i184, (emlrtRTEInfo *)
    NULL);
  for (c1_i185 = 0; c1_i185 < 2; c1_i185++) {
    c1_bv7[c1_i185] = c1_bv8[c1_i185];
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r8->data, 1, 0, 0U, 1, 0U,
                   2, c1_bv7, c1_uv5, c1_r8->size);
  c1_i186 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = 1;
  c1_y->size[1] = c1_r8->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_y, c1_i186, (emlrtRTEInfo *)NULL);
  c1_i187 = c1_y->size[0];
  c1_i188 = c1_y->size[1];
  c1_loop_ub = c1_r8->size[0] * c1_r8->size[1] - 1;
  for (c1_i189 = 0; c1_i189 <= c1_loop_ub; c1_i189++) {
    c1_y->data[c1_i189] = c1_r8->data[c1_i189];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_real_T(chartInstance, &c1_r8);
}

static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData)
{
  c1_emxArray_real_T *c1_y;
  const mxArray *c1_rho;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i190;
  int32_T c1_loop_ub;
  int32_T c1_i191;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_y, 2, (emlrtRTEInfo *)NULL);
  c1_rho = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_rho), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_rho);
  c1_i190 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = 1;
  c1_outData->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_outData, c1_i190, (emlrtRTEInfo *)
    NULL);
  c1_loop_ub = c1_y->size[1] - 1;
  for (c1_i191 = 0; c1_i191 <= c1_loop_ub; c1_i191++) {
    c1_outData->data[c1_i191] = c1_y->data[c1_i191];
  }

  c1_emxFree_real_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_h_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_real_T *c1_b_u;
  int32_T c1_i192;
  int32_T c1_i193;
  int32_T c1_i194;
  int32_T c1_loop_ub;
  int32_T c1_i195;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i192 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = c1_inData->size[0];
  c1_b_u->size[1] = c1_inData->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_u, c1_i192, (emlrtRTEInfo *)
    NULL);
  c1_i193 = c1_b_u->size[0];
  c1_i194 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i195 = 0; c1_i195 <= c1_loop_ub; c1_i195++) {
    c1_b_u->data[c1_i195] = c1_inData->data[c1_i195];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 0, 0U, 1U, 0U, 2,
    c1_b_u->size[0], c1_b_u->size[1]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_real_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_f_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y)
{
  c1_emxArray_real_T *c1_r9;
  int32_T c1_i196;
  int32_T c1_i197;
  uint32_T c1_uv6[2];
  static uint32_T c1_uv7[2] = { MAX_uint32_T, 2U };

  int32_T c1_i198;
  boolean_T c1_bv9[2];
  int32_T c1_i199;
  int32_T c1_i200;
  int32_T c1_i201;
  int32_T c1_loop_ub;
  int32_T c1_i202;
  c1_emxInit_real_T(chartInstance, &c1_r9, 2, (emlrtRTEInfo *)NULL);
  for (c1_i196 = 0; c1_i196 < 2; c1_i196++) {
    c1_uv6[c1_i196] = c1_uv7[c1_i196];
  }

  c1_i197 = c1_r9->size[0] * c1_r9->size[1];
  c1_r9->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_r9->size[1] = sf_mex_get_dimension(c1_b_u, 1);
  c1_emxEnsureCapacity_real_T(chartInstance, c1_r9, c1_i197, (emlrtRTEInfo *)
    NULL);
  for (c1_i198 = 0; c1_i198 < 2; c1_i198++) {
    c1_bv9[c1_i198] = true;
  }

  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r9->data, 1, 0, 0U, 1, 0U,
                   2, c1_bv9, c1_uv6, c1_r9->size);
  c1_i199 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = c1_r9->size[0];
  c1_y->size[1] = c1_r9->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_y, c1_i199, (emlrtRTEInfo *)NULL);
  c1_i200 = c1_y->size[0];
  c1_i201 = c1_y->size[1];
  c1_loop_ub = c1_r9->size[0] * c1_r9->size[1] - 1;
  for (c1_i202 = 0; c1_i202 <= c1_loop_ub; c1_i202++) {
    c1_y->data[c1_i202] = c1_r9->data[c1_i202];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_real_T(chartInstance, &c1_r9);
}

static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData)
{
  c1_emxArray_real_T *c1_y;
  const mxArray *c1_P;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i203;
  int32_T c1_loop_ub;
  int32_T c1_i204;
  int32_T c1_b_loop_ub;
  int32_T c1_i205;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T(chartInstance, &c1_y, 2, (emlrtRTEInfo *)NULL);
  c1_P = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_P), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_P);
  c1_i203 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = c1_y->size[0];
  c1_outData->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_outData, c1_i203, (emlrtRTEInfo *)
    NULL);
  c1_loop_ub = c1_y->size[1] - 1;
  for (c1_i204 = 0; c1_i204 <= c1_loop_ub; c1_i204++) {
    c1_b_loop_ub = c1_y->size[0] - 1;
    for (c1_i205 = 0; c1_i205 <= c1_b_loop_ub; c1_i205++) {
      c1_outData->data[c1_i205 + c1_outData->size[0] * c1_i204] = c1_y->
        data[c1_i205 + c1_y->size[0] * c1_i204];
    }
  }

  c1_emxFree_real_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_i_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_b_u;
  int32_T c1_i206;
  int32_T c1_i207;
  int32_T c1_i208;
  int32_T c1_loop_ub;
  int32_T c1_i209;
  const mxArray *c1_y = NULL;
  int32_T c1_i210;
  int32_T c1_iv1[2];
  static const char * c1_sv0[4] = { "point1", "point2", "theta", "rho" };

  int32_T c1_i;
  int32_T c1_j1;
  int32_T c1_i211;
  const mxArray *c1_b_y = NULL;
  real_T c1_c_u[2];
  int32_T c1_i212;
  const mxArray *c1_c_y = NULL;
  real_T c1_d_u;
  const mxArray *c1_d_y = NULL;
  real_T c1_e_u;
  const mxArray *c1_e_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_b_u, 2, (emlrtRTEInfo *)
    NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i206 = c1_b_u->size[0] * c1_b_u->size[1];
  c1_b_u->size[0] = 1;
  c1_b_u->size[1] = c1_inData->size[1];
  c1_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c1_b_u, c1_i206,
    (emlrtRTEInfo *)NULL);
  c1_i207 = c1_b_u->size[0];
  c1_i208 = c1_b_u->size[1];
  c1_loop_ub = c1_inData->size[0] * c1_inData->size[1] - 1;
  for (c1_i209 = 0; c1_i209 <= c1_loop_ub; c1_i209++) {
    c1_b_u->data[c1_i209] = c1_inData->data[c1_i209];
  }

  c1_y = NULL;
  for (c1_i210 = 0; c1_i210 < 2; c1_i210++) {
    c1_iv1[c1_i210] = c1_b_u->size[c1_i210];
  }

  sf_mex_assign(&c1_y, sf_mex_createstructarray("structure", 2, c1_iv1, 4,
    c1_sv0), false);
  sf_mex_createfield(c1_y, "point1", "point1");
  sf_mex_createfield(c1_y, "point2", "point2");
  sf_mex_createfield(c1_y, "theta", "theta");
  sf_mex_createfield(c1_y, "rho", "rho");
  c1_i = 0;
  for (c1_j1 = 0; c1_j1 < c1_b_u->size[1U]; c1_j1++) {
    for (c1_i211 = 0; c1_i211 < 2; c1_i211++) {
      c1_c_u[c1_i211] = c1_b_u->data[c1_j1].point1[c1_i211];
    }

    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_c_u, 0, 0U, 1U, 0U, 2, 1, 2),
                  false);
    sf_mex_setfieldbynum(c1_y, c1_i, "point1", c1_b_y, 0);
    for (c1_i212 = 0; c1_i212 < 2; c1_i212++) {
      c1_c_u[c1_i212] = c1_b_u->data[c1_j1].point2[c1_i212];
    }

    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_c_u, 0, 0U, 1U, 0U, 2, 1, 2),
                  false);
    sf_mex_setfieldbynum(c1_y, c1_i, "point2", c1_c_y, 1);
    c1_d_u = c1_b_u->data[c1_j1].theta;
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_d_u, 0, 0U, 0U, 0U, 0), false);
    sf_mex_setfieldbynum(c1_y, c1_i, "theta", c1_d_y, 2);
    c1_e_u = c1_b_u->data[c1_j1].rho;
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", &c1_e_u, 0, 0U, 0U, 0U, 0), false);
    sf_mex_setfieldbynum(c1_y, c1_i, "rho", c1_e_y, 3);
    c1_i++;
  }

  c1_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_b_u);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_g_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_y)
{
  int32_T c1_i213;
  int32_T c1_i214;
  uint32_T c1_uv8[2];
  emlrtMsgIdentifier c1_thisId;
  uint32_T c1_uv9[2];
  int32_T c1_i215;
  static const char * c1_fieldNames[4] = { "point1", "point2", "theta", "rho" };

  boolean_T c1_bv10[2];
  static boolean_T c1_bv11[2] = { false, true };

  int32_T c1_i216;
  int32_T c1_n;
  int32_T c1_i217;
  int32_T c1_b_n[1];
  int32_T c1_c_n[1];
  for (c1_i213 = 0; c1_i213 < 2; c1_i213++) {
    c1_uv8[c1_i213] = 1U + 4294967294U * (uint32_T)c1_i213;
  }

  for (c1_i214 = 0; c1_i214 < 2; c1_i214++) {
    c1_uv9[c1_i214] = 1U + 4294967294U * (uint32_T)c1_i214;
  }

  c1_thisId.fParent = c1_parentId;
  c1_thisId.bParentIsCell = false;
  for (c1_i215 = 0; c1_i215 < 2; c1_i215++) {
    c1_bv10[c1_i215] = c1_bv11[c1_i215];
  }

  sf_mex_check_struct_vs(c1_parentId, c1_b_u, 4, c1_fieldNames, 2U, c1_bv10,
    c1_uv8, c1_uv9);
  c1_i216 = c1_y->size[0] * c1_y->size[1];
  c1_y->size[0] = (int32_T)c1_uv9[0];
  c1_y->size[1] = (int32_T)c1_uv9[1];
  c1_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c1_y, c1_i216, (emlrtRTEInfo
    *)NULL);
  c1_n = c1_y->size[1];
  for (c1_i217 = 0; c1_i217 < c1_n; c1_i217++) {
    c1_thisId.fIdentifier = "point1";
    c1_b_n[0] = c1_n;
    c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c1_b_u,
      "point1", "point1", c1_i217)), &c1_thisId, c1_y->data[c1_i217].point1);
    c1_thisId.fIdentifier = "point2";
    c1_c_n[0] = c1_n;
    c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c1_b_u,
      "point2", "point2", c1_i217)), &c1_thisId, c1_y->data[c1_i217].point2);
    c1_thisId.fIdentifier = "theta";
    c1_y->data[c1_i217].theta = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c1_b_u, "theta", "theta", c1_i217)), &c1_thisId);
    c1_thisId.fIdentifier = "rho";
    c1_y->data[c1_i217].rho = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup
      (sf_mex_getfield(c1_b_u, "rho", "rho", c1_i217)), &c1_thisId);
  }

  sf_mex_destroy(&c1_b_u);
}

static void c1_h_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[2])
{
  real_T c1_dv3[2];
  int32_T c1_i218;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), c1_dv3, 1, 0, 0U, 1, 0U, 2, 1,
                2);
  for (c1_i218 = 0; c1_i218 < 2; c1_i218++) {
    c1_y[c1_i218] = c1_dv3[c1_i218];
  }

  sf_mex_destroy(&c1_b_u);
}

static real_T c1_i_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d10;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_d10, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d10;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName,
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_outData)
{
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_y;
  const mxArray *c1_lines;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i219;
  int32_T c1_loop_ub;
  int32_T c1_i220;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_y, 2, (emlrtRTEInfo *)
    NULL);
  c1_lines = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_lines), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_lines);
  c1_i219 = c1_outData->size[0] * c1_outData->size[1];
  c1_outData->size[0] = 1;
  c1_outData->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c1_outData, c1_i219,
    (emlrtRTEInfo *)NULL);
  c1_loop_ub = c1_y->size[1] - 1;
  for (c1_i220 = 0; c1_i220 <= c1_loop_ub; c1_i220++) {
    c1_outData->data[c1_i220] = c1_y->data[c1_i220];
  }

  c1_emxFree_skoeQIuVNKJRHNtBIlOCZhD(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_j_sf_marshallOut(void *chartInstanceVoid,
  c1_emxArray_real_T *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_emxArray_real_T *c1_b_u;
  int32_T c1_i221;
  int32_T c1_loop_ub;
  int32_T c1_i222;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T1(chartInstance, &c1_b_u, 1, (emlrtRTEInfo *)NULL);
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i221 = c1_b_u->size[0];
  c1_b_u->size[0] = c1_inData->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_u, c1_i221, (emlrtRTEInfo *)
    NULL);
  c1_loop_ub = c1_inData->size[0] - 1;
  for (c1_i222 = 0; c1_i222 <= c1_loop_ub; c1_i222++) {
    c1_b_u->data[c1_i222] = c1_inData->data[c1_i222];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u->data, 0, 0U, 1U, 0U, 1,
    c1_b_u->size[0]), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  c1_emxFree_real_T(chartInstance, &c1_b_u);
  return c1_mxArrayOutData;
}

static void c1_j_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  c1_emxArray_real_T *c1_y)
{
  c1_emxArray_real_T *c1_r10;
  uint32_T c1_uv10[1];
  int32_T c1_i223;
  boolean_T c1_bv12[1];
  int32_T c1_i224;
  int32_T c1_loop_ub;
  int32_T c1_i225;
  c1_emxInit_real_T1(chartInstance, &c1_r10, 1, (emlrtRTEInfo *)NULL);
  c1_uv10[0] = MAX_uint32_T;
  c1_i223 = c1_r10->size[0];
  c1_r10->size[0] = sf_mex_get_dimension(c1_b_u, 0);
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_r10, c1_i223, (emlrtRTEInfo *)
    NULL);
  c1_bv12[0] = true;
  sf_mex_import_vs(c1_parentId, sf_mex_dup(c1_b_u), c1_r10->data, 1, 0, 0U, 1,
                   0U, 1, c1_bv12, c1_uv10, c1_r10->size);
  c1_i224 = c1_y->size[0];
  c1_y->size[0] = c1_r10->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_y, c1_i224, (emlrtRTEInfo *)
    NULL);
  c1_loop_ub = c1_r10->size[0] - 1;
  for (c1_i225 = 0; c1_i225 <= c1_loop_ub; c1_i225++) {
    c1_y->data[c1_i225] = c1_r10->data[c1_i225];
  }

  sf_mex_destroy(&c1_b_u);
  c1_emxFree_real_T(chartInstance, &c1_r10);
}

static void c1_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, c1_emxArray_real_T *c1_outData)
{
  c1_emxArray_real_T *c1_y;
  const mxArray *c1_orientationsOfLines;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_i226;
  int32_T c1_loop_ub;
  int32_T c1_i227;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_emxInit_real_T1(chartInstance, &c1_y, 1, (emlrtRTEInfo *)NULL);
  c1_orientationsOfLines = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_orientationsOfLines),
                        &c1_thisId, c1_y);
  sf_mex_destroy(&c1_orientationsOfLines);
  c1_i226 = c1_outData->size[0];
  c1_outData->size[0] = c1_y->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_outData, c1_i226, (emlrtRTEInfo
    *)NULL);
  c1_loop_ub = c1_y->size[0] - 1;
  for (c1_i227 = 0; c1_i227 <= c1_loop_ub; c1_i227++) {
    c1_outData->data[c1_i227] = c1_y->data[c1_i227];
  }

  c1_emxFree_real_T(chartInstance, &c1_y);
  sf_mex_destroy(&c1_mxArrayInData);
}

static void c1_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_k;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_k = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_k), &c1_thisId);
  sf_mex_destroy(&c1_k);
  *(real_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_k_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int32_T c1_i228;
  int32_T c1_i229;
  const mxArray *c1_y = NULL;
  int32_T c1_i230;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i228 = 0;
  for (c1_i229 = 0; c1_i229 < 2; c1_i229++) {
    for (c1_i230 = 0; c1_i230 < 50000; c1_i230++) {
      chartInstance->c1_u[c1_i230 + c1_i228] = (*(real_T (*)[100000])c1_inData)
        [c1_i230 + c1_i228];
    }

    c1_i228 += 50000;
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", chartInstance->c1_u, 0, 0U, 1U, 0U, 2,
    50000, 2), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_l_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int32_T c1_i231;
  int32_T c1_i232;
  const mxArray *c1_y = NULL;
  int32_T c1_i233;
  real_T c1_b_u[4];
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_i231 = 0;
  for (c1_i232 = 0; c1_i232 < 2; c1_i232++) {
    for (c1_i233 = 0; c1_i233 < 2; c1_i233++) {
      c1_b_u[c1_i233 + c1_i231] = (*(real_T (*)[4])c1_inData)[c1_i233 + c1_i231];
    }

    c1_i231 += 2;
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u, 0, 0U, 1U, 0U, 2, 2, 2), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_k_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_rotationMatrix, const char_T *c1_identifier, real_T c1_y[4])
{
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_rotationMatrix),
                        &c1_thisId, c1_y);
  sf_mex_destroy(&c1_b_rotationMatrix);
}

static void c1_l_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[4])
{
  real_T c1_dv4[4];
  int32_T c1_i234;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), c1_dv4, 1, 0, 0U, 1, 0U, 2, 2,
                2);
  for (c1_i234 = 0; c1_i234 < 4; c1_i234++) {
    c1_y[c1_i234] = c1_dv4[c1_i234];
  }

  sf_mex_destroy(&c1_b_u);
}

static void c1_j_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_rotationMatrix;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y[4];
  int32_T c1_i235;
  int32_T c1_i236;
  int32_T c1_i237;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_b_rotationMatrix = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_rotationMatrix),
                        &c1_thisId, c1_y);
  sf_mex_destroy(&c1_b_rotationMatrix);
  c1_i235 = 0;
  for (c1_i236 = 0; c1_i236 < 2; c1_i236++) {
    for (c1_i237 = 0; c1_i237 < 2; c1_i237++) {
      (*(real_T (*)[4])c1_outData)[c1_i237 + c1_i235] = c1_y[c1_i237 + c1_i235];
    }

    c1_i235 += 2;
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

const mxArray *sf_c1_LIDAR_sim_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static void c1_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, boolean_T c1_idx_data[], int32_T c1_idx_size[1],
  c1_emxArray_real_T *c1_b_x)
{
  int32_T c1_i238;
  int32_T c1_i239;
  int32_T c1_i240;
  int32_T c1_loop_ub;
  int32_T c1_i241;
  int32_T c1_b_idx_size[1];
  int32_T c1_b_loop_ub;
  int32_T c1_i242;
  boolean_T c1_b_idx_data[50000];
  c1_i238 = c1_b_x->size[0] * c1_b_x->size[1];
  c1_b_x->size[0] = c1_x->size[0];
  c1_b_x->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_x, c1_i238, &c1_yb_emlrtRTEI);
  c1_i239 = c1_b_x->size[0];
  c1_i240 = c1_b_x->size[1];
  c1_loop_ub = c1_x->size[0] * c1_x->size[1] - 1;
  for (c1_i241 = 0; c1_i241 <= c1_loop_ub; c1_i241++) {
    c1_b_x->data[c1_i241] = c1_x->data[c1_i241];
  }

  c1_b_idx_size[0] = c1_idx_size[0];
  c1_b_loop_ub = c1_idx_size[0] - 1;
  for (c1_i242 = 0; c1_i242 <= c1_b_loop_ub; c1_i242++) {
    c1_b_idx_data[c1_i242] = c1_idx_data[c1_i242];
  }

  c1_c_nullAssignment(chartInstance, c1_b_x, c1_b_idx_data, c1_b_idx_size);
}

static void c1_check_forloop_overflow_error(SFc1_LIDAR_simInstanceStruct
  *chartInstance, boolean_T c1_overflow)
{
  const mxArray *c1_y = NULL;
  static char_T c1_cv17[34] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'i', 'n', 't', '_', 'f', 'o', 'r', 'l', 'o', 'o', 'p',
    '_', 'o', 'v', 'e', 'r', 'f', 'l', 'o', 'w' };

  const mxArray *c1_b_y = NULL;
  const mxArray *c1_c_y = NULL;
  static char_T c1_cv18[5] = { 'i', 'n', 't', '3', '2' };

  (void)chartInstance;
  (void)c1_overflow;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv17, 10, 0U, 1U, 0U, 2, 1, 34),
                false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv17, 10, 0U, 1U, 0U, 2, 1, 34),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv18, 10, 0U, 1U, 0U, 2, 1, 5),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
    1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U, 2U,
    14, c1_b_y, 14, c1_c_y)));
}

static void c1_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_real_T *c1_x, c1_emxArray_real_T *c1_b_x)
{
  int32_T c1_i243;
  int32_T c1_i244;
  int32_T c1_i245;
  int32_T c1_loop_ub;
  int32_T c1_i246;
  c1_i243 = c1_b_x->size[0] * c1_b_x->size[1];
  c1_b_x->size[0] = c1_x->size[0];
  c1_b_x->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_x, c1_i243, &c1_ac_emlrtRTEI);
  c1_i244 = c1_b_x->size[0];
  c1_i245 = c1_b_x->size[1];
  c1_loop_ub = c1_x->size[0] * c1_x->size[1] - 1;
  for (c1_i246 = 0; c1_i246 <= c1_loop_ub; c1_i246++) {
    c1_b_x->data[c1_i246] = c1_x->data[c1_i246];
  }

  c1_c_round(chartInstance, c1_b_x);
}

static boolean_T c1_sortLE(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_v, int32_T c1_dir_data[], int32_T c1_dir_size[2],
  int32_T c1_idx1, int32_T c1_idx2)
{
  boolean_T c1_p;
  int32_T c1_irow1;
  int32_T c1_irow2;
  int32_T c1_k;
  int32_T c1_b_k;
  int32_T c1_colk;
  int32_T c1_x;
  int32_T c1_b_x;
  int32_T c1_c_x;
  int32_T c1_abscolk;
  real_T c1_v1;
  real_T c1_v2;
  boolean_T c1_v1eqv2;
  real_T c1_d_x;
  boolean_T c1_b;
  boolean_T c1_b4;
  real_T c1_e_x;
  boolean_T c1_b_b;
  real_T c1_a;
  real_T c1_c_b;
  real_T c1_b_a;
  real_T c1_d_b;
  real_T c1_c_a;
  real_T c1_e_b;
  boolean_T c1_b_p;
  real_T c1_f_x;
  boolean_T c1_f_b;
  boolean_T c1_c_p;
  boolean_T exitg1;
  (void)chartInstance;
  (void)c1_dir_size;
  c1_irow1 = c1_idx1 - 1;
  c1_irow2 = c1_idx2 - 1;
  c1_p = true;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k < 2)) {
    c1_b_k = c1_k;
    c1_colk = c1_dir_data[c1_b_k];
    c1_x = c1_colk;
    c1_b_x = c1_x;
    c1_c_x = c1_b_x;
    c1_abscolk = c1_c_x - 1;
    c1_v1 = c1_v->data[c1_irow1 + c1_v->size[0] * c1_abscolk];
    c1_v2 = c1_v->data[c1_irow2 + c1_v->size[0] * c1_abscolk];
    c1_v1eqv2 = (c1_v1 == c1_v2);
    if (c1_v1eqv2) {
      c1_b4 = true;
    } else {
      c1_d_x = c1_v1;
      c1_b = muDoubleScalarIsNaN(c1_d_x);
      if (c1_b) {
        c1_e_x = c1_v2;
        c1_b_b = muDoubleScalarIsNaN(c1_e_x);
        if (c1_b_b) {
          c1_b4 = true;
        } else {
          c1_b4 = false;
        }
      } else {
        c1_b4 = false;
      }
    }

    if (!c1_b4) {
      c1_a = c1_v1;
      c1_c_b = c1_v2;
      c1_b_a = c1_a;
      c1_d_b = c1_c_b;
      c1_c_a = c1_b_a;
      c1_e_b = c1_d_b;
      c1_b_p = (c1_c_a <= c1_e_b);
      if (c1_b_p) {
        c1_c_p = true;
      } else {
        c1_f_x = c1_d_b;
        c1_f_b = muDoubleScalarIsNaN(c1_f_x);
        if (c1_f_b) {
          c1_c_p = true;
        } else {
          c1_c_p = false;
        }
      }

      c1_p = c1_c_p;
      exitg1 = true;
    } else {
      c1_k++;
    }
  }

  return c1_p;
}

static void c1_apply_row_permutation(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_y, c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T
  *c1_b_y)
{
  int32_T c1_i247;
  int32_T c1_i248;
  int32_T c1_i249;
  int32_T c1_loop_ub;
  int32_T c1_i250;
  c1_emxArray_int32_T *c1_b_idx;
  int32_T c1_i251;
  int32_T c1_b_loop_ub;
  int32_T c1_i252;
  c1_i247 = c1_b_y->size[0] * c1_b_y->size[1];
  c1_b_y->size[0] = c1_y->size[0];
  c1_b_y->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_y, c1_i247, &c1_bc_emlrtRTEI);
  c1_i248 = c1_b_y->size[0];
  c1_i249 = c1_b_y->size[1];
  c1_loop_ub = c1_y->size[0] * c1_y->size[1] - 1;
  for (c1_i250 = 0; c1_i250 <= c1_loop_ub; c1_i250++) {
    c1_b_y->data[c1_i250] = c1_y->data[c1_i250];
  }

  c1_emxInit_int32_T(chartInstance, &c1_b_idx, 1, &c1_bc_emlrtRTEI);
  c1_i251 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i251,
    &c1_bc_emlrtRTEI);
  c1_b_loop_ub = c1_idx->size[0] - 1;
  for (c1_i252 = 0; c1_i252 <= c1_b_loop_ub; c1_i252++) {
    c1_b_idx->data[c1_i252] = c1_idx->data[c1_i252];
  }

  c1_b_apply_row_permutation(chartInstance, c1_b_y, c1_b_idx);
  c1_emxFree_int32_T(chartInstance, &c1_b_idx);
}

static boolean_T c1_rows_differ(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_b, int32_T c1_k0, int32_T c1_k)
{
  boolean_T c1_p;
  int32_T c1_j;
  real_T c1_b_j;
  real_T c1_a;
  real_T c1_b_b;
  real_T c1_x;
  real_T c1_b_x;
  real_T c1_c_x;
  real_T c1_y;
  real_T c1_d_x;
  real_T c1_e_x;
  real_T c1_f_x;
  real_T c1_g_x;
  real_T c1_absxk;
  real_T c1_h_x;
  real_T c1_i_x;
  boolean_T c1_c_b;
  boolean_T c1_b5;
  real_T c1_j_x;
  boolean_T c1_d_b;
  boolean_T c1_b6;
  boolean_T c1_e_b;
  real_T c1_r;
  int32_T c1_exponent;
  int32_T c1_b_exponent;
  real_T c1_k_x;
  boolean_T c1_f_b;
  boolean_T c1_b_p;
  real_T c1_l_x;
  boolean_T c1_c_p;
  boolean_T c1_g_b;
  boolean_T exitg1;
  (void)chartInstance;
  c1_p = false;
  c1_j = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_j < 2)) {
    c1_b_j = 1.0 + (real_T)c1_j;
    c1_a = c1_b->data[(c1_k0 + c1_b->size[0] * ((int32_T)c1_b_j - 1)) - 1];
    c1_b_b = c1_b->data[(c1_k + c1_b->size[0] * ((int32_T)c1_b_j - 1)) - 1];
    c1_x = c1_b_b - c1_a;
    c1_b_x = c1_x;
    c1_c_x = c1_b_x;
    c1_y = muDoubleScalarAbs(c1_c_x);
    c1_d_x = c1_b_b / 2.0;
    c1_e_x = c1_d_x;
    c1_f_x = c1_e_x;
    c1_g_x = c1_f_x;
    c1_absxk = muDoubleScalarAbs(c1_g_x);
    c1_h_x = c1_absxk;
    c1_i_x = c1_h_x;
    c1_c_b = muDoubleScalarIsInf(c1_i_x);
    c1_b5 = !c1_c_b;
    c1_j_x = c1_h_x;
    c1_d_b = muDoubleScalarIsNaN(c1_j_x);
    c1_b6 = !c1_d_b;
    c1_e_b = (c1_b5 && c1_b6);
    if (c1_e_b) {
      if (c1_absxk <= 2.2250738585072014E-308) {
        c1_r = 4.94065645841247E-324;
      } else {
        frexp(c1_absxk, &c1_exponent);
        c1_b_exponent = c1_exponent;
        c1_r = ldexp(1.0, c1_b_exponent - 53);
      }
    } else {
      c1_r = rtNaN;
    }

    if (c1_y < c1_r) {
      c1_b_p = true;
    } else {
      c1_k_x = c1_a;
      c1_f_b = muDoubleScalarIsInf(c1_k_x);
      if (c1_f_b) {
        c1_l_x = c1_b_b;
        c1_g_b = muDoubleScalarIsInf(c1_l_x);
        if (c1_g_b && ((c1_a > 0.0) == (c1_b_b > 0.0))) {
          c1_b_p = true;
        } else {
          c1_b_p = false;
        }
      } else {
        c1_b_p = false;
      }
    }

    c1_c_p = c1_b_p;
    if (!c1_c_p) {
      c1_p = true;
      exitg1 = true;
    } else {
      c1_j++;
    }
  }

  return c1_p;
}

static void c1_b_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_real_T *c1_b_x)
{
  int32_T c1_i253;
  int32_T c1_loop_ub;
  int32_T c1_i254;
  c1_i253 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i253, &c1_ac_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i254 = 0; c1_i254 <= c1_loop_ub; c1_i254++) {
    c1_b_x->data[c1_i254] = c1_x->data[c1_i254];
  }

  c1_d_round(chartInstance, c1_b_x);
}

static void c1_sparse(SFc1_LIDAR_simInstanceStruct *chartInstance,
                      c1_emxArray_real_T *c1_varargin_1, c1_emxArray_real_T
                      *c1_varargin_2, c1_coder_internal_sparse *c1_y)
{
  int32_T c1_nc;
  int32_T c1_nr;
  boolean_T c1_b7;
  static char_T c1_cv19[14] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'a', 'm',
    'e', 'l', 'e', 'n' };

  c1_emxArray_real_T *c1_b_varargin_1;
  int32_T c1_i255;
  int32_T c1_i256;
  int32_T c1_i257;
  int32_T c1_loop_ub;
  int32_T c1_i258;
  c1_emxArray_int32_T *c1_ridxInt;
  c1_emxArray_real_T *c1_b_varargin_2;
  int32_T c1_i259;
  int32_T c1_i260;
  int32_T c1_i261;
  int32_T c1_b_loop_ub;
  int32_T c1_i262;
  c1_emxArray_int32_T *c1_cidxInt;
  c1_emxArray_int32_T *c1_sortedIndices;
  int32_T c1_i263;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  int32_T c1_istop;
  int32_T c1_thism;
  int32_T c1_thisn;
  int32_T c1_c_b;
  int32_T c1_d_b;
  boolean_T c1_b_overflow;
  int32_T c1_a;
  int32_T c1_numalloc;
  int32_T c1_c_k;
  int32_T c1_i264;
  int32_T c1_c_loop_ub;
  int32_T c1_i265;
  int32_T c1_iv2[1];
  int32_T c1_i266;
  int32_T c1_i267;
  int32_T c1_d_loop_ub;
  int32_T c1_i268;
  int32_T c1_cptr;
  int32_T c1_i269;
  int32_T c1_e_b;
  int32_T c1_f_b;
  boolean_T c1_c_overflow;
  int32_T c1_c;
  int32_T c1_b_c;
  int32_T c1_g_b;
  int32_T c1_h_b;
  boolean_T c1_d_overflow;
  int32_T c1_d_k;
  c1_nc = c1_varargin_2->size[1];
  c1_nr = c1_varargin_1->size[1];
  if (c1_nr == c1_nc) {
    c1_b7 = true;
  } else {
    c1_b7 = false;
  }

  if (c1_b7) {
  } else {
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                      c1_b_emlrt_marshallOut(chartInstance, c1_cv19), 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c1_b_emlrt_marshallOut(chartInstance, c1_cv19))));
  }

  c1_emxInit_real_T(chartInstance, &c1_b_varargin_1, 2, &c1_cc_emlrtRTEI);
  c1_i255 = c1_b_varargin_1->size[0] * c1_b_varargin_1->size[1];
  c1_b_varargin_1->size[0] = 1;
  c1_b_varargin_1->size[1] = c1_varargin_1->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_varargin_1, c1_i255,
    &c1_cc_emlrtRTEI);
  c1_i256 = c1_b_varargin_1->size[0];
  c1_i257 = c1_b_varargin_1->size[1];
  c1_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i258 = 0; c1_i258 <= c1_loop_ub; c1_i258++) {
    c1_b_varargin_1->data[c1_i258] = c1_varargin_1->data[c1_i258];
  }

  c1_emxInit_int32_T(chartInstance, &c1_ridxInt, 1, &c1_gc_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_b_varargin_2, 2, &c1_dc_emlrtRTEI);
  c1_assertValidIndexArg(chartInstance, c1_b_varargin_1, c1_ridxInt);
  c1_i259 = c1_b_varargin_2->size[0] * c1_b_varargin_2->size[1];
  c1_b_varargin_2->size[0] = 1;
  c1_b_varargin_2->size[1] = c1_varargin_2->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_varargin_2, c1_i259,
    &c1_dc_emlrtRTEI);
  c1_i260 = c1_b_varargin_2->size[0];
  c1_i261 = c1_b_varargin_2->size[1];
  c1_b_loop_ub = c1_varargin_2->size[0] * c1_varargin_2->size[1] - 1;
  c1_emxFree_real_T(chartInstance, &c1_b_varargin_1);
  for (c1_i262 = 0; c1_i262 <= c1_b_loop_ub; c1_i262++) {
    c1_b_varargin_2->data[c1_i262] = c1_varargin_2->data[c1_i262];
  }

  c1_emxInit_int32_T(chartInstance, &c1_cidxInt, 1, &c1_hc_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_sortedIndices, 1, &c1_ic_emlrtRTEI);
  c1_assertValidIndexArg(chartInstance, c1_b_varargin_2, c1_cidxInt);
  c1_i263 = c1_sortedIndices->size[0];
  c1_sortedIndices->size[0] = c1_nc;
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_sortedIndices, c1_i263,
    &c1_ec_emlrtRTEI);
  c1_b = c1_nc;
  c1_b_b = c1_b;
  c1_emxFree_real_T(chartInstance, &c1_b_varargin_2);
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_nc; c1_k++) {
    c1_b_k = c1_k;
    c1_sortedIndices->data[c1_b_k] = c1_b_k + 1;
  }

  c1_b_locSortrows(chartInstance, c1_sortedIndices, c1_cidxInt, c1_ridxInt);
  c1_emxFree_int32_T(chartInstance, &c1_sortedIndices);
  if ((c1_ridxInt->size[0] == 0) || (c1_cidxInt->size[0] == 0)) {
    c1_thism = 0;
    c1_thisn = 0;
  } else {
    c1_istop = c1_ridxInt->size[0];
    c1_thism = c1_ridxInt->data[0];
    c1_c_b = c1_istop;
    c1_d_b = c1_c_b;
    if (2 > c1_d_b) {
      c1_b_overflow = false;
    } else {
      c1_b_overflow = (c1_d_b > 2147483646);
    }

    if (c1_b_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_c_k = 1; c1_c_k < c1_istop; c1_c_k++) {
      if (c1_thism < c1_ridxInt->data[c1_c_k]) {
        c1_thism = c1_ridxInt->data[c1_c_k];
      }
    }

    c1_thisn = c1_cidxInt->data[c1_cidxInt->size[0] - 1];
  }

  c1_y->m = c1_thism;
  c1_y->n = c1_thisn;
  c1_a = c1_nc;
  if (c1_a >= 1) {
    c1_numalloc = c1_a;
  } else {
    c1_numalloc = 1;
  }

  c1_i264 = c1_y->d->size[0];
  c1_y->d->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_numalloc);
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_y->d, c1_i264,
    &c1_fc_emlrtRTEI);
  c1_c_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_numalloc) - 1;
  for (c1_i265 = 0; c1_i265 <= c1_c_loop_ub; c1_i265++) {
    c1_y->d->data[c1_i265] = false;
  }

  c1_y->maxnz = c1_numalloc;
  c1_iv2[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)(c1_y->n + 1));
  c1_i266 = c1_y->colidx->size[0];
  c1_y->colidx->size[0] = c1_iv2[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_y->colidx, c1_i266,
    &c1_ec_emlrtRTEI);
  c1_y->colidx->data[0] = 1;
  c1_i267 = c1_y->rowidx->size[0];
  c1_y->rowidx->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)
    c1_numalloc);
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_y->rowidx, c1_i267,
    &c1_fc_emlrtRTEI);
  c1_d_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_numalloc) - 1;
  for (c1_i268 = 0; c1_i268 <= c1_d_loop_ub; c1_i268++) {
    c1_y->rowidx->data[c1_i268] = 0;
  }

  c1_cptr = 0;
  c1_i269 = c1_y->n;
  c1_e_b = c1_i269;
  c1_f_b = c1_e_b;
  if (1 > c1_f_b) {
    c1_c_overflow = false;
  } else {
    c1_c_overflow = (c1_f_b > 2147483646);
  }

  if (c1_c_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_c = 1; c1_c - 1 < c1_i269; c1_c++) {
    c1_b_c = c1_c;
    while ((c1_cptr + 1 <= c1_nc) && (c1_cidxInt->data[c1_cptr] == c1_b_c)) {
      c1_y->rowidx->data[c1_cptr] = c1_ridxInt->data[c1_cptr];
      c1_cptr++;
    }

    c1_y->colidx->data[c1_b_c] = c1_cptr + 1;
  }

  c1_emxFree_int32_T(chartInstance, &c1_cidxInt);
  c1_emxFree_int32_T(chartInstance, &c1_ridxInt);
  c1_g_b = c1_nc;
  c1_h_b = c1_g_b;
  if (1 > c1_h_b) {
    c1_d_overflow = false;
  } else {
    c1_d_overflow = (c1_h_b > 2147483646);
  }

  if (c1_d_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_d_k = 0; c1_d_k < c1_nc; c1_d_k++) {
    c1_b_k = c1_d_k;
    c1_y->d->data[c1_b_k] = true;
  }

  c1_b_sparse_fillIn(chartInstance, c1_y);
}

static void c1_assertValidIndexArg(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_s, c1_emxArray_int32_T *c1_sint)
{
  int32_T c1_ns;
  int32_T c1_i270;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  real_T c1_sk;
  real_T c1_x;
  real_T c1_b_x;
  const mxArray *c1_y = NULL;
  static char_T c1_cv20[31] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 'n', 'o', 'n', 'I', 'n', 't', 'e', 'g', 'e',
    'r', 'I', 'n', 'd', 'e', 'x' };

  const mxArray *c1_b_y = NULL;
  const mxArray *c1_c_y = NULL;
  static char_T c1_cv21[26] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 'l', 'a', 'r', 'g', 'e', 'I', 'n', 'd', 'e',
    'x' };

  const mxArray *c1_d_y = NULL;
  const mxArray *c1_e_y = NULL;
  static char_T c1_cv22[27] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 'n', 'o', 'n', 'P', 'o', 's', 'I', 'n', 'd',
    'e', 'x' };

  const mxArray *c1_f_y = NULL;
  c1_ns = c1_s->size[1];
  c1_i270 = c1_sint->size[0];
  c1_sint->size[0] = c1_ns;
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_sint, c1_i270, &c1_jc_emlrtRTEI);
  c1_b = c1_ns;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_ns; c1_k++) {
    c1_b_k = c1_k;
    c1_sk = c1_s->data[c1_b_k];
    c1_x = c1_sk;
    c1_b_x = c1_x;
    c1_b_x = muDoubleScalarFloor(c1_b_x);
    if (c1_b_x == c1_sk) {
    } else {
      c1_y = NULL;
      sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv20, 10, 0U, 1U, 0U, 2, 1, 31),
                    false);
      c1_b_y = NULL;
      sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv20, 10, 0U, 1U, 0U, 2, 1,
        31), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_b_y)));
    }

    if (c1_sk < 2.147483647E+9) {
    } else {
      c1_c_y = NULL;
      sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv21, 10, 0U, 1U, 0U, 2, 1,
        26), false);
      c1_d_y = NULL;
      sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv21, 10, 0U, 1U, 0U, 2, 1,
        26), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_d_y)));
    }

    if (0.0 < c1_sk) {
    } else {
      c1_e_y = NULL;
      sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv22, 10, 0U, 1U, 0U, 2, 1,
        27), false);
      c1_f_y = NULL;
      sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv22, 10, 0U, 1U, 0U, 2, 1,
        27), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_e_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_f_y)));
    }

    c1_sint->data[c1_b_k] = (int32_T)c1_sk;
  }
}

static void c1_locSortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_a, c1_emxArray_int32_T
  *c1_b, c1_emxArray_int32_T *c1_b_idx, c1_emxArray_int32_T *c1_b_a,
  c1_emxArray_int32_T *c1_b_b)
{
  int32_T c1_i271;
  int32_T c1_loop_ub;
  int32_T c1_i272;
  int32_T c1_i273;
  int32_T c1_b_loop_ub;
  int32_T c1_i274;
  int32_T c1_i275;
  int32_T c1_c_loop_ub;
  int32_T c1_i276;
  c1_i271 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i271,
    &c1_kc_emlrtRTEI);
  c1_loop_ub = c1_idx->size[0] - 1;
  for (c1_i272 = 0; c1_i272 <= c1_loop_ub; c1_i272++) {
    c1_b_idx->data[c1_i272] = c1_idx->data[c1_i272];
  }

  c1_i273 = c1_b_a->size[0];
  c1_b_a->size[0] = c1_a->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_a, c1_i273, &c1_kc_emlrtRTEI);
  c1_b_loop_ub = c1_a->size[0] - 1;
  for (c1_i274 = 0; c1_i274 <= c1_b_loop_ub; c1_i274++) {
    c1_b_a->data[c1_i274] = c1_a->data[c1_i274];
  }

  c1_i275 = c1_b_b->size[0];
  c1_b_b->size[0] = c1_b->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_b, c1_i275, &c1_kc_emlrtRTEI);
  c1_c_loop_ub = c1_b->size[0] - 1;
  for (c1_i276 = 0; c1_i276 <= c1_c_loop_ub; c1_i276++) {
    c1_b_b->data[c1_i276] = c1_b->data[c1_i276];
  }

  c1_b_locSortrows(chartInstance, c1_b_idx, c1_b_a, c1_b_b);
}

static void c1_insertionsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  int32_T c1_i277;
  int32_T c1_loop_ub;
  int32_T c1_i278;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_lc_emlrtRTEI);
  c1_i277 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_x, c1_i277, &c1_lc_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i278 = 0; c1_i278 <= c1_loop_ub; c1_i278++) {
    c1_b_x->data[c1_i278] = c1_x->data[c1_i278];
  }

  c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
    &c1_lc_emlrtRTEI);
  c1_b_insertionsort(chartInstance, c1_b_x, c1_xstart, c1_xend, c1_b_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static boolean_T c1___anon_fcn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_a, c1_emxArray_int32_T *c1_b, int32_T c1_i, int32_T
  c1_j)
{
  boolean_T c1_varargout_1;
  int32_T c1_b_i;
  int32_T c1_b_j;
  int32_T c1_ai;
  int32_T c1_aj;
  (void)chartInstance;
  c1_b_i = c1_i - 1;
  c1_b_j = c1_j - 1;
  c1_ai = c1_a->data[c1_b_i];
  c1_aj = c1_a->data[c1_b_j];
  if (c1_ai < c1_aj) {
    c1_varargout_1 = true;
  } else if (c1_ai == c1_aj) {
    c1_varargout_1 = (c1_b->data[c1_b_i] < c1_b->data[c1_b_j]);
  } else {
    c1_varargout_1 = false;
  }

  return c1_varargout_1;
}

static int32_T c1_nextpow2(SFc1_LIDAR_simInstanceStruct *chartInstance, int32_T
  c1_n)
{
  int32_T c1_p;
  int32_T c1_x;
  int32_T c1_b_n;
  int32_T c1_c_n;
  int32_T c1_pmax;
  int32_T c1_pmin;
  int32_T c1_pow2p;
  boolean_T exitg1;
  (void)chartInstance;
  c1_x = c1_n;
  c1_p = c1_x;
  c1_b_n = c1_p;
  c1_c_n = c1_b_n;
  c1_pmax = 31;
  c1_pmin = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_pmax - c1_pmin > 1)) {
    c1_p = (c1_pmin + c1_pmax) >> 1;
    c1_pow2p = 1 << c1_p;
    if (c1_pow2p == c1_c_n) {
      c1_pmax = c1_p;
      exitg1 = true;
    } else if (c1_pow2p > c1_c_n) {
      c1_pmax = c1_p;
    } else {
      c1_pmin = c1_p;
    }
  }

  return c1_pmax;
}

static void c1_introsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  int32_T c1_i279;
  int32_T c1_loop_ub;
  int32_T c1_i280;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_mc_emlrtRTEI);
  c1_i279 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_x, c1_i279, &c1_mc_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i280 = 0; c1_i280 <= c1_loop_ub; c1_i280++) {
    c1_b_x->data[c1_i280] = c1_x->data[c1_i280];
  }

  c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
    &c1_mc_emlrtRTEI);
  c1_b_introsort(chartInstance, c1_b_x, c1_xend, c1_b_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static void c1_stack_stack(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_sBaHy6MF1FZJsDHxMqvBaiH c1_eg, int32_T c1_n, c1_coder_internal_stack
  *c1_this)
{
  int32_T c1_iv3[2];
  int32_T c1_loop_ub;
  int32_T c1_i281;
  (void)chartInstance;
  c1_this->n = 0;
  c1_iv3[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_n);
  c1_iv3[1] = 1;
  c1_this->d.size[0] = c1_iv3[0];
  c1_loop_ub = c1_iv3[0] - 1;
  for (c1_i281 = 0; c1_i281 <= c1_loop_ub; c1_i281++) {
    c1_this->d.data[c1_i281] = c1_eg;
  }
}

static void c1_heapsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  int32_T c1_i282;
  int32_T c1_loop_ub;
  int32_T c1_i283;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_oc_emlrtRTEI);
  c1_i282 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_x, c1_i282, &c1_oc_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i283 = 0; c1_i283 <= c1_loop_ub; c1_i283++) {
    c1_b_x->data[c1_i283] = c1_x->data[c1_i283];
  }

  c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
    &c1_oc_emlrtRTEI);
  c1_b_heapsort(chartInstance, c1_b_x, c1_xstart, c1_xend, c1_b_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static void c1_heapify(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_idx, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, c1_emxArray_int32_T *c1_b_x)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  int32_T c1_i284;
  int32_T c1_loop_ub;
  int32_T c1_i285;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_pc_emlrtRTEI);
  c1_i284 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_x, c1_i284, &c1_pc_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i285 = 0; c1_i285 <= c1_loop_ub; c1_i285++) {
    c1_b_x->data[c1_i285] = c1_x->data[c1_i285];
  }

  c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
    &c1_pc_emlrtRTEI);
  c1_b_heapify(chartInstance, c1_b_x, c1_idx, c1_xstart, c1_xend, c1_b_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static void c1_sortpartition(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp, int32_T *c1_p,
  c1_emxArray_int32_T *c1_b_x)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  int32_T c1_i286;
  int32_T c1_loop_ub;
  int32_T c1_i287;
  int32_T c1_b_p;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_qc_emlrtRTEI);
  c1_i286 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_x, c1_i286, &c1_qc_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i287 = 0; c1_i287 <= c1_loop_ub; c1_i287++) {
    c1_b_x->data[c1_i287] = c1_x->data[c1_i287];
  }

  c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
    &c1_qc_emlrtRTEI);
  c1_b_p = c1_b_sortpartition(chartInstance, c1_b_x, c1_xstart, c1_xend,
    c1_b_cmp);
  *c1_p = c1_b_p;
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static void c1_permuteVector(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_y, c1_emxArray_int32_T
  *c1_b_y)
{
  int32_T c1_i288;
  int32_T c1_loop_ub;
  int32_T c1_i289;
  c1_emxArray_int32_T *c1_b_idx;
  int32_T c1_i290;
  int32_T c1_b_loop_ub;
  int32_T c1_i291;
  c1_i288 = c1_b_y->size[0];
  c1_b_y->size[0] = c1_y->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_y, c1_i288, &c1_rc_emlrtRTEI);
  c1_loop_ub = c1_y->size[0] - 1;
  for (c1_i289 = 0; c1_i289 <= c1_loop_ub; c1_i289++) {
    c1_b_y->data[c1_i289] = c1_y->data[c1_i289];
  }

  c1_emxInit_int32_T(chartInstance, &c1_b_idx, 1, &c1_rc_emlrtRTEI);
  c1_i290 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i290,
    &c1_rc_emlrtRTEI);
  c1_b_loop_ub = c1_idx->size[0] - 1;
  for (c1_i291 = 0; c1_i291 <= c1_b_loop_ub; c1_i291++) {
    c1_b_idx->data[c1_i291] = c1_idx->data[c1_i291];
  }

  c1_b_permuteVector(chartInstance, c1_b_idx, c1_b_y);
  c1_emxFree_int32_T(chartInstance, &c1_b_idx);
}

static void c1_sparse_fillIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_coder_internal_sparse c1_this, c1_coder_internal_sparse *c1_b_this)
{
  c1_emxCopyStruct_coder_internal_sp(chartInstance, c1_b_this, &c1_this,
    &c1_sc_emlrtRTEI);
  c1_b_sparse_fillIn(chartInstance, c1_b_this);
}

static void c1_edge(SFc1_LIDAR_simInstanceStruct *chartInstance,
                    c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_boolean_T *
                    c1_varargout_1)
{
  c1_emxArray_real32_T *c1_a;
  int32_T c1_i292;
  int32_T c1_i293;
  int32_T c1_i294;
  int32_T c1_loop_ub;
  int32_T c1_i295;
  boolean_T c1_b8;
  boolean_T c1_b9;
  real_T c1_n;
  int32_T c1_i296;
  real_T c1_m;
  int32_T c1_i297;
  int32_T c1_i298;
  real_T c1_connDimsT[2];
  int32_T c1_i299;
  real_T c1_derivGaussKernel[13];
  static real_T c1_dv5[13] = { 0.00050975920363612861, 0.0025659736304223,
    0.014594040963812248, 0.049305200293708981, 0.09498179875488523,
    0.089159205155936727, 0.0, -0.089159205155936727, -0.09498179875488523,
    -0.049305200293708981, -0.014594040963812248, -0.0025659736304223,
    -0.00050975920363612861 };

  int32_T c1_i300;
  int32_T c1_i301;
  static real_T c1_dv6[6] = { 0.0020299751839417141, 0.010218281014351701,
    0.058116735860084041, 0.19634433732941295, 0.37823877042972093,
    0.35505190018248872 };

  int32_T c1_i302;
  real_T c1_y;
  real_T c1_x[6];
  int32_T c1_i303;
  int32_T c1_k;
  int32_T c1_b_loop_ub;
  int32_T c1_i304;
  real_T c1_b_x;
  int32_T c1_xoffset;
  real_T c1_c_x;
  int32_T c1_ix;
  real_T c1_d_x;
  real_T c1_b_y;
  int32_T c1_i305;
  real_T c1_outSizeT[2];
  real_T c1_startT[2];
  boolean_T c1_b10;
  boolean_T c1_b11;
  c1_emxArray_real32_T *c1_b_a;
  c1_emxArray_real32_T *c1_i1;
  c1_emxArray_real32_T *c1_c_a;
  int32_T c1_i306;
  int32_T c1_i307;
  int32_T c1_i308;
  int32_T c1_i309;
  int32_T c1_i310;
  int32_T c1_i311;
  int32_T c1_c_loop_ub;
  int32_T c1_d_loop_ub;
  int32_T c1_i312;
  int32_T c1_i313;
  int32_T c1_i314;
  real_T c1_b_startT[2];
  boolean_T c1_tooBig;
  boolean_T c1_b12;
  boolean_T c1_b13;
  int32_T c1_i;
  int32_T c1_i315;
  real_T c1_b_i;
  boolean_T c1_modeFlag;
  real_T c1_c_startT[2];
  int32_T c1_trueCount;
  boolean_T c1_b_tooBig;
  int32_T c1_c_i;
  boolean_T c1_b14;
  boolean_T c1_b_modeFlag;
  boolean_T c1_b15;
  boolean_T c1_c_modeFlag;
  int32_T c1_tmp_size[1];
  int32_T c1_i316;
  int32_T c1_i317;
  int32_T c1_partialTrueCount;
  int32_T c1_i318;
  int32_T c1_d_i;
  real_T c1_d_startT[2];
  boolean_T c1_c_tooBig;
  int32_T c1_nonzero_h_size[1];
  int32_T c1_e_i;
  int32_T c1_i319;
  int32_T c1_i320;
  int32_T c1_e_loop_ub;
  int32_T c1_tmp_data[13];
  int32_T c1_i321;
  real_T c1_f_i;
  boolean_T c1_b16;
  int32_T c1_i322;
  real_T c1_padSizeT[2];
  int32_T c1_i323;
  boolean_T c1_d_modeFlag;
  boolean_T c1_b17;
  int32_T c1_i324;
  real_T c1_nonzero_h_data[13];
  boolean_T c1_d_tooBig;
  int32_T c1_i325;
  int32_T c1_i326;
  int32_T c1_i327;
  boolean_T c1_e_modeFlag;
  c1_emxArray_real32_T *c1_b_i1;
  boolean_T c1_densityFlag;
  boolean_T c1_conn[13];
  int32_T c1_i328;
  boolean_T c1_f_modeFlag;
  real_T c1_e_startT[2];
  int32_T c1_i329;
  static real_T c1_kernel[13] = { 3.4813359214923066E-5, 0.00054457256285105169,
    0.0051667606200595231, 0.029732654490475546, 0.10377716120747749,
    0.219696252000246, 0.28209557151935094, 0.219696252000246,
    0.10377716120747749, 0.029732654490475546, 0.0051667606200595231,
    0.00054457256285105169, 3.4813359214923066E-5 };

  int32_T c1_i330;
  int32_T c1_i331;
  boolean_T c1_e_tooBig;
  int32_T c1_g_i;
  int32_T c1_i332;
  int32_T c1_b_trueCount;
  int32_T c1_i333;
  int32_T c1_h_i;
  int32_T c1_f_loop_ub;
  real_T c1_i_i;
  int32_T c1_i334;
  int32_T c1_i335;
  int32_T c1_i336;
  boolean_T c1_g_modeFlag;
  int32_T c1_b_tmp_size[1];
  boolean_T c1_f_tooBig;
  int32_T c1_i337;
  int32_T c1_i338;
  int32_T c1_b_partialTrueCount;
  c1_emxArray_real32_T *c1_d_a;
  int32_T c1_j_i;
  int32_T c1_i339;
  boolean_T c1_h_modeFlag;
  int32_T c1_i340;
  int32_T c1_i341;
  int32_T c1_i342;
  int32_T c1_i343;
  int32_T c1_b_tmp_data[13];
  int32_T c1_i344;
  int32_T c1_i345;
  static real_T c1_b_kernel[13] = { 3.4813359214923066E-5,
    0.00054457256285105169, 0.0051667606200595231, 0.029732654490475546,
    0.10377716120747749, 0.219696252000246, 0.28209557151935094,
    0.219696252000246, 0.10377716120747749, 0.029732654490475546,
    0.0051667606200595231, 0.00054457256285105169, 3.4813359214923066E-5 };

  int32_T c1_i346;
  boolean_T c1_i_modeFlag;
  int32_T c1_g_loop_ub;
  int32_T c1_i347;
  int32_T c1_h_loop_ub;
  int32_T c1_i348;
  int32_T c1_i349;
  c1_emxArray_real32_T *c1_b_varargin_1;
  int32_T c1_i350;
  int32_T c1_i351;
  int32_T c1_i352;
  int32_T c1_i353;
  boolean_T c1_b_densityFlag;
  int32_T c1_i354;
  int32_T c1_i355;
  int32_T c1_i_loop_ub;
  boolean_T c1_g_tooBig;
  real_T c1_numKernElem;
  int32_T c1_i356;
  int32_T c1_k_i;
  int32_T c1_i357;
  int32_T c1_i358;
  real_T c1_l_i;
  int32_T c1_i359;
  const mxArray *c1_c_y = NULL;
  boolean_T c1_j_modeFlag;
  int32_T c1_b_n;
  boolean_T c1_h_tooBig;
  const mxArray *c1_d_y = NULL;
  int32_T c1_c_n;
  boolean_T c1_k_modeFlag;
  real32_T c1_e_x;
  real32_T c1_magmax;
  real32_T c1_f_x;
  real32_T c1_g_x;
  int32_T c1_i360;
  boolean_T c1_b;
  boolean_T c1_b_b;
  boolean_T c1_p;
  real32_T c1_h_x;
  boolean_T c1_l_modeFlag;
  int32_T c1_idx;
  boolean_T c1_c_b;
  real32_T c1_e_y;
  int32_T c1_i361;
  int32_T c1_d_b;
  real_T c1_d_n;
  real32_T c1_f_y;
  int32_T c1_e_b;
  real_T c1_b_m;
  int32_T c1_i362;
  int32_T c1_first;
  boolean_T c1_b18;
  int32_T c1_i363;
  boolean_T c1_overflow;
  int32_T c1_last;
  boolean_T c1_b19;
  real32_T c1_ex;
  int32_T c1_i364;
  int32_T c1_i365;
  int32_T c1_i366;
  real_T c1_numCores;
  int32_T c1_i367;
  int32_T c1_e_a;
  int32_T c1_i368;
  int32_T c1_b_k;
  int32_T c1_f_b;
  int32_T c1_i369;
  int32_T c1_i370;
  int32_T c1_i371;
  int32_T c1_f_a;
  real_T c1_counts[64];
  int32_T c1_i372;
  int32_T c1_g_b;
  boolean_T c1_useParallel;
  int32_T c1_c_k;
  int32_T c1_j_loop_ub;
  real32_T c1_i_x;
  int32_T c1_i373;
  real_T c1_b_numKernElem;
  int32_T c1_i374;
  real32_T c1_j_x;
  boolean_T c1_b_overflow;
  real_T c1_rowsA;
  int32_T c1_i375;
  boolean_T c1_h_b;
  real_T c1_colsAEtc;
  real_T c1_d11;
  int32_T c1_d_k;
  boolean_T c1_b_p;
  int32_T c1_i376;
  int32_T c1_i377;
  int32_T c1_i378;
  real_T c1_numelA;
  int32_T c1_e_k;
  real_T c1_numRows;
  real_T c1_numCols;
  int32_T c1_b_idx;
  boolean_T c1_k_x[64];
  boolean_T c1_nanFlag;
  boolean_T c1_b_nanFlag;
  int32_T c1_ii_size[1];
  real_T c1_d12;
  boolean_T c1_rngFlag;
  int32_T c1_ii;
  int32_T c1_i379;
  int32_T c1_m_i;
  boolean_T c1_b_rngFlag;
  boolean_T c1_c_nanFlag;
  int32_T c1_b_ii;
  real_T c1_n_i;
  int32_T c1_i380;
  real32_T c1_l_x;
  int32_T c1_highThreshTemp_size[1];
  int32_T c1_ii_data[1];
  boolean_T c1_i_b;
  int32_T c1_k_loop_ub;
  int32_T c1_i381;
  real32_T c1_c_idx;
  real_T c1_highThreshTemp_data[1];
  real32_T c1_m_x;
  int32_T c1_l_loop_ub;
  boolean_T c1_j_b;
  int32_T c1_i382;
  real32_T c1_g_a;
  int32_T c1_c;
  real32_T c1_h_a;
  real_T c1_d13;
  int32_T c1_i383;
  int32_T c1_b_c;
  int32_T c1_i384;
  int32_T c1_lowThresh_size[1];
  int32_T c1_b_size[1];
  int32_T c1_m_loop_ub;
  int32_T c1_i385;
  int32_T c1_i386;
  real_T c1_b_data[1];
  int32_T c1_i387;
  int32_T c1_i388;
  real_T c1_lowThresh_data[1];
  int32_T c1_n_loop_ub;
  int32_T c1_i389;
  boolean_T c1_b20;
  boolean_T c1_b21;
  c1_emxArray_boolean_T *c1_E;
  int32_T c1_i390;
  int32_T c1_i391;
  int32_T c1_i392;
  int32_T c1_o_loop_ub;
  int32_T c1_i393;
  c1_emxArray_boolean_T *c1_n_x;
  real_T c1_e_n;
  real_T c1_c_m;
  real_T c1_lowThresh;
  real_T c1_b_lowThresh;
  int32_T c1_i394;
  real_T c1_highThreshTemp;
  int32_T c1_i395;
  int32_T c1_i396;
  int32_T c1_p_loop_ub;
  int32_T c1_i397;
  int32_T c1_i398;
  int32_T c1_i399;
  int32_T c1_o_x[2];
  int32_T c1_b_E[2];
  int32_T c1_i400;
  int32_T c1_i401;
  int32_T c1_i402;
  int32_T c1_i403;
  int32_T c1_i404;
  int32_T c1_i405;
  int32_T c1_i406;
  int32_T c1_q_loop_ub;
  int32_T c1_i407;
  int32_T c1_nx;
  boolean_T c1_b22;
  boolean_T c1_b23;
  boolean_T c1_b24;
  const mxArray *c1_g_y = NULL;
  real_T c1_dn;
  const mxArray *c1_h_y = NULL;
  real_T c1_dm;
  int32_T c1_d_m;
  int32_T c1_f_n;
  c1_emxArray_int32_T *c1_o_i;
  c1_emxArray_int32_T *c1_j;
  c1_emxArray_boolean_T *c1_v;
  int32_T c1_i408;
  int32_T c1_f_k;
  int32_T c1_e_m;
  int32_T c1_i409;
  int32_T c1_g_n;
  int32_T c1_d_idx;
  int32_T c1_i410;
  c1_emxArray_real_T *c1_idxStrongR;
  int32_T c1_i411;
  int32_T c1_i412;
  int32_T c1_r_loop_ub;
  int32_T c1_indexSize[2];
  int32_T c1_i413;
  int32_T c1_i414;
  c1_emxArray_real_T *c1_idxStrongC;
  int32_T c1_c_ii;
  int32_T c1_i415;
  int32_T c1_jj;
  int32_T c1_s_loop_ub;
  int32_T c1_i416;
  const mxArray *c1_i_y = NULL;
  const mxArray *c1_j_y = NULL;
  boolean_T c1_b25;
  int32_T c1_i417;
  int32_T c1_i418;
  int32_T c1_i419;
  int32_T c1_i420;
  c1_emxArray_int32_T *c1_r11;
  int32_T c1_i421;
  int32_T c1_i422;
  int32_T c1_i423;
  int32_T c1_t_loop_ub;
  int32_T c1_u_loop_ub;
  int32_T c1_i424;
  int32_T c1_i425;
  int32_T c1_matrixSize;
  int32_T c1_size1;
  boolean_T c1_k_b;
  boolean_T c1_nonSingletonDimFound;
  boolean_T c1_c_c;
  boolean_T c1_d_c;
  boolean_T c1_l_b;
  const mxArray *c1_k_y = NULL;
  int32_T c1_i426;
  const mxArray *c1_l_y = NULL;
  boolean_T c1_b26;
  int32_T c1_i427;
  int32_T c1_i428;
  int32_T c1_v_loop_ub;
  int32_T c1_i429;
  int32_T c1_b_matrixSize;
  int32_T c1_b_size1;
  boolean_T c1_m_b;
  boolean_T c1_b_nonSingletonDimFound;
  boolean_T c1_e_c;
  boolean_T c1_f_c;
  boolean_T c1_n_b;
  const mxArray *c1_m_y = NULL;
  int32_T c1_i430;
  const mxArray *c1_n_y = NULL;
  boolean_T c1_b27;
  int32_T c1_i431;
  int32_T c1_i432;
  int32_T c1_w_loop_ub;
  int32_T c1_i433;
  int32_T c1_c_matrixSize;
  int32_T c1_c_size1;
  boolean_T c1_o_b;
  boolean_T c1_c_nonSingletonDimFound;
  boolean_T c1_g_c;
  boolean_T c1_h_c;
  boolean_T c1_p_b;
  const mxArray *c1_o_y = NULL;
  const mxArray *c1_p_y = NULL;
  boolean_T exitg1;
  boolean_T guard1 = false;
  c1_emxInit_real32_T(chartInstance, &c1_a, 2, &c1_tc_emlrtRTEI);
  c1_i292 = c1_a->size[0] * c1_a->size[1];
  c1_a->size[0] = c1_varargin_1->size[0];
  c1_a->size[1] = c1_varargin_1->size[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_a, c1_i292, &c1_tc_emlrtRTEI);
  c1_i293 = c1_a->size[0];
  c1_i294 = c1_a->size[1];
  c1_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i295 = 0; c1_i295 <= c1_loop_ub; c1_i295++) {
    c1_a->data[c1_i295] = (real32_T)c1_varargin_1->data[c1_i295];
  }

  c1_b8 = (c1_a->size[0] == 0);
  c1_b9 = (c1_a->size[1] == 0);
  if (c1_b8 || c1_b9) {
    for (c1_i296 = 0; c1_i296 < 2; c1_i296++) {
      c1_connDimsT[c1_i296] = (real_T)c1_a->size[c1_i296];
    }

    for (c1_i298 = 0; c1_i298 < 2; c1_i298++) {
      c1_connDimsT[c1_i298];
    }

    c1_i300 = c1_varargout_1->size[0] * c1_varargout_1->size[1];
    c1_varargout_1->size[0] = (int32_T)c1_connDimsT[0];
    c1_varargout_1->size[1] = (int32_T)c1_connDimsT[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_varargout_1, c1_i300,
      &c1_uc_emlrtRTEI);
    c1_i302 = c1_varargout_1->size[0];
    c1_i303 = c1_varargout_1->size[1];
    c1_b_loop_ub = (int32_T)c1_connDimsT[0] * (int32_T)c1_connDimsT[1] - 1;
    for (c1_i304 = 0; c1_i304 <= c1_b_loop_ub; c1_i304++) {
      c1_varargout_1->data[c1_i304] = false;
    }
  } else {
    c1_n = (real_T)c1_a->size[1];
    c1_m = (real_T)c1_a->size[0];
    for (c1_i297 = 0; c1_i297 < 13; c1_i297++) {
      c1_derivGaussKernel[c1_i297] = c1_dv5[c1_i297];
    }

    for (c1_i299 = 0; c1_i299 < 6; c1_i299++) {
      c1_derivGaussKernel[c1_i299] = c1_dv6[c1_i299];
    }

    for (c1_i301 = 0; c1_i301 < 6; c1_i301++) {
      c1_x[c1_i301] = c1_derivGaussKernel[c1_i301 + 7];
    }

    c1_y = c1_x[0];
    for (c1_k = 0; c1_k < 5; c1_k++) {
      c1_xoffset = c1_k;
      c1_ix = c1_xoffset + 1;
      c1_y += c1_x[c1_ix];
    }

    c1_b_x = c1_y;
    c1_c_x = c1_b_x;
    c1_d_x = c1_c_x;
    c1_b_y = muDoubleScalarAbs(c1_d_x);
    for (c1_i305 = 0; c1_i305 < 6; c1_i305++) {
      c1_derivGaussKernel[c1_i305 + 7] /= c1_b_y;
    }

    c1_outSizeT[0] = (real_T)c1_a->size[0];
    c1_startT[0] = 6.0;
    c1_outSizeT[1] = (real_T)c1_a->size[1];
    c1_startT[1] = 0.0;
    c1_b10 = (c1_a->size[0] == 0);
    c1_b11 = (c1_a->size[1] == 0);
    c1_emxInit_real32_T(chartInstance, &c1_b_a, 2, &c1_yd_emlrtRTEI);
    c1_emxInit_real32_T(chartInstance, &c1_i1, 2, &c1_be_emlrtRTEI);
    if (c1_b10 || c1_b11) {
      c1_i306 = c1_i1->size[0] * c1_i1->size[1];
      c1_i1->size[0] = c1_a->size[0];
      c1_i1->size[1] = c1_a->size[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_i1, c1_i306,
        &c1_vc_emlrtRTEI);
      c1_i308 = c1_i1->size[0];
      c1_i310 = c1_i1->size[1];
      c1_c_loop_ub = c1_a->size[0] * c1_a->size[1] - 1;
      for (c1_i312 = 0; c1_i312 <= c1_c_loop_ub; c1_i312++) {
        c1_i1->data[c1_i312] = c1_a->data[c1_i312];
      }
    } else {
      c1_emxInit_real32_T(chartInstance, &c1_c_a, 2, &c1_wc_emlrtRTEI);
      c1_i307 = c1_c_a->size[0] * c1_c_a->size[1];
      c1_c_a->size[0] = c1_a->size[0];
      c1_c_a->size[1] = c1_a->size[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_c_a, c1_i307,
        &c1_wc_emlrtRTEI);
      c1_i309 = c1_c_a->size[0];
      c1_i311 = c1_c_a->size[1];
      c1_d_loop_ub = c1_a->size[0] * c1_a->size[1] - 1;
      for (c1_i313 = 0; c1_i313 <= c1_d_loop_ub; c1_i313++) {
        c1_c_a->data[c1_i313] = c1_a->data[c1_i313];
      }

      for (c1_i314 = 0; c1_i314 < 2; c1_i314++) {
        c1_b_startT[c1_i314] = c1_startT[c1_i314];
      }

      c1_padImage(chartInstance, c1_c_a, c1_b_startT, c1_b_a);
      c1_tooBig = true;
      c1_emxFree_real32_T(chartInstance, &c1_c_a);
      for (c1_i = 0; c1_i < 2; c1_i++) {
        c1_b_i = 1.0 + (real_T)c1_i;
        if (c1_tooBig && (c1_outSizeT[(int32_T)c1_b_i - 1] > 65500.0)) {
          c1_b_tooBig = true;
        } else {
          c1_b_tooBig = false;
        }

        c1_tooBig = c1_b_tooBig;
      }

      if (!c1_tooBig) {
        c1_modeFlag = true;
      } else {
        c1_modeFlag = false;
      }

      if (c1_modeFlag) {
        c1_b_modeFlag = true;
      } else {
        c1_b_modeFlag = false;
      }

      c1_c_modeFlag = c1_b_modeFlag;
      c1_i317 = c1_i1->size[0] * c1_i1->size[1];
      c1_i1->size[0] = (int32_T)c1_outSizeT[0];
      c1_i1->size[1] = (int32_T)c1_outSizeT[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_i1, c1_i317,
        &c1_xc_emlrtRTEI);
      if (c1_c_modeFlag) {
        for (c1_i320 = 0; c1_i320 < 2; c1_i320++) {
          c1_padSizeT[c1_i320] = (real_T)c1_b_a->size[c1_i320];
        }

        for (c1_i323 = 0; c1_i323 < 2; c1_i323++) {
          c1_padSizeT[c1_i323];
        }

        for (c1_i327 = 0; c1_i327 < 2; c1_i327++) {
          c1_connDimsT[c1_i327] = 13.0 + -12.0 * (real_T)c1_i327;
        }

        ippfilter_real32(&c1_b_a->data[0], &c1_i1->data[0], c1_outSizeT, 2.0,
                         c1_padSizeT, c1_kernel, c1_connDimsT, true);
      } else {
        for (c1_i319 = 0; c1_i319 < 2; c1_i319++) {
          c1_padSizeT[c1_i319] = (real_T)c1_b_a->size[c1_i319];
        }

        for (c1_i322 = 0; c1_i322 < 2; c1_i322++) {
          c1_padSizeT[c1_i322];
        }

        for (c1_i326 = 0; c1_i326 < 13; c1_i326++) {
          c1_conn[c1_i326] = true;
        }

        for (c1_i329 = 0; c1_i329 < 2; c1_i329++) {
          c1_connDimsT[c1_i329] = 13.0 + -12.0 * (real_T)c1_i329;
        }

        imfilter_real32(&c1_b_a->data[0], &c1_i1->data[0], 2.0, c1_outSizeT, 2.0,
                        c1_padSizeT, c1_kernel, 13.0, c1_conn, 2.0, c1_connDimsT,
                        c1_startT, 2.0, true, true);
      }
    }

    c1_outSizeT[0] = (real_T)c1_i1->size[0];
    c1_startT[0] = 0.0;
    c1_outSizeT[1] = (real_T)c1_i1->size[1];
    c1_startT[1] = 6.0;
    c1_b12 = (c1_i1->size[0] == 0);
    c1_b13 = (c1_i1->size[1] == 0);
    if (c1_b12 || c1_b13) {
    } else {
      for (c1_i315 = 0; c1_i315 < 2; c1_i315++) {
        c1_c_startT[c1_i315] = c1_startT[c1_i315];
      }

      c1_padImage(chartInstance, c1_i1, c1_c_startT, c1_b_a);
      c1_trueCount = 0;
      for (c1_c_i = 0; c1_c_i < 13; c1_c_i++) {
        if (c1_derivGaussKernel[c1_c_i] != 0.0) {
          c1_trueCount++;
        }
      }

      c1_tmp_size[0] = c1_trueCount;
      c1_partialTrueCount = 0;
      for (c1_d_i = 0; c1_d_i < 13; c1_d_i++) {
        if (c1_derivGaussKernel[c1_d_i] != 0.0) {
          c1_tmp_data[c1_partialTrueCount] = c1_d_i + 1;
          c1_partialTrueCount++;
        }
      }

      c1_nonzero_h_size[0] = c1_tmp_size[0];
      c1_e_loop_ub = c1_tmp_size[0] - 1;
      for (c1_i321 = 0; c1_i321 <= c1_e_loop_ub; c1_i321++) {
        c1_nonzero_h_data[c1_i321] = c1_derivGaussKernel[c1_tmp_data[c1_i321] -
          1];
      }

      for (c1_i324 = 0; c1_i324 < 13; c1_i324++) {
        c1_conn[c1_i324] = (c1_derivGaussKernel[c1_i324] != 0.0);
      }

      c1_densityFlag = false;
      if ((real_T)c1_nonzero_h_size[0] / 13.0 > 0.05) {
        c1_densityFlag = true;
      }

      c1_e_tooBig = true;
      for (c1_g_i = 0; c1_g_i < 2; c1_g_i++) {
        c1_i_i = 1.0 + (real_T)c1_g_i;
        if (c1_e_tooBig && (c1_outSizeT[(int32_T)c1_i_i - 1] > 65500.0)) {
          c1_f_tooBig = true;
        } else {
          c1_f_tooBig = false;
        }

        c1_e_tooBig = c1_f_tooBig;
      }

      if (c1_densityFlag && (!c1_e_tooBig)) {
        c1_g_modeFlag = true;
      } else {
        c1_g_modeFlag = false;
      }

      if (c1_g_modeFlag) {
        c1_h_modeFlag = true;
      } else {
        c1_h_modeFlag = false;
      }

      for (c1_i342 = 0; c1_i342 < 13; c1_i342++) {
        c1_conn[c1_i342];
      }

      c1_i_modeFlag = c1_h_modeFlag;
      c1_i347 = c1_i1->size[0] * c1_i1->size[1];
      c1_i1->size[0] = (int32_T)c1_outSizeT[0];
      c1_i1->size[1] = (int32_T)c1_outSizeT[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_i1, c1_i347,
        &c1_xc_emlrtRTEI);
      if (c1_i_modeFlag) {
        for (c1_i352 = 0; c1_i352 < 2; c1_i352++) {
          c1_padSizeT[c1_i352] = (real_T)c1_b_a->size[c1_i352];
        }

        for (c1_i355 = 0; c1_i355 < 2; c1_i355++) {
          c1_padSizeT[c1_i355];
        }

        for (c1_i356 = 0; c1_i356 < 2; c1_i356++) {
          c1_connDimsT[c1_i356] = 1.0 + 12.0 * (real_T)c1_i356;
        }

        ippfilter_real32(&c1_b_a->data[0], &c1_i1->data[0], c1_outSizeT, 2.0,
                         c1_padSizeT, c1_derivGaussKernel, c1_connDimsT, true);
      } else {
        for (c1_i351 = 0; c1_i351 < 2; c1_i351++) {
          c1_padSizeT[c1_i351] = (real_T)c1_b_a->size[c1_i351];
        }

        for (c1_i354 = 0; c1_i354 < 2; c1_i354++) {
          c1_padSizeT[c1_i354];
        }

        c1_numKernElem = (real_T)c1_nonzero_h_size[0];
        for (c1_i358 = 0; c1_i358 < 13; c1_i358++) {
          c1_conn[c1_i358];
        }

        for (c1_i359 = 0; c1_i359 < 2; c1_i359++) {
          c1_connDimsT[c1_i359] = 1.0 + 12.0 * (real_T)c1_i359;
        }

        imfilter_real32(&c1_b_a->data[0], &c1_i1->data[0], 2.0, c1_outSizeT, 2.0,
                        c1_padSizeT, &c1_nonzero_h_data[0], c1_numKernElem,
                        c1_conn, 2.0, c1_connDimsT, c1_startT, 2.0, true, true);
      }
    }

    c1_outSizeT[0] = (real_T)c1_a->size[0];
    c1_startT[0] = 0.0;
    c1_outSizeT[1] = (real_T)c1_a->size[1];
    c1_startT[1] = 6.0;
    c1_b14 = (c1_a->size[0] == 0);
    c1_b15 = (c1_a->size[1] == 0);
    if (c1_b14 || c1_b15) {
    } else {
      for (c1_i316 = 0; c1_i316 < 2; c1_i316++) {
        c1_d_startT[c1_i316] = c1_startT[c1_i316];
      }

      c1_padImage(chartInstance, c1_a, c1_d_startT, c1_b_a);
      c1_c_tooBig = true;
      for (c1_e_i = 0; c1_e_i < 2; c1_e_i++) {
        c1_f_i = 1.0 + (real_T)c1_e_i;
        if (c1_c_tooBig && (c1_outSizeT[(int32_T)c1_f_i - 1] > 65500.0)) {
          c1_d_tooBig = true;
        } else {
          c1_d_tooBig = false;
        }

        c1_c_tooBig = c1_d_tooBig;
      }

      if (!c1_c_tooBig) {
        c1_d_modeFlag = true;
      } else {
        c1_d_modeFlag = false;
      }

      if (c1_d_modeFlag) {
        c1_e_modeFlag = true;
      } else {
        c1_e_modeFlag = false;
      }

      c1_f_modeFlag = c1_e_modeFlag;
      c1_i330 = c1_a->size[0] * c1_a->size[1];
      c1_a->size[0] = (int32_T)c1_outSizeT[0];
      c1_a->size[1] = (int32_T)c1_outSizeT[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_a, c1_i330,
        &c1_xc_emlrtRTEI);
      if (c1_f_modeFlag) {
        for (c1_i335 = 0; c1_i335 < 2; c1_i335++) {
          c1_padSizeT[c1_i335] = (real_T)c1_b_a->size[c1_i335];
        }

        for (c1_i338 = 0; c1_i338 < 2; c1_i338++) {
          c1_padSizeT[c1_i338];
        }

        for (c1_i341 = 0; c1_i341 < 2; c1_i341++) {
          c1_connDimsT[c1_i341] = 1.0 + 12.0 * (real_T)c1_i341;
        }

        ippfilter_real32(&c1_b_a->data[0], &c1_a->data[0], c1_outSizeT, 2.0,
                         c1_padSizeT, c1_b_kernel, c1_connDimsT, true);
      } else {
        for (c1_i334 = 0; c1_i334 < 2; c1_i334++) {
          c1_padSizeT[c1_i334] = (real_T)c1_b_a->size[c1_i334];
        }

        for (c1_i337 = 0; c1_i337 < 2; c1_i337++) {
          c1_padSizeT[c1_i337];
        }

        for (c1_i340 = 0; c1_i340 < 13; c1_i340++) {
          c1_conn[c1_i340] = true;
        }

        for (c1_i345 = 0; c1_i345 < 2; c1_i345++) {
          c1_connDimsT[c1_i345] = 1.0 + 12.0 * (real_T)c1_i345;
        }

        imfilter_real32(&c1_b_a->data[0], &c1_a->data[0], 2.0, c1_outSizeT, 2.0,
                        c1_padSizeT, c1_kernel, 13.0, c1_conn, 2.0, c1_connDimsT,
                        c1_startT, 2.0, true, true);
      }
    }

    for (c1_i318 = 0; c1_i318 < 13; c1_i318++) {
      c1_derivGaussKernel[c1_i318];
    }

    c1_outSizeT[0] = (real_T)c1_a->size[0];
    c1_startT[0] = 6.0;
    c1_outSizeT[1] = (real_T)c1_a->size[1];
    c1_startT[1] = 0.0;
    c1_b16 = (c1_a->size[0] == 0);
    c1_b17 = (c1_a->size[1] == 0);
    if (c1_b16 || c1_b17) {
    } else {
      for (c1_i325 = 0; c1_i325 < 2; c1_i325++) {
        c1_e_startT[c1_i325] = c1_startT[c1_i325];
      }

      c1_padImage(chartInstance, c1_a, c1_e_startT, c1_b_a);
      for (c1_i331 = 0; c1_i331 < 13; c1_i331++) {
        c1_derivGaussKernel[c1_i331];
      }

      c1_b_trueCount = 0;
      for (c1_h_i = 0; c1_h_i < 13; c1_h_i++) {
        if (c1_derivGaussKernel[c1_h_i] != 0.0) {
          c1_b_trueCount++;
        }
      }

      c1_b_tmp_size[0] = c1_b_trueCount;
      c1_b_partialTrueCount = 0;
      for (c1_j_i = 0; c1_j_i < 13; c1_j_i++) {
        if (c1_derivGaussKernel[c1_j_i] != 0.0) {
          c1_b_tmp_data[c1_b_partialTrueCount] = c1_j_i + 1;
          c1_b_partialTrueCount++;
        }
      }

      for (c1_i343 = 0; c1_i343 < 13; c1_i343++) {
        c1_derivGaussKernel[c1_i343];
      }

      c1_nonzero_h_size[0] = c1_b_tmp_size[0];
      c1_h_loop_ub = c1_b_tmp_size[0] - 1;
      for (c1_i349 = 0; c1_i349 <= c1_h_loop_ub; c1_i349++) {
        c1_nonzero_h_data[c1_i349] = c1_derivGaussKernel[c1_b_tmp_data[c1_i349]
          - 1];
      }

      for (c1_i350 = 0; c1_i350 < 13; c1_i350++) {
        c1_conn[c1_i350] = (c1_derivGaussKernel[c1_i350] != 0.0);
      }

      c1_b_densityFlag = false;
      if ((real_T)c1_nonzero_h_size[0] / 13.0 > 0.05) {
        c1_b_densityFlag = true;
      }

      c1_g_tooBig = true;
      for (c1_k_i = 0; c1_k_i < 2; c1_k_i++) {
        c1_l_i = 1.0 + (real_T)c1_k_i;
        if (c1_g_tooBig && (c1_outSizeT[(int32_T)c1_l_i - 1] > 65500.0)) {
          c1_h_tooBig = true;
        } else {
          c1_h_tooBig = false;
        }

        c1_g_tooBig = c1_h_tooBig;
      }

      if (c1_b_densityFlag && (!c1_g_tooBig)) {
        c1_j_modeFlag = true;
      } else {
        c1_j_modeFlag = false;
      }

      if (c1_j_modeFlag) {
        c1_k_modeFlag = true;
      } else {
        c1_k_modeFlag = false;
      }

      for (c1_i360 = 0; c1_i360 < 13; c1_i360++) {
        c1_derivGaussKernel[c1_i360];
      }

      c1_l_modeFlag = c1_k_modeFlag;
      c1_i361 = c1_a->size[0] * c1_a->size[1];
      c1_a->size[0] = (int32_T)c1_outSizeT[0];
      c1_a->size[1] = (int32_T)c1_outSizeT[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_a, c1_i361,
        &c1_xc_emlrtRTEI);
      if (c1_l_modeFlag) {
        for (c1_i365 = 0; c1_i365 < 2; c1_i365++) {
          c1_padSizeT[c1_i365] = (real_T)c1_b_a->size[c1_i365];
        }

        for (c1_i371 = 0; c1_i371 < 2; c1_i371++) {
          c1_padSizeT[c1_i371];
        }

        for (c1_i374 = 0; c1_i374 < 13; c1_i374++) {
          c1_derivGaussKernel[c1_i374];
        }

        for (c1_i377 = 0; c1_i377 < 2; c1_i377++) {
          c1_connDimsT[c1_i377] = 13.0 + -12.0 * (real_T)c1_i377;
        }

        ippfilter_real32(&c1_b_a->data[0], &c1_a->data[0], c1_outSizeT, 2.0,
                         c1_padSizeT, c1_derivGaussKernel, c1_connDimsT, true);
      } else {
        for (c1_i364 = 0; c1_i364 < 2; c1_i364++) {
          c1_padSizeT[c1_i364] = (real_T)c1_b_a->size[c1_i364];
        }

        for (c1_i370 = 0; c1_i370 < 2; c1_i370++) {
          c1_padSizeT[c1_i370];
        }

        c1_b_numKernElem = (real_T)c1_nonzero_h_size[0];
        for (c1_i375 = 0; c1_i375 < 2; c1_i375++) {
          c1_connDimsT[c1_i375] = 13.0 + -12.0 * (real_T)c1_i375;
        }

        imfilter_real32(&c1_b_a->data[0], &c1_a->data[0], 2.0, c1_outSizeT, 2.0,
                        c1_padSizeT, &c1_nonzero_h_data[0], c1_b_numKernElem,
                        c1_conn, 2.0, c1_connDimsT, c1_startT, 2.0, true, true);
      }
    }

    c1_emxInit_real32_T(chartInstance, &c1_b_i1, 2, &c1_bd_emlrtRTEI);
    c1_i328 = c1_b_i1->size[0] * c1_b_i1->size[1];
    c1_b_i1->size[0] = c1_i1->size[0];
    c1_b_i1->size[1] = c1_i1->size[1];
    c1_emxEnsureCapacity_real32_T(chartInstance, c1_b_i1, c1_i328,
      &c1_bd_emlrtRTEI);
    c1_i332 = c1_b_i1->size[0];
    c1_i333 = c1_b_i1->size[1];
    c1_f_loop_ub = c1_i1->size[0] * c1_i1->size[1] - 1;
    for (c1_i336 = 0; c1_i336 <= c1_f_loop_ub; c1_i336++) {
      c1_b_i1->data[c1_i336] = c1_i1->data[c1_i336];
    }

    c1_emxInit_real32_T(chartInstance, &c1_d_a, 2, &c1_cd_emlrtRTEI);
    c1_i339 = c1_d_a->size[0] * c1_d_a->size[1];
    c1_d_a->size[0] = c1_a->size[0];
    c1_d_a->size[1] = c1_a->size[1];
    c1_emxEnsureCapacity_real32_T(chartInstance, c1_d_a, c1_i339,
      &c1_cd_emlrtRTEI);
    c1_i344 = c1_d_a->size[0];
    c1_i346 = c1_d_a->size[1];
    c1_g_loop_ub = c1_a->size[0] * c1_a->size[1] - 1;
    for (c1_i348 = 0; c1_i348 <= c1_g_loop_ub; c1_i348++) {
      c1_d_a->data[c1_i348] = c1_a->data[c1_i348];
    }

    c1_emxInit_real32_T1(chartInstance, &c1_b_varargin_1, 1, &c1_dd_emlrtRTEI);
    c1_hypot(chartInstance, c1_b_i1, c1_d_a, c1_b_a);
    c1_i353 = c1_b_varargin_1->size[0];
    c1_b_varargin_1->size[0] = c1_b_a->size[0] * c1_b_a->size[1];
    c1_emxEnsureCapacity_real32_T1(chartInstance, c1_b_varargin_1, c1_i353,
      &c1_dd_emlrtRTEI);
    c1_i_loop_ub = c1_b_a->size[0] * c1_b_a->size[1] - 1;
    c1_emxFree_real32_T(chartInstance, &c1_d_a);
    c1_emxFree_real32_T(chartInstance, &c1_b_i1);
    for (c1_i357 = 0; c1_i357 <= c1_i_loop_ub; c1_i357++) {
      c1_b_varargin_1->data[c1_i357] = c1_b_a->data[c1_i357];
    }

    if ((real_T)c1_b_varargin_1->size[0] >= 1.0) {
    } else {
      c1_c_y = NULL;
      sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv0, 10, 0U, 1U, 0U, 2, 1, 39),
                    false);
      c1_d_y = NULL;
      sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv0, 10, 0U, 1U, 0U, 2, 1, 39),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_d_y)));
    }

    c1_b_n = c1_b_varargin_1->size[0];
    if (c1_b_n <= 2) {
      if (c1_b_n == 1) {
        c1_magmax = c1_b_varargin_1->data[0];
      } else if (c1_b_varargin_1->data[0] < c1_b_varargin_1->data[1]) {
        c1_magmax = c1_b_varargin_1->data[1];
      } else {
        c1_g_x = c1_b_varargin_1->data[0];
        c1_b_b = muSingleScalarIsNaN(c1_g_x);
        if (c1_b_b) {
          c1_h_x = c1_b_varargin_1->data[1];
          c1_c_b = muSingleScalarIsNaN(c1_h_x);
          if (!c1_c_b) {
            c1_magmax = c1_b_varargin_1->data[1];
          } else {
            c1_magmax = c1_b_varargin_1->data[0];
          }
        } else {
          c1_magmax = c1_b_varargin_1->data[0];
        }
      }
    } else {
      c1_c_n = c1_b_varargin_1->size[0];
      c1_e_x = c1_b_varargin_1->data[0];
      c1_f_x = c1_e_x;
      c1_b = muSingleScalarIsNaN(c1_f_x);
      c1_p = !c1_b;
      if (c1_p) {
        c1_idx = 1;
      } else {
        c1_idx = 0;
        c1_d_b = c1_c_n;
        c1_e_b = c1_d_b;
        if (2 > c1_e_b) {
          c1_overflow = false;
        } else {
          c1_overflow = (c1_e_b > 2147483646);
        }

        if (c1_overflow) {
          c1_check_forloop_overflow_error(chartInstance, true);
        }

        c1_b_k = 2;
        exitg1 = false;
        while ((!exitg1) && (c1_b_k <= c1_c_n)) {
          c1_i_x = c1_b_varargin_1->data[c1_b_k - 1];
          c1_j_x = c1_i_x;
          c1_h_b = muSingleScalarIsNaN(c1_j_x);
          c1_b_p = !c1_h_b;
          if (c1_b_p) {
            c1_idx = c1_b_k;
            exitg1 = true;
          } else {
            c1_b_k++;
          }
        }
      }

      if (c1_idx == 0) {
        c1_magmax = c1_b_varargin_1->data[0];
      } else {
        c1_first = c1_idx - 1;
        c1_last = c1_b_n;
        c1_ex = c1_b_varargin_1->data[c1_first];
        c1_i366 = c1_first + 2;
        c1_e_a = c1_i366;
        c1_f_b = c1_last;
        c1_f_a = c1_e_a;
        c1_g_b = c1_f_b;
        if (c1_f_a > c1_g_b) {
          c1_b_overflow = false;
        } else {
          c1_b_overflow = (c1_g_b > 2147483646);
        }

        if (c1_b_overflow) {
          c1_check_forloop_overflow_error(chartInstance, true);
        }

        for (c1_e_k = c1_i366 - 1; c1_e_k < c1_last; c1_e_k++) {
          if (c1_ex < c1_b_varargin_1->data[c1_e_k]) {
            c1_ex = c1_b_varargin_1->data[c1_e_k];
          }
        }

        c1_magmax = c1_ex;
      }
    }

    c1_emxFree_real32_T(chartInstance, &c1_b_varargin_1);
    if (c1_magmax > 0.0F) {
      c1_e_y = c1_magmax;
      c1_f_y = c1_e_y;
      c1_i362 = c1_b_a->size[0] * c1_b_a->size[1];
      c1_i363 = c1_b_a->size[0] * c1_b_a->size[1];
      c1_b_a->size[0];
      c1_b_a->size[1];
      c1_emxEnsureCapacity_real32_T(chartInstance, c1_b_a, c1_i363,
        &c1_ed_emlrtRTEI);
      c1_i368 = c1_b_a->size[0];
      c1_i369 = c1_b_a->size[1];
      c1_i372 = c1_i362;
      c1_j_loop_ub = c1_i372 - 1;
      for (c1_i373 = 0; c1_i373 <= c1_j_loop_ub; c1_i373++) {
        c1_b_a->data[c1_i373] /= c1_f_y;
      }
    }

    c1_d_n = (real_T)c1_b_a->size[1];
    c1_b_m = (real_T)c1_b_a->size[0];
    c1_b18 = (c1_b_a->size[0] == 0);
    c1_b19 = (c1_b_a->size[1] == 0);
    if (c1_b18 || c1_b19) {
      for (c1_i367 = 0; c1_i367 < 64; c1_i367++) {
        c1_counts[c1_i367] = 0.0;
      }
    } else {
      c1_numCores = 1.0;
      getnumcores(&c1_numCores);
      if (((real_T)(c1_b_a->size[0] * c1_b_a->size[1]) > 500000.0) &&
          (c1_numCores > 1.0)) {
        c1_useParallel = true;
      } else {
        c1_useParallel = false;
      }

      c1_rowsA = (real_T)c1_b_a->size[0];
      c1_colsAEtc = (real_T)(c1_b_a->size[0] * c1_b_a->size[1]) / (real_T)
        c1_b_a->size[0];
      if (c1_useParallel) {
        c1_numelA = (real_T)(c1_b_a->size[0] * c1_b_a->size[1]);
        c1_numRows = c1_rowsA;
        c1_numCols = c1_colsAEtc;
        c1_b_nanFlag = false;
        c1_rngFlag = false;
        tbbhist_real32_scaled(&c1_b_a->data[0], c1_numelA, c1_numRows,
                              c1_numCols, c1_counts, 64.0, 1.0, 64.0,
                              &c1_rngFlag, &c1_b_nanFlag);
        c1_b_rngFlag = c1_rngFlag;
        c1_c_nanFlag = c1_b_nanFlag;
      } else {
        for (c1_i378 = 0; c1_i378 < 64; c1_i378++) {
          c1_counts[c1_i378] = 0.0;
        }

        c1_nanFlag = false;
        c1_d12 = (real_T)(c1_b_a->size[0] * c1_b_a->size[1]);
        c1_i379 = (int32_T)c1_d12 - 1;
        for (c1_m_i = 0; c1_m_i <= c1_i379; c1_m_i++) {
          c1_n_i = 1.0 + (real_T)c1_m_i;
          c1_l_x = c1_b_a->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_n_i, 1, c1_b_a->size[0] * c1_b_a->size[1])
            - 1];
          c1_i_b = muSingleScalarIsNaN(c1_l_x);
          if (c1_i_b) {
            c1_nanFlag = true;
            c1_c_idx = 0.0F;
          } else {
            c1_c_idx = c1_b_a->data[sf_eml_array_bounds_check
              (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
               MAX_uint32_T, (int32_T)c1_n_i, 1, c1_b_a->size[0] * c1_b_a->size
               [1]) - 1] * 63.0F + 0.5F;
          }

          if (c1_c_idx > 63.0F) {
            c1_counts[63]++;
          } else {
            c1_m_x = c1_b_a->data[sf_eml_array_bounds_check
              (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
               MAX_uint32_T, (int32_T)c1_n_i, 1, c1_b_a->size[0] * c1_b_a->size
               [1]) - 1];
            c1_j_b = muSingleScalarIsInf(c1_m_x);
            if (c1_j_b) {
              c1_counts[63]++;
            } else {
              c1_g_a = c1_c_idx;
              c1_c = (int32_T)c1_g_a;
              c1_h_a = c1_c_idx;
              c1_b_c = (int32_T)c1_h_a;
              c1_counts[c1_c] = c1_counts[c1_b_c] + 1.0;
            }
          }
        }

        c1_b_rngFlag = false;
        c1_c_nanFlag = c1_nanFlag;
      }

      if (c1_b_rngFlag) {
        c1_warning(chartInstance);
      }

      if (c1_c_nanFlag) {
        c1_b_warning(chartInstance);
      }
    }

    for (c1_c_k = 0; c1_c_k < 63; c1_c_k++) {
      c1_d_k = c1_c_k;
      c1_counts[c1_d_k + 1] += c1_counts[c1_d_k];
    }

    c1_d11 = 0.7 * c1_b_m * c1_d_n;
    for (c1_i376 = 0; c1_i376 < 64; c1_i376++) {
      c1_k_x[c1_i376] = (c1_counts[c1_i376] > c1_d11);
    }

    c1_b_idx = 0;
    c1_ii_size[0] = 1;
    c1_ii = 1;
    exitg1 = false;
    while ((!exitg1) && (c1_ii - 1 < 64)) {
      c1_b_ii = c1_ii;
      if (c1_k_x[c1_b_ii - 1]) {
        c1_b_idx = 1;
        c1_ii_data[0] = c1_b_ii;
        exitg1 = true;
      } else {
        c1_ii++;
      }
    }

    if (c1_b_idx == 0) {
      c1_i380 = c1_ii_size[0];
      c1_ii_size[0] = 0;
    }

    c1_highThreshTemp_size[0] = c1_ii_size[0];
    c1_k_loop_ub = c1_ii_size[0] - 1;
    for (c1_i381 = 0; c1_i381 <= c1_k_loop_ub; c1_i381++) {
      c1_highThreshTemp_data[c1_i381] = (real_T)c1_ii_data[c1_i381];
    }

    c1_highThreshTemp_size[0];
    c1_l_loop_ub = c1_highThreshTemp_size[0] - 1;
    for (c1_i382 = 0; c1_i382 <= c1_l_loop_ub; c1_i382++) {
      c1_highThreshTemp_data[c1_i382] /= 64.0;
    }

    if (c1_highThreshTemp_size[0] == 0) {
      c1_i383 = c1_highThreshTemp_size[0];
      c1_highThreshTemp_size[0] = 0;
      c1_i384 = c1_lowThresh_size[0];
      c1_lowThresh_size[0] = 0;
    } else {
      c1_d13 = c1_highThreshTemp_data[0];
      c1_highThreshTemp_size[0] = 1;
      c1_highThreshTemp_data[0] = c1_d13;
      c1_b_size[0] = c1_highThreshTemp_size[0];
      c1_m_loop_ub = c1_highThreshTemp_size[0] - 1;
      for (c1_i385 = 0; c1_i385 <= c1_m_loop_ub; c1_i385++) {
        c1_b_data[c1_i385] = c1_highThreshTemp_data[c1_i385];
      }

      c1_b_size[0] = 1;
      c1_b_data[0] *= 0.4;
      c1_lowThresh_size[0] = 1;
      c1_lowThresh_data[0] = c1_b_data[0];
    }

    c1_i386 = c1_varargout_1->size[0] * c1_varargout_1->size[1];
    c1_varargout_1->size[0] = (int32_T)c1_m;
    c1_varargout_1->size[1] = (int32_T)c1_n;
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_varargout_1, c1_i386,
      &c1_jd_emlrtRTEI);
    c1_i387 = c1_varargout_1->size[0];
    c1_i388 = c1_varargout_1->size[1];
    c1_n_loop_ub = (int32_T)c1_m * (int32_T)c1_n - 1;
    for (c1_i389 = 0; c1_i389 <= c1_n_loop_ub; c1_i389++) {
      c1_varargout_1->data[c1_i389] = false;
    }

    c1_b20 = (c1_varargout_1->size[0] == 1);
    c1_b21 = (c1_varargout_1->size[1] == 1);
    if ((!c1_b20) && (!c1_b21)) {
      c1_emxInit_boolean_T(chartInstance, &c1_E, 2, &c1_nd_emlrtRTEI);
      c1_i390 = c1_E->size[0] * c1_E->size[1];
      c1_E->size[0] = c1_varargout_1->size[0];
      c1_E->size[1] = c1_varargout_1->size[1];
      c1_emxEnsureCapacity_boolean_T(chartInstance, c1_E, c1_i390,
        &c1_nd_emlrtRTEI);
      c1_i391 = c1_E->size[0];
      c1_i392 = c1_E->size[1];
      c1_o_loop_ub = c1_varargout_1->size[0] * c1_varargout_1->size[1] - 1;
      for (c1_i393 = 0; c1_i393 <= c1_o_loop_ub; c1_i393++) {
        c1_E->data[c1_i393] = c1_varargout_1->data[c1_i393];
      }

      c1_emxInit_boolean_T(chartInstance, &c1_n_x, 2, &c1_od_emlrtRTEI);
      c1_e_n = (real_T)c1_E->size[1];
      c1_c_m = (real_T)c1_E->size[0];
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_lowThresh_size[0]);
      c1_connDimsT[0] = c1_c_m;
      c1_connDimsT[1] = c1_e_n;
      c1_lowThresh = c1_lowThresh_data[0];
      c1_b_lowThresh = c1_lowThresh;
      cannythresholding_real32_tbb(&c1_i1->data[0], &c1_a->data[0],
        &c1_b_a->data[0], c1_connDimsT, c1_b_lowThresh, &c1_E->data[0]);
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_highThreshTemp_size[0]);
      c1_i394 = c1_n_x->size[0] * c1_n_x->size[1];
      c1_n_x->size[0] = c1_b_a->size[0];
      c1_n_x->size[1] = c1_b_a->size[1];
      c1_emxEnsureCapacity_boolean_T(chartInstance, c1_n_x, c1_i394,
        &c1_od_emlrtRTEI);
      c1_highThreshTemp = c1_highThreshTemp_data[0];
      c1_i395 = c1_n_x->size[0];
      c1_i396 = c1_n_x->size[1];
      c1_p_loop_ub = c1_b_a->size[0] * c1_b_a->size[1] - 1;
      for (c1_i397 = 0; c1_i397 <= c1_p_loop_ub; c1_i397++) {
        c1_n_x->data[c1_i397] = ((real_T)c1_b_a->data[c1_i397] >
          c1_highThreshTemp);
      }

      for (c1_i398 = 0; c1_i398 < 2; c1_i398++) {
        c1_o_x[c1_i398] = c1_n_x->size[c1_i398];
      }

      for (c1_i399 = 0; c1_i399 < 2; c1_i399++) {
        c1_b_E[c1_i399] = c1_E->size[c1_i399];
      }

      _SFD_SIZE_EQ_CHECK_ND(c1_o_x, c1_b_E, 2);
      c1_i400 = c1_n_x->size[0] * c1_n_x->size[1];
      c1_i401 = c1_E->size[0] * c1_E->size[1];
      c1_i402 = c1_n_x->size[0] * c1_n_x->size[1];
      c1_n_x->size[0];
      c1_n_x->size[1];
      c1_emxEnsureCapacity_boolean_T(chartInstance, c1_n_x, c1_i402,
        &c1_od_emlrtRTEI);
      c1_i403 = c1_n_x->size[0];
      c1_i404 = c1_n_x->size[1];
      c1_i405 = c1_i400;
      c1_i406 = c1_i401;
      c1_q_loop_ub = c1_i405 - 1;
      for (c1_i407 = 0; c1_i407 <= c1_q_loop_ub; c1_i407++) {
        c1_n_x->data[c1_i407] = (c1_n_x->data[c1_i407] && c1_E->data[c1_i407]);
      }

      c1_nx = c1_n_x->size[0] * c1_n_x->size[1];
      c1_b22 = (c1_n_x->size[0] == 1);
      c1_b23 = (c1_n_x->size[1] == 1);
      if (((!c1_b22) && (!c1_b23)) || ((real_T)c1_n_x->size[0] != 1.0) ||
          ((real_T)c1_n_x->size[1] <= 1.0)) {
        c1_b24 = true;
      } else {
        c1_b24 = false;
      }

      if (c1_b24) {
      } else {
        c1_g_y = NULL;
        sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv2, 10, 0U, 1U, 0U, 2, 1,
          36), false);
        c1_h_y = NULL;
        sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv2, 10, 0U, 1U, 0U, 2, 1,
          36), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                          c1_g_y, 14, sf_mex_call_debug
                          (sfGlobalDebugInstanceStruct, "getString", 1U, 1U, 14,
                           sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "message", 1U, 1U, 14, c1_h_y)));
      }

      c1_dn = (real_T)c1_n_x->size[1];
      c1_dm = (real_T)c1_n_x->size[0];
      c1_d_m = (int32_T)c1_dm;
      c1_f_n = (int32_T)c1_dn;
      c1_emxInit_int32_T(chartInstance, &c1_o_i, 1, &c1_ae_emlrtRTEI);
      c1_emxInit_int32_T(chartInstance, &c1_j, 1, &c1_ae_emlrtRTEI);
      if (c1_nx == 0) {
        c1_i408 = c1_o_i->size[0];
        c1_o_i->size[0] = 0;
        c1_i409 = c1_j->size[0];
        c1_j->size[0] = 0;
      } else {
        c1_emxInit_boolean_T1(chartInstance, &c1_v, 1, &c1_ae_emlrtRTEI);
        c1_f_k = c1_nx;
        c1_e_m = c1_d_m;
        c1_g_n = c1_f_n;
        c1_d_idx = 0;
        c1_i410 = c1_o_i->size[0];
        c1_o_i->size[0] = c1_f_k;
        c1_emxEnsureCapacity_int32_T(chartInstance, c1_o_i, c1_i410,
          &c1_qd_emlrtRTEI);
        c1_i412 = c1_j->size[0];
        c1_j->size[0] = c1_f_k;
        c1_emxEnsureCapacity_int32_T(chartInstance, c1_j, c1_i412,
          &c1_qd_emlrtRTEI);
        c1_indexSize[0] = c1_f_k;
        c1_indexSize[1] = 1;
        c1_i414 = c1_v->size[0];
        c1_v->size[0] = c1_indexSize[0];
        c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_v, c1_i414,
          &c1_qd_emlrtRTEI);
        c1_c_ii = 0;
        c1_jj = 0;
        exitg1 = false;
        while ((!exitg1) && (c1_jj + 1 <= c1_g_n)) {
          guard1 = false;
          if (c1_n_x->data[c1_c_ii + c1_n_x->size[0] * c1_jj]) {
            c1_d_idx++;
            c1_o_i->data[c1_d_idx - 1] = c1_c_ii + 1;
            c1_j->data[c1_d_idx - 1] = c1_jj + 1;
            c1_v->data[c1_d_idx - 1] = c1_n_x->data[c1_c_ii + c1_n_x->size[0] *
              c1_jj];
            if (c1_d_idx >= c1_f_k) {
              exitg1 = true;
            } else {
              guard1 = true;
            }
          } else {
            guard1 = true;
          }

          if (guard1) {
            c1_c_ii++;
            if (c1_c_ii + 1 > c1_e_m) {
              c1_c_ii = 0;
              c1_jj++;
            }
          }
        }

        if (c1_d_idx <= c1_f_k) {
        } else {
          c1_i_y = NULL;
          sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          c1_j_y = NULL;
          sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c1_i_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c1_j_y)));
        }

        if (c1_f_k == 1) {
          if (c1_d_idx == 0) {
            c1_i417 = c1_o_i->size[0];
            c1_o_i->size[0] = 0;
            c1_i420 = c1_j->size[0];
            c1_j->size[0] = 0;
          }
        } else {
          c1_b25 = (1 > c1_d_idx);
          if (c1_b25) {
            c1_i419 = 0;
          } else {
            c1_i419 = c1_d_idx;
          }

          c1_emxInit_int32_T(chartInstance, &c1_r11, 1, &c1_td_emlrtRTEI);
          c1_i421 = c1_r11->size[0];
          c1_r11->size[0] = c1_i419;
          c1_emxEnsureCapacity_int32_T(chartInstance, c1_r11, c1_i421,
            &c1_td_emlrtRTEI);
          c1_u_loop_ub = c1_i419 - 1;
          for (c1_i425 = 0; c1_i425 <= c1_u_loop_ub; c1_i425++) {
            c1_r11->data[c1_i425] = 1 + c1_i425;
          }

          c1_matrixSize = c1_o_i->size[0];
          c1_indexSize[0] = 1;
          c1_indexSize[1] = c1_r11->size[0];
          c1_size1 = c1_matrixSize;
          if (c1_size1 != 1) {
            c1_k_b = false;
          } else {
            c1_k_b = true;
          }

          if (c1_k_b) {
            c1_nonSingletonDimFound = false;
            if (c1_indexSize[1] != 1) {
              c1_nonSingletonDimFound = true;
            }

            c1_l_b = c1_nonSingletonDimFound;
            if (c1_l_b) {
              c1_c_c = true;
            } else {
              c1_c_c = false;
            }
          } else {
            c1_c_c = false;
          }

          c1_d_c = c1_c_c;
          if (!c1_d_c) {
          } else {
            c1_k_y = NULL;
            sf_mex_assign(&c1_k_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            c1_l_y = NULL;
            sf_mex_assign(&c1_l_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                              c1_k_y, 14, sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                               14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
              "message", 1U, 1U, 14, c1_l_y)));
          }

          c1_i426 = c1_o_i->size[0];
          c1_o_i->size[0] = c1_i419;
          c1_emxEnsureCapacity_int32_T(chartInstance, c1_o_i, c1_i426,
            &c1_ud_emlrtRTEI);
          c1_b26 = (1 > c1_d_idx);
          if (c1_b26) {
            c1_i427 = 0;
          } else {
            c1_i427 = c1_d_idx;
          }

          c1_i428 = c1_r11->size[0];
          c1_r11->size[0] = c1_i427;
          c1_emxEnsureCapacity_int32_T(chartInstance, c1_r11, c1_i428,
            &c1_vd_emlrtRTEI);
          c1_v_loop_ub = c1_i427 - 1;
          for (c1_i429 = 0; c1_i429 <= c1_v_loop_ub; c1_i429++) {
            c1_r11->data[c1_i429] = 1 + c1_i429;
          }

          c1_b_matrixSize = c1_j->size[0];
          c1_indexSize[0] = 1;
          c1_indexSize[1] = c1_r11->size[0];
          c1_b_size1 = c1_b_matrixSize;
          if (c1_b_size1 != 1) {
            c1_m_b = false;
          } else {
            c1_m_b = true;
          }

          if (c1_m_b) {
            c1_b_nonSingletonDimFound = false;
            if (c1_indexSize[1] != 1) {
              c1_b_nonSingletonDimFound = true;
            }

            c1_n_b = c1_b_nonSingletonDimFound;
            if (c1_n_b) {
              c1_e_c = true;
            } else {
              c1_e_c = false;
            }
          } else {
            c1_e_c = false;
          }

          c1_f_c = c1_e_c;
          if (!c1_f_c) {
          } else {
            c1_m_y = NULL;
            sf_mex_assign(&c1_m_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            c1_n_y = NULL;
            sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                              c1_m_y, 14, sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                               14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
              "message", 1U, 1U, 14, c1_n_y)));
          }

          c1_i430 = c1_j->size[0];
          c1_j->size[0] = c1_i427;
          c1_emxEnsureCapacity_int32_T(chartInstance, c1_j, c1_i430,
            &c1_wd_emlrtRTEI);
          c1_b27 = (1 > c1_d_idx);
          if (c1_b27) {
            c1_i431 = 0;
          } else {
            c1_i431 = c1_d_idx;
          }

          c1_i432 = c1_r11->size[0];
          c1_r11->size[0] = c1_i431;
          c1_emxEnsureCapacity_int32_T(chartInstance, c1_r11, c1_i432,
            &c1_xd_emlrtRTEI);
          c1_w_loop_ub = c1_i431 - 1;
          for (c1_i433 = 0; c1_i433 <= c1_w_loop_ub; c1_i433++) {
            c1_r11->data[c1_i433] = 1 + c1_i433;
          }

          c1_c_matrixSize = c1_v->size[0];
          c1_indexSize[0] = 1;
          c1_indexSize[1] = c1_r11->size[0];
          c1_c_size1 = c1_c_matrixSize;
          c1_emxFree_int32_T(chartInstance, &c1_r11);
          if (c1_c_size1 != 1) {
            c1_o_b = false;
          } else {
            c1_o_b = true;
          }

          if (c1_o_b) {
            c1_c_nonSingletonDimFound = false;
            if (c1_indexSize[1] != 1) {
              c1_c_nonSingletonDimFound = true;
            }

            c1_p_b = c1_c_nonSingletonDimFound;
            if (c1_p_b) {
              c1_g_c = true;
            } else {
              c1_g_c = false;
            }
          } else {
            c1_g_c = false;
          }

          c1_h_c = c1_g_c;
          if (!c1_h_c) {
          } else {
            c1_o_y = NULL;
            sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            c1_p_y = NULL;
            sf_mex_assign(&c1_p_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2,
              1, 30), false);
            sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                              c1_o_y, 14, sf_mex_call_debug
                              (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                               14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
              "message", 1U, 1U, 14, c1_p_y)));
          }
        }

        c1_emxFree_boolean_T(chartInstance, &c1_v);
      }

      c1_emxFree_boolean_T(chartInstance, &c1_n_x);
      c1_emxInit_real_T1(chartInstance, &c1_idxStrongR, 1, &c1_ae_emlrtRTEI);
      c1_i411 = c1_idxStrongR->size[0];
      c1_idxStrongR->size[0] = c1_o_i->size[0];
      c1_emxEnsureCapacity_real_T1(chartInstance, c1_idxStrongR, c1_i411,
        &c1_pd_emlrtRTEI);
      c1_r_loop_ub = c1_o_i->size[0] - 1;
      for (c1_i413 = 0; c1_i413 <= c1_r_loop_ub; c1_i413++) {
        c1_idxStrongR->data[c1_i413] = (real_T)c1_o_i->data[c1_i413];
      }

      c1_emxFree_int32_T(chartInstance, &c1_o_i);
      c1_emxInit_real_T1(chartInstance, &c1_idxStrongC, 1, &c1_ae_emlrtRTEI);
      c1_i415 = c1_idxStrongC->size[0];
      c1_idxStrongC->size[0] = c1_j->size[0];
      c1_emxEnsureCapacity_real_T1(chartInstance, c1_idxStrongC, c1_i415,
        &c1_rd_emlrtRTEI);
      c1_s_loop_ub = c1_j->size[0] - 1;
      for (c1_i416 = 0; c1_i416 <= c1_s_loop_ub; c1_i416++) {
        c1_idxStrongC->data[c1_i416] = (real_T)c1_j->data[c1_i416];
      }

      c1_emxFree_int32_T(chartInstance, &c1_j);
      if (c1_idxStrongC->size[0] != 0) {
        c1_bwselect(chartInstance, c1_E, c1_idxStrongC, c1_idxStrongR,
                    c1_varargout_1);
      } else {
        c1_i418 = c1_varargout_1->size[0] * c1_varargout_1->size[1];
        c1_varargout_1->size[0] = (int32_T)c1_c_m;
        c1_varargout_1->size[1] = (int32_T)c1_e_n;
        c1_emxEnsureCapacity_boolean_T(chartInstance, c1_varargout_1, c1_i418,
          &c1_sd_emlrtRTEI);
        c1_i422 = c1_varargout_1->size[0];
        c1_i423 = c1_varargout_1->size[1];
        c1_t_loop_ub = (int32_T)c1_c_m * (int32_T)c1_e_n - 1;
        for (c1_i424 = 0; c1_i424 <= c1_t_loop_ub; c1_i424++) {
          c1_varargout_1->data[c1_i424] = false;
        }
      }

      c1_emxFree_real_T(chartInstance, &c1_idxStrongC);
      c1_emxFree_real_T(chartInstance, &c1_idxStrongR);
      c1_emxFree_boolean_T(chartInstance, &c1_E);
    }

    c1_emxFree_real32_T(chartInstance, &c1_i1);
    c1_emxFree_real32_T(chartInstance, &c1_b_a);
    (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_lowThresh_size[0]);
    (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_highThreshTemp_size[0]);
  }

  c1_emxFree_real32_T(chartInstance, &c1_a);
}

static void c1_error(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_cv23[30] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'E', 'l', 'F', 'u', 'n', 'D', 'o', 'm', 'a', 'i', 'n',
    'E', 'r', 'r', 'o', 'r' };

  const mxArray *c1_b_y = NULL;
  const mxArray *c1_c_y = NULL;
  static char_T c1_cv24[4] = { 's', 'q', 'r', 't' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv23, 10, 0U, 1U, 0U, 2, 1, 30),
                false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv23, 10, 0U, 1U, 0U, 2, 1, 30),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv24, 10, 0U, 1U, 0U, 2, 1, 4),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
    1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U, 2U,
    14, c1_b_y, 14, c1_c_y)));
}

static void c1_padImage(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T *c1_a_tmp, real_T c1_pad[2], c1_emxArray_real32_T *c1_a)
{
  boolean_T c1_p;
  int32_T c1_k;
  real_T c1_b_k;
  real_T c1_x;
  boolean_T c1_b28;
  real_T c1_b_x;
  boolean_T c1_b;
  boolean_T c1_b_p;
  const mxArray *c1_y = NULL;
  static char_T c1_cv25[30] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'p', 'a', 'd',
    'a', 'r', 'r', 'a', 'y', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N',
    'o', 'n', 'N', 'a', 'N' };

  int32_T c1_i434;
  const mxArray *c1_b_y = NULL;
  const mxArray *c1_c_y = NULL;
  real_T c1_b_pad[2];
  static char_T c1_cv26[24] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'P', 'A', 'D', 'S', 'I', 'Z', 'E', ',' };

  boolean_T c1_b29;
  const mxArray *c1_d_y = NULL;
  static char_T c1_cv27[31] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'p', 'a', 'd',
    'a', 'r', 'r', 'a', 'y', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'I',
    'n', 't', 'e', 'g', 'e', 'r' };

  boolean_T c1_b30;
  const mxArray *c1_e_y = NULL;
  boolean_T c1_b31;
  const mxArray *c1_f_y = NULL;
  int32_T c1_i435;
  int32_T c1_i436;
  static char_T c1_cv28[24] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'P', 'A', 'D', 'S', 'I', 'Z', 'E', ',' };

  int32_T c1_i437;
  real_T c1_sizeA[2];
  int32_T c1_i438;
  real_T c1_b_b[2];
  int32_T c1_i439;
  int32_T c1_i440;
  int32_T c1_i441;
  real_T c1_b_sizeA[2];
  real_T c1_varargin_1[2];
  int32_T c1_i442;
  int32_T c1_c_k;
  real_T c1_c_x;
  boolean_T c1_c_p;
  real_T c1_d_k;
  boolean_T c1_c_b;
  real_T c1_maxval;
  real_T c1_d_x;
  real_T c1_e_x;
  c1_emxArray_int32_T *c1_idxA;
  int32_T c1_e_k;
  boolean_T c1_d_b;
  boolean_T c1_e_b;
  int32_T c1_outsize[2];
  boolean_T c1_b32;
  int32_T c1_i443;
  boolean_T c1_d_p;
  real_T c1_f_k;
  const mxArray *c1_g_y = NULL;
  static char_T c1_cv29[57] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'a', 's', 's', 'e', 'r', 't', '_',
    'v', 'a', 'l', 'i', 'd', '_', 's', 'i', 'z', 'e', '_', 'a', 'r', 'g', '_',
    'i', 'n', 'v', 'a', 'l', 'i', 'd', 'S', 'i', 'z', 'e', 'V', 'e', 'c', 't',
    'o', 'r' };

  real_T c1_n;
  const mxArray *c1_h_y = NULL;
  int32_T c1_g_k;
  int32_T c1_onesVector_size[2];
  int32_T c1_b_u;
  int32_T c1_i444;
  const mxArray *c1_i_y = NULL;
  real_T c1_h_k;
  int32_T c1_i445;
  const mxArray *c1_j_y = NULL;
  int32_T c1_loop_ub;
  int32_T c1_c_u;
  static char_T c1_cv30[21] = { 'C', 'o', 'd', 'e', 'r', ':', 'M', 'A', 'T', 'L',
    'A', 'B', ':', 'p', 'm', 'a', 'x', 's', 'i', 'z', 'e' };

  c1_emxArray_boolean_T *c1_tile;
  int32_T c1_i446;
  const mxArray *c1_k_y = NULL;
  const mxArray *c1_l_y = NULL;
  int32_T c1_i447;
  c1_emxArray_real_T *c1_m_y;
  real_T c1_d14;
  real_T c1_d;
  int32_T c1_i448;
  real_T c1_f_b;
  int32_T c1_i449;
  real_T c1_f_x;
  uint32_T c1_u0;
  int32_T c1_b_loop_ub;
  boolean_T c1_g_b;
  int32_T c1_i450;
  int32_T c1_i451;
  uint32_T c1_onesVector_data[6];
  real_T c1_g_x;
  int32_T c1_i452;
  boolean_T c1_h_b;
  const mxArray *c1_n_y = NULL;
  int32_T c1_i453;
  int32_T c1_i454;
  int32_T c1_i455;
  int32_T c1_i456;
  int32_T c1_i457;
  int32_T c1_i458;
  const mxArray *c1_o_y = NULL;
  real_T c1_b_a;
  int32_T c1_i459;
  int32_T c1_c_loop_ub;
  int32_T c1_i460;
  int32_T c1_y_size[2];
  int32_T c1_i461;
  int32_T c1_i462;
  int32_T c1_i463;
  int32_T c1_i464;
  int32_T c1_i465;
  int32_T c1_d_loop_ub;
  int32_T c1_i466;
  int32_T c1_i467;
  int32_T c1_e_loop_ub;
  int32_T c1_i468;
  c1_emxArray_uint32_T *c1_idxDir;
  real_T c1_d15;
  int32_T c1_i469;
  uint32_T c1_u1;
  int32_T c1_f_loop_ub;
  int32_T c1_i470;
  uint32_T c1_y_data[6];
  int32_T c1_g_loop_ub;
  int32_T c1_i471;
  int32_T c1_h_loop_ub;
  real_T c1_d16;
  int32_T c1_i472;
  uint32_T c1_u2;
  real_T c1_d17;
  boolean_T c1_b33;
  int32_T c1_i473;
  c1_emxArray_int32_T *c1_r12;
  int32_T c1_i474;
  int32_T c1_i_loop_ub;
  int32_T c1_i475;
  c1_emxArray_int32_T *c1_b_idxDir;
  int32_T c1_iv4[1];
  int32_T c1_i476;
  int32_T c1_i477;
  int32_T c1_i478;
  int32_T c1_j_loop_ub;
  int32_T c1_i479;
  int32_T c1_iv5[1];
  int32_T c1_k_loop_ub;
  int32_T c1_i480;
  int32_T c1_i481;
  int32_T c1_i482;
  int32_T c1_l_loop_ub;
  int32_T c1_i483;
  real_T c1_b_d;
  real_T c1_d18;
  real_T c1_i_b;
  real_T c1_h_x;
  boolean_T c1_j_b;
  uint32_T c1_u3;
  int32_T c1_i484;
  real_T c1_i_x;
  int32_T c1_i485;
  boolean_T c1_k_b;
  int32_T c1_i486;
  int32_T c1_i487;
  int32_T c1_i488;
  int32_T c1_i489;
  int32_T c1_i490;
  int32_T c1_i491;
  real_T c1_c_a;
  int32_T c1_m_loop_ub;
  int32_T c1_i492;
  int32_T c1_i493;
  int32_T c1_i494;
  int32_T c1_i495;
  int32_T c1_i496;
  int32_T c1_n_loop_ub;
  int32_T c1_i497;
  int32_T c1_i498;
  real_T c1_d19;
  uint32_T c1_u4;
  int32_T c1_o_loop_ub;
  int32_T c1_i499;
  int32_T c1_p_loop_ub;
  int32_T c1_i500;
  int32_T c1_q_loop_ub;
  real_T c1_d20;
  int32_T c1_i501;
  uint32_T c1_u5;
  real_T c1_d21;
  boolean_T c1_b34;
  int32_T c1_i502;
  int32_T c1_i503;
  int32_T c1_r_loop_ub;
  int32_T c1_i504;
  int32_T c1_iv6[1];
  int32_T c1_i505;
  int32_T c1_i506;
  int32_T c1_i507;
  int32_T c1_s_loop_ub;
  int32_T c1_i508;
  int32_T c1_iv7[1];
  int32_T c1_t_loop_ub;
  int32_T c1_i509;
  int32_T c1_i510;
  int32_T c1_i511;
  int32_T c1_i512;
  int32_T c1_i513;
  real_T c1_d22;
  int32_T c1_i514;
  int32_T c1_j;
  real_T c1_b_j;
  real_T c1_d23;
  int32_T c1_i515;
  int32_T c1_i;
  real_T c1_b_i;
  boolean_T exitg1;
  int32_T exitg2;
  c1_p = true;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k < 2)) {
    c1_b_k = 1.0 + (real_T)c1_k;
    c1_x = c1_pad[(int32_T)c1_b_k - 1];
    c1_b_x = c1_x;
    c1_b = muDoubleScalarIsNaN(c1_b_x);
    c1_b_p = !c1_b;
    if (c1_b_p) {
      c1_k++;
    } else {
      c1_p = false;
      exitg1 = true;
    }
  }

  if (c1_p) {
    c1_b28 = true;
  } else {
    c1_b28 = false;
  }

  if (c1_b28) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv25, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv4, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv26, 10, 0U, 1U, 0U, 2, 1, 24),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c1_b_y, 14, c1_c_y)));
  }

  for (c1_i434 = 0; c1_i434 < 2; c1_i434++) {
    c1_b_pad[c1_i434] = c1_pad[c1_i434];
  }

  if (c1_all(chartInstance, c1_b_pad)) {
    c1_b29 = true;
  } else {
    c1_b29 = false;
  }

  if (c1_b29) {
  } else {
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv27, 10, 0U, 1U, 0U, 2, 1, 31),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv5, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv28, 10, 0U, 1U, 0U, 2, 1, 24),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_e_y, 14, c1_f_y)));
  }

  c1_b30 = (c1_a_tmp->size[0] == 0);
  c1_b31 = (c1_a_tmp->size[1] == 0);
  if (c1_b30 || c1_b31) {
    for (c1_i436 = 0; c1_i436 < 2; c1_i436++) {
      c1_sizeA[c1_i436] = (real_T)c1_a_tmp->size[c1_i436];
    }

    for (c1_i438 = 0; c1_i438 < 2; c1_i438++) {
      c1_sizeA[c1_i438];
    }

    for (c1_i439 = 0; c1_i439 < 2; c1_i439++) {
      c1_b_b[c1_i439] = 2.0 * c1_pad[c1_i439];
    }

    for (c1_i441 = 0; c1_i441 < 2; c1_i441++) {
      c1_sizeA[c1_i441] += c1_b_b[c1_i441];
    }

    c1_varargin_1[0] = c1_sizeA[0];
    c1_varargin_1[1] = c1_sizeA[1];
    c1_c_k = 0;
    do {
      exitg2 = 0;
      if (c1_c_k < 2) {
        c1_d_k = 1.0 + (real_T)c1_c_k;
        if (c1_varargin_1[(int32_T)c1_d_k - 1] != c1_varargin_1[(int32_T)c1_d_k
            - 1]) {
          c1_c_p = false;
          exitg2 = 1;
        } else {
          c1_d_x = c1_varargin_1[(int32_T)c1_d_k - 1];
          c1_d_b = muDoubleScalarIsInf(c1_d_x);
          if (c1_d_b) {
            c1_c_p = false;
            exitg2 = 1;
          } else {
            c1_c_k++;
          }
        }
      } else {
        c1_c_p = true;
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    if (c1_c_p) {
      c1_e_k = 0;
      do {
        exitg2 = 0;
        if (c1_e_k < 2) {
          c1_f_k = 1.0 + (real_T)c1_e_k;
          if (c1_varargin_1[(int32_T)c1_f_k - 1] > 2.147483647E+9) {
            c1_d_p = false;
            exitg2 = 1;
          } else {
            c1_e_k++;
          }
        } else {
          c1_d_p = true;
          exitg2 = 1;
        }
      } while (exitg2 == 0);

      if (c1_d_p) {
        c1_b32 = true;
      } else {
        c1_b32 = false;
      }
    } else {
      c1_b32 = false;
    }

    if (c1_b32) {
    } else {
      c1_g_y = NULL;
      sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv29, 10, 0U, 1U, 0U, 2, 1,
        57), false);
      c1_h_y = NULL;
      sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv29, 10, 0U, 1U, 0U, 2, 1,
        57), false);
      c1_b_u = MIN_int32_T;
      c1_i_y = NULL;
      sf_mex_assign(&c1_i_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0),
                    false);
      c1_c_u = MAX_int32_T;
      c1_k_y = NULL;
      sf_mex_assign(&c1_k_y, sf_mex_create("y", &c1_c_u, 6, 0U, 0U, 0U, 0),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_g_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 3U, 14, c1_h_y, 14, c1_i_y, 14, c1_k_y)));
    }

    c1_n = 1.0;
    for (c1_g_k = 0; c1_g_k < 2; c1_g_k++) {
      c1_h_k = 1.0 + (real_T)c1_g_k;
      if (c1_varargin_1[(int32_T)c1_h_k - 1] <= 0.0) {
        c1_n = 0.0;
      } else {
        c1_n *= c1_varargin_1[(int32_T)c1_h_k - 1];
      }
    }

    if (c1_n <= 2.147483647E+9) {
    } else {
      c1_j_y = NULL;
      sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv30, 10, 0U, 1U, 0U, 2, 1,
        21), false);
      c1_l_y = NULL;
      sf_mex_assign(&c1_l_y, sf_mex_create("y", c1_cv30, 10, 0U, 1U, 0U, 2, 1,
        21), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_j_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_l_y)));
    }

    c1_emxInit_boolean_T(chartInstance, &c1_tile, 2, &c1_ee_emlrtRTEI);
    c1_i447 = c1_tile->size[0] * c1_tile->size[1];
    c1_tile->size[0] = (int32_T)c1_varargin_1[0];
    c1_tile->size[1] = (int32_T)c1_varargin_1[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_tile, c1_i447,
      &c1_ee_emlrtRTEI);
    c1_i448 = c1_tile->size[0];
    c1_i449 = c1_tile->size[1];
    c1_b_loop_ub = (int32_T)c1_varargin_1[0] * (int32_T)c1_varargin_1[1] - 1;
    for (c1_i450 = 0; c1_i450 <= c1_b_loop_ub; c1_i450++) {
      c1_tile->data[c1_i450] = false;
    }

    c1_outsize[0] = c1_tile->size[0];
    if ((real_T)c1_outsize[0] == (real_T)c1_tile->size[0]) {
    } else {
      c1_n_y = NULL;
      sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv6, 10, 0U, 1U, 0U, 2, 1, 15),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c1_n_y);
    }

    c1_outsize[1] = c1_tile->size[1];
    if ((real_T)c1_outsize[1] == (real_T)c1_tile->size[1]) {
    } else {
      c1_o_y = NULL;
      sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv6, 10, 0U, 1U, 0U, 2, 1, 15),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c1_o_y);
    }

    c1_emxFree_boolean_T(chartInstance, &c1_tile);
    c1_i459 = c1_a->size[0] * c1_a->size[1];
    c1_a->size[0] = c1_outsize[0];
    c1_a->size[1] = c1_outsize[1];
    c1_emxEnsureCapacity_real32_T(chartInstance, c1_a, c1_i459, &c1_ge_emlrtRTEI);
    c1_i465 = c1_a->size[0];
    c1_i466 = c1_a->size[1];
    c1_e_loop_ub = c1_outsize[0] * c1_outsize[1] - 1;
    for (c1_i468 = 0; c1_i468 <= c1_e_loop_ub; c1_i468++) {
      c1_a->data[c1_i468] = 0.0F;
    }
  } else {
    for (c1_i435 = 0; c1_i435 < 2; c1_i435++) {
      c1_sizeA[c1_i435] = (real_T)c1_a_tmp->size[c1_i435];
    }

    for (c1_i437 = 0; c1_i437 < 2; c1_i437++) {
      c1_sizeA[c1_i437];
    }

    c1_b_b[0] = c1_pad[0];
    c1_b_b[1] = c1_pad[1];
    for (c1_i440 = 0; c1_i440 < 2; c1_i440++) {
      c1_b_b[c1_i440] *= 2.0;
    }

    c1_b_sizeA[0] = c1_sizeA[0];
    c1_b_sizeA[1] = c1_sizeA[1];
    for (c1_i442 = 0; c1_i442 < 2; c1_i442++) {
      c1_varargin_1[c1_i442] = c1_b_b[c1_i442] + c1_b_sizeA[c1_i442];
    }

    if (c1_varargin_1[0] < c1_varargin_1[1]) {
      c1_maxval = c1_varargin_1[1];
    } else {
      c1_c_x = c1_varargin_1[0];
      c1_c_b = muDoubleScalarIsNaN(c1_c_x);
      if (c1_c_b) {
        c1_e_x = c1_varargin_1[1];
        c1_e_b = muDoubleScalarIsNaN(c1_e_x);
        if (!c1_e_b) {
          c1_maxval = c1_varargin_1[1];
        } else {
          c1_maxval = c1_varargin_1[0];
        }
      } else {
        c1_maxval = c1_varargin_1[0];
      }
    }

    c1_emxInit_int32_T1(chartInstance, &c1_idxA, 2, &c1_ce_emlrtRTEI);
    c1_outsize[0] = (int32_T)sf_integer_check(chartInstance->S, 1U, 0U, 0U,
      c1_maxval);
    c1_outsize[1] = 2;
    c1_i443 = c1_idxA->size[0] * c1_idxA->size[1];
    c1_idxA->size[0] = c1_outsize[0];
    c1_idxA->size[1] = c1_outsize[1];
    c1_emxEnsureCapacity_int32_T1(chartInstance, c1_idxA, c1_i443,
      &c1_ce_emlrtRTEI);
    c1_onesVector_size[0] = 1;
    c1_onesVector_size[1] = (int32_T)c1_pad[0];
    c1_i444 = c1_onesVector_size[0];
    c1_i445 = c1_onesVector_size[1];
    c1_loop_ub = (int32_T)c1_pad[0] - 1;
    for (c1_i446 = 0; c1_i446 <= c1_loop_ub; c1_i446++) {
      c1_d14 = muDoubleScalarRound(1.0);
      if (c1_d14 < 4.294967296E+9) {
        if (c1_d14 >= 0.0) {
          c1_u0 = (uint32_T)c1_d14;
        } else {
          c1_u0 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c1_d14 >= 4.294967296E+9) {
        c1_u0 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c1_u0 = 0U;
      }

      c1_onesVector_data[c1_i446] = c1_u0;
    }

    c1_emxInit_real_T(chartInstance, &c1_m_y, 2, &c1_ne_emlrtRTEI);
    c1_d = c1_sizeA[0];
    c1_f_b = c1_d;
    c1_f_x = c1_f_b;
    c1_g_b = muDoubleScalarIsNaN(c1_f_x);
    if (c1_g_b) {
      c1_i451 = c1_m_y->size[0] * c1_m_y->size[1];
      c1_m_y->size[0] = 1;
      c1_m_y->size[1] = 1;
      c1_emxEnsureCapacity_real_T(chartInstance, c1_m_y, c1_i451,
        &c1_fe_emlrtRTEI);
      c1_i454 = c1_m_y->size[0];
      c1_i458 = c1_m_y->size[1];
      c1_m_y->data[0] = rtNaN;
    } else if (c1_f_b < 1.0) {
      c1_i452 = c1_m_y->size[0] * c1_m_y->size[1];
      c1_m_y->size[0] = 1;
      c1_m_y->size[1] = 0;
      c1_i453 = c1_m_y->size[0];
      c1_i457 = c1_m_y->size[1];
    } else {
      c1_g_x = c1_f_b;
      c1_h_b = muDoubleScalarIsInf(c1_g_x);
      if (c1_h_b && (1.0 == c1_f_b)) {
        c1_i456 = c1_m_y->size[0] * c1_m_y->size[1];
        c1_m_y->size[0] = 1;
        c1_m_y->size[1] = 1;
        c1_emxEnsureCapacity_real_T(chartInstance, c1_m_y, c1_i456,
          &c1_fe_emlrtRTEI);
        c1_i460 = c1_m_y->size[0];
        c1_i462 = c1_m_y->size[1];
        c1_m_y->data[0] = rtNaN;
      } else {
        c1_i455 = c1_m_y->size[0] * c1_m_y->size[1];
        c1_m_y->size[0] = 1;
        c1_m_y->size[1] = (int32_T)muDoubleScalarFloor(c1_f_b - 1.0) + 1;
        c1_emxEnsureCapacity_real_T(chartInstance, c1_m_y, c1_i455,
          &c1_fe_emlrtRTEI);
        c1_c_loop_ub = (int32_T)muDoubleScalarFloor(c1_f_b - 1.0);
        for (c1_i461 = 0; c1_i461 <= c1_c_loop_ub; c1_i461++) {
          c1_m_y->data[c1_i461] = 1.0 + (real_T)c1_i461;
        }
      }
    }

    c1_b_a = c1_sizeA[0];
    c1_y_size[0] = 1;
    c1_y_size[1] = c1_onesVector_size[1];
    c1_i463 = c1_y_size[0];
    c1_i464 = c1_y_size[1];
    c1_d_loop_ub = c1_onesVector_size[0] * c1_onesVector_size[1] - 1;
    for (c1_i467 = 0; c1_i467 <= c1_d_loop_ub; c1_i467++) {
      c1_d15 = muDoubleScalarRound(c1_b_a * (real_T)c1_onesVector_data[c1_i467]);
      if (c1_d15 < 4.294967296E+9) {
        if (c1_d15 >= 0.0) {
          c1_u1 = (uint32_T)c1_d15;
        } else {
          c1_u1 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c1_d15 >= 4.294967296E+9) {
        c1_u1 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c1_u1 = 0U;
      }

      c1_y_data[c1_i467] = c1_u1;
    }

    c1_emxInit_uint32_T(chartInstance, &c1_idxDir, 2, &c1_le_emlrtRTEI);
    c1_i469 = c1_idxDir->size[0] * c1_idxDir->size[1];
    c1_idxDir->size[0] = 1;
    c1_idxDir->size[1] = (c1_onesVector_size[1] + c1_m_y->size[1]) + c1_y_size[1];
    c1_emxEnsureCapacity_uint32_T(chartInstance, c1_idxDir, c1_i469,
      &c1_he_emlrtRTEI);
    c1_f_loop_ub = c1_onesVector_size[1] - 1;
    for (c1_i470 = 0; c1_i470 <= c1_f_loop_ub; c1_i470++) {
      c1_idxDir->data[c1_i470] = c1_onesVector_data[c1_i470];
    }

    c1_g_loop_ub = c1_m_y->size[1] - 1;
    for (c1_i471 = 0; c1_i471 <= c1_g_loop_ub; c1_i471++) {
      c1_d16 = muDoubleScalarRound(c1_m_y->data[c1_i471]);
      if (c1_d16 < 4.294967296E+9) {
        if (c1_d16 >= 0.0) {
          c1_u2 = (uint32_T)c1_d16;
        } else {
          c1_u2 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c1_d16 >= 4.294967296E+9) {
        c1_u2 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c1_u2 = 0U;
      }

      c1_idxDir->data[c1_i471 + c1_onesVector_size[1]] = c1_u2;
    }

    c1_h_loop_ub = c1_y_size[1] - 1;
    for (c1_i472 = 0; c1_i472 <= c1_h_loop_ub; c1_i472++) {
      c1_idxDir->data[(c1_i472 + c1_onesVector_size[1]) + c1_m_y->size[1]] =
        c1_y_data[c1_i472];
    }

    c1_d17 = (real_T)c1_idxDir->size[1];
    c1_b33 = (1.0 > c1_d17);
    if (c1_b33) {
      c1_i473 = 0;
    } else {
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_idxA->size[0]);
      c1_i473 = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_d17, 1,
        c1_idxA->size[0]);
    }

    c1_emxInit_int32_T(chartInstance, &c1_r12, 1, &c1_me_emlrtRTEI);
    c1_i474 = c1_r12->size[0];
    c1_r12->size[0] = c1_i473;
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r12, c1_i474,
      &c1_ie_emlrtRTEI);
    c1_i_loop_ub = c1_i473 - 1;
    for (c1_i475 = 0; c1_i475 <= c1_i_loop_ub; c1_i475++) {
      c1_r12->data[c1_i475] = c1_i475;
    }

    c1_emxInit_int32_T1(chartInstance, &c1_b_idxDir, 2, &c1_je_emlrtRTEI);
    c1_iv4[0] = c1_r12->size[0];
    _SFD_SUB_ASSIGN_SIZE_CHECK_ND(c1_iv4, 1, *(int32_T (*)[2])c1_idxDir->size, 2);
    c1_i476 = c1_b_idxDir->size[0] * c1_b_idxDir->size[1];
    c1_b_idxDir->size[0] = 1;
    c1_b_idxDir->size[1] = c1_idxDir->size[1];
    c1_emxEnsureCapacity_int32_T1(chartInstance, c1_b_idxDir, c1_i476,
      &c1_je_emlrtRTEI);
    c1_i477 = c1_b_idxDir->size[0];
    c1_i478 = c1_b_idxDir->size[1];
    c1_j_loop_ub = c1_idxDir->size[0] * c1_idxDir->size[1] - 1;
    for (c1_i479 = 0; c1_i479 <= c1_j_loop_ub; c1_i479++) {
      c1_b_idxDir->data[c1_i479] = (int32_T)c1_idxDir->data[c1_i479];
    }

    c1_iv5[0] = c1_r12->size[0];
    c1_k_loop_ub = c1_iv5[0] - 1;
    for (c1_i480 = 0; c1_i480 <= c1_k_loop_ub; c1_i480++) {
      c1_idxA->data[c1_r12->data[c1_i480]] = c1_b_idxDir->data[c1_i480];
    }

    c1_onesVector_size[0] = 1;
    c1_onesVector_size[1] = (int32_T)c1_pad[1];
    c1_i481 = c1_onesVector_size[0];
    c1_i482 = c1_onesVector_size[1];
    c1_l_loop_ub = (int32_T)c1_pad[1] - 1;
    for (c1_i483 = 0; c1_i483 <= c1_l_loop_ub; c1_i483++) {
      c1_d18 = muDoubleScalarRound(1.0);
      if (c1_d18 < 4.294967296E+9) {
        if (c1_d18 >= 0.0) {
          c1_u3 = (uint32_T)c1_d18;
        } else {
          c1_u3 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c1_d18 >= 4.294967296E+9) {
        c1_u3 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c1_u3 = 0U;
      }

      c1_onesVector_data[c1_i483] = c1_u3;
    }

    c1_b_d = c1_sizeA[1];
    c1_i_b = c1_b_d;
    c1_h_x = c1_i_b;
    c1_j_b = muDoubleScalarIsNaN(c1_h_x);
    if (c1_j_b) {
      c1_i484 = c1_m_y->size[0] * c1_m_y->size[1];
      c1_m_y->size[0] = 1;
      c1_m_y->size[1] = 1;
      c1_emxEnsureCapacity_real_T(chartInstance, c1_m_y, c1_i484,
        &c1_fe_emlrtRTEI);
      c1_i487 = c1_m_y->size[0];
      c1_i491 = c1_m_y->size[1];
      c1_m_y->data[0] = rtNaN;
    } else if (c1_i_b < 1.0) {
      c1_i485 = c1_m_y->size[0] * c1_m_y->size[1];
      c1_m_y->size[0] = 1;
      c1_m_y->size[1] = 0;
      c1_i486 = c1_m_y->size[0];
      c1_i490 = c1_m_y->size[1];
    } else {
      c1_i_x = c1_i_b;
      c1_k_b = muDoubleScalarIsInf(c1_i_x);
      if (c1_k_b && (1.0 == c1_i_b)) {
        c1_i489 = c1_m_y->size[0] * c1_m_y->size[1];
        c1_m_y->size[0] = 1;
        c1_m_y->size[1] = 1;
        c1_emxEnsureCapacity_real_T(chartInstance, c1_m_y, c1_i489,
          &c1_fe_emlrtRTEI);
        c1_i492 = c1_m_y->size[0];
        c1_i494 = c1_m_y->size[1];
        c1_m_y->data[0] = rtNaN;
      } else {
        c1_i488 = c1_m_y->size[0] * c1_m_y->size[1];
        c1_m_y->size[0] = 1;
        c1_m_y->size[1] = (int32_T)muDoubleScalarFloor(c1_i_b - 1.0) + 1;
        c1_emxEnsureCapacity_real_T(chartInstance, c1_m_y, c1_i488,
          &c1_fe_emlrtRTEI);
        c1_m_loop_ub = (int32_T)muDoubleScalarFloor(c1_i_b - 1.0);
        for (c1_i493 = 0; c1_i493 <= c1_m_loop_ub; c1_i493++) {
          c1_m_y->data[c1_i493] = 1.0 + (real_T)c1_i493;
        }
      }
    }

    c1_c_a = c1_sizeA[1];
    c1_y_size[0] = 1;
    c1_y_size[1] = c1_onesVector_size[1];
    c1_i495 = c1_y_size[0];
    c1_i496 = c1_y_size[1];
    c1_n_loop_ub = c1_onesVector_size[0] * c1_onesVector_size[1] - 1;
    for (c1_i497 = 0; c1_i497 <= c1_n_loop_ub; c1_i497++) {
      c1_d19 = muDoubleScalarRound(c1_c_a * (real_T)c1_onesVector_data[c1_i497]);
      if (c1_d19 < 4.294967296E+9) {
        if (c1_d19 >= 0.0) {
          c1_u4 = (uint32_T)c1_d19;
        } else {
          c1_u4 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c1_d19 >= 4.294967296E+9) {
        c1_u4 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c1_u4 = 0U;
      }

      c1_y_data[c1_i497] = c1_u4;
    }

    c1_i498 = c1_idxDir->size[0] * c1_idxDir->size[1];
    c1_idxDir->size[0] = 1;
    c1_idxDir->size[1] = (c1_onesVector_size[1] + c1_m_y->size[1]) + c1_y_size[1];
    c1_emxEnsureCapacity_uint32_T(chartInstance, c1_idxDir, c1_i498,
      &c1_he_emlrtRTEI);
    c1_o_loop_ub = c1_onesVector_size[1] - 1;
    for (c1_i499 = 0; c1_i499 <= c1_o_loop_ub; c1_i499++) {
      c1_idxDir->data[c1_i499] = c1_onesVector_data[c1_i499];
    }

    c1_p_loop_ub = c1_m_y->size[1] - 1;
    for (c1_i500 = 0; c1_i500 <= c1_p_loop_ub; c1_i500++) {
      c1_d20 = muDoubleScalarRound(c1_m_y->data[c1_i500]);
      if (c1_d20 < 4.294967296E+9) {
        if (c1_d20 >= 0.0) {
          c1_u5 = (uint32_T)c1_d20;
        } else {
          c1_u5 = 0U;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
        }
      } else if (c1_d20 >= 4.294967296E+9) {
        c1_u5 = MAX_uint32_T;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1U, 0U, 0U);
      } else {
        c1_u5 = 0U;
      }

      c1_idxDir->data[c1_i500 + c1_onesVector_size[1]] = c1_u5;
    }

    c1_q_loop_ub = c1_y_size[1] - 1;
    for (c1_i501 = 0; c1_i501 <= c1_q_loop_ub; c1_i501++) {
      c1_idxDir->data[(c1_i501 + c1_onesVector_size[1]) + c1_m_y->size[1]] =
        c1_y_data[c1_i501];
    }

    c1_emxFree_real_T(chartInstance, &c1_m_y);
    c1_d21 = (real_T)c1_idxDir->size[1];
    c1_b34 = (1.0 > c1_d21);
    if (c1_b34) {
      c1_i502 = 0;
    } else {
      (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_idxA->size[0]);
      c1_i502 = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_d21, 1,
        c1_idxA->size[0]);
    }

    c1_i503 = c1_r12->size[0];
    c1_r12->size[0] = c1_i502;
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r12, c1_i503,
      &c1_ie_emlrtRTEI);
    c1_r_loop_ub = c1_i502 - 1;
    for (c1_i504 = 0; c1_i504 <= c1_r_loop_ub; c1_i504++) {
      c1_r12->data[c1_i504] = c1_i504;
    }

    c1_iv6[0] = c1_r12->size[0];
    _SFD_SUB_ASSIGN_SIZE_CHECK_ND(c1_iv6, 1, *(int32_T (*)[2])c1_idxDir->size, 2);
    c1_i505 = c1_b_idxDir->size[0] * c1_b_idxDir->size[1];
    c1_b_idxDir->size[0] = 1;
    c1_b_idxDir->size[1] = c1_idxDir->size[1];
    c1_emxEnsureCapacity_int32_T1(chartInstance, c1_b_idxDir, c1_i505,
      &c1_je_emlrtRTEI);
    c1_i506 = c1_b_idxDir->size[0];
    c1_i507 = c1_b_idxDir->size[1];
    c1_s_loop_ub = c1_idxDir->size[0] * c1_idxDir->size[1] - 1;
    for (c1_i508 = 0; c1_i508 <= c1_s_loop_ub; c1_i508++) {
      c1_b_idxDir->data[c1_i508] = (int32_T)c1_idxDir->data[c1_i508];
    }

    c1_emxFree_uint32_T(chartInstance, &c1_idxDir);
    c1_iv7[0] = c1_r12->size[0];
    c1_t_loop_ub = c1_iv7[0] - 1;
    for (c1_i509 = 0; c1_i509 <= c1_t_loop_ub; c1_i509++) {
      c1_idxA->data[c1_r12->data[c1_i509] + c1_idxA->size[0]] =
        c1_b_idxDir->data[c1_i509];
    }

    c1_emxFree_int32_T(chartInstance, &c1_b_idxDir);
    c1_emxFree_int32_T(chartInstance, &c1_r12);
    for (c1_i510 = 0; c1_i510 < 2; c1_i510++) {
      c1_varargin_1[c1_i510] = (real_T)c1_a_tmp->size[c1_i510];
    }

    for (c1_i511 = 0; c1_i511 < 2; c1_i511++) {
      c1_b_b[c1_i511] = 2.0 * c1_pad[c1_i511];
    }

    for (c1_i512 = 0; c1_i512 < 2; c1_i512++) {
      c1_varargin_1[c1_i512] = sf_integer_check(chartInstance->S, 1U, 0U, 0U,
        c1_varargin_1[c1_i512] + c1_b_b[c1_i512]);
    }

    c1_i513 = c1_a->size[0] * c1_a->size[1];
    c1_a->size[0] = (int32_T)c1_varargin_1[0];
    c1_a->size[1] = (int32_T)c1_varargin_1[1];
    c1_emxEnsureCapacity_real32_T(chartInstance, c1_a, c1_i513, &c1_ke_emlrtRTEI);
    c1_d22 = (real_T)c1_a->size[1];
    c1_i514 = (int32_T)c1_d22 - 1;
    for (c1_j = 0; c1_j <= c1_i514; c1_j++) {
      c1_b_j = 1.0 + (real_T)c1_j;
      c1_d23 = (real_T)c1_a->size[0];
      c1_i515 = (int32_T)c1_d23 - 1;
      for (c1_i = 0; c1_i <= c1_i515; c1_i++) {
        c1_b_i = 1.0 + (real_T)c1_i;
        c1_a->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_b_i, 1,
          c1_a->size[0]) + c1_a->size[0] * (sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_b_j, 1, c1_a->size[1]) - 1)) - 1] = c1_a_tmp->data
          [(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
             chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_idxA->
             data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
              chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_b_i, 1,
              c1_idxA->size[0]) - 1], 1, c1_a_tmp->size[0]) + c1_a_tmp->size[0] *
            (sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
              chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_idxA->data
              [(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
                chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_b_j, 1,
                c1_idxA->size[0]) + c1_idxA->size[0]) - 1], 1, c1_a_tmp->size[1])
             - 1)) - 1];
      }
    }

    c1_emxFree_int32_T(chartInstance, &c1_idxA);
  }
}

static boolean_T c1_all(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T
  c1_a[2])
{
  boolean_T c1_p;
  int32_T c1_k;
  real_T c1_b_k;
  real_T c1_x;
  real_T c1_b_x;
  real_T c1_c_x;
  boolean_T c1_b;
  boolean_T c1_b35;
  real_T c1_d_x;
  boolean_T c1_b_b;
  boolean_T c1_b36;
  boolean_T c1_c_b;
  real_T c1_e_x;
  boolean_T c1_b_p;
  real_T c1_f_x;
  boolean_T c1_c_p;
  boolean_T exitg1;
  (void)chartInstance;
  c1_p = true;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k < 2)) {
    c1_b_k = 1.0 + (real_T)c1_k;
    c1_x = c1_a[(int32_T)c1_b_k - 1];
    c1_b_x = c1_x;
    c1_c_x = c1_b_x;
    c1_b = muDoubleScalarIsInf(c1_c_x);
    c1_b35 = !c1_b;
    c1_d_x = c1_b_x;
    c1_b_b = muDoubleScalarIsNaN(c1_d_x);
    c1_b36 = !c1_b_b;
    c1_c_b = (c1_b35 && c1_b36);
    if (c1_c_b) {
      c1_e_x = c1_x;
      c1_f_x = c1_e_x;
      c1_f_x = muDoubleScalarFloor(c1_f_x);
      if (c1_f_x == c1_x) {
        c1_b_p = true;
      } else {
        c1_b_p = false;
      }
    } else {
      c1_b_p = false;
    }

    c1_c_p = c1_b_p;
    if (c1_c_p) {
      c1_k++;
    } else {
      c1_p = false;
      exitg1 = true;
    }
  }

  return c1_p;
}

static void c1_hypot(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_real32_T *c1_x, c1_emxArray_real32_T *c1_y,
                     c1_emxArray_real32_T *c1_r)
{
  c1_emxArray_real32_T *c1_r13;
  int32_T c1_sak;
  int32_T c1_sbk;
  int32_T c1_a;
  int32_T c1_b;
  int32_T c1_c;
  int32_T c1_csz[2];
  int32_T c1_b_a;
  int32_T c1_b_b;
  int32_T c1_b_c;
  c1_emxArray_real32_T *c1_ztemp;
  int32_T c1_i516;
  int32_T c1_i517;
  int32_T c1_i518;
  int32_T c1_i519;
  int32_T c1_i520;
  c1_emxArray_real32_T *c1_r14;
  int32_T c1_i521;
  int32_T c1_i522;
  int32_T c1_i523;
  int32_T c1_i524;
  int32_T c1_loop_ub;
  int32_T c1_i525;
  c1_emxArray_real32_T *c1_b_x;
  int32_T c1_i526;
  int32_T c1_i527;
  int32_T c1_i528;
  int32_T c1_b_loop_ub;
  int32_T c1_i529;
  c1_emxArray_real32_T *c1_b_y;
  int32_T c1_i530;
  int32_T c1_i531;
  int32_T c1_i532;
  int32_T c1_c_loop_ub;
  int32_T c1_i533;
  const mxArray *c1_c_y = NULL;
  static char_T c1_cv31[15] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'd', 'i', 'm',
    'a', 'g', 'r', 'e', 'e' };

  const mxArray *c1_d_y = NULL;
  int32_T c1_i534;
  int32_T c1_i535;
  int32_T c1_nx;
  int32_T c1_c_b;
  int32_T c1_d_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  real32_T c1_c_x;
  real32_T c1_e_y;
  real32_T c1_x1;
  real32_T c1_x2;
  real32_T c1_z;
  c1_emxInit_real32_T(chartInstance, &c1_r13, 2, &c1_qe_emlrtRTEI);
  c1_sak = c1_x->size[0];
  c1_sbk = c1_y->size[0];
  c1_a = c1_sak;
  c1_b = c1_sbk;
  if (c1_a <= c1_b) {
    c1_c = c1_a;
  } else {
    c1_c = c1_b;
  }

  c1_csz[0] = c1_c;
  c1_sak = c1_x->size[1];
  c1_sbk = c1_y->size[1];
  c1_b_a = c1_sak;
  c1_b_b = c1_sbk;
  if (c1_b_a <= c1_b_b) {
    c1_b_c = c1_b_a;
  } else {
    c1_b_c = c1_b_b;
  }

  c1_emxInit_real32_T(chartInstance, &c1_ztemp, 2, &c1_ue_emlrtRTEI);
  c1_csz[1] = c1_b_c;
  c1_i516 = c1_ztemp->size[0] * c1_ztemp->size[1];
  c1_ztemp->size[0] = c1_csz[0];
  c1_ztemp->size[1] = c1_csz[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_ztemp, c1_i516,
    &c1_oe_emlrtRTEI);
  c1_i517 = c1_r13->size[0] * c1_r13->size[1];
  c1_r13->size[0] = c1_ztemp->size[0];
  c1_r13->size[1] = c1_ztemp->size[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_r13, c1_i517, &c1_oe_emlrtRTEI);
  c1_emxFree_real32_T(chartInstance, &c1_ztemp);
  for (c1_i518 = 0; c1_i518 < 2; c1_i518++) {
    c1_csz[c1_i518] = c1_r13->size[c1_i518];
  }

  c1_i519 = c1_r13->size[0] * c1_r13->size[1];
  c1_r13->size[0] = c1_csz[0];
  c1_r13->size[1] = c1_csz[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_r13, c1_i519, &c1_pe_emlrtRTEI);
  for (c1_i520 = 0; c1_i520 < 2; c1_i520++) {
    c1_csz[c1_i520] = c1_r13->size[c1_i520];
  }

  c1_emxInit_real32_T(chartInstance, &c1_r14, 2, &c1_re_emlrtRTEI);
  c1_i521 = c1_r13->size[0] * c1_r13->size[1];
  c1_r13->size[0] = c1_csz[0];
  c1_r13->size[1] = c1_csz[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_r13, c1_i521, &c1_qe_emlrtRTEI);
  c1_i522 = c1_r14->size[0] * c1_r14->size[1];
  c1_r14->size[0] = c1_r13->size[0];
  c1_r14->size[1] = c1_r13->size[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_r14, c1_i522, &c1_re_emlrtRTEI);
  c1_i523 = c1_r14->size[0];
  c1_i524 = c1_r14->size[1];
  c1_loop_ub = c1_r13->size[0] * c1_r13->size[1] - 1;
  for (c1_i525 = 0; c1_i525 <= c1_loop_ub; c1_i525++) {
    c1_r14->data[c1_i525] = c1_r13->data[c1_i525];
  }

  c1_emxInit_real32_T(chartInstance, &c1_b_x, 2, &c1_se_emlrtRTEI);
  c1_i526 = c1_b_x->size[0] * c1_b_x->size[1];
  c1_b_x->size[0] = c1_x->size[0];
  c1_b_x->size[1] = c1_x->size[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_b_x, c1_i526, &c1_se_emlrtRTEI);
  c1_i527 = c1_b_x->size[0];
  c1_i528 = c1_b_x->size[1];
  c1_b_loop_ub = c1_x->size[0] * c1_x->size[1] - 1;
  for (c1_i529 = 0; c1_i529 <= c1_b_loop_ub; c1_i529++) {
    c1_b_x->data[c1_i529] = c1_x->data[c1_i529];
  }

  c1_emxInit_real32_T(chartInstance, &c1_b_y, 2, &c1_se_emlrtRTEI);
  c1_i530 = c1_b_y->size[0] * c1_b_y->size[1];
  c1_b_y->size[0] = c1_y->size[0];
  c1_b_y->size[1] = c1_y->size[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_b_y, c1_i530, &c1_se_emlrtRTEI);
  c1_i531 = c1_b_y->size[0];
  c1_i532 = c1_b_y->size[1];
  c1_c_loop_ub = c1_y->size[0] * c1_y->size[1] - 1;
  for (c1_i533 = 0; c1_i533 <= c1_c_loop_ub; c1_i533++) {
    c1_b_y->data[c1_i533] = c1_y->data[c1_i533];
  }

  if (c1_dimagree(chartInstance, c1_r14, c1_b_x, c1_b_y)) {
  } else {
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv31, 10, 0U, 1U, 0U, 2, 1, 15),
                  false);
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv31, 10, 0U, 1U, 0U, 2, 1, 15),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_d_y)));
  }

  c1_emxFree_real32_T(chartInstance, &c1_b_y);
  c1_emxFree_real32_T(chartInstance, &c1_b_x);
  c1_emxFree_real32_T(chartInstance, &c1_r14);
  for (c1_i534 = 0; c1_i534 < 2; c1_i534++) {
    c1_csz[c1_i534] = c1_r13->size[c1_i534];
  }

  c1_emxFree_real32_T(chartInstance, &c1_r13);
  c1_i535 = c1_r->size[0] * c1_r->size[1];
  c1_r->size[0] = c1_csz[0];
  c1_r->size[1] = c1_csz[1];
  c1_emxEnsureCapacity_real32_T(chartInstance, c1_r, c1_i535, &c1_te_emlrtRTEI);
  c1_nx = c1_r->size[0] * c1_r->size[1];
  c1_c_b = c1_nx;
  c1_d_b = c1_c_b;
  if (1 > c1_d_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_d_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_nx; c1_k++) {
    c1_b_k = c1_k;
    c1_c_x = c1_x->data[c1_b_k];
    c1_e_y = c1_y->data[c1_b_k];
    c1_x1 = c1_c_x;
    c1_x2 = c1_e_y;
    c1_z = muSingleScalarHypot(c1_x1, c1_x2);
    c1_r->data[c1_b_k] = c1_z;
  }
}

static boolean_T c1_dimagree(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T *c1_z, c1_emxArray_real32_T *c1_varargin_1,
  c1_emxArray_real32_T *c1_varargin_2)
{
  boolean_T c1_p;
  boolean_T c1_b_p;
  int32_T c1_k;
  int32_T c1_b_k;
  real_T c1_d24;
  boolean_T c1_c_p;
  real_T c1_d25;
  int32_T c1_c_k;
  int32_T c1_d_k;
  real_T c1_d26;
  real_T c1_d27;
  boolean_T exitg1;
  (void)chartInstance;
  c1_p = true;
  c1_b_p = true;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k < 2)) {
    c1_b_k = c1_k;
    c1_d24 = (real_T)c1_z->size[c1_b_k];
    c1_d25 = (real_T)c1_varargin_1->size[c1_b_k];
    if ((int32_T)c1_d24 != (int32_T)c1_d25) {
      c1_b_p = false;
      exitg1 = true;
    } else {
      c1_k++;
    }
  }

  if (c1_b_p) {
    c1_c_p = true;
    c1_c_k = 0;
    exitg1 = false;
    while ((!exitg1) && (c1_c_k < 2)) {
      c1_d_k = c1_c_k;
      c1_d26 = (real_T)c1_z->size[c1_d_k];
      c1_d27 = (real_T)c1_varargin_2->size[c1_d_k];
      if ((int32_T)c1_d26 != (int32_T)c1_d27) {
        c1_c_p = false;
        exitg1 = true;
      } else {
        c1_c_k++;
      }
    }

    if (c1_c_p) {
    } else {
      c1_p = false;
    }
  } else {
    c1_p = false;
  }

  return c1_p;
}

static void c1_warning(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_cv32[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c1_b_y = NULL;
  static char_T c1_cv33[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c1_c_y = NULL;
  static char_T c1_msgID[25] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm',
    'h', 'i', 's', 't', 'c', ':', 'o', 'u', 't', 'O', 'f', 'R', 'a', 'n', 'g',
    'e' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv32, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv33, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_msgID, 10, 0U, 1U, 0U, 2, 1, 25),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c1_b_y, 14, c1_c_y));
}

static void c1_b_warning(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_cv34[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c1_b_y = NULL;
  static char_T c1_cv35[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c1_c_y = NULL;
  static char_T c1_msgID[27] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm',
    'h', 'i', 's', 't', 'c', ':', 'i', 'n', 'p', 'u', 't', 'H', 'a', 's', 'N',
    'a', 'N', 's' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv34, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv35, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_msgID, 10, 0U, 1U, 0U, 2, 1, 27),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c1_b_y, 14, c1_c_y));
}

static void c1_bwselect(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T *c1_varargin_2,
  c1_emxArray_real_T *c1_varargin_3, c1_emxArray_boolean_T *c1_varargout_1)
{
  c1_emxArray_real_T *c1_r;
  int32_T c1_i536;
  int32_T c1_loop_ub;
  int32_T c1_i537;
  c1_emxArray_real_T *c1_c;
  int32_T c1_i538;
  int32_T c1_b_loop_ub;
  int32_T c1_i539;
  real_T c1_b_varargin_1;
  real_T c1_c_varargin_1;
  boolean_T c1_p;
  real_T c1_x1;
  boolean_T c1_b_p;
  real_T c1_b_x1;
  boolean_T c1_c_p;
  c1_emxArray_boolean_T *c1_BW;
  c1_emxArray_real_T *c1_tmp;
  boolean_T c1_column_BW;
  int32_T c1_i540;
  int32_T c1_i541;
  int32_T c1_i542;
  int32_T c1_c_loop_ub;
  int32_T c1_i543;
  int32_T c1_i544;
  int32_T c1_d_loop_ub;
  int32_T c1_i545;
  int32_T c1_i546;
  int32_T c1_e_loop_ub;
  int32_T c1_i547;
  c1_emxArray_boolean_T *c1_x;
  int32_T c1_f_loop_ub;
  int32_T c1_i548;
  int32_T c1_i549;
  int32_T c1_g_loop_ub;
  int32_T c1_i550;
  int32_T c1_i551;
  int32_T c1_h_loop_ub;
  c1_emxArray_boolean_T *c1_r15;
  int32_T c1_i552;
  real_T c1_b_BW;
  int32_T c1_i553;
  int32_T c1_i554;
  int32_T c1_i_loop_ub;
  int32_T c1_i555;
  int32_T c1_j_loop_ub;
  int32_T c1_i556;
  int32_T c1_i557;
  int32_T c1_k_loop_ub;
  int32_T c1_i558;
  int32_T c1_i559;
  int32_T c1_l_loop_ub;
  int32_T c1_i560;
  int32_T c1_i561;
  int32_T c1_m_loop_ub;
  int32_T c1_i562;
  real_T c1_c_BW;
  int32_T c1_i563;
  int32_T c1_n_loop_ub;
  int32_T c1_i564;
  int32_T c1_i565;
  int32_T c1_o_loop_ub;
  int32_T c1_i566;
  c1_emxArray_int32_T *c1_ii;
  int32_T c1_nx;
  int32_T c1_k;
  int32_T c1_b_nx;
  int32_T c1_idx;
  int32_T c1_i567;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_b_ii;
  int32_T c1_c_ii;
  const mxArray *c1_y = NULL;
  c1_emxArray_int32_T *c1_c_b;
  const mxArray *c1_b_y = NULL;
  boolean_T c1_b37;
  int32_T c1_i568;
  int32_T c1_i569;
  int32_T c1_i570;
  int32_T c1_i571;
  int32_T c1_p_loop_ub;
  int32_T c1_q_loop_ub;
  int32_T c1_i572;
  int32_T c1_i573;
  int32_T c1_matrixSize;
  c1_emxArray_int32_T *c1_b_tmp;
  int32_T c1_indexSize[2];
  int32_T c1_i574;
  int32_T c1_i575;
  int32_T c1_size1;
  int32_T c1_i576;
  real_T c1_d_BW[2];
  boolean_T c1_d_b;
  int32_T c1_r_loop_ub;
  int32_T c1_i577;
  int32_T c1_i578;
  boolean_T c1_nonSingletonDimFound;
  boolean_T c1_b_c;
  c1_emxArray_int32_T *c1_c_tmp;
  c1_emxArray_real_T *c1_b_r;
  boolean_T c1_c_c;
  boolean_T c1_e_b;
  int32_T c1_hi;
  int32_T c1_i579;
  int32_T c1_i580;
  const mxArray *c1_c_y = NULL;
  int32_T c1_i581;
  int32_T c1_s_loop_ub;
  const mxArray *c1_d_y = NULL;
  int32_T c1_t_loop_ub;
  int32_T c1_i582;
  int32_T c1_i583;
  const mxArray *c1_e_y = NULL;
  static char_T c1_cv36[30] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'u', 'b',
    '2', 'i', 'n', 'd', ':', 'I', 'n', 'd', 'e', 'x', 'O', 'u', 't', 'O', 'f',
    'R', 'a', 'n', 'g', 'e' };

  const mxArray *c1_f_y = NULL;
  real_T c1_b_varargin_2[2];
  int32_T c1_i584;
  int32_T c1_i585;
  int32_T c1_i586;
  int32_T c1_i587;
  boolean_T c1_d_p;
  int32_T c1_i588;
  int32_T c1_i589;
  boolean_T c1_e_p;
  int32_T c1_b_k;
  real_T c1_c_k;
  real_T c1_c_x1;
  real_T c1_x2;
  boolean_T c1_f_p;
  const mxArray *c1_g_y = NULL;
  static char_T c1_cv37[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'u', 'b',
    '2', 'i', 'n', 'd', ':', 'S', 'u', 'b', 's', 'c', 'r', 'i', 'p', 't', 'V',
    'e', 'c', 't', 'o', 'r', 'S', 'i', 'z', 'e' };

  int32_T c1_i590;
  const mxArray *c1_h_y = NULL;
  c1_emxArray_real_T *c1_d_c;
  int32_T c1_i591;
  int32_T c1_u_loop_ub;
  int32_T c1_i592;
  const mxArray *c1_i_y = NULL;
  const mxArray *c1_j_y = NULL;
  int32_T c1_psiz;
  int32_T c1_i593;
  int32_T c1_v_loop_ub;
  int32_T c1_i594;
  int32_T c1_a;
  int32_T c1_i595;
  int32_T c1_w_loop_ub;
  int32_T c1_i596;
  int32_T c1_i597;
  int32_T c1_x_loop_ub;
  int32_T c1_i598;
  int32_T c1_i599;
  int32_T c1_y_loop_ub;
  int32_T c1_i600;
  int32_T c1_i601;
  int32_T c1_ab_loop_ub;
  int32_T c1_i602;
  c1_emxArray_boolean_T *c1_BW2_temp;
  int32_T c1_i603;
  int32_T c1_i604;
  int32_T c1_i605;
  int32_T c1_bb_loop_ub;
  int32_T c1_i606;
  int32_T c1_i607;
  int32_T c1_i608;
  int32_T c1_b_BW2_temp[2];
  int32_T c1_e_BW[2];
  int32_T c1_i609;
  int32_T c1_i610;
  int32_T c1_i611;
  int32_T c1_i612;
  int32_T c1_i613;
  int32_T c1_i614;
  int32_T c1_i615;
  int32_T c1_cb_loop_ub;
  int32_T c1_i616;
  boolean_T c1_b38;
  boolean_T c1_b39;
  int32_T c1_c_nx;
  boolean_T c1_b40;
  boolean_T c1_b41;
  int32_T c1_i617;
  int32_T c1_i618;
  boolean_T c1_b42;
  int32_T c1_i619;
  int32_T c1_db_loop_ub;
  const mxArray *c1_k_y = NULL;
  int32_T c1_i620;
  int32_T c1_i621;
  int32_T c1_d_k;
  int32_T c1_eb_loop_ub;
  const mxArray *c1_l_y = NULL;
  int32_T c1_d_nx;
  int32_T c1_i622;
  int32_T c1_b_idx;
  int32_T c1_fb_loop_ub;
  int32_T c1_i623;
  int32_T c1_i624;
  int32_T c1_f_b;
  int32_T c1_g_b;
  boolean_T c1_b_overflow;
  int32_T c1_d_ii;
  int32_T c1_e_ii;
  const mxArray *c1_m_y = NULL;
  const mxArray *c1_n_y = NULL;
  boolean_T c1_b43;
  int32_T c1_i625;
  int32_T c1_i626;
  int32_T c1_gb_loop_ub;
  int32_T c1_i627;
  int32_T c1_b_matrixSize;
  int32_T c1_b_size1;
  boolean_T c1_h_b;
  boolean_T c1_b_nonSingletonDimFound;
  boolean_T c1_e_c;
  boolean_T c1_f_c;
  boolean_T c1_i_b;
  const mxArray *c1_o_y = NULL;
  const mxArray *c1_p_y = NULL;
  boolean_T exitg1;
  c1_emxInit_real_T1(chartInstance, &c1_r, 1, &c1_yf_emlrtRTEI);
  c1_i536 = c1_r->size[0];
  c1_r->size[0] = c1_varargin_3->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_r, c1_i536, &c1_ve_emlrtRTEI);
  c1_loop_ub = c1_varargin_3->size[0] - 1;
  for (c1_i537 = 0; c1_i537 <= c1_loop_ub; c1_i537++) {
    c1_r->data[c1_i537] = c1_varargin_3->data[c1_i537];
  }

  c1_emxInit_real_T1(chartInstance, &c1_c, 1, &c1_ag_emlrtRTEI);
  c1_d_round(chartInstance, c1_r);
  c1_i538 = c1_c->size[0];
  c1_c->size[0] = c1_varargin_2->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_c, c1_i538, &c1_we_emlrtRTEI);
  c1_b_loop_ub = c1_varargin_2->size[0] - 1;
  for (c1_i539 = 0; c1_i539 <= c1_b_loop_ub; c1_i539++) {
    c1_c->data[c1_i539] = c1_varargin_2->data[c1_i539];
  }

  c1_d_round(chartInstance, c1_c);
  c1_b_varargin_1 = (real_T)c1_varargin_1->size[0];
  c1_c_varargin_1 = c1_b_varargin_1;
  c1_p = false;
  c1_x1 = c1_c_varargin_1;
  c1_b_p = true;
  c1_b_x1 = c1_x1;
  c1_c_p = (c1_b_x1 == 1.0);
  if (!c1_c_p) {
    c1_b_p = false;
  }

  if (!c1_b_p) {
  } else {
    c1_p = true;
  }

  c1_emxInit_boolean_T(chartInstance, &c1_BW, 2, &c1_xf_emlrtRTEI);
  c1_emxInit_real_T1(chartInstance, &c1_tmp, 1, &c1_af_emlrtRTEI);
  if (c1_p && ((real_T)c1_varargin_1->size[1] > 1.0)) {
    c1_column_BW = true;
    c1_i541 = c1_BW->size[0] * c1_BW->size[1];
    c1_BW->size[0] = c1_varargin_1->size[1];
    c1_BW->size[1] = c1_varargin_1->size[0];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_BW, c1_i541,
      &c1_ye_emlrtRTEI);
    c1_c_loop_ub = c1_varargin_1->size[0] - 1;
    for (c1_i544 = 0; c1_i544 <= c1_c_loop_ub; c1_i544++) {
      c1_e_loop_ub = c1_varargin_1->size[1] - 1;
      for (c1_i547 = 0; c1_i547 <= c1_e_loop_ub; c1_i547++) {
        c1_BW->data[c1_i547 + c1_BW->size[0] * c1_i544] = c1_varargin_1->
          data[c1_i544 + c1_varargin_1->size[0] * c1_i547];
      }
    }

    c1_i546 = c1_tmp->size[0];
    c1_tmp->size[0] = c1_r->size[0];
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_tmp, c1_i546,
      &c1_af_emlrtRTEI);
    c1_f_loop_ub = c1_r->size[0] - 1;
    for (c1_i549 = 0; c1_i549 <= c1_f_loop_ub; c1_i549++) {
      c1_tmp->data[c1_i549] = c1_r->data[c1_i549];
    }

    c1_i550 = c1_r->size[0];
    c1_r->size[0] = c1_c->size[0];
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_r, c1_i550, &c1_cf_emlrtRTEI);
    c1_h_loop_ub = c1_c->size[0] - 1;
    for (c1_i552 = 0; c1_i552 <= c1_h_loop_ub; c1_i552++) {
      c1_r->data[c1_i552] = c1_c->data[c1_i552];
    }

    c1_i554 = c1_c->size[0];
    c1_c->size[0] = c1_tmp->size[0];
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_c, c1_i554, &c1_ef_emlrtRTEI);
    c1_j_loop_ub = c1_tmp->size[0] - 1;
    for (c1_i556 = 0; c1_i556 <= c1_j_loop_ub; c1_i556++) {
      c1_c->data[c1_i556] = c1_tmp->data[c1_i556];
    }
  } else {
    c1_column_BW = false;
    c1_i540 = c1_BW->size[0] * c1_BW->size[1];
    c1_BW->size[0] = c1_varargin_1->size[0];
    c1_BW->size[1] = c1_varargin_1->size[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_BW, c1_i540,
      &c1_xe_emlrtRTEI);
    c1_i542 = c1_BW->size[0];
    c1_i543 = c1_BW->size[1];
    c1_d_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
    for (c1_i545 = 0; c1_i545 <= c1_d_loop_ub; c1_i545++) {
      c1_BW->data[c1_i545] = c1_varargin_1->data[c1_i545];
    }
  }

  c1_emxInit_boolean_T1(chartInstance, &c1_x, 1, &c1_ff_emlrtRTEI);
  c1_i548 = c1_x->size[0];
  c1_x->size[0] = c1_r->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_x, c1_i548, &c1_bf_emlrtRTEI);
  c1_g_loop_ub = c1_r->size[0] - 1;
  for (c1_i551 = 0; c1_i551 <= c1_g_loop_ub; c1_i551++) {
    c1_x->data[c1_i551] = (c1_r->data[c1_i551] < 1.0);
  }

  c1_emxInit_boolean_T1(chartInstance, &c1_r15, 1, &c1_bg_emlrtRTEI);
  c1_b_BW = (real_T)c1_BW->size[0];
  c1_i553 = c1_r15->size[0];
  c1_r15->size[0] = c1_r->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_r15, c1_i553,
    &c1_df_emlrtRTEI);
  c1_i_loop_ub = c1_r->size[0] - 1;
  for (c1_i555 = 0; c1_i555 <= c1_i_loop_ub; c1_i555++) {
    c1_r15->data[c1_i555] = (c1_r->data[c1_i555] > c1_b_BW);
  }

  _SFD_SIZE_EQ_CHECK_1D(c1_x->size[0], c1_r15->size[0]);
  c1_i557 = c1_x->size[0];
  c1_x->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_x, c1_i557, &c1_ff_emlrtRTEI);
  c1_k_loop_ub = c1_x->size[0] - 1;
  for (c1_i558 = 0; c1_i558 <= c1_k_loop_ub; c1_i558++) {
    c1_x->data[c1_i558] = (c1_x->data[c1_i558] || c1_r15->data[c1_i558]);
  }

  c1_i559 = c1_r15->size[0];
  c1_r15->size[0] = c1_c->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_r15, c1_i559,
    &c1_gf_emlrtRTEI);
  c1_l_loop_ub = c1_c->size[0] - 1;
  for (c1_i560 = 0; c1_i560 <= c1_l_loop_ub; c1_i560++) {
    c1_r15->data[c1_i560] = (c1_c->data[c1_i560] < 1.0);
  }

  _SFD_SIZE_EQ_CHECK_1D(c1_x->size[0], c1_r15->size[0]);
  c1_i561 = c1_x->size[0];
  c1_x->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_x, c1_i561, &c1_ff_emlrtRTEI);
  c1_m_loop_ub = c1_x->size[0] - 1;
  for (c1_i562 = 0; c1_i562 <= c1_m_loop_ub; c1_i562++) {
    c1_x->data[c1_i562] = (c1_x->data[c1_i562] || c1_r15->data[c1_i562]);
  }

  c1_c_BW = (real_T)c1_BW->size[1];
  c1_i563 = c1_r15->size[0];
  c1_r15->size[0] = c1_c->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_r15, c1_i563,
    &c1_hf_emlrtRTEI);
  c1_n_loop_ub = c1_c->size[0] - 1;
  for (c1_i564 = 0; c1_i564 <= c1_n_loop_ub; c1_i564++) {
    c1_r15->data[c1_i564] = (c1_c->data[c1_i564] > c1_c_BW);
  }

  _SFD_SIZE_EQ_CHECK_1D(c1_x->size[0], c1_r15->size[0]);
  c1_i565 = c1_x->size[0];
  c1_x->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_x, c1_i565, &c1_ff_emlrtRTEI);
  c1_o_loop_ub = c1_x->size[0] - 1;
  for (c1_i566 = 0; c1_i566 <= c1_o_loop_ub; c1_i566++) {
    c1_x->data[c1_i566] = (c1_x->data[c1_i566] || c1_r15->data[c1_i566]);
  }

  c1_emxFree_boolean_T(chartInstance, &c1_r15);
  c1_emxInit_int32_T(chartInstance, &c1_ii, 1, &c1_cg_emlrtRTEI);
  c1_nx = c1_x->size[0];
  c1_k = c1_nx;
  c1_b_nx = c1_nx;
  c1_idx = 0;
  c1_i567 = c1_ii->size[0];
  c1_ii->size[0] = c1_k;
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_ii, c1_i567, &c1_fd_emlrtRTEI);
  c1_b = c1_b_nx;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  c1_b_ii = 1;
  exitg1 = false;
  while ((!exitg1) && (c1_b_ii - 1 <= c1_b_nx - 1)) {
    c1_c_ii = c1_b_ii;
    if (c1_x->data[c1_c_ii - 1]) {
      c1_idx++;
      c1_ii->data[c1_idx - 1] = c1_c_ii;
      if (c1_idx >= c1_k) {
        exitg1 = true;
      } else {
        c1_b_ii++;
      }
    } else {
      c1_b_ii++;
    }
  }

  c1_emxFree_boolean_T(chartInstance, &c1_x);
  if (c1_idx <= c1_k) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c1_b_y)));
  }

  c1_emxInit_int32_T(chartInstance, &c1_c_b, 1, &c1_pf_emlrtRTEI);
  if (c1_k == 1) {
    if (c1_idx == 0) {
      c1_i568 = c1_ii->size[0];
      c1_ii->size[0] = 0;
    }
  } else {
    c1_b37 = (1 > c1_idx);
    if (c1_b37) {
      c1_i569 = 0;
    } else {
      c1_i569 = c1_idx;
    }

    c1_i571 = c1_c_b->size[0];
    c1_c_b->size[0] = c1_i569;
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_b, c1_i571,
      &c1_jf_emlrtRTEI);
    c1_q_loop_ub = c1_i569 - 1;
    for (c1_i573 = 0; c1_i573 <= c1_q_loop_ub; c1_i573++) {
      c1_c_b->data[c1_i573] = 1 + c1_i573;
    }

    c1_matrixSize = c1_ii->size[0];
    c1_indexSize[0] = 1;
    c1_indexSize[1] = c1_c_b->size[0];
    c1_size1 = c1_matrixSize;
    if (c1_size1 != 1) {
      c1_d_b = false;
    } else {
      c1_d_b = true;
    }

    if (c1_d_b) {
      c1_nonSingletonDimFound = false;
      if (c1_indexSize[1] != 1) {
        c1_nonSingletonDimFound = true;
      }

      c1_e_b = c1_nonSingletonDimFound;
      if (c1_e_b) {
        c1_b_c = true;
      } else {
        c1_b_c = false;
      }
    } else {
      c1_b_c = false;
    }

    c1_c_c = c1_b_c;
    if (!c1_c_c) {
    } else {
      c1_c_y = NULL;
      sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      c1_d_y = NULL;
      sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_d_y)));
    }

    c1_i581 = c1_ii->size[0];
    c1_ii->size[0] = c1_i569;
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_ii, c1_i581, &c1_nf_emlrtRTEI);
  }

  c1_i570 = c1_tmp->size[0];
  c1_tmp->size[0] = c1_ii->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_tmp, c1_i570, &c1_if_emlrtRTEI);
  c1_p_loop_ub = c1_ii->size[0] - 1;
  for (c1_i572 = 0; c1_i572 <= c1_p_loop_ub; c1_i572++) {
    c1_tmp->data[c1_i572] = (real_T)c1_ii->data[c1_i572];
  }

  if (c1_tmp->size[0] != 0) {
    c1_emxInit_int32_T(chartInstance, &c1_b_tmp, 1, &c1_kf_emlrtRTEI);
    c1_c_warning(chartInstance);
    c1_i575 = c1_b_tmp->size[0];
    c1_b_tmp->size[0] = c1_tmp->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_tmp, c1_i575,
      &c1_kf_emlrtRTEI);
    c1_r_loop_ub = c1_tmp->size[0] - 1;
    for (c1_i577 = 0; c1_i577 <= c1_r_loop_ub; c1_i577++) {
      c1_b_tmp->data[c1_i577] = (int32_T)c1_tmp->data[c1_i577];
    }

    c1_emxInit_int32_T(chartInstance, &c1_c_tmp, 1, &c1_lf_emlrtRTEI);
    c1_d_nullAssignment(chartInstance, c1_r, c1_b_tmp);
    c1_i579 = c1_c_tmp->size[0];
    c1_c_tmp->size[0] = c1_tmp->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_tmp, c1_i579,
      &c1_lf_emlrtRTEI);
    c1_s_loop_ub = c1_tmp->size[0] - 1;
    c1_emxFree_int32_T(chartInstance, &c1_b_tmp);
    for (c1_i583 = 0; c1_i583 <= c1_s_loop_ub; c1_i583++) {
      c1_c_tmp->data[c1_i583] = (int32_T)c1_tmp->data[c1_i583];
    }

    c1_d_nullAssignment(chartInstance, c1_c, c1_c_tmp);
    c1_emxFree_int32_T(chartInstance, &c1_c_tmp);
  }

  for (c1_i574 = 0; c1_i574 < 2; c1_i574++) {
    c1_d_BW[c1_i574] = (real_T)c1_BW->size[c1_i574];
  }

  for (c1_i576 = 0; c1_i576 < 2; c1_i576++) {
    c1_d_BW[c1_i576];
  }

  for (c1_i578 = 0; c1_i578 < 2; c1_i578++) {
    c1_indexSize[c1_i578] = (int32_T)c1_d_BW[c1_i578];
  }

  c1_emxInit_real_T1(chartInstance, &c1_b_r, 1, &c1_mf_emlrtRTEI);
  c1_hi = c1_indexSize[0];
  c1_i580 = c1_b_r->size[0];
  c1_b_r->size[0] = c1_r->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_r, c1_i580, &c1_mf_emlrtRTEI);
  c1_t_loop_ub = c1_r->size[0] - 1;
  for (c1_i582 = 0; c1_i582 <= c1_t_loop_ub; c1_i582++) {
    c1_b_r->data[c1_i582] = c1_r->data[c1_i582];
  }

  if (c1_allinrange(chartInstance, c1_b_r, 1.0, c1_hi)) {
  } else {
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv36, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv36, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_e_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_f_y)));
  }

  c1_emxFree_real_T(chartInstance, &c1_b_r);
  c1_d_BW[0] = (real_T)c1_r->size[0];
  c1_d_BW[1] = 1.0;
  c1_b_varargin_2[0] = (real_T)c1_c->size[0];
  c1_b_varargin_2[1] = 1.0;
  for (c1_i584 = 0; c1_i584 < 2; c1_i584++) {
    c1_d_BW[c1_i584];
  }

  for (c1_i585 = 0; c1_i585 < 2; c1_i585++) {
    c1_b_varargin_2[c1_i585];
  }

  for (c1_i586 = 0; c1_i586 < 2; c1_i586++) {
    c1_d_BW[c1_i586];
  }

  for (c1_i587 = 0; c1_i587 < 2; c1_i587++) {
    c1_b_varargin_2[c1_i587];
  }

  c1_d_p = false;
  for (c1_i588 = 0; c1_i588 < 2; c1_i588++) {
    c1_d_BW[c1_i588];
  }

  for (c1_i589 = 0; c1_i589 < 2; c1_i589++) {
    c1_b_varargin_2[c1_i589];
  }

  c1_e_p = true;
  c1_b_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_b_k < 2)) {
    c1_c_k = 1.0 + (real_T)c1_b_k;
    c1_c_x1 = c1_d_BW[(int32_T)c1_c_k - 1];
    c1_x2 = c1_b_varargin_2[(int32_T)c1_c_k - 1];
    c1_f_p = (c1_c_x1 == c1_x2);
    if (!c1_f_p) {
      c1_e_p = false;
      exitg1 = true;
    } else {
      c1_b_k++;
    }
  }

  if (!c1_e_p) {
  } else {
    c1_d_p = true;
  }

  if (c1_d_p) {
  } else {
    c1_g_y = NULL;
    sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv37, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c1_h_y = NULL;
    sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv37, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_h_y)));
  }

  for (c1_i590 = 0; c1_i590 < 2; c1_i590++) {
    c1_indexSize[c1_i590];
  }

  c1_emxInit_real_T1(chartInstance, &c1_d_c, 1, &c1_mf_emlrtRTEI);
  c1_hi = c1_indexSize[1];
  c1_i591 = c1_d_c->size[0];
  c1_d_c->size[0] = c1_c->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_d_c, c1_i591, &c1_mf_emlrtRTEI);
  c1_u_loop_ub = c1_c->size[0] - 1;
  for (c1_i592 = 0; c1_i592 <= c1_u_loop_ub; c1_i592++) {
    c1_d_c->data[c1_i592] = c1_c->data[c1_i592];
  }

  if (c1_allinrange(chartInstance, c1_d_c, 1.0, c1_hi)) {
  } else {
    c1_i_y = NULL;
    sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv36, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_j_y = NULL;
    sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv36, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_i_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_j_y)));
  }

  c1_emxFree_real_T(chartInstance, &c1_d_c);
  c1_psiz = c1_indexSize[0];
  c1_i593 = c1_ii->size[0];
  c1_ii->size[0] = c1_r->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_ii, c1_i593, &c1_of_emlrtRTEI);
  c1_v_loop_ub = c1_r->size[0] - 1;
  for (c1_i594 = 0; c1_i594 <= c1_v_loop_ub; c1_i594++) {
    c1_ii->data[c1_i594] = (int32_T)c1_r->data[c1_i594];
  }

  c1_emxFree_real_T(chartInstance, &c1_r);
  c1_a = c1_psiz;
  c1_i595 = c1_c_b->size[0];
  c1_c_b->size[0] = c1_c->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_b, c1_i595, &c1_pf_emlrtRTEI);
  c1_w_loop_ub = c1_c->size[0] - 1;
  for (c1_i596 = 0; c1_i596 <= c1_w_loop_ub; c1_i596++) {
    c1_c_b->data[c1_i596] = (int32_T)c1_c->data[c1_i596] - 1;
  }

  c1_emxFree_real_T(chartInstance, &c1_c);
  c1_i597 = c1_c_b->size[0];
  c1_c_b->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_b, c1_i597, &c1_qf_emlrtRTEI);
  c1_x_loop_ub = c1_c_b->size[0] - 1;
  for (c1_i598 = 0; c1_i598 <= c1_x_loop_ub; c1_i598++) {
    c1_c_b->data[c1_i598] *= c1_a;
  }

  c1_i599 = c1_ii->size[0];
  c1_ii->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_ii, c1_i599, &c1_rf_emlrtRTEI);
  c1_y_loop_ub = c1_ii->size[0] - 1;
  for (c1_i600 = 0; c1_i600 <= c1_y_loop_ub; c1_i600++) {
    c1_ii->data[c1_i600] += c1_c_b->data[c1_i600];
  }

  c1_i601 = c1_tmp->size[0];
  c1_tmp->size[0] = c1_ii->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_tmp, c1_i601, &c1_sf_emlrtRTEI);
  c1_ab_loop_ub = c1_ii->size[0] - 1;
  for (c1_i602 = 0; c1_i602 <= c1_ab_loop_ub; c1_i602++) {
    c1_tmp->data[c1_i602] = (real_T)c1_ii->data[c1_i602];
  }

  c1_emxInit_boolean_T(chartInstance, &c1_BW2_temp, 2, &c1_tf_emlrtRTEI);
  c1_i603 = c1_BW2_temp->size[0] * c1_BW2_temp->size[1];
  c1_BW2_temp->size[0] = c1_BW->size[0];
  c1_BW2_temp->size[1] = c1_BW->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_BW2_temp, c1_i603,
    &c1_tf_emlrtRTEI);
  c1_i604 = c1_BW2_temp->size[0];
  c1_i605 = c1_BW2_temp->size[1];
  c1_bb_loop_ub = c1_BW->size[0] * c1_BW->size[1] - 1;
  for (c1_i606 = 0; c1_i606 <= c1_bb_loop_ub; c1_i606++) {
    c1_BW2_temp->data[c1_i606] = !c1_BW->data[c1_i606];
  }

  c1_b_imfill(chartInstance, c1_BW2_temp, c1_tmp);
  c1_emxFree_real_T(chartInstance, &c1_tmp);
  for (c1_i607 = 0; c1_i607 < 2; c1_i607++) {
    c1_b_BW2_temp[c1_i607] = c1_BW2_temp->size[c1_i607];
  }

  for (c1_i608 = 0; c1_i608 < 2; c1_i608++) {
    c1_e_BW[c1_i608] = c1_BW->size[c1_i608];
  }

  _SFD_SIZE_EQ_CHECK_ND(c1_b_BW2_temp, c1_e_BW, 2);
  c1_i609 = c1_BW2_temp->size[0] * c1_BW2_temp->size[1];
  c1_i610 = c1_BW->size[0] * c1_BW->size[1];
  c1_i611 = c1_BW2_temp->size[0] * c1_BW2_temp->size[1];
  c1_BW2_temp->size[0];
  c1_BW2_temp->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_BW2_temp, c1_i611,
    &c1_uf_emlrtRTEI);
  c1_i612 = c1_BW2_temp->size[0];
  c1_i613 = c1_BW2_temp->size[1];
  c1_i614 = c1_i609;
  c1_i615 = c1_i610;
  c1_cb_loop_ub = c1_i614 - 1;
  for (c1_i616 = 0; c1_i616 <= c1_cb_loop_ub; c1_i616++) {
    c1_BW2_temp->data[c1_i616] = (c1_BW2_temp->data[c1_i616] && c1_BW->
      data[c1_i616]);
  }

  c1_emxFree_boolean_T(chartInstance, &c1_BW);
  c1_b38 = (c1_BW2_temp->size[0] == 0);
  c1_b39 = (c1_BW2_temp->size[1] == 0);
  if (c1_b38 || c1_b39) {
  } else {
    c1_c_nx = c1_BW2_temp->size[0] * c1_BW2_temp->size[1];
    c1_b40 = (c1_BW2_temp->size[0] == 1);
    c1_b41 = (c1_BW2_temp->size[1] == 1);
    if (((!c1_b40) && (!c1_b41)) || ((real_T)c1_BW2_temp->size[0] != 1.0) ||
        ((real_T)c1_BW2_temp->size[1] <= 1.0)) {
      c1_b42 = true;
    } else {
      c1_b42 = false;
    }

    if (c1_b42) {
    } else {
      c1_k_y = NULL;
      sf_mex_assign(&c1_k_y, sf_mex_create("y", c1_cv2, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      c1_l_y = NULL;
      sf_mex_assign(&c1_l_y, sf_mex_create("y", c1_cv2, 10, 0U, 1U, 0U, 2, 1, 36),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_k_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_l_y)));
    }

    c1_d_k = c1_c_nx;
    c1_d_nx = c1_c_nx;
    c1_b_idx = 0;
    c1_i623 = c1_ii->size[0];
    c1_ii->size[0] = c1_d_k;
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_ii, c1_i623, &c1_fd_emlrtRTEI);
    c1_f_b = c1_d_nx;
    c1_g_b = c1_f_b;
    if (1 > c1_g_b) {
      c1_b_overflow = false;
    } else {
      c1_b_overflow = (c1_g_b > 2147483646);
    }

    if (c1_b_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    c1_d_ii = 1;
    exitg1 = false;
    while ((!exitg1) && (c1_d_ii - 1 <= c1_d_nx - 1)) {
      c1_e_ii = c1_d_ii;
      if (c1_BW2_temp->data[c1_e_ii - 1]) {
        c1_b_idx++;
        c1_ii->data[c1_b_idx - 1] = c1_e_ii;
        if (c1_b_idx >= c1_d_k) {
          exitg1 = true;
        } else {
          c1_d_ii++;
        }
      } else {
        c1_d_ii++;
      }
    }

    if (c1_b_idx <= c1_d_k) {
    } else {
      c1_m_y = NULL;
      sf_mex_assign(&c1_m_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      c1_n_y = NULL;
      sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                    false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_m_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_n_y)));
    }

    if (c1_d_k == 1) {
    } else {
      c1_b43 = (1 > c1_b_idx);
      if (c1_b43) {
        c1_i625 = 0;
      } else {
        c1_i625 = c1_b_idx;
      }

      c1_i626 = c1_c_b->size[0];
      c1_c_b->size[0] = c1_i625;
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_b, c1_i626,
        &c1_jf_emlrtRTEI);
      c1_gb_loop_ub = c1_i625 - 1;
      for (c1_i627 = 0; c1_i627 <= c1_gb_loop_ub; c1_i627++) {
        c1_c_b->data[c1_i627] = 1 + c1_i627;
      }

      c1_b_matrixSize = c1_ii->size[0];
      c1_indexSize[0] = 1;
      c1_indexSize[1] = c1_c_b->size[0];
      c1_b_size1 = c1_b_matrixSize;
      if (c1_b_size1 != 1) {
        c1_h_b = false;
      } else {
        c1_h_b = true;
      }

      if (c1_h_b) {
        c1_b_nonSingletonDimFound = false;
        if (c1_indexSize[1] != 1) {
          c1_b_nonSingletonDimFound = true;
        }

        c1_i_b = c1_b_nonSingletonDimFound;
        if (c1_i_b) {
          c1_e_c = true;
        } else {
          c1_e_c = false;
        }
      } else {
        c1_e_c = false;
      }

      c1_f_c = c1_e_c;
      if (!c1_f_c) {
      } else {
        c1_o_y = NULL;
        sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2, 1,
          30), false);
        c1_p_y = NULL;
        sf_mex_assign(&c1_p_y, sf_mex_create("y", c1_cv3, 10, 0U, 1U, 0U, 2, 1,
          30), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                          c1_o_y, 14, sf_mex_call_debug
                          (sfGlobalDebugInstanceStruct, "getString", 1U, 1U, 14,
                           sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "message", 1U, 1U, 14, c1_p_y)));
      }
    }
  }

  c1_emxFree_int32_T(chartInstance, &c1_c_b);
  c1_emxFree_int32_T(chartInstance, &c1_ii);
  if (c1_column_BW) {
    c1_i618 = c1_varargout_1->size[0] * c1_varargout_1->size[1];
    c1_varargout_1->size[0] = c1_BW2_temp->size[1];
    c1_varargout_1->size[1] = c1_BW2_temp->size[0];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_varargout_1, c1_i618,
      &c1_wf_emlrtRTEI);
    c1_db_loop_ub = c1_BW2_temp->size[0] - 1;
    for (c1_i621 = 0; c1_i621 <= c1_db_loop_ub; c1_i621++) {
      c1_fb_loop_ub = c1_BW2_temp->size[1] - 1;
      for (c1_i624 = 0; c1_i624 <= c1_fb_loop_ub; c1_i624++) {
        c1_varargout_1->data[c1_i624 + c1_varargout_1->size[0] * c1_i621] =
          c1_BW2_temp->data[c1_i621 + c1_BW2_temp->size[0] * c1_i624];
      }
    }
  } else {
    c1_i617 = c1_varargout_1->size[0] * c1_varargout_1->size[1];
    c1_varargout_1->size[0] = c1_BW2_temp->size[0];
    c1_varargout_1->size[1] = c1_BW2_temp->size[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_varargout_1, c1_i617,
      &c1_vf_emlrtRTEI);
    c1_i619 = c1_varargout_1->size[0];
    c1_i620 = c1_varargout_1->size[1];
    c1_eb_loop_ub = c1_BW2_temp->size[0] * c1_BW2_temp->size[1] - 1;
    for (c1_i622 = 0; c1_i622 <= c1_eb_loop_ub; c1_i622++) {
      c1_varargout_1->data[c1_i622] = c1_BW2_temp->data[c1_i622];
    }
  }

  c1_emxFree_boolean_T(chartInstance, &c1_BW2_temp);
}

static void c1_c_warning(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_cv38[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c1_b_y = NULL;
  static char_T c1_cv39[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c1_c_y = NULL;
  static char_T c1_msgID[26] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'b', 'w',
    's', 'e', 'l', 'e', 'c', 't', ':', 'o', 'u', 't', 'O', 'f', 'R', 'a', 'n',
    'g', 'e' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv38, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv39, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_msgID, 10, 0U, 1U, 0U, 2, 1, 26),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c1_b_y, 14, c1_c_y));
}

static void c1_b_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T
  *c1_b_x)
{
  int32_T c1_i628;
  int32_T c1_loop_ub;
  int32_T c1_i629;
  c1_emxArray_int32_T *c1_b_idx;
  int32_T c1_i630;
  int32_T c1_b_loop_ub;
  int32_T c1_i631;
  c1_i628 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i628, &c1_yb_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i629 = 0; c1_i629 <= c1_loop_ub; c1_i629++) {
    c1_b_x->data[c1_i629] = c1_x->data[c1_i629];
  }

  c1_emxInit_int32_T(chartInstance, &c1_b_idx, 1, &c1_yb_emlrtRTEI);
  c1_i630 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i630,
    &c1_yb_emlrtRTEI);
  c1_b_loop_ub = c1_idx->size[0] - 1;
  for (c1_i631 = 0; c1_i631 <= c1_b_loop_ub; c1_i631++) {
    c1_b_idx->data[c1_i631] = c1_idx->data[c1_i631];
  }

  c1_d_nullAssignment(chartInstance, c1_b_x, c1_b_idx);
  c1_emxFree_int32_T(chartInstance, &c1_b_idx);
}

static boolean_T c1_allinrange(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, real_T c1_lo, int32_T c1_hi)
{
  boolean_T c1_p;
  real_T c1_d28;
  int32_T c1_i632;
  int32_T c1_k;
  real_T c1_b_k;
  boolean_T c1_b44;
  int32_T exitg1;
  (void)chartInstance;
  (void)c1_lo;
  c1_d28 = (real_T)c1_x->size[0];
  c1_i632 = (int32_T)c1_d28 - 1;
  c1_k = 0;
  do {
    exitg1 = 0;
    if (c1_k <= c1_i632) {
      c1_b_k = 1.0 + (real_T)c1_k;
      if ((c1_x->data[(int32_T)c1_b_k - 1] >= 1.0) && (c1_x->data[(int32_T)
           c1_b_k - 1] <= (real_T)c1_hi)) {
        c1_b44 = true;
      } else {
        c1_b44 = false;
      }

      if (!c1_b44) {
        c1_p = false;
        exitg1 = 1;
      } else {
        c1_k++;
      }
    } else {
      c1_p = true;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return c1_p;
}

static void c1_imfill(SFc1_LIDAR_simInstanceStruct *chartInstance,
                      c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T
                      *c1_varargin_2, c1_emxArray_boolean_T *c1_I2)
{
  int32_T c1_i633;
  int32_T c1_i634;
  int32_T c1_i635;
  int32_T c1_loop_ub;
  int32_T c1_i636;
  c1_emxArray_real_T *c1_b_varargin_2;
  int32_T c1_i637;
  int32_T c1_b_loop_ub;
  int32_T c1_i638;
  c1_i633 = c1_I2->size[0] * c1_I2->size[1];
  c1_I2->size[0] = c1_varargin_1->size[0];
  c1_I2->size[1] = c1_varargin_1->size[1];
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_I2, c1_i633, &c1_dg_emlrtRTEI);
  c1_i634 = c1_I2->size[0];
  c1_i635 = c1_I2->size[1];
  c1_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i636 = 0; c1_i636 <= c1_loop_ub; c1_i636++) {
    c1_I2->data[c1_i636] = c1_varargin_1->data[c1_i636];
  }

  c1_emxInit_real_T1(chartInstance, &c1_b_varargin_2, 1, &c1_dg_emlrtRTEI);
  c1_i637 = c1_b_varargin_2->size[0];
  c1_b_varargin_2->size[0] = c1_varargin_2->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_varargin_2, c1_i637,
    &c1_dg_emlrtRTEI);
  c1_b_loop_ub = c1_varargin_2->size[0] - 1;
  for (c1_i638 = 0; c1_i638 <= c1_b_loop_ub; c1_i638++) {
    c1_b_varargin_2->data[c1_i638] = c1_varargin_2->data[c1_i638];
  }

  c1_b_imfill(chartInstance, c1_I2, c1_b_varargin_2);
  c1_emxFree_real_T(chartInstance, &c1_b_varargin_2);
}

static void c1_d_warning(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_cv40[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c1_b_y = NULL;
  static char_T c1_cv41[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c1_c_y = NULL;
  static char_T c1_msgID[24] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm',
    'f', 'i', 'l', 'l', ':', 'o', 'u', 't', 'O', 'f', 'R', 'a', 'n', 'g', 'e' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv40, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv41, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_msgID, 10, 0U, 1U, 0U, 2, 1, 24),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c1_b_y, 14, c1_c_y));
}

static void c1_hough(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T
                     *c1_h, c1_emxArray_real_T *c1_rho)
{
  boolean_T c1_b45;
  boolean_T c1_b46;
  const mxArray *c1_y = NULL;
  static char_T c1_cv42[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'N', 'o', 'n', 'e',
    'm', 'p', 't', 'y' };

  real_T c1_N;
  const mxArray *c1_b_y = NULL;
  real_T c1_M;
  real_T c1_b_M;
  const mxArray *c1_c_y = NULL;
  real_T c1_b_N;
  real_T c1_normSquared;
  const mxArray *c1_d_y = NULL;
  static char_T c1_cv43[26] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd', 'R', 'h', 'o', 'R', 'e',
    's' };

  real_T c1_a;
  const mxArray *c1_e_y = NULL;
  real_T c1_b_a;
  real_T c1_c_a;
  const mxArray *c1_f_y = NULL;
  real_T c1_x;
  static char_T c1_cv44[13] = { 'R', 'h', 'o', 'R', 'e', 's', 'o', 'l', 'u', 't',
    'i', 'o', 'n' };

  real_T c1_d_a;
  real_T c1_c;
  real_T c1_e_a;
  real_T c1_f_a;
  real_T c1_g_a;
  real_T c1_b_x;
  real_T c1_h_a;
  real_T c1_b_c;
  real_T c1_c_x;
  real_T c1_D;
  real_T c1_d_x;
  boolean_T c1_p;
  boolean_T c1_b_p;
  real_T c1_e_x;
  real_T c1_q;
  real_T c1_nrho;
  real_T c1_d1;
  real_T c1_d2;
  real_T c1_n1;
  real_T c1_f_x;
  real_T c1_g_x;
  real_T c1_dv7[2];
  int32_T c1_i639;
  int32_T c1_n;
  int32_T c1_varargin_2;
  int32_T c1_b_varargin_2;
  real_T c1_b_n;
  int32_T c1_rhoLength;
  real_T c1_firstRho;
  real_T c1_delta1;
  real_T c1_numCol;
  real_T c1_d29;
  real_T c1_numRow;
  int32_T c1_i640;
  real_T c1_delta2;
  int32_T c1_i641;
  int32_T c1_k;
  real_T c1_d30;
  int32_T c1_i642;
  int32_T c1_b_k;
  real_T c1_c_k;
  int32_T c1_i643;
  int32_T c1_i644;
  int32_T c1_loop_ub;
  int32_T c1_i645;
  int32_T c1_i;
  real_T c1_slope;
  int32_T c1_b_i;
  int32_T c1_i646;
  real_T c1_h_x;
  int32_T c1_c_n;
  real_T c1_i_x;
  real_T c1_cost[181];
  real_T c1_d_n;
  real_T c1_j_x;
  int32_T c1_i647;
  real_T c1_k_x;
  int32_T c1_m;
  real_T c1_sint[181];
  real_T c1_b_m;
  int32_T c1_thetaIdx;
  int32_T c1_b_thetaIdx;
  real_T c1_myRho;
  real_T c1_l_x;
  int32_T c1_g_y;
  int32_T c1_rhoIdx;
  c1_b45 = (c1_varargin_1->size[0] == 0);
  c1_b46 = (c1_varargin_1->size[1] == 0);
  if ((!c1_b45) && (!c1_b46)) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv42, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv7, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv8, 10, 0U, 1U, 0U, 2, 1, 19),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c1_b_y, 14, c1_c_y)));
  }

  c1_N = (real_T)c1_varargin_1->size[1];
  c1_M = (real_T)c1_varargin_1->size[0];
  c1_b_M = c1_M;
  c1_b_N = c1_N;
  c1_normSquared = c1_b_N * c1_b_N + c1_b_M * c1_b_M;
  if (!(1.0 >= c1_normSquared)) {
  } else {
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv43, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv43, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv44, 10, 0U, 1U, 0U, 2, 1, 13),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_e_y, 14, c1_f_y)));
  }

  c1_a = c1_M - 1.0;
  c1_b_a = c1_a;
  c1_c_a = c1_b_a;
  c1_x = c1_c_a;
  c1_d_a = c1_x;
  c1_c = c1_d_a * c1_d_a;
  c1_e_a = c1_N - 1.0;
  c1_f_a = c1_e_a;
  c1_g_a = c1_f_a;
  c1_b_x = c1_g_a;
  c1_h_a = c1_b_x;
  c1_b_c = c1_h_a * c1_h_a;
  c1_c_x = c1_c + c1_b_c;
  c1_D = c1_c_x;
  c1_d_x = c1_D;
  if (c1_d_x < 0.0) {
    c1_p = true;
  } else {
    c1_p = false;
  }

  c1_b_p = c1_p;
  if (c1_b_p) {
    c1_error(chartInstance);
  }

  c1_D = muDoubleScalarSqrt(c1_D);
  c1_e_x = c1_D;
  c1_q = c1_e_x;
  c1_q = muDoubleScalarCeil(c1_q);
  c1_nrho = 2.0 * c1_q + 1.0;
  c1_d1 = -c1_q;
  c1_d2 = c1_q;
  c1_n1 = c1_nrho;
  if (c1_n1 < 0.0) {
    c1_n1 = 0.0;
  }

  c1_f_x = c1_n1;
  c1_g_x = c1_f_x;
  c1_g_x = muDoubleScalarFloor(c1_g_x);
  c1_dv7[0] = 1.0;
  c1_dv7[1] = _SFD_NON_NEGATIVE_CHECK("", c1_g_x);
  c1_i639 = c1_rho->size[0] * c1_rho->size[1];
  c1_rho->size[0] = 1;
  c1_rho->size[1] = (int32_T)c1_dv7[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_rho, c1_i639, &c1_eg_emlrtRTEI);
  c1_n = c1_rho->size[1] - 1;
  if ((real_T)c1_rho->size[1] >= 1.0) {
    c1_rho->data[c1_n] = c1_d2;
    if ((real_T)c1_rho->size[1] >= 2.0) {
      c1_rho->data[0] = c1_d1;
      if ((real_T)c1_rho->size[1] >= 3.0) {
        if (((c1_d1 < 0.0) != (c1_d2 < 0.0)) && ((muDoubleScalarAbs(c1_d1) >
              8.9884656743115785E+307) || (muDoubleScalarAbs(c1_d2) >
              8.9884656743115785E+307))) {
          c1_delta1 = c1_d1 / ((real_T)c1_rho->size[1] - 1.0);
          c1_delta2 = c1_d2 / ((real_T)c1_rho->size[1] - 1.0);
          c1_d30 = (real_T)c1_rho->size[1] - 2.0;
          c1_i642 = (int32_T)c1_d30 - 1;
          for (c1_b_k = 0; c1_b_k <= c1_i642; c1_b_k++) {
            c1_c_k = 1.0 + (real_T)c1_b_k;
            c1_rho->data[(int32_T)(c1_c_k + 1.0) - 1] = (c1_d1 + c1_delta2 *
              c1_c_k) - c1_delta1 * c1_c_k;
          }
        } else {
          c1_delta1 = (c1_d2 - c1_d1) / ((real_T)c1_rho->size[1] - 1.0);
          c1_d29 = (real_T)c1_rho->size[1] - 2.0;
          c1_i640 = (int32_T)c1_d29 - 1;
          for (c1_k = 0; c1_k <= c1_i640; c1_k++) {
            c1_c_k = 1.0 + (real_T)c1_k;
            c1_rho->data[(int32_T)(c1_c_k + 1.0) - 1] = c1_d1 + c1_c_k *
              c1_delta1;
          }
        }
      }
    }
  }

  c1_varargin_2 = c1_rho->size[1];
  c1_b_varargin_2 = c1_varargin_2;
  c1_b_n = (real_T)c1_b_varargin_2;
  c1_rhoLength = (int32_T)c1_b_n;
  (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
    chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_rho->size[1]);
  c1_firstRho = c1_rho->data[0];
  c1_numCol = (real_T)c1_varargin_1->size[1];
  c1_numRow = (real_T)c1_varargin_1->size[0];
  c1_i641 = c1_h->size[0] * c1_h->size[1];
  c1_h->size[0] = c1_rhoLength;
  c1_h->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_h, c1_i641, &c1_fg_emlrtRTEI);
  c1_i643 = c1_h->size[0];
  c1_i644 = c1_h->size[1];
  c1_loop_ub = c1_rhoLength * 181 - 1;
  for (c1_i645 = 0; c1_i645 <= c1_loop_ub; c1_i645++) {
    c1_h->data[c1_i645] = 0.0;
  }

  for (c1_i = 0; c1_i < 181; c1_i++) {
    c1_b_i = c1_i;
    c1_h_x = (-45.0 + 0.5 * (real_T)c1_b_i) * 3.1415926535897931 / 180.0;
    c1_i_x = c1_h_x;
    c1_i_x = muDoubleScalarCos(c1_i_x);
    c1_cost[c1_b_i] = c1_i_x;
    c1_j_x = (-45.0 + 0.5 * (real_T)c1_b_i) * 3.1415926535897931 / 180.0;
    c1_k_x = c1_j_x;
    c1_k_x = muDoubleScalarSin(c1_k_x);
    c1_sint[c1_b_i] = c1_k_x;
  }

  c1_slope = (real_T)(c1_rhoLength - 1) / (c1_rho->
    data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S,
    1U, 0, 0, MAX_uint32_T, c1_rhoLength, 1, c1_rho->size[1]) - 1] - c1_firstRho);
  c1_i646 = (int32_T)c1_numCol - 1;
  for (c1_c_n = 0; c1_c_n <= c1_i646; c1_c_n++) {
    c1_d_n = 1.0 + (real_T)c1_c_n;
    c1_i647 = (int32_T)c1_numRow - 1;
    for (c1_m = 0; c1_m <= c1_i647; c1_m++) {
      c1_b_m = 1.0 + (real_T)c1_m;
      if (c1_varargin_1->data[(sf_eml_array_bounds_check
           (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
            MAX_uint32_T, (int32_T)c1_b_m, 1, c1_varargin_1->size[0]) +
           c1_varargin_1->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_d_n, 1, c1_varargin_1->size[1]) - 1)) - 1])
      {
        for (c1_thetaIdx = 0; c1_thetaIdx < 181; c1_thetaIdx++) {
          c1_b_thetaIdx = c1_thetaIdx;
          c1_myRho = (c1_d_n - 1.0) * c1_cost[c1_b_thetaIdx] + (c1_b_m - 1.0) *
            c1_sint[c1_b_thetaIdx];
          c1_l_x = c1_slope * (c1_myRho - c1_firstRho);
          c1_g_y = (int32_T)(c1_l_x + 0.5) + 1;
          c1_rhoIdx = c1_g_y;
          c1_h->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_rhoIdx, 1, c1_h->size[0])
                      + c1_h->size[0] * c1_b_thetaIdx) - 1] = c1_h->data
            [(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
               chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_rhoIdx, 1,
               c1_h->size[0]) + c1_h->size[0] * c1_b_thetaIdx) - 1] + 1.0;
        }
      }
    }
  }
}

static void c1_houghpeaks(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_varargin_1, real_T c1_varargin_4, c1_emxArray_real_T
  *c1_peaks)
{
  c1_emxArray_real_T *c1_b_varargin_1;
  real_T c1_b_varargin_4;
  int32_T c1_i648;
  int32_T c1_i649;
  int32_T c1_i650;
  int32_T c1_loop_ub;
  int32_T c1_i651;
  real_T c1_numElems;
  int32_T c1_i652;
  int32_T c1_k;
  real_T c1_M;
  real_T c1_b_k;
  real_T c1_nhoodSizeDefault[2];
  int32_T c1_i653;
  int32_T c1_c_k;
  int32_T c1_i654;
  int32_T c1_d_k;
  real_T c1_x;
  real_T c1_b_x;
  int32_T c1_i655;
  int32_T c1_e_k;
  real_T c1_c_varargin_1[2];
  int32_T c1_f_k;
  real_T c1_c_x;
  real_T c1_varargin_2;
  real_T c1_d_x;
  real_T c1_b_varargin_2;
  real_T c1_ex;
  real_T c1_c_varargin_2;
  real_T c1_threshold;
  real_T c1_b_threshold;
  real_T c1_c_threshold;
  real_T c1_a;
  real_T c1_b_a;
  real_T c1_c_a;
  boolean_T c1_p;
  real_T c1_e_x;
  real_T c1_f_x;
  boolean_T c1_b;
  boolean_T c1_b_p;
  boolean_T c1_b47;
  const mxArray *c1_y = NULL;
  static char_T c1_cv45[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'N', 'a', 'N' };

  boolean_T c1_c_p;
  const mxArray *c1_b_y = NULL;
  int32_T c1_g_k;
  const mxArray *c1_c_y = NULL;
  static char_T c1_cv46[9] = { 'T', 'h', 'r', 'e', 's', 'h', 'o', 'l', 'd' };

  real_T c1_h_k;
  real_T c1_g_x;
  boolean_T c1_b48;
  real_T c1_h_x;
  boolean_T c1_b_b;
  boolean_T c1_b49;
  const mxArray *c1_d_y = NULL;
  real_T c1_i_x;
  int32_T c1_i656;
  boolean_T c1_c_b;
  const mxArray *c1_e_y = NULL;
  boolean_T c1_b50;
  boolean_T c1_d_b;
  const mxArray *c1_f_y = NULL;
  real_T c1_b_nhoodSizeDefault[2];
  static char_T c1_cv47[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  boolean_T c1_b51;
  const mxArray *c1_g_y = NULL;
  boolean_T c1_d_p;
  const mxArray *c1_h_y = NULL;
  int32_T c1_i_k;
  const mxArray *c1_i_y = NULL;
  static char_T c1_cv48[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c1_j_k;
  real_T c1_j_x;
  boolean_T c1_b52;
  boolean_T c1_e_p;
  const mxArray *c1_j_y = NULL;
  static char_T c1_cv49[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'P', 'o', 's', 'i', 't', 'i', 'v', 'e' };

  boolean_T c1_f_p;
  const mxArray *c1_k_y = NULL;
  int32_T c1_k_k;
  const mxArray *c1_l_y = NULL;
  static char_T c1_cv50[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c1_l_k;
  real_T c1_k_x;
  boolean_T c1_b53;
  real_T c1_l_x;
  real_T c1_m_x;
  real_T c1_n_x;
  const mxArray *c1_m_y = NULL;
  real_T c1_o_x;
  static char_T c1_cv51[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'O', 'd', 'd' };

  int32_T c1_i657;
  real_T c1_p_x;
  const mxArray *c1_n_y = NULL;
  real_T c1_q_x;
  static char_T c1_cv52[43] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'V', 'a', 'l', 'i', 'd', 'a', 't', 'e', 'a', 't', 't',
    'r', 'i', 'b', 'u', 't', 'e', 's', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd',
    'O', 'd', 'd' };

  boolean_T c1_e_b;
  const mxArray *c1_o_y = NULL;
  int32_T c1_i658;
  boolean_T c1_b54;
  static char_T c1_cv53[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c1_r_x;
  boolean_T c1_f_b;
  boolean_T c1_p_y;
  boolean_T c1_s_x[2];
  boolean_T c1_b55;
  int32_T c1_m_k;
  boolean_T c1_g_b;
  real_T c1_r;
  real_T c1_n_k;
  boolean_T c1_rEQ0;
  boolean_T c1_g_p;
  const mxArray *c1_q_y = NULL;
  boolean_T c1_b56;
  static char_T c1_cv54[33] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 't', 'o', 'o', 'B', 'i', 'g', 'N',
    'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  boolean_T c1_h_p;
  const mxArray *c1_r_y = NULL;
  int32_T c1_o_k;
  const mxArray *c1_s_y = NULL;
  static char_T c1_cv55[9] = { 'N', 'H', 'o', 'o', 'd', 'S', 'i', 'z', 'e' };

  real_T c1_p_k;
  real_T c1_t_x;
  boolean_T c1_b57;
  real_T c1_u_x;
  boolean_T c1_h_b;
  boolean_T c1_b58;
  const mxArray *c1_t_y = NULL;
  real_T c1_v_x;
  int32_T c1_ixLead;
  boolean_T c1_i_b;
  const mxArray *c1_u_y = NULL;
  int32_T c1_iyLead;
  boolean_T c1_b59;
  real_T c1_work;
  boolean_T c1_j_b;
  const mxArray *c1_v_y = NULL;
  int32_T c1_m;
  static char_T c1_cv56[5] = { 'T', 'h', 'e', 't', 'a' };

  real_T c1_y1[179];
  real_T c1_w_x[178];
  real_T c1_tmp1;
  real_T c1_w_y;
  real_T c1_tmp2;
  int32_T c1_q_k;
  real_T c1_x_x;
  int32_T c1_xoffset;
  real_T c1_y_x;
  int32_T c1_ix;
  real_T c1_ab_x;
  real_T c1_x_y;
  const mxArray *c1_y_y = NULL;
  static char_T c1_cv57[43] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd',
    'T', 'h', 'e', 't', 'a', 'V', 'e', 'c', 't', 'o', 'r', 'S', 'p', 'a', 'c',
    'i', 'n', 'g' };

  const mxArray *c1_ab_y = NULL;
  c1_emxArray_real_T *c1_hnew;
  int32_T c1_i659;
  int32_T c1_numRowH;
  boolean_T c1_isDone;
  int32_T c1_i660;
  int32_T c1_i661;
  int32_T c1_i662;
  int32_T c1_i663;
  int32_T c1_i664;
  int32_T c1_b_loop_ub;
  int32_T c1_i665;
  c1_emxArray_real_T *c1_peakCoordinates;
  int32_T c1_nhoodCenter_i;
  int32_T c1_nhoodCenter_j;
  int32_T c1_i666;
  int32_T c1_peakIdx;
  real_T c1_b_ex;
  int32_T c1_r_k;
  real_T c1_minTheta;
  real_T c1_c_ex;
  int32_T c1_s_k;
  real_T c1_maxTheta;
  real_T c1_bb_x;
  real_T c1_cb_x;
  real_T c1_db_x;
  real_T c1_bb_y;
  real_T c1_thetaResolution;
  real_T c1_eb_x;
  real_T c1_fb_x;
  real_T c1_gb_x;
  real_T c1_cb_y;
  boolean_T c1_isThetaAntisymmetric;
  int32_T c1_iPeak;
  int32_T c1_jPeak;
  int32_T c1_i667;
  real_T c1_maxVal;
  int32_T c1_i668;
  int32_T c1_b_M;
  int32_T c1_i669;
  int32_T c1_j;
  int32_T c1_i670;
  int32_T c1_i671;
  int32_T c1_b_j;
  int32_T c1_i672;
  int32_T c1_k_b;
  int32_T c1_l_b;
  int32_T c1_c_loop_ub;
  int32_T c1_rhoMin;
  boolean_T c1_overflow;
  int32_T c1_i673;
  int32_T c1_rhoMax;
  int32_T c1_thetaMin;
  int32_T c1_thetaMax;
  int32_T c1_i;
  int32_T c1_b_i;
  int32_T c1_d_a;
  real_T c1_val;
  int32_T c1_m_b;
  int32_T c1_e_a;
  int32_T c1_n_b;
  boolean_T c1_b_overflow;
  int32_T c1_theta;
  int32_T c1_f_a;
  int32_T c1_o_b;
  int32_T c1_g_a;
  int32_T c1_p_b;
  boolean_T c1_c_overflow;
  int32_T c1_rho;
  int32_T c1_rhoToRemove;
  int32_T c1_thetaToRemove;
  boolean_T exitg1;
  c1_emxInit_real_T(chartInstance, &c1_b_varargin_1, 2, &c1_gg_emlrtRTEI);
  c1_b_varargin_4 = c1_varargin_4;
  c1_i648 = c1_b_varargin_1->size[0] * c1_b_varargin_1->size[1];
  c1_b_varargin_1->size[0] = c1_varargin_1->size[0];
  c1_b_varargin_1->size[1] = 181;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_varargin_1, c1_i648,
    &c1_gg_emlrtRTEI);
  c1_i649 = c1_b_varargin_1->size[0];
  c1_i650 = c1_b_varargin_1->size[1];
  c1_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
  for (c1_i651 = 0; c1_i651 <= c1_loop_ub; c1_i651++) {
    c1_b_varargin_1->data[c1_i651] = c1_varargin_1->data[c1_i651];
  }

  c1_validateattributes(chartInstance, c1_b_varargin_1);
  c1_numElems = (real_T)(c1_varargin_1->size[0] * 181);
  c1_i652 = (int32_T)c1_numElems - 1;
  c1_k = 0;
  c1_emxFree_real_T(chartInstance, &c1_b_varargin_1);
  while (c1_k <= c1_i652) {
    c1_b_k = 1.0 + (real_T)c1_k;
    (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_b_k, 1,
      c1_varargin_1->size[0] * 181);
    c1_k++;
  }

  c1_M = (real_T)c1_varargin_1->size[0];
  c1_nhoodSizeDefault[0] = c1_M / 50.0;
  c1_nhoodSizeDefault[1] = 3.62;
  for (c1_i653 = 0; c1_i653 < 2; c1_i653++) {
    c1_nhoodSizeDefault[c1_i653] /= 2.0;
  }

  for (c1_c_k = 0; c1_c_k < 2; c1_c_k++) {
    c1_d_k = c1_c_k;
    c1_x = c1_nhoodSizeDefault[c1_d_k];
    c1_b_x = c1_x;
    c1_b_x = muDoubleScalarCeil(c1_b_x);
    c1_nhoodSizeDefault[c1_d_k] = c1_b_x;
  }

  for (c1_i654 = 0; c1_i654 < 2; c1_i654++) {
    c1_nhoodSizeDefault[c1_i654] *= 2.0;
  }

  for (c1_i655 = 0; c1_i655 < 2; c1_i655++) {
    c1_c_varargin_1[c1_i655] = c1_nhoodSizeDefault[c1_i655] + 1.0;
  }

  for (c1_e_k = 0; c1_e_k < 2; c1_e_k++) {
    c1_f_k = c1_e_k;
    c1_c_x = c1_c_varargin_1[c1_f_k];
    c1_d_x = c1_c_x;
    c1_ex = c1_d_x;
    c1_nhoodSizeDefault[c1_f_k] = c1_ex;
  }

  if (c1_M < 3.0) {
    c1_nhoodSizeDefault[0] = 1.0;
  }

  c1_varargin_2 = c1_b_varargin_4;
  c1_b_varargin_2 = c1_varargin_2;
  c1_c_varargin_2 = c1_b_varargin_2;
  c1_threshold = c1_c_varargin_2;
  c1_b_threshold = c1_threshold;
  c1_c_threshold = c1_threshold;
  c1_a = c1_c_threshold;
  c1_b_a = c1_a;
  c1_c_a = c1_b_a;
  c1_p = true;
  c1_e_x = c1_c_a;
  c1_f_x = c1_e_x;
  c1_b = muDoubleScalarIsNaN(c1_f_x);
  c1_b_p = !c1_b;
  if (c1_b_p) {
  } else {
    c1_p = false;
  }

  if (c1_p) {
    c1_b47 = true;
  } else {
    c1_b47 = false;
  }

  if (c1_b47) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv45, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv4, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv46, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c1_b_y, 14, c1_c_y)));
  }

  c1_c_p = true;
  c1_g_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_g_k < 2)) {
    c1_h_k = 1.0 + (real_T)c1_g_k;
    c1_g_x = c1_nhoodSizeDefault[(int32_T)c1_h_k - 1];
    c1_h_x = c1_g_x;
    c1_b_b = muDoubleScalarIsInf(c1_h_x);
    c1_b49 = !c1_b_b;
    c1_i_x = c1_g_x;
    c1_c_b = muDoubleScalarIsNaN(c1_i_x);
    c1_b50 = !c1_c_b;
    c1_d_b = (c1_b49 && c1_b50);
    if (c1_d_b) {
      c1_g_k++;
    } else {
      c1_c_p = false;
      exitg1 = true;
    }
  }

  if (c1_c_p) {
    c1_b48 = true;
  } else {
    c1_b48 = false;
  }

  if (c1_b48) {
  } else {
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv9, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv47, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_e_y, 14, c1_f_y)));
  }

  for (c1_i656 = 0; c1_i656 < 2; c1_i656++) {
    c1_b_nhoodSizeDefault[c1_i656] = c1_nhoodSizeDefault[c1_i656];
  }

  if (c1_all(chartInstance, c1_b_nhoodSizeDefault)) {
    c1_b51 = true;
  } else {
    c1_b51 = false;
  }

  if (c1_b51) {
  } else {
    c1_g_y = NULL;
    sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv11, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c1_h_y = NULL;
    sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv5, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c1_i_y = NULL;
    sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv48, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_h_y, 14, c1_i_y)));
  }

  c1_d_p = true;
  c1_i_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_i_k < 2)) {
    c1_j_k = 1.0 + (real_T)c1_i_k;
    c1_j_x = c1_nhoodSizeDefault[(int32_T)c1_j_k - 1];
    c1_e_p = !(c1_j_x <= 0.0);
    if (c1_e_p) {
      c1_i_k++;
    } else {
      c1_d_p = false;
      exitg1 = true;
    }
  }

  if (c1_d_p) {
    c1_b52 = true;
  } else {
    c1_b52 = false;
  }

  if (c1_b52) {
  } else {
    c1_j_y = NULL;
    sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv49, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c1_k_y = NULL;
    sf_mex_assign(&c1_k_y, sf_mex_create("y", c1_cv12, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_l_y = NULL;
    sf_mex_assign(&c1_l_y, sf_mex_create("y", c1_cv50, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_k_y, 14, c1_l_y)));
  }

  c1_f_p = true;
  c1_k_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k_k < 2)) {
    c1_l_k = 1.0 + (real_T)c1_k_k;
    c1_k_x = c1_nhoodSizeDefault[(int32_T)c1_l_k - 1];
    c1_l_x = c1_k_x;
    c1_m_x = c1_l_x;
    c1_n_x = c1_m_x;
    c1_o_x = c1_n_x;
    c1_p_x = c1_o_x;
    c1_q_x = c1_p_x;
    c1_e_b = muDoubleScalarIsInf(c1_q_x);
    c1_b54 = !c1_e_b;
    c1_r_x = c1_p_x;
    c1_f_b = muDoubleScalarIsNaN(c1_r_x);
    c1_b55 = !c1_f_b;
    c1_g_b = (c1_b54 && c1_b55);
    if (c1_g_b) {
      c1_r = muDoubleScalarRem(c1_o_x, 2.0);
      c1_rEQ0 = (c1_r == 0.0);
      if (c1_rEQ0) {
        c1_r = 0.0;
      }
    } else {
      c1_r = rtNaN;
    }

    c1_g_p = (c1_r == 1.0);
    if (c1_g_p) {
      c1_k_k++;
    } else {
      c1_f_p = false;
      exitg1 = true;
    }
  }

  if (c1_f_p) {
    c1_b53 = true;
  } else {
    c1_b53 = false;
  }

  if (c1_b53) {
  } else {
    c1_m_y = NULL;
    sf_mex_assign(&c1_m_y, sf_mex_create("y", c1_cv51, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c1_n_y = NULL;
    sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv52, 10, 0U, 1U, 0U, 2, 1, 43),
                  false);
    c1_o_y = NULL;
    sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv53, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_m_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_n_y, 14, c1_o_y)));
  }

  for (c1_i657 = 0; c1_i657 < 2; c1_i657++) {
    c1_c_varargin_1[c1_i657] = (real_T)c1_varargin_1->size[c1_i657];
  }

  for (c1_i658 = 0; c1_i658 < 2; c1_i658++) {
    c1_s_x[c1_i658] = (c1_nhoodSizeDefault[c1_i658] > c1_c_varargin_1[c1_i658]);
  }

  c1_p_y = false;
  c1_m_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_m_k < 2)) {
    c1_n_k = 1.0 + (real_T)c1_m_k;
    if (!c1_s_x[(int32_T)c1_n_k - 1]) {
      c1_b56 = true;
    } else {
      c1_b56 = false;
    }

    if (!c1_b56) {
      c1_p_y = true;
      exitg1 = true;
    } else {
      c1_m_k++;
    }
  }

  if (!c1_p_y) {
  } else {
    c1_q_y = NULL;
    sf_mex_assign(&c1_q_y, sf_mex_create("y", c1_cv54, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c1_r_y = NULL;
    sf_mex_assign(&c1_r_y, sf_mex_create("y", c1_cv54, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c1_s_y = NULL;
    sf_mex_assign(&c1_s_y, sf_mex_create("y", c1_cv55, 10, 0U, 1U, 0U, 2, 1, 9),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_q_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_r_y, 14, c1_s_y)));
  }

  c1_h_p = true;
  c1_o_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_o_k < 180)) {
    c1_p_k = 1.0 + (real_T)c1_o_k;
    c1_t_x = -90.0 + (real_T)((int32_T)c1_p_k - 1);
    c1_u_x = c1_t_x;
    c1_h_b = muDoubleScalarIsInf(c1_u_x);
    c1_b58 = !c1_h_b;
    c1_v_x = c1_t_x;
    c1_i_b = muDoubleScalarIsNaN(c1_v_x);
    c1_b59 = !c1_i_b;
    c1_j_b = (c1_b58 && c1_b59);
    if (c1_j_b) {
      c1_o_k++;
    } else {
      c1_h_p = false;
      exitg1 = true;
    }
  }

  if (c1_h_p) {
    c1_b57 = true;
  } else {
    c1_b57 = false;
  }

  if (c1_b57) {
  } else {
    c1_t_y = NULL;
    sf_mex_assign(&c1_t_y, sf_mex_create("y", c1_cv9, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c1_u_y = NULL;
    sf_mex_assign(&c1_u_y, sf_mex_create("y", c1_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c1_v_y = NULL;
    sf_mex_assign(&c1_v_y, sf_mex_create("y", c1_cv56, 10, 0U, 1U, 0U, 2, 1, 5),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_t_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_u_y, 14, c1_v_y)));
  }

  c1_ixLead = 1;
  c1_iyLead = 0;
  c1_work = -90.0;
  for (c1_m = 0; c1_m < 179; c1_m++) {
    c1_tmp1 = -90.0 + (real_T)c1_ixLead;
    c1_tmp2 = c1_work;
    c1_work = c1_tmp1;
    c1_tmp1 -= c1_tmp2;
    c1_ixLead++;
    c1_y1[c1_iyLead] = c1_tmp1;
    c1_iyLead++;
  }

  c1_diff(chartInstance, c1_y1, c1_w_x);
  c1_w_y = c1_w_x[0];
  for (c1_q_k = 0; c1_q_k < 177; c1_q_k++) {
    c1_xoffset = c1_q_k;
    c1_ix = c1_xoffset;
    c1_w_y += c1_w_x[c1_ix + 1];
  }

  c1_x_x = c1_w_y;
  c1_y_x = c1_x_x;
  c1_ab_x = c1_y_x;
  c1_x_y = muDoubleScalarAbs(c1_ab_x);
  if (!(c1_x_y > 1.4901161193847656E-8)) {
  } else {
    c1_y_y = NULL;
    sf_mex_assign(&c1_y_y, sf_mex_create("y", c1_cv57, 10, 0U, 1U, 0U, 2, 1, 43),
                  false);
    c1_ab_y = NULL;
    sf_mex_assign(&c1_ab_y, sf_mex_create("y", c1_cv57, 10, 0U, 1U, 0U, 2, 1, 43),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_ab_y)));
  }

  if ((real_T)(c1_varargin_1->size[0] * 181) < 1.0) {
    c1_i659 = c1_peaks->size[0] * c1_peaks->size[1];
    c1_peaks->size[0] = 0;
    c1_peaks->size[1] = 0;
    c1_i661 = c1_peaks->size[0];
    c1_i662 = c1_peaks->size[1];
  } else {
    c1_emxInit_real_T(chartInstance, &c1_hnew, 2, &c1_hg_emlrtRTEI);
    c1_numRowH = c1_varargin_1->size[0];
    c1_isDone = false;
    c1_i660 = c1_hnew->size[0] * c1_hnew->size[1];
    c1_hnew->size[0] = c1_varargin_1->size[0];
    c1_hnew->size[1] = 181;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_hnew, c1_i660,
      &c1_hg_emlrtRTEI);
    c1_i663 = c1_hnew->size[0];
    c1_i664 = c1_hnew->size[1];
    c1_b_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
    for (c1_i665 = 0; c1_i665 <= c1_b_loop_ub; c1_i665++) {
      c1_hnew->data[c1_i665] = c1_varargin_1->data[c1_i665];
    }

    c1_emxInit_real_T(chartInstance, &c1_peakCoordinates, 2, &c1_kg_emlrtRTEI);
    c1_nhoodCenter_i = (int32_T)((c1_nhoodSizeDefault[0] - 1.0) / 2.0);
    c1_nhoodCenter_j = (int32_T)((c1_nhoodSizeDefault[1] - 1.0) / 2.0);
    c1_i666 = c1_peakCoordinates->size[0] * c1_peakCoordinates->size[1];
    c1_peakCoordinates->size[0] = c1_varargin_1->size[0] * 181;
    c1_peakCoordinates->size[1] = 2;
    c1_emxEnsureCapacity_real_T(chartInstance, c1_peakCoordinates, c1_i666,
      &c1_ig_emlrtRTEI);
    c1_peakIdx = 0;
    c1_b_ex = -90.0;
    for (c1_r_k = 1; c1_r_k + 1 < 181; c1_r_k++) {
      if (c1_b_ex > -90.0 + (real_T)c1_r_k) {
        c1_b_ex = -90.0 + (real_T)c1_r_k;
      }
    }

    c1_minTheta = c1_b_ex;
    c1_c_ex = -90.0;
    for (c1_s_k = 1; c1_s_k + 1 < 181; c1_s_k++) {
      if (c1_c_ex < -90.0 + (real_T)c1_s_k) {
        c1_c_ex = -90.0 + (real_T)c1_s_k;
      }
    }

    c1_maxTheta = c1_c_ex;
    c1_bb_x = c1_maxTheta - c1_minTheta;
    c1_cb_x = c1_bb_x;
    c1_db_x = c1_cb_x;
    c1_bb_y = muDoubleScalarAbs(c1_db_x);
    c1_thetaResolution = c1_bb_y / 179.0;
    c1_eb_x = c1_minTheta + c1_thetaResolution * c1_nhoodSizeDefault[1];
    c1_fb_x = c1_eb_x;
    c1_gb_x = c1_fb_x;
    c1_cb_y = muDoubleScalarAbs(c1_gb_x);
    c1_isThetaAntisymmetric = (c1_cb_y <= c1_maxTheta);
    while (!c1_isDone) {
      c1_iPeak = 0;
      c1_jPeak = 0;
      c1_maxVal = -1.0;
      c1_b_M = c1_hnew->size[0];
      for (c1_j = 0; c1_j < 181; c1_j++) {
        c1_b_j = c1_j;
        c1_k_b = c1_b_M;
        c1_l_b = c1_k_b;
        if (1 > c1_l_b) {
          c1_overflow = false;
        } else {
          c1_overflow = (c1_l_b > 2147483646);
        }

        if (c1_overflow) {
          c1_check_forloop_overflow_error(chartInstance, true);
        }

        for (c1_i = 1; c1_i - 1 < c1_b_M; c1_i++) {
          c1_b_i = c1_i;
          c1_val = c1_hnew->data[(sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c1_b_i, 1, c1_hnew->size[0]) + c1_hnew->size[0] *
            c1_b_j) - 1];
          if (c1_val > c1_maxVal) {
            c1_iPeak = c1_b_i;
            c1_jPeak = c1_b_j + 1;
            c1_maxVal = c1_val;
          }
        }
      }

      if (c1_hnew->data[(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_iPeak, 1, c1_hnew->
            size[0]) + c1_hnew->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c1_jPeak, 1, 181) - 1)) - 1] >= c1_b_threshold) {
        c1_peakIdx++;
        c1_peakCoordinates->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c1_peakIdx, 1, c1_peakCoordinates->size[0]) - 1] = (real_T)c1_iPeak;
        c1_peakCoordinates->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c1_peakIdx, 1, c1_peakCoordinates->size[0]) +
          c1_peakCoordinates->size[0]) - 1] = (real_T)c1_jPeak;
        c1_rhoMin = c1_iPeak - c1_nhoodCenter_i;
        c1_rhoMax = c1_iPeak + c1_nhoodCenter_i;
        c1_thetaMin = c1_jPeak - c1_nhoodCenter_j;
        c1_thetaMax = c1_jPeak + c1_nhoodCenter_j;
        if (c1_rhoMin < 1) {
          c1_rhoMin = 1;
        }

        if (c1_rhoMax > c1_numRowH) {
          c1_rhoMax = c1_numRowH;
        }

        c1_d_a = c1_thetaMin;
        c1_m_b = c1_thetaMax;
        c1_e_a = c1_d_a;
        c1_n_b = c1_m_b;
        if (c1_e_a > c1_n_b) {
          c1_b_overflow = false;
        } else {
          c1_b_overflow = (c1_n_b > 2147483646);
        }

        if (c1_b_overflow) {
          c1_check_forloop_overflow_error(chartInstance, true);
        }

        for (c1_theta = c1_thetaMin; c1_theta <= c1_thetaMax; c1_theta++) {
          c1_f_a = c1_rhoMin;
          c1_o_b = c1_rhoMax;
          c1_g_a = c1_f_a;
          c1_p_b = c1_o_b;
          if (c1_g_a > c1_p_b) {
            c1_c_overflow = false;
          } else {
            c1_c_overflow = (c1_p_b > 2147483646);
          }

          if (c1_c_overflow) {
            c1_check_forloop_overflow_error(chartInstance, true);
          }

          for (c1_rho = c1_rhoMin; c1_rho <= c1_rhoMax; c1_rho++) {
            c1_rhoToRemove = c1_rho - 1;
            c1_thetaToRemove = c1_theta;
            if (c1_isThetaAntisymmetric) {
              if (c1_theta > 181) {
                c1_rhoToRemove = c1_numRowH - c1_rho;
                c1_thetaToRemove = c1_theta - 181;
              } else {
                if (c1_theta < 1) {
                  c1_rhoToRemove = c1_numRowH - c1_rho;
                  c1_thetaToRemove = c1_theta + 181;
                }
              }
            }

            if ((c1_thetaToRemove > 181) || (c1_thetaToRemove < 1)) {
            } else {
              c1_hnew->data[(sf_eml_array_bounds_check
                             (sfGlobalDebugInstanceStruct, chartInstance->S, 1U,
                              0, 0, MAX_uint32_T, c1_rhoToRemove + 1, 1,
                              c1_hnew->size[0]) + c1_hnew->size[0] *
                             (c1_thetaToRemove - 1)) - 1] = 0.0;
            }
          }
        }

        c1_isDone = (c1_peakIdx == 1);
      } else {
        c1_isDone = true;
      }
    }

    c1_emxFree_real_T(chartInstance, &c1_hnew);
    if (c1_peakIdx == 0) {
      c1_i667 = c1_peaks->size[0] * c1_peaks->size[1];
      c1_peaks->size[0] = 0;
      c1_peaks->size[1] = 0;
      c1_i670 = c1_peaks->size[0];
      c1_i671 = c1_peaks->size[1];
    } else {
      sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S,
        1U, 0, 0, MAX_uint32_T, 1, 1, c1_peakCoordinates->size[0]);
      c1_i668 = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_peakIdx, 1,
        c1_peakCoordinates->size[0]);
      c1_i669 = c1_peaks->size[0] * c1_peaks->size[1];
      c1_peaks->size[0] = c1_i668;
      c1_peaks->size[1] = 2;
      c1_emxEnsureCapacity_real_T(chartInstance, c1_peaks, c1_i669,
        &c1_jg_emlrtRTEI);
      for (c1_i672 = 0; c1_i672 < 2; c1_i672++) {
        c1_c_loop_ub = c1_i668 - 1;
        for (c1_i673 = 0; c1_i673 <= c1_c_loop_ub; c1_i673++) {
          c1_peaks->data[c1_i673 + c1_peaks->size[0] * c1_i672] =
            c1_peakCoordinates->data[c1_i673 + c1_peakCoordinates->size[0] *
            c1_i672];
        }
      }
    }

    c1_emxFree_real_T(chartInstance, &c1_peakCoordinates);
  }
}

static void c1_validateattributes(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_a)
{
  boolean_T c1_b60;
  const mxArray *c1_y = NULL;
  static char_T c1_cv58[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'p', 'e', 'a', 'k', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  boolean_T c1_p;
  const mxArray *c1_b_y = NULL;
  real_T c1_d31;
  int32_T c1_i674;
  const mxArray *c1_c_y = NULL;
  int32_T c1_k;
  static char_T c1_cv59[18] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '1', ',', ' ', 'H', ',' };

  real_T c1_b_k;
  real_T c1_x;
  boolean_T c1_b61;
  real_T c1_b_x;
  boolean_T c1_b;
  boolean_T c1_b62;
  const mxArray *c1_d_y = NULL;
  real_T c1_c_x;
  boolean_T c1_b_p;
  boolean_T c1_b_b;
  const mxArray *c1_e_y = NULL;
  real_T c1_d32;
  boolean_T c1_b63;
  int32_T c1_i675;
  boolean_T c1_c_b;
  const mxArray *c1_f_y = NULL;
  int32_T c1_c_k;
  static char_T c1_cv60[18] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '1', ',', ' ', 'H', ',' };

  real_T c1_d_k;
  real_T c1_d_x;
  boolean_T c1_b64;
  real_T c1_e_x;
  real_T c1_f_x;
  boolean_T c1_d_b;
  const mxArray *c1_g_y = NULL;
  boolean_T c1_b65;
  real_T c1_g_x;
  const mxArray *c1_h_y = NULL;
  boolean_T c1_e_b;
  boolean_T c1_b66;
  const mxArray *c1_i_y = NULL;
  boolean_T c1_f_b;
  static char_T c1_cv61[18] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '1', ',', ' ', 'H', ',' };

  real_T c1_h_x;
  boolean_T c1_c_p;
  real_T c1_i_x;
  boolean_T c1_d_p;
  boolean_T exitg1;
  (void)chartInstance;
  c1_b60 = (c1_a->size[0] == 0);
  if (!c1_b60) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv58, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv7, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv59, 10, 0U, 1U, 0U, 2, 1, 18),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c1_b_y, 14, c1_c_y)));
  }

  c1_p = true;
  c1_d31 = (real_T)(c1_a->size[0] * 181);
  c1_i674 = (int32_T)c1_d31 - 1;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k <= c1_i674)) {
    c1_b_k = 1.0 + (real_T)c1_k;
    c1_x = c1_a->data[(int32_T)c1_b_k - 1];
    c1_b_x = c1_x;
    c1_b = muDoubleScalarIsInf(c1_b_x);
    c1_b62 = !c1_b;
    c1_c_x = c1_x;
    c1_b_b = muDoubleScalarIsNaN(c1_c_x);
    c1_b63 = !c1_b_b;
    c1_c_b = (c1_b62 && c1_b63);
    if (c1_c_b) {
      c1_k++;
    } else {
      c1_p = false;
      exitg1 = true;
    }
  }

  if (c1_p) {
    c1_b61 = true;
  } else {
    c1_b61 = false;
  }

  if (c1_b61) {
  } else {
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv9, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv60, 10, 0U, 1U, 0U, 2, 1, 18),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_e_y, 14, c1_f_y)));
  }

  c1_b_p = true;
  c1_d32 = (real_T)(c1_a->size[0] * 181);
  c1_i675 = (int32_T)c1_d32 - 1;
  c1_c_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_c_k <= c1_i675)) {
    c1_d_k = 1.0 + (real_T)c1_c_k;
    c1_d_x = c1_a->data[(int32_T)c1_d_k - 1];
    c1_e_x = c1_d_x;
    c1_f_x = c1_e_x;
    c1_d_b = muDoubleScalarIsInf(c1_f_x);
    c1_b65 = !c1_d_b;
    c1_g_x = c1_e_x;
    c1_e_b = muDoubleScalarIsNaN(c1_g_x);
    c1_b66 = !c1_e_b;
    c1_f_b = (c1_b65 && c1_b66);
    if (c1_f_b) {
      c1_h_x = c1_d_x;
      c1_i_x = c1_h_x;
      c1_i_x = muDoubleScalarFloor(c1_i_x);
      if (c1_i_x == c1_d_x) {
        c1_c_p = true;
      } else {
        c1_c_p = false;
      }
    } else {
      c1_c_p = false;
    }

    c1_d_p = c1_c_p;
    if (c1_d_p) {
      c1_c_k++;
    } else {
      c1_b_p = false;
      exitg1 = true;
    }
  }

  if (c1_b_p) {
    c1_b64 = true;
  } else {
    c1_b64 = false;
  }

  if (c1_b64) {
  } else {
    c1_g_y = NULL;
    sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv11, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c1_h_y = NULL;
    sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv5, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c1_i_y = NULL;
    sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv61, 10, 0U, 1U, 0U, 2, 1, 18),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_g_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_h_y, 14, c1_i_y)));
  }
}

static void c1_diff(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T c1_x[179],
                    real_T c1_y[178])
{
  int32_T c1_ixLead;
  int32_T c1_iyLead;
  real_T c1_work;
  int32_T c1_m;
  real_T c1_tmp1;
  real_T c1_tmp2;
  (void)chartInstance;
  c1_ixLead = 1;
  c1_iyLead = 0;
  c1_work = c1_x[0];
  for (c1_m = 0; c1_m < 178; c1_m++) {
    c1_tmp1 = c1_x[c1_ixLead];
    c1_tmp2 = c1_work;
    c1_work = c1_tmp1;
    c1_tmp1 -= c1_tmp2;
    c1_ixLead++;
    c1_y[c1_iyLead] = c1_tmp1;
    c1_iyLead++;
  }
}

static void c1_houghlines(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T *c1_varargin_3,
  c1_emxArray_real_T *c1_varargin_4, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh
  *c1_lines)
{
  boolean_T c1_b67;
  boolean_T c1_b68;
  const mxArray *c1_y = NULL;
  static char_T c1_cv62[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  boolean_T c1_p;
  const mxArray *c1_b_y = NULL;
  real_T c1_d33;
  int32_T c1_i676;
  const mxArray *c1_c_y = NULL;
  int32_T c1_k;
  real_T c1_b_k;
  real_T c1_x;
  boolean_T c1_b69;
  real_T c1_b_x;
  boolean_T c1_b;
  boolean_T c1_b70;
  const mxArray *c1_d_y = NULL;
  real_T c1_c_x;
  static char_T c1_cv63[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'F', 'i', 'n', 'i', 't', 'e' };

  boolean_T c1_b71;
  boolean_T c1_b_b;
  const mxArray *c1_e_y = NULL;
  boolean_T c1_b72;
  const mxArray *c1_f_y = NULL;
  boolean_T c1_c_b;
  const mxArray *c1_g_y = NULL;
  static char_T c1_cv64[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'N', 'o', 'n', 'e', 'm', 'p', 't', 'y' };

  boolean_T c1_b_p;
  static char_T c1_cv65[20] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '3', ',', ' ', 'R', 'H', 'O', ',' };

  const mxArray *c1_h_y = NULL;
  real_T c1_d34;
  int32_T c1_i677;
  const mxArray *c1_i_y = NULL;
  int32_T c1_c_k;
  static char_T c1_cv66[20] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '3', ',', ' ', 'R', 'H', 'O', ',' };

  real_T c1_d_k;
  real_T c1_d_x;
  boolean_T c1_b73;
  boolean_T c1_c_p;
  const mxArray *c1_j_y = NULL;
  static char_T c1_cv67[34] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'P', 'o', 's', 'i', 't', 'i', 'v', 'e' };

  boolean_T c1_d_p;
  const mxArray *c1_k_y = NULL;
  real_T c1_d35;
  int32_T c1_i678;
  const mxArray *c1_l_y = NULL;
  int32_T c1_e_k;
  static char_T c1_cv68[22] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '4', ',', ' ', 'P', 'E', 'A', 'K', 'S', ',' };

  real_T c1_f_k;
  real_T c1_e_x;
  boolean_T c1_b74;
  real_T c1_f_x;
  real_T c1_g_x;
  boolean_T c1_d_b;
  const mxArray *c1_m_y = NULL;
  boolean_T c1_b75;
  static char_T c1_cv69[33] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e',
    'd', 'I', 'n', 't', 'e', 'g', 'e', 'r' };

  real_T c1_h_x;
  const mxArray *c1_n_y = NULL;
  const mxArray *c1_o_y = NULL;
  boolean_T c1_e_b;
  static char_T c1_cv70[30] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'h', 'o', 'u',
    'g', 'h', 'l', 'i', 'n', 'e', 's', ':', 'i', 'n', 'v', 'a', 'l', 'i', 'd',
    'P', 'E', 'A', 'K', 'S' };

  c1_emxArray_real_T *c1_peaks;
  boolean_T c1_b76;
  const mxArray *c1_p_y = NULL;
  const mxArray *c1_q_y = NULL;
  int32_T c1_i679;
  boolean_T c1_f_b;
  static char_T c1_cv71[22] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '4', ',', ' ', 'P', 'E', 'A', 'K', 'S', ',' };

  real_T c1_i_x;
  boolean_T c1_e_p;
  real_T c1_j_x;
  int32_T c1_i680;
  int32_T c1_i681;
  boolean_T c1_f_p;
  int32_T c1_loop_ub;
  int32_T c1_i682;
  real_T c1_numCol;
  real_T c1_numRow;
  int32_T c1_numNonZero;
  int32_T c1_i683;
  int32_T c1_j;
  c1_emxArray_int32_T *c1_nonZeroPixels;
  real_T c1_b_j;
  c1_emxArray_real_T *c1_r16;
  int32_T c1_i684;
  int32_T c1_i685;
  int32_T c1_i;
  real_T c1_b_i;
  int32_T c1_i686;
  int32_T c1_i687;
  int32_T c1_b_loop_ub;
  int32_T c1_i688;
  int32_T c1_i689;
  real_T c1_g_k;
  int32_T c1_i690;
  int32_T c1_c_j;
  c1_emxArray_int32_T *c1_point1Array;
  c1_emxArray_int32_T *c1_point2Array;
  int32_T c1_i691;
  c1_emxArray_real32_T *c1_thetaArray;
  int32_T c1_c_i;
  c1_emxArray_real32_T *c1_rhoArray;
  int32_T c1_numLines;
  int32_T c1_i692;
  int32_T c1_i693;
  int32_T c1_i694;
  int32_T c1_i695;
  int32_T c1_i696;
  int32_T c1_i697;
  int32_T c1_i698;
  int32_T c1_i699;
  real_T c1_firstRho;
  real_T c1_numRho;
  real_T c1_lastRho;
  real_T c1_slope;
  real_T c1_numPeaks;
  int32_T c1_i700;
  int32_T c1_peakIdx;
  c1_emxArray_real_T *c1_indices;
  c1_emxArray_int32_T *c1_houghPix;
  c1_emxArray_int32_T *c1_rhoBinIdx;
  c1_emxArray_int32_T *c1_b_houghPix;
  c1_emxArray_real_T *c1_distances2;
  c1_emxArray_int32_T *c1_b_point1Array;
  c1_emxArray_int32_T *c1_b_point2Array;
  c1_emxArray_real32_T *c1_b_thetaArray;
  c1_emxArray_real32_T *c1_b_rhoArray;
  real_T c1_b_peakIdx;
  int32_T c1_peak1;
  int32_T c1_peak2;
  real_T c1_b_firstRho;
  real_T c1_b_slope;
  int32_T c1_b_peak1;
  int32_T c1_b_peak2;
  real_T c1_b_numNonZero;
  int32_T c1_i701;
  int32_T c1_c_loop_ub;
  c1_emxArray_boolean_T *c1_tile;
  int32_T c1_i702;
  int32_T c1_b_numLines;
  int32_T c1_c_numLines;
  int32_T c1_varargin_2;
  int32_T c1_i703;
  int32_T c1_i704;
  int32_T c1_numHoughPix;
  real_T c1_thetaVal;
  int32_T c1_i705;
  real_T c1_k_x;
  int32_T c1_i706;
  real_T c1_cosTheta;
  int32_T c1_d_loop_ub;
  int32_T c1_i707;
  real_T c1_l_x;
  real_T c1_sinTheta;
  int32_T c1_outsize[2];
  int32_T c1_i708;
  int32_T c1_h_k;
  const mxArray *c1_r_y = NULL;
  int32_T c1_i709;
  real_T c1_i_k;
  int32_T c1_i710;
  int32_T c1_i711;
  real_T c1_rhoVal;
  real_T c1_m_x;
  int32_T c1_i712;
  c1_skoeQIuVNKJRHNtBIlOCZhD c1_s;
  int32_T c1_s_y;
  int32_T c1_i713;
  int32_T c1_i714;
  int32_T c1_i715;
  int32_T c1_i716;
  int32_T c1_e_loop_ub;
  int32_T c1_i717;
  int32_T c1_i718;
  real_T c1_b_numHoughPix;
  int32_T c1_i719;
  int32_T c1_i720;
  int32_T c1_i721;
  real_T c1_numPairs;
  int32_T c1_i722;
  real_T c1_d36;
  int32_T c1_f_loop_ub;
  int32_T c1_i723;
  int32_T c1_i724;
  real_T c1_n;
  int32_T c1_j_k;
  int32_T c1_i725;
  int32_T c1_k_k;
  int32_T c1_g_b;
  int32_T c1_i726;
  real_T c1_l_k;
  int32_T c1_h_b;
  real_T c1_rowMax;
  boolean_T c1_overflow;
  real_T c1_rowMin;
  real_T c1_colMax;
  real_T c1_colMin;
  real_T c1_b_n;
  int32_T c1_a;
  int32_T c1_i_b;
  real_T c1_d37;
  int32_T c1_b_a;
  int32_T c1_m_k;
  int32_T c1_j_b;
  int32_T c1_i727;
  int32_T c1_c_a;
  int32_T c1_n_k;
  int32_T c1_n_x;
  boolean_T c1_b_overflow;
  int32_T c1_d_a;
  int32_T c1_o_k;
  int32_T c1_c;
  int32_T c1_i728;
  real_T c1_d38;
  int32_T c1_bu;
  int32_T c1_iv8[1];
  int32_T c1_i729;
  int32_T c1_i730;
  int32_T c1_p_k;
  int32_T c1_q_k;
  int32_T c1_r_k;
  int32_T c1_i731;
  real_T c1_rowRange;
  int32_T c1_s_k;
  real_T c1_t_k;
  real_T c1_colRange;
  real_T c1_r;
  int32_T c1_i732;
  int32_T c1_e_a;
  int32_T c1_i733;
  int32_T c1_b_indices;
  int32_T c1_f_a;
  int32_T c1_iv9[1];
  int32_T c1_i734;
  int32_T c1_i735;
  int32_T c1_point1_size[2];
  int32_T c1_g_a;
  int32_T c1_i736;
  int32_T c1_o_x;
  int32_T c1_u_k;
  int32_T c1_g_loop_ub;
  int32_T c1_h_a;
  int32_T c1_i737;
  real_T c1_sortingOrder[2];
  real_T c1_b_c;
  int32_T c1_i738;
  int32_T c1_c_c;
  int32_T c1_b_bu;
  int32_T c1_i739;
  int32_T c1_i740;
  int32_T c1_point1_data[2];
  int32_T c1_c_indices;
  int32_T c1_point2_size[2];
  int32_T c1_i741;
  int32_T c1_i742;
  int32_T c1_h_loop_ub;
  int32_T c1_i_loop_ub;
  int32_T c1_i743;
  int32_T c1_i744;
  int32_T c1_point2_data[2];
  int32_T c1_i_a;
  int32_T c1_j_a;
  int32_T c1_k_a;
  int32_T c1_p_x;
  int32_T c1_l_a;
  int32_T c1_d_c;
  int32_T c1_c_bu;
  int32_T c1_m_a;
  int32_T c1_n_a;
  int32_T c1_o_a;
  int32_T c1_q_x;
  int32_T c1_p_a;
  int32_T c1_e_c;
  int32_T c1_d_bu;
  int32_T c1_lineLength2;
  int32_T c1_point1[1];
  int32_T c1_b_point1[1];
  int32_T c1_i745;
  int32_T c1_i746;
  int32_T c1_j_loop_ub;
  int32_T c1_i747;
  int32_T c1_i748;
  int32_T c1_i749;
  int32_T c1_i750;
  int32_T c1_k_loop_ub;
  int32_T c1_i751;
  int32_T c1_point2[1];
  int32_T c1_b_point2[1];
  int32_T c1_i752;
  int32_T c1_i753;
  int32_T c1_l_loop_ub;
  int32_T c1_i754;
  int32_T c1_i755;
  int32_T c1_i756;
  int32_T c1_i757;
  int32_T c1_m_loop_ub;
  int32_T c1_i758;
  int32_T c1_i759;
  int32_T c1_n_loop_ub;
  int32_T c1_i760;
  int32_T c1_i761;
  int32_T c1_o_loop_ub;
  int32_T c1_i762;
  int32_T c1_b_varargin_3[1];
  int32_T c1_i763;
  int32_T c1_p_loop_ub;
  int32_T c1_i764;
  int32_T c1_i765;
  int32_T c1_q_loop_ub;
  int32_T c1_i766;
  boolean_T exitg1;
  int32_T exitg2;
  c1_b67 = (c1_varargin_1->size[0] == 0);
  c1_b68 = (c1_varargin_1->size[1] == 0);
  if ((!c1_b67) && (!c1_b68)) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv62, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv7, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv8, 10, 0U, 1U, 0U, 2, 1, 19),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      2U, 14, c1_b_y, 14, c1_c_y)));
  }

  c1_p = true;
  c1_d33 = (real_T)c1_varargin_3->size[1];
  c1_i676 = (int32_T)c1_d33 - 1;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k <= c1_i676)) {
    c1_b_k = 1.0 + (real_T)c1_k;
    c1_x = c1_varargin_3->data[(int32_T)c1_b_k - 1];
    c1_b_x = c1_x;
    c1_b = muDoubleScalarIsInf(c1_b_x);
    c1_b70 = !c1_b;
    c1_c_x = c1_x;
    c1_b_b = muDoubleScalarIsNaN(c1_c_x);
    c1_b72 = !c1_b_b;
    c1_c_b = (c1_b70 && c1_b72);
    if (c1_c_b) {
      c1_k++;
    } else {
      c1_p = false;
      exitg1 = true;
    }
  }

  if (c1_p) {
    c1_b69 = true;
  } else {
    c1_b69 = false;
  }

  if (c1_b69) {
  } else {
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv63, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv10, 10, 0U, 1U, 0U, 2, 1, 46),
                  false);
    c1_g_y = NULL;
    sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv65, 10, 0U, 1U, 0U, 2, 1, 20),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_d_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_e_y, 14, c1_g_y)));
  }

  c1_b71 = (c1_varargin_3->size[1] == 0);
  if (!c1_b71) {
  } else {
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv64, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c1_h_y = NULL;
    sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv7, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_i_y = NULL;
    sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv66, 10, 0U, 1U, 0U, 2, 1, 20),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_f_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_h_y, 14, c1_i_y)));
  }

  c1_b_p = true;
  c1_d34 = (real_T)(c1_varargin_4->size[0] * c1_varargin_4->size[1]);
  c1_i677 = (int32_T)c1_d34 - 1;
  c1_c_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_c_k <= c1_i677)) {
    c1_d_k = 1.0 + (real_T)c1_c_k;
    c1_d_x = c1_varargin_4->data[(int32_T)c1_d_k - 1];
    c1_c_p = !(c1_d_x <= 0.0);
    if (c1_c_p) {
      c1_c_k++;
    } else {
      c1_b_p = false;
      exitg1 = true;
    }
  }

  if (c1_b_p) {
    c1_b73 = true;
  } else {
    c1_b73 = false;
  }

  if (c1_b73) {
  } else {
    c1_j_y = NULL;
    sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv67, 10, 0U, 1U, 0U, 2, 1, 34),
                  false);
    c1_k_y = NULL;
    sf_mex_assign(&c1_k_y, sf_mex_create("y", c1_cv12, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_l_y = NULL;
    sf_mex_assign(&c1_l_y, sf_mex_create("y", c1_cv68, 10, 0U, 1U, 0U, 2, 1, 22),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_k_y, 14, c1_l_y)));
  }

  c1_d_p = true;
  c1_d35 = (real_T)(c1_varargin_4->size[0] * c1_varargin_4->size[1]);
  c1_i678 = (int32_T)c1_d35 - 1;
  c1_e_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_e_k <= c1_i678)) {
    c1_f_k = 1.0 + (real_T)c1_e_k;
    c1_e_x = c1_varargin_4->data[(int32_T)c1_f_k - 1];
    c1_f_x = c1_e_x;
    c1_g_x = c1_f_x;
    c1_d_b = muDoubleScalarIsInf(c1_g_x);
    c1_b75 = !c1_d_b;
    c1_h_x = c1_f_x;
    c1_e_b = muDoubleScalarIsNaN(c1_h_x);
    c1_b76 = !c1_e_b;
    c1_f_b = (c1_b75 && c1_b76);
    if (c1_f_b) {
      c1_i_x = c1_e_x;
      c1_j_x = c1_i_x;
      c1_j_x = muDoubleScalarFloor(c1_j_x);
      if (c1_j_x == c1_e_x) {
        c1_e_p = true;
      } else {
        c1_e_p = false;
      }
    } else {
      c1_e_p = false;
    }

    c1_f_p = c1_e_p;
    if (c1_f_p) {
      c1_e_k++;
    } else {
      c1_d_p = false;
      exitg1 = true;
    }
  }

  if (c1_d_p) {
    c1_b74 = true;
  } else {
    c1_b74 = false;
  }

  if (c1_b74) {
  } else {
    c1_m_y = NULL;
    sf_mex_assign(&c1_m_y, sf_mex_create("y", c1_cv69, 10, 0U, 1U, 0U, 2, 1, 33),
                  false);
    c1_n_y = NULL;
    sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv5, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c1_p_y = NULL;
    sf_mex_assign(&c1_p_y, sf_mex_create("y", c1_cv71, 10, 0U, 1U, 0U, 2, 1, 22),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_m_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_n_y, 14, c1_p_y)));
  }

  if (!((real_T)c1_varargin_4->size[1] != 2.0)) {
  } else {
    c1_o_y = NULL;
    sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv70, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_q_y = NULL;
    sf_mex_assign(&c1_q_y, sf_mex_create("y", c1_cv70, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_o_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_q_y)));
  }

  c1_emxInit_real_T(chartInstance, &c1_peaks, 2, &c1_kh_emlrtRTEI);
  c1_i679 = c1_peaks->size[0] * c1_peaks->size[1];
  c1_peaks->size[0] = c1_varargin_4->size[0];
  c1_peaks->size[1] = c1_varargin_4->size[1];
  c1_emxEnsureCapacity_real_T(chartInstance, c1_peaks, c1_i679, &c1_lg_emlrtRTEI);
  c1_i680 = c1_peaks->size[0];
  c1_i681 = c1_peaks->size[1];
  c1_loop_ub = c1_varargin_4->size[0] * c1_varargin_4->size[1] - 1;
  for (c1_i682 = 0; c1_i682 <= c1_loop_ub; c1_i682++) {
    c1_peaks->data[c1_i682] = c1_varargin_4->data[c1_i682];
  }

  c1_numCol = (real_T)c1_varargin_1->size[1];
  c1_numRow = (real_T)c1_varargin_1->size[0];
  c1_numNonZero = 0;
  c1_i683 = (int32_T)c1_numCol - 1;
  for (c1_j = 0; c1_j <= c1_i683; c1_j++) {
    c1_b_j = 1.0 + (real_T)c1_j;
    c1_i684 = (int32_T)c1_numRow - 1;
    for (c1_i = 0; c1_i <= c1_i684; c1_i++) {
      c1_b_i = 1.0 + (real_T)c1_i;
      c1_numNonZero += ((real_T)c1_varargin_1->data[(sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c1_b_i, 1, c1_varargin_1->size[0]) + c1_varargin_1->size[0] *
        (sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct, chartInstance->S,
        1U, 0, 0, MAX_uint32_T, (int32_T)c1_b_j, 1, c1_varargin_1->size[1]) - 1))
                        - 1] > 0.0);
    }
  }

  c1_emxInit_int32_T1(chartInstance, &c1_nonZeroPixels, 2, &c1_ng_emlrtRTEI);
  c1_emxInit_real_T(chartInstance, &c1_r16, 2, &c1_mg_emlrtRTEI);
  c1_i685 = c1_r16->size[0] * c1_r16->size[1];
  c1_r16->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_numNonZero);
  c1_r16->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_r16, c1_i685, &c1_mg_emlrtRTEI);
  c1_i686 = c1_r16->size[0];
  c1_i687 = c1_r16->size[1];
  c1_b_loop_ub = ((int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_numNonZero) <<
                  1) - 1;
  for (c1_i688 = 0; c1_i688 <= c1_b_loop_ub; c1_i688++) {
    c1_r16->data[c1_i688] = 0.0;
  }

  c1_i689 = c1_nonZeroPixels->size[0] * c1_nonZeroPixels->size[1];
  c1_nonZeroPixels->size[0] = c1_r16->size[0];
  c1_nonZeroPixels->size[1] = c1_r16->size[1];
  c1_emxEnsureCapacity_int32_T1(chartInstance, c1_nonZeroPixels, c1_i689,
    &c1_ng_emlrtRTEI);
  c1_g_k = 0.0;
  c1_i690 = (int32_T)c1_numCol - 1;
  for (c1_c_j = 0; c1_c_j <= c1_i690; c1_c_j++) {
    c1_b_j = 1.0 + (real_T)c1_c_j;
    c1_i691 = (int32_T)c1_numRow - 1;
    for (c1_c_i = 0; c1_c_i <= c1_i691; c1_c_i++) {
      c1_b_i = 1.0 + (real_T)c1_c_i;
      if ((real_T)c1_varargin_1->data[(sf_eml_array_bounds_check
           (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
            MAX_uint32_T, (int32_T)c1_b_i, 1, c1_varargin_1->size[0]) +
           c1_varargin_1->size[0] * (sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_b_j, 1, c1_varargin_1->size[1]) - 1)) - 1]
          > 0.0) {
        c1_g_k++;
        c1_nonZeroPixels->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_g_k, 1, c1_nonZeroPixels->size[0]) - 1] = (int32_T)
          (c1_b_j - 1.0);
        c1_nonZeroPixels->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_g_k, 1, c1_nonZeroPixels->size[0]) +
          c1_nonZeroPixels->size[0]) - 1] = (int32_T)(c1_b_i - 1.0);
      }
    }
  }

  c1_emxInit_int32_T1(chartInstance, &c1_point1Array, 2, &c1_fh_emlrtRTEI);
  c1_emxInit_int32_T1(chartInstance, &c1_point2Array, 2, &c1_gh_emlrtRTEI);
  c1_emxInit_real32_T1(chartInstance, &c1_thetaArray, 1, &c1_hh_emlrtRTEI);
  c1_emxInit_real32_T1(chartInstance, &c1_rhoArray, 1, &c1_ih_emlrtRTEI);
  c1_numLines = 0;
  c1_i692 = c1_point1Array->size[0] * c1_point1Array->size[1];
  c1_point1Array->size[0] = 0;
  c1_point1Array->size[1] = 2;
  c1_i693 = c1_point1Array->size[0];
  c1_i694 = c1_point1Array->size[1];
  c1_i695 = c1_point2Array->size[0] * c1_point2Array->size[1];
  c1_point2Array->size[0] = 0;
  c1_point2Array->size[1] = 2;
  c1_i696 = c1_point2Array->size[0];
  c1_i697 = c1_point2Array->size[1];
  c1_i698 = c1_thetaArray->size[0];
  c1_thetaArray->size[0] = 0;
  c1_i699 = c1_rhoArray->size[0];
  c1_rhoArray->size[0] = 0;
  (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
    chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_varargin_3->size[1]);
  c1_firstRho = c1_varargin_3->data[0];
  c1_numRho = (real_T)c1_varargin_3->size[1];
  c1_lastRho = c1_varargin_3->data[sf_eml_array_bounds_check
    (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
     (int32_T)c1_numRho, 1, c1_varargin_3->size[1]) - 1];
  c1_slope = (c1_numRho - 1.0) / (c1_lastRho - c1_firstRho);
  c1_numPeaks = (real_T)c1_peaks->size[0];
  c1_i700 = (int32_T)c1_numPeaks - 1;
  c1_peakIdx = 0;
  c1_emxInit_real_T1(chartInstance, &c1_indices, 1, &c1_jh_emlrtRTEI);
  c1_emxInit_int32_T1(chartInstance, &c1_houghPix, 2, &c1_lh_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_rhoBinIdx, 1, &c1_mh_emlrtRTEI);
  c1_emxInit_int32_T1(chartInstance, &c1_b_houghPix, 2, &c1_nh_emlrtRTEI);
  c1_emxInit_real_T1(chartInstance, &c1_distances2, 1, &c1_oh_emlrtRTEI);
  c1_emxInit_int32_T1(chartInstance, &c1_b_point1Array, 2, &c1_wg_emlrtRTEI);
  c1_emxInit_int32_T1(chartInstance, &c1_b_point2Array, 2, &c1_yg_emlrtRTEI);
  c1_emxInit_real32_T1(chartInstance, &c1_b_thetaArray, 1, &c1_bh_emlrtRTEI);
  c1_emxInit_real32_T1(chartInstance, &c1_b_rhoArray, 1, &c1_dh_emlrtRTEI);
  while (c1_peakIdx <= c1_i700) {
    c1_b_peakIdx = 1.0 + (real_T)c1_peakIdx;
    c1_peak1 = (int32_T)c1_peaks->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       (int32_T)c1_b_peakIdx, 1, c1_peaks->size[0]) - 1];
    c1_peak2 = (int32_T)c1_peaks->data[(sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       (int32_T)c1_b_peakIdx, 1, c1_peaks->size[0]) + c1_peaks->size[0]) - 1];
    c1_b_firstRho = c1_firstRho;
    c1_b_slope = c1_slope;
    c1_b_peak1 = c1_peak1;
    c1_b_peak2 = c1_peak2;
    c1_b_numNonZero = (real_T)c1_nonZeroPixels->size[0];
    c1_i701 = c1_distances2->size[0];
    c1_distances2->size[0] = (int32_T)c1_b_numNonZero;
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_distances2, c1_i701,
      &c1_og_emlrtRTEI);
    c1_c_loop_ub = (int32_T)c1_b_numNonZero - 1;
    for (c1_i702 = 0; c1_i702 <= c1_c_loop_ub; c1_i702++) {
      c1_distances2->data[c1_i702] = 0.0;
    }

    c1_i703 = c1_rhoBinIdx->size[0];
    c1_rhoBinIdx->size[0] = c1_distances2->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_rhoBinIdx, c1_i703,
      &c1_pg_emlrtRTEI);
    c1_numHoughPix = 0;
    c1_thetaVal = (-45.0 + 0.5 * (real_T)(sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       c1_b_peak2, 1, 181) - 1)) * 3.1415926535897931 / 180.0;
    c1_k_x = c1_thetaVal;
    c1_cosTheta = c1_k_x;
    c1_cosTheta = muDoubleScalarCos(c1_cosTheta);
    c1_l_x = c1_thetaVal;
    c1_sinTheta = c1_l_x;
    c1_sinTheta = muDoubleScalarSin(c1_sinTheta);
    c1_i708 = (int32_T)c1_b_numNonZero - 1;
    for (c1_h_k = 0; c1_h_k <= c1_i708; c1_h_k++) {
      c1_i_k = 1.0 + (real_T)c1_h_k;
      c1_rhoVal = (real_T)c1_nonZeroPixels->data[sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c1_i_k, 1, c1_nonZeroPixels->size[0]) - 1] * c1_cosTheta +
        (real_T)c1_nonZeroPixels->data[(sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c1_i_k, 1, c1_nonZeroPixels->size[0]) + c1_nonZeroPixels->
        size[0]) - 1] * c1_sinTheta;
      c1_m_x = c1_b_slope * (c1_rhoVal - c1_b_firstRho) + 1.0;
      c1_s_y = (int32_T)(c1_m_x + 0.5);
      c1_rhoBinIdx->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_i_k, 1,
        c1_rhoBinIdx->size[0]) - 1] = c1_s_y;
      if (c1_rhoBinIdx->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_i_k, 1, c1_rhoBinIdx->size[0]) - 1] == c1_b_peak1) {
        c1_numHoughPix++;
      }
    }

    if (c1_numHoughPix < 1) {
      c1_i711 = c1_houghPix->size[0] * c1_houghPix->size[1];
      c1_houghPix->size[0] = 0;
      c1_houghPix->size[1] = 0;
      c1_i713 = c1_houghPix->size[0];
      c1_i715 = c1_houghPix->size[1];
    } else {
      c1_i710 = c1_r16->size[0] * c1_r16->size[1];
      c1_r16->size[0] = c1_numHoughPix;
      c1_r16->size[1] = 2;
      c1_emxEnsureCapacity_real_T(chartInstance, c1_r16, c1_i710,
        &c1_rg_emlrtRTEI);
      c1_i714 = c1_r16->size[0];
      c1_i716 = c1_r16->size[1];
      c1_e_loop_ub = (c1_numHoughPix << 1) - 1;
      for (c1_i718 = 0; c1_i718 <= c1_e_loop_ub; c1_i718++) {
        c1_r16->data[c1_i718] = 0.0;
      }

      c1_i720 = c1_b_houghPix->size[0] * c1_b_houghPix->size[1];
      c1_b_houghPix->size[0] = c1_r16->size[0];
      c1_b_houghPix->size[1] = c1_r16->size[1];
      c1_emxEnsureCapacity_int32_T1(chartInstance, c1_b_houghPix, c1_i720,
        &c1_pg_emlrtRTEI);
      c1_n = 0.0;
      c1_i725 = (int32_T)c1_b_numNonZero - 1;
      for (c1_k_k = 0; c1_k_k <= c1_i725; c1_k_k++) {
        c1_i_k = 1.0 + (real_T)c1_k_k;
        if (c1_rhoBinIdx->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_i_k, 1, c1_rhoBinIdx->size[0]) - 1] ==
            c1_b_peak1) {
          c1_n++;
          c1_b_houghPix->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_n, 1, c1_b_houghPix->size[0]) - 1] =
            c1_nonZeroPixels->data[(sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_i_k, 1, c1_nonZeroPixels->size[0]) +
            c1_nonZeroPixels->size[0]) - 1] + 1;
          c1_b_houghPix->data[(sf_eml_array_bounds_check
                               (sfGlobalDebugInstanceStruct, chartInstance->S,
                                1U, 0, 0, MAX_uint32_T, (int32_T)c1_n, 1,
                                c1_b_houghPix->size[0]) + c1_b_houghPix->size[0])
            - 1] = c1_nonZeroPixels->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_i_k, 1, c1_nonZeroPixels->size[0]) - 1] +
            1;
        }
      }

      c1_rowMax = 0.0;
      c1_rowMin = rtInf;
      c1_colMax = 0.0;
      c1_colMin = rtInf;
      c1_i_b = c1_numHoughPix;
      c1_j_b = c1_i_b;
      if (1 > c1_j_b) {
        c1_b_overflow = false;
      } else {
        c1_b_overflow = (c1_j_b > 2147483646);
      }

      if (c1_b_overflow) {
        c1_check_forloop_overflow_error(chartInstance, true);
      }

      for (c1_p_k = 1; c1_p_k - 1 < c1_numHoughPix; c1_p_k++) {
        c1_s_k = c1_p_k;
        c1_r = (real_T)c1_b_houghPix->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c1_s_k, 1, c1_b_houghPix->size[0]) - 1];
        if (c1_r > c1_rowMax) {
          c1_rowMax = c1_r;
        }

        if (c1_rowMin > c1_r) {
          c1_rowMin = c1_r;
        }

        c1_b_c = (real_T)c1_b_houghPix->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           c1_s_k, 1, c1_b_houghPix->size[0]) + c1_b_houghPix->size[0]) - 1];
        if (c1_b_c > c1_colMax) {
          c1_colMax = c1_b_c;
        }

        if (c1_colMin > c1_b_c) {
          c1_colMin = c1_b_c;
        }
      }

      c1_rowRange = c1_rowMax - c1_rowMin;
      c1_colRange = c1_colMax - c1_colMin;
      if (c1_rowRange > c1_colRange) {
        for (c1_i735 = 0; c1_i735 < 2; c1_i735++) {
          c1_sortingOrder[c1_i735] = 1.0 + (real_T)c1_i735;
        }
      } else {
        for (c1_i734 = 0; c1_i734 < 2; c1_i734++) {
          c1_sortingOrder[c1_i734] = 2.0 - (real_T)c1_i734;
        }
      }

      c1_b_sortrows(chartInstance, c1_b_houghPix, c1_sortingOrder);
      c1_i739 = c1_houghPix->size[0] * c1_houghPix->size[1];
      c1_houghPix->size[0] = c1_b_houghPix->size[0];
      c1_houghPix->size[1] = 2;
      c1_emxEnsureCapacity_int32_T1(chartInstance, c1_houghPix, c1_i739,
        &c1_ug_emlrtRTEI);
      c1_i741 = c1_houghPix->size[0];
      c1_i742 = c1_houghPix->size[1];
      c1_i_loop_ub = c1_b_houghPix->size[0] * c1_b_houghPix->size[1] - 1;
      for (c1_i744 = 0; c1_i744 <= c1_i_loop_ub; c1_i744++) {
        c1_houghPix->data[c1_i744] = c1_b_houghPix->data[c1_i744];
      }
    }

    if (c1_numHoughPix < 1) {
    } else {
      c1_b_numHoughPix = (real_T)c1_houghPix->size[0];
      c1_i719 = c1_distances2->size[0];
      c1_distances2->size[0] = (int32_T)_SFD_NON_NEGATIVE_CHECK("",
        c1_b_numHoughPix - 1.0);
      c1_emxEnsureCapacity_real_T1(chartInstance, c1_distances2, c1_i719,
        &c1_sg_emlrtRTEI);
      c1_numPairs = 0.0;
      c1_d36 = c1_b_numHoughPix - 1.0;
      c1_i723 = (int32_T)c1_d36 - 1;
      for (c1_j_k = 0; c1_j_k <= c1_i723; c1_j_k++) {
        c1_l_k = 1.0 + (real_T)c1_j_k;
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_houghPix->size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_houghPix->size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c1_houghPix->size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c1_houghPix->size[1]);
        c1_a = c1_houghPix->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)(c1_l_k + 1.0), 1, c1_houghPix->size[0]) - 1] -
          c1_houghPix->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_l_k, 1, c1_houghPix->size[0]) - 1];
        c1_b_a = c1_a;
        c1_c_a = c1_b_a;
        c1_n_x = c1_c_a;
        c1_d_a = c1_n_x;
        c1_c = 1;
        c1_bu = 2;
        do {
          exitg2 = 0;
          if ((c1_bu & 1) != 0) {
            c1_c *= c1_d_a;
          }

          c1_bu >>= 1;
          if (c1_bu == 0) {
            exitg2 = 1;
          } else {
            c1_d_a *= c1_d_a;
          }
        } while (exitg2 == 0);

        c1_e_a = c1_houghPix->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)(c1_l_k + 1.0), 1, c1_houghPix->size[0]) + c1_houghPix->
          size[0]) - 1] - c1_houghPix->data[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_l_k, 1, c1_houghPix->size[0]) + c1_houghPix->size[0]) - 1];
        c1_f_a = c1_e_a;
        c1_g_a = c1_f_a;
        c1_o_x = c1_g_a;
        c1_h_a = c1_o_x;
        c1_c_c = 1;
        c1_b_bu = 2;
        do {
          exitg2 = 0;
          if ((c1_b_bu & 1) != 0) {
            c1_c_c *= c1_h_a;
          }

          c1_b_bu >>= 1;
          if (c1_b_bu == 0) {
            exitg2 = 1;
          } else {
            c1_h_a *= c1_h_a;
          }
        } while (exitg2 == 0);

        c1_distances2->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_l_k, 1, c1_distances2->size[0]) - 1] = (real_T)(c1_c +
          c1_c_c);
        if (c1_distances2->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_l_k, 1, c1_distances2->size[0]) - 1] >
            25.0) {
          c1_numPairs++;
        }
      }

      c1_i726 = c1_indices->size[0];
      c1_indices->size[0] = (int32_T)sf_integer_check(chartInstance->S, 1U, 0U,
        0U, c1_numPairs + 2.0);
      c1_emxEnsureCapacity_real_T1(chartInstance, c1_indices, c1_i726,
        &c1_sg_emlrtRTEI);
      c1_indices->data[0] = 0.0;
      c1_indices->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_indices->size[0], 1,
        c1_indices->size[0]) - 1] = c1_b_numHoughPix;
      c1_b_n = 1.0;
      c1_d37 = c1_b_numHoughPix - 1.0;
      c1_i727 = (int32_T)c1_d37 - 1;
      for (c1_n_k = 0; c1_n_k <= c1_i727; c1_n_k++) {
        c1_l_k = 1.0 + (real_T)c1_n_k;
        if (c1_distances2->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, (int32_T)c1_l_k, 1, c1_distances2->size[0]) - 1] >
            25.0) {
          c1_b_n++;
          c1_indices->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_b_n, 1,
            c1_indices->size[0]) - 1] = c1_l_k;
        }
      }

      c1_d38 = (real_T)c1_indices->size[0] - 1.0;
      c1_i729 = (int32_T)c1_d38 - 1;
      for (c1_q_k = 0; c1_q_k <= c1_i729; c1_q_k++) {
        c1_t_k = 1.0 + (real_T)c1_q_k;
        c1_i732 = c1_houghPix->size[1];
        c1_b_indices = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)(c1_indices->
          data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_t_k, 1,
          c1_indices->size[0]) - 1] + 1.0), 1, c1_houghPix->size[0]) - 1;
        c1_point1_size[0] = 1;
        c1_point1_size[1] = c1_i732;
        c1_g_loop_ub = c1_i732 - 1;
        for (c1_i738 = 0; c1_i738 <= c1_g_loop_ub; c1_i738++) {
          c1_point1_data[c1_i738] = c1_houghPix->data[c1_b_indices +
            c1_houghPix->size[0] * c1_i738];
        }

        c1_i740 = c1_houghPix->size[1];
        c1_c_indices = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)c1_indices->
          data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, (int32_T)(c1_t_k + 1.0), 1,
          c1_indices->size[0]) - 1], 1, c1_houghPix->size[0]) - 1;
        c1_point2_size[0] = 1;
        c1_point2_size[1] = c1_i740;
        c1_h_loop_ub = c1_i740 - 1;
        for (c1_i743 = 0; c1_i743 <= c1_h_loop_ub; c1_i743++) {
          c1_point2_data[c1_i743] = c1_houghPix->data[c1_c_indices +
            c1_houghPix->size[0] * c1_i743];
        }

        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_point2_size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_point1_size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c1_point2_size[1]);
        (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
          chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c1_point1_size[1]);
        c1_i_a = c1_point2_data[0] - c1_point1_data[0];
        c1_j_a = c1_i_a;
        c1_k_a = c1_j_a;
        c1_p_x = c1_k_a;
        c1_l_a = c1_p_x;
        c1_d_c = 1;
        c1_c_bu = 2;
        do {
          exitg2 = 0;
          if ((c1_c_bu & 1) != 0) {
            c1_d_c *= c1_l_a;
          }

          c1_c_bu >>= 1;
          if (c1_c_bu == 0) {
            exitg2 = 1;
          } else {
            c1_l_a *= c1_l_a;
          }
        } while (exitg2 == 0);

        c1_m_a = c1_point2_data[1] - c1_point1_data[1];
        c1_n_a = c1_m_a;
        c1_o_a = c1_n_a;
        c1_q_x = c1_o_a;
        c1_p_a = c1_q_x;
        c1_e_c = 1;
        c1_d_bu = 2;
        do {
          exitg2 = 0;
          if ((c1_d_bu & 1) != 0) {
            c1_e_c *= c1_p_a;
          }

          c1_d_bu >>= 1;
          if (c1_d_bu == 0) {
            exitg2 = 1;
          } else {
            c1_p_a *= c1_p_a;
          }
        } while (exitg2 == 0);

        c1_lineLength2 = c1_d_c + c1_e_c;
        if (c1_lineLength2 >= 49) {
          c1_numLines++;
          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c1_point1_size[1]);
          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_point1_size[1]);
          c1_point1[0] = c1_point1_size[1];
          c1_b_point1[0] = c1_point1_size[1];
          c1_i745 = c1_b_point1Array->size[0] * c1_b_point1Array->size[1];
          c1_b_point1Array->size[0] = c1_point1Array->size[0] + 1;
          c1_b_point1Array->size[1] = 2;
          c1_emxEnsureCapacity_int32_T1(chartInstance, c1_b_point1Array, c1_i745,
            &c1_wg_emlrtRTEI);
          for (c1_i746 = 0; c1_i746 < 2; c1_i746++) {
            c1_j_loop_ub = c1_point1Array->size[0] - 1;
            for (c1_i747 = 0; c1_i747 <= c1_j_loop_ub; c1_i747++) {
              c1_b_point1Array->data[c1_i747 + c1_b_point1Array->size[0] *
                c1_i746] = c1_point1Array->data[c1_i747 + c1_point1Array->size[0]
                * c1_i746];
            }
          }

          c1_b_point1Array->data[c1_point1Array->size[0]] = c1_point1_data[1];
          c1_b_point1Array->data[c1_point1Array->size[0] +
            c1_b_point1Array->size[0]] = c1_point1_data[0];
          c1_i748 = c1_point1Array->size[0] * c1_point1Array->size[1];
          c1_point1Array->size[0] = c1_b_point1Array->size[0];
          c1_point1Array->size[1] = 2;
          c1_emxEnsureCapacity_int32_T1(chartInstance, c1_point1Array, c1_i748,
            &c1_xg_emlrtRTEI);
          c1_i749 = c1_point1Array->size[0];
          c1_i750 = c1_point1Array->size[1];
          c1_k_loop_ub = c1_b_point1Array->size[0] * c1_b_point1Array->size[1] -
            1;
          for (c1_i751 = 0; c1_i751 <= c1_k_loop_ub; c1_i751++) {
            c1_point1Array->data[c1_i751] = c1_b_point1Array->data[c1_i751];
          }

          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 2, 1, c1_point2_size[1]);
          (real_T)sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, 1, 1, c1_point2_size[1]);
          c1_point2[0] = c1_point2_size[1];
          c1_b_point2[0] = c1_point2_size[1];
          c1_i752 = c1_b_point2Array->size[0] * c1_b_point2Array->size[1];
          c1_b_point2Array->size[0] = c1_point2Array->size[0] + 1;
          c1_b_point2Array->size[1] = 2;
          c1_emxEnsureCapacity_int32_T1(chartInstance, c1_b_point2Array, c1_i752,
            &c1_yg_emlrtRTEI);
          for (c1_i753 = 0; c1_i753 < 2; c1_i753++) {
            c1_l_loop_ub = c1_point2Array->size[0] - 1;
            for (c1_i754 = 0; c1_i754 <= c1_l_loop_ub; c1_i754++) {
              c1_b_point2Array->data[c1_i754 + c1_b_point2Array->size[0] *
                c1_i753] = c1_point2Array->data[c1_i754 + c1_point2Array->size[0]
                * c1_i753];
            }
          }

          c1_b_point2Array->data[c1_point2Array->size[0]] = c1_point2_data[1];
          c1_b_point2Array->data[c1_point2Array->size[0] +
            c1_b_point2Array->size[0]] = c1_point2_data[0];
          c1_i755 = c1_point2Array->size[0] * c1_point2Array->size[1];
          c1_point2Array->size[0] = c1_b_point2Array->size[0];
          c1_point2Array->size[1] = 2;
          c1_emxEnsureCapacity_int32_T1(chartInstance, c1_point2Array, c1_i755,
            &c1_ah_emlrtRTEI);
          c1_i756 = c1_point2Array->size[0];
          c1_i757 = c1_point2Array->size[1];
          c1_m_loop_ub = c1_b_point2Array->size[0] * c1_b_point2Array->size[1] -
            1;
          for (c1_i758 = 0; c1_i758 <= c1_m_loop_ub; c1_i758++) {
            c1_point2Array->data[c1_i758] = c1_b_point2Array->data[c1_i758];
          }

          c1_i759 = c1_b_thetaArray->size[0];
          c1_b_thetaArray->size[0] = c1_thetaArray->size[0] + 1;
          c1_emxEnsureCapacity_real32_T1(chartInstance, c1_b_thetaArray, c1_i759,
            &c1_bh_emlrtRTEI);
          c1_n_loop_ub = c1_thetaArray->size[0] - 1;
          for (c1_i760 = 0; c1_i760 <= c1_n_loop_ub; c1_i760++) {
            c1_b_thetaArray->data[c1_i760] = c1_thetaArray->data[c1_i760];
          }

          c1_b_thetaArray->data[c1_thetaArray->size[0]] = (real32_T)(-45.0 + 0.5
            * (real_T)(sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
            chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_peak2, 1, 181) - 1));
          c1_i761 = c1_thetaArray->size[0];
          c1_thetaArray->size[0] = c1_b_thetaArray->size[0];
          c1_emxEnsureCapacity_real32_T1(chartInstance, c1_thetaArray, c1_i761,
            &c1_ch_emlrtRTEI);
          c1_o_loop_ub = c1_b_thetaArray->size[0] - 1;
          for (c1_i762 = 0; c1_i762 <= c1_o_loop_ub; c1_i762++) {
            c1_thetaArray->data[c1_i762] = c1_b_thetaArray->data[c1_i762];
          }

          c1_b_varargin_3[0] = c1_varargin_3->size[1];
          c1_i763 = c1_b_rhoArray->size[0];
          c1_b_rhoArray->size[0] = c1_rhoArray->size[0] + 1;
          c1_emxEnsureCapacity_real32_T1(chartInstance, c1_b_rhoArray, c1_i763,
            &c1_dh_emlrtRTEI);
          c1_p_loop_ub = c1_rhoArray->size[0] - 1;
          for (c1_i764 = 0; c1_i764 <= c1_p_loop_ub; c1_i764++) {
            c1_b_rhoArray->data[c1_i764] = c1_rhoArray->data[c1_i764];
          }

          c1_b_rhoArray->data[c1_rhoArray->size[0]] = (real32_T)
            c1_varargin_3->data[sf_eml_array_bounds_check
            (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0,
             MAX_uint32_T, c1_peak1, 1, c1_varargin_3->size[1]) - 1];
          c1_i765 = c1_rhoArray->size[0];
          c1_rhoArray->size[0] = c1_b_rhoArray->size[0];
          c1_emxEnsureCapacity_real32_T1(chartInstance, c1_rhoArray, c1_i765,
            &c1_eh_emlrtRTEI);
          c1_q_loop_ub = c1_b_rhoArray->size[0] - 1;
          for (c1_i766 = 0; c1_i766 <= c1_q_loop_ub; c1_i766++) {
            c1_rhoArray->data[c1_i766] = c1_b_rhoArray->data[c1_i766];
          }
        }
      }
    }

    c1_peakIdx++;
  }

  c1_emxFree_real32_T(chartInstance, &c1_b_rhoArray);
  c1_emxFree_real32_T(chartInstance, &c1_b_thetaArray);
  c1_emxFree_int32_T(chartInstance, &c1_b_point2Array);
  c1_emxFree_int32_T(chartInstance, &c1_b_point1Array);
  c1_emxFree_real_T(chartInstance, &c1_distances2);
  c1_emxFree_int32_T(chartInstance, &c1_b_houghPix);
  c1_emxFree_int32_T(chartInstance, &c1_rhoBinIdx);
  c1_emxFree_real_T(chartInstance, &c1_r16);
  c1_emxFree_int32_T(chartInstance, &c1_houghPix);
  c1_emxFree_real_T(chartInstance, &c1_peaks);
  c1_emxFree_real_T(chartInstance, &c1_indices);
  c1_emxFree_int32_T(chartInstance, &c1_nonZeroPixels);
  c1_emxInit_boolean_T(chartInstance, &c1_tile, 2, &c1_ee_emlrtRTEI);
  c1_b_numLines = c1_numLines;
  c1_c_numLines = c1_b_numLines;
  c1_varargin_2 = c1_c_numLines;
  c1_i704 = c1_tile->size[0] * c1_tile->size[1];
  c1_tile->size[0] = 1;
  c1_tile->size[1] = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_varargin_2);
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_tile, c1_i704,
    &c1_ee_emlrtRTEI);
  c1_i705 = c1_tile->size[0];
  c1_i706 = c1_tile->size[1];
  c1_d_loop_ub = (int32_T)_SFD_NON_NEGATIVE_CHECK("", (real_T)c1_varargin_2) - 1;
  for (c1_i707 = 0; c1_i707 <= c1_d_loop_ub; c1_i707++) {
    c1_tile->data[c1_i707] = false;
  }

  c1_outsize[1] = c1_tile->size[1];
  if ((real_T)c1_outsize[1] == (real_T)c1_tile->size[1]) {
  } else {
    c1_r_y = NULL;
    sf_mex_assign(&c1_r_y, sf_mex_create("y", c1_cv6, 10, 0U, 1U, 0U, 2, 1, 15),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14, c1_r_y);
  }

  c1_emxFree_boolean_T(chartInstance, &c1_tile);
  for (c1_i709 = 0; c1_i709 < 2; c1_i709++) {
    c1_s.point1[c1_i709] = 0.0;
  }

  for (c1_i712 = 0; c1_i712 < 2; c1_i712++) {
    c1_s.point2[c1_i712] = 0.0;
  }

  c1_s.theta = 0.0;
  c1_s.rho = 0.0;
  c1_i717 = c1_lines->size[0] * c1_lines->size[1];
  c1_lines->size[0] = 1;
  c1_lines->size[1] = c1_outsize[1];
  c1_emxEnsureCapacity_skoeQIuVNKJRH(chartInstance, c1_lines, c1_i717,
    &c1_qg_emlrtRTEI);
  c1_i721 = c1_lines->size[0];
  c1_i722 = c1_lines->size[1];
  c1_f_loop_ub = c1_outsize[1] - 1;
  for (c1_i724 = 0; c1_i724 <= c1_f_loop_ub; c1_i724++) {
    c1_lines->data[c1_i724] = c1_s;
  }

  c1_g_b = c1_b_numLines;
  c1_h_b = c1_g_b;
  if (1 > c1_h_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_h_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_m_k = 1; c1_m_k - 1 < c1_b_numLines; c1_m_k++) {
    c1_o_k = c1_m_k;
    c1_i728 = c1_lines->size[1];
    c1_iv8[0] = c1_i728;
    c1_i730 = c1_lines->size[1];
    c1_r_k = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_o_k, 1, c1_point1Array->size
      [0]) - 1;
    for (c1_i731 = 0; c1_i731 < 2; c1_i731++) {
      c1_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_o_k, 1, c1_i730) - 1].
        point1[c1_i731] = (real_T)c1_point1Array->data[c1_r_k +
        c1_point1Array->size[0] * c1_i731];
    }

    c1_i733 = c1_lines->size[1];
    c1_iv9[0] = c1_i733;
    c1_i736 = c1_lines->size[1];
    c1_u_k = sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_o_k, 1, c1_point2Array->size
      [0]) - 1;
    for (c1_i737 = 0; c1_i737 < 2; c1_i737++) {
      c1_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
        chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_o_k, 1, c1_i736) - 1].
        point2[c1_i737] = (real_T)c1_point2Array->data[c1_u_k +
        c1_point2Array->size[0] * c1_i737];
    }

    c1_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_o_k, 1, c1_lines->size[1]) -
      1].theta = c1_thetaArray->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       c1_o_k, 1, c1_thetaArray->size[0]) - 1];
    c1_lines->data[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 0, 0, MAX_uint32_T, c1_o_k, 1, c1_lines->size[1]) -
      1].rho = c1_rhoArray->data[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
       c1_o_k, 1, c1_rhoArray->size[0]) - 1];
  }

  c1_emxFree_real32_T(chartInstance, &c1_rhoArray);
  c1_emxFree_real32_T(chartInstance, &c1_thetaArray);
  c1_emxFree_int32_T(chartInstance, &c1_point2Array);
  c1_emxFree_int32_T(chartInstance, &c1_point1Array);
}

static void c1_sortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_y, real_T c1_varargin_1[2], c1_emxArray_int32_T
  *c1_b_y)
{
  int32_T c1_i767;
  int32_T c1_i768;
  int32_T c1_i769;
  int32_T c1_loop_ub;
  int32_T c1_i770;
  int32_T c1_i771;
  real_T c1_b_varargin_1[2];
  c1_i767 = c1_b_y->size[0] * c1_b_y->size[1];
  c1_b_y->size[0] = c1_y->size[0];
  c1_b_y->size[1] = 2;
  c1_emxEnsureCapacity_int32_T1(chartInstance, c1_b_y, c1_i767, &c1_ph_emlrtRTEI);
  c1_i768 = c1_b_y->size[0];
  c1_i769 = c1_b_y->size[1];
  c1_loop_ub = c1_y->size[0] * c1_y->size[1] - 1;
  for (c1_i770 = 0; c1_i770 <= c1_loop_ub; c1_i770++) {
    c1_b_y->data[c1_i770] = c1_y->data[c1_i770];
  }

  for (c1_i771 = 0; c1_i771 < 2; c1_i771++) {
    c1_b_varargin_1[c1_i771] = c1_varargin_1[c1_i771];
  }

  c1_b_sortrows(chartInstance, c1_b_y, c1_b_varargin_1);
}

static boolean_T c1_b_sortLE(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_v, int32_T c1_dir[2], int32_T c1_idx1, int32_T c1_idx2)
{
  boolean_T c1_p;
  int32_T c1_irow1;
  int32_T c1_irow2;
  int32_T c1_k;
  int32_T c1_b_k;
  int32_T c1_colk;
  int32_T c1_x;
  int32_T c1_b_x;
  int32_T c1_c_x;
  int32_T c1_abscolk;
  int32_T c1_v1;
  int32_T c1_v2;
  boolean_T c1_v1eqv2;
  boolean_T c1_b77;
  int32_T c1_a;
  int32_T c1_b;
  int32_T c1_b_a;
  int32_T c1_b_b;
  boolean_T exitg1;
  (void)chartInstance;
  c1_irow1 = c1_idx1 - 1;
  c1_irow2 = c1_idx2 - 1;
  c1_p = true;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k < 2)) {
    c1_b_k = c1_k;
    c1_colk = c1_dir[c1_b_k];
    c1_x = c1_colk;
    c1_b_x = c1_x;
    c1_c_x = c1_b_x;
    c1_abscolk = c1_c_x - 1;
    c1_v1 = c1_v->data[c1_irow1 + c1_v->size[0] * c1_abscolk];
    c1_v2 = c1_v->data[c1_irow2 + c1_v->size[0] * c1_abscolk];
    c1_v1eqv2 = (c1_v1 == c1_v2);
    if (c1_v1eqv2) {
      c1_b77 = true;
    } else {
      c1_b77 = false;
    }

    if (!c1_b77) {
      c1_a = c1_v1;
      c1_b = c1_v2;
      c1_b_a = c1_a;
      c1_b_b = c1_b;
      c1_p = (c1_b_a <= c1_b_b);
      exitg1 = true;
    } else {
      c1_k++;
    }
  }

  return c1_p;
}

static void c1_sort(SFc1_LIDAR_simInstanceStruct *chartInstance,
                    c1_emxArray_real_T *c1_x, c1_emxArray_real_T *c1_b_x)
{
  int32_T c1_i772;
  int32_T c1_loop_ub;
  int32_T c1_i773;
  c1_i772 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i772, &c1_qh_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i773 = 0; c1_i773 <= c1_loop_ub; c1_i773++) {
    c1_b_x->data[c1_i773] = c1_x->data[c1_i773];
  }

  c1_b_sort(chartInstance, c1_b_x);
}

static void c1_sortIdx(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T
  *c1_b_x)
{
  int32_T c1_i774;
  int32_T c1_loop_ub;
  int32_T c1_i775;
  c1_i774 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i774, &c1_rh_emlrtRTEI);
  c1_loop_ub = c1_x->size[0] - 1;
  for (c1_i775 = 0; c1_i775 <= c1_loop_ub; c1_i775++) {
    c1_b_x->data[c1_i775] = c1_x->data[c1_i775];
  }

  c1_b_sortIdx(chartInstance, c1_b_x, c1_idx);
}

static void c1_merge_block(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x, int32_T c1_offset,
  int32_T c1_n, int32_T c1_preSortLevel, c1_emxArray_int32_T *c1_iwork,
  c1_emxArray_real_T *c1_xwork, c1_emxArray_int32_T *c1_b_idx,
  c1_emxArray_real_T *c1_b_x, c1_emxArray_int32_T *c1_b_iwork,
  c1_emxArray_real_T *c1_b_xwork)
{
  int32_T c1_i776;
  int32_T c1_loop_ub;
  int32_T c1_i777;
  int32_T c1_i778;
  int32_T c1_b_loop_ub;
  int32_T c1_i779;
  int32_T c1_i780;
  int32_T c1_c_loop_ub;
  int32_T c1_i781;
  int32_T c1_i782;
  int32_T c1_d_loop_ub;
  int32_T c1_i783;
  c1_i776 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i776,
    &c1_sh_emlrtRTEI);
  c1_loop_ub = c1_idx->size[0] - 1;
  for (c1_i777 = 0; c1_i777 <= c1_loop_ub; c1_i777++) {
    c1_b_idx->data[c1_i777] = c1_idx->data[c1_i777];
  }

  c1_i778 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i778, &c1_sh_emlrtRTEI);
  c1_b_loop_ub = c1_x->size[0] - 1;
  for (c1_i779 = 0; c1_i779 <= c1_b_loop_ub; c1_i779++) {
    c1_b_x->data[c1_i779] = c1_x->data[c1_i779];
  }

  c1_i780 = c1_b_iwork->size[0];
  c1_b_iwork->size[0] = c1_iwork->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_iwork, c1_i780,
    &c1_sh_emlrtRTEI);
  c1_c_loop_ub = c1_iwork->size[0] - 1;
  for (c1_i781 = 0; c1_i781 <= c1_c_loop_ub; c1_i781++) {
    c1_b_iwork->data[c1_i781] = c1_iwork->data[c1_i781];
  }

  c1_i782 = c1_b_xwork->size[0];
  c1_b_xwork->size[0] = c1_xwork->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_xwork, c1_i782,
    &c1_sh_emlrtRTEI);
  c1_d_loop_ub = c1_xwork->size[0] - 1;
  for (c1_i783 = 0; c1_i783 <= c1_d_loop_ub; c1_i783++) {
    c1_b_xwork->data[c1_i783] = c1_xwork->data[c1_i783];
  }

  c1_b_merge_block(chartInstance, c1_b_idx, c1_b_x, c1_offset, c1_n,
                   c1_preSortLevel, c1_b_iwork, c1_b_xwork);
}

static void c1_merge(SFc1_LIDAR_simInstanceStruct *chartInstance,
                     c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x,
                     int32_T c1_offset, int32_T c1_np, int32_T c1_nq,
                     c1_emxArray_int32_T *c1_iwork, c1_emxArray_real_T *c1_xwork,
                     c1_emxArray_int32_T *c1_b_idx, c1_emxArray_real_T *c1_b_x,
                     c1_emxArray_int32_T *c1_b_iwork, c1_emxArray_real_T
                     *c1_b_xwork)
{
  int32_T c1_i784;
  int32_T c1_loop_ub;
  int32_T c1_i785;
  int32_T c1_i786;
  int32_T c1_b_loop_ub;
  int32_T c1_i787;
  int32_T c1_i788;
  int32_T c1_c_loop_ub;
  int32_T c1_i789;
  int32_T c1_i790;
  int32_T c1_d_loop_ub;
  int32_T c1_i791;
  c1_i784 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i784,
    &c1_th_emlrtRTEI);
  c1_loop_ub = c1_idx->size[0] - 1;
  for (c1_i785 = 0; c1_i785 <= c1_loop_ub; c1_i785++) {
    c1_b_idx->data[c1_i785] = c1_idx->data[c1_i785];
  }

  c1_i786 = c1_b_x->size[0];
  c1_b_x->size[0] = c1_x->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_x, c1_i786, &c1_th_emlrtRTEI);
  c1_b_loop_ub = c1_x->size[0] - 1;
  for (c1_i787 = 0; c1_i787 <= c1_b_loop_ub; c1_i787++) {
    c1_b_x->data[c1_i787] = c1_x->data[c1_i787];
  }

  c1_i788 = c1_b_iwork->size[0];
  c1_b_iwork->size[0] = c1_iwork->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_iwork, c1_i788,
    &c1_th_emlrtRTEI);
  c1_c_loop_ub = c1_iwork->size[0] - 1;
  for (c1_i789 = 0; c1_i789 <= c1_c_loop_ub; c1_i789++) {
    c1_b_iwork->data[c1_i789] = c1_iwork->data[c1_i789];
  }

  c1_i790 = c1_b_xwork->size[0];
  c1_b_xwork->size[0] = c1_xwork->size[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_xwork, c1_i790,
    &c1_th_emlrtRTEI);
  c1_d_loop_ub = c1_xwork->size[0] - 1;
  for (c1_i791 = 0; c1_i791 <= c1_d_loop_ub; c1_i791++) {
    c1_b_xwork->data[c1_i791] = c1_xwork->data[c1_i791];
  }

  c1_b_merge(chartInstance, c1_b_idx, c1_b_x, c1_offset, c1_np, c1_nq,
             c1_b_iwork, c1_b_xwork);
}

static real_T c1_cosd(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T c1_x)
{
  real_T c1_b_x;
  c1_b_x = c1_x;
  c1_b_cosd(chartInstance, &c1_b_x);
  return c1_b_x;
}

static real_T c1_sind(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T c1_x)
{
  real_T c1_b_x;
  c1_b_x = c1_x;
  c1_b_sind(chartInstance, &c1_b_x);
  return c1_b_x;
}

static const mxArray *c1_emlrt_marshallOut(SFc1_LIDAR_simInstanceStruct
  *chartInstance, const char_T c1_b_u[30])
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u, 10, 0U, 1U, 0U, 2, 1, 30),
                false);
  return c1_y;
}

static const mxArray *c1_b_emlrt_marshallOut(SFc1_LIDAR_simInstanceStruct
  *chartInstance, const char_T c1_b_u[14])
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u, 10, 0U, 1U, 0U, 2, 1, 14),
                false);
  return c1_y;
}

static const mxArray *c1_m_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int32_T c1_b_u;
  const mxArray *c1_y = NULL;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_b_u = *(int32_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int32_T c1_m_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i792;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_i792, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i792;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_k_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static uint8_T c1_n_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_is_active_c1_LIDAR_sim, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_o_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_is_active_c1_LIDAR_sim), &c1_thisId);
  sf_mex_destroy(&c1_b_is_active_c1_LIDAR_sim);
  return c1_y;
}

static uint8_T c1_o_emlrt_marshallIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u6;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_u6, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u6;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_c_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, boolean_T c1_idx_data[], int32_T c1_idx_size[1])
{
  real_T c1_d39;
  int32_T c1_n;
  int32_T c1_k;
  boolean_T c1_p;
  const mxArray *c1_y = NULL;
  int32_T c1_nrowx;
  const mxArray *c1_b_y = NULL;
  int32_T c1_b_n;
  int32_T c1_i793;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_b_k;
  int32_T c1_nrows;
  int32_T c1_c_k;
  int32_T c1_nb;
  int32_T c1_i;
  int32_T c1_c_b;
  int32_T c1_d_b;
  boolean_T c1_b_overflow;
  int32_T c1_d_k;
  int32_T c1_e_k;
  const mxArray *c1_c_y = NULL;
  boolean_T c1_b78;
  const mxArray *c1_d_y = NULL;
  int32_T c1_j;
  int32_T c1_i794;
  c1_emxArray_real_T *c1_b_x;
  int32_T c1_b_j;
  int32_T c1_i795;
  int32_T c1_i796;
  int32_T c1_i797;
  int32_T c1_loop_ub;
  int32_T c1_i798;
  int32_T c1_i799;
  int32_T c1_b_loop_ub;
  int32_T c1_i800;
  c1_d39 = (real_T)c1_x->size[0];
  c1_n = (int32_T)c1_d39;
  c1_k = c1_idx_size[0];
  while ((c1_k >= 1) && (!c1_idx_data[c1_k - 1])) {
    c1_k--;
  }

  c1_p = (c1_k <= c1_n);
  if (c1_p) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv13, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv13, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c1_b_y)));
  }

  c1_nrowx = c1_x->size[0];
  c1_b_n = 0;
  c1_i793 = c1_idx_size[0];
  c1_b = c1_i793;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_b_k = 0; c1_b_k < c1_i793; c1_b_k++) {
    c1_c_k = c1_b_k;
    c1_b_n += (int32_T)c1_idx_data[c1_c_k];
  }

  c1_nrows = c1_nrowx - c1_b_n;
  c1_nb = c1_idx_size[0];
  c1_i = 0;
  c1_c_b = c1_nrowx;
  c1_d_b = c1_c_b;
  if (1 > c1_d_b) {
    c1_b_overflow = false;
  } else {
    c1_b_overflow = (c1_d_b > 2147483646);
  }

  if (c1_b_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_d_k = 0; c1_d_k < c1_nrowx; c1_d_k++) {
    c1_e_k = c1_d_k;
    if ((c1_e_k + 1 > c1_nb) || (!c1_idx_data[c1_e_k])) {
      for (c1_j = 0; c1_j < 2; c1_j++) {
        c1_b_j = c1_j;
        c1_x->data[c1_i + c1_x->size[0] * c1_b_j] = c1_x->data[c1_e_k +
          c1_x->size[0] * c1_b_j];
      }

      c1_i++;
    }
  }

  if (c1_nrows <= c1_nrowx) {
  } else {
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_d_y)));
  }

  c1_b78 = (1 > c1_nrows);
  if (c1_b78) {
    c1_i794 = 0;
  } else {
    c1_i794 = c1_nrows;
  }

  c1_emxInit_real_T(chartInstance, &c1_b_x, 2, &c1_uh_emlrtRTEI);
  c1_i795 = c1_b_x->size[0] * c1_b_x->size[1];
  c1_b_x->size[0] = c1_i794;
  c1_b_x->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_b_x, c1_i795, &c1_uh_emlrtRTEI);
  for (c1_i796 = 0; c1_i796 < 2; c1_i796++) {
    c1_loop_ub = c1_i794 - 1;
    for (c1_i798 = 0; c1_i798 <= c1_loop_ub; c1_i798++) {
      c1_b_x->data[c1_i798 + c1_b_x->size[0] * c1_i796] = c1_x->data[c1_i798 +
        c1_x->size[0] * c1_i796];
    }
  }

  c1_i797 = c1_x->size[0] * c1_x->size[1];
  c1_x->size[0] = c1_b_x->size[0];
  c1_x->size[1] = 2;
  c1_emxEnsureCapacity_real_T(chartInstance, c1_x, c1_i797, &c1_vh_emlrtRTEI);
  for (c1_i799 = 0; c1_i799 < 2; c1_i799++) {
    c1_b_loop_ub = c1_b_x->size[0] - 1;
    for (c1_i800 = 0; c1_i800 <= c1_b_loop_ub; c1_i800++) {
      c1_x->data[c1_i800 + c1_x->size[0] * c1_i799] = c1_b_x->data[c1_i800 +
        c1_b_x->size[0] * c1_i799];
    }
  }

  c1_emxFree_real_T(chartInstance, &c1_b_x);
}

static void c1_c_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x)
{
  int32_T c1_nx;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  real_T c1_b_x;
  real_T c1_c_x;
  int32_T c1_d_x[1];
  c1_nx = c1_x->size[0] << 1;
  c1_b = c1_nx;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_nx; c1_k++) {
    c1_b_k = c1_k;
    c1_b_x = c1_x->data[c1_b_k];
    c1_c_x = c1_b_x;
    c1_c_x = muDoubleScalarRound(c1_c_x);
    c1_d_x[0] = c1_x->size[0] << 1;
    c1_x->data[c1_b_k] = c1_c_x;
  }
}

static void c1_b_apply_row_permutation(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real_T *c1_y, c1_emxArray_int32_T *c1_idx)
{
  c1_emxArray_real_T *c1_ycol;
  int32_T c1_m;
  int32_T c1_iv10[2];
  int32_T c1_i801;
  int32_T c1_j;
  int32_T c1_b_j;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_i;
  int32_T c1_c_b;
  int32_T c1_b_i;
  int32_T c1_d_b;
  boolean_T c1_b_overflow;
  int32_T c1_c_i;
  c1_emxInit_real_T1(chartInstance, &c1_ycol, 1, &c1_wh_emlrtRTEI);
  c1_m = c1_y->size[0];
  c1_iv10[0] = c1_m;
  c1_iv10[1] = 1;
  c1_i801 = c1_ycol->size[0];
  c1_ycol->size[0] = c1_iv10[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_ycol, c1_i801, &c1_bc_emlrtRTEI);
  for (c1_j = 0; c1_j < 2; c1_j++) {
    c1_b_j = c1_j;
    c1_b = c1_m;
    c1_b_b = c1_b;
    if (1 > c1_b_b) {
      c1_overflow = false;
    } else {
      c1_overflow = (c1_b_b > 2147483646);
    }

    if (c1_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_i = 0; c1_i < c1_m; c1_i++) {
      c1_b_i = c1_i;
      c1_ycol->data[c1_b_i] = c1_y->data[(c1_idx->data[c1_b_i] + c1_y->size[0] *
        c1_b_j) - 1];
    }

    c1_c_b = c1_m;
    c1_d_b = c1_c_b;
    if (1 > c1_d_b) {
      c1_b_overflow = false;
    } else {
      c1_b_overflow = (c1_d_b > 2147483646);
    }

    if (c1_b_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_c_i = 0; c1_c_i < c1_m; c1_c_i++) {
      c1_b_i = c1_c_i;
      c1_y->data[c1_b_i + c1_y->size[0] * c1_b_j] = c1_ycol->data[c1_b_i];
    }
  }

  c1_emxFree_real_T(chartInstance, &c1_ycol);
}

static void c1_d_round(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x)
{
  int32_T c1_nx;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  real_T c1_b_x;
  real_T c1_c_x;
  c1_nx = c1_x->size[0];
  c1_b = c1_nx;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_nx; c1_k++) {
    c1_b_k = c1_k;
    c1_b_x = c1_x->data[c1_b_k];
    c1_c_x = c1_b_x;
    c1_c_x = muDoubleScalarRound(c1_c_x);
    c1_x->data[c1_b_k] = c1_c_x;
  }
}

static void c1_b_locSortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_a, c1_emxArray_int32_T
  *c1_b)
{
  c1_coder_internal_anonymous_function c1_this;
  c1_cell_wrap_1 c1_tunableEnvironment[2];
  int32_T c1_i802;
  int32_T c1_loop_ub;
  int32_T c1_i803;
  int32_T c1_i804;
  int32_T c1_b_loop_ub;
  int32_T c1_i805;
  int32_T c1_i806;
  c1_emxArray_int32_T *c1_b_idx;
  int32_T c1_i807;
  int32_T c1_c_loop_ub;
  int32_T c1_i808;
  c1_emxArray_int32_T *c1_c_idx;
  int32_T c1_i809;
  int32_T c1_d_loop_ub;
  int32_T c1_i810;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_this, &c1_ci_emlrtRTEI);
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_tunableEnvironment,
    &c1_di_emlrtRTEI);
  c1_i802 = c1_tunableEnvironment[0].f1->size[0];
  c1_tunableEnvironment[0].f1->size[0] = c1_a->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_tunableEnvironment[0].f1,
    c1_i802, &c1_xh_emlrtRTEI);
  c1_loop_ub = c1_a->size[0] - 1;
  for (c1_i803 = 0; c1_i803 <= c1_loop_ub; c1_i803++) {
    c1_tunableEnvironment[0].f1->data[c1_i803] = c1_a->data[c1_i803];
  }

  c1_i804 = c1_tunableEnvironment[1].f1->size[0];
  c1_tunableEnvironment[1].f1->size[0] = c1_b->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_tunableEnvironment[1].f1,
    c1_i804, &c1_xh_emlrtRTEI);
  c1_b_loop_ub = c1_b->size[0] - 1;
  for (c1_i805 = 0; c1_i805 <= c1_b_loop_ub; c1_i805++) {
    c1_tunableEnvironment[1].f1->data[c1_i805] = c1_b->data[c1_i805];
  }

  for (c1_i806 = 0; c1_i806 < 2; c1_i806++) {
    c1_emxCopyStruct_cell_wrap_1(chartInstance,
      &c1_this.tunableEnvironment[c1_i806], &c1_tunableEnvironment[c1_i806],
      &c1_yh_emlrtRTEI);
  }

  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_tunableEnvironment);
  c1_emxInit_int32_T(chartInstance, &c1_b_idx, 1, &c1_ai_emlrtRTEI);
  c1_b_introsort(chartInstance, c1_idx, c1_a->size[0], c1_this);
  c1_i807 = c1_b_idx->size[0];
  c1_b_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_idx, c1_i807,
    &c1_ai_emlrtRTEI);
  c1_c_loop_ub = c1_idx->size[0] - 1;
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_this);
  for (c1_i808 = 0; c1_i808 <= c1_c_loop_ub; c1_i808++) {
    c1_b_idx->data[c1_i808] = c1_idx->data[c1_i808];
  }

  c1_emxInit_int32_T(chartInstance, &c1_c_idx, 1, &c1_bi_emlrtRTEI);
  c1_b_permuteVector(chartInstance, c1_b_idx, c1_a);
  c1_i809 = c1_c_idx->size[0];
  c1_c_idx->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_idx, c1_i809,
    &c1_bi_emlrtRTEI);
  c1_d_loop_ub = c1_idx->size[0] - 1;
  c1_emxFree_int32_T(chartInstance, &c1_b_idx);
  for (c1_i810 = 0; c1_i810 <= c1_d_loop_ub; c1_i810++) {
    c1_c_idx->data[c1_i810] = c1_idx->data[c1_i810];
  }

  c1_b_permuteVector(chartInstance, c1_c_idx, c1_b);
  c1_emxFree_int32_T(chartInstance, &c1_c_idx);
}

static void c1_b_insertionsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp)
{
  int32_T c1_i811;
  int32_T c1_a;
  int32_T c1_b;
  int32_T c1_b_a;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  c1_cell_wrap_1 c1_environment[2];
  c1_emxArray_int32_T *c1_r17;
  c1_emxArray_int32_T *c1_r18;
  c1_emxArray_int32_T *c1_b_environment;
  c1_emxArray_int32_T *c1_c_environment;
  int32_T c1_xc;
  int32_T c1_idx;
  int32_T c1_varargin_1;
  int32_T c1_varargin_2;
  int32_T c1_i812;
  int32_T c1_loop_ub;
  int32_T c1_i813;
  int32_T c1_i814;
  int32_T c1_b_loop_ub;
  int32_T c1_i815;
  int32_T c1_i816;
  int32_T c1_c_loop_ub;
  int32_T c1_i817;
  int32_T c1_i818;
  int32_T c1_d_loop_ub;
  int32_T c1_i819;
  int32_T c1_i820;
  int32_T c1_e_loop_ub;
  int32_T c1_i821;
  int32_T c1_i822;
  int32_T c1_f_loop_ub;
  int32_T c1_i823;
  boolean_T c1_varargout_1;
  boolean_T exitg1;
  c1_i811 = c1_xstart + 1;
  c1_a = c1_i811;
  c1_b = c1_xend;
  c1_b_a = c1_a;
  c1_b_b = c1_b;
  if (c1_b_a > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  c1_k = c1_i811 - 1;
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_environment, &c1_gi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_r17, 1, &c1_lc_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_r18, 1, &c1_lc_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_b_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_c_environment, 1, &c1_fi_emlrtRTEI);
  while (c1_k + 1 <= c1_xend) {
    c1_xc = c1_x->data[c1_k];
    c1_idx = c1_k;
    exitg1 = false;
    while ((!exitg1) && (c1_idx >= c1_xstart)) {
      c1_varargin_1 = c1_xc;
      c1_varargin_2 = c1_x->data[c1_idx - 1];
      c1_i812 = c1_r17->size[0];
      c1_r17->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_r17, c1_i812,
        &c1_ei_emlrtRTEI);
      c1_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
      for (c1_i813 = 0; c1_i813 <= c1_loop_ub; c1_i813++) {
        c1_r17->data[c1_i813] = c1_cmp.tunableEnvironment[0].f1->data[c1_i813];
      }

      c1_i814 = c1_r18->size[0];
      c1_r18->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_r18, c1_i814,
        &c1_ei_emlrtRTEI);
      c1_b_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
      for (c1_i815 = 0; c1_i815 <= c1_b_loop_ub; c1_i815++) {
        c1_r18->data[c1_i815] = c1_cmp.tunableEnvironment[1].f1->data[c1_i815];
      }

      c1_i816 = c1_environment[0].f1->size[0];
      c1_environment[0].f1->size[0] = c1_r17->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_environment[0].f1, c1_i816,
        &c1_ei_emlrtRTEI);
      c1_c_loop_ub = c1_r17->size[0] - 1;
      for (c1_i817 = 0; c1_i817 <= c1_c_loop_ub; c1_i817++) {
        c1_environment[0].f1->data[c1_i817] = c1_r17->data[c1_i817];
      }

      c1_i818 = c1_environment[1].f1->size[0];
      c1_environment[1].f1->size[0] = c1_r18->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_environment[1].f1, c1_i818,
        &c1_ei_emlrtRTEI);
      c1_d_loop_ub = c1_r18->size[0] - 1;
      for (c1_i819 = 0; c1_i819 <= c1_d_loop_ub; c1_i819++) {
        c1_environment[1].f1->data[c1_i819] = c1_r18->data[c1_i819];
      }

      c1_i820 = c1_b_environment->size[0];
      c1_b_environment->size[0] = c1_environment[0].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_environment, c1_i820,
        &c1_fi_emlrtRTEI);
      c1_e_loop_ub = c1_environment[0].f1->size[0] - 1;
      for (c1_i821 = 0; c1_i821 <= c1_e_loop_ub; c1_i821++) {
        c1_b_environment->data[c1_i821] = c1_environment[0].f1->data[c1_i821];
      }

      c1_i822 = c1_c_environment->size[0];
      c1_c_environment->size[0] = c1_environment[1].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_environment, c1_i822,
        &c1_fi_emlrtRTEI);
      c1_f_loop_ub = c1_environment[1].f1->size[0] - 1;
      for (c1_i823 = 0; c1_i823 <= c1_f_loop_ub; c1_i823++) {
        c1_c_environment->data[c1_i823] = c1_environment[1].f1->data[c1_i823];
      }

      c1_varargout_1 = c1___anon_fcn(chartInstance, c1_b_environment,
        c1_c_environment, c1_varargin_1, c1_varargin_2);
      if (c1_varargout_1) {
        c1_x->data[c1_idx] = c1_x->data[c1_idx - 1];
        c1_idx--;
      } else {
        exitg1 = true;
      }
    }

    c1_x->data[c1_idx] = c1_xc;
    c1_k++;
  }

  c1_emxFree_int32_T(chartInstance, &c1_c_environment);
  c1_emxFree_int32_T(chartInstance, &c1_b_environment);
  c1_emxFree_int32_T(chartInstance, &c1_r18);
  c1_emxFree_int32_T(chartInstance, &c1_r17);
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_environment);
}

static void c1_b_introsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  c1_coder_internal_anonymous_function c1_c_cmp;
  c1_coder_internal_anonymous_function c1_d_cmp;
  c1_coder_internal_anonymous_function c1_e_cmp;
  int32_T c1_b;
  int32_T c1_MAXDEPTH;
  c1_sBaHy6MF1FZJsDHxMqvBaiH c1_frame;
  int32_T c1_b_b;
  int32_T c1_y;
  c1_coder_internal_stack c1_st;
  int32_T c1_nd;
  const mxArray *c1_b_y = NULL;
  static char_T c1_cv72[28] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'u', 's', 'h', 'L', 'i',
    'm', 'i', 't' };

  const mxArray *c1_c_y = NULL;
  int32_T c1_n;
  const mxArray *c1_d_y = NULL;
  static char_T c1_cv73[27] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'o', 'p', 'E', 'm', 'p',
    't', 'y' };

  const mxArray *c1_e_y = NULL;
  int32_T c1_xstart;
  int32_T c1_depth;
  int32_T c1_p;
  c1_sBaHy6MF1FZJsDHxMqvBaiH c1_b_x;
  c1_sBaHy6MF1FZJsDHxMqvBaiH c1_c_x;
  int32_T c1_b_nd;
  int32_T c1_c_nd;
  const mxArray *c1_f_y = NULL;
  static char_T c1_cv74[28] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'u', 's', 'h', 'L', 'i',
    'm', 'i', 't' };

  const mxArray *c1_g_y = NULL;
  const mxArray *c1_h_y = NULL;
  static char_T c1_cv75[28] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'S', 't', 'a', 'c', 'k', 'P', 'u', 's', 'h', 'L', 'i',
    'm', 'i', 't' };

  const mxArray *c1_i_y = NULL;
  int32_T exitg1;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_ji_emlrtRTEI);
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_c_cmp, &c1_ki_emlrtRTEI);
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_d_cmp, &c1_ii_emlrtRTEI);
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_e_cmp, &c1_hi_emlrtRTEI);
  if (1 >= c1_xend) {
  } else if (c1_xend <= 32) {
    c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_e_cmp, &c1_cmp,
      &c1_hi_emlrtRTEI);
    c1_b_insertionsort(chartInstance, c1_x, 1, c1_xend, c1_e_cmp);
  } else {
    c1_b = c1_nextpow2(chartInstance, c1_xend);
    c1_MAXDEPTH = (c1_b - 1) << 1;
    c1_frame.xstart = 1;
    c1_frame.xend = c1_xend;
    c1_frame.depth = 0;
    c1_b_b = c1_MAXDEPTH;
    c1_y = c1_b_b << 1;
    c1_stack_stack(chartInstance, c1_frame, c1_y, &c1_st);
    c1_nd = c1_st.d.size[0];
    if (c1_st.n < c1_nd) {
    } else {
      c1_b_y = NULL;
      sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv72, 10, 0U, 1U, 0U, 2, 1,
        28), false);
      c1_c_y = NULL;
      sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv72, 10, 0U, 1U, 0U, 2, 1,
        28), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_b_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_c_y)));
    }

    c1_st.d.data[c1_st.n] = c1_frame;
    c1_st.n++;
    do {
      exitg1 = 0;
      c1_n = c1_st.n;
      if (c1_n > 0) {
        if (c1_st.n > 0) {
        } else {
          c1_d_y = NULL;
          sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv73, 10, 0U, 1U, 0U, 2,
            1, 27), false);
          c1_e_y = NULL;
          sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv73, 10, 0U, 1U, 0U, 2,
            1, 27), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c1_d_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c1_e_y)));
        }

        c1_frame = c1_st.d.data[c1_st.n - 1];
        c1_st.n--;
        c1_xstart = c1_frame.xstart;
        c1_xend = c1_frame.xend;
        c1_depth = c1_frame.depth + 1;
        if ((c1_xend - c1_xstart) + 1 <= 32) {
          c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_d_cmp, &c1_cmp,
            &c1_ii_emlrtRTEI);
          c1_b_insertionsort(chartInstance, c1_x, c1_xstart, c1_xend, c1_d_cmp);
        } else if (c1_depth - 1 == c1_MAXDEPTH) {
          c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_c_cmp, &c1_cmp,
            &c1_ki_emlrtRTEI);
          c1_b_heapsort(chartInstance, c1_x, c1_xstart, c1_xend, c1_c_cmp);
        } else {
          c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
            &c1_ji_emlrtRTEI);
          c1_p = c1_b_sortpartition(chartInstance, c1_x, c1_xstart, c1_xend,
            c1_b_cmp);
          if (c1_p + 1 < c1_xend) {
            c1_b_x.xstart = c1_p + 1;
            c1_b_x.xend = c1_xend;
            c1_b_x.depth = c1_depth;
            c1_b_nd = c1_st.d.size[0];
            if (c1_st.n < c1_b_nd) {
            } else {
              c1_f_y = NULL;
              sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv74, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              c1_h_y = NULL;
              sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv74, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                                c1_f_y, 14, sf_mex_call_debug
                                (sfGlobalDebugInstanceStruct, "getString", 1U,
                                 1U, 14, sf_mex_call_debug
                                 (sfGlobalDebugInstanceStruct, "message", 1U, 1U,
                                  14, c1_h_y)));
            }

            c1_st.d.data[c1_st.n] = c1_b_x;
            c1_st.n++;
          }

          if (c1_xstart < c1_p) {
            c1_c_x.xstart = c1_xstart;
            c1_c_x.xend = c1_p;
            c1_c_x.depth = c1_depth;
            c1_c_nd = c1_st.d.size[0];
            if (c1_st.n < c1_c_nd) {
            } else {
              c1_g_y = NULL;
              sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv75, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              c1_i_y = NULL;
              sf_mex_assign(&c1_i_y, sf_mex_create("y", c1_cv75, 10, 0U, 1U, 0U,
                2, 1, 28), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                                c1_g_y, 14, sf_mex_call_debug
                                (sfGlobalDebugInstanceStruct, "getString", 1U,
                                 1U, 14, sf_mex_call_debug
                                 (sfGlobalDebugInstanceStruct, "message", 1U, 1U,
                                  14, c1_i_y)));
            }

            c1_st.d.data[c1_st.n] = c1_c_x;
            c1_st.n++;
          }
        }
      } else {
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_e_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_d_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_c_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static void c1_b_heapsort(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp)
{
  c1_coder_internal_anonymous_function c1_b_cmp;
  c1_coder_internal_anonymous_function c1_c_cmp;
  int32_T c1_n;
  int32_T c1_b_xstart;
  int32_T c1_b_xend;
  int32_T c1_b_n;
  int32_T c1_idx;
  int32_T c1_i824;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_t;
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_li_emlrtRTEI);
  c1_emxInitStruct_coder_internal_an(chartInstance, &c1_c_cmp, &c1_mi_emlrtRTEI);
  c1_n = c1_xend - c1_xstart;
  c1_b_xstart = c1_xstart;
  c1_b_xend = c1_xend;
  c1_b_n = c1_n + 1;
  for (c1_idx = c1_b_n; c1_idx > 0; c1_idx--) {
    c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_b_cmp, &c1_cmp,
      &c1_li_emlrtRTEI);
    c1_b_heapify(chartInstance, c1_x, c1_idx, c1_b_xstart, c1_b_xend, c1_b_cmp);
  }

  c1_i824 = c1_n;
  c1_b = c1_i824;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_i824; c1_k++) {
    c1_t = c1_x->data[c1_xend - 1];
    c1_x->data[c1_xend - 1] = c1_x->data[c1_xstart - 1];
    c1_x->data[c1_xstart - 1] = c1_t;
    c1_xend--;
    c1_emxCopyStruct_coder_internal_an(chartInstance, &c1_c_cmp, &c1_cmp,
      &c1_mi_emlrtRTEI);
    c1_b_heapify(chartInstance, c1_x, 1, c1_xstart, c1_xend, c1_c_cmp);
  }

  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_c_cmp);
  c1_emxFreeStruct_coder_internal_an(chartInstance, &c1_b_cmp);
}

static void c1_b_heapify(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_idx, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp)
{
  boolean_T c1_changed;
  int32_T c1_xoff;
  int32_T c1_extremumIdx;
  int32_T c1_b;
  int32_T c1_y;
  int32_T c1_leftIdx;
  c1_emxArray_int32_T *c1_r19;
  c1_emxArray_int32_T *c1_r20;
  c1_cell_wrap_1 c1_environment[2];
  c1_cell_wrap_1 c1_b_environment[2];
  c1_emxArray_int32_T *c1_c_environment;
  c1_emxArray_int32_T *c1_d_environment;
  c1_emxArray_int32_T *c1_e_environment;
  c1_emxArray_int32_T *c1_f_environment;
  int32_T c1_rightIdx;
  int32_T c1_extremum;
  int32_T c1_cmpIdx;
  int32_T c1_xcmp;
  int32_T c1_xr;
  c1_cell_wrap_1 c1_g_environment[2];
  int32_T c1_varargin_1;
  c1_emxArray_int32_T *c1_h_environment;
  int32_T c1_varargin_2;
  c1_emxArray_int32_T *c1_i_environment;
  int32_T c1_i825;
  int32_T c1_loop_ub;
  int32_T c1_i826;
  int32_T c1_b_varargin_1;
  int32_T c1_b_varargin_2;
  int32_T c1_i827;
  int32_T c1_i828;
  int32_T c1_b_loop_ub;
  int32_T c1_c_loop_ub;
  int32_T c1_i829;
  int32_T c1_i830;
  int32_T c1_i831;
  int32_T c1_i832;
  int32_T c1_d_loop_ub;
  int32_T c1_e_loop_ub;
  int32_T c1_i833;
  int32_T c1_i834;
  int32_T c1_i835;
  int32_T c1_i836;
  int32_T c1_f_loop_ub;
  int32_T c1_g_loop_ub;
  int32_T c1_i837;
  int32_T c1_i838;
  int32_T c1_i839;
  int32_T c1_i840;
  int32_T c1_h_loop_ub;
  int32_T c1_i_loop_ub;
  int32_T c1_i841;
  int32_T c1_i842;
  int32_T c1_i843;
  int32_T c1_i844;
  int32_T c1_j_loop_ub;
  int32_T c1_k_loop_ub;
  int32_T c1_i845;
  int32_T c1_i846;
  int32_T c1_i847;
  boolean_T c1_varargout_1;
  int32_T c1_l_loop_ub;
  int32_T c1_c_varargin_1;
  int32_T c1_i848;
  int32_T c1_c_varargin_2;
  int32_T c1_i849;
  boolean_T c1_b_varargout_1;
  int32_T c1_m_loop_ub;
  int32_T c1_i850;
  int32_T c1_i851;
  int32_T c1_n_loop_ub;
  int32_T c1_i852;
  int32_T c1_i853;
  int32_T c1_o_loop_ub;
  int32_T c1_i854;
  int32_T c1_i855;
  int32_T c1_p_loop_ub;
  int32_T c1_i856;
  int32_T c1_i857;
  int32_T c1_q_loop_ub;
  int32_T c1_i858;
  int32_T c1_i859;
  int32_T c1_r_loop_ub;
  int32_T c1_i860;
  boolean_T c1_c_varargout_1;
  int32_T c1_b_b;
  int32_T c1_b_y;
  c1_changed = true;
  c1_xoff = c1_xstart - 1;
  c1_extremumIdx = (c1_idx + c1_xoff) - 1;
  c1_b = c1_idx;
  c1_y = c1_b << 1;
  c1_leftIdx = c1_y + c1_xoff;
  c1_emxInit_int32_T(chartInstance, &c1_r19, 1, &c1_pc_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_r20, 1, &c1_pc_emlrtRTEI);
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_environment, &c1_gi_emlrtRTEI);
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_b_environment, &c1_gi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_c_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_d_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_e_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_f_environment, 1, &c1_fi_emlrtRTEI);
  while (c1_changed && (c1_leftIdx < c1_xend)) {
    c1_changed = false;
    c1_rightIdx = c1_leftIdx;
    c1_extremum = c1_x->data[c1_extremumIdx];
    c1_cmpIdx = c1_leftIdx;
    c1_xcmp = c1_x->data[c1_leftIdx - 1];
    c1_xr = c1_x->data[c1_rightIdx];
    c1_varargin_1 = c1_xcmp;
    c1_varargin_2 = c1_xr;
    c1_i825 = c1_r19->size[0];
    c1_r19->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r19, c1_i825,
      &c1_ei_emlrtRTEI);
    c1_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
    for (c1_i826 = 0; c1_i826 <= c1_loop_ub; c1_i826++) {
      c1_r19->data[c1_i826] = c1_cmp.tunableEnvironment[0].f1->data[c1_i826];
    }

    c1_i828 = c1_r20->size[0];
    c1_r20->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r20, c1_i828,
      &c1_ei_emlrtRTEI);
    c1_c_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
    for (c1_i830 = 0; c1_i830 <= c1_c_loop_ub; c1_i830++) {
      c1_r20->data[c1_i830] = c1_cmp.tunableEnvironment[1].f1->data[c1_i830];
    }

    c1_i832 = c1_environment[0].f1->size[0];
    c1_environment[0].f1->size[0] = c1_r19->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_environment[0].f1, c1_i832,
      &c1_ei_emlrtRTEI);
    c1_e_loop_ub = c1_r19->size[0] - 1;
    for (c1_i834 = 0; c1_i834 <= c1_e_loop_ub; c1_i834++) {
      c1_environment[0].f1->data[c1_i834] = c1_r19->data[c1_i834];
    }

    c1_i836 = c1_environment[1].f1->size[0];
    c1_environment[1].f1->size[0] = c1_r20->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_environment[1].f1, c1_i836,
      &c1_ei_emlrtRTEI);
    c1_g_loop_ub = c1_r20->size[0] - 1;
    for (c1_i838 = 0; c1_i838 <= c1_g_loop_ub; c1_i838++) {
      c1_environment[1].f1->data[c1_i838] = c1_r20->data[c1_i838];
    }

    c1_i840 = c1_e_environment->size[0];
    c1_e_environment->size[0] = c1_environment[0].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_e_environment, c1_i840,
      &c1_fi_emlrtRTEI);
    c1_i_loop_ub = c1_environment[0].f1->size[0] - 1;
    for (c1_i842 = 0; c1_i842 <= c1_i_loop_ub; c1_i842++) {
      c1_e_environment->data[c1_i842] = c1_environment[0].f1->data[c1_i842];
    }

    c1_i844 = c1_f_environment->size[0];
    c1_f_environment->size[0] = c1_environment[1].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_f_environment, c1_i844,
      &c1_fi_emlrtRTEI);
    c1_k_loop_ub = c1_environment[1].f1->size[0] - 1;
    for (c1_i846 = 0; c1_i846 <= c1_k_loop_ub; c1_i846++) {
      c1_f_environment->data[c1_i846] = c1_environment[1].f1->data[c1_i846];
    }

    c1_varargout_1 = c1___anon_fcn(chartInstance, c1_e_environment,
      c1_f_environment, c1_varargin_1, c1_varargin_2);
    if (c1_varargout_1) {
      c1_cmpIdx = c1_leftIdx + 1;
      c1_xcmp = c1_xr;
    }

    c1_c_varargin_1 = c1_extremum;
    c1_c_varargin_2 = c1_xcmp;
    c1_i849 = c1_r19->size[0];
    c1_r19->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r19, c1_i849,
      &c1_ei_emlrtRTEI);
    c1_m_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
    for (c1_i850 = 0; c1_i850 <= c1_m_loop_ub; c1_i850++) {
      c1_r19->data[c1_i850] = c1_cmp.tunableEnvironment[0].f1->data[c1_i850];
    }

    c1_i851 = c1_r20->size[0];
    c1_r20->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r20, c1_i851,
      &c1_ei_emlrtRTEI);
    c1_n_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
    for (c1_i852 = 0; c1_i852 <= c1_n_loop_ub; c1_i852++) {
      c1_r20->data[c1_i852] = c1_cmp.tunableEnvironment[1].f1->data[c1_i852];
    }

    c1_i853 = c1_b_environment[0].f1->size[0];
    c1_b_environment[0].f1->size[0] = c1_r19->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_environment[0].f1, c1_i853,
      &c1_ei_emlrtRTEI);
    c1_o_loop_ub = c1_r19->size[0] - 1;
    for (c1_i854 = 0; c1_i854 <= c1_o_loop_ub; c1_i854++) {
      c1_b_environment[0].f1->data[c1_i854] = c1_r19->data[c1_i854];
    }

    c1_i855 = c1_b_environment[1].f1->size[0];
    c1_b_environment[1].f1->size[0] = c1_r20->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_environment[1].f1, c1_i855,
      &c1_ei_emlrtRTEI);
    c1_p_loop_ub = c1_r20->size[0] - 1;
    for (c1_i856 = 0; c1_i856 <= c1_p_loop_ub; c1_i856++) {
      c1_b_environment[1].f1->data[c1_i856] = c1_r20->data[c1_i856];
    }

    c1_i857 = c1_c_environment->size[0];
    c1_c_environment->size[0] = c1_b_environment[0].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_environment, c1_i857,
      &c1_fi_emlrtRTEI);
    c1_q_loop_ub = c1_b_environment[0].f1->size[0] - 1;
    for (c1_i858 = 0; c1_i858 <= c1_q_loop_ub; c1_i858++) {
      c1_c_environment->data[c1_i858] = c1_b_environment[0].f1->data[c1_i858];
    }

    c1_i859 = c1_d_environment->size[0];
    c1_d_environment->size[0] = c1_b_environment[1].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_d_environment, c1_i859,
      &c1_fi_emlrtRTEI);
    c1_r_loop_ub = c1_b_environment[1].f1->size[0] - 1;
    for (c1_i860 = 0; c1_i860 <= c1_r_loop_ub; c1_i860++) {
      c1_d_environment->data[c1_i860] = c1_b_environment[1].f1->data[c1_i860];
    }

    c1_c_varargout_1 = c1___anon_fcn(chartInstance, c1_c_environment,
      c1_d_environment, c1_c_varargin_1, c1_c_varargin_2);
    if (c1_c_varargout_1) {
      c1_x->data[c1_extremumIdx] = c1_xcmp;
      c1_x->data[c1_cmpIdx - 1] = c1_extremum;
      c1_extremumIdx = c1_cmpIdx - 1;
      c1_b_b = c1_cmpIdx - c1_xoff;
      c1_b_y = c1_b_b << 1;
      c1_leftIdx = c1_b_y + c1_xoff;
      c1_changed = true;
    }
  }

  c1_emxFree_int32_T(chartInstance, &c1_f_environment);
  c1_emxFree_int32_T(chartInstance, &c1_e_environment);
  c1_emxFree_int32_T(chartInstance, &c1_d_environment);
  c1_emxFree_int32_T(chartInstance, &c1_c_environment);
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_b_environment);
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_environment);
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_g_environment, &c1_gi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_h_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_i_environment, 1, &c1_fi_emlrtRTEI);
  if (c1_changed && (c1_leftIdx <= c1_xend)) {
    c1_extremum = c1_x->data[c1_extremumIdx];
    c1_xcmp = c1_x->data[c1_leftIdx - 1];
    c1_b_varargin_1 = c1_extremum;
    c1_b_varargin_2 = c1_xcmp;
    c1_i827 = c1_r19->size[0];
    c1_r19->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r19, c1_i827,
      &c1_ei_emlrtRTEI);
    c1_b_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
    for (c1_i829 = 0; c1_i829 <= c1_b_loop_ub; c1_i829++) {
      c1_r19->data[c1_i829] = c1_cmp.tunableEnvironment[0].f1->data[c1_i829];
    }

    c1_i831 = c1_r20->size[0];
    c1_r20->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r20, c1_i831,
      &c1_ei_emlrtRTEI);
    c1_d_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
    for (c1_i833 = 0; c1_i833 <= c1_d_loop_ub; c1_i833++) {
      c1_r20->data[c1_i833] = c1_cmp.tunableEnvironment[1].f1->data[c1_i833];
    }

    c1_i835 = c1_g_environment[0].f1->size[0];
    c1_g_environment[0].f1->size[0] = c1_r19->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_g_environment[0].f1, c1_i835,
      &c1_ei_emlrtRTEI);
    c1_f_loop_ub = c1_r19->size[0] - 1;
    for (c1_i837 = 0; c1_i837 <= c1_f_loop_ub; c1_i837++) {
      c1_g_environment[0].f1->data[c1_i837] = c1_r19->data[c1_i837];
    }

    c1_i839 = c1_g_environment[1].f1->size[0];
    c1_g_environment[1].f1->size[0] = c1_r20->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_g_environment[1].f1, c1_i839,
      &c1_ei_emlrtRTEI);
    c1_h_loop_ub = c1_r20->size[0] - 1;
    for (c1_i841 = 0; c1_i841 <= c1_h_loop_ub; c1_i841++) {
      c1_g_environment[1].f1->data[c1_i841] = c1_r20->data[c1_i841];
    }

    c1_i843 = c1_h_environment->size[0];
    c1_h_environment->size[0] = c1_g_environment[0].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_h_environment, c1_i843,
      &c1_fi_emlrtRTEI);
    c1_j_loop_ub = c1_g_environment[0].f1->size[0] - 1;
    for (c1_i845 = 0; c1_i845 <= c1_j_loop_ub; c1_i845++) {
      c1_h_environment->data[c1_i845] = c1_g_environment[0].f1->data[c1_i845];
    }

    c1_i847 = c1_i_environment->size[0];
    c1_i_environment->size[0] = c1_g_environment[1].f1->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_i_environment, c1_i847,
      &c1_fi_emlrtRTEI);
    c1_l_loop_ub = c1_g_environment[1].f1->size[0] - 1;
    for (c1_i848 = 0; c1_i848 <= c1_l_loop_ub; c1_i848++) {
      c1_i_environment->data[c1_i848] = c1_g_environment[1].f1->data[c1_i848];
    }

    c1_b_varargout_1 = c1___anon_fcn(chartInstance, c1_h_environment,
      c1_i_environment, c1_b_varargin_1, c1_b_varargin_2);
    if (c1_b_varargout_1) {
      c1_x->data[c1_extremumIdx] = c1_xcmp;
      c1_x->data[c1_leftIdx - 1] = c1_extremum;
    }
  }

  c1_emxFree_int32_T(chartInstance, &c1_i_environment);
  c1_emxFree_int32_T(chartInstance, &c1_h_environment);
  c1_emxFree_int32_T(chartInstance, &c1_r20);
  c1_emxFree_int32_T(chartInstance, &c1_r19);
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_g_environment);
}

static int32_T c1_b_sortpartition(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_x, int32_T c1_xstart, int32_T c1_xend,
  c1_coder_internal_anonymous_function c1_cmp)
{
  int32_T c1_p;
  c1_emxArray_int32_T *c1_r21;
  int32_T c1_a;
  int32_T c1_c;
  int32_T c1_xmid;
  int32_T c1_varargin_1;
  int32_T c1_varargin_2;
  int32_T c1_i861;
  int32_T c1_loop_ub;
  int32_T c1_i862;
  c1_emxArray_int32_T *c1_r22;
  int32_T c1_i863;
  int32_T c1_b_loop_ub;
  int32_T c1_i864;
  c1_cell_wrap_1 c1_environment[2];
  int32_T c1_i865;
  int32_T c1_c_loop_ub;
  int32_T c1_i866;
  int32_T c1_i867;
  int32_T c1_d_loop_ub;
  int32_T c1_i868;
  c1_emxArray_int32_T *c1_b_environment;
  int32_T c1_i869;
  int32_T c1_e_loop_ub;
  int32_T c1_i870;
  c1_emxArray_int32_T *c1_c_environment;
  int32_T c1_i871;
  int32_T c1_f_loop_ub;
  int32_T c1_i872;
  boolean_T c1_varargout_1;
  int32_T c1_t;
  int32_T c1_b_varargin_1;
  int32_T c1_b_varargin_2;
  int32_T c1_i873;
  int32_T c1_g_loop_ub;
  int32_T c1_i874;
  int32_T c1_i875;
  int32_T c1_h_loop_ub;
  int32_T c1_i876;
  c1_cell_wrap_1 c1_d_environment[2];
  int32_T c1_i877;
  int32_T c1_i_loop_ub;
  int32_T c1_i878;
  int32_T c1_i879;
  int32_T c1_j_loop_ub;
  int32_T c1_i880;
  c1_emxArray_int32_T *c1_e_environment;
  int32_T c1_i881;
  int32_T c1_k_loop_ub;
  int32_T c1_i882;
  c1_emxArray_int32_T *c1_f_environment;
  int32_T c1_i883;
  int32_T c1_l_loop_ub;
  int32_T c1_i884;
  boolean_T c1_b_varargout_1;
  int32_T c1_c_varargin_1;
  int32_T c1_c_varargin_2;
  int32_T c1_i885;
  int32_T c1_m_loop_ub;
  int32_T c1_i886;
  int32_T c1_i887;
  int32_T c1_n_loop_ub;
  int32_T c1_i888;
  c1_cell_wrap_1 c1_g_environment[2];
  int32_T c1_i889;
  int32_T c1_o_loop_ub;
  int32_T c1_i890;
  int32_T c1_i891;
  int32_T c1_p_loop_ub;
  int32_T c1_i892;
  c1_emxArray_int32_T *c1_h_environment;
  int32_T c1_i893;
  int32_T c1_q_loop_ub;
  int32_T c1_i894;
  c1_emxArray_int32_T *c1_i_environment;
  int32_T c1_i895;
  int32_T c1_r_loop_ub;
  int32_T c1_i896;
  boolean_T c1_c_varargout_1;
  int32_T c1_pivot;
  int32_T c1_i;
  int32_T c1_j;
  c1_cell_wrap_1 c1_j_environment[2];
  c1_cell_wrap_1 c1_k_environment[2];
  c1_emxArray_int32_T *c1_l_environment;
  c1_emxArray_int32_T *c1_m_environment;
  c1_emxArray_int32_T *c1_n_environment;
  c1_emxArray_int32_T *c1_o_environment;
  int32_T c1_d_varargin_1;
  int32_T c1_d_varargin_2;
  int32_T c1_i897;
  int32_T c1_s_loop_ub;
  int32_T c1_i898;
  int32_T c1_i899;
  int32_T c1_t_loop_ub;
  int32_T c1_i900;
  int32_T c1_i901;
  int32_T c1_u_loop_ub;
  int32_T c1_i902;
  int32_T c1_i903;
  int32_T c1_v_loop_ub;
  int32_T c1_i904;
  int32_T c1_i905;
  int32_T c1_w_loop_ub;
  int32_T c1_i906;
  int32_T c1_i907;
  int32_T c1_x_loop_ub;
  int32_T c1_i908;
  boolean_T c1_d_varargout_1;
  int32_T c1_e_varargin_1;
  int32_T c1_e_varargin_2;
  int32_T c1_i909;
  int32_T c1_y_loop_ub;
  int32_T c1_i910;
  int32_T c1_i911;
  int32_T c1_ab_loop_ub;
  int32_T c1_i912;
  int32_T c1_i913;
  int32_T c1_bb_loop_ub;
  int32_T c1_i914;
  int32_T c1_i915;
  int32_T c1_cb_loop_ub;
  int32_T c1_i916;
  int32_T c1_i917;
  int32_T c1_db_loop_ub;
  int32_T c1_i918;
  int32_T c1_i919;
  int32_T c1_eb_loop_ub;
  int32_T c1_i920;
  boolean_T c1_e_varargout_1;
  int32_T exitg1;
  int32_T exitg2;
  c1_emxInit_int32_T(chartInstance, &c1_r21, 1, &c1_qc_emlrtRTEI);
  c1_a = c1_xend - c1_xstart;
  c1_c = c1_div_nzp_s32(chartInstance, c1_a, 2, 1U, 0, 0);
  c1_xmid = (c1_xstart + c1_c) - 1;
  c1_varargin_1 = c1_x->data[c1_xmid];
  c1_varargin_2 = c1_x->data[c1_xstart - 1];
  c1_i861 = c1_r21->size[0];
  c1_r21->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_r21, c1_i861, &c1_ei_emlrtRTEI);
  c1_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
  for (c1_i862 = 0; c1_i862 <= c1_loop_ub; c1_i862++) {
    c1_r21->data[c1_i862] = c1_cmp.tunableEnvironment[0].f1->data[c1_i862];
  }

  c1_emxInit_int32_T(chartInstance, &c1_r22, 1, &c1_qc_emlrtRTEI);
  c1_i863 = c1_r22->size[0];
  c1_r22->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_r22, c1_i863, &c1_ei_emlrtRTEI);
  c1_b_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
  for (c1_i864 = 0; c1_i864 <= c1_b_loop_ub; c1_i864++) {
    c1_r22->data[c1_i864] = c1_cmp.tunableEnvironment[1].f1->data[c1_i864];
  }

  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_environment, &c1_gi_emlrtRTEI);
  c1_i865 = c1_environment[0].f1->size[0];
  c1_environment[0].f1->size[0] = c1_r21->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_environment[0].f1, c1_i865,
    &c1_ei_emlrtRTEI);
  c1_c_loop_ub = c1_r21->size[0] - 1;
  for (c1_i866 = 0; c1_i866 <= c1_c_loop_ub; c1_i866++) {
    c1_environment[0].f1->data[c1_i866] = c1_r21->data[c1_i866];
  }

  c1_i867 = c1_environment[1].f1->size[0];
  c1_environment[1].f1->size[0] = c1_r22->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_environment[1].f1, c1_i867,
    &c1_ei_emlrtRTEI);
  c1_d_loop_ub = c1_r22->size[0] - 1;
  for (c1_i868 = 0; c1_i868 <= c1_d_loop_ub; c1_i868++) {
    c1_environment[1].f1->data[c1_i868] = c1_r22->data[c1_i868];
  }

  c1_emxInit_int32_T(chartInstance, &c1_b_environment, 1, &c1_fi_emlrtRTEI);
  c1_i869 = c1_b_environment->size[0];
  c1_b_environment->size[0] = c1_environment[0].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_b_environment, c1_i869,
    &c1_fi_emlrtRTEI);
  c1_e_loop_ub = c1_environment[0].f1->size[0] - 1;
  for (c1_i870 = 0; c1_i870 <= c1_e_loop_ub; c1_i870++) {
    c1_b_environment->data[c1_i870] = c1_environment[0].f1->data[c1_i870];
  }

  c1_emxInit_int32_T(chartInstance, &c1_c_environment, 1, &c1_fi_emlrtRTEI);
  c1_i871 = c1_c_environment->size[0];
  c1_c_environment->size[0] = c1_environment[1].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_c_environment, c1_i871,
    &c1_fi_emlrtRTEI);
  c1_f_loop_ub = c1_environment[1].f1->size[0] - 1;
  for (c1_i872 = 0; c1_i872 <= c1_f_loop_ub; c1_i872++) {
    c1_c_environment->data[c1_i872] = c1_environment[1].f1->data[c1_i872];
  }

  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_environment);
  c1_varargout_1 = c1___anon_fcn(chartInstance, c1_b_environment,
    c1_c_environment, c1_varargin_1, c1_varargin_2);
  c1_emxFree_int32_T(chartInstance, &c1_c_environment);
  c1_emxFree_int32_T(chartInstance, &c1_b_environment);
  if (c1_varargout_1) {
    c1_t = c1_x->data[c1_xstart - 1];
    c1_x->data[c1_xstart - 1] = c1_x->data[c1_xmid];
    c1_x->data[c1_xmid] = c1_t;
  }

  c1_b_varargin_1 = c1_x->data[c1_xend - 1];
  c1_b_varargin_2 = c1_x->data[c1_xstart - 1];
  c1_i873 = c1_r21->size[0];
  c1_r21->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_r21, c1_i873, &c1_ei_emlrtRTEI);
  c1_g_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
  for (c1_i874 = 0; c1_i874 <= c1_g_loop_ub; c1_i874++) {
    c1_r21->data[c1_i874] = c1_cmp.tunableEnvironment[0].f1->data[c1_i874];
  }

  c1_i875 = c1_r22->size[0];
  c1_r22->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_r22, c1_i875, &c1_ei_emlrtRTEI);
  c1_h_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
  for (c1_i876 = 0; c1_i876 <= c1_h_loop_ub; c1_i876++) {
    c1_r22->data[c1_i876] = c1_cmp.tunableEnvironment[1].f1->data[c1_i876];
  }

  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_d_environment, &c1_gi_emlrtRTEI);
  c1_i877 = c1_d_environment[0].f1->size[0];
  c1_d_environment[0].f1->size[0] = c1_r21->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_d_environment[0].f1, c1_i877,
    &c1_ei_emlrtRTEI);
  c1_i_loop_ub = c1_r21->size[0] - 1;
  for (c1_i878 = 0; c1_i878 <= c1_i_loop_ub; c1_i878++) {
    c1_d_environment[0].f1->data[c1_i878] = c1_r21->data[c1_i878];
  }

  c1_i879 = c1_d_environment[1].f1->size[0];
  c1_d_environment[1].f1->size[0] = c1_r22->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_d_environment[1].f1, c1_i879,
    &c1_ei_emlrtRTEI);
  c1_j_loop_ub = c1_r22->size[0] - 1;
  for (c1_i880 = 0; c1_i880 <= c1_j_loop_ub; c1_i880++) {
    c1_d_environment[1].f1->data[c1_i880] = c1_r22->data[c1_i880];
  }

  c1_emxInit_int32_T(chartInstance, &c1_e_environment, 1, &c1_fi_emlrtRTEI);
  c1_i881 = c1_e_environment->size[0];
  c1_e_environment->size[0] = c1_d_environment[0].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_e_environment, c1_i881,
    &c1_fi_emlrtRTEI);
  c1_k_loop_ub = c1_d_environment[0].f1->size[0] - 1;
  for (c1_i882 = 0; c1_i882 <= c1_k_loop_ub; c1_i882++) {
    c1_e_environment->data[c1_i882] = c1_d_environment[0].f1->data[c1_i882];
  }

  c1_emxInit_int32_T(chartInstance, &c1_f_environment, 1, &c1_fi_emlrtRTEI);
  c1_i883 = c1_f_environment->size[0];
  c1_f_environment->size[0] = c1_d_environment[1].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_f_environment, c1_i883,
    &c1_fi_emlrtRTEI);
  c1_l_loop_ub = c1_d_environment[1].f1->size[0] - 1;
  for (c1_i884 = 0; c1_i884 <= c1_l_loop_ub; c1_i884++) {
    c1_f_environment->data[c1_i884] = c1_d_environment[1].f1->data[c1_i884];
  }

  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_d_environment);
  c1_b_varargout_1 = c1___anon_fcn(chartInstance, c1_e_environment,
    c1_f_environment, c1_b_varargin_1, c1_b_varargin_2);
  c1_emxFree_int32_T(chartInstance, &c1_f_environment);
  c1_emxFree_int32_T(chartInstance, &c1_e_environment);
  if (c1_b_varargout_1) {
    c1_t = c1_x->data[c1_xstart - 1];
    c1_x->data[c1_xstart - 1] = c1_x->data[c1_xend - 1];
    c1_x->data[c1_xend - 1] = c1_t;
  }

  c1_c_varargin_1 = c1_x->data[c1_xend - 1];
  c1_c_varargin_2 = c1_x->data[c1_xmid];
  c1_i885 = c1_r21->size[0];
  c1_r21->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_r21, c1_i885, &c1_ei_emlrtRTEI);
  c1_m_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
  for (c1_i886 = 0; c1_i886 <= c1_m_loop_ub; c1_i886++) {
    c1_r21->data[c1_i886] = c1_cmp.tunableEnvironment[0].f1->data[c1_i886];
  }

  c1_i887 = c1_r22->size[0];
  c1_r22->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_r22, c1_i887, &c1_ei_emlrtRTEI);
  c1_n_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
  for (c1_i888 = 0; c1_i888 <= c1_n_loop_ub; c1_i888++) {
    c1_r22->data[c1_i888] = c1_cmp.tunableEnvironment[1].f1->data[c1_i888];
  }

  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_g_environment, &c1_gi_emlrtRTEI);
  c1_i889 = c1_g_environment[0].f1->size[0];
  c1_g_environment[0].f1->size[0] = c1_r21->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_g_environment[0].f1, c1_i889,
    &c1_ei_emlrtRTEI);
  c1_o_loop_ub = c1_r21->size[0] - 1;
  for (c1_i890 = 0; c1_i890 <= c1_o_loop_ub; c1_i890++) {
    c1_g_environment[0].f1->data[c1_i890] = c1_r21->data[c1_i890];
  }

  c1_i891 = c1_g_environment[1].f1->size[0];
  c1_g_environment[1].f1->size[0] = c1_r22->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_g_environment[1].f1, c1_i891,
    &c1_ei_emlrtRTEI);
  c1_p_loop_ub = c1_r22->size[0] - 1;
  for (c1_i892 = 0; c1_i892 <= c1_p_loop_ub; c1_i892++) {
    c1_g_environment[1].f1->data[c1_i892] = c1_r22->data[c1_i892];
  }

  c1_emxInit_int32_T(chartInstance, &c1_h_environment, 1, &c1_fi_emlrtRTEI);
  c1_i893 = c1_h_environment->size[0];
  c1_h_environment->size[0] = c1_g_environment[0].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_h_environment, c1_i893,
    &c1_fi_emlrtRTEI);
  c1_q_loop_ub = c1_g_environment[0].f1->size[0] - 1;
  for (c1_i894 = 0; c1_i894 <= c1_q_loop_ub; c1_i894++) {
    c1_h_environment->data[c1_i894] = c1_g_environment[0].f1->data[c1_i894];
  }

  c1_emxInit_int32_T(chartInstance, &c1_i_environment, 1, &c1_fi_emlrtRTEI);
  c1_i895 = c1_i_environment->size[0];
  c1_i_environment->size[0] = c1_g_environment[1].f1->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_i_environment, c1_i895,
    &c1_fi_emlrtRTEI);
  c1_r_loop_ub = c1_g_environment[1].f1->size[0] - 1;
  for (c1_i896 = 0; c1_i896 <= c1_r_loop_ub; c1_i896++) {
    c1_i_environment->data[c1_i896] = c1_g_environment[1].f1->data[c1_i896];
  }

  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_g_environment);
  c1_c_varargout_1 = c1___anon_fcn(chartInstance, c1_h_environment,
    c1_i_environment, c1_c_varargin_1, c1_c_varargin_2);
  c1_emxFree_int32_T(chartInstance, &c1_i_environment);
  c1_emxFree_int32_T(chartInstance, &c1_h_environment);
  if (c1_c_varargout_1) {
    c1_t = c1_x->data[c1_xmid];
    c1_x->data[c1_xmid] = c1_x->data[c1_xend - 1];
    c1_x->data[c1_xend - 1] = c1_t;
  }

  c1_pivot = c1_x->data[c1_xmid];
  c1_x->data[c1_xmid] = c1_x->data[c1_xend - 2];
  c1_x->data[c1_xend - 2] = c1_pivot;
  c1_i = c1_xstart - 1;
  c1_j = c1_xend - 2;
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_j_environment, &c1_gi_emlrtRTEI);
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_k_environment, &c1_gi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_l_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_m_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_n_environment, 1, &c1_fi_emlrtRTEI);
  c1_emxInit_int32_T(chartInstance, &c1_o_environment, 1, &c1_fi_emlrtRTEI);
  do {
    exitg1 = 0;
    c1_i++;
    do {
      exitg2 = 0;
      c1_d_varargin_1 = c1_x->data[c1_i];
      c1_d_varargin_2 = c1_pivot;
      c1_i897 = c1_r21->size[0];
      c1_r21->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_r21, c1_i897,
        &c1_ei_emlrtRTEI);
      c1_s_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
      for (c1_i898 = 0; c1_i898 <= c1_s_loop_ub; c1_i898++) {
        c1_r21->data[c1_i898] = c1_cmp.tunableEnvironment[0].f1->data[c1_i898];
      }

      c1_i899 = c1_r22->size[0];
      c1_r22->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_r22, c1_i899,
        &c1_ei_emlrtRTEI);
      c1_t_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
      for (c1_i900 = 0; c1_i900 <= c1_t_loop_ub; c1_i900++) {
        c1_r22->data[c1_i900] = c1_cmp.tunableEnvironment[1].f1->data[c1_i900];
      }

      c1_i901 = c1_j_environment[0].f1->size[0];
      c1_j_environment[0].f1->size[0] = c1_r21->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_j_environment[0].f1,
        c1_i901, &c1_ei_emlrtRTEI);
      c1_u_loop_ub = c1_r21->size[0] - 1;
      for (c1_i902 = 0; c1_i902 <= c1_u_loop_ub; c1_i902++) {
        c1_j_environment[0].f1->data[c1_i902] = c1_r21->data[c1_i902];
      }

      c1_i903 = c1_j_environment[1].f1->size[0];
      c1_j_environment[1].f1->size[0] = c1_r22->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_j_environment[1].f1,
        c1_i903, &c1_ei_emlrtRTEI);
      c1_v_loop_ub = c1_r22->size[0] - 1;
      for (c1_i904 = 0; c1_i904 <= c1_v_loop_ub; c1_i904++) {
        c1_j_environment[1].f1->data[c1_i904] = c1_r22->data[c1_i904];
      }

      c1_i905 = c1_n_environment->size[0];
      c1_n_environment->size[0] = c1_j_environment[0].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_n_environment, c1_i905,
        &c1_fi_emlrtRTEI);
      c1_w_loop_ub = c1_j_environment[0].f1->size[0] - 1;
      for (c1_i906 = 0; c1_i906 <= c1_w_loop_ub; c1_i906++) {
        c1_n_environment->data[c1_i906] = c1_j_environment[0].f1->data[c1_i906];
      }

      c1_i907 = c1_o_environment->size[0];
      c1_o_environment->size[0] = c1_j_environment[1].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_o_environment, c1_i907,
        &c1_fi_emlrtRTEI);
      c1_x_loop_ub = c1_j_environment[1].f1->size[0] - 1;
      for (c1_i908 = 0; c1_i908 <= c1_x_loop_ub; c1_i908++) {
        c1_o_environment->data[c1_i908] = c1_j_environment[1].f1->data[c1_i908];
      }

      c1_d_varargout_1 = c1___anon_fcn(chartInstance, c1_n_environment,
        c1_o_environment, c1_d_varargin_1, c1_d_varargin_2);
      if (c1_d_varargout_1) {
        c1_i++;
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    c1_j--;
    do {
      exitg2 = 0;
      c1_e_varargin_1 = c1_pivot;
      c1_e_varargin_2 = c1_x->data[c1_j];
      c1_i909 = c1_r21->size[0];
      c1_r21->size[0] = c1_cmp.tunableEnvironment[0].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_r21, c1_i909,
        &c1_ei_emlrtRTEI);
      c1_y_loop_ub = c1_cmp.tunableEnvironment[0].f1->size[0] - 1;
      for (c1_i910 = 0; c1_i910 <= c1_y_loop_ub; c1_i910++) {
        c1_r21->data[c1_i910] = c1_cmp.tunableEnvironment[0].f1->data[c1_i910];
      }

      c1_i911 = c1_r22->size[0];
      c1_r22->size[0] = c1_cmp.tunableEnvironment[1].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_r22, c1_i911,
        &c1_ei_emlrtRTEI);
      c1_ab_loop_ub = c1_cmp.tunableEnvironment[1].f1->size[0] - 1;
      for (c1_i912 = 0; c1_i912 <= c1_ab_loop_ub; c1_i912++) {
        c1_r22->data[c1_i912] = c1_cmp.tunableEnvironment[1].f1->data[c1_i912];
      }

      c1_i913 = c1_k_environment[0].f1->size[0];
      c1_k_environment[0].f1->size[0] = c1_r21->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_k_environment[0].f1,
        c1_i913, &c1_ei_emlrtRTEI);
      c1_bb_loop_ub = c1_r21->size[0] - 1;
      for (c1_i914 = 0; c1_i914 <= c1_bb_loop_ub; c1_i914++) {
        c1_k_environment[0].f1->data[c1_i914] = c1_r21->data[c1_i914];
      }

      c1_i915 = c1_k_environment[1].f1->size[0];
      c1_k_environment[1].f1->size[0] = c1_r22->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_k_environment[1].f1,
        c1_i915, &c1_ei_emlrtRTEI);
      c1_cb_loop_ub = c1_r22->size[0] - 1;
      for (c1_i916 = 0; c1_i916 <= c1_cb_loop_ub; c1_i916++) {
        c1_k_environment[1].f1->data[c1_i916] = c1_r22->data[c1_i916];
      }

      c1_i917 = c1_l_environment->size[0];
      c1_l_environment->size[0] = c1_k_environment[0].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_l_environment, c1_i917,
        &c1_fi_emlrtRTEI);
      c1_db_loop_ub = c1_k_environment[0].f1->size[0] - 1;
      for (c1_i918 = 0; c1_i918 <= c1_db_loop_ub; c1_i918++) {
        c1_l_environment->data[c1_i918] = c1_k_environment[0].f1->data[c1_i918];
      }

      c1_i919 = c1_m_environment->size[0];
      c1_m_environment->size[0] = c1_k_environment[1].f1->size[0];
      c1_emxEnsureCapacity_int32_T(chartInstance, c1_m_environment, c1_i919,
        &c1_fi_emlrtRTEI);
      c1_eb_loop_ub = c1_k_environment[1].f1->size[0] - 1;
      for (c1_i920 = 0; c1_i920 <= c1_eb_loop_ub; c1_i920++) {
        c1_m_environment->data[c1_i920] = c1_k_environment[1].f1->data[c1_i920];
      }

      c1_e_varargout_1 = c1___anon_fcn(chartInstance, c1_l_environment,
        c1_m_environment, c1_e_varargin_1, c1_e_varargin_2);
      if (c1_e_varargout_1) {
        c1_j--;
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    if (c1_i + 1 >= c1_j + 1) {
      exitg1 = 1;
    } else {
      c1_t = c1_x->data[c1_i];
      c1_x->data[c1_i] = c1_x->data[c1_j];
      c1_x->data[c1_j] = c1_t;
    }
  } while (exitg1 == 0);

  c1_emxFree_int32_T(chartInstance, &c1_o_environment);
  c1_emxFree_int32_T(chartInstance, &c1_n_environment);
  c1_emxFree_int32_T(chartInstance, &c1_m_environment);
  c1_emxFree_int32_T(chartInstance, &c1_l_environment);
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_k_environment);
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_j_environment);
  c1_emxFree_int32_T(chartInstance, &c1_r22);
  c1_emxFree_int32_T(chartInstance, &c1_r21);
  c1_p = c1_i + 1;
  c1_x->data[c1_xend - 2] = c1_x->data[c1_i];
  c1_x->data[c1_i] = c1_pivot;
  return c1_p;
}

static void c1_b_permuteVector(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_int32_T *c1_y)
{
  c1_emxArray_int32_T *c1_t;
  int32_T c1_ny;
  int32_T c1_i921;
  int32_T c1_loop_ub;
  int32_T c1_i922;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  c1_emxInit_int32_T(chartInstance, &c1_t, 1, &c1_ni_emlrtRTEI);
  c1_ny = c1_y->size[0];
  c1_i921 = c1_t->size[0];
  c1_t->size[0] = c1_y->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_t, c1_i921, &c1_ni_emlrtRTEI);
  c1_loop_ub = c1_y->size[0] - 1;
  for (c1_i922 = 0; c1_i922 <= c1_loop_ub; c1_i922++) {
    c1_t->data[c1_i922] = c1_y->data[c1_i922];
  }

  c1_b = c1_ny;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_ny; c1_k++) {
    c1_b_k = c1_k;
    c1_y->data[c1_b_k] = c1_t->data[c1_idx->data[c1_b_k] - 1];
  }

  c1_emxFree_int32_T(chartInstance, &c1_t);
}

static void c1_b_sparse_fillIn(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_coder_internal_sparse *c1_this)
{
  int32_T c1_idx;
  int32_T c1_i923;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_c;
  int32_T c1_b_c;
  int32_T c1_ridx;
  int32_T c1_lastRowIdx;
  int32_T c1_currRowIdx;
  const mxArray *c1_y = NULL;
  static char_T c1_cv76[40] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'p', 'a',
    'r', 's', 'f', 'c', 'n', ':', 's', 'p', 'L', 'o', 'g', 'i', 'c', 'a', 'l',
    'R', 'e', 'p', 'e', 'a', 't', 'e', 'd', 'I', 'n', 'd', 'i', 'c', 'e', 's' };

  boolean_T c1_val;
  const mxArray *c1_b_y = NULL;
  c1_idx = 0;
  c1_i923 = c1_this->colidx->size[0] - 2;
  c1_b = c1_i923 + 1;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_c = 0; c1_c <= c1_i923; c1_c++) {
    c1_b_c = c1_c;
    c1_ridx = c1_this->colidx->data[c1_b_c] - 1;
    c1_this->colidx->data[c1_b_c] = c1_idx + 1;
    c1_lastRowIdx = 0;
    while (c1_ridx + 1 < c1_this->colidx->data[c1_b_c + 1]) {
      c1_currRowIdx = c1_this->rowidx->data[c1_ridx];
      if (c1_currRowIdx != c1_lastRowIdx) {
      } else {
        c1_y = NULL;
        sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv76, 10, 0U, 1U, 0U, 2, 1,
          40), false);
        c1_b_y = NULL;
        sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv76, 10, 0U, 1U, 0U, 2, 1,
          40), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y,
                          14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
          "message", 1U, 1U, 14, c1_b_y)));
      }

      c1_val = c1_this->d->data[c1_ridx];
      c1_lastRowIdx = c1_currRowIdx;
      c1_ridx++;
      if (c1_val) {
        c1_this->d->data[c1_idx] = true;
        c1_this->rowidx->data[c1_idx] = c1_currRowIdx;
        c1_idx++;
      }
    }
  }

  c1_this->colidx->data[c1_this->colidx->size[0] - 1] = c1_idx + 1;
}

static void c1_d_nullAssignment(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx)
{
  int32_T c1_n;
  boolean_T c1_p;
  real_T c1_d40;
  int32_T c1_i924;
  int32_T c1_k;
  real_T c1_b_k;
  const mxArray *c1_y = NULL;
  c1_emxArray_boolean_T *c1_b;
  int32_T c1_b_x;
  const mxArray *c1_b_y = NULL;
  int32_T c1_nxin;
  int32_T c1_c_x;
  int32_T c1_nrowx;
  int32_T c1_b_n;
  int32_T c1_i925;
  int32_T c1_i926;
  int32_T c1_i927;
  int32_T c1_loop_ub;
  int32_T c1_i928;
  int32_T c1_i929;
  int32_T c1_b_b;
  int32_T c1_c_b;
  boolean_T c1_overflow;
  int32_T c1_c_k;
  int32_T c1_c_n;
  int32_T c1_d_k;
  int32_T c1_i930;
  int32_T c1_d_b;
  int32_T c1_e_b;
  boolean_T c1_b_overflow;
  int32_T c1_e_k;
  int32_T c1_nxout;
  int32_T c1_f_k;
  int32_T c1_nb;
  int32_T c1_i931;
  int32_T c1_k0;
  int32_T c1_i932;
  int32_T c1_f_b;
  int32_T c1_g_b;
  boolean_T c1_c_overflow;
  int32_T c1_g_k;
  int32_T c1_h_k;
  const mxArray *c1_c_y = NULL;
  boolean_T c1_b79;
  const mxArray *c1_d_y = NULL;
  int32_T c1_i933;
  int32_T c1_i934;
  boolean_T exitg1;
  c1_n = c1_x->size[0];
  c1_p = true;
  c1_d40 = (real_T)c1_idx->size[0];
  c1_i924 = (int32_T)c1_d40 - 1;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k <= c1_i924)) {
    c1_b_k = 1.0 + (real_T)c1_k;
    if ((c1_idx->data[(int32_T)c1_b_k - 1] < 1) || (c1_idx->data[(int32_T)c1_b_k
         - 1] > c1_n)) {
      c1_p = false;
      exitg1 = true;
    } else {
      c1_b_x = c1_idx->data[(int32_T)c1_b_k - 1];
      c1_c_x = c1_b_x;
      if (c1_idx->data[(int32_T)c1_b_k - 1] != c1_c_x) {
        c1_p = false;
        exitg1 = true;
      } else {
        c1_k++;
      }
    }
  }

  if (c1_p) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv13, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv13, 10, 0U, 1U, 0U, 2, 1, 25),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c1_b_y)));
  }

  c1_emxInit_boolean_T(chartInstance, &c1_b, 2, &c1_qi_emlrtRTEI);
  c1_nxin = c1_x->size[0];
  c1_nrowx = c1_x->size[0];
  c1_b_n = c1_nxin;
  c1_i925 = c1_b->size[0] * c1_b->size[1];
  c1_b->size[0] = 1;
  c1_b->size[1] = c1_b_n;
  c1_emxEnsureCapacity_boolean_T(chartInstance, c1_b, c1_i925, &c1_oi_emlrtRTEI);
  c1_i926 = c1_b->size[0];
  c1_i927 = c1_b->size[1];
  c1_loop_ub = c1_b_n - 1;
  for (c1_i928 = 0; c1_i928 <= c1_loop_ub; c1_i928++) {
    c1_b->data[c1_i928] = false;
  }

  c1_i929 = c1_idx->size[0];
  c1_b_b = c1_i929;
  c1_c_b = c1_b_b;
  if (1 > c1_c_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_c_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_c_k = 0; c1_c_k < c1_i929; c1_c_k++) {
    c1_d_k = c1_c_k;
    c1_b->data[c1_idx->data[c1_d_k] - 1] = true;
  }

  c1_c_n = 0;
  c1_i930 = c1_b->size[1];
  c1_d_b = c1_i930;
  c1_e_b = c1_d_b;
  if (1 > c1_e_b) {
    c1_b_overflow = false;
  } else {
    c1_b_overflow = (c1_e_b > 2147483646);
  }

  if (c1_b_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_e_k = 0; c1_e_k < c1_i930; c1_e_k++) {
    c1_f_k = c1_e_k;
    c1_i931 = c1_b->size[1];
    c1_i932 = c1_i931;
    c1_c_n += (int32_T)c1_b->data[c1_f_k];
  }

  c1_nxout = c1_nxin - c1_c_n;
  c1_nb = c1_b->size[1];
  c1_k0 = -1;
  c1_f_b = c1_nxin;
  c1_g_b = c1_f_b;
  if (1 > c1_g_b) {
    c1_c_overflow = false;
  } else {
    c1_c_overflow = (c1_g_b > 2147483646);
  }

  if (c1_c_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_g_k = 0; c1_g_k < c1_nxin; c1_g_k++) {
    c1_h_k = c1_g_k;
    if ((c1_h_k + 1 > c1_nb) || (!c1_b->data[c1_h_k])) {
      c1_k0++;
      c1_x->data[c1_k0] = c1_x->data[c1_h_k];
    }
  }

  c1_emxFree_boolean_T(chartInstance, &c1_b);
  if (c1_nxout <= c1_nrowx) {
  } else {
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_d_y)));
  }

  c1_b79 = (1 > c1_nxout);
  if (c1_b79) {
    c1_i933 = 0;
  } else {
    c1_i933 = c1_nxout;
  }

  c1_i934 = c1_x->size[0];
  c1_x->size[0] = c1_i933;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_x, c1_i934, &c1_pi_emlrtRTEI);
}

static void c1_b_imfill(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T *c1_varargin_1, c1_emxArray_real_T *c1_varargin_2)
{
  boolean_T c1_p;
  boolean_T c1_b_p;
  boolean_T c1_c_p;
  boolean_T c1_d_p;
  real_T c1_x1;
  boolean_T c1_e_p;
  const mxArray *c1_y = NULL;
  static char_T c1_cv77[36] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm', 'f',
    'i', 'l', 'l', ':', 'n', 'o', 'I', 'n', 't', 'e', 'r', 'a', 'c', 't', 'i',
    'v', 'e', 'I', 'n', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  int32_T c1_i935;
  const mxArray *c1_b_y = NULL;
  int32_T c1_i936;
  real_T c1_imSizeT[2];
  boolean_T c1_f_p;
  real_T c1_d41;
  int32_T c1_i937;
  int32_T c1_k;
  real_T c1_b_k;
  real_T c1_x;
  boolean_T c1_b80;
  boolean_T c1_g_p;
  const mxArray *c1_c_y = NULL;
  static char_T c1_cv78[30] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'i', 'm', 'f',
    'i', 'l', 'l', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'P', 'o', 's',
    'i', 't', 'i', 'v', 'e' };

  boolean_T c1_h_p;
  const mxArray *c1_d_y = NULL;
  real_T c1_d42;
  int32_T c1_i938;
  const mxArray *c1_e_y = NULL;
  int32_T c1_c_k;
  static char_T c1_cv79[26] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'L', 'O', 'C', 'A', 'T', 'I', 'O', 'N', 'S',
    ',' };

  real_T c1_d_k;
  real_T c1_b_x;
  boolean_T c1_b81;
  real_T c1_c_x;
  real_T c1_d_x;
  boolean_T c1_b;
  const mxArray *c1_f_y = NULL;
  boolean_T c1_b82;
  static char_T c1_cv80[29] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'i', 'm', 'f',
    'i', 'l', 'l', ':', 'e', 'x', 'p', 'e', 'c', 't', 'e', 'd', 'I', 'n', 't',
    'e', 'g', 'e', 'r' };

  c1_emxArray_real_T *c1_locationsVar;
  real_T c1_e_x;
  const mxArray *c1_g_y = NULL;
  int32_T c1_i939;
  boolean_T c1_b_b;
  boolean_T c1_b83;
  const mxArray *c1_h_y = NULL;
  boolean_T c1_c_b;
  static char_T c1_cv81[26] = { 'i', 'n', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b',
    'e', 'r', ' ', '2', ',', ' ', 'L', 'O', 'C', 'A', 'T', 'I', 'O', 'N', 'S',
    ',' };

  c1_emxArray_real_T *c1_b_varargin_2;
  c1_emxArray_boolean_T *c1_badPixels;
  int32_T c1_i940;
  real_T c1_f_x;
  int32_T c1_i941;
  boolean_T c1_i_p;
  real_T c1_g_x;
  int32_T c1_loop_ub;
  boolean_T c1_j_p;
  int32_T c1_b_loop_ub;
  int32_T c1_i942;
  int32_T c1_i943;
  int32_T c1_c_loop_ub;
  c1_emxArray_boolean_T *c1_r23;
  int32_T c1_i944;
  real_T c1_i_y;
  int32_T c1_i945;
  int32_T c1_i946;
  int32_T c1_d_loop_ub;
  int32_T c1_e_loop_ub;
  int32_T c1_i947;
  int32_T c1_i948;
  int32_T c1_i949;
  int32_T c1_f_loop_ub;
  int32_T c1_i950;
  boolean_T c1_b84;
  const mxArray *c1_j_y = NULL;
  static char_T c1_cv82[51] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'a', 'l', 'l', '_', 'o', 'r', '_',
    'a', 'n', 'y', '_', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  boolean_T c1_k_y;
  const mxArray *c1_l_y = NULL;
  real_T c1_vlen;
  real_T c1_a;
  int32_T c1_c;
  int32_T c1_b_a;
  int32_T c1_vspread;
  int32_T c1_d_b;
  int32_T c1_i2;
  int32_T c1_c_a;
  int32_T c1_e_b;
  int32_T c1_f_b;
  boolean_T c1_overflow;
  int32_T c1_ix;
  boolean_T c1_b85;
  real_T c1_numelBadPix;
  boolean_T c1_b86;
  int32_T c1_i951;
  boolean_T c1_b87;
  int32_T c1_s;
  c1_emxArray_boolean_T *c1_mask;
  int32_T c1_i952;
  real_T c1_b_s;
  int32_T c1_idx;
  int32_T c1_i953;
  int32_T c1_b_idx;
  int32_T c1_i954;
  int32_T c1_c_idx;
  int32_T c1_g_loop_ub;
  real_T c1_d43;
  int32_T c1_i955;
  int32_T c1_n;
  boolean_T c1_k_p;
  int32_T c1_i956;
  int32_T c1_h_x;
  int32_T c1_i957;
  int32_T c1_i_x;
  const mxArray *c1_m_y = NULL;
  int32_T c1_i958;
  int32_T c1_d_idx;
  int32_T c1_i959;
  const mxArray *c1_n_y = NULL;
  int32_T c1_nrowx;
  int32_T c1_i960;
  int32_T c1_nrows;
  int32_T c1_h_loop_ub;
  int32_T c1_d_a;
  int32_T c1_i961;
  int32_T c1_g_b;
  int32_T c1_e_a;
  int32_T c1_h_b;
  c1_emxArray_boolean_T *c1_marker;
  int32_T c1_i962;
  boolean_T c1_b_overflow;
  int32_T c1_i963;
  int32_T c1_i;
  int32_T c1_i964;
  int32_T c1_i_loop_ub;
  int32_T c1_i965;
  int32_T c1_b_locationsVar[2];
  const mxArray *c1_o_y = NULL;
  boolean_T c1_b88;
  c1_emxArray_int32_T *c1_r24;
  const mxArray *c1_p_y = NULL;
  int32_T c1_b_marker;
  int32_T c1_i966;
  int32_T c1_i967;
  int32_T c1_i968;
  int32_T c1_j_loop_ub;
  int32_T c1_i969;
  int32_T c1_b_mask;
  int32_T c1_i970;
  int32_T c1_k_loop_ub;
  int32_T c1_i971;
  int32_T c1_i972;
  int32_T c1_i973;
  int32_T c1_l_loop_ub;
  int32_T c1_i974;
  int32_T c1_i975;
  int32_T c1_i976;
  int32_T c1_i977;
  real_T c1_c_mask[2];
  int32_T c1_i978;
  int32_T c1_i979;
  boolean_T c1_l_p;
  int32_T c1_i980;
  boolean_T c1_m_p;
  int32_T c1_e_k;
  real_T c1_f_k;
  real_T c1_b_x1;
  real_T c1_x2;
  boolean_T c1_n_p;
  const mxArray *c1_q_y = NULL;
  static char_T c1_cv83[32] = { 'i', 'm', 'a', 'g', 'e', 's', ':', 'i', 'm', 'r',
    'e', 'c', 'o', 'n', 's', 't', 'r', 'u', 'c', 't', ':', 'n', 'o', 't', 'S',
    'a', 'm', 'e', 'S', 'i', 'z', 'e' };

  int32_T c1_i981;
  const mxArray *c1_r_y = NULL;
  int32_T c1_i982;
  int32_T c1_i983;
  int32_T c1_i984;
  int32_T c1_b_varargin_1[2];
  int32_T c1_c_marker[2];
  int32_T c1_i985;
  int32_T c1_i986;
  int32_T c1_i987;
  int32_T c1_i988;
  int32_T c1_i989;
  int32_T c1_m_loop_ub;
  int32_T c1_i990;
  int32_T c1_n_loop_ub;
  int32_T c1_i991;
  boolean_T exitg1;
  c1_p = false;
  c1_b_p = false;
  if ((real_T)c1_varargin_2->size[0] != 1.0) {
  } else {
    c1_b_p = true;
  }

  if (c1_b_p) {
    c1_c_p = true;
  } else {
    c1_c_p = false;
  }

  c1_d_p = c1_c_p;
  if (c1_c_p && (c1_varargin_2->size[0] != 0)) {
    c1_x1 = c1_varargin_2->data[0];
    c1_e_p = (c1_x1 == 0.0);
    if (!c1_e_p) {
      c1_d_p = false;
    }
  }

  if (!c1_d_p) {
  } else {
    c1_p = true;
  }

  if (!c1_p) {
  } else {
    c1_y = NULL;
    sf_mex_assign(&c1_y, sf_mex_create("y", c1_cv77, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv77, 10, 0U, 1U, 0U, 2, 1, 36),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_y, 14,
                      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "getString",
      1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
      1U, 14, c1_b_y)));
  }

  for (c1_i935 = 0; c1_i935 < 2; c1_i935++) {
    c1_imSizeT[c1_i935] = (real_T)c1_varargin_1->size[c1_i935];
  }

  for (c1_i936 = 0; c1_i936 < 2; c1_i936++) {
    c1_imSizeT[c1_i936];
  }

  c1_f_p = true;
  c1_d41 = (real_T)c1_varargin_2->size[0];
  c1_i937 = (int32_T)c1_d41 - 1;
  c1_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_k <= c1_i937)) {
    c1_b_k = 1.0 + (real_T)c1_k;
    c1_x = c1_varargin_2->data[(int32_T)c1_b_k - 1];
    c1_g_p = !(c1_x <= 0.0);
    if (c1_g_p) {
      c1_k++;
    } else {
      c1_f_p = false;
      exitg1 = true;
    }
  }

  if (c1_f_p) {
    c1_b80 = true;
  } else {
    c1_b80 = false;
  }

  if (c1_b80) {
  } else {
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv78, 10, 0U, 1U, 0U, 2, 1, 30),
                  false);
    c1_d_y = NULL;
    sf_mex_assign(&c1_d_y, sf_mex_create("y", c1_cv12, 10, 0U, 1U, 0U, 2, 1, 48),
                  false);
    c1_e_y = NULL;
    sf_mex_assign(&c1_e_y, sf_mex_create("y", c1_cv79, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_c_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_d_y, 14, c1_e_y)));
  }

  c1_h_p = true;
  c1_d42 = (real_T)c1_varargin_2->size[0];
  c1_i938 = (int32_T)c1_d42 - 1;
  c1_c_k = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_c_k <= c1_i938)) {
    c1_d_k = 1.0 + (real_T)c1_c_k;
    c1_b_x = c1_varargin_2->data[(int32_T)c1_d_k - 1];
    c1_c_x = c1_b_x;
    c1_d_x = c1_c_x;
    c1_b = muDoubleScalarIsInf(c1_d_x);
    c1_b82 = !c1_b;
    c1_e_x = c1_c_x;
    c1_b_b = muDoubleScalarIsNaN(c1_e_x);
    c1_b83 = !c1_b_b;
    c1_c_b = (c1_b82 && c1_b83);
    if (c1_c_b) {
      c1_f_x = c1_b_x;
      c1_g_x = c1_f_x;
      c1_g_x = muDoubleScalarFloor(c1_g_x);
      if (c1_g_x == c1_b_x) {
        c1_i_p = true;
      } else {
        c1_i_p = false;
      }
    } else {
      c1_i_p = false;
    }

    c1_j_p = c1_i_p;
    if (c1_j_p) {
      c1_c_k++;
    } else {
      c1_h_p = false;
      exitg1 = true;
    }
  }

  if (c1_h_p) {
    c1_b81 = true;
  } else {
    c1_b81 = false;
  }

  if (c1_b81) {
  } else {
    c1_f_y = NULL;
    sf_mex_assign(&c1_f_y, sf_mex_create("y", c1_cv80, 10, 0U, 1U, 0U, 2, 1, 29),
                  false);
    c1_g_y = NULL;
    sf_mex_assign(&c1_g_y, sf_mex_create("y", c1_cv5, 10, 0U, 1U, 0U, 2, 1, 47),
                  false);
    c1_h_y = NULL;
    sf_mex_assign(&c1_h_y, sf_mex_create("y", c1_cv81, 10, 0U, 1U, 0U, 2, 1, 26),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_f_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 2U, 14, c1_g_y, 14, c1_h_y)));
  }

  c1_emxInit_real_T1(chartInstance, &c1_locationsVar, 1, &c1_cj_emlrtRTEI);
  c1_i939 = c1_locationsVar->size[0];
  c1_locationsVar->size[0] = 0;
  if (c1_varargin_2->size[0] != 0) {
    c1_emxInit_real_T1(chartInstance, &c1_b_varargin_2, 1, &c1_si_emlrtRTEI);
    c1_i940 = c1_b_varargin_2->size[0];
    c1_b_varargin_2->size[0] = c1_varargin_2->size[0] + c1_locationsVar->size[0];
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_b_varargin_2, c1_i940,
      &c1_si_emlrtRTEI);
    c1_loop_ub = c1_varargin_2->size[0] - 1;
    for (c1_i942 = 0; c1_i942 <= c1_loop_ub; c1_i942++) {
      c1_b_varargin_2->data[c1_i942] = c1_varargin_2->data[c1_i942];
    }

    c1_c_loop_ub = c1_locationsVar->size[0] - 1;
    for (c1_i944 = 0; c1_i944 <= c1_c_loop_ub; c1_i944++) {
      c1_b_varargin_2->data[c1_i944 + c1_varargin_2->size[0]] =
        c1_locationsVar->data[c1_i944];
    }

    c1_i946 = c1_locationsVar->size[0];
    c1_locationsVar->size[0] = c1_b_varargin_2->size[0];
    c1_emxEnsureCapacity_real_T1(chartInstance, c1_locationsVar, c1_i946,
      &c1_ui_emlrtRTEI);
    c1_e_loop_ub = c1_b_varargin_2->size[0] - 1;
    for (c1_i948 = 0; c1_i948 <= c1_e_loop_ub; c1_i948++) {
      c1_locationsVar->data[c1_i948] = c1_b_varargin_2->data[c1_i948];
    }

    c1_emxFree_real_T(chartInstance, &c1_b_varargin_2);
  }

  c1_emxInit_boolean_T1(chartInstance, &c1_badPixels, 1, &c1_vi_emlrtRTEI);
  c1_i941 = c1_badPixels->size[0];
  c1_badPixels->size[0] = c1_varargin_2->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_badPixels, c1_i941,
    &c1_ri_emlrtRTEI);
  c1_b_loop_ub = c1_varargin_2->size[0] - 1;
  for (c1_i943 = 0; c1_i943 <= c1_b_loop_ub; c1_i943++) {
    c1_badPixels->data[c1_i943] = (c1_varargin_2->data[c1_i943] < 1.0);
  }

  c1_emxInit_boolean_T1(chartInstance, &c1_r23, 1, &c1_dg_emlrtRTEI);
  c1_i_y = c1_imSizeT[0];
  c1_i_y *= c1_imSizeT[1];
  c1_i945 = c1_r23->size[0];
  c1_r23->size[0] = c1_varargin_2->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_r23, c1_i945,
    &c1_ti_emlrtRTEI);
  c1_d_loop_ub = c1_varargin_2->size[0] - 1;
  for (c1_i947 = 0; c1_i947 <= c1_d_loop_ub; c1_i947++) {
    c1_r23->data[c1_i947] = (c1_varargin_2->data[c1_i947] > c1_i_y);
  }

  _SFD_SIZE_EQ_CHECK_1D(c1_badPixels->size[0], c1_r23->size[0]);
  c1_i949 = c1_badPixels->size[0];
  c1_badPixels->size[0];
  c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_badPixels, c1_i949,
    &c1_vi_emlrtRTEI);
  c1_f_loop_ub = c1_badPixels->size[0] - 1;
  for (c1_i950 = 0; c1_i950 <= c1_f_loop_ub; c1_i950++) {
    c1_badPixels->data[c1_i950] = (c1_badPixels->data[c1_i950] || c1_r23->
      data[c1_i950]);
  }

  if ((c1_badPixels->size[0] == 1) || ((real_T)c1_badPixels->size[0] != 1.0)) {
    c1_b84 = true;
  } else {
    c1_b84 = false;
  }

  if (c1_b84) {
  } else {
    c1_j_y = NULL;
    sf_mex_assign(&c1_j_y, sf_mex_create("y", c1_cv82, 10, 0U, 1U, 0U, 2, 1, 51),
                  false);
    c1_l_y = NULL;
    sf_mex_assign(&c1_l_y, sf_mex_create("y", c1_cv82, 10, 0U, 1U, 0U, 2, 1, 51),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_j_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_l_y)));
  }

  c1_k_y = false;
  c1_vlen = (real_T)c1_badPixels->size[0];
  c1_a = c1_vlen;
  c1_c = (int32_T)c1_a;
  c1_b_a = c1_c - 1;
  c1_vspread = c1_b_a;
  c1_d_b = c1_vspread;
  c1_i2 = c1_d_b;
  c1_c_a = c1_i2 + 1;
  c1_i2 = c1_c_a;
  c1_e_b = c1_i2;
  c1_f_b = c1_e_b;
  if (1 > c1_f_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_f_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  c1_ix = 0;
  exitg1 = false;
  while ((!exitg1) && (c1_ix + 1 <= c1_i2)) {
    if (!c1_badPixels->data[c1_ix]) {
      c1_b85 = true;
    } else {
      c1_b85 = false;
    }

    if (!c1_b85) {
      c1_k_y = true;
      exitg1 = true;
    } else {
      c1_ix++;
    }
  }

  if (c1_k_y) {
    c1_numelBadPix = (real_T)c1_badPixels->size[0];
    c1_d_warning(chartInstance);
    c1_i951 = (int32_T)((1.0 + (-1.0 - c1_numelBadPix)) / -1.0);
    _SFD_FOR_LOOP_VECTOR_CHECK(c1_numelBadPix, -1.0, 1.0, mxDOUBLE_CLASS,
      c1_i951);
    for (c1_s = 0; c1_s < c1_i951; c1_s++) {
      c1_b_s = c1_numelBadPix + -(real_T)c1_s;
      if (c1_badPixels->data[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
           (int32_T)c1_b_s, 1, c1_badPixels->size[0]) - 1]) {
        c1_idx = (int32_T)c1_b_s;
        c1_b_idx = c1_idx;
        c1_c_idx = c1_b_idx;
        c1_d43 = (real_T)c1_locationsVar->size[0];
        c1_n = (int32_T)c1_d43;
        c1_k_p = true;
        if (c1_c_idx > c1_n) {
          c1_k_p = false;
        } else {
          c1_h_x = c1_c_idx;
          c1_i_x = c1_h_x;
          if (c1_c_idx != c1_i_x) {
            c1_k_p = false;
          }
        }

        if (c1_k_p) {
        } else {
          c1_m_y = NULL;
          sf_mex_assign(&c1_m_y, sf_mex_create("y", c1_cv13, 10, 0U, 1U, 0U, 2,
            1, 25), false);
          c1_n_y = NULL;
          sf_mex_assign(&c1_n_y, sf_mex_create("y", c1_cv13, 10, 0U, 1U, 0U, 2,
            1, 25), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c1_m_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c1_n_y)));
        }

        c1_d_idx = c1_idx;
        c1_nrowx = c1_locationsVar->size[0];
        c1_nrows = c1_nrowx - 1;
        c1_d_a = c1_d_idx;
        c1_g_b = c1_nrows;
        c1_e_a = c1_d_a;
        c1_h_b = c1_g_b;
        if (c1_e_a > c1_h_b) {
          c1_b_overflow = false;
        } else {
          c1_b_overflow = (c1_h_b > 2147483646);
        }

        if (c1_b_overflow) {
          c1_check_forloop_overflow_error(chartInstance, true);
        }

        for (c1_i = c1_d_idx; c1_i <= c1_nrows; c1_i++) {
          c1_b_locationsVar[0] = c1_locationsVar->size[0];
          c1_b_locationsVar[1] = 1;
          c1_locationsVar->data[c1_i - 1] = c1_locationsVar->data[c1_i];
        }

        if (c1_nrows <= c1_nrowx) {
        } else {
          c1_o_y = NULL;
          sf_mex_assign(&c1_o_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          c1_p_y = NULL;
          sf_mex_assign(&c1_p_y, sf_mex_create("y", c1_cv1, 10, 0U, 1U, 0U, 2, 1,
            30), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14,
                            c1_o_y, 14, sf_mex_call_debug
                            (sfGlobalDebugInstanceStruct, "getString", 1U, 1U,
                             14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
            "message", 1U, 1U, 14, c1_p_y)));
        }

        c1_b88 = (1 > c1_nrows);
        if (c1_b88) {
          c1_i966 = 0;
        } else {
          c1_i966 = c1_nrows;
        }

        c1_i968 = c1_locationsVar->size[0];
        c1_locationsVar->size[0] = c1_i966;
        c1_emxEnsureCapacity_real_T1(chartInstance, c1_locationsVar, c1_i968,
          &c1_ui_emlrtRTEI);
      }
    }
  }

  c1_emxFree_boolean_T(chartInstance, &c1_badPixels);
  c1_b86 = (c1_varargin_1->size[0] == 0);
  c1_b87 = (c1_varargin_1->size[1] == 0);
  if (c1_b86 || c1_b87) {
  } else {
    c1_emxInit_boolean_T(chartInstance, &c1_mask, 2, &c1_dj_emlrtRTEI);
    c1_i952 = c1_mask->size[0] * c1_mask->size[1];
    c1_mask->size[0] = c1_varargin_1->size[0];
    c1_mask->size[1] = c1_varargin_1->size[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_mask, c1_i952,
      &c1_wi_emlrtRTEI);
    c1_i953 = c1_mask->size[0];
    c1_i954 = c1_mask->size[1];
    c1_g_loop_ub = c1_varargin_1->size[0] * c1_varargin_1->size[1] - 1;
    for (c1_i955 = 0; c1_i955 <= c1_g_loop_ub; c1_i955++) {
      c1_mask->data[c1_i955] = c1_varargin_1->data[c1_i955];
    }

    c1_i956 = c1_mask->size[0] * c1_mask->size[1];
    c1_i957 = c1_mask->size[0] * c1_mask->size[1];
    c1_mask->size[0];
    c1_mask->size[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_mask, c1_i957,
      &c1_wi_emlrtRTEI);
    c1_i958 = c1_mask->size[0];
    c1_i959 = c1_mask->size[1];
    c1_i960 = c1_i956;
    c1_h_loop_ub = c1_i960 - 1;
    for (c1_i961 = 0; c1_i961 <= c1_h_loop_ub; c1_i961++) {
      c1_mask->data[c1_i961] = !c1_mask->data[c1_i961];
    }

    c1_emxInit_boolean_T(chartInstance, &c1_marker, 2, &c1_ej_emlrtRTEI);
    c1_i962 = c1_marker->size[0] * c1_marker->size[1];
    c1_marker->size[0] = c1_mask->size[0];
    c1_marker->size[1] = c1_mask->size[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_marker, c1_i962,
      &c1_xi_emlrtRTEI);
    c1_i963 = c1_marker->size[0];
    c1_i964 = c1_marker->size[1];
    c1_i_loop_ub = c1_mask->size[0] * c1_mask->size[1] - 1;
    for (c1_i965 = 0; c1_i965 <= c1_i_loop_ub; c1_i965++) {
      c1_marker->data[c1_i965] = false;
    }

    c1_emxInit_int32_T(chartInstance, &c1_r24, 1, &c1_dg_emlrtRTEI);
    c1_b_marker = c1_marker->size[0] * c1_marker->size[1];
    c1_i967 = c1_r24->size[0];
    c1_r24->size[0] = c1_locationsVar->size[0];
    c1_emxEnsureCapacity_int32_T(chartInstance, c1_r24, c1_i967,
      &c1_yi_emlrtRTEI);
    c1_j_loop_ub = c1_locationsVar->size[0] - 1;
    for (c1_i969 = 0; c1_i969 <= c1_j_loop_ub; c1_i969++) {
      c1_r24->data[c1_i969] = sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c1_locationsVar->data[c1_i969], 1, c1_b_marker);
    }

    c1_b_mask = c1_mask->size[0] * c1_mask->size[1];
    c1_i970 = c1_r23->size[0];
    c1_r23->size[0] = c1_locationsVar->size[0];
    c1_emxEnsureCapacity_boolean_T1(chartInstance, c1_r23, c1_i970,
      &c1_aj_emlrtRTEI);
    c1_k_loop_ub = c1_locationsVar->size[0] - 1;
    for (c1_i971 = 0; c1_i971 <= c1_k_loop_ub; c1_i971++) {
      c1_r23->data[c1_i971] = c1_mask->data[sf_eml_array_bounds_check
        (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 0, 0, MAX_uint32_T,
         (int32_T)c1_locationsVar->data[c1_i971], 1, c1_b_mask) - 1];
    }

    _SFD_SUB_ASSIGN_SIZE_CHECK_1D(c1_r24->size[0], c1_r23->size[0]);
    c1_i972 = c1_marker->size[0];
    c1_i973 = c1_marker->size[1];
    c1_l_loop_ub = c1_r23->size[0] - 1;
    for (c1_i974 = 0; c1_i974 <= c1_l_loop_ub; c1_i974++) {
      c1_marker->data[c1_r24->data[c1_i974] - 1] = c1_r23->data[c1_i974];
    }

    c1_emxFree_int32_T(chartInstance, &c1_r24);
    for (c1_i975 = 0; c1_i975 < 2; c1_i975++) {
      c1_imSizeT[c1_i975] = (real_T)c1_marker->size[c1_i975];
    }

    for (c1_i976 = 0; c1_i976 < 2; c1_i976++) {
      c1_c_mask[c1_i976] = (real_T)c1_mask->size[c1_i976];
    }

    for (c1_i977 = 0; c1_i977 < 2; c1_i977++) {
      c1_imSizeT[c1_i977];
    }

    for (c1_i978 = 0; c1_i978 < 2; c1_i978++) {
      c1_c_mask[c1_i978];
    }

    for (c1_i979 = 0; c1_i979 < 2; c1_i979++) {
      c1_c_mask[c1_i979];
    }

    c1_l_p = false;
    for (c1_i980 = 0; c1_i980 < 2; c1_i980++) {
      c1_c_mask[c1_i980];
    }

    c1_m_p = true;
    c1_e_k = 0;
    exitg1 = false;
    while ((!exitg1) && (c1_e_k < 2)) {
      c1_f_k = 1.0 + (real_T)c1_e_k;
      c1_b_x1 = c1_imSizeT[(int32_T)c1_f_k - 1];
      c1_x2 = c1_c_mask[(int32_T)c1_f_k - 1];
      c1_n_p = (c1_b_x1 == c1_x2);
      if (!c1_n_p) {
        c1_m_p = false;
        exitg1 = true;
      } else {
        c1_e_k++;
      }
    }

    if (!c1_m_p) {
    } else {
      c1_l_p = true;
    }

    if (c1_l_p) {
    } else {
      c1_q_y = NULL;
      sf_mex_assign(&c1_q_y, sf_mex_create("y", c1_cv83, 10, 0U, 1U, 0U, 2, 1,
        32), false);
      c1_r_y = NULL;
      sf_mex_assign(&c1_r_y, sf_mex_create("y", c1_cv83, 10, 0U, 1U, 0U, 2, 1,
        32), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_q_y,
                        14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
        "message", 1U, 1U, 14, c1_r_y)));
    }

    for (c1_i981 = 0; c1_i981 < 2; c1_i981++) {
      c1_imSizeT[c1_i981] = (real_T)c1_marker->size[c1_i981];
    }

    for (c1_i982 = 0; c1_i982 < 2; c1_i982++) {
      c1_imSizeT[c1_i982];
    }

    ippreconstruct_uint8(&c1_marker->data[0], &c1_mask->data[0], c1_imSizeT, 2.0);
    c1_emxFree_boolean_T(chartInstance, &c1_mask);
    for (c1_i983 = 0; c1_i983 < 2; c1_i983++) {
      c1_b_varargin_1[c1_i983] = c1_varargin_1->size[c1_i983];
    }

    for (c1_i984 = 0; c1_i984 < 2; c1_i984++) {
      c1_c_marker[c1_i984] = c1_marker->size[c1_i984];
    }

    _SFD_SIZE_EQ_CHECK_ND(c1_b_varargin_1, c1_c_marker, 2);
    c1_i985 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
    c1_i986 = c1_marker->size[0] * c1_marker->size[1];
    c1_i987 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
    c1_i988 = c1_marker->size[0] * c1_marker->size[1];
    c1_i989 = c1_varargin_1->size[0] * c1_varargin_1->size[1];
    c1_varargin_1->size[0];
    c1_varargin_1->size[1];
    c1_emxEnsureCapacity_boolean_T(chartInstance, c1_varargin_1, c1_i989,
      &c1_bj_emlrtRTEI);
    c1_m_loop_ub = c1_varargin_1->size[1] - 1;
    for (c1_i990 = 0; c1_i990 <= c1_m_loop_ub; c1_i990++) {
      c1_n_loop_ub = c1_varargin_1->size[0] - 1;
      for (c1_i991 = 0; c1_i991 <= c1_n_loop_ub; c1_i991++) {
        c1_varargin_1->data[c1_i991 + c1_varargin_1->size[0] * c1_i990] =
          (c1_varargin_1->data[c1_i991 + c1_varargin_1->size[0] * c1_i990] ||
           c1_marker->data[c1_i991 + c1_marker->size[0] * c1_i990]);
      }
    }

    c1_emxFree_boolean_T(chartInstance, &c1_marker);
  }

  c1_emxFree_boolean_T(chartInstance, &c1_r23);
  c1_emxFree_real_T(chartInstance, &c1_locationsVar);
}

static void c1_b_sortrows(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_y, real_T c1_varargin_1[2])
{
  int32_T c1_k;
  boolean_T c1_p;
  real_T c1_b_k;
  real_T c1_x;
  real_T c1_b_x;
  const mxArray *c1_b_y = NULL;
  real_T c1_c_x;
  static char_T c1_cv84[32] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'i', 's', 's',
    'o', 'r', 't', 'e', 'd', 'r', 'o', 'w', 's', ':', 'C', 'o', 'l', 'M', 'i',
    's', 'm', 'a', 't', 'c', 'h', 'X' };

  int32_T c1_i992;
  real_T c1_ck;
  const mxArray *c1_c_y = NULL;
  real_T c1_d_x;
  real_T c1_e_x;
  c1_emxArray_int32_T *c1_idx;
  int32_T c1_col[2];
  int32_T c1_n;
  int32_T c1_i993;
  int32_T c1_loop_ub;
  int32_T c1_i994;
  c1_emxArray_int32_T *c1_iwork;
  int32_T c1_b_n;
  int32_T c1_len;
  int32_T c1_i995;
  int32_T c1_iv11[1];
  int32_T c1_i996;
  int32_T c1_np1;
  int32_T c1_i997;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_c_k;
  c1_emxArray_int32_T *c1_d_y;
  int32_T c1_i998;
  int32_T c1_i;
  c1_emxArray_int32_T *c1_e_y;
  int32_T c1_i999;
  int32_T c1_i1000;
  int32_T c1_b_loop_ub;
  int32_T c1_a;
  int32_T c1_i1001;
  int32_T c1_m;
  int32_T c1_i2;
  int32_T c1_j;
  int32_T c1_pEnd;
  int32_T c1_i1002;
  int32_T c1_i1003;
  int32_T c1_b_p;
  int32_T c1_b_col[2];
  int32_T c1_b_j;
  int32_T c1_q;
  int32_T c1_qEnd;
  int32_T c1_c_j;
  int32_T c1_c_b;
  int32_T c1_d_k;
  int32_T c1_d_b;
  int32_T c1_kEnd;
  boolean_T c1_b_overflow;
  int32_T c1_i1004;
  int32_T c1_e_b;
  int32_T c1_f_b;
  int32_T c1_b_i;
  boolean_T c1_c_overflow;
  int32_T c1_i1005;
  int32_T c1_i1006;
  int32_T c1_g_b;
  int32_T c1_c_i;
  int32_T c1_c_loop_ub;
  int32_T c1_h_b;
  int32_T c1_i1007;
  int32_T c1_e_k;
  boolean_T c1_d_overflow;
  int32_T c1_i1008;
  int32_T c1_d_i;
  int32_T c1_c_col[2];
  int32_T exitg1;
  c1_k = 0;
  do {
    exitg1 = 0;
    if (c1_k < 2) {
      c1_b_k = 1.0 + (real_T)c1_k;
      c1_x = c1_varargin_1[(int32_T)c1_b_k - 1];
      c1_b_x = c1_x;
      c1_c_x = c1_b_x;
      c1_ck = muDoubleScalarAbs(c1_c_x);
      c1_d_x = c1_ck;
      c1_e_x = c1_d_x;
      c1_e_x = muDoubleScalarFloor(c1_e_x);
      if ((c1_e_x != c1_ck) || (c1_ck < 1.0) || (c1_ck > 2.0)) {
        c1_p = false;
        exitg1 = 1;
      } else {
        c1_k++;
      }
    } else {
      c1_p = true;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  if (c1_p) {
  } else {
    c1_b_y = NULL;
    sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_cv84, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    c1_c_y = NULL;
    sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_cv84, 10, 0U, 1U, 0U, 2, 1, 32),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 2U, 14, c1_b_y,
                      14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "getString", 1U, 1U, 14, sf_mex_call_debug(sfGlobalDebugInstanceStruct,
      "message", 1U, 1U, 14, c1_c_y)));
  }

  for (c1_i992 = 0; c1_i992 < 2; c1_i992++) {
    c1_col[c1_i992] = (int32_T)c1_varargin_1[c1_i992];
  }

  c1_emxInit_int32_T(chartInstance, &c1_idx, 1, &c1_o_emlrtRTEI);
  c1_n = c1_y->size[0];
  c1_i993 = c1_idx->size[0];
  c1_idx->size[0] = c1_y->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_idx, c1_i993, &c1_o_emlrtRTEI);
  c1_loop_ub = c1_y->size[0] - 1;
  for (c1_i994 = 0; c1_i994 <= c1_loop_ub; c1_i994++) {
    c1_idx->data[c1_i994] = 0;
  }

  c1_emxInit_int32_T(chartInstance, &c1_iwork, 1, &c1_tb_emlrtRTEI);
  c1_b_n = c1_n;
  c1_len = c1_idx->size[0];
  c1_i995 = c1_iwork->size[0];
  c1_iwork->size[0] = c1_len;
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i995, &c1_q_emlrtRTEI);
  c1_iv11[0] = c1_iwork->size[0];
  c1_i996 = c1_iwork->size[0];
  c1_iwork->size[0] = c1_iv11[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i996, &c1_s_emlrtRTEI);
  c1_np1 = c1_b_n + 1;
  c1_i997 = c1_b_n - 1;
  c1_b = c1_i997;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483645);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  c1_c_k = 1;
  c1_emxInit_int32_T1(chartInstance, &c1_d_y, 2, &c1_u_emlrtRTEI);
  while (c1_c_k <= c1_i997) {
    c1_i998 = c1_d_y->size[0] * c1_d_y->size[1];
    c1_d_y->size[0] = c1_y->size[0];
    c1_d_y->size[1] = 2;
    c1_emxEnsureCapacity_int32_T1(chartInstance, c1_d_y, c1_i998,
      &c1_u_emlrtRTEI);
    c1_i999 = c1_d_y->size[0];
    c1_i1000 = c1_d_y->size[1];
    c1_b_loop_ub = c1_y->size[0] * c1_y->size[1] - 1;
    for (c1_i1001 = 0; c1_i1001 <= c1_b_loop_ub; c1_i1001++) {
      c1_d_y->data[c1_i1001] = c1_y->data[c1_i1001];
    }

    for (c1_i1002 = 0; c1_i1002 < 2; c1_i1002++) {
      c1_b_col[c1_i1002] = c1_col[c1_i1002];
    }

    if (c1_b_sortLE(chartInstance, c1_d_y, c1_b_col, c1_c_k, c1_c_k + 1)) {
      c1_idx->data[c1_c_k - 1] = c1_c_k;
      c1_idx->data[c1_c_k] = c1_c_k + 1;
    } else {
      c1_idx->data[c1_c_k - 1] = c1_c_k + 1;
      c1_idx->data[c1_c_k] = c1_c_k;
    }

    c1_c_k += 2;
  }

  c1_emxFree_int32_T(chartInstance, &c1_d_y);
  if ((c1_b_n & 1) != 0) {
    c1_idx->data[c1_b_n - 1] = c1_b_n;
  }

  c1_i = 2;
  c1_emxInit_int32_T1(chartInstance, &c1_e_y, 2, &c1_u_emlrtRTEI);
  while (c1_i < c1_b_n) {
    c1_a = c1_i;
    c1_i2 = c1_a << 1;
    c1_j = 1;
    for (c1_pEnd = 1 + c1_i; c1_pEnd < c1_np1; c1_pEnd = c1_qEnd + c1_i) {
      c1_b_p = c1_j - 1;
      c1_q = c1_pEnd - 1;
      c1_qEnd = c1_j + c1_i2;
      if (c1_qEnd > c1_np1) {
        c1_qEnd = c1_np1;
      }

      c1_d_k = 0;
      c1_kEnd = c1_qEnd - c1_j;
      while (c1_d_k + 1 <= c1_kEnd) {
        c1_i1004 = c1_e_y->size[0] * c1_e_y->size[1];
        c1_e_y->size[0] = c1_y->size[0];
        c1_e_y->size[1] = 2;
        c1_emxEnsureCapacity_int32_T1(chartInstance, c1_e_y, c1_i1004,
          &c1_u_emlrtRTEI);
        c1_i1005 = c1_e_y->size[0];
        c1_i1006 = c1_e_y->size[1];
        c1_c_loop_ub = c1_y->size[0] * c1_y->size[1] - 1;
        for (c1_i1007 = 0; c1_i1007 <= c1_c_loop_ub; c1_i1007++) {
          c1_e_y->data[c1_i1007] = c1_y->data[c1_i1007];
        }

        for (c1_i1008 = 0; c1_i1008 < 2; c1_i1008++) {
          c1_c_col[c1_i1008] = c1_col[c1_i1008];
        }

        if (c1_b_sortLE(chartInstance, c1_e_y, c1_c_col, c1_idx->data[c1_b_p],
                        c1_idx->data[c1_q])) {
          c1_iwork->data[c1_d_k] = c1_idx->data[c1_b_p];
          c1_b_p++;
          if (c1_b_p + 1 == c1_pEnd) {
            while (c1_q + 1 < c1_qEnd) {
              c1_d_k++;
              c1_iwork->data[c1_d_k] = c1_idx->data[c1_q];
              c1_q++;
            }
          }
        } else {
          c1_iwork->data[c1_d_k] = c1_idx->data[c1_q];
          c1_q++;
          if (c1_q + 1 == c1_qEnd) {
            while (c1_b_p + 1 < c1_pEnd) {
              c1_d_k++;
              c1_iwork->data[c1_d_k] = c1_idx->data[c1_b_p];
              c1_b_p++;
            }
          }
        }

        c1_d_k++;
      }

      c1_b_p = c1_j - 2;
      c1_e_b = c1_kEnd;
      c1_f_b = c1_e_b;
      if (1 > c1_f_b) {
        c1_c_overflow = false;
      } else {
        c1_c_overflow = (c1_f_b > 2147483646);
      }

      if (c1_c_overflow) {
        c1_check_forloop_overflow_error(chartInstance, true);
      }

      for (c1_e_k = 0; c1_e_k < c1_kEnd; c1_e_k++) {
        c1_d_k = c1_e_k;
        c1_idx->data[(c1_b_p + c1_d_k) + 1] = c1_iwork->data[c1_d_k];
      }

      c1_j = c1_qEnd;
    }

    c1_i = c1_i2;
  }

  c1_emxFree_int32_T(chartInstance, &c1_e_y);
  c1_m = c1_y->size[0];
  c1_col[0] = c1_m;
  c1_col[1] = 1;
  c1_i1003 = c1_iwork->size[0];
  c1_iwork->size[0] = c1_col[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i1003,
    &c1_fj_emlrtRTEI);
  for (c1_b_j = 0; c1_b_j < 2; c1_b_j++) {
    c1_c_j = c1_b_j;
    c1_c_b = c1_m;
    c1_d_b = c1_c_b;
    if (1 > c1_d_b) {
      c1_b_overflow = false;
    } else {
      c1_b_overflow = (c1_d_b > 2147483646);
    }

    if (c1_b_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_b_i = 0; c1_b_i < c1_m; c1_b_i++) {
      c1_c_i = c1_b_i;
      c1_iwork->data[c1_c_i] = c1_y->data[(c1_idx->data[c1_c_i] + c1_y->size[0] *
        c1_c_j) - 1];
    }

    c1_g_b = c1_m;
    c1_h_b = c1_g_b;
    if (1 > c1_h_b) {
      c1_d_overflow = false;
    } else {
      c1_d_overflow = (c1_h_b > 2147483646);
    }

    if (c1_d_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_d_i = 0; c1_d_i < c1_m; c1_d_i++) {
      c1_c_i = c1_d_i;
      c1_y->data[c1_c_i + c1_y->size[0] * c1_c_j] = c1_iwork->data[c1_c_i];
    }
  }

  c1_emxFree_int32_T(chartInstance, &c1_iwork);
  c1_emxFree_int32_T(chartInstance, &c1_idx);
}

static void c1_b_sort(SFc1_LIDAR_simInstanceStruct *chartInstance,
                      c1_emxArray_real_T *c1_x)
{
  int32_T c1_dim;
  real_T c1_d44;
  c1_emxArray_real_T *c1_vwork;
  int32_T c1_vlen;
  int32_T c1_iv12[2];
  int32_T c1_i1009;
  int32_T c1_b_dim;
  int32_T c1_vstride;
  int32_T c1_i1010;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_c_b;
  real_T c1_d45;
  int32_T c1_d_b;
  boolean_T c1_b_overflow;
  int32_T c1_j;
  c1_emxArray_int32_T *c1_b_vwork;
  int32_T c1_b_j;
  int32_T c1_e_b;
  int32_T c1_f_b;
  boolean_T c1_c_overflow;
  int32_T c1_b_k;
  int32_T c1_c_k;
  int32_T c1_g_b;
  int32_T c1_h_b;
  boolean_T c1_d_overflow;
  int32_T c1_d_k;
  c1_dim = 2;
  if ((real_T)c1_x->size[0] != 1.0) {
    c1_dim = 1;
  }

  if (c1_dim <= 1) {
    c1_d44 = (real_T)c1_x->size[0];
  } else {
    c1_d44 = 1.0;
  }

  c1_emxInit_real_T1(chartInstance, &c1_vwork, 1, &c1_gj_emlrtRTEI);
  c1_vlen = (int32_T)c1_d44;
  c1_iv12[0] = c1_vlen;
  c1_iv12[1] = 1;
  c1_i1009 = c1_vwork->size[0];
  c1_vwork->size[0] = c1_iv12[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_vwork, c1_i1009,
    &c1_qh_emlrtRTEI);
  c1_b_dim = c1_dim - 1;
  c1_vstride = 1;
  c1_i1010 = c1_b_dim;
  c1_b = c1_i1010;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_i1010; c1_k++) {
    c1_d45 = (real_T)c1_x->size[0];
    c1_vstride *= (int32_T)c1_d45;
  }

  c1_c_b = c1_vstride;
  c1_d_b = c1_c_b;
  if (1 > c1_d_b) {
    c1_b_overflow = false;
  } else {
    c1_b_overflow = (c1_d_b > 2147483646);
  }

  if (c1_b_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  c1_j = 1;
  c1_emxInit_int32_T(chartInstance, &c1_b_vwork, 1, &c1_qh_emlrtRTEI);
  while (c1_j - 1 <= c1_vstride - 1) {
    c1_b_j = c1_j - 1;
    c1_e_b = c1_vlen;
    c1_f_b = c1_e_b;
    if (1 > c1_f_b) {
      c1_c_overflow = false;
    } else {
      c1_c_overflow = (c1_f_b > 2147483646);
    }

    if (c1_c_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_b_k = 0; c1_b_k < c1_vlen; c1_b_k++) {
      c1_c_k = c1_b_k;
      c1_vwork->data[c1_c_k] = c1_x->data[c1_b_j + c1_c_k * c1_vstride];
    }

    c1_b_sortIdx(chartInstance, c1_vwork, c1_b_vwork);
    c1_g_b = c1_vlen;
    c1_h_b = c1_g_b;
    if (1 > c1_h_b) {
      c1_d_overflow = false;
    } else {
      c1_d_overflow = (c1_h_b > 2147483646);
    }

    if (c1_d_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_d_k = 0; c1_d_k < c1_vlen; c1_d_k++) {
      c1_c_k = c1_d_k;
      c1_x->data[c1_b_j + c1_c_k * c1_vstride] = c1_vwork->data[c1_c_k];
    }

    c1_j++;
  }

  c1_emxFree_int32_T(chartInstance, &c1_b_vwork);
  c1_emxFree_real_T(chartInstance, &c1_vwork);
}

static void c1_b_sortIdx(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T *c1_x, c1_emxArray_int32_T *c1_idx)
{
  real_T c1_b_x[2];
  int32_T c1_i1011;
  int32_T c1_i1012;
  int32_T c1_loop_ub;
  int32_T c1_i1013;
  int32_T c1_n;
  int32_T c1_b_n;
  int32_T c1_i1014;
  int32_T c1_i1015;
  real_T c1_x4[4];
  c1_emxArray_int32_T *c1_iwork;
  int32_T c1_idx4[4];
  int32_T c1_i1016;
  int32_T c1_iv13[1];
  int32_T c1_i1017;
  int32_T c1_i1018;
  int32_T c1_i1019;
  int32_T c1_b_loop_ub;
  int32_T c1_i1020;
  c1_emxArray_real_T *c1_xwork;
  int32_T c1_i1021;
  int32_T c1_i1022;
  int32_T c1_i1023;
  int32_T c1_i1024;
  int32_T c1_c_loop_ub;
  int32_T c1_i1025;
  int32_T c1_nNaNs;
  int32_T c1_ib;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_wOffset;
  int32_T c1_b_k;
  real_T c1_c_x;
  int32_T c1_c_n;
  boolean_T c1_c_b;
  int32_T c1_m;
  int32_T c1_i1026;
  int32_T c1_d_b;
  int32_T c1_e_b;
  int32_T c1_perm[4];
  boolean_T c1_b_overflow;
  int32_T c1_quartetOffset;
  int32_T c1_f_b;
  int32_T c1_g_b;
  int32_T c1_i1;
  int32_T c1_c_k;
  int32_T c1_i2;
  boolean_T c1_c_overflow;
  int32_T c1_i3;
  int32_T c1_itmp;
  int32_T c1_i4;
  int32_T c1_nNonNaN;
  int32_T c1_d_k;
  int32_T c1_preSortLevel;
  int32_T c1_nBlocks;
  int32_T c1_h_b;
  int32_T c1_i_b;
  boolean_T c1_d_overflow;
  int32_T c1_j_b;
  int32_T c1_tailOffset;
  int32_T c1_k_b;
  int32_T c1_nLastBlock;
  int32_T c1_offset;
  int32_T c1_l_b;
  int32_T c1_bLen;
  int32_T c1_bLen2;
  int32_T c1_nPairs;
  int32_T c1_m_b;
  int32_T c1_n_b;
  boolean_T c1_e_overflow;
  int32_T c1_e_k;
  int32_T c1_f_k;
  int32_T c1_blockOffset;
  int32_T c1_o_b;
  int32_T c1_p_b;
  boolean_T c1_f_overflow;
  int32_T c1_j;
  int32_T c1_p;
  int32_T c1_b_j;
  int32_T c1_q;
  int32_T c1_b_iwork[256];
  int32_T c1_iout;
  real_T c1_b_xwork[256];
  int32_T c1_offset1;
  int32_T c1_a;
  int32_T c1_q_b;
  int32_T c1_b_a;
  int32_T c1_r_b;
  boolean_T c1_g_overflow;
  int32_T c1_c_j;
  int32_T exitg1;
  c1_b_x[0] = (real_T)c1_x->size[0];
  c1_b_x[1] = 1.0;
  for (c1_i1011 = 0; c1_i1011 < 2; c1_i1011++) {
    c1_b_x[c1_i1011];
  }

  c1_i1012 = c1_idx->size[0];
  c1_idx->size[0] = (int32_T)c1_b_x[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_idx, c1_i1012, &c1_hj_emlrtRTEI);
  c1_loop_ub = (int32_T)c1_b_x[0] - 1;
  for (c1_i1013 = 0; c1_i1013 <= c1_loop_ub; c1_i1013++) {
    c1_idx->data[c1_i1013] = 0;
  }

  c1_n = c1_x->size[0];
  c1_b_n = c1_x->size[0];
  for (c1_i1014 = 0; c1_i1014 < 4; c1_i1014++) {
    c1_x4[c1_i1014] = 0.0;
  }

  for (c1_i1015 = 0; c1_i1015 < 4; c1_i1015++) {
    c1_idx4[c1_i1015] = 0;
  }

  c1_emxInit_int32_T(chartInstance, &c1_iwork, 1, &c1_nj_emlrtRTEI);
  c1_i1016 = c1_iwork->size[0];
  c1_iwork->size[0] = c1_idx->size[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i1016,
    &c1_ij_emlrtRTEI);
  c1_iv13[0] = c1_iwork->size[0];
  c1_i1017 = c1_iwork->size[0];
  c1_iwork->size[0] = c1_iv13[0];
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i1017,
    &c1_jj_emlrtRTEI);
  c1_i1018 = c1_iwork->size[0];
  c1_i1019 = c1_iwork->size[0];
  c1_iwork->size[0] = c1_i1018;
  c1_emxEnsureCapacity_int32_T(chartInstance, c1_iwork, c1_i1019,
    &c1_kj_emlrtRTEI);
  c1_b_loop_ub = c1_i1018 - 1;
  for (c1_i1020 = 0; c1_i1020 <= c1_b_loop_ub; c1_i1020++) {
    c1_iwork->data[c1_i1020] = 0;
  }

  c1_emxInit_real_T1(chartInstance, &c1_xwork, 1, &c1_oj_emlrtRTEI);
  c1_b_x[0] = (real_T)c1_x->size[0];
  c1_b_x[1] = 1.0;
  c1_i1021 = c1_xwork->size[0];
  c1_xwork->size[0] = (int32_T)c1_b_x[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_xwork, c1_i1021,
    &c1_lj_emlrtRTEI);
  c1_iv13[0] = c1_xwork->size[0];
  c1_i1022 = c1_xwork->size[0];
  c1_xwork->size[0] = c1_iv13[0];
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_xwork, c1_i1022,
    &c1_jj_emlrtRTEI);
  c1_i1023 = c1_xwork->size[0];
  c1_i1024 = c1_xwork->size[0];
  c1_xwork->size[0] = c1_i1023;
  c1_emxEnsureCapacity_real_T1(chartInstance, c1_xwork, c1_i1024,
    &c1_mj_emlrtRTEI);
  c1_c_loop_ub = c1_i1023 - 1;
  for (c1_i1025 = 0; c1_i1025 <= c1_c_loop_ub; c1_i1025++) {
    c1_xwork->data[c1_i1025] = 0.0;
  }

  c1_nNaNs = 0;
  c1_ib = 0;
  c1_b = c1_b_n;
  c1_b_b = c1_b;
  if (1 > c1_b_b) {
    c1_overflow = false;
  } else {
    c1_overflow = (c1_b_b > 2147483646);
  }

  if (c1_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_k = 0; c1_k < c1_b_n; c1_k++) {
    c1_b_k = c1_k;
    c1_c_x = c1_x->data[c1_b_k];
    c1_c_b = muDoubleScalarIsNaN(c1_c_x);
    if (c1_c_b) {
      c1_idx->data[(c1_b_n - c1_nNaNs) - 1] = c1_b_k + 1;
      c1_xwork->data[(c1_b_n - c1_nNaNs) - 1] = c1_x->data[c1_b_k];
      c1_nNaNs++;
    } else {
      c1_ib++;
      c1_idx4[c1_ib - 1] = c1_b_k + 1;
      c1_x4[c1_ib - 1] = c1_x->data[c1_b_k];
      if (c1_ib == 4) {
        c1_quartetOffset = c1_b_k - c1_nNaNs;
        if (c1_x4[0] <= c1_x4[1]) {
          c1_i1 = 1;
          c1_i2 = 2;
        } else {
          c1_i1 = 2;
          c1_i2 = 1;
        }

        if (c1_x4[2] <= c1_x4[3]) {
          c1_i3 = 3;
          c1_i4 = 4;
        } else {
          c1_i3 = 4;
          c1_i4 = 3;
        }

        if (c1_x4[c1_i1 - 1] <= c1_x4[c1_i3 - 1]) {
          if (c1_x4[c1_i2 - 1] <= c1_x4[c1_i3 - 1]) {
            c1_perm[0] = c1_i1;
            c1_perm[1] = c1_i2;
            c1_perm[2] = c1_i3;
            c1_perm[3] = c1_i4;
          } else if (c1_x4[c1_i2 - 1] <= c1_x4[c1_i4 - 1]) {
            c1_perm[0] = c1_i1;
            c1_perm[1] = c1_i3;
            c1_perm[2] = c1_i2;
            c1_perm[3] = c1_i4;
          } else {
            c1_perm[0] = c1_i1;
            c1_perm[1] = c1_i3;
            c1_perm[2] = c1_i4;
            c1_perm[3] = c1_i2;
          }
        } else if (c1_x4[c1_i1 - 1] <= c1_x4[c1_i4 - 1]) {
          if (c1_x4[c1_i2 - 1] <= c1_x4[c1_i4 - 1]) {
            c1_perm[0] = c1_i3;
            c1_perm[1] = c1_i1;
            c1_perm[2] = c1_i2;
            c1_perm[3] = c1_i4;
          } else {
            c1_perm[0] = c1_i3;
            c1_perm[1] = c1_i1;
            c1_perm[2] = c1_i4;
            c1_perm[3] = c1_i2;
          }
        } else {
          c1_perm[0] = c1_i3;
          c1_perm[1] = c1_i4;
          c1_perm[2] = c1_i1;
          c1_perm[3] = c1_i2;
        }

        c1_idx->data[c1_quartetOffset - 3] = c1_idx4[c1_perm[0] - 1];
        c1_idx->data[c1_quartetOffset - 2] = c1_idx4[c1_perm[1] - 1];
        c1_idx->data[c1_quartetOffset - 1] = c1_idx4[c1_perm[2] - 1];
        c1_idx->data[c1_quartetOffset] = c1_idx4[c1_perm[3] - 1];
        c1_x->data[c1_quartetOffset - 3] = c1_x4[c1_perm[0] - 1];
        c1_x->data[c1_quartetOffset - 2] = c1_x4[c1_perm[1] - 1];
        c1_x->data[c1_quartetOffset - 1] = c1_x4[c1_perm[2] - 1];
        c1_x->data[c1_quartetOffset] = c1_x4[c1_perm[3] - 1];
        c1_ib = 0;
      }
    }
  }

  c1_wOffset = (c1_b_n - c1_nNaNs) - 1;
  if (c1_ib > 0) {
    c1_c_n = c1_ib;
    for (c1_i1026 = 0; c1_i1026 < 4; c1_i1026++) {
      c1_perm[c1_i1026] = 0;
    }

    if (c1_c_n == 1) {
      c1_perm[0] = 1;
    } else if (c1_c_n == 2) {
      if (c1_x4[0] <= c1_x4[1]) {
        c1_perm[0] = 1;
        c1_perm[1] = 2;
      } else {
        c1_perm[0] = 2;
        c1_perm[1] = 1;
      }
    } else if (c1_x4[0] <= c1_x4[1]) {
      if (c1_x4[1] <= c1_x4[2]) {
        c1_perm[0] = 1;
        c1_perm[1] = 2;
        c1_perm[2] = 3;
      } else if (c1_x4[0] <= c1_x4[2]) {
        c1_perm[0] = 1;
        c1_perm[1] = 3;
        c1_perm[2] = 2;
      } else {
        c1_perm[0] = 3;
        c1_perm[1] = 1;
        c1_perm[2] = 2;
      }
    } else if (c1_x4[0] <= c1_x4[2]) {
      c1_perm[0] = 2;
      c1_perm[1] = 1;
      c1_perm[2] = 3;
    } else if (c1_x4[1] <= c1_x4[2]) {
      c1_perm[0] = 2;
      c1_perm[1] = 3;
      c1_perm[2] = 1;
    } else {
      c1_perm[0] = 3;
      c1_perm[1] = 2;
      c1_perm[2] = 1;
    }

    c1_f_b = c1_ib;
    c1_g_b = c1_f_b;
    if (1 > c1_g_b) {
      c1_c_overflow = false;
    } else {
      c1_c_overflow = (c1_g_b > 2147483646);
    }

    if (c1_c_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_d_k = 0; c1_d_k < c1_ib; c1_d_k++) {
      c1_b_k = c1_d_k;
      c1_idx->data[((c1_wOffset - c1_ib) + c1_b_k) + 1] = c1_idx4[c1_perm[c1_b_k]
        - 1];
      c1_x->data[((c1_wOffset - c1_ib) + c1_b_k) + 1] = c1_x4[c1_perm[c1_b_k] -
        1];
    }
  }

  c1_m = c1_nNaNs >> 1;
  c1_d_b = c1_m;
  c1_e_b = c1_d_b;
  if (1 > c1_e_b) {
    c1_b_overflow = false;
  } else {
    c1_b_overflow = (c1_e_b > 2147483646);
  }

  if (c1_b_overflow) {
    c1_check_forloop_overflow_error(chartInstance, true);
  }

  for (c1_c_k = 1; c1_c_k - 1 < c1_m; c1_c_k++) {
    c1_b_k = c1_c_k;
    c1_itmp = c1_idx->data[c1_wOffset + c1_b_k];
    c1_idx->data[c1_wOffset + c1_b_k] = c1_idx->data[c1_b_n - c1_b_k];
    c1_idx->data[c1_b_n - c1_b_k] = c1_itmp;
    c1_x->data[c1_wOffset + c1_b_k] = c1_xwork->data[c1_b_n - c1_b_k];
    c1_x->data[c1_b_n - c1_b_k] = c1_xwork->data[c1_wOffset + c1_b_k];
  }

  if ((c1_nNaNs & 1) != 0) {
    c1_x->data[(c1_wOffset + c1_m) + 1] = c1_xwork->data[(c1_wOffset + c1_m) + 1];
  }

  c1_nNonNaN = c1_n - c1_nNaNs;
  c1_preSortLevel = 2;
  if (c1_nNonNaN > 1) {
    if (c1_n >= 256) {
      c1_nBlocks = c1_nNonNaN >> 8;
      if (c1_nBlocks > 0) {
        c1_h_b = c1_nBlocks;
        c1_i_b = c1_h_b;
        if (1 > c1_i_b) {
          c1_d_overflow = false;
        } else {
          c1_d_overflow = (c1_i_b > 2147483646);
        }

        if (c1_d_overflow) {
          c1_check_forloop_overflow_error(chartInstance, true);
        }

        for (c1_j_b = 0; c1_j_b < c1_nBlocks; c1_j_b++) {
          c1_k_b = c1_j_b;
          c1_offset = c1_k_b << 8;
          for (c1_l_b = 0; c1_l_b < 6; c1_l_b++) {
            c1_bLen = 1 << (c1_l_b + 2);
            c1_bLen2 = c1_bLen << 1;
            c1_nPairs = 256 >> (c1_l_b + 3);
            c1_m_b = c1_nPairs;
            c1_n_b = c1_m_b;
            if (1 > c1_n_b) {
              c1_e_overflow = false;
            } else {
              c1_e_overflow = (c1_n_b > 2147483646);
            }

            if (c1_e_overflow) {
              c1_check_forloop_overflow_error(chartInstance, true);
            }

            for (c1_e_k = 0; c1_e_k < c1_nPairs; c1_e_k++) {
              c1_f_k = c1_e_k;
              c1_blockOffset = (c1_offset + c1_f_k * c1_bLen2) - 1;
              c1_o_b = c1_bLen2;
              c1_p_b = c1_o_b;
              if (1 > c1_p_b) {
                c1_f_overflow = false;
              } else {
                c1_f_overflow = (c1_p_b > 2147483646);
              }

              if (c1_f_overflow) {
                c1_check_forloop_overflow_error(chartInstance, true);
              }

              for (c1_j = 0; c1_j < c1_bLen2; c1_j++) {
                c1_b_j = c1_j;
                c1_b_iwork[c1_b_j] = c1_idx->data[(c1_blockOffset + c1_b_j) + 1];
                c1_b_xwork[c1_b_j] = c1_x->data[(c1_blockOffset + c1_b_j) + 1];
              }

              c1_p = 0;
              c1_q = c1_bLen;
              c1_iout = c1_blockOffset;
              do {
                exitg1 = 0;
                c1_iout++;
                if (c1_b_xwork[c1_p] <= c1_b_xwork[c1_q]) {
                  c1_idx->data[c1_iout] = c1_b_iwork[c1_p];
                  c1_x->data[c1_iout] = c1_b_xwork[c1_p];
                  if (c1_p + 1 < c1_bLen) {
                    c1_p++;
                  } else {
                    exitg1 = 1;
                  }
                } else {
                  c1_idx->data[c1_iout] = c1_b_iwork[c1_q];
                  c1_x->data[c1_iout] = c1_b_xwork[c1_q];
                  if (c1_q + 1 < c1_bLen2) {
                    c1_q++;
                  } else {
                    c1_offset1 = c1_iout - c1_p;
                    c1_a = c1_p + 1;
                    c1_q_b = c1_bLen;
                    c1_b_a = c1_a;
                    c1_r_b = c1_q_b;
                    if (c1_b_a > c1_r_b) {
                      c1_g_overflow = false;
                    } else {
                      c1_g_overflow = (c1_r_b > 2147483646);
                    }

                    if (c1_g_overflow) {
                      c1_check_forloop_overflow_error(chartInstance, true);
                    }

                    for (c1_c_j = c1_p + 1; c1_c_j <= c1_bLen; c1_c_j++) {
                      c1_idx->data[c1_offset1 + c1_c_j] = c1_b_iwork[c1_c_j - 1];
                      c1_x->data[c1_offset1 + c1_c_j] = c1_b_xwork[c1_c_j - 1];
                    }

                    exitg1 = 1;
                  }
                }
              } while (exitg1 == 0);
            }
          }
        }

        c1_tailOffset = c1_nBlocks << 8;
        c1_nLastBlock = c1_nNonNaN - c1_tailOffset;
        if (c1_nLastBlock > 0) {
          c1_b_merge_block(chartInstance, c1_idx, c1_x, c1_tailOffset,
                           c1_nLastBlock, 2, c1_iwork, c1_xwork);
        }

        c1_preSortLevel = 8;
      }
    }

    c1_b_merge_block(chartInstance, c1_idx, c1_x, 0, c1_nNonNaN, c1_preSortLevel,
                     c1_iwork, c1_xwork);
  }

  c1_emxFree_real_T(chartInstance, &c1_xwork);
  c1_emxFree_int32_T(chartInstance, &c1_iwork);
}

static void c1_b_merge_block(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x, int32_T c1_offset,
  int32_T c1_n, int32_T c1_preSortLevel, c1_emxArray_int32_T *c1_iwork,
  c1_emxArray_real_T *c1_xwork)
{
  int32_T c1_nBlocks;
  int32_T c1_bLen;
  int32_T c1_bLen2;
  int32_T c1_tailOffset;
  int32_T c1_nPairs;
  int32_T c1_nTail;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_k;
  int32_T c1_b_k;
  c1_nBlocks = c1_n >> c1_preSortLevel;
  c1_bLen = 1 << c1_preSortLevel;
  while (c1_nBlocks > 1) {
    if ((c1_nBlocks & 1) != 0) {
      c1_nBlocks--;
      c1_tailOffset = c1_bLen * c1_nBlocks;
      c1_nTail = c1_n - c1_tailOffset;
      if (c1_nTail > c1_bLen) {
        c1_b_merge(chartInstance, c1_idx, c1_x, c1_offset + c1_tailOffset,
                   c1_bLen, c1_nTail - c1_bLen, c1_iwork, c1_xwork);
      }
    }

    c1_bLen2 = c1_bLen << 1;
    c1_nPairs = c1_nBlocks >> 1;
    c1_b = c1_nPairs;
    c1_b_b = c1_b;
    if (1 > c1_b_b) {
      c1_overflow = false;
    } else {
      c1_overflow = (c1_b_b > 2147483646);
    }

    if (c1_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_k = 0; c1_k < c1_nPairs; c1_k++) {
      c1_b_k = c1_k;
      c1_b_merge(chartInstance, c1_idx, c1_x, c1_offset + c1_b_k * c1_bLen2,
                 c1_bLen, c1_bLen, c1_iwork, c1_xwork);
    }

    c1_bLen = c1_bLen2;
    c1_nBlocks = c1_nPairs;
  }

  if (c1_n > c1_bLen) {
    c1_b_merge(chartInstance, c1_idx, c1_x, c1_offset, c1_bLen, c1_n - c1_bLen,
               c1_iwork, c1_xwork);
  }
}

static void c1_b_merge(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T *c1_idx, c1_emxArray_real_T *c1_x, int32_T c1_offset,
  int32_T c1_np, int32_T c1_nq, c1_emxArray_int32_T *c1_iwork,
  c1_emxArray_real_T *c1_xwork)
{
  int32_T c1_n;
  int32_T c1_b;
  int32_T c1_b_b;
  boolean_T c1_overflow;
  int32_T c1_j;
  int32_T c1_p;
  int32_T c1_b_j;
  int32_T c1_q;
  int32_T c1_qend;
  int32_T c1_iout;
  int32_T c1_offset1;
  int32_T c1_a;
  int32_T c1_c_b;
  int32_T c1_b_a;
  int32_T c1_d_b;
  boolean_T c1_b_overflow;
  int32_T c1_c_j;
  int32_T exitg1;
  if (c1_nq == 0) {
  } else {
    c1_n = c1_np + c1_nq;
    c1_b = c1_n;
    c1_b_b = c1_b;
    if (1 > c1_b_b) {
      c1_overflow = false;
    } else {
      c1_overflow = (c1_b_b > 2147483646);
    }

    if (c1_overflow) {
      c1_check_forloop_overflow_error(chartInstance, true);
    }

    for (c1_j = 0; c1_j < c1_n; c1_j++) {
      c1_b_j = c1_j;
      c1_iwork->data[c1_b_j] = c1_idx->data[c1_offset + c1_b_j];
      c1_xwork->data[c1_b_j] = c1_x->data[c1_offset + c1_b_j];
    }

    c1_p = 1;
    c1_q = c1_np;
    c1_qend = c1_np + c1_nq;
    c1_iout = c1_offset - 1;
    do {
      exitg1 = 0;
      c1_iout++;
      if (c1_xwork->data[c1_p - 1] <= c1_xwork->data[c1_q]) {
        c1_idx->data[c1_iout] = c1_iwork->data[c1_p - 1];
        c1_x->data[c1_iout] = c1_xwork->data[c1_p - 1];
        if (c1_p < c1_np) {
          c1_p++;
        } else {
          exitg1 = 1;
        }
      } else {
        c1_idx->data[c1_iout] = c1_iwork->data[c1_q];
        c1_x->data[c1_iout] = c1_xwork->data[c1_q];
        if (c1_q + 1 < c1_qend) {
          c1_q++;
        } else {
          c1_offset1 = (c1_iout - c1_p) + 1;
          c1_a = c1_p;
          c1_c_b = c1_np;
          c1_b_a = c1_a;
          c1_d_b = c1_c_b;
          if (c1_b_a > c1_d_b) {
            c1_b_overflow = false;
          } else {
            c1_b_overflow = (c1_d_b > 2147483646);
          }

          if (c1_b_overflow) {
            c1_check_forloop_overflow_error(chartInstance, true);
          }

          for (c1_c_j = c1_p; c1_c_j <= c1_np; c1_c_j++) {
            c1_idx->data[c1_offset1 + c1_c_j] = c1_iwork->data[c1_c_j - 1];
            c1_x->data[c1_offset1 + c1_c_j] = c1_xwork->data[c1_c_j - 1];
          }

          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);
  }
}

static void c1_b_cosd(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T *c1_x)
{
  real_T c1_b_x;
  real_T c1_c_x;
  real_T c1_d_x;
  boolean_T c1_b;
  boolean_T c1_b89;
  real_T c1_e_x;
  boolean_T c1_b_b;
  boolean_T c1_b90;
  boolean_T c1_c_b;
  real_T c1_f_x;
  real_T c1_g_x;
  real_T c1_h_x;
  real_T c1_i_x;
  real_T c1_j_x;
  real_T c1_k_x;
  real_T c1_absx;
  int8_T c1_n;
  real_T c1_l_x;
  real_T c1_m_x;
  real_T c1_n_x;
  (void)chartInstance;
  c1_b_x = *c1_x;
  c1_c_x = c1_b_x;
  c1_d_x = c1_c_x;
  c1_b = muDoubleScalarIsInf(c1_d_x);
  c1_b89 = !c1_b;
  c1_e_x = c1_c_x;
  c1_b_b = muDoubleScalarIsNaN(c1_e_x);
  c1_b90 = !c1_b_b;
  c1_c_b = (c1_b89 && c1_b90);
  if (!c1_c_b) {
    *c1_x = rtNaN;
  } else {
    c1_f_x = c1_b_x;
    c1_g_x = c1_f_x;
    c1_h_x = c1_g_x;
    c1_i_x = c1_h_x;
    c1_j_x = c1_i_x;
    c1_g_x = muDoubleScalarRem(c1_j_x, 360.0);
    c1_k_x = c1_g_x;
    c1_absx = muDoubleScalarAbs(c1_k_x);
    if (c1_absx > 180.0) {
      if (c1_g_x > 0.0) {
        c1_g_x -= 360.0;
      } else {
        c1_g_x += 360.0;
      }

      c1_l_x = c1_g_x;
      c1_m_x = c1_l_x;
      c1_n_x = c1_m_x;
      c1_absx = muDoubleScalarAbs(c1_n_x);
    }

    if (c1_absx <= 45.0) {
      c1_g_x *= 0.017453292519943295;
      c1_n = 0;
    } else if (c1_absx <= 135.0) {
      if (c1_g_x > 0.0) {
        c1_g_x = 0.017453292519943295 * (c1_g_x - 90.0);
        c1_n = 1;
      } else {
        c1_g_x = 0.017453292519943295 * (c1_g_x + 90.0);
        c1_n = -1;
      }
    } else if (c1_g_x > 0.0) {
      c1_g_x = 0.017453292519943295 * (c1_g_x - 180.0);
      c1_n = 2;
    } else {
      c1_g_x = 0.017453292519943295 * (c1_g_x + 180.0);
      c1_n = -2;
    }

    if ((real_T)c1_n == 0.0) {
      *c1_x = muDoubleScalarCos(c1_g_x);
    } else if ((real_T)c1_n == 1.0) {
      *c1_x = -muDoubleScalarSin(c1_g_x);
    } else if ((real_T)c1_n == -1.0) {
      *c1_x = muDoubleScalarSin(c1_g_x);
    } else {
      *c1_x = -muDoubleScalarCos(c1_g_x);
    }
  }
}

static void c1_b_sind(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T *c1_x)
{
  real_T c1_b_x;
  real_T c1_c_x;
  real_T c1_d_x;
  boolean_T c1_b;
  boolean_T c1_b91;
  real_T c1_e_x;
  boolean_T c1_b_b;
  boolean_T c1_b92;
  boolean_T c1_c_b;
  real_T c1_f_x;
  real_T c1_g_x;
  real_T c1_h_x;
  real_T c1_i_x;
  real_T c1_j_x;
  real_T c1_k_x;
  real_T c1_l_x;
  real_T c1_absx;
  int8_T c1_n;
  real_T c1_m_x;
  real_T c1_n_x;
  real_T c1_o_x;
  (void)chartInstance;
  c1_b_x = *c1_x;
  c1_c_x = c1_b_x;
  c1_d_x = c1_c_x;
  c1_b = muDoubleScalarIsInf(c1_d_x);
  c1_b91 = !c1_b;
  c1_e_x = c1_c_x;
  c1_b_b = muDoubleScalarIsNaN(c1_e_x);
  c1_b92 = !c1_b_b;
  c1_c_b = (c1_b91 && c1_b92);
  if (!c1_c_b) {
    c1_g_x = rtNaN;
  } else {
    c1_f_x = c1_b_x;
    c1_h_x = c1_f_x;
    c1_i_x = c1_h_x;
    c1_j_x = c1_i_x;
    c1_k_x = c1_j_x;
    c1_h_x = muDoubleScalarRem(c1_k_x, 360.0);
    c1_l_x = c1_h_x;
    c1_absx = muDoubleScalarAbs(c1_l_x);
    if (c1_absx > 180.0) {
      if (c1_h_x > 0.0) {
        c1_h_x -= 360.0;
      } else {
        c1_h_x += 360.0;
      }

      c1_m_x = c1_h_x;
      c1_n_x = c1_m_x;
      c1_o_x = c1_n_x;
      c1_absx = muDoubleScalarAbs(c1_o_x);
    }

    if (c1_absx <= 45.0) {
      c1_h_x *= 0.017453292519943295;
      c1_n = 0;
    } else if (c1_absx <= 135.0) {
      if (c1_h_x > 0.0) {
        c1_h_x = 0.017453292519943295 * (c1_h_x - 90.0);
        c1_n = 1;
      } else {
        c1_h_x = 0.017453292519943295 * (c1_h_x + 90.0);
        c1_n = -1;
      }
    } else if (c1_h_x > 0.0) {
      c1_h_x = 0.017453292519943295 * (c1_h_x - 180.0);
      c1_n = 2;
    } else {
      c1_h_x = 0.017453292519943295 * (c1_h_x + 180.0);
      c1_n = -2;
    }

    if ((real_T)c1_n == 0.0) {
      c1_g_x = muDoubleScalarSin(c1_h_x);
    } else if ((real_T)c1_n == 1.0) {
      c1_g_x = muDoubleScalarCos(c1_h_x);
    } else if ((real_T)c1_n == -1.0) {
      c1_g_x = -muDoubleScalarCos(c1_h_x);
    } else {
      c1_g_x = -muDoubleScalarSin(c1_h_x);
    }
  }

  *c1_x = c1_g_x;
}

static void c1_emxEnsureCapacity_real_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(real_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(real_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (real_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxEnsureCapacity_real_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(real_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(real_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (real_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxEnsureCapacity_int32_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_int32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(int32_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(int32_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (int32_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxEnsureCapacity_boolean_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_boolean_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(boolean_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(boolean_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (boolean_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxEnsureCapacity_skoeQIuVNKJRH(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_emxArray, int32_T
  c1_oldNumel, const emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof
      (c1_skoeQIuVNKJRHNtBIlOCZhD));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(c1_skoeQIuVNKJRHNtBIlOCZhD) *
             (uint32_T)c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (c1_skoeQIuVNKJRHNtBIlOCZhD *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxInit_real_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_real_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_real_T *)emlrtMallocMex(sizeof(c1_emxArray_real_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (real_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInit_boolean_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_boolean_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_boolean_T *)emlrtMallocMex(sizeof
    (c1_emxArray_boolean_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (boolean_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInit_skoeQIuVNKJRHNtBIlOCZhD(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh **c1_pEmxArray, int32_T
  c1_numDimensions, const emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *)emlrtMallocMex(sizeof
    (c1_emxArray_skoeQIuVNKJRHNtBIlOCZh));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (c1_skoeQIuVNKJRHNtBIlOCZhD *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInit_real_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_real_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_real_T *)emlrtMallocMex(sizeof(c1_emxArray_real_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (real_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInit_int32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_int32_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_int32_T *)emlrtMallocMex(sizeof
    (c1_emxArray_int32_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (int32_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInitStruct_coder_internal_sp(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_sparse *c1_pStruct, const emlrtRTEInfo
  *c1_srcLocation)
{
  c1_emxInit_boolean_T1(chartInstance, &c1_pStruct->d, 1, c1_srcLocation);
  c1_emxInit_int32_T(chartInstance, &c1_pStruct->colidx, 1, c1_srcLocation);
  c1_emxInit_int32_T(chartInstance, &c1_pStruct->rowidx, 1, c1_srcLocation);
}

static void c1_emxInit_boolean_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_boolean_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_boolean_T *)emlrtMallocMex(sizeof
    (c1_emxArray_boolean_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (boolean_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxFree_real_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real_T **c1_pEmxArray)
{
  (void)chartInstance;
  if (*c1_pEmxArray != (c1_emxArray_real_T *)NULL) {
    if (((*c1_pEmxArray)->data != (real_T *)NULL) && (*c1_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c1_pEmxArray)->data);
    }

    emlrtFreeMex((*c1_pEmxArray)->size);
    emlrtFreeMex(*c1_pEmxArray);
    *c1_pEmxArray = (c1_emxArray_real_T *)NULL;
  }
}

static void c1_emxFree_boolean_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_pEmxArray)
{
  (void)chartInstance;
  if (*c1_pEmxArray != (c1_emxArray_boolean_T *)NULL) {
    if (((*c1_pEmxArray)->data != (boolean_T *)NULL) && (*c1_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c1_pEmxArray)->data);
    }

    emlrtFreeMex((*c1_pEmxArray)->size);
    emlrtFreeMex(*c1_pEmxArray);
    *c1_pEmxArray = (c1_emxArray_boolean_T *)NULL;
  }
}

static void c1_emxFree_skoeQIuVNKJRHNtBIlOCZhD(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_skoeQIuVNKJRHNtBIlOCZh **c1_pEmxArray)
{
  (void)chartInstance;
  if (*c1_pEmxArray != (c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *)NULL) {
    if (((*c1_pEmxArray)->data != (c1_skoeQIuVNKJRHNtBIlOCZhD *)NULL) &&
        (*c1_pEmxArray)->canFreeData) {
      emlrtFreeMex((*c1_pEmxArray)->data);
    }

    emlrtFreeMex((*c1_pEmxArray)->size);
    emlrtFreeMex(*c1_pEmxArray);
    *c1_pEmxArray = (c1_emxArray_skoeQIuVNKJRHNtBIlOCZh *)NULL;
  }
}

static void c1_emxFree_int32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_pEmxArray)
{
  (void)chartInstance;
  if (*c1_pEmxArray != (c1_emxArray_int32_T *)NULL) {
    if (((*c1_pEmxArray)->data != (int32_T *)NULL) && (*c1_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c1_pEmxArray)->data);
    }

    emlrtFreeMex((*c1_pEmxArray)->size);
    emlrtFreeMex(*c1_pEmxArray);
    *c1_pEmxArray = (c1_emxArray_int32_T *)NULL;
  }
}

static void c1_emxFreeStruct_coder_internal_sp(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_sparse *c1_pStruct)
{
  c1_emxFree_boolean_T(chartInstance, &c1_pStruct->d);
  c1_emxFree_int32_T(chartInstance, &c1_pStruct->colidx);
  c1_emxFree_int32_T(chartInstance, &c1_pStruct->rowidx);
}

static void c1_emxEnsureCapacity_boolean_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_boolean_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(boolean_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(boolean_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (boolean_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxCopyStruct_coder_internal_an(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_anonymous_function *c1_dst, const
  c1_coder_internal_anonymous_function *c1_src, const emlrtRTEInfo
  *c1_srcLocation)
{
  c1_emxCopyMatrix_real_T(chartInstance, c1_srcLocation);
  c1_emxCopyMatrix_cell_wrap_1(chartInstance, c1_dst->tunableEnvironment,
    c1_src->tunableEnvironment, c1_srcLocation);
}

static void c1_emxCopyMatrix_real_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  const emlrtRTEInfo *c1_srcLocation)
{
  (void)chartInstance;
  (void)c1_srcLocation;
}

static void c1_emxCopyMatrix_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 c1_dst[2], const c1_cell_wrap_1 c1_src[2],
  const emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_i;
  for (c1_i = 0; c1_i < 2; c1_i++) {
    c1_emxCopyStruct_cell_wrap_1(chartInstance, &c1_dst[c1_i], &c1_src[c1_i],
      c1_srcLocation);
  }
}

static void c1_emxCopyStruct_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 *c1_dst, const c1_cell_wrap_1 *c1_src, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxCopy_int32_T(chartInstance, &c1_dst->f1, &c1_src->f1, c1_srcLocation);
}

static void c1_emxCopy_int32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_dst, c1_emxArray_int32_T * const *c1_src, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_numElDst;
  int32_T c1_numElSrc;
  int32_T c1_i;
  c1_numElDst = 1;
  c1_numElSrc = 1;
  for (c1_i = 0; c1_i < (*c1_dst)->numDimensions; c1_i++) {
    c1_numElDst *= (*c1_dst)->size[c1_i];
    c1_numElSrc *= (*c1_src)->size[c1_i];
  }

  for (c1_i = 0; c1_i < (*c1_dst)->numDimensions; c1_i++) {
    (*c1_dst)->size[c1_i] = (*c1_src)->size[c1_i];
  }

  c1_emxEnsureCapacity_int32_T(chartInstance, *c1_dst, c1_numElDst,
    c1_srcLocation);
  for (c1_i = 0; c1_i < c1_numElSrc; c1_i++) {
    (*c1_dst)->data[c1_i] = (*c1_src)->data[c1_i];
  }
}

static void c1_emxInitStruct_coder_internal_an(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_anonymous_function *c1_pStruct, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxInitMatrix_cell_wrap_1(chartInstance, c1_pStruct->tunableEnvironment,
    c1_srcLocation);
}

static void c1_emxInitMatrix_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 c1_pMatrix[2], const emlrtRTEInfo
  *c1_srcLocation)
{
  int32_T c1_i;
  for (c1_i = 0; c1_i < 2; c1_i++) {
    c1_emxInitStruct_cell_wrap_1(chartInstance, &c1_pMatrix[c1_i],
      c1_srcLocation);
  }
}

static void c1_emxInitStruct_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 *c1_pStruct, const emlrtRTEInfo *c1_srcLocation)
{
  c1_emxInit_int32_T(chartInstance, &c1_pStruct->f1, 1, c1_srcLocation);
}

static void c1_emxFreeMatrix_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 c1_pMatrix[2])
{
  int32_T c1_i;
  for (c1_i = 0; c1_i < 2; c1_i++) {
    c1_emxFreeStruct_cell_wrap_1(chartInstance, &c1_pMatrix[c1_i]);
  }
}

static void c1_emxFreeStruct_cell_wrap_1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_cell_wrap_1 *c1_pStruct)
{
  c1_emxFree_int32_T(chartInstance, &c1_pStruct->f1);
}

static void c1_emxFreeStruct_coder_internal_an(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_anonymous_function *c1_pStruct)
{
  c1_emxFreeMatrix_cell_wrap_1(chartInstance, c1_pStruct->tunableEnvironment);
}

static void c1_emxCopyStruct_coder_internal_sp(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_coder_internal_sparse *c1_dst, const
  c1_coder_internal_sparse *c1_src, const emlrtRTEInfo *c1_srcLocation)
{
  c1_emxCopy_boolean_T(chartInstance, &c1_dst->d, &c1_src->d, c1_srcLocation);
  c1_emxCopy_int32_T(chartInstance, &c1_dst->colidx, &c1_src->colidx,
                     c1_srcLocation);
  c1_emxCopy_int32_T(chartInstance, &c1_dst->rowidx, &c1_src->rowidx,
                     c1_srcLocation);
  c1_dst->m = c1_src->m;
  c1_dst->n = c1_src->n;
  c1_dst->maxnz = c1_src->maxnz;
}

static void c1_emxCopy_boolean_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_boolean_T **c1_dst, c1_emxArray_boolean_T * const *c1_src, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_numElDst;
  int32_T c1_numElSrc;
  int32_T c1_i;
  c1_numElDst = 1;
  c1_numElSrc = 1;
  for (c1_i = 0; c1_i < (*c1_dst)->numDimensions; c1_i++) {
    c1_numElDst *= (*c1_dst)->size[c1_i];
    c1_numElSrc *= (*c1_src)->size[c1_i];
  }

  for (c1_i = 0; c1_i < (*c1_dst)->numDimensions; c1_i++) {
    (*c1_dst)->size[c1_i] = (*c1_src)->size[c1_i];
  }

  c1_emxEnsureCapacity_boolean_T1(chartInstance, *c1_dst, c1_numElDst,
    c1_srcLocation);
  for (c1_i = 0; c1_i < c1_numElSrc; c1_i++) {
    (*c1_dst)->data[c1_i] = (*c1_src)->data[c1_i];
  }
}

static void c1_emxEnsureCapacity_real32_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(real32_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(real32_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (real32_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxEnsureCapacity_real32_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_real32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(real32_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(real32_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (real32_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxInit_real32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_real32_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_real32_T *)emlrtMallocMex(sizeof
    (c1_emxArray_real32_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (real32_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInit_real32_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_real32_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_real32_T *)emlrtMallocMex(sizeof
    (c1_emxArray_real32_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (real32_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxFree_real32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_real32_T **c1_pEmxArray)
{
  (void)chartInstance;
  if (*c1_pEmxArray != (c1_emxArray_real32_T *)NULL) {
    if (((*c1_pEmxArray)->data != (real32_T *)NULL) && (*c1_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c1_pEmxArray)->data);
    }

    emlrtFreeMex((*c1_pEmxArray)->size);
    emlrtFreeMex(*c1_pEmxArray);
    *c1_pEmxArray = (c1_emxArray_real32_T *)NULL;
  }
}

static void c1_emxEnsureCapacity_int32_T1(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_int32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(int32_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(int32_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (int32_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxEnsureCapacity_uint32_T(SFc1_LIDAR_simInstanceStruct
  *chartInstance, c1_emxArray_uint32_T *c1_emxArray, int32_T c1_oldNumel, const
  emlrtRTEInfo *c1_srcLocation)
{
  int32_T c1_newNumel;
  int32_T c1_i;
  int32_T c1_newCapacity;
  void *c1_newData;
  if (c1_oldNumel < 0) {
    c1_oldNumel = 0;
  }

  c1_newNumel = 1;
  for (c1_i = 0; c1_i < c1_emxArray->numDimensions; c1_i++) {
    c1_newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)c1_newNumel, (uint32_T)
      c1_emxArray->size[c1_i], c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  if (c1_newNumel > c1_emxArray->allocatedSize) {
    c1_newCapacity = c1_emxArray->allocatedSize;
    if (c1_newCapacity < 16) {
      c1_newCapacity = 16;
    }

    while (c1_newCapacity < c1_newNumel) {
      if (c1_newCapacity > 1073741823) {
        c1_newCapacity = MAX_int32_T;
      } else {
        c1_newCapacity <<= 1;
      }
    }

    c1_newData = emlrtCallocMex((uint32_T)c1_newCapacity, sizeof(uint32_T));
    if (c1_newData == NULL) {
      emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
    }

    if (c1_emxArray->data != NULL) {
      memcpy(c1_newData, c1_emxArray->data, sizeof(uint32_T) * (uint32_T)
             c1_oldNumel);
      if (c1_emxArray->canFreeData) {
        emlrtFreeMex(c1_emxArray->data);
      }
    }

    c1_emxArray->data = (uint32_T *)c1_newData;
    c1_emxArray->allocatedSize = c1_newCapacity;
    c1_emxArray->canFreeData = true;
  }
}

static void c1_emxInit_int32_T1(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_int32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_int32_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_int32_T *)emlrtMallocMex(sizeof
    (c1_emxArray_int32_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (int32_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxInit_uint32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_uint32_T **c1_pEmxArray, int32_T c1_numDimensions, const
  emlrtRTEInfo *c1_srcLocation)
{
  c1_emxArray_uint32_T *c1_emxArray;
  int32_T c1_i;
  *c1_pEmxArray = (c1_emxArray_uint32_T *)emlrtMallocMex(sizeof
    (c1_emxArray_uint32_T));
  if ((void *)*c1_pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray = *c1_pEmxArray;
  c1_emxArray->data = (uint32_T *)NULL;
  c1_emxArray->numDimensions = c1_numDimensions;
  c1_emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * (uint32_T)
    c1_numDimensions);
  if ((void *)c1_emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(c1_srcLocation, chartInstance->c1_fEmlrtCtx);
  }

  c1_emxArray->allocatedSize = 0;
  c1_emxArray->canFreeData = true;
  for (c1_i = 0; c1_i < c1_numDimensions; c1_i++) {
    c1_emxArray->size[c1_i] = 0;
  }
}

static void c1_emxFree_uint32_T(SFc1_LIDAR_simInstanceStruct *chartInstance,
  c1_emxArray_uint32_T **c1_pEmxArray)
{
  (void)chartInstance;
  if (*c1_pEmxArray != (c1_emxArray_uint32_T *)NULL) {
    if (((*c1_pEmxArray)->data != (uint32_T *)NULL) && (*c1_pEmxArray)
        ->canFreeData) {
      emlrtFreeMex((*c1_pEmxArray)->data);
    }

    emlrtFreeMex((*c1_pEmxArray)->size);
    emlrtFreeMex(*c1_pEmxArray);
    *c1_pEmxArray = (c1_emxArray_uint32_T *)NULL;
  }
}

static int32_T c1_div_nzp_s32(SFc1_LIDAR_simInstanceStruct *chartInstance,
  int32_T c1_numerator, int32_T c1_denominator, uint32_T c1_ssid_src_loc,
  int32_T c1_offset_src_loc, int32_T c1_length_src_loc)
{
  int32_T c1_quotient;
  uint32_T c1_absNumerator;
  uint32_T c1_absDenominator;
  boolean_T c1_quotientNeedsNegation;
  uint32_T c1_tempAbsQuotient;
  (void)chartInstance;
  (void)c1_ssid_src_loc;
  (void)c1_offset_src_loc;
  (void)c1_length_src_loc;
  if (c1_numerator < 0) {
    c1_absNumerator = ~(uint32_T)c1_numerator + 1U;
  } else {
    c1_absNumerator = (uint32_T)c1_numerator;
  }

  if (c1_denominator < 0) {
    c1_absDenominator = ~(uint32_T)c1_denominator + 1U;
  } else {
    c1_absDenominator = (uint32_T)c1_denominator;
  }

  c1_quotientNeedsNegation = ((c1_numerator < 0) != (c1_denominator < 0));
  c1_tempAbsQuotient = c1_absNumerator / c1_absDenominator;
  if (c1_quotientNeedsNegation) {
    c1_quotient = -(int32_T)c1_tempAbsQuotient;
  } else {
    c1_quotient = (int32_T)c1_tempAbsQuotient;
  }

  return c1_quotient;
}

static int32_T c1__s32_s64_(SFc1_LIDAR_simInstanceStruct *chartInstance, int64_T
  c1_b, uint32_T c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T
  c1_length_src_loc)
{
  int32_T c1_a;
  (void)chartInstance;
  c1_a = (int32_T)c1_b;
  if ((int64_T)c1_a != c1_b) {
    _SFD_OVERFLOW_DETECTION(SFDB_OVERFLOW, c1_ssid_src_loc, c1_offset_src_loc,
      c1_length_src_loc);
  }

  return c1_a;
}

static int32_T c1__s32_d_(SFc1_LIDAR_simInstanceStruct *chartInstance, real_T
  c1_b, uint32_T c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T
  c1_length_src_loc)
{
  int32_T c1_a;
  real_T c1_b_b;
  (void)chartInstance;
  c1_a = (int32_T)c1_b;
  if (c1_b < 0.0) {
    c1_b_b = muDoubleScalarCeil(c1_b);
  } else {
    c1_b_b = muDoubleScalarFloor(c1_b);
  }

  if ((real_T)c1_a != c1_b_b) {
    _SFD_OVERFLOW_DETECTION(SFDB_OVERFLOW, c1_ssid_src_loc, c1_offset_src_loc,
      c1_length_src_loc);
  }

  return c1_a;
}

static void init_dsm_address_info(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_LIDAR_simInstanceStruct *chartInstance)
{
  chartInstance->c1_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c1_b_matrOfObstacles = (real_T (*)[100000])
    ssGetInputPortSignal_wrapper(chartInstance->S, 0);
  chartInstance->c1_rotationMatrix = (real_T (*)[4])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c1_invRotationMatrix = (real_T (*)[4])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 2);
}

/* SFunction Glue Code */
void sf_c1_LIDAR_sim_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3132190150U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3645366752U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(441876151U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2459058420U);
}

mxArray* sf_c1_LIDAR_sim_get_post_codegen_info(void);
mxArray *sf_c1_LIDAR_sim_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("WnLl9AgCC6rXAm2syDcacC");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(50000);
      pr[1] = (double)(2);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(2);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(2);
      pr[1] = (double)(2);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c1_LIDAR_sim_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c1_LIDAR_sim_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,6);
  mxSetCell(mxcell3p, 0, mxCreateString(
             "images.internal.coder.buildable.IppfilterBuildable"));
  mxSetCell(mxcell3p, 1, mxCreateString(
             "images.internal.coder.buildable.ImfilterBuildable"));
  mxSetCell(mxcell3p, 2, mxCreateString(
             "images.internal.coder.buildable.GetnumcoresBuildable"));
  mxSetCell(mxcell3p, 3, mxCreateString(
             "images.internal.coder.buildable.TbbhistBuildable"));
  mxSetCell(mxcell3p, 4, mxCreateString(
             "images.internal.coder.buildable.CannyThresholdingTbbBuildable"));
  mxSetCell(mxcell3p, 5, mxCreateString(
             "images.internal.coder.buildable.IppreconstructBuildable"));
  return(mxcell3p);
}

mxArray *sf_c1_LIDAR_sim_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("ir_function_calls");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("ippfilter_real32");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_LIDAR_sim_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c1_LIDAR_sim_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c1_LIDAR_sim(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[6],T\"invRotationMatrix\",},{M[1],M[5],T\"rotationMatrix\",},{M[8],M[0],T\"is_active_c1_LIDAR_sim\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_LIDAR_sim_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_LIDAR_simInstanceStruct *chartInstance = (SFc1_LIDAR_simInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _LIDAR_simMachineNumber_,
           1,
           1,
           1,
           0,
           3,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_LIDAR_simMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_LIDAR_simMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _LIDAR_simMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"matrOfObstacles");
          _SFD_SET_DATA_PROPS(1,2,0,1,"rotationMatrix");
          _SFD_SET_DATA_PROPS(2,2,0,1,"invRotationMatrix");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,2,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,3000);
        _SFD_CV_INIT_EML_IF(0,1,0,1792,1817,-1,1848);
        _SFD_CV_INIT_EML_IF(0,1,1,2321,2340,-1,-2);
        _SFD_CV_INIT_EML_FOR(0,1,0,2581,2605,2651);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,2324,2339,-1,2);

        {
          unsigned int dimVector[2];
          dimVector[0]= 50000U;
          dimVector[1]= 2U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_k_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 2U;
          dimVector[1]= 2U;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_l_sf_marshallOut,(MexInFcnForType)
            c1_j_sf_marshallIn);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 2U;
          dimVector[1]= 2U;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_l_sf_marshallOut,(MexInFcnForType)
            c1_j_sf_marshallIn);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _LIDAR_simMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_LIDAR_simInstanceStruct *chartInstance = (SFc1_LIDAR_simInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c1_b_matrOfObstacles);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c1_rotationMatrix);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c1_invRotationMatrix);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sgqsSycZ6LwIDzVZJ5KUOdF";
}

static void sf_opaque_initialize_c1_LIDAR_sim(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*)
    chartInstanceVar);
  initialize_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c1_LIDAR_sim(void *chartInstanceVar)
{
  enable_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c1_LIDAR_sim(void *chartInstanceVar)
{
  disable_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c1_LIDAR_sim(void *chartInstanceVar)
{
  sf_gateway_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_LIDAR_sim(SimStruct* S)
{
  return get_sim_state_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_LIDAR_sim(SimStruct* S, const mxArray *st)
{
  set_sim_state_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c1_LIDAR_sim(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_LIDAR_sim_optimization_info();
    }

    finalize_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_LIDAR_sim(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_LIDAR_sim((SFc1_LIDAR_simInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c1_LIDAR_sim(SimStruct *S)
{
  /* Set overwritable ports for inplace optimization */
  ssSetInputPortDirectFeedThrough(S, 0, 1);
  ssSetStatesModifiedOnlyInUpdate(S, 1);
  ssSetBlockIsPurelyCombinatorial_wrapper(S, 1);
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_LIDAR_sim_optimization_info(sim_mode_is_rtw_gen(S),
      sim_mode_is_modelref_sim(S), sim_mode_is_external(S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetSupportedForRowMajorCodeGen(S, 1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,1,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 1);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,1);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,1,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,1,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,1);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1527569741U));
  ssSetChecksum1(S,(1056431410U));
  ssSetChecksum2(S,(2414442640U));
  ssSetChecksum3(S,(3505070863U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c1_LIDAR_sim(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c1_LIDAR_sim(SimStruct *S)
{
  SFc1_LIDAR_simInstanceStruct *chartInstance;
  chartInstance = (SFc1_LIDAR_simInstanceStruct *)utMalloc(sizeof
    (SFc1_LIDAR_simInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc1_LIDAR_simInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c1_LIDAR_sim;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c1_LIDAR_sim;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c1_LIDAR_sim;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_LIDAR_sim;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c1_LIDAR_sim;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c1_LIDAR_sim;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c1_LIDAR_sim;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c1_LIDAR_sim;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_LIDAR_sim;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_LIDAR_sim;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_LIDAR_sim;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0, NULL, NULL);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c1_LIDAR_sim(chartInstance);
}

void c1_LIDAR_sim_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_LIDAR_sim(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_LIDAR_sim(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_LIDAR_sim(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_LIDAR_sim_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
