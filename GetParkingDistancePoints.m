function pointForParkingDistance= GetParkingDistancePoints(coordinatesToAdd,minimalDistanceFromCar,maximalDistanceFromCar,isItRightSide)
%Parking quality is graded between 0 and 1 based on the distance from the
%car. The most optimal parkingplaces are located somewhere between       
%minimalDistanceFromCar and maximalDistanceFromCar 
%(closer to minimalDistanceFromCar). The quality is calculated with a
%gaussian function which has a maximal sigma of 3
%
% inputs:  coordinatesToAdd - possible parkingplace-  array of coordinates
%                          -[bottomleft,topleft;topright,bottomright]
%          minimalDistanceFromCar - minimal distance from car where
%                                   parkingplace might be located
%
%          maximalDistanceFromCar - maximal distance from car where
%                                   parkingplace might be loacated
%
% output:  pointsForParkingDistance - scalar value between 0 and 1 which   
%                                    indicates the quality of parking place

sigma=abs(maximalDistanceFromCar-minimalDistanceFromCar)...
                                            /abs(maximalDistanceFromCar)*3;


if(isItRightSide==1)
    middlePoint=abs(abs(maximalDistanceFromCar-minimalDistanceFromCar)/2 ...
                                                -2*minimalDistanceFromCar);
    penalityMultiplierForOppositeSide=1;
else
    middlePoint=abs(abs(maximalDistanceFromCar-minimalDistanceFromCar)...
                                                -4*minimalDistanceFromCar);
    penalityMultiplierForOppositeSide=0.8;
end
actualPoint=abs(coordinatesToAdd(2,2));
pointForParkingDistance=gaussmf(actualPoint,[sigma middlePoint])...
                                      .*penalityMultiplierForOppositeSide;
if(pointForParkingDistance<0)
 pointForParkingDistance=0;
end

end

