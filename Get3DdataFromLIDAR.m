function matrOfObstacles3D = Get3DdataFromLIDAR(filepath,time)
%This functions reads data from VLP-16 , and returns a matrix storing a set
%of 2D coordinates representing obstaclepoints in xy plane. The Lidar is
%located at the origin of the local coordinate system (0,0)
%
%input:
%               sensorOrientation  -this matrix determines base vectors of 
%                                   the sensorOrientation, which should be 
%                                   fixed relative to the vehicle
%                                  -3x3 matrix
%
%output:        
%               matrOfObstacles    -2D matrix storing the relevant obstacle
%                                   points, without the ground, and too
%                                   high obstaclespoints, which would not
%                                   cause any collision
%                                  -nx2 matrix [x,y]

global draw

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Processing point cloud from VLP-16          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%Read from file%%%%%%%%%%%%%%%% should be read from LIDAR directly
veloReader = velodyneFileReader(filepath,'VLP16');



%Set the CurrentTime property to that time to being reading point clouds
%from there.
veloReader.CurrentTime = veloReader.StartTime + seconds(time);
ptCloudObj = readFrame(veloReader);


%VLP-16 specific%
layerNumber=16;


%%%%%Adding all layers to a 2D matrix %%%%%%%%%%% 
matrOfObstacles3D=NaN(size(ptCloudObj.Location,2)*layerNumber,3);
indexOfNewPoints=1;
iterator=size(ptCloudObj.Location,2);
 for i=1:layerNumber   %when pointcloud contains 16 layers
    matrOfObstacles3D(indexOfNewPoints:(indexOfNewPoints+iterator-1),:)=...
        [ptCloudObj.Location(i,:,1)',...
        ptCloudObj.Location(i,:,2)',...
        ptCloudObj.Location(i,:,3)'];
    
    indexOfNewPoints=indexOfNewPoints+iterator;
 end
 
if(draw)
    figure;
   pcshow(ptCloudObj); 
end

end

